package com.volga.dealershieldtablet.interfaceCallback;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdPartyReportList;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.LocalSalesPerson;
import com.volga.dealershieldtablet.screenRevamping.pojo.NoVerbalPromise;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class NetworkChangeReceiver extends BroadcastReceiver {
    public static final String NETWORK_AVAILABLE_ACTION = "com.volga.dealershieldtablet.interfaceCallback.interfaceCallback.NetworkChangeReceiver";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";
    public static final String STARTED_API_CALLS = "startedAPICalls";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
//        Timber.e(info.getDetailedState().toString());
        if (DSAPP.getInstance().isNetworkAvailable()) {
//            Intent intents = new Intent();
//            intents.putExtra(STARTED_API_CALLS, true);
//            intents.setAction("StartedAPI");
            getDealershipsSilently();
            getSalesPersonList();
            final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
            getCheckListItems(id);
            getVerbalPromiseList(id);
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData != null) {
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                if (record != null && record.getVehicleDetails() != null && record.getVehicleDetails().getVINNumber() != null && record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used") && record.getVehicleDetails().getVINNumber().length() == 17) {
//                    getAdditionalDisclosures(record.getVehicleDetails().getVINNumber());
                    Intent intents = new Intent();
                    intents.putExtra(STARTED_API_CALLS, DSAPP.getInstance().isNetworkAvailable());
                    intents.setAction("StartedAPI");
                    DSAPP.getInstance().sendBroadcast(intents);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setWasInternetAvailable(true);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                    getAdditionalDisclosures(record.getVehicleDetails().getVINNumber(), "CONDITION");
//                    getAdditionalDisclosures(record.getVehicleDetails().getVINNumber(), "HISTORY");
                    if (record.getVehicleDetails().getInitiateThirdParty() == null) {
                        callThirdPartyInitiateAPI(record.getVehicleDetails().getVINNumber());
                    } else {
                        downloadOfflineReportIfNotAvailable();
                        Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
                        networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, true);

                        LocalBroadcastManager.getInstance(DSAPP.getInstance()).sendBroadcast(networkStateIntent);
                        if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                            ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                            for (int i = 0; i < locallist.size(); i++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                    for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(locallist.get(i).getId()) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                            File file = ShowImageFromPath(locallist.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                            if (locallist.get(i).isActive()) {
                                                if (file == null || !file.exists()) {
                                                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), locallist.get(i).getId(), locallist.get(i).getTitle().trim());

                                                    }
                                                }
                                                if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_MULTI_SIGN)) {
                                                    getAllThirdPartyReportList(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), locallist.get(i).getId(), locallist.get(i).getTitle().trim());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Timber.e("Now internet is available");
        } else {
            Timber.e("Now internet is not available");
        }
    }

    private void callThirdPartyInitiateAPI(final String vin) {
        final Gson[] gson = {new GsonBuilder().create()};
        final RecordData recordData = gson[0].fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(true);
        String recordDataString = gson[0].toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().initiateThirdParty("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vin, id).enqueue(new Callback<InitiateThirdParty>() {
            @Override
            public void onResponse(Call<InitiateThirdParty> call, Response<InitiateThirdParty> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    gson[0] = new GsonBuilder().create();
                    Timber.e(gson[0].toJson(response.body()));

                    if (response.body().getSucceeded().equalsIgnoreCase("true")) {
                        RecordData recordData = gson[0].fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(response.body());
                        if (response.body().getValue().size() > 0) {
                            for (int i = 0; i < response.body().getValue().size(); i++) {
                                if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                    CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                    alerts.setAlertName("CarFax Report is Not Available");
                                    PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                                    PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getValue().get(i).getErrorsString());
                                    alerts.setAlertDesc(response.body().getValue().get(i).getErrorsString());
                                    alerts.setDocId(response.body().getValue().get(i).getThirdPartyID());
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(false);
//                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
                                } else if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("1") && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
//                                    if (tpId.equalsIgnoreCase("1")) {
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
                                    CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
//                                    } else if (tpId.equalsIgnoreCase("2")) {

//                                    }
                                }
                                if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("2") && response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                    CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                    PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                                    PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getValue().get(i).getErrorsString());
                                    alerts.setAlertName("AutoCheck Report is Not Available");
                                    alerts.setAlertDesc(response.body().getValue().get(i).getErrorsString());
                                    alerts.setDocId(response.body().getValue().get(i).getThirdPartyID());
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(false);
                                } else if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("2") && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
                                    CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                                }
                            }
                        }
                        String recordDataString = gson[0].toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }

                    RecordData recordData = gson[0].fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));

                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(response.body());
                        String recordDataString = gson[0].toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        if (response.body().getSucceeded().equalsIgnoreCase("true")) {
                            downloadOfflineReportIfNotAvailable();
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<InitiateThirdParty> call, Throwable t) {
                Log.e("Initiate Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void getAdditionalDisclosures(String vin, final String type) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getAdditionalDisclosures("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), /*"1FM5K8D84EGB73657"*/vin, /*"50348"*/id, type).enqueue(new Callback<AdditionalDisclosure>() {
            @Override
            public void onResponse(Call<AdditionalDisclosure> call, Response<AdditionalDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        if (type.equalsIgnoreCase("CONDITION") /*&& (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue().size() == 0)*/) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue().size() > 0) {
                                ArrayList<Value> local = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue();
                                ArrayList<Value> local1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue();
                                if (response.body() != null && response.body().getValue() != null && response.body().getValue().size() > 0) {
                                    for (int i = 0; i < response.body().getValue().size(); i++) {
                                        for (int j = 0; j < local.size(); j++) {
                                            if (response.body().getValue().get(i).getId().equalsIgnoreCase(local.get(j).getId())) {
                                                local1.get(j).setManual(false);
                                            }
                                        }
                                    }
                                }
                                AdditionalDisclosure additionalDisclosure1 = new AdditionalDisclosure();
                                additionalDisclosure1.setValue(local1);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(additionalDisclosure1);
                            } else {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(response.body());
                            }
                        } else if (type.equalsIgnoreCase("HISTORY")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalHistoryDisclosure(response.body());
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Log.i("AdditionalDisclosure: ", gson.toJson(response.body()));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                        Log.e("Size of history: ", response.body().getValue().size() + "");
                    }
                } else {
                    try {
                        Log.e("Error Addtnl ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AdditionalDisclosure> call, Throwable t) {

            }
        });
    }

    private void downloadOfflineReportIfNotAvailable() {
        Gson gson = new GsonBuilder().create();

        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record != null && record.getVehicleDetails() != null && record.getVehicleDetails().getVINNumber() != null && record.getVehicleDetails().getVINNumber().length() == 17) {
                /*Added null check to avoid deselection of manual logic */
                if (record.getVehicleDetails().getThirdPartyList() == null || record.getVehicleDetails().getThirdPartyList().getValue() == null || record.getVehicleDetails().getThirdPartyList().getValue().size() == 0 || record.getVehicleDetails().isOfflineData())
                    getThirdPartyOffline(record.getVehicleDetails().getVINNumber());
                if (record.getVehicleDetails().getVehicleConditionDisclosure() == null || record.getVehicleDetails().getVehicleConditionDisclosure().getValue() == null || record.getVehicleDetails().getVehicleConditionDisclosure().getValue().size() == 0 || record.getVehicleDetails().isOfflineData())
                    getConditionDisclosure(record.getVehicleDetails().getVINNumber());
                else {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(false);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                }
                if (record.getVehicleDetails().getVehicleHistoryDisclosure() == null || record.getVehicleDetails().getVehicleHistoryDisclosure().getValue() == null || record.getVehicleDetails().getVehicleHistoryDisclosure().getValue().size() == 0 || record.getVehicleDetails().isOfflineData())
                    getHistoryDisclosure(record.getVehicleDetails().getVINNumber());
            }

        }

    }


    private void getHistoryDisclosure(String vin) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String vehicleType = "1";
        ArrayList<HistoryListPojo> localLi = new ArrayList<>();
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
            }
            if (record.getVehicleDetails().getTypeOfVehicle() != null) {
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                    vehicleType = "1";
                } else {
                    vehicleType = "2";
                }
            }
        }
        RetrofitInitialization.getDs_services().getVehicleHistoryDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, vin, "0", vehicleType).enqueue(new Callback<VehicleHistoryDisclosure>() {
            @Override
            public void onResponse(Call<VehicleHistoryDisclosure> call, Response<VehicleHistoryDisclosure> response) {

                if (response.isSuccessful() && response.code() == 200) {

                    ArrayList<Value> list;
                    list = response.body().getValue();
                    VehicleHistoryDisclosure vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleHistoryDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleHistoryDisclosure(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistoryVerified(true);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
//                    Log.i("HistoryDisclosure: ", gson.toJson(vehicleConditionDisclosure));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);

//                    Log.e("Size of history: ", response.body().getValue().size() + "");
                }
                Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
                networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, true);
                LocalBroadcastManager.getInstance(DSAPP.getInstance()).sendBroadcast(networkStateIntent);
            }

            @Override
            public void onFailure(Call<VehicleHistoryDisclosure> call, Throwable t) {
                Timber.e(t.getLocalizedMessage());
                Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
                networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, true);
                LocalBroadcastManager.getInstance(DSAPP.getInstance()).sendBroadcast(networkStateIntent);
            }
        });
    }

    private void getConditionDisclosure(String vin) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(true);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        String vehicleType = "1";
        ArrayList<HistoryListPojo> localLi = new ArrayList<>();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
            }

            if (record.getVehicleDetails().getTypeOfVehicle() != null) {
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                    vehicleType = "1";
                } else {
                    vehicleType = "2";
                }
            }
        }

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, vin, "0", vehicleType).enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {

                    VehicleConditionDisclosure vehicleConditionDisclosure;
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleConditionDisclosure(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setWarningAlerts(response.body().getWarningAlerts());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionVerified(true);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(false);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Timber.i(gson.toJson(vehicleConditionDisclosure));
                    }
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE, dealershipDataString);
//                    Log.e("Size of common: ", response.body().getValue().size() + "");
                }
                Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
                networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, true);
                LocalBroadcastManager.getInstance(DSAPP.getInstance()).sendBroadcast(networkStateIntent);
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                Timber.e(t.getLocalizedMessage());
                Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
                networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, true);
                LocalBroadcastManager.getInstance(DSAPP.getInstance()).sendBroadcast(networkStateIntent);
            }
        });
    }

    private void getThirdPartyOffline(final String vinNumber) {
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    ThirdPartyList vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new ThirdPartyList();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setViewReport(false);
                            list.get(i).setMandatory(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Timber.i(gson.toJson(vehicleConditionDisclosure));
                        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
                        for (int i = 0; i < list.size(); i++) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(list.get(i).getId())) {
                                        File file = ShowImageFromPath(list.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (list.get(i).isActive()) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim());

                                                }
                                            }
                                            if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_MULTI_SIGN)) {
                                                getAllThirdPartyReportList(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim());
                                            }
                                        }
                                    }
                                }
                            }
                        }
//                        Log.e("Size of reports:NCR ", response.body().getValue().size() + "");
                    }
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {

            }
        });
    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(getActivity())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    private void getAllThirdPartyReportList(String thirdPartyReportRawId, String id, String name) {
        RetrofitInitialization.getDs_services().GetHistoryReportRawPdfBytesList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), thirdPartyReportRawId).enqueue(new Callback<ThirdPartyReportList>() {
            @Override
            public void onResponse(Call<ThirdPartyReportList> call, Response<ThirdPartyReportList> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage().size() != 0) {
                       ArrayList<String> reportList = new ArrayList<>(response.body().getMessage());
                        for (int i = 0; i < reportList.size(); i++) {
                            File dwldsPath = getOutputMediaFile(name+thirdPartyReportRawId, name.trim() + i);
                            byte[] pdfAsBytes = Base64.decode(reportList.get(i), 0);
                            FileOutputStream os;
                            try {
                                os = new FileOutputStream(dwldsPath, false);
                                os.write(pdfAsBytes);
                                os.flush();
                                os.close();
                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
//                        File dwldsPath = getOutputMediaFile(name, name.trim() + TPRRID);
//                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
//                        FileOutputStream os;
//                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
//                        PreferenceManger.putString(AppConstant.ALERT, "");
//                        if (tpId.equalsIgnoreCase("1") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
//                        } else if (tpId.equalsIgnoreCase("2") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
//                        }
//                        String recordDataString = gson.toJson(recordData);
//                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                        try {
//                            os = new FileOutputStream(dwldsPath, false);
//                            os.write(pdfAsBytes);
//                            os.flush();
//                            os.close();
////                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    }
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(id)) {
                                list.get(i).setThirdPartyList(response.body());
                            }
                        }

                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportList> call, Throwable t) {
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    private void getAllThirdPartyReports(final String TPRRID, final String tpId, final String name) {

//        "40334", "2g1fd1e34f9207229", "1", lng
        RetrofitInitialization.getDs_services().getThirdPartyReportsBytes("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), TPRRID).enqueue(new Callback<ThirdPartyReportBytes>() {
            @Override
            public void onResponse(Call<ThirdPartyReportBytes> call, Response<ThirdPartyReportBytes> response) {
                if (response.code() == 200 && response.isSuccessful()) {
//                    final File dwldsPath = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + "thirdParty" + ".pdf");
                    Log.e("Downloading report: ", name);
                    assert response.body() != null;
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage() != null && response.body().getMessage().length() > 0) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        File dwldsPath = getOutputMediaFile("localPdf", name.trim() + TPRRID);
                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
                        FileOutputStream os;
                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
                        PreferenceManger.putString(AppConstant.ALERT, "");
                        if (tpId.equalsIgnoreCase("1") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && response.body().getSucceeded().equalsIgnoreCase("true")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                        } else if (tpId.equalsIgnoreCase("2") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && response.body().getSucceeded().equalsIgnoreCase("true")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                        }
//                        if (tpId.equalsIgnoreCase("1")) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
//                        } else if (tpId.equalsIgnoreCase("2")) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
//                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        try {
                            os = new FileOutputStream(dwldsPath, false);
                            os.write(pdfAsBytes);
                            os.flush();
                            os.close();
                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {

//                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(tpId)) {
                                list.get(i).setThirdPartyReportBytes(response.body());
                            }
                        }
                        if (tpId.equalsIgnoreCase("1") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            alerts.setAlertName("CarFax Report is Not Available");
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getErrorMessage());
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                        } else if (tpId.equalsIgnoreCase("2") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getErrorMessage());
                            alerts.setAlertName("AutoCheck Report is Not Available");
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportBytes> call, Throwable t) {
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void getDealershipsSilently() {
        String time = PreferenceManger.getStringValue(AppConstant.TIME_STAMP);
        int min = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
            Date date = formatter.parse(time);
            long lastTime = date.getTime();
            long currentTime = System.currentTimeMillis();
            long diff = currentTime - lastTime;
            min = (int) ((currentTime - lastTime) / (1000 * 60));
        } catch (ParseException e) {
            Timber.e(e.getLocalizedMessage());
        }
        if (min >= 1 || time.length() == 0) {
            Call<DealershipData> get_dealerships = RetrofitInitialization.getDs_services().get_dealerships("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
            get_dealerships.enqueue(new Callback<DealershipData>() {
                @Override
                public void onResponse(Call<DealershipData> call, Response<DealershipData> response) {
                    if (response.code() == 200 && response.isSuccessful()) {
                        DealershipData dealershipData = response.body();
                        Gson gson = new GsonBuilder().create();
                        Log.i("Refreshed dealer: ", gson.toJson(dealershipData));
                        String dealershipDataString = gson.toJson(response.body());
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, dealershipDataString);
                        if (dealershipData != null) {
                            for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                                if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                                    PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(i).getDealershipName());
                                    PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(i).isAllowCheckID());
                                    PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(i).isCaptureCustomerPhotoOnSign());
                                    PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(i).isCaptureScreenOnSign());
                                    PreferenceManger.putBoolean(AppConstant.VIEW_MANDATORY, dealershipData.getDealerships().get(i).isViewThirdPartyReportMandatory());
                                    PreferenceManger.putBoolean(AppConstant.USE_THIRD_PARTY_API, dealershipData.getDealerships().get(i).isUseThirdPartyAPI());
                                    PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(i).isUsedCarOnly());
                                    PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(i).isProcessTradeIn());
                                    PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(i).isTakePhotoOfProofOfInsurance());
                                    PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(i).isTakePhotoOfVehicle());
                                    PreferenceManger.putBoolean(AppConstant.PROCESS_MULTI_SIGN, dealershipData.getDealerships().get(i).isThirdPartyMultiSign());
                                }
                            }
                        }

                    } else {

                        assert response.errorBody() != null;
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response.errorBody().string());
                            if (response.code() == 401 || response.code() == 403) {
                                if (json.getString("Message").contains("INACTIVE")) {
                                    String[] arr = json.getString("Message").split("\\|");
//                                new CustomToast(NewDealViewPager.this).toast(arr[1]);
                                    Log.e("Logout: ", "logout from NewDeal during inactive" + json.toString());
//                                PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
//                                PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
//                                Locale myLocale = new Locale("en");
//                                Resources res = getResources();
//                                DisplayMetrics dm = res.getDisplayMetrics();
//                                Configuration conf = res.getConfiguration();
//                                conf.locale = myLocale;
//                                res.updateConfiguration(conf, dm);
//                                callHomeActivity();
                                } else {
                                    if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                        callRefreshTokenAPI();
                                    } else {
                                        Log.e("Logout: ", "logout from NewDeal during zero length" + json.toString());
                                        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                        Locale myLocale = new Locale("en");
//                                    Resources res = getResources();
//                                    DisplayMetrics dm = res.getDisplayMetrics();
//                                    Configuration conf = res.getConfiguration();
//                                    conf.locale = myLocale;
//                                    res.updateConfiguration(conf, dm);
//                                    callHomeActivity();
                                    }
                                }

                            }
//                        showPopup(json.getString("error_description"));
                            Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<DealershipData> call, Throwable t) {
//                new CustomToast(DSAPP.getInstance()).alert(t.getLocalizedMessage());

                }
            });
        }
    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    Log.e("Refresh: ", loginData.getAccessToken());
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
//                    final ProgressDialog progressDialog = new ProgressDialog(DSAPP.getInstance());
//                    progressDialog.setMessage("Refreshing pending deals...");
//                    progressDialog.show();
//                    progressDialog.setCancelable(false);

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Logout: ", "logout from NewDeal during during refresh" + json.toString());
                        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                        Locale myLocale = new Locale("en");
//                        Resources res = getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        Configuration conf = res.getConfiguration();
//                        conf.locale = myLocale;
//                        res.updateConfiguration(conf, dm);
//                        callHomeActivity();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
//                if (DSAPP.getInstance() != null && !DSAPP.getInstance().isFinishing())
//                    new CustomToast(DSAPP.getInstance()).alert(getString(R.string.connection_check));
//                }
            }
        });
    }


    private void getSalesPersonList() {
//        final ProgressDialog progressDialog = new ProgressDialog(DSAPP.getInstance());
//        progressDialog.setMessage("Loading " + dealershipName.getDealershipName() + " configuration...");
//        progressDialog.show();
//        progressDialog.setCancelable(true);
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getSalesPerson("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<ArrayList<SalesPersonList>>() {
            @Override
            public void onResponse(Call<ArrayList<SalesPersonList>> call, Response<ArrayList<SalesPersonList>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    LocalSalesPerson localSalesPerson = new LocalSalesPerson();
                    localSalesPerson.setLocalSales(response.body());
                    String list = gson.toJson(localSalesPerson);
                    Log.i("Sales persons: ", list);
                    PreferenceManger.putString(AppConstant.SALES_PERSON, list);

                }
            }

            @Override
            public void onFailure(Call<ArrayList<SalesPersonList>> call, Throwable t) {

            }
        });
    }

    private void getCheckListItems(String id) {
        RetrofitInitialization.getDs_services().getCheckList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<CheckListPojo>() {
            @Override
            public void onResponse(Call<CheckListPojo> call, Response<CheckListPojo> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    Log.i("Check list: ", list);
                    PreferenceManger.putString(AppConstant.CHECK_LIST, list);
                } else {

                }
            }

            @Override
            public void onFailure(Call<CheckListPojo> call, Throwable t) {

            }
        });
    }

    private void getVerbalPromiseList(final String id) {
        RetrofitInitialization.getDs_services().getVerbalPromise("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<NoVerbalPromise>() {
            @Override
            public void onResponse(Call<NoVerbalPromise> call, Response<NoVerbalPromise> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    Log.i("no verbal promise: ", list);
                    PreferenceManger.putString(AppConstant.VERBAL_PROMISE, list);

                }
            }

            @Override
            public void onFailure(Call<NoVerbalPromise> call, Throwable t) {

            }
        });
    }
} 