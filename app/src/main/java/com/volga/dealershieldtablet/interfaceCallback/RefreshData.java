package com.volga.dealershieldtablet.interfaceCallback;

public interface RefreshData {
    void reload();
}
