package com.volga.dealershieldtablet.interfaceCallback;

/**
 * Created by GUFRAN on 20-01-2017.
 */

public interface GoBackNextInterface {
    void whatNextClick();
    void goToBackIndex();
}
