package com.volga.dealershieldtablet.interfaceCallback;

import com.manateeworks.BarcodeScanner;

import org.json.JSONException;

/**
 * Created by lazarilievski on 1/22/18.
 */

public interface MWBScannerListener {
      void onScannedResult(BarcodeScanner.MWResult result) throws JSONException;
}
