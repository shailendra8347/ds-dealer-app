package com.volga.dealershieldtablet.Retrofit.Pojo;

public class UploadedDocs
{
    private String CustomerVehicleTradeId;

    private String IsAdditionalImage;

    private String ImageForText;

    private String ImageForId;

    private String DocTypeName;

    private String CatupreDate;

    public String getCustomerVehicleTradeId ()
    {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId (String CustomerVehicleTradeId)
    {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public String getIsAdditionalImage ()
    {
        return IsAdditionalImage;
    }

    public void setIsAdditionalImage (String IsAdditionalImage)
    {
        this.IsAdditionalImage = IsAdditionalImage;
    }

    public String getImageForText ()
    {
        return ImageForText;
    }

    public void setImageForText (String ImageForText)
    {
        this.ImageForText = ImageForText;
    }

    public String getImageForId ()
    {
        return ImageForId;
    }

    public void setImageForId (String ImageForId)
    {
        this.ImageForId = ImageForId;
    }

    public String getDocTypeName ()
    {
        return DocTypeName;
    }

    public void setDocTypeName (String DocTypeName)
    {
        this.DocTypeName = DocTypeName;
    }

    public String getCatupreDate ()
    {
        return CatupreDate;
    }

    public void setCatupreDate (String CatupreDate)
    {
        this.CatupreDate = CatupreDate;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CustomerVehicleTradeId = "+CustomerVehicleTradeId+", IsAdditionalImage = "+IsAdditionalImage+", ImageForText = "+ImageForText+", ImageForId = "+ImageForId+", DocTypeName = "+DocTypeName+", CatupreDate = "+CatupreDate+"]";
    }
}
