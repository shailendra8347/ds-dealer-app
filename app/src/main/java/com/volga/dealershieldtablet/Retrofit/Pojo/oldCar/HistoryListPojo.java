package com.volga.dealershieldtablet.Retrofit.Pojo.oldCar;

import com.volga.dealershieldtablet.Retrofit.Pojo.ManualAdditionalDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 20-08-2018.
 */
public class HistoryListPojo implements Serializable {
    private String name;

    public ArrayList<WarningAlerts> getWarningAlerts() {
        return warningAlerts;
    }

    public void setWarningAlerts(ArrayList<WarningAlerts> warningAlerts) {
        this.warningAlerts = warningAlerts;
    }

    ArrayList<String> dependentIds;

    public ArrayList<String> getDependentIds() {
        return dependentIds;
    }

    public void setDependentIds(ArrayList<String> dependentIds) {
        this.dependentIds = dependentIds;
    }

    public ArrayList<ManualAdditionalDisclosures> getManualAdditionalDisclosures() {
        return manualAdditionalDisclosures;
    }

    public void setManualAdditionalDisclosures(ArrayList<ManualAdditionalDisclosures> manualAdditionalDisclosures) {
        this.manualAdditionalDisclosures = manualAdditionalDisclosures;
    }

    ArrayList<ManualAdditionalDisclosures> manualAdditionalDisclosures;

    ArrayList<WarningAlerts> warningAlerts;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String url;

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    private String additionalData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    private boolean checked,isAutoMarked;

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    private boolean isVerified;

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    private boolean isMandatory;

    public boolean isReportSeen() {
        return reportSeen;
    }

    public void setReportSeen(boolean reportSeen) {
        this.reportSeen = reportSeen;
    }

    private boolean reportSeen;

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    private boolean value;

    public boolean isViewReport() {
        return ViewReport;
    }

    public void setViewReport(boolean viewReport) {
        ViewReport = viewReport;
    }

    private boolean ViewReport;

    public boolean isAutoMarked() {
        return isAutoMarked;
    }

    public void setAutoMarked(boolean autoMarked) {
        isAutoMarked = autoMarked;
    }
}
