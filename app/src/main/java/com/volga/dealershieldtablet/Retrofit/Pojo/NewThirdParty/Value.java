package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

import com.volga.dealershieldtablet.Retrofit.Pojo.ManualAdditionalDisclosures;

import java.util.ArrayList;

public class Value {
    private VerifiedThirdParty[] VerifiedThirdParty;

    private String Title;

    private String Id;

    public boolean isManual() {
        return isManual;
    }

    public void setManual(boolean manual) {
        isManual = manual;
    }

    private boolean isManual;

    private ArrayList<String> DependentDislosureIds;

    public ArrayList<String> getDependentDislosureIds() {
        return DependentDislosureIds;
    }

    public void setDependentDislosureIds(ArrayList<String> dependentDislosureIds) {
        DependentDislosureIds = dependentDislosureIds;
    }

    public ArrayList<ManualAdditionalDisclosures> getManualAdditionalDisclosure() {
        return ManualAdditionalDisclosure;
    }

    public void setManualAdditionalDisclosure(ArrayList<ManualAdditionalDisclosures> manualAdditionalDisclosure) {
        ManualAdditionalDisclosure = manualAdditionalDisclosure;
    }

    private ArrayList<ManualAdditionalDisclosures> ManualAdditionalDisclosure;

    private boolean IsVerified;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts> getWarningAlerts() {
        return WarningAlerts;
    }

    public void setWarningAlerts(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts> warningAlerts) {
        WarningAlerts = warningAlerts;
    }

    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts> WarningAlerts;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> getTranslations() {
        return Translations;
    }

    public void setTranslations(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> translations) {
        Translations = translations;
    }

    private ArrayList<Translations> Translations;

    public VerifiedThirdParty[] getVerifiedThirdParty() {
        return VerifiedThirdParty;
    }

    public void setVerifiedThirdParty(VerifiedThirdParty[] VerifiedThirdParty) {
        this.VerifiedThirdParty = VerifiedThirdParty;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public boolean getIsVerified() {
        return IsVerified;
    }

    public void setIsVerified(boolean IsVerified) {
        this.IsVerified = IsVerified;
    }

    @Override
    public String toString() {
        return "ClassPojo [VerifiedThirdParty = " + VerifiedThirdParty + ", Title = " + Title + ", Id = " + Id + ", IsVerified = " + IsVerified + "]";
    }
}
			
			