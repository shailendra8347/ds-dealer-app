package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

public class Translations {
    String DestinationLanguageId;

    public String getDestinationLanguageId() {
        return DestinationLanguageId;
    }

    public void setDestinationLanguageId(String destinationLanguageId) {
        DestinationLanguageId = destinationLanguageId;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    String Text;
}
