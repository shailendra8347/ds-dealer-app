package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

public class SalesPersons
{
    private String Email;

    private String FirstName;

    private String Id;

    private String CustomerId;

    private String LastName;

    private String MiddleName;

    private String Mobile;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    private String UserId;

    public String getEmail ()
    {
        return Email;
    }

    public void setEmail (String Email)
    {
        this.Email = Email;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getCustomerId ()
    {
        return CustomerId;
    }

    public void setCustomerId (String CustomerId)
    {
        this.CustomerId = CustomerId;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getMiddleName ()
    {
        return MiddleName;
    }

    public void setMiddleName (String MiddleName)
    {
        this.MiddleName = MiddleName;
    }

    public String getMobile ()
    {
        return Mobile;
    }

    public void setMobile (String Mobile)
    {
        this.Mobile = Mobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Email = "+Email+", FirstName = "+FirstName+", Id = "+Id+", CustomerId = "+CustomerId+", LastName = "+LastName+", MiddleName = "+MiddleName+", Mobile = "+Mobile+"]";
    }
}
