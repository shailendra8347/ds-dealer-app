
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class License {

    @SerializedName("isIMG_DLFRONT_SYNCED")
    @Expose
    private boolean isIMGDLFRONTSYNCED;

    public boolean getIsIMGDLFRONTSYNCED() {
        return isIMGDLFRONTSYNCED;
    }

    public void setIsIMGDLFRONTSYNCED(boolean isIMGDLFRONTSYNCED) {
        this.isIMGDLFRONTSYNCED = isIMGDLFRONTSYNCED;
    }

}
