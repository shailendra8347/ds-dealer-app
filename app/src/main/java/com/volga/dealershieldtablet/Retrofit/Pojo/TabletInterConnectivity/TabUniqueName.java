package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity;

public class TabUniqueName{
	private String CompanyName;
	private int CompanyId;
	private String DeviceID;
	private String UserId;
	private int TabletUniquenessId;
	private String DeviceName;

	public void setCompanyName(String companyName){
		this.CompanyName = companyName;
	}

	public String getCompanyName(){
		return CompanyName;
	}

	public void setCompanyId(int companyId){
		this.CompanyId = companyId;
	}

	public int getCompanyId(){
		return CompanyId;
	}

	public void setDeviceID(String deviceID){
		this.DeviceID = deviceID;
	}

	public String getDeviceID(){
		return DeviceID;
	}

	public void setUserId(String userId){
		this.UserId = userId;
	}

	public String getUserId(){
		return UserId;
	}

	public void setTabletUniquenessId(int tabletUniquenessId){
		this.TabletUniquenessId = tabletUniquenessId;
	}

	public int getTabletUniquenessId(){
		return TabletUniquenessId;
	}

	public void setDeviceName(String deviceName){
		this.DeviceName = deviceName;
	}

	public String getDeviceName(){
		return DeviceName;
	}

	@Override
 	public String toString(){
		return 
			"TabUniqueName{" + 
			"CompanyName = '" + CompanyName + '\'' +
			",CompanyId = '" + CompanyId + '\'' +
			",DeviceID = '" + DeviceID + '\'' +
			",UserId = '" + UserId + '\'' +
			",TabletUniquenessId = '" + TabletUniquenessId + '\'' +
			",DeviceName = '" + DeviceName + '\'' +
			"}";
		}
}
