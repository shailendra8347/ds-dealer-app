package com.volga.dealershieldtablet.Retrofit.Pojo.Vin;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Root;

@Root(name = "Vehicle", strict = false)
public class Vehicle
{
    @Element(name = "VINquery_Vehicle_ID")
    private String VINquery_Vehicle_ID;
    @Element(name = "Model_Year")
    private String Model_Year;
    @Element(name = "Model")
    private String Model;
    @ElementArray(name = "Item")
    private Item[] Item;
    @Element(name = "Trim_Level")
    private String Trim_Level;
    @Element(name = "Make")
    private String Make;
    public String getVINquery_Vehicle_ID ()
    {
        return VINquery_Vehicle_ID;
    }

    public void setVINquery_Vehicle_ID (String VINquery_Vehicle_ID)
    {
        this.VINquery_Vehicle_ID = VINquery_Vehicle_ID;
    }

    public String getModel_Year ()
    {
        return Model_Year;
    }

    public void setModel_Year (String Model_Year)
    {
        this.Model_Year = Model_Year;
    }

    public String getModel ()
    {
        return Model;
    }

    public void setModel (String Model)
    {
        this.Model = Model;
    }

    public Item[] getItem ()
    {
        return Item;
    }

    public void setItem (Item[] Item)
    {
        this.Item = Item;
    }

    public String getTrim_Level ()
    {
        return Trim_Level;
    }

    public void setTrim_Level (String Trim_Level)
    {
        this.Trim_Level = Trim_Level;
    }

    public String getMake ()
    {
        return Make;
    }

    public void setMake (String Make)
    {
        this.Make = Make;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [VINquery_Vehicle_ID = "+VINquery_Vehicle_ID+", Model_Year = "+Model_Year+", Model = "+Model+", Item = "+Item+", Trim_Level = "+Trim_Level+", Make = "+Make+"]";
    }
}