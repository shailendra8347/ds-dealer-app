package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity;

import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosureToUpload;
import com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListUpload;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromiseUpload;

import java.util.ArrayList;

public class IncompleteDealPojo {
    private String CustomerId;
    private String FirstName;
    private String MiddleName;
    private String LastName;
    private int GenderTypeId;
    private String DateOfBirth;
    private String LanguageId;
    private String DrivingLicenseNumber;
    private String DrivingLicenseExpiryDate;
    private String Email;
    private String ContactNumber;
    private String Address1;
    private String Address2;
    private String City;
    private String Zipcode;
    private String State;
    private String Country;
    private int CustomerVehicleTradeId;
    private String CarTypeId;
    private String TradeTypeId;
    private String VIN;
    private String Make;
    private String Model;
    private int ModelYear;
    private String TestDriveDurationId;
    private boolean IsTestDriveTaken;
    private boolean Allow2RemoveWindowSticker;
    private String Mileage;
    private String TradeDateTime;
    private String stockNumber,TradeCompletionUTC;
    private boolean IsAcceptedUsedCarPolicy;
    private int DealershipId;
    private boolean IsTermConditionAccepted;
    private boolean IsLead;

    public ArrayList<AdditionalDisclosureToUpload> getAdditinalDisclosures() {
        return AdditinalDisclosures;
    }

    public void setAdditinalDisclosures(ArrayList<AdditionalDisclosureToUpload> additinalDisclosures) {
        AdditinalDisclosures = additinalDisclosures;
    }

    private ArrayList<AdditionalDisclosureToUpload> AdditinalDisclosures;
    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs> getThirdPartyReportDocs() {
        return ThirdPartyReportDocs;
    }

    public void setThirdPartyReportDocs(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs> thirdPartyReportDocs) {
        ThirdPartyReportDocs = thirdPartyReportDocs;
    }

    private ArrayList<ThirdPartyReportDocs> ThirdPartyReportDocs;

    public ArrayList<InitThirdParty> getThirdPartyReportRaws() {
        return ThirdPartyReportRaws;
    }

    public void setThirdPartyReportRaws(ArrayList<InitThirdParty> thirdPartyReportRaws) {
        ThirdPartyReportRaws = thirdPartyReportRaws;
    }

    ArrayList<InitThirdParty> ThirdPartyReportRaws;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts> getCustomerVehicleDealAlerts() {
        return CustomerVehicleDealAlerts;
    }

    public void setCustomerVehicleDealAlerts(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts> customerVehicleDealAlerts) {
        CustomerVehicleDealAlerts = customerVehicleDealAlerts;
    }

    private ArrayList<CustomerVehicleDealAlerts> CustomerVehicleDealAlerts;

    public boolean isDisclosureSkipped() {
        return IsDisclosureSkipped;
    }

    public void setDisclosureSkipped(boolean disclosureSkipped) {
        IsDisclosureSkipped = disclosureSkipped;
    }

    private boolean IsDisclosureSkipped;

    public boolean getIsRemote() {
        return IsRemote;
    }

    public void setIsRemote(boolean isRemote) {
        IsRemote = isRemote;
    }

    private boolean IsRemote;

    private String LastScreenName;
    private ArrayList<ThirdPartyHistoryReport> ThirdPartyHistoryReport = new ArrayList<>();
    private ArrayList<HistoryDisclosures> HistoryDisclosures = new ArrayList<>();
    private ArrayList<ConditionDisclosures> ConditionDisclosures = new ArrayList<>();
    private CoBuyerDetail CoBuyerDetail;
    private TradeInVehicle TradeInVehicle;
    private ArrayList<VerbalPromiseUpload> VerbalPromises;
    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> getCustomerAgreement() {
        return CustomerAgreement;
    }

    public void setCustomerAgreement(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> customerAgreement) {
        CustomerAgreement = customerAgreement;
    }

    ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> CustomerAgreement;


    public ArrayList<VerbalPromiseUpload> getVerbalPromises() {
        return VerbalPromises;
    }

    public void setVerbalPromises(ArrayList<VerbalPromiseUpload> verbalPromises) {
        VerbalPromises = verbalPromises;
    }

    public ArrayList<CheckListUpload> getCheckLists() {
        return CheckLists;
    }

    public void setCheckLists(ArrayList<CheckListUpload> checkLists) {
        CheckLists = checkLists;
    }

    private ArrayList<CheckListUpload> CheckLists;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport> getThirdPartyHistoryReport() {
        return ThirdPartyHistoryReport;
    }

    public void setThirdPartyHistoryReport(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport> thirdPartyHistoryReport) {
        ThirdPartyHistoryReport = thirdPartyHistoryReport;
    }

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures> getHistoryDisclosures() {
        return HistoryDisclosures;
    }

    public void setHistoryDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures> historyDisclosures) {
        HistoryDisclosures = historyDisclosures;
    }

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures> getConditionDisclosures() {
        return ConditionDisclosures;
    }

    public void setConditionDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures> conditionDisclosures) {
        ConditionDisclosures = conditionDisclosures;
    }

    public ArrayList< SalesPersons> getSalesPersons() {
        return SalesPersons;
    }

    public void setSalesPersons(ArrayList< SalesPersons> salesPersons) {
        SalesPersons = salesPersons;
    }

    private ArrayList< SalesPersons> SalesPersons = new ArrayList<>();
    private int TabletUniquenessId;


    // Getter Methods

    public String getCustomerId() {
        return CustomerId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public String getLastName() {
        return LastName;
    }

    public int getGenderTypeId() {
        return GenderTypeId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public String getLanguageId() {
        return LanguageId;
    }

    public String getDrivingLicenseNumber() {
        return DrivingLicenseNumber;
    }

    public String getDrivingLicenseExpiryDate() {
        return DrivingLicenseExpiryDate;
    }

    public String getEmail() {
        return Email;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public String getAddress1() {
        return Address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public String getCity() {
        return City;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public String getState() {
        return State;
    }

    public String getCountry() {
        return Country;
    }

    public int getCustomerVehicleTradeId() {
        return CustomerVehicleTradeId;
    }

    public String getCarTypeId() {
        return CarTypeId;
    }

    public String getTradeTypeId() {
        return TradeTypeId;
    }

    public String getVIN() {
        return VIN;
    }

    public String getMake() {
        return Make;
    }

    public String getModel() {
        return Model;
    }

    public int getModelYear() {
        return ModelYear;
    }

    public String getTestDriveDurationId() {
        return TestDriveDurationId;
    }

    public boolean getIsTestDriveTaken() {
        return IsTestDriveTaken;
    }

    public boolean getAllow2RemoveWindowSticker() {
        return Allow2RemoveWindowSticker;
    }

    public String getMileage() {
        return Mileage;
    }

    public String getTradeDateTime() {
        return TradeDateTime;
    }

    public String getStockNumber() {
        return stockNumber;
    }

    public boolean getIsAcceptedUsedCarPolicy() {
        return IsAcceptedUsedCarPolicy;
    }

    public int getDealershipId() {
        return DealershipId;
    }

    public boolean getIsTermConditionAccepted() {
        return IsTermConditionAccepted;
    }

    public boolean getIsLead() {
        return IsLead;
    }

    public String getLastScreenName() {
        return LastScreenName;
    }

    public CoBuyerDetail getCoBuyerDetail() {
        return CoBuyerDetail;
    }

    public TradeInVehicle getTradeInVehicle() {
        return TradeInVehicle;
    }

    public int getTabletUniquenessId() {
        return TabletUniquenessId;
    }

    // Setter Methods

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public void setGenderTypeId(int GenderTypeId) {
        this.GenderTypeId = GenderTypeId;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public void setLanguageId(String LanguageId) {
        this.LanguageId = LanguageId;
    }

    public void setDrivingLicenseNumber(String DrivingLicenseNumber) {
        this.DrivingLicenseNumber = DrivingLicenseNumber;
    }

    public void setDrivingLicenseExpiryDate(String DrivingLicenseExpiryDate) {
        this.DrivingLicenseExpiryDate = DrivingLicenseExpiryDate;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public void setAddress1(String Address1) {
        this.Address1 = Address1;
    }

    public void setAddress2(String Address2) {
        this.Address2 = Address2;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public void setZipcode(String Zipcode) {
        this.Zipcode = Zipcode;
    }

    public void setState(String State) {
        this.State = State;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public void setCustomerVehicleTradeId(int CustomerVehicleTradeId) {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public void setCarTypeId(String CarTypeId) {
        this.CarTypeId = CarTypeId;
    }

    public void setTradeTypeId(String TradeTypeId) {
        this.TradeTypeId = TradeTypeId;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public void setModelYear(int ModelYear) {
        this.ModelYear = ModelYear;
    }

    public void setTestDriveDurationId(String TestDriveDurationId) {
        this.TestDriveDurationId = TestDriveDurationId;
    }

    public void setIsTestDriveTaken(boolean IsTestDriveTaken) {
        this.IsTestDriveTaken = IsTestDriveTaken;
    }

    public void setAllow2RemoveWindowSticker(boolean Allow2RemoveWindowSticker) {
        this.Allow2RemoveWindowSticker = Allow2RemoveWindowSticker;
    }

    public void setMileage(String Mileage) {
        this.Mileage = Mileage;
    }

    public void setTradeDateTime(String TradeDateTime) {
        this.TradeDateTime = TradeDateTime;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    public void setIsAcceptedUsedCarPolicy(boolean IsAcceptedUsedCarPolicy) {
        this.IsAcceptedUsedCarPolicy = IsAcceptedUsedCarPolicy;
    }

    public void setDealershipId(int DealershipId) {
        this.DealershipId = DealershipId;
    }

    public void setIsTermConditionAccepted(boolean IsTermConditionAccepted) {
        this.IsTermConditionAccepted = IsTermConditionAccepted;
    }

    public void setIsLead(boolean IsLead) {
        this.IsLead = IsLead;
    }

    public void setLastScreenName(String LastScreenName) {
        this.LastScreenName = LastScreenName;
    }

    public void setCoBuyerDetail(CoBuyerDetail CoBuyerDetailObject) {
        this.CoBuyerDetail = CoBuyerDetailObject;
    }

    public void setTradeInVehicle(TradeInVehicle TradeInVehicleObject) {
        this.TradeInVehicle = TradeInVehicleObject;
    }

    public void setTabletUniquenessId(int TabletUniquenessId) {
        this.TabletUniquenessId = TabletUniquenessId;
    }

    public String getTradeCompletionUTC() {
        return TradeCompletionUTC;
    }

    public void setTradeCompletionUTC(String tradeCompletionUTC) {
        TradeCompletionUTC = tradeCompletionUTC;
    }
}

