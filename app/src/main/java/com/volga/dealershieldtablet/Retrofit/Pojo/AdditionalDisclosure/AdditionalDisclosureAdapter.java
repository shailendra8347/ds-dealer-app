package com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.utils.AppConstant;

import java.util.ArrayList;

public class AdditionalDisclosureAdapter extends RecyclerView.Adapter<AdditionalDisclosureAdapter.TypeRow> {

    private final Activity activity;
    private ArrayList<HistoryListPojo> list;
    private boolean isHistory;

    public interface OnAdditionalSelect {
        void languageSelected(boolean allSelected, String id);
    }

    OnAdditionalSelect onAdditionalSelect;

    public AdditionalDisclosureAdapter(Activity activity, OnAdditionalSelect onAdditionalSelect, ArrayList<HistoryListPojo> list, boolean isHistory) {
        this.activity = activity;
        this.list = list;
        this.onAdditionalSelect = onAdditionalSelect;
        this.isHistory = isHistory;
    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.additional_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        if (list.get(position).getName() != null) {
            holder.name.setText(Html.fromHtml(list.get(position).getName()));
        } else {
            holder.name.setText(" ");
        }
        holder.checkBox.setOnCheckedChangeListener(null);
//        holder.checkBox.setChecked(list.get(position).isChecked());
        if (list.get(position).isChecked()) {
            holder.checkBox.setChecked(true);
            holder.name.setTextColor(activity.getResources().getColor(R.color.button_color_blue));
        }else {
            holder.checkBox.setChecked(false);
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                checkChangeLocal(position, b, false,holder);

            }
        });
        holder.checkBox.setVisibility(View.GONE);
        holder.bullet.setVisibility(View.VISIBLE);
//        holder.name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.checkBox.isChecked()) {
//                    holder.checkBox.setChecked(false);
//                    checkChangeLocal(position, false, false, holder);
//                } else {
//                    holder.checkBox.setChecked(true);
//                    checkChangeLocal(position, true, false, holder);
//                }
//            }
//        });

    }

    private void checkChangeLocal(int position, boolean b, boolean islogo, TypeRow holder) {
        if (b)
            holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        else{
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        }
        boolean isAllSelected = false;
        if (isHistory) {
            AppConstant.additionalHistory.get(position).setChecked(b);
            for (int i = 0; i < AppConstant.additionalHistory.size(); i++) {
                if (!AppConstant.additionalHistory.get(i).isChecked()) {
                    isAllSelected = AppConstant.additionalHistory.get(i).isChecked();
                    break;
                }
                isAllSelected = AppConstant.additionalHistory.get(i).isChecked();
            }
        }else {
            AppConstant.additional.get(position).setChecked(b);
            for (int i = 0; i < AppConstant.additional.size(); i++) {
                if (!AppConstant.additional.get(i).isChecked()) {
                    isAllSelected = AppConstant.additional.get(i).isChecked();
                    break;
                }
                isAllSelected = AppConstant.additional.get(i).isChecked();
            }
        }
        onAdditionalSelect.languageSelected(isAllSelected, list.get(position).getId());
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name, view, mandatory,bullet;
        CheckBox checkBox;
        ImageView logo;
        LinearLayout logoView;

        TypeRow(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            name = itemView.findViewById(R.id.name);
            bullet = itemView.findViewById(R.id.bulletPoint);
            view = itemView.findViewById(R.id.view);
            mandatory = itemView.findViewById(R.id.mandatory);
            logo = itemView.findViewById(R.id.logo);
            logoView = itemView.findViewById(R.id.logoBox);
        }
    }

}