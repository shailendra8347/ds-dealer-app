package com.volga.dealershieldtablet.Retrofit.Pojo.Vin;

public class VinData
{
    private VINquery VINquery;

    public VINquery getVINquery ()
    {
        return VINquery;
    }

    public void setVINquery (VINquery VINquery)
    {
        this.VINquery = VINquery;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [VINquery = "+VINquery+"]";
    }
}
	