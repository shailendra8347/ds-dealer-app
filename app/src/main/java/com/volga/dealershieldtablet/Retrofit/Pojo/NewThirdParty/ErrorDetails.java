package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

import androidx.annotation.NonNull;

public class ErrorDetails
{
    private String ErrorDescription;

    private String Error;

    public String getErrorDescription ()
    {
        return ErrorDescription;
    }

    public void setErrorDescription (String ErrorDescription)
    {
        this.ErrorDescription = ErrorDescription;
    }

    public String getError ()
    {
        return Error;
    }

    public void setError (String Error)
    {
        this.Error = Error;
    }

    @NonNull
    @Override
    public String toString()
    {
        return "ClassPojo [ErrorDescription = "+ErrorDescription+", Error = "+Error+"]";
    }
}