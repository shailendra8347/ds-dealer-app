
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RecordData implements Serializable {

    @SerializedName("Records")
    @Expose
    private List<Record> records = new ArrayList<>();

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public Record where(String dmvNumber) {
        Record record = new Record();
        for (int i = 0; i < this.getRecords().size(); i++) {
            if (this.getRecords().get(i).getCustomerDetails().getDLNumber()!=null&&this.getRecords().get(i).getCustomerDetails().getDLNumber().equalsIgnoreCase(dmvNumber)) {
                record = this.getRecords().get(i);
            }
        }
        return record;
    }
    public Record whereVTID(String tradeId) {
        Record record = new Record();
        for (int i = 0; i < this.getRecords().size(); i++) {
            if (this.getRecords().get(i).getSettingsAndPermissions().getCustomerVehicleTradeId()!=null)
            if (this.getRecords().get(i).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase(tradeId)) {
                record = this.getRecords().get(i);
            }
        }
        return record;
    }

    public int getIndexNumber(String dmvNumber) {
        for (int i = 0; i < this.getRecords().size(); i++) {
            if (this.getRecords().get(i).getCustomerDetails().getDLNumber() != null && this.getRecords().get(i).getCustomerDetails().getDLNumber().equalsIgnoreCase(dmvNumber)) {
                return i;
            }
        }
        return 0;
    }

}
