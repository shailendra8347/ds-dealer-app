package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

public class VehicleTradeHistoryDisclosures
{
    private String CustomerVehicleTradeId;

    private String VehicleHistoryDisclosureId;

    private String Id;

    private String Date;
    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    boolean IsVerified;

    public String getCustomerVehicleTradeId ()
    {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId (String CustomerVehicleTradeId)
    {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public String getVehicleHistoryDisclosureId ()
    {
        return VehicleHistoryDisclosureId;
    }

    public void setVehicleHistoryDisclosureId (String VehicleHistoryDisclosureId)
    {
        this.VehicleHistoryDisclosureId = VehicleHistoryDisclosureId;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getDate ()
    {
        return Date;
    }

    public void setDate (String Date)
    {
        this.Date = Date;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CustomerVehicleTradeId = "+CustomerVehicleTradeId+", VehicleHistoryDisclosureId = "+VehicleHistoryDisclosureId+", Id = "+Id+", Date = "+Date+"]";
    }
}