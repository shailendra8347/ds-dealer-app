package com.volga.dealershieldtablet.Retrofit.Pojo;

import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations;

import java.util.ArrayList;

public class ManualAdditionalDisclosures {
    String Id;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> getTranslations() {
        return Translations;
    }

    public void setTranslations(ArrayList<Translations> translations) {
        Translations = translations;
    }

    String Title;
    ArrayList<Translations> Translations;
}
