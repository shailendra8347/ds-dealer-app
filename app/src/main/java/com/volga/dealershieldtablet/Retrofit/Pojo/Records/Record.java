
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Record implements Serializable {

    public Record() {
    }

    @SerializedName("DealershipDetails")
    @Expose
    private DealershipDetails dealershipDetails;
    @SerializedName("CustomerDetails")
    @Expose
    private CustomerDetails customerDetails;

    public CoBuyerCustomerDetails getCoBuyerCustomerDetails() {
        return coBuyerCustomerDetails;
    }

    public void setCoBuyerCustomerDetails(CoBuyerCustomerDetails coBuyerCustomerDetails) {
        this.coBuyerCustomerDetails = coBuyerCustomerDetails;
    }

    private int DealStatusId;
    private boolean IsBuyerSigned;
    private boolean IsCoBuyerSigned;

    public boolean isBuyerSigned() {
        return IsBuyerSigned;
    }

    public void setBuyerSigned(boolean buyerSigned) {
        IsBuyerSigned = buyerSigned;
    }

    public boolean isCoBuyerSigned() {
        return IsCoBuyerSigned;
    }

    public void setCoBuyerSigned(boolean coBuyerSigned) {
        IsCoBuyerSigned = coBuyerSigned;
    }

    public boolean isCoBuyer() {
        return IsCoBuyer;
    }

    public void setCoBuyer(boolean coBuyer) {
        IsCoBuyer = coBuyer;
    }

    private boolean IsCoBuyer;
    public int getDealStatusId() {
        return DealStatusId;
    }

    public void setDealStatusId(int dealStatusId) {
        DealStatusId = dealStatusId;
    }

    public boolean isWebDeal() {
        return IsWebDeal;
    }

    public void setWebDeal(boolean webDeal) {
        IsWebDeal = webDeal;
    }

    public String getDealStatusText() {
        return DealStatusText;
    }

    public void setDealStatusText(String dealStatusText) {
        DealStatusText = dealStatusText;
    }

    private boolean IsWebDeal;
    private String DealStatusText;
    @SerializedName("CoBuyerCustomerDetails")
    @Expose
    private CoBuyerCustomerDetails coBuyerCustomerDetails;
    @SerializedName("VehicleDetails")
    @Expose
    private VehicleDetails vehicleDetails;


    public com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle getTradeInVehicle() {
        return TradeInVehicle;
    }

    public void setTradeInVehicle(com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle tradeInVehicle) {
        TradeInVehicle = tradeInVehicle;
    }

    @SerializedName("TradeInVehicle")
    @Expose
    private TradeInVehicle TradeInVehicle;


    @SerializedName("SettingsAndPermissions")
    @Expose
    private SettingsAndPermissions settingsAndPermissions;

    public SyncUp getSyncUp() {
        return syncUp;
    }

    public void setSyncUp(SyncUp syncUp) {
        this.syncUp = syncUp;
    }

    private SyncUp syncUp;

    public DealershipDetails getDealershipDetails() {
        return dealershipDetails;
    }

    public void setDealershipDetails(DealershipDetails dealershipDetails) {
        this.dealershipDetails = dealershipDetails;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public VehicleDetails getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(VehicleDetails vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public SettingsAndPermissions getSettingsAndPermissions() {
        return settingsAndPermissions;
    }

    public void setSettingsAndPermissions(SettingsAndPermissions settingsAndPermissions) {
        this.settingsAndPermissions = settingsAndPermissions;
    }

}
