package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

import java.util.ArrayList;

public class ThirdPartyList {
    private String Succeeded;

    private String[] Errors;

    private String IsDuplicate;

    private String DuePayment;

    private String IsAlreadyLeadMarked;

    private ArrayList<ThirdPartyObj> Value;

    private String ErrorsString;

    private String IsUpdated;

    private ErrorDetails ErrorDetails;

    public String getSucceeded ()
    {
        return Succeeded;
    }

    public void setSucceeded (String Succeeded)
    {
        this.Succeeded = Succeeded;
    }

    public String[] getErrors ()
    {
        return Errors;
    }

    public void setErrors (String[] Errors)
    {
        this.Errors = Errors;
    }

    public String getIsDuplicate ()
    {
        return IsDuplicate;
    }

    public void setIsDuplicate (String IsDuplicate)
    {
        this.IsDuplicate = IsDuplicate;
    }

    public String getDuePayment ()
    {
        return DuePayment;
    }

    public void setDuePayment (String DuePayment)
    {
        this.DuePayment = DuePayment;
    }

    public String getIsAlreadyLeadMarked ()
    {
        return IsAlreadyLeadMarked;
    }

    public void setIsAlreadyLeadMarked (String IsAlreadyLeadMarked)
    {
        this.IsAlreadyLeadMarked = IsAlreadyLeadMarked;
    }

    public ArrayList<ThirdPartyObj> getValue ()
    {
        return Value;
    }

    public void setValue (ArrayList<ThirdPartyObj> Value)
    {
        this.Value = Value;
    }

    public String getErrorsString ()
    {
        return ErrorsString;
    }

    public void setErrorsString (String ErrorsString)
    {
        this.ErrorsString = ErrorsString;
    }

    public String getIsUpdated ()
    {
        return IsUpdated;
    }

    public void setIsUpdated (String IsUpdated)
    {
        this.IsUpdated = IsUpdated;
    }

    public ErrorDetails getErrorDetails ()
    {
        return ErrorDetails;
    }

    public void setErrorDetails (ErrorDetails ErrorDetails)
    {
        this.ErrorDetails = ErrorDetails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Succeeded = "+Succeeded+", Errors = "+Errors+", IsDuplicate = "+IsDuplicate+", DuePayment = "+DuePayment+", IsAlreadyLeadMarked = "+IsAlreadyLeadMarked+", Value = "+Value+", ErrorsString = "+ErrorsString+", IsUpdated = "+IsUpdated+", ErrorDetails = "+ErrorDetails+"]";
    }

}
