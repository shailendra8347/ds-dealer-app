
package com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("LocationName")
    @Expose
    private Object locationName;
    @SerializedName("Lat")
    @Expose
    private Integer lat;
    @SerializedName("Long")
    @Expose
    private Integer _long;
    @SerializedName("StateProvince")
    @Expose
    private StateProvince stateProvince;
    @SerializedName("StateProvinceId")
    @Expose
    private Integer stateProvinceId;

    public Object getLocationName() {
        return locationName;
    }

    public void setLocationName(Object locationName) {
        this.locationName = locationName;
    }

    public Integer getLat() {
        return lat;
    }

    public void setLat(Integer lat) {
        this.lat = lat;
    }

    public Integer getLong() {
        return _long;
    }

    public void setLong(Integer _long) {
        this._long = _long;
    }

    public StateProvince getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(StateProvince stateProvince) {
        this.stateProvince = stateProvince;
    }

    public Integer getStateProvinceId() {
        return stateProvinceId;
    }

    public void setStateProvinceId(Integer stateProvinceId) {
        this.stateProvinceId = stateProvinceId;
    }

}
