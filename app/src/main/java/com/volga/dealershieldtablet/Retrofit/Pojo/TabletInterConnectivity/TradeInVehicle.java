package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity;

public class TradeInVehicle {
  private int Id;
  private String VIN;
  private String Make;
  private String Model;
  private int ModelYear;
  private String ExteriorColor;
  private String InteriorColor;
  private String Mileage;


 // Getter Methods 

  public int getId() {
    return Id;
  }

  public String getVIN() {
    return VIN;
  }

  public String getMake() {
    return Make;
  }

  public String getModel() {
    return Model;
  }

  public int getModelYear() {
    return ModelYear;
  }

  public String getExteriorColor() {
    return ExteriorColor;
  }

  public String getInteriorColor() {
    return InteriorColor;
  }

  public String getMileage() {
    return Mileage;
  }

 // Setter Methods 

  public void setId( int Id ) {
    this.Id = Id;
  }

  public void setVIN( String VIN ) {
    this.VIN = VIN;
  }

  public void setMake( String Make ) {
    this.Make = Make;
  }

  public void setModel( String Model ) {
    this.Model = Model;
  }

  public void setModelYear( int ModelYear ) {
    this.ModelYear = ModelYear;
  }

  public void setExteriorColor( String ExteriorColor ) {
    this.ExteriorColor = ExteriorColor;
  }

  public void setInteriorColor( String InteriorColor ) {
    this.InteriorColor = InteriorColor;
  }

  public void setMileage( String Mileage ) {
    this.Mileage = Mileage;
  }
}