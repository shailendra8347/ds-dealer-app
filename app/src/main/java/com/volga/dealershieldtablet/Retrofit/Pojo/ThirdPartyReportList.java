package com.volga.dealershieldtablet.Retrofit.Pojo;

import java.util.ArrayList;

public class ThirdPartyReportList {
    private String Succeeded;

    private ArrayList<String> Value;

    private String IsExist;

    private String IsDuplicate;

    private String IsError;

    private String RedirectUrl;

    private String ErrorsString;

    private String HasMessage;

    public String getSucceeded() {
        return Succeeded;
    }

    public void setSucceeded(String Succeeded) {
        this.Succeeded = Succeeded;
    }

    public ArrayList<String> getMessage() {
        return Value;
    }

    public void setMessage(ArrayList<String> Value) {
        this.Value = Value;
    }

    public String getIsExist() {
        return IsExist;
    }

    public void setIsExist(String IsExist) {
        this.IsExist = IsExist;
    }

    public String getIsDuplicate() {
        return IsDuplicate;
    }

    public void setIsDuplicate(String IsDuplicate) {
        this.IsDuplicate = IsDuplicate;
    }

    public String getIsError() {
        return IsError;
    }

    public void setIsError(String IsError) {
        this.IsError = IsError;
    }

    public String getRedirectUrl() {
        return RedirectUrl;
    }

    public void setRedirectUrl(String RedirectUrl) {
        this.RedirectUrl = RedirectUrl;
    }

    public String getErrorMessage() {
        return ErrorsString;
    }

    public void setErrorMessage(String ErrorMessage) {
        this.ErrorsString = ErrorMessage;
    }

    public String getHasMessage() {
        return HasMessage;
    }

    public void setHasMessage(String HasMessage) {
        this.HasMessage = HasMessage;
    }

    @Override
    public String toString() {
        return "ClassPojo [Succeeded = " + Succeeded + ", Message = " + Value + ", IsExist = " + IsExist + ", IsDuplicate = " + IsDuplicate + ", IsError = " + IsError + ", RedirectUrl = " + RedirectUrl + ", ErrorMessage = " + ErrorsString + ", HasMessage = " + HasMessage + "]";
    }
}
