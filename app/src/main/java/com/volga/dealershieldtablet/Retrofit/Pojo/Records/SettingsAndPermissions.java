
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListUpload;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromiseUpload;

import java.util.ArrayList;

public class SettingsAndPermissions {

    @SerializedName("SelectedLanguage")
    @Expose
    private String selectedLanguage;

    public ArrayList<CheckListUpload> getCheckLists() {
        return CheckLists;
    }

    public void setCheckLists(ArrayList<CheckListUpload> checkLists) {
        CheckLists = checkLists;
    }

    public ArrayList<VerbalPromiseUpload> getVerbalPromises() {
        return VerbalPromises;
    }

    public void setVerbalPromises(ArrayList<VerbalPromiseUpload> verbalPromises) {
        VerbalPromises = verbalPromises;
    }

    ArrayList<CheckListUpload> CheckLists;

    public boolean isWasInternetAvailable() {
        return wasInternetAvailable;
    }

    public void setWasInternetAvailable(boolean wasInternetAvailable) {
        this.wasInternetAvailable = wasInternetAvailable;
    }

    boolean wasInternetAvailable;
    ArrayList<VerbalPromiseUpload> VerbalPromises;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> getCustomerVehicleTradeDocs() {
        return CustomerVehicleTradeDocs;
    }

    public void setCustomerVehicleTradeDocs(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> customerVehicleTradeDocs) {
        CustomerVehicleTradeDocs = customerVehicleTradeDocs;
    }

    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> CustomerVehicleTradeDocs;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> getCustomerVehicleTradeDocsAll() {
        return CustomerVehicleTradeDocsAll;
    }

    public void setCustomerVehicleTradeDocsAll(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> customerVehicleTradeDocsAll) {
        CustomerVehicleTradeDocsAll = customerVehicleTradeDocsAll;
    }

    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> CustomerVehicleTradeDocsAll;

    public String getCoBuyerSelectedLanguage() {
        return coBuyerSelectedLanguage;
    }

    public void setCoBuyerSelectedLanguage(String coBuyerSelectedLanguage) {
        this.coBuyerSelectedLanguage = coBuyerSelectedLanguage;
    }

    @SerializedName("coBuyerSelectedLanguage")
    @Expose
    private String coBuyerSelectedLanguage;
    @SerializedName("SelectedLanguageId")
    @Expose
    private int selectedLanguageId;

    private boolean hideMileage=true;

    public long getCurrentMillis() {
        return currentMillis;
    }

    public void setCurrentMillis(long currentMillis) {
        this.currentMillis = currentMillis;
    }

    private long currentMillis;

    public int getCoBuyerSelectedLanguageId() {
        return coBuyerSelectedLanguageId;
    }

    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeHistoryDisclosures> VehicleTradeHistoryDisclosures;
    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeThirdPartyHistoryReports> VehicleTradeThirdPartyHistoryReports;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeHistoryDisclosures> getVehicleTradeHistoryDisclosures() {
        return VehicleTradeHistoryDisclosures;
    }

    public void setVehicleTradeHistoryDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeHistoryDisclosures> vehicleTradeHistoryDisclosures) {
        VehicleTradeHistoryDisclosures = vehicleTradeHistoryDisclosures;
    }

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeThirdPartyHistoryReports> getVehicleTradeThirdPartyHistoryReports() {
        return VehicleTradeThirdPartyHistoryReports;
    }

    public void setVehicleTradeThirdPartyHistoryReports(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeThirdPartyHistoryReports> vehicleTradeThirdPartyHistoryReports) {
        VehicleTradeThirdPartyHistoryReports = vehicleTradeThirdPartyHistoryReports;
    }

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeConditionDisclosures> getVehicleTradeConditionDisclosures() {
        return VehicleTradeConditionDisclosures;
    }

    public void setVehicleTradeConditionDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeConditionDisclosures> vehicleTradeConditionDisclosures) {
        VehicleTradeConditionDisclosures = vehicleTradeConditionDisclosures;
    }

    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeConditionDisclosures> VehicleTradeConditionDisclosures;


    public void setCoBuyerSelectedLanguageId(int coBuyerSelectedLanguageId) {
        this.coBuyerSelectedLanguageId = coBuyerSelectedLanguageId;
    }

    @SerializedName("coBuyerSelectedLanguageId")
    @Expose
    private int coBuyerSelectedLanguageId;
    @SerializedName("TypeOfVehicle")
    @Expose
    private String typeOfVehicle;
    @SerializedName("TestDriveTaken")
    @Expose
    private boolean testDriveTaken;

    public String getDealStatus() {
        return DealStatus;
    }

    public void setDealStatus(String dealStatus) {
        DealStatus = dealStatus;
    }

    private String DealStatus;

    public String getFrontUrl() {
        return frontUrl;
    }

    public void setFrontUrl(String frontUrl) {
        this.frontUrl = frontUrl;
    }

    private String frontUrl;

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    private String screenName,TradeCompletionUTC;

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    private String tabName;


    public boolean isCoBuyerTestDriveTaken() {
        return coBuyerTestDriveTaken;
    }

    public void setCoBuyerTestDriveTaken(boolean coBuyerTestDriveTaken) {
        this.coBuyerTestDriveTaken = coBuyerTestDriveTaken;
    }

    @SerializedName("coBuyerTestDriveTaken")
    @Expose
    private boolean coBuyerTestDriveTaken;

    @SerializedName("TestDriveTimeId")
    @Expose
    private int testDriveTimeId;

    public int getCoBuyerTestDriveTimeId() {
        return coBuyerTestDriveTimeId;
    }

    public void setCoBuyerTestDriveTimeId(int coBuyerTestDriveTimeId) {
        this.coBuyerTestDriveTimeId = coBuyerTestDriveTimeId;
    }

    @SerializedName("coBuyerTestDriveTimeId")
    @Expose
    private int coBuyerTestDriveTimeId;
    @SerializedName("NoTestDriveConfirm")
    @Expose
    private boolean noTestDriveConfirm;

    public boolean isCoBuyerNoTestDriveConfirm() {
        return coBuyerNoTestDriveConfirm;
    }

    public void setCoBuyerNoTestDriveConfirm(boolean coBuyerNoTestDriveConfirm) {
        this.coBuyerNoTestDriveConfirm = coBuyerNoTestDriveConfirm;
    }

    @SerializedName("CoBuyerNoTestDriveConfirm")
    @Expose
    private boolean coBuyerNoTestDriveConfirm;
    @SerializedName("StickerRemovalPermission")
    @Expose
    private boolean stickerRemovalPermission;
    @SerializedName("PrintCopyRecieved")
    @Expose
    private boolean printCopyRecieved;

    public boolean isCoBuyerPrintCopyRecieved() {
        return coBuyerPrintCopyRecieved;
    }

    public void setCoBuyerPrintCopyRecieved(boolean coBuyerPrintCopyRecieved) {
        this.coBuyerPrintCopyRecieved = coBuyerPrintCopyRecieved;
    }

    @SerializedName("coBuyerPrintCopyRecieved")
    @Expose
    private boolean coBuyerPrintCopyRecieved;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("IsDataSynced")
    @Expose
    private boolean isDataSynced;
    @SerializedName("IsRecordComplete")
    @Expose
    private boolean isRecordComplete;
    @SerializedName("LatestTimeStamp")
    @Expose
    private String latestTimeStamp;

    public String getLocalLogs() {
        return localLogs;
    }

    public void setLocalLogs(String localLogs) {
        this.localLogs = localLogs;
    }

    private String localLogs;

    public String getErrorDesc() {
        return errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    private String errorDesc;

    public boolean isCoBuyer() {
        return isCoBuyer;
    }

    public void setCoBuyer(boolean coBuyer) {
        isCoBuyer = coBuyer;
    }

    private boolean isCoBuyer,coBuyerAdded;

    public boolean isDisclosureSkipped() {
        return disclosureSkipped;
    }

    public void setDisclosureSkipped(boolean disclosureSkipped) {
        this.disclosureSkipped = disclosureSkipped;
    }

    private boolean disclosureSkipped;

    public boolean isManualEmailSent() {
        return isManualEmailSent;
    }

    public void setManualEmailSent(boolean manualEmailSent) {
        isManualEmailSent = manualEmailSent;
    }

    private boolean isManualEmailSent;

    public boolean isEditing() {
        return isEditing;
    }

    public void setEditing(boolean editing) {
        isEditing = editing;
    }

    private boolean isEditing;

    public boolean isFromServer() {
        return isFromServer;
    }

    public void setFromServer(boolean fromServer) {
        isFromServer = fromServer;
    }

    private boolean isFromServer;

    public boolean isSyncingInProgress() {
        return isSyncingInProgress;
    }

    public void setSyncingInProgress(boolean syncingInProgress) {
        isSyncingInProgress = syncingInProgress;
    }

    private boolean isSyncingInProgress;

    public boolean isSentEmailForIncomplete() {
        return sentEmailForIncomplete;
    }

    public void setSentEmailForIncomplete(boolean sentEmailForIncomplete) {
        this.sentEmailForIncomplete = sentEmailForIncomplete;
    }

    private boolean sentEmailForIncomplete;

    public boolean isHasCoBuyer() {
        return hasCoBuyer;
    }

    public void setHasCoBuyer(boolean hasCoBuyer) {
        this.hasCoBuyer = hasCoBuyer;
    }

    private boolean hasCoBuyer;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    private boolean error;

    public int getSequenceOrder() {
        return sequenceOrder;
    }

    public void setSequenceOrder(int sequenceOrder) {
        this.sequenceOrder = sequenceOrder;
    }

    private int sequenceOrder;
    private boolean isIncompleteDeal;

    public boolean isIncompleteDeal() {
        return isIncompleteDeal;
    }

    public void setIncompleteDeal(boolean incompleteDeal) {
        isIncompleteDeal = incompleteDeal;
    }

    public boolean isIncompleteDealSynced() {
        return isIncompleteDealSynced;
    }

    public void setIncompleteDealSynced(boolean incompleteDealSynced) {
        isIncompleteDealSynced = incompleteDealSynced;
    }

    private boolean isIncompleteDealSynced;
    private boolean isBussinessLead;

    public boolean isBussinessLead() {
        return isBussinessLead;
    }

    public void setBussinessLead(boolean bussinessLead) {
        isBussinessLead = bussinessLead;
    }

    public boolean isBussinessLeadSynced() {
        return isBussinessLeadSynced;
    }

    public void setBussinessLeadSynced(boolean bussinessLeadSynced) {
        isBussinessLeadSynced = bussinessLeadSynced;
    }

    private boolean isBussinessLeadSynced;

    public boolean isHasTradeIn() {
        return hasTradeIn;
    }

    public void setHasTradeIn(boolean hasTradeIn) {
        this.hasTradeIn = hasTradeIn;
    }

    public boolean isTradeIn() {
        return isTradeIn;
    }

    public void setTradeIn(boolean tradeIn) {
        isTradeIn = tradeIn;
    }

    private boolean hasTradeIn;
    private boolean isTradeIn;

    public boolean isIncompleteDealTextSynced() {
        return isIncompleteDealTextSynced;
    }

    public void setIncompleteDealTextSynced(boolean incompleteDealTextSynced) {
        isIncompleteDealTextSynced = incompleteDealTextSynced;
    }

    private boolean isIncompleteDealTextSynced;

    public boolean isDataSyncedWithServer() {
        return dataSyncedWithServer;
    }

    public void setDataSyncedWithServer(boolean dataSyncedWithServer) {
        this.dataSyncedWithServer = dataSyncedWithServer;
    }

    private boolean dataSyncedWithServer;

    public boolean isEmailSent() {
        return isEmailSent;
    }

    public void setEmailSent(boolean emailSent) {
        isEmailSent = emailSent;
    }

    private boolean isEmailSent;

    public String getCustomerVehicleTradeId() {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId(String customerVehicleTradeId) {
        CustomerVehicleTradeId = customerVehicleTradeId;
    }

    private String CustomerVehicleTradeId;

    public String getStickerRemovalString() {
        return stickerRemovalString;
    }

    public void setStickerRemovalString(String stickerRemovalString) {
        this.stickerRemovalString = stickerRemovalString;
    }

    private String stickerRemovalString;

    private boolean isCarImagesConfirmed;

    public boolean isAcceptedUsedCarPolicy() {
        return IsAcceptedUsedCarPolicy;
    }

    public void setAcceptedUsedCarPolicy(boolean acceptedUsedCarPolicy) {
        IsAcceptedUsedCarPolicy = acceptedUsedCarPolicy;
    }

    private boolean IsAcceptedUsedCarPolicy ;

    public boolean isCarImagesConfirmed() {
        return isCarImagesConfirmed;
    }

    public void setCarImagesConfirmed(boolean carImagesConfirmed) {
        isCarImagesConfirmed = carImagesConfirmed;
    }

    public boolean isCarDetailConfirmed() {
        return isCarDetailConfirmed;
    }

    public void setCarDetailConfirmed(boolean carDetailConfirmed) {
        isCarDetailConfirmed = carDetailConfirmed;
    }

    private boolean isCarDetailConfirmed;

    public int getLastPageIndex() {
        return lastPageIndex;
    }

    public void setLastPageIndex(int lastPageIndex) {
        this.lastPageIndex = lastPageIndex;
    }

    private int lastPageIndex;

    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public int getSelectedLanguageId() {
        return selectedLanguageId;
    }

    public void setSelectedLanguageId(int selectedLanguageId) {
        this.selectedLanguageId = selectedLanguageId;
    }

    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    public void setTypeOfVehicle(String typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }

    public boolean getTestDriveTaken() {
        return testDriveTaken;
    }

    public void setTestDriveTaken(boolean testDriveTaken) {
        this.testDriveTaken = testDriveTaken;
    }

    public int getTestDriveTimeId() {
        return testDriveTimeId;
    }

    public void setTestDriveTimeId(int testDriveTimeId) {
        this.testDriveTimeId = testDriveTimeId;
    }

    public boolean getNoTestDriveConfirm() {
        return noTestDriveConfirm;
    }

    public void setNoTestDriveConfirm(boolean noTestDriveConfirm) {
        this.noTestDriveConfirm = noTestDriveConfirm;
    }

    public boolean getStickerRemovalPermission() {
        return stickerRemovalPermission;
    }

    public void setStickerRemovalPermission(boolean stickerRemovalPermission) {
        this.stickerRemovalPermission = stickerRemovalPermission;
    }

    public boolean getPrintCopyRecieved() {
        return printCopyRecieved;
    }

    public void setPrintCopyRecieved(boolean printCopyRecieved) {
        this.printCopyRecieved = printCopyRecieved;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean getIsDataSynced() {
        return isDataSynced;
    }

    public void setIsDataSynced(boolean isDataSynced) {
        this.isDataSynced = isDataSynced;
    }

    public boolean getIsRecordComplete() {
        return isRecordComplete;
    }

    public void setIsRecordComplete(boolean isRecordComplete) {
        this.isRecordComplete = isRecordComplete;
    }

    public String getLatestTimeStamp() {
        return latestTimeStamp;
    }

    public void setLatestTimeStamp(String latestTimeStamp) {
        this.latestTimeStamp = latestTimeStamp;
    }

    private ArrayList<HistoryListPojo> printReceivedList;

    public ArrayList<HistoryListPojo> getCobuyerPrintReceivedList() {
        return cobuyerPrintReceivedList;
    }

    public void setCobuyerPrintReceivedList(ArrayList<HistoryListPojo> cobuyerPrintReceivedList) {
        this.cobuyerPrintReceivedList = cobuyerPrintReceivedList;
    }

    private ArrayList<HistoryListPojo> cobuyerPrintReceivedList;

    public ArrayList<ImagesSynced> getImagesSyncedArrayList() {
        return imagesSyncedArrayList;
    }

    public void setImagesSyncedArrayList(ArrayList<ImagesSynced> imagesSyncedArrayList) {
        this.imagesSyncedArrayList = imagesSyncedArrayList;
    }

    private ArrayList<ImagesSynced> imagesSyncedArrayList;

    public ArrayList<ImagesSynced> getMultiSignImages() {
        return multiSignImages;
    }

    public void setMultiSignImages(ArrayList<ImagesSynced> multiSignImages) {
        this.multiSignImages = multiSignImages;
    }

    private ArrayList<ImagesSynced> multiSignImages=new ArrayList<>();

    public ArrayList<HistoryListPojo> getPrintReceivedList() {
        return printReceivedList;
    }

    public void setPrintReceivedList(ArrayList<HistoryListPojo> printReceivedList) {
        this.printReceivedList = printReceivedList;
    }


    public String getTradeCompletionUTC() {
        return TradeCompletionUTC;
    }

    public void setTradeCompletionUTC(String tradeCompletionUTC) {
        TradeCompletionUTC = tradeCompletionUTC;
    }

    public boolean isCoBuyerAdded() {
        return coBuyerAdded;
    }

    public void setCoBuyerAdded(boolean coBuyerAdded) {
        this.coBuyerAdded = coBuyerAdded;
    }

    public boolean isHideMileage() {
        return hideMileage;
    }

    public void setHideMileage(boolean hideMileage) {
        this.hideMileage = hideMileage;
    }
}
