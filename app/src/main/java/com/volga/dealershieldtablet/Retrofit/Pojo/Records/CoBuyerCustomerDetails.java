package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListUpload;
import com.volga.dealershieldtablet.screenRevamping.pojo.CoBuyerDocs;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromiseUpload;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;

import java.io.Serializable;
import java.util.ArrayList;

public class CoBuyerCustomerDetails implements Serializable {

    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("LastName")
    @Expose
    private String LastName;
    @SerializedName("GenderTypeId")
    @Expose
    private String GenderTypeId;
    @SerializedName("DateOfBirth")
    @Expose
    private String DateOfBirth;
    @SerializedName("Address1")
    @Expose
    private String Address1;
    @SerializedName("Address2")
    @Expose
    private String Address2;
    @SerializedName("City")
    @Expose
    private String City;
    @SerializedName("State")
    @Expose
    private String State;
    @SerializedName("Country")
    @Expose
    private String Country;
    @SerializedName("DrivingLicenseNumber")
    @Expose
    private String DrivingLicenseNumber;
    @SerializedName("DrivingLicenseExpiryDate")
    @Expose
    private String DrivingLicenseExpiryDate;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("NoEmailChecked")
    @Expose
    private String noEmailChecked;
    @SerializedName("ContactNumber")
    @Expose
    private String ContactNumber;

    @SerializedName("TestDriveDurationId")
    @Expose
    private String testDriveTimeId;
    @SerializedName("IsTestDriveTaken")
    @Expose
    private boolean testDriveTaken;

    public boolean getIsRemote() {
        return IsRemote;
    }

    public void setIsRemote(boolean isRemote) {
        IsRemote = isRemote;
    }

    private boolean IsRemote;

    public String getTestDriveTimeId() {
        return testDriveTimeId;
    }

    public void setTestDriveTimeId(String testDriveTimeId) {
        this.testDriveTimeId = testDriveTimeId;
    }

    public boolean isTestDriveTaken() {
        return testDriveTaken;
    }

    public void setTestDriveTaken(boolean testDriveTaken) {
        this.testDriveTaken = testDriveTaken;
    }

    public boolean isNoTestDriveConfirm() {
        return noTestDriveConfirm;
    }

    public void setNoTestDriveConfirm(boolean noTestDriveConfirm) {
        this.noTestDriveConfirm = noTestDriveConfirm;
    }

    @SerializedName("NoTestDriveConfirm")
    @Expose
    private boolean noTestDriveConfirm;


    public ArrayList<CheckListUpload> getCheckLists() {
        return CheckLists;
    }

    public void setCheckLists(ArrayList<CheckListUpload> checkLists) {
        CheckLists = checkLists;
    }

    public ArrayList<VerbalPromiseUpload> getVerbalPromises() {
        return VerbalPromises;
    }

    public void setVerbalPromises(ArrayList<VerbalPromiseUpload> verbalPromises) {
        VerbalPromises = verbalPromises;
    }

    ArrayList<CheckListUpload> CheckLists;
    ArrayList<VerbalPromiseUpload> VerbalPromises;

    public ArrayList<com.volga.dealershieldtablet.screenRevamping.pojo.CoBuyerDocs> getCoBuyerDocs() {
        return CoBuyerDocs;
    }

    public void setCoBuyerDocs(ArrayList<com.volga.dealershieldtablet.screenRevamping.pojo.CoBuyerDocs> coBuyerDocs) {
        CoBuyerDocs = coBuyerDocs;
    }

    private ArrayList<CoBuyerDocs> CoBuyerDocs;


    public boolean isAcceptedUsedCarPolicy() {
        return IsAcceptedUsedCarPolicy;
    }

    public void setAcceptedUsedCarPolicy(boolean acceptedUsedCarPolicy) {
        IsAcceptedUsedCarPolicy = acceptedUsedCarPolicy;
    }

    private ArrayList<CustomerAgreement> printReceivedList;

    public ArrayList<CustomerAgreement> getPrintReceivedList() {
        return printReceivedList;
    }

    public void setPrintReceivedList(ArrayList<CustomerAgreement> printReceivedList) {
        this.printReceivedList = printReceivedList;
    }
    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> getCustomerAgreement() {
        return CustomerAgreement;
    }

    public void setCustomerAgreement(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> customerAgreement) {
        CustomerAgreement = customerAgreement;
    }

    ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> CustomerAgreement;

    private boolean IsAcceptedUsedCarPolicy;

    public SalesPersons getSalesPersons() {
        return salesPersons;
    }

    public void setSalesPersons(SalesPersons salesPersons) {
        this.salesPersons = salesPersons;
    }

    private SalesPersons salesPersons;


    public String getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(String languageId) {
        LanguageId = languageId;
    }

    private String LanguageId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    private String Id;

    public boolean isTermConditionAccepted() {
        return IsTermConditionAccepted;
    }

    public void setTermConditionAccepted(boolean termConditionAccepted) {
        IsTermConditionAccepted = termConditionAccepted;
    }

    private boolean IsTermConditionAccepted;

    public String getZipCode() {
        return Zipcode;
    }

    public void setZipCode(String zipCode) {
        this.Zipcode = zipCode;
    }

    private String Zipcode;

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    private String MiddleName;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getGender() {
        return GenderTypeId;
    }

    public void setGender(String GenderTypeId) {
        this.GenderTypeId = GenderTypeId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public String getAddressLineOne() {
        return Address1;
    }

    public void setAddressLineOne(String Address1) {
        this.Address1 = Address1;
    }

    public String getAddressLineTwo() {
        return Address2;
    }

    public void setAddressLineTwo(String Address2) {
        this.Address2 = Address2;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        this.City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        this.State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        this.Country = country;
    }

    public String getDrivingLicenseNumber() {
        return DrivingLicenseNumber;
    }

    public void setDLNumber(String DrivingLicenseNumber) {
        this.DrivingLicenseNumber = DrivingLicenseNumber;
    }

    public String getDLExpiryDate() {
        return DrivingLicenseExpiryDate;
    }

    public void setDLExpiryDate(String dLExpiryDate) {
        this.DrivingLicenseExpiryDate = dLExpiryDate;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public String getNoEmailChecked() {
        return noEmailChecked;
    }

    public void setNoEmailChecked(String noEmailChecked) {
        this.noEmailChecked = noEmailChecked;
    }

    public String getMobileNumber() {
        return ContactNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.ContactNumber = mobileNumber;
    }

}
