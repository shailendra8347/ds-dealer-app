
package com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DealershipData {

    @SerializedName("Dealerships")
    @Expose
    private List<Dealership> dealerships = new ArrayList<>();

    public List<Dealership> getDealerships() {
        return dealerships;
    }

    public void setDealerships(List<Dealership> dealerships) {
        this.dealerships = dealerships;
    }

    public Dealership where(int dealershipID)
    {
        Dealership dealership = new Dealership();
        for (int i = 0 ; i < this.getDealerships().size() ; i++)
        {
            if (this.getDealerships().get(i).getId() == dealershipID)
            {
                dealership = this.getDealerships().get(i);
            }
        }
        return dealership;
    }
}
