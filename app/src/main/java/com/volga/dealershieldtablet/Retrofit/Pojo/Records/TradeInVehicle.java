
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs;

import java.util.ArrayList;

public class TradeInVehicle {

    @SerializedName("TypeOfPurchase")
    @Expose
    private String typeOfPurchase;
    @SerializedName("VIN")
    @Expose
    private String VIN;
    @SerializedName("Make")
    @Expose
    private String Make;
    @SerializedName("Model")
    @Expose
    private String Model;
    @SerializedName("ModelYear")
    @Expose
    private String ModelYear;
    @SerializedName("Mileage")
    @Expose
    private String Mileage;
    private String ExteriorColor;
    private String InteriorColor;
    private String licencePlateNumber;
    private String carOdoMeterTime;
    private String stockNumber;
    private String typeOfVehicle;

    private String title1;
    private String title2;
    private String title3;
    private String title4;

    public ArrayList<CustomerVehicleTradeDocs> getTradeInVehicleDocs() {
        return TradeInVehicleDocs;
    }

    public void setTradeInVehicleDocs(ArrayList<CustomerVehicleTradeDocs> tradeInVehicleDocs) {
        TradeInVehicleDocs = tradeInVehicleDocs;
    }

    private ArrayList<CustomerVehicleTradeDocs> TradeInVehicleDocs;
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    private String Id;

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public String getTitle4() {
        return title4;
    }

    public void setTitle4(String title4) {
        this.title4 = title4;
    }

    public String getTitle5() {
        return title5;
    }

    public void setTitle5(String title5) {
        this.title5 = title5;
    }

    private String title5;


    public String getExteriorColor() {
        return ExteriorColor;
    }

    public void setExteriorColor(String exteriorColor) {
        ExteriorColor = exteriorColor;
    }

    public String getInteriorColor() {
        return InteriorColor;
    }

    public void setInteriorColor(String interiorColor) {
        InteriorColor = interiorColor;
    }

    public String getLicencePlateNumber() {
        return licencePlateNumber;
    }

    public void setLicencePlateNumber(String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }


    public String getCarOdoMeterTime() {
        return carOdoMeterTime;
    }

    public void setCarOdoMeterTime(String carOdoMeterTime) {
        this.carOdoMeterTime = carOdoMeterTime;
    }


    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }


    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    public void setTypeOfVehicle(String typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }


    public String getTypeOfPurchase() {
        return typeOfPurchase;
    }

    public void setTypeOfPurchase(String typeOfPurchase) {
        this.typeOfPurchase = typeOfPurchase;
    }

    public String getVINNumber() {
        return VIN;
    }

    public void setVINNumber(String vINNumber) {
        this.VIN = vINNumber;
    }

    public String getCarMake() {
        return Make;
    }

    public void setCarMake(String carMake) {
        this.Make = carMake;
    }

    public String getCarModel() {
        return Model;
    }

    public void setCarModel(String carModel) {
        this.Model = carModel;
    }

    public String getCarYear() {
        return ModelYear;
    }

    public void setCarYear(String carYear) {
        this.ModelYear = carYear;
    }

    public String getMileage() {
        return Mileage;
    }

    public void setMileage(String mileage) {
        this.Mileage = mileage;
    }


}
