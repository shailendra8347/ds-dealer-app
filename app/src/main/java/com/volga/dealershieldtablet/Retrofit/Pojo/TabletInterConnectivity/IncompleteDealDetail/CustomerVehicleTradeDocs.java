package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

public class CustomerVehicleTradeDocs {
    private String DocId;

    private boolean IsAdditionalImage;

    public boolean isInventoryImage() {
        return IsInventoryImage;
    }

    public void setInventoryImage(boolean inventoryImage) {
        IsInventoryImage = inventoryImage;
    }

    private boolean IsInventoryImage;

    private String DocUrl;

    private String CustomerVehicleTradeId;

    private String DateCreatedUtc;

    private String DateModifiedUtc;

    private String Id;

    private String DocTypeName;

    public String getDocId() {
        return DocId;
    }

    public void setDocId(String DocId) {
        this.DocId = DocId;
    }

    public boolean getIsAdditionalImage() {
        return IsAdditionalImage;
    }

    public void setIsAdditionalImage(boolean IsAdditionalImage) {
        this.IsAdditionalImage = IsAdditionalImage;
    }

    public String getDocUrl() {
        return DocUrl;
    }

    public void setDocUrl(String DocUrl) {
        this.DocUrl = DocUrl;
    }

    public String getCustomerVehicleTradeId() {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId(String CustomerVehicleTradeId) {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public String getDateCreatedUtc() {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc(String DateCreatedUtc) {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getDateModifiedUtc() {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc(String DateModifiedUtc) {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getDocTypeName() {
        return DocTypeName;
    }

    public void setDocTypeName(String DocTypeName) {
        this.DocTypeName = DocTypeName;
    }

    @Override
    public String toString() {
        return "ClassPojo [DocId = " + DocId + ", IsAdditionalImage = " + IsAdditionalImage + ", DocUrl = " + DocUrl + ", CustomerVehicleTradeId = " + CustomerVehicleTradeId + ", DateCreatedUtc = " + DateCreatedUtc + ", DateModifiedUtc = " + DateModifiedUtc + ",  Id = " + Id + ", DocTypeName = " + DocTypeName + "]";
    }
}
