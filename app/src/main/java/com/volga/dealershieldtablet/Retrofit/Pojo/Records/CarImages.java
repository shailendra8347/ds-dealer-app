
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarImages {

    @SerializedName("isIMG_FRONT_SYNCED")
    @Expose
    private boolean isIMGFRONTSYNCED;
    @SerializedName("isIMG_LEFT_SYNCED")
    @Expose
    private boolean isIMGLEFTSYNCED;
    @SerializedName("isIMG_MILEAGE_SYNCED")
    @Expose
    private boolean isIMGMILEAGESYNCED;
    @SerializedName("isIMG_REAR_SYNCED")
    @Expose
    private boolean isIMGREARSYNCED;
    @SerializedName("isIMG_RIGHT_SYNCED")
    @Expose
    private boolean isIMGRIGHTSYNCED;
    @SerializedName("isIMG_STIKER_SYNCED")
    @Expose
    private boolean isIMGSTIKERSYNCED;

    public boolean getIsIMGFRONTSYNCED() {
        return isIMGFRONTSYNCED;
    }

    public void setIsIMGFRONTSYNCED(boolean isIMGFRONTSYNCED) {
        this.isIMGFRONTSYNCED = isIMGFRONTSYNCED;
    }

    public boolean getIsIMGLEFTSYNCED() {
        return isIMGLEFTSYNCED;
    }

    public void setIsIMGLEFTSYNCED(boolean isIMGLEFTSYNCED) {
        this.isIMGLEFTSYNCED = isIMGLEFTSYNCED;
    }

    public boolean getIsIMGMILEAGESYNCED() {
        return isIMGMILEAGESYNCED;
    }

    public void setIsIMGMILEAGESYNCED(boolean isIMGMILEAGESYNCED) {
        this.isIMGMILEAGESYNCED = isIMGMILEAGESYNCED;
    }

    public boolean getIsIMGREARSYNCED() {
        return isIMGREARSYNCED;
    }

    public void setIsIMGREARSYNCED(boolean isIMGREARSYNCED) {
        this.isIMGREARSYNCED = isIMGREARSYNCED;
    }

    public boolean getIsIMGRIGHTSYNCED() {
        return isIMGRIGHTSYNCED;
    }

    public void setIsIMGRIGHTSYNCED(boolean isIMGRIGHTSYNCED) {
        this.isIMGRIGHTSYNCED = isIMGRIGHTSYNCED;
    }

    public boolean getIsIMGSTIKERSYNCED() {
        return isIMGSTIKERSYNCED;
    }

    public void setIsIMGSTIKERSYNCED(boolean isIMGSTIKERSYNCED) {
        this.isIMGSTIKERSYNCED = isIMGSTIKERSYNCED;
    }

}
