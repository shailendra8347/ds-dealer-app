package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

import com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts;

import java.util.ArrayList;

public class VehicleConditionDisclosure
{
    private String Succeeded;

    private String[] Errors;

    private String IsDuplicate;

    private String DuePayment;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    private String Message;

    private String IsAlreadyLeadMarked;

    private ArrayList<Value> Value;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts> getWarningAlerts() {
        return WarningAlerts;
    }

    public void setWarningAlerts(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts> warningAlerts) {
        WarningAlerts = warningAlerts;
    }

    private ArrayList<WarningAlerts> WarningAlerts;

    private String ErrorsString;

    private String IsUpdated;

    private ErrorDetails ErrorDetails;

    public String getSucceeded ()
    {
        return Succeeded;
    }

    public void setSucceeded (String Succeeded)
    {
        this.Succeeded = Succeeded;
    }

    public String[] getErrors ()
    {
        return Errors;
    }

    public void setErrors (String[] Errors)
    {
        this.Errors = Errors;
    }

    public String getIsDuplicate ()
    {
        return IsDuplicate;
    }

    public void setIsDuplicate (String IsDuplicate)
    {
        this.IsDuplicate = IsDuplicate;
    }

    public String getDuePayment ()
    {
        return DuePayment;
    }

    public void setDuePayment (String DuePayment)
    {
        this.DuePayment = DuePayment;
    }

    public String getIsAlreadyLeadMarked ()
    {
        return IsAlreadyLeadMarked;
    }

    public void setIsAlreadyLeadMarked (String IsAlreadyLeadMarked)
    {
        this.IsAlreadyLeadMarked = IsAlreadyLeadMarked;
    }

    public ArrayList<Value> getValue ()
    {
        return Value;
    }

    public void setValue (ArrayList<Value> Value)
    {
        this.Value = Value;
    }

    public String getErrorsString ()
    {
        return ErrorsString;
    }

    public void setErrorsString (String ErrorsString)
    {
        this.ErrorsString = ErrorsString;
    }

    public String getIsUpdated ()
    {
        return IsUpdated;
    }

    public void setIsUpdated (String IsUpdated)
    {
        this.IsUpdated = IsUpdated;
    }

    public ErrorDetails getErrorDetails ()
    {
        return ErrorDetails;
    }

    public void setErrorDetails (ErrorDetails ErrorDetails)
    {
        this.ErrorDetails = ErrorDetails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Succeeded = "+Succeeded+", Errors = "+Errors+", IsDuplicate = "+IsDuplicate+", DuePayment = "+DuePayment+", IsAlreadyLeadMarked = "+IsAlreadyLeadMarked+", Value = "+Value+", ErrorsString = "+ErrorsString+", IsUpdated = "+IsUpdated+", ErrorDetails = "+ErrorDetails+"]";
    }
}