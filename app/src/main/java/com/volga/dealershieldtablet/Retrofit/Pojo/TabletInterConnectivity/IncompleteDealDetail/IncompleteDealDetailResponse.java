package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts;

import java.util.ArrayList;

public class IncompleteDealDetailResponse {
    private boolean Allow2RemoveWindowSticker;

    private String stockNumber;

    private ArrayList<CoBuyers> CoBuyers;
    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.VehicleTradeAdditionalDisclosures> getVehicleTradeAdditionalDisclosures() {
        return VehicleTradeAdditionalDisclosures;
    }

    public void setVehicleTradeAdditionalDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.VehicleTradeAdditionalDisclosures> vehicleTradeAdditionalDisclosures) {
        VehicleTradeAdditionalDisclosures = vehicleTradeAdditionalDisclosures;
    }

    private ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.VehicleTradeAdditionalDisclosures> VehicleTradeAdditionalDisclosures;

    public ArrayList<InitThirdParty> getThirdPartyReportRawDTOList() {
        return ThirdPartyReportRawDTOList;
    }

    public void setThirdPartyReportRawDTOList(ArrayList<InitThirdParty> thirdPartyReportRawDTOList) {
        ThirdPartyReportRawDTOList = thirdPartyReportRawDTOList;
    }

    private ArrayList<InitThirdParty> ThirdPartyReportRawDTOList;

    private String DateModifiedUtc;

    public ArrayList<WarningAlerts> getConditionDisclosureWarningAlert() {
        return ConditionDisclosureWarningAlert;
    }

    public void setConditionDisclosureWarningAlert(ArrayList<WarningAlerts> conditionDisclosureWarningAlert) {
        ConditionDisclosureWarningAlert = conditionDisclosureWarningAlert;
    }

    private ArrayList<WarningAlerts> ConditionDisclosureWarningAlert;

    private String CarTypeTitle;


    private String TestDriveDurationTitle;

    private boolean IsTestDriveTaken;

    public boolean isRemote() {
        return IsRemote;
    }

    public void setRemote(boolean remote) {
        IsRemote = remote;
    }

    private boolean IsRemote;

    public boolean isDisclosureSkipped() {
        return IsDisclosureSkipped;
    }

    public void setDisclosureSkipped(boolean disclosureSkipped) {
        IsDisclosureSkipped = disclosureSkipped;
    }

    private boolean IsDisclosureSkipped;

    private String TradeDateTime;

    private String TestDriveDurationId;

    private String TradeTypeTitle;

    private String Mileage;
    private ArrayList<VehicleTradeHistoryDisclosures> VehicleTradeHistoryDisclosures;
    private ArrayList<VehicleTradeThirdPartyHistoryReports> VehicleTradeThirdPartyHistoryReports;
    private ArrayList<VehicleTradeConditionDisclosures> VehicleTradeConditionDisclosures;

    private String Make;

    private String CustomerId;

    private CustomerDetail CustomerDetail;

    private ArrayList<CustomerVehicleTradeDocs> CustomerVehicleTradeDocs;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> getCustomerVehicleTradeDocsAll() {
        return CustomerVehicleTradeDocsAll;
    }

    public void setCustomerVehicleTradeDocsAll(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs> customerVehicleTradeDocsAll) {
        CustomerVehicleTradeDocsAll = customerVehicleTradeDocsAll;
    }

    private ArrayList<CustomerVehicleTradeDocs> CustomerVehicleTradeDocsAll;

    private ArrayList<Value> ConditionDisclosure, HistoryDisclosure;

    public ArrayList<Value> getConditionDisclosure() {
        return ConditionDisclosure;
    }

    public void setConditionDisclosure(ArrayList<Value> conditionDisclosure) {
        ConditionDisclosure = conditionDisclosure;
    }

    public ArrayList<Value> getHistoryDisclosure() {
        return HistoryDisclosure;
    }

    public void setHistoryDisclosure(ArrayList<Value> historyDisclosure) {
        HistoryDisclosure = historyDisclosure;
    }

    public ArrayList<ThirdPartyObj> getThirdParty() {
        return ThirdParty;
    }

    public void setThirdParty(ArrayList<ThirdPartyObj> thirdParty) {
        ThirdParty = thirdParty;
    }

    private ArrayList<ThirdPartyObj> ThirdParty;


    private String TradeTypeId;

    private String DateCreatedUtc;

    private String Model;

    private String VIN;

    private String ModelYear;

    private String Id;

    private ArrayList<TradeInVehicle> TradeInVehicle;

    private String CarTypeId;

    public boolean getAllow2RemoveWindowSticker() {
        return Allow2RemoveWindowSticker;
    }

    public void setAllow2RemoveWindowSticker(boolean Allow2RemoveWindowSticker) {
        this.Allow2RemoveWindowSticker = Allow2RemoveWindowSticker;
    }

    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    public ArrayList<CoBuyers> getCoBuyers() {
        return CoBuyers;
    }

    public void setCoBuyers(ArrayList<CoBuyers> CoBuyers) {
        this.CoBuyers = CoBuyers;
    }

    public String getDateModifiedUtc() {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc(String DateModifiedUtc) {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getCarTypeTitle() {
        return CarTypeTitle;
    }

    public void setCarTypeTitle(String CarTypeTitle) {
        this.CarTypeTitle = CarTypeTitle;
    }

    public ArrayList<VehicleTradeHistoryDisclosures> getVehicleTradeHistoryDisclosures() {
        return VehicleTradeHistoryDisclosures;
    }

    public void setVehicleTradeHistoryDisclosures(ArrayList<VehicleTradeHistoryDisclosures> VehicleTradeHistoryDisclosures) {
        this.VehicleTradeHistoryDisclosures = VehicleTradeHistoryDisclosures;
    }

    public String getTestDriveDurationTitle() {
        return TestDriveDurationTitle;
    }

    public void setTestDriveDurationTitle(String TestDriveDurationTitle) {
        this.TestDriveDurationTitle = TestDriveDurationTitle;
    }

    public boolean getIsTestDriveTaken() {
        return IsTestDriveTaken;
    }

    public void setIsTestDriveTaken(boolean IsTestDriveTaken) {
        this.IsTestDriveTaken = IsTestDriveTaken;
    }

    public String getTradeDateTime() {
        return TradeDateTime;
    }

    public void setTradeDateTime(String TradeDateTime) {
        this.TradeDateTime = TradeDateTime;
    }

    public String getTestDriveDurationId() {
        return TestDriveDurationId;
    }

    public void setTestDriveDurationId(String TestDriveDurationId) {
        this.TestDriveDurationId = TestDriveDurationId;
    }

    public String getTradeTypeTitle() {
        return TradeTypeTitle;
    }

    public void setTradeTypeTitle(String TradeTypeTitle) {
        this.TradeTypeTitle = TradeTypeTitle;
    }

    public String getMileage() {
        return Mileage;
    }

    public void setMileage(String Mileage) {
        this.Mileage = Mileage;
    }

    public ArrayList<VehicleTradeThirdPartyHistoryReports> getVehicleTradeThirdPartyHistoryReports() {
        return VehicleTradeThirdPartyHistoryReports;
    }

    public void setVehicleTradeThirdPartyHistoryReports(ArrayList<VehicleTradeThirdPartyHistoryReports> VehicleTradeThirdPartyHistoryReports) {
        this.VehicleTradeThirdPartyHistoryReports = VehicleTradeThirdPartyHistoryReports;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String CustomerId) {
        this.CustomerId = CustomerId;
    }

    public CustomerDetail getCustomerDetail() {
        return CustomerDetail;
    }

    public void setCustomerDetail(CustomerDetail CustomerDetail) {
        this.CustomerDetail = CustomerDetail;
    }

    public ArrayList<CustomerVehicleTradeDocs> getCustomerVehicleTradeDocs() {
        return CustomerVehicleTradeDocs;
    }

    public void setCustomerVehicleTradeDocs(ArrayList<CustomerVehicleTradeDocs> CustomerVehicleTradeDocs) {
        this.CustomerVehicleTradeDocs = CustomerVehicleTradeDocs;
    }

    public ArrayList<VehicleTradeConditionDisclosures> getVehicleTradeConditionDisclosures() {
        return VehicleTradeConditionDisclosures;
    }

    public void setVehicleTradeConditionDisclosures(ArrayList<VehicleTradeConditionDisclosures> VehicleTradeConditionDisclosures) {
        this.VehicleTradeConditionDisclosures = VehicleTradeConditionDisclosures;
    }

    public String getTradeTypeId() {
        return TradeTypeId;
    }

    public void setTradeTypeId(String TradeTypeId) {
        this.TradeTypeId = TradeTypeId;
    }

    public String getDateCreatedUtc() {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc(String DateCreatedUtc) {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getModelYear() {
        return ModelYear;
    }

    public void setModelYear(String ModelYear) {
        this.ModelYear = ModelYear;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public ArrayList<TradeInVehicle> getTradeInVehicle() {
        return TradeInVehicle;
    }

    public void setTradeInVehicle(ArrayList<TradeInVehicle> TradeInVehicle) {
        this.TradeInVehicle = TradeInVehicle;
    }

    public String getCarTypeId() {
        return CarTypeId;
    }

    public void setCarTypeId(String CarTypeId) {
        this.CarTypeId = CarTypeId;
    }

    @Override
    public String toString() {
        return "ClassPojo [Allow2RemoveWindowSticker = " + Allow2RemoveWindowSticker + ", stockNumber = " + stockNumber + ", CoBuyers = " + CoBuyers + ", DateModifiedUtc = " + DateModifiedUtc + ", CarTypeTitle = " + CarTypeTitle + ", VehicleTradeHistoryDisclosures = " + VehicleTradeHistoryDisclosures + ", TestDriveDurationTitle = " + TestDriveDurationTitle + ", IsTestDriveTaken = " + IsTestDriveTaken + ", TradeDateTime = " + TradeDateTime + ", TestDriveDurationId = " + TestDriveDurationId + ", TradeTypeTitle = " + TradeTypeTitle + ", Mileage = " + Mileage + ", VehicleTradeThirdPartyHistoryReports = " + VehicleTradeThirdPartyHistoryReports + ", Make = " + Make + ", CustomerId = " + CustomerId + ", CustomerDetail = " + CustomerDetail + ", CustomerVehicleTradeDocs = " + CustomerVehicleTradeDocs + ", VehicleTradeConditionDisclosures = " + VehicleTradeConditionDisclosures + ", TradeTypeId = " + TradeTypeId + ", DateCreatedUtc = " + DateCreatedUtc + ", Model = " + Model + ", VIN = " + VIN + ", ModelYear = " + ModelYear + ", Id = " + Id + ", TradeInVehicle = " + TradeInVehicle + ", CarTypeId = " + CarTypeId + "]";
    }
}
