package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdPartyReportList;

import java.util.ArrayList;

public class ThirdPartyObj {
    private String LogoUrl;

    private String Title;

    public ThirdPartyReportBytes getThirdPartyReportBytes() {
        return thirdPartyReportBytes;
    }

    public void setThirdPartyReportBytes(ThirdPartyReportBytes thirdPartyReportBytes) {
        this.thirdPartyReportBytes = thirdPartyReportBytes;
    }

    private ThirdPartyReportBytes thirdPartyReportBytes;

    public ThirdPartyReportList getThirdPartyList() {
        return thirdPartyList;
    }

    public void setThirdPartyList(ThirdPartyReportList thirdPartyList) {
        this.thirdPartyList = thirdPartyList;
    }

    private ThirdPartyReportList thirdPartyList;

    private String Id;

    private String DisplaySeqNo;

    public boolean isMandatory() {
        return IsMandatory;
    }

    public void setMandatory(boolean mandatory) {
        IsMandatory = mandatory;
    }

    private boolean IsMandatory,isAutoMarked;

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    private boolean IsActive;

    public boolean isViewReport() {
        return ViewReport;
    }

    public void setViewReport(boolean viewReport) {
        ViewReport = viewReport;
    }

    private boolean ViewReport;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> getTranslations() {
        return Translations;
    }

    public void setTranslations(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> translations) {
        Translations = translations;
    }

    private ArrayList<Translations> Translations;

    public String getLogoUrl() {
        return LogoUrl;
    }

    public void setLogoUrl(String LogoUrl) {
        this.LogoUrl = LogoUrl;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getDisplaySeqNo() {
        return DisplaySeqNo;
    }

    public void setDisplaySeqNo(String DisplaySeqNo) {
        this.DisplaySeqNo = DisplaySeqNo;
    }

    @Override
    public String toString() {
        return "ClassPojo [LogoUrl = " + LogoUrl + ", Title = " + Title + ", Id = " + Id + ", DisplaySeqNo = " + DisplaySeqNo + "]";
    }

    public boolean isAutoMarked() {
        return isAutoMarked;
    }

    public void setAutoMarked(boolean autoMarked) {
        isAutoMarked = autoMarked;
    }
}
