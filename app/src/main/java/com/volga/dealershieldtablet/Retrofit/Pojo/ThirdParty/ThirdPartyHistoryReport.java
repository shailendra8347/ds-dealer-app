package com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty;

public class ThirdPartyHistoryReport
{
    private String LogoUrl;

    private String Title;

    private String Id;

    private String DisplaySeqNo;

    public String getLogoUrl ()
    {
        return LogoUrl;
    }

    public void setLogoUrl (String LogoUrl)
    {
        this.LogoUrl = LogoUrl;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getDisplaySeqNo ()
    {
        return DisplaySeqNo;
    }

    public void setDisplaySeqNo (String DisplaySeqNo)
    {
        this.DisplaySeqNo = DisplaySeqNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LogoUrl = "+LogoUrl+", Title = "+Title+", Id = "+Id+", DisplaySeqNo = "+DisplaySeqNo+"]";
    }
}