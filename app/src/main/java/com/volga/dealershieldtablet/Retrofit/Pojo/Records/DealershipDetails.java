
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehiclePhotoType;

import java.util.ArrayList;

public class DealershipDetails {

    @SerializedName("DealershipName")
    @Expose
    private String dealershipName;
    @SerializedName("DealershipId")
    @Expose
    private int dealershipId;
    private ArrayList<VehiclePhotoType> vehiclePhotoTypes;

    public String getDealershipName() {
        return dealershipName;
    }

    public void setDealershipName(String dealershipName) {
        this.dealershipName = dealershipName;
    }

    public int getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(int dealershipId) {
        this.dealershipId = dealershipId;
    }

    public ArrayList<VehiclePhotoType> getVehiclePhotoTypes() {
        return vehiclePhotoTypes;
    }

    public void setVehiclePhotoTypes(ArrayList<VehiclePhotoType> vehiclePhotoTypes) {
        this.vehiclePhotoTypes = vehiclePhotoTypes;
    }
}
