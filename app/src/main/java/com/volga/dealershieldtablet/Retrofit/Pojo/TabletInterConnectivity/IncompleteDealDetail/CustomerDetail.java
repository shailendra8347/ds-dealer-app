package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.screenRevamping.pojo.Location;

import java.util.ArrayList;

public class CustomerDetail {
    private String DrivingLicenseExpiryDate;

    private String Email;

    private String GenderTitle;

    private String DateModifiedUtc;

    private String IsLead;

    public boolean getIsRemote() {
        return IsRemote;
    }

    public void setIsRemote(boolean isRemote) {
        IsRemote = isRemote;
    }

    private boolean IsRemote;

    private ArrayList<String> CustomerVehicleTrades;

    private String DealershipId;

    private String CreatedById;

    private ArrayList<CustomerAgreement> VehicleTradeCustomerAgreements;

    private boolean IsAcceptedUsedCarPolicy;

    private String ContactNumber;

    private SalesPersons[] SalesPersons;

    private String DateOfBirth;

    private boolean IsTermConditionAccepted;

    private String FirstName;

    private String DrivingLicenseNumber;

    private String LocationId;

    private String MiddleName;

    private String LanguageTitle;

    private String GenderTypeId;

    private String DateCreatedUtc;

    private String LastScreenName;

    private String Id;

    private String LastName;

    private String LanguageId;

    private com.volga.dealershieldtablet.screenRevamping.pojo.Location Location;

    public String getDrivingLicenseExpiryDate() {
        return DrivingLicenseExpiryDate;
    }

    public void setDrivingLicenseExpiryDate(String DrivingLicenseExpiryDate) {
        this.DrivingLicenseExpiryDate = DrivingLicenseExpiryDate;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getGenderTitle() {
        return GenderTitle;
    }

    public void setGenderTitle(String GenderTitle) {
        this.GenderTitle = GenderTitle;
    }

    public String getDateModifiedUtc() {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc(String DateModifiedUtc) {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getIsLead() {
        return IsLead;
    }

    public void setIsLead(String IsLead) {
        this.IsLead = IsLead;
    }

    public ArrayList<String> getCustomerVehicleTrades() {
        return CustomerVehicleTrades;
    }

    public void setCustomerVehicleTrades(ArrayList<String> CustomerVehicleTrades) {
        this.CustomerVehicleTrades = CustomerVehicleTrades;
    }

    public String getDealershipId() {
        return DealershipId;
    }

    public void setDealershipId(String DealershipId) {
        this.DealershipId = DealershipId;
    }

    public String getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(String CreatedById) {
        this.CreatedById = CreatedById;
    }

    public ArrayList<CustomerAgreement> getVehicleTradeCustomerAgreements() {
        return VehicleTradeCustomerAgreements;
    }

    public void setVehicleTradeCustomerAgreements(ArrayList<CustomerAgreement> VehicleTradeCustomerAgreements) {
        this.VehicleTradeCustomerAgreements = VehicleTradeCustomerAgreements;
    }

    public boolean getIsAcceptedUsedCarPolicy() {
        return IsAcceptedUsedCarPolicy;
    }

    public void setIsAcceptedUsedCarPolicy(boolean IsAcceptedUsedCarPolicy) {
        this.IsAcceptedUsedCarPolicy = IsAcceptedUsedCarPolicy;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public SalesPersons[] getSalesPersons() {
        return SalesPersons;
    }

    public void setSalesPersons(SalesPersons[] SalesPersons) {
        this.SalesPersons = SalesPersons;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public boolean getIsTermConditionAccepted() {
        return IsTermConditionAccepted;
    }

    public void setIsTermConditionAccepted(boolean IsTermConditionAccepted) {
        this.IsTermConditionAccepted = IsTermConditionAccepted;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getDrivingLicenseNumber() {
        return DrivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String DrivingLicenseNumber) {
        this.DrivingLicenseNumber = DrivingLicenseNumber;
    }

    public String getLocationId() {
        return LocationId;
    }

    public void setLocationId(String LocationId) {
        this.LocationId = LocationId;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public String getLanguageTitle() {
        return LanguageTitle;
    }

    public void setLanguageTitle(String LanguageTitle) {
        this.LanguageTitle = LanguageTitle;
    }

    public String getGenderTypeId() {
        return GenderTypeId;
    }

    public void setGenderTypeId(String GenderTypeId) {
        this.GenderTypeId = GenderTypeId;
    }

    public String getDateCreatedUtc() {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc(String DateCreatedUtc) {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getLastScreenName() {
        return LastScreenName;
    }

    public void setLastScreenName(String LastScreenName) {
        this.LastScreenName = LastScreenName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(String LanguageId) {
        this.LanguageId = LanguageId;
    }

    public Location getLocation() {
        return Location;
    }

    public void setLocation(Location Location) {
        this.Location = Location;
    }

    @Override
    public String toString() {
        return "ClassPojo [DrivingLicenseExpiryDate = " + DrivingLicenseExpiryDate + ", Email = " + Email + ", GenderTitle = " + GenderTitle + ", DateModifiedUtc = " + DateModifiedUtc + ", IsLead = " + IsLead + ", CustomerVehicleTrades = " + CustomerVehicleTrades + ", DealershipId = " + DealershipId + ", CreatedById = " + CreatedById + ", VehicleTradeCustomerAgreements = " + VehicleTradeCustomerAgreements + ", IsAcceptedUsedCarPolicy = " + IsAcceptedUsedCarPolicy + ", ContactNumber = " + ContactNumber + ", SalesPersons = " + SalesPersons + ", DateOfBirth = " + DateOfBirth + ", IsTermConditionAccepted = " + IsTermConditionAccepted + ", FirstName = " + FirstName + ", DrivingLicenseNumber = " + DrivingLicenseNumber + ", LocationId = " + LocationId + ", MiddleName = " + MiddleName + ", LanguageTitle = " + LanguageTitle + ", GenderTypeId = " + GenderTypeId + ", DateCreatedUtc = " + DateCreatedUtc + ", LastScreenName = " + LastScreenName + ", Id = " + Id + ", LastName = " + LastName + ", LanguageId = " + LanguageId + ", Location = " + Location + "]";
    }
}