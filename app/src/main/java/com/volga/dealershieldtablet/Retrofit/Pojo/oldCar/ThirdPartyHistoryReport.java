package com.volga.dealershieldtablet.Retrofit.Pojo.oldCar;

/**
 * Created by ${Shailendra} on 20-08-2018.
 */
public class ThirdPartyHistoryReport {
    public String getThirdPartyHistoryReportId() {
        return ThirdPartyHistoryReportId;
    }

    public void setThirdPartyHistoryReportId(String thirdPartyHistoryReportId) {
        ThirdPartyHistoryReportId = thirdPartyHistoryReportId;
    }

    String ThirdPartyHistoryReportId;

    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    boolean IsVerified;
}
