package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity;

import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListUpload;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromiseUpload;

import java.util.ArrayList;

public class CoBuyerDetail {
  private String Id;
  private String FirstName;
  private String MiddleName;
  private String LastName;
  private int GenderTypeId;
  private String DateOfBirth;
  private String LanguageId;
  private String DrivingLicenseNumber;
  private String DrivingLicenseExpiryDate;
  private String Email;
  private String ContactNumber;
  private String Address1;
  private String Address2;
  private String City;
  private String Zipcode;
  private String State;
  private String Country;
  private boolean IsTermConditionAccepted;
  private String TestDriveDurationId;
  private boolean IsTestDriveTaken;

  public boolean getIsRemote() {
    return IsRemote;
  }

  public void setIsRemote(boolean isRemote) {
    IsRemote = isRemote;
  }

  private boolean IsRemote;
  private boolean IsAcceptedUsedCarPolicy;
  private ArrayList<VerbalPromiseUpload> VerbalPromises;
  public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> getCustomerAgreement() {
    return CustomerAgreement;
  }

  public void setCustomerAgreement(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> customerAgreement) {
    CustomerAgreement = customerAgreement;
  }

  ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> CustomerAgreement;


  public ArrayList<VerbalPromiseUpload> getVerbalPromises() {
    return VerbalPromises;
  }

  public void setVerbalPromises(ArrayList<VerbalPromiseUpload> verbalPromises) {
    VerbalPromises = verbalPromises;
  }

  public ArrayList<CheckListUpload> getCheckLists() {
    return CheckLists;
  }

  public void setCheckLists(ArrayList<CheckListUpload> checkLists) {
    CheckLists = checkLists;
  }

  private ArrayList<CheckListUpload> CheckLists;


 // Getter Methods 

  public String getId() {
    return Id;
  }

  public String getFirstName() {
    return FirstName;
  }

  public String getMiddleName() {
    return MiddleName;
  }

  public String getLastName() {
    return LastName;
  }

  public int getGenderTypeId() {
    return GenderTypeId;
  }

  public String getDateOfBirth() {
    return DateOfBirth;
  }

  public String getLanguageId() {
    return LanguageId;
  }

  public String getDrivingLicenseNumber() {
    return DrivingLicenseNumber;
  }

  public String getDrivingLicenseExpiryDate() {
    return DrivingLicenseExpiryDate;
  }

  public String getEmail() {
    return Email;
  }

  public String getContactNumber() {
    return ContactNumber;
  }

  public String getAddress1() {
    return Address1;
  }

  public String getAddress2() {
    return Address2;
  }

  public String getCity() {
    return City;
  }

  public String getZipcode() {
    return Zipcode;
  }

  public String getState() {
    return State;
  }

  public String getCountry() {
    return Country;
  }

  public boolean getIsTermConditionAccepted() {
    return IsTermConditionAccepted;
  }

  public String getTestDriveDurationId() {
    return TestDriveDurationId;
  }

  public boolean getIsTestDriveTaken() {
    return IsTestDriveTaken;
  }

  public boolean getIsAcceptedUsedCarPolicy() {
    return IsAcceptedUsedCarPolicy;
  }

 // Setter Methods 

  public void setId( String Id ) {
    this.Id = Id;
  }

  public void setFirstName( String FirstName ) {
    this.FirstName = FirstName;
  }

  public void setMiddleName( String MiddleName ) {
    this.MiddleName = MiddleName;
  }

  public void setLastName( String LastName ) {
    this.LastName = LastName;
  }

  public void setGenderTypeId( int GenderTypeId ) {
    this.GenderTypeId = GenderTypeId;
  }

  public void setDateOfBirth( String DateOfBirth ) {
    this.DateOfBirth = DateOfBirth;
  }

  public void setLanguageId( String LanguageId ) {
    this.LanguageId = LanguageId;
  }

  public void setDrivingLicenseNumber( String DrivingLicenseNumber ) {
    this.DrivingLicenseNumber = DrivingLicenseNumber;
  }

  public void setDrivingLicenseExpiryDate( String DrivingLicenseExpiryDate ) {
    this.DrivingLicenseExpiryDate = DrivingLicenseExpiryDate;
  }

  public void setEmail( String Email ) {
    this.Email = Email;
  }

  public void setContactNumber( String ContactNumber ) {
    this.ContactNumber = ContactNumber;
  }

  public void setAddress1( String Address1 ) {
    this.Address1 = Address1;
  }

  public void setAddress2( String Address2 ) {
    this.Address2 = Address2;
  }

  public void setCity( String City ) {
    this.City = City;
  }

  public void setZipcode( String Zipcode ) {
    this.Zipcode = Zipcode;
  }

  public void setState( String State ) {
    this.State = State;
  }

  public void setCountry( String Country ) {
    this.Country = Country;
  }

  public void setIsTermConditionAccepted( boolean IsTermConditionAccepted ) {
    this.IsTermConditionAccepted = IsTermConditionAccepted;
  }

  public void setTestDriveDurationId( String TestDriveDurationId ) {
    this.TestDriveDurationId = TestDriveDurationId;
  }

  public void setIsTestDriveTaken( boolean IsTestDriveTaken ) {
    this.IsTestDriveTaken = IsTestDriveTaken;
  }

  public void setIsAcceptedUsedCarPolicy( boolean IsAcceptedUsedCarPolicy ) {
    this.IsAcceptedUsedCarPolicy = IsAcceptedUsedCarPolicy;
  }
}