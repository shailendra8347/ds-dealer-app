package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;

import java.io.Serializable;

public class CustomerDetails implements Serializable {

    //@SerializedName("FirstName")
    //@Expose
    private String firstName;
    //@SerializedName("LastName")
    //@Expose
    private String lastName;
    ////@SerializedName("Gender")
    ////@Expose
    private String gender;
    //@SerializedName("DateOfBirth")
    //@Expose
    private String dateOfBirth;
    //@SerializedName("AddressLineOne")
    //@Expose
    private String addressLineOne;
    //@SerializedName("AddressLineTwo")
    //@Expose
    private String addressLineTwo;
    //@SerializedName("City")
    //@Expose
    private String city;
    //@SerializedName("State")
    //@Expose
    private String state;
    //@SerializedName("Country")
    //@Expose
    private String country;
    //@SerializedName("DLNumber")
    //@Expose
    private String dLNumber;
    //@SerializedName("DLExpiryDate")
    //@Expose
    private String dLExpiryDate;
    //@SerializedName("Email")
    //@Expose
    private String email;
    //@SerializedName("NoEmailChecked")
    //@Expose
    private String noEmailChecked;
    //@SerializedName("MobileNumber")
    //@Expose
    private String mobileNumber;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    private String Id;

    public boolean getIsRemote() {
        return IsRemote;
    }

    public void setIsRemote(boolean isRemote) {
        IsRemote = isRemote;
    }

    private boolean IsRemote;

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    private String CustomerId;

    public String getDrivingLicenceNumber() {
        return drivingLicenceNumber;
    }

    public void setDrivingLicenceNumber(String drivingLicenceNumber) {
        this.drivingLicenceNumber = drivingLicenceNumber;
    }

    private String drivingLicenceNumber;

    public SalesPersons getSalesPersons() {
        return salesPersons;
    }

    public void setSalesPersons(SalesPersons salesPersons) {
        this.salesPersons = salesPersons;
    }

    private SalesPersons salesPersons;

    public boolean isTermConditionAccepted() {
        return IsTermConditionAccepted;
    }

    public void setTermConditionAccepted(boolean termConditionAccepted) {
        IsTermConditionAccepted = termConditionAccepted;
    }

    private boolean IsTermConditionAccepted;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    private String zipCode;

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    private String middleName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddressLineOne() {
        return addressLineOne;
    }

    public void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDLNumber() {
        if (dLNumber == null) {
            dLNumber =" ";
        }
        return dLNumber;
    }

    public void setDLNumber(String dLNumber) {
        this.dLNumber = dLNumber;
    }

    public String getDLExpiryDate() {
        return dLExpiryDate;
    }

    public void setDLExpiryDate(String dLExpiryDate) {
        this.dLExpiryDate = dLExpiryDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoEmailChecked() {
        return noEmailChecked;
    }

    public void setNoEmailChecked(String noEmailChecked) {
        this.noEmailChecked = noEmailChecked;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

}
