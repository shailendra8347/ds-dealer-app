package com.volga.dealershieldtablet.Retrofit.Pojo;

public class CustomerVehicleDealAlerts {
    String AlertName;

    public String getAlertName() {
        return AlertName;
    }

    public void setAlertName(String alertName) {
        AlertName = alertName;
    }

    public String getAlertDesc() {
        return AlertDesc;
    }

    public void setAlertDesc(String alertDesc) {
        AlertDesc = alertDesc;
    }

    public String getDocId() {
        return DocId;
    }

    public void setDocId(String docId) {
        DocId = docId;
    }

    String AlertDesc;
    String DocId;
}
