package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity;

public class SalesPersons {
  private String SalesPersonId;
  private String FirstName;
  private String MiddleName;
  private String LastName;
  private String Email;
  private String Mobile;


 // Getter Methods 
 public String getUserId() {
   return UserId;
 }

  public void setUserId(String userId) {
    UserId = userId;
  }

  String  UserId;
  public String getSalesPersonId() {
    return SalesPersonId;
  }

  public String getFirstName() {
    return FirstName;
  }

  public String getMiddleName() {
    return MiddleName;
  }

  public String getLastName() {
    return LastName;
  }

  public String getEmail() {
    return Email;
  }

  public String getMobile() {
    return Mobile;
  }

 // Setter Methods 

  public void setSalesPersonId( String  SalesPersonId ) {
    this.SalesPersonId = SalesPersonId;
  }

  public void setFirstName( String FirstName ) {
    this.FirstName = FirstName;
  }

  public void setMiddleName( String MiddleName ) {
    this.MiddleName = MiddleName;
  }

  public void setLastName( String LastName ) {
    this.LastName = LastName;
  }

  public void setEmail( String Email ) {
    this.Email = Email;
  }

  public void setMobile( String Mobile ) {
    this.Mobile = Mobile;
  }
}