package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

public class ThirdPartyReportDocs {
    String DocId;

    public String getDocId() {
        return DocId;
    }

    public void setDocId(String docId) {
        DocId = docId;
    }

    public String getThirdPartyId() {
        return ThirdPartyId;
    }

    public void setThirdPartyId(String thirdPartyId) {
        ThirdPartyId = thirdPartyId;
    }

    String ThirdPartyId;

    public String getDocTypeName() {
        return DocTypeName;
    }

    public void setDocTypeName(String docTypeName) {
        DocTypeName = docTypeName;
    }

    String DocTypeName;
}
