package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.screenRevamping.pojo.CoBuyerDocs;
import com.volga.dealershieldtablet.screenRevamping.pojo.Location;

import java.util.ArrayList;

public class CoBuyers {
    private ArrayList<CustomerAgreement> CustomerAgreement;

    private String DrivingLicenseExpiryDate;

    private String Email;

    private String GenderTitle;

    private String DateModifiedUtc;

    private String DealershipId;

    private String TestDriveDurationTitle;

    private String CreatedById;

    private String IsTestDriveTaken;

    private boolean IsAcceptedUsedCarPolicy;

    private String ContactNumber;

    private String TestDriveDurationId;

    private String DateOfBirth;

    private boolean IsTermConditionAccepted;

    private String FirstName;

    private String DrivingLicenseNumber;

    private String MiddleName;

    private String LanguageTitle;

    private String GenderTypeId;
    public boolean isRemote() {
        return IsRemote;
    }

    public void setRemote(boolean remote) {
        IsRemote = remote;
    }

    private boolean IsRemote;

    private ArrayList<CoBuyerDocs> CoBuyerDocs;

    private String DateCreatedUtc;

    private String Id;

    private String LastName;

    private String LanguageId;

    private com.volga.dealershieldtablet.screenRevamping.pojo.Location Location;
    public com.volga.dealershieldtablet.screenRevamping.pojo.Location getLocation() {
        return Location;
    }

    public void setLocation(Location Location) {
        this.Location = Location;
    }

    public ArrayList<CustomerAgreement> getCustomerAgreement() {
        return CustomerAgreement;
    }

    public void setCustomerAgreement(ArrayList<CustomerAgreement> CustomerAgreement) {
        this.CustomerAgreement = CustomerAgreement;
    }

    public String getDrivingLicenseExpiryDate() {
        return DrivingLicenseExpiryDate;
    }

    public void setDrivingLicenseExpiryDate(String DrivingLicenseExpiryDate) {
        this.DrivingLicenseExpiryDate = DrivingLicenseExpiryDate;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getGenderTitle() {
        return GenderTitle;
    }

    public void setGenderTitle(String GenderTitle) {
        this.GenderTitle = GenderTitle;
    }

    public String getDateModifiedUtc() {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc(String DateModifiedUtc) {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getDealershipId() {
        return DealershipId;
    }

    public void setDealershipId(String DealershipId) {
        this.DealershipId = DealershipId;
    }

    public String getTestDriveDurationTitle() {
        return TestDriveDurationTitle;
    }

    public void setTestDriveDurationTitle(String TestDriveDurationTitle) {
        this.TestDriveDurationTitle = TestDriveDurationTitle;
    }

    public String getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(String CreatedById) {
        this.CreatedById = CreatedById;
    }

    public String getIsTestDriveTaken() {
        return IsTestDriveTaken;
    }

    public void setIsTestDriveTaken(String IsTestDriveTaken) {
        this.IsTestDriveTaken = IsTestDriveTaken;
    }

    public boolean getIsAcceptedUsedCarPolicy() {
        return IsAcceptedUsedCarPolicy;
    }

    public void setIsAcceptedUsedCarPolicy(boolean IsAcceptedUsedCarPolicy) {
        this.IsAcceptedUsedCarPolicy = IsAcceptedUsedCarPolicy;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public String getTestDriveDurationId() {
        return TestDriveDurationId;
    }

    public void setTestDriveDurationId(String TestDriveDurationId) {
        this.TestDriveDurationId = TestDriveDurationId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public boolean getIsTermConditionAccepted() {
        return IsTermConditionAccepted;
    }

    public void setIsTermConditionAccepted(boolean IsTermConditionAccepted) {
        this.IsTermConditionAccepted = IsTermConditionAccepted;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getDrivingLicenseNumber() {
        return DrivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String DrivingLicenseNumber) {
        this.DrivingLicenseNumber = DrivingLicenseNumber;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public String getLanguageTitle() {
        return LanguageTitle;
    }

    public void setLanguageTitle(String LanguageTitle) {
        this.LanguageTitle = LanguageTitle;
    }

    public String getGenderTypeId() {
        return GenderTypeId;
    }

    public void setGenderTypeId(String GenderTypeId) {
        this.GenderTypeId = GenderTypeId;
    }

    public ArrayList<CoBuyerDocs> getCoBuyerDocs() {
        return CoBuyerDocs;
    }

    public void setCoBuyerDocs(ArrayList<CoBuyerDocs> CoBuyerDocs) {
        this.CoBuyerDocs = CoBuyerDocs;
    }

    public String getDateCreatedUtc() {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc(String DateCreatedUtc) {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(String LanguageId) {
        this.LanguageId = LanguageId;
    }


    @Override
    public String toString() {
        return "ClassPojo [CustomerAgreement = " + CustomerAgreement + ", DrivingLicenseExpiryDate = " + DrivingLicenseExpiryDate + ", Email = " + Email + ", GenderTitle = " + GenderTitle + ", DateModifiedUtc = " + DateModifiedUtc + ", DealershipId = " + DealershipId + ", TestDriveDurationTitle = " + TestDriveDurationTitle + ", CreatedById = " + CreatedById + ", IsTestDriveTaken = " + IsTestDriveTaken + ", IsAcceptedUsedCarPolicy = " + IsAcceptedUsedCarPolicy + ", ContactNumber = " + ContactNumber + ", TestDriveDurationId = " + TestDriveDurationId + ", DateOfBirth = " + DateOfBirth + ", IsTermConditionAccepted = " + IsTermConditionAccepted + ", FirstName = " + FirstName + ", DrivingLicenseNumber = " + DrivingLicenseNumber + ", MiddleName = " + MiddleName + ", LanguageTitle = " + LanguageTitle + ", GenderTypeId = " + GenderTypeId + ", CoBuyerDocs = " + CoBuyerDocs + ", DateCreatedUtc = " + DateCreatedUtc + ", Id = " + Id + ", LastName = " + LastName + ", LanguageId = " + LanguageId + ", Location = " + Location + "]";
    }
}