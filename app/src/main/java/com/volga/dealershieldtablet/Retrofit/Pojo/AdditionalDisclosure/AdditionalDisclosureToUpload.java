package com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure;

public class AdditionalDisclosureToUpload {
    public String getAdditionalDisclosureId() {
        return AdditionalDisclosureId;
    }

    public void setAdditionalDisclosureId(String additionalDisclosureId) {
        AdditionalDisclosureId = additionalDisclosureId;
    }

    String AdditionalDisclosureId;

    public String getDisclosureType() {
        return DisclosureType;
    }

    public void setDisclosureType(String disclosureType) {
        DisclosureType = disclosureType;
    }

    String DisclosureType;
}
