package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity;

public class IncompleteDealList {
    private int CustomerVehicleTradeId, DealershipId;
    private String CustomerFirstName = null;
    private String CustomerLastName = null;
    private String VIN = null;
    private String Make = null;
    private String Model = null;
    private int ModelYear, DealStatusId;
    private String TradeDateTime;
    private String SalesPersonFirstName;
    private String SalesPersonLastName;
    private String SalesPersonEmail;
    private String DealStatus;
    private String DealershipName;
    private String LastScreenName;
    private boolean IsBuyerSigned;
    private boolean IsCoBuyerSigned;

    public boolean isBuyerSigned() {
        return IsBuyerSigned;
    }

    public void setBuyerSigned(boolean buyerSigned) {
        IsBuyerSigned = buyerSigned;
    }

    public boolean isCoBuyerSigned() {
        return IsCoBuyerSigned;
    }

    public void setCoBuyerSigned(boolean coBuyerSigned) {
        IsCoBuyerSigned = coBuyerSigned;
    }

    public boolean isCoBuyer() {
        return IsCoBuyer;
    }

    public void setCoBuyer(boolean coBuyer) {
        IsCoBuyer = coBuyer;
    }

    private boolean IsCoBuyer;

    public int getDealStatusId() {
        return DealStatusId;
    }

    public void setDealStatusId(int dealStatusId) {
        DealStatusId = dealStatusId;
    }

    public boolean isWebDeal() {
        return IsWebDeal;
    }

    public void setWebDeal(boolean webDeal) {
        IsWebDeal = webDeal;
    }

    public String getDealStatusText() {
        return DealStatusText;
    }

    public void setDealStatusText(String dealStatusText) {
        DealStatusText = dealStatusText;
    }

    private boolean IsWebDeal;
    private String DealStatusText;

    public int getDealershipId() {
        return DealershipId;
    }

    public void setDealershipId(int dealershipId) {
        DealershipId = dealershipId;
    }

    public String getDealershipName() {
        return DealershipName;
    }

    public void setDealershipName(String dealershipName) {
        DealershipName = dealershipName;
    }

    public String getLastScreenName() {
        return LastScreenName;
    }

    public void setLastScreenName(String lastScreenName) {
        LastScreenName = lastScreenName;
    }

    public String getLastAccessingTabletName() {
        return LastAccessingTabletName;
    }

    public void setLastAccessingTabletName(String lastAccessingTabletName) {
        LastAccessingTabletName = lastAccessingTabletName;
    }

    private String LastAccessingTabletName;

    public String getFrontImageUrl() {
        return FrontImageUrl;
    }

    public void setFrontImageUrl(String frontImageUrl) {
        FrontImageUrl = frontImageUrl;
    }

    private String FrontImageUrl;

    public String getSalesPersonUserId() {
        return SalesPersonUserId;
    }

    public void setSalesPersonUserId(String salesPersonUserId) {
        SalesPersonUserId = salesPersonUserId;
    }

    private String SalesPersonUserId;


    // Getter Methods

    public int getCustomerVehicleTradeId() {
        return CustomerVehicleTradeId;
    }

    public String getCustomerFirstName() {
        return CustomerFirstName;
    }

    public String getCustomerLastName() {
        return CustomerLastName;
    }

    public String getVIN() {
        return VIN;
    }

    public String getMake() {
        return Make;
    }

    public String getModel() {
        return Model;
    }

    public int getModelYear() {
        return ModelYear;
    }

    public String getTradeDateTime() {
        return TradeDateTime;
    }

    public String getSalesPersonFirstName() {
        return SalesPersonFirstName;
    }

    public String getSalesPersonLastName() {
        return SalesPersonLastName;
    }

    public String getSalesPersonEmail() {
        return SalesPersonEmail;
    }

    public String getDealStatus() {
        return DealStatus;
    }

    // Setter Methods

    public void setCustomerVehicleTradeId(int CustomerVehicleTradeId) {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public void setCustomerFirstName(String CustomerFirstName) {
        this.CustomerFirstName = CustomerFirstName;
    }

    public void setCustomerLastName(String CustomerLastName) {
        this.CustomerLastName = CustomerLastName;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public void setModelYear(int ModelYear) {
        this.ModelYear = ModelYear;
    }

    public void setTradeDateTime(String TradeDateTime) {
        this.TradeDateTime = TradeDateTime;
    }

    public void setSalesPersonFirstName(String SalesPersonFirstName) {
        this.SalesPersonFirstName = SalesPersonFirstName;
    }

    public void setSalesPersonLastName(String SalesPersonLastName) {
        this.SalesPersonLastName = SalesPersonLastName;
    }

    public void setSalesPersonEmail(String SalesPersonEmail) {
        this.SalesPersonEmail = SalesPersonEmail;
    }

    public void setDealStatus(String DealStatus) {
        this.DealStatus = DealStatus;
    }
}