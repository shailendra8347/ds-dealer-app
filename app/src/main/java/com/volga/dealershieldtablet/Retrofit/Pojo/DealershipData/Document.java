
package com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Document {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Path")
    @Expose
    private Object path;
    @SerializedName("FileName")
    @Expose
    private Object fileName;
    @SerializedName("Extension")
    @Expose
    private Object extension;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("DateCreatedUtc")
    @Expose
    private String dateCreatedUtc;
    @SerializedName("DateModifiedUtc")
    @Expose
    private String dateModifiedUtc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getPath() {
        return path;
    }

    public void setPath(Object path) {
        this.path = path;
    }

    public Object getFileName() {
        return fileName;
    }

    public void setFileName(Object fileName) {
        this.fileName = fileName;
    }

    public Object getExtension() {
        return extension;
    }

    public void setExtension(Object extension) {
        this.extension = extension;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getDateCreatedUtc() {
        return dateCreatedUtc;
    }

    public void setDateCreatedUtc(String dateCreatedUtc) {
        this.dateCreatedUtc = dateCreatedUtc;
    }

    public String getDateModifiedUtc() {
        return dateModifiedUtc;
    }

    public void setDateModifiedUtc(String dateModifiedUtc) {
        this.dateModifiedUtc = dateModifiedUtc;
    }

}
