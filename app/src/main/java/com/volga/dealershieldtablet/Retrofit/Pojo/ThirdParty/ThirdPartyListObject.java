package com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty;

import java.util.ArrayList;

public class ThirdPartyListObject
{
    private ArrayList<DealershipThirdParties> DealershipThirdParties;

    public ArrayList<DealershipThirdParties> getDealershipThirdParties ()
    {
        return DealershipThirdParties;
    }

    public void setDealershipThirdParties (ArrayList<DealershipThirdParties> DealershipThirdParties)
    {
        this.DealershipThirdParties = DealershipThirdParties;
    }

    @Override
    public String toString()
    {
        return "Class Pojo [DealershipThirdParties = "+DealershipThirdParties+"]";
    }
}