package com.volga.dealershieldtablet.Retrofit.Pojo;


import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations;

import java.util.ArrayList;

public class VehicleTradeAdditionalDisclosures {
    String AdditionalDisclosureId;
    String CustomerVehicleTradeId;
    String DisclosureType;

    public String getAdditionalTitle() {
        return AdditionalTitle;
    }

    public void setAdditionalTitle(String additionalTitle) {
        AdditionalTitle = additionalTitle;
    }

    String AdditionalTitle;

    public String getAdditionalDisclosureId() {
        return AdditionalDisclosureId;
    }

    public void setAdditionalDisclosureId(String additionalDisclosureId) {
        AdditionalDisclosureId = additionalDisclosureId;
    }

    public String getCustomerVehicleTradeId() {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId(String customerVehicleTradeId) {
        CustomerVehicleTradeId = customerVehicleTradeId;
    }

    public String getDisclosureType() {
        return DisclosureType;
    }

    public void setDisclosureType(String disclosureType) {
        DisclosureType = disclosureType;
    }


    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> getTranslations() {
        return Translations;
    }

    public void setTranslations(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations> translations) {
        Translations = translations;
    }

    ArrayList<Translations> Translations;
}
