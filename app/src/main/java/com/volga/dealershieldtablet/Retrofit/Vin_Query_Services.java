package com.volga.dealershieldtablet.Retrofit;

import com.volga.dealershieldtablet.Retrofit.Pojo.Vin.VINquery;
import com.volga.dealershieldtablet.Retrofit.Pojo.Vin.VinData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface Vin_Query_Services {

    @GET("/restxml.aspx")
    Call<String> get_vinData(@Query("accesscode") String access_code, @Query("reportType") int reportType, @Query("vin") String vin);

}

