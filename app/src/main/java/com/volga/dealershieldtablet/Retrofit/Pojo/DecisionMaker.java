package com.volga.dealershieldtablet.Retrofit.Pojo;

public class DecisionMaker {
    int index=-1;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isGoNext() {
        return goNext;
    }

    public void setGoNext(boolean goNext) {
        this.goNext = goNext;
    }

    boolean goNext;
}
