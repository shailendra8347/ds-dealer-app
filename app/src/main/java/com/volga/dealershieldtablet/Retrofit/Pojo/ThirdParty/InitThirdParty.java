package com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty;

public class InitThirdParty
{
    private String ThirdPartyID;

    public String getReportFetchDateTimeUTC() {
        return ReportFetchDateTimeUTC;
    }

    public void setReportFetchDateTimeUTC(String reportFetchDateTimeUTC) {
        ReportFetchDateTimeUTC = reportFetchDateTimeUTC;
    }

    private String ReportFetchDateTimeUTC;

    private String ThirdPartyReportRawId;

    private String LanguageId;

    public String getErrorsString() {
        return ErrorsString;
    }

    public void setErrorsString(String errorsString) {
        ErrorsString = errorsString;
    }

    private String ErrorsString;

    public String getThirdPartyID ()
    {
        return ThirdPartyID;
    }

    public void setThirdPartyID (String ThirdPartyID)
    {
        this.ThirdPartyID = ThirdPartyID;
    }

    public String getThirdPartyReportRawId ()
    {
        return ThirdPartyReportRawId;
    }

    public void setThirdPartyReportRawId (String ThirdPartyReportRawId)
    {
        this.ThirdPartyReportRawId = ThirdPartyReportRawId;
    }

    public String getLanguageId ()
    {
        return LanguageId;
    }

    public void setLanguageId (String LanguageId)
    {
        this.LanguageId = LanguageId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ThirdPartyID = "+ThirdPartyID+", ThirdPartyReportRawId = "+ThirdPartyReportRawId+", LanguageId = "+LanguageId+"]";
    }
}