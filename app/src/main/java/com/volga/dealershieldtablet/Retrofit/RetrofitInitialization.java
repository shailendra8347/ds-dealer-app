package com.volga.dealershieldtablet.Retrofit;

//import com.bugclipper.android.BugClipper;

import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.crypto.DecryptionImpl;
import com.volga.dealershieldtablet.utils.crypto.EncryptionImpl;
import com.volga.dealershieldtablet.utils.interceptor.DecryptionInterceptor;
import com.volga.dealershieldtablet.utils.interceptor.EncryptionInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import timber.log.Timber;

public class RetrofitInitialization {

    private static DS_Services ds_services, ds_services1 = null;
    private static Vin_Query_Services vin_query_services = null;
    final public static OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(180, TimeUnit.SECONDS)
            .writeTimeout(180, TimeUnit.SECONDS)
            .readTimeout(180, TimeUnit.SECONDS);

    public static DS_Services getDs_services() {
//        BugClipper.getOKHTTPBuilder(okHttpClient);
        if (ds_services == null) {
            // HttpLoggingInterceptor
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> Timber.i(message));
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


            /**
             * injection of interceptors to handle encryption and decryption
             */

            //Encryption Interceptor
            EncryptionInterceptor encryptionInterceptor = new EncryptionInterceptor(new EncryptionImpl());
            //Decryption Interceptor
            DecryptionInterceptor decryptionInterceptor = new DecryptionInterceptor(new DecryptionImpl());
            okHttpClient.addInterceptor(httpLoggingInterceptor);
//                    .addInterceptor(encryptionInterceptor)
//                    .addInterceptor(decryptionInterceptor);
            Retrofit retrofit = new Retrofit.Builder().client(okHttpClient.build())
                    .baseUrl(AppConstant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ds_services = retrofit.create(DS_Services.class);
        }
        return ds_services;
    }

    public static DS_Services getPdfServices() {
//        BugClipper.getOKHTTPBuilder(okHttpClient);
        if (ds_services1 == null) {
            Retrofit retrofit = new Retrofit.Builder().client(okHttpClient.build())
                    .baseUrl(AppConstant.BASE_URL1)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ds_services1 = retrofit.create(DS_Services.class);
        }
        return ds_services1;
    }


    public static Vin_Query_Services getVin_query_services() {

        if (vin_query_services == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .baseUrl(AppConstant.VIN_QUERY_BASE_URL)
                    .build();
         /*   Retrofit retrofit = new Retrofit.Builder().client(okHttpClient)
                    .baseUrl(AppConstant.VIN_QUERY_BASE_URL)
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
            vin_query_services = retrofit.create(Vin_Query_Services.class);*/
            vin_query_services = retrofit.create(Vin_Query_Services.class);
        }
        return vin_query_services;
    }


    ;

//    private static HttpLoggingInterceptor getInterceptor(){
//        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message -> Timber.i(message));
//        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//// set your desired log level
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        return logging;
//    }

}


