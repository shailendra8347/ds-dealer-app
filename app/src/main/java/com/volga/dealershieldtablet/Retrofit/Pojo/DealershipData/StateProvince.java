
package com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateProvince {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private Object name;
    @SerializedName("SortOrder")
    @Expose
    private Integer sortOrder;
    @SerializedName("Abbreviation")
    @Expose
    private String abbreviation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

}
