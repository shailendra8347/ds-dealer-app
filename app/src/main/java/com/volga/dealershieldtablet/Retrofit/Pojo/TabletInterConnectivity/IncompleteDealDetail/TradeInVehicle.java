package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

import java.util.ArrayList;

public class TradeInVehicle
{
    private String ExteriorColor;

    private String Mileage;

    private String DateModifiedUtc;

    private String Make;

    private String InteriorColor;

    private ArrayList<CustomerVehicleTradeDocs> TradeInVehicleDocs;

    private String CustomerVehicleTradeId;

    private String DateCreatedUtc;

    private String Model;

    private String TradeDateTime;

    private String VIN;

    private String ModelYear;

    private String Id;

    public String getExteriorColor ()
    {
        return ExteriorColor;
    }

    public void setExteriorColor (String ExteriorColor)
    {
        this.ExteriorColor = ExteriorColor;
    }

    public String getMileage ()
    {
        return Mileage;
    }

    public void setMileage (String Mileage)
    {
        this.Mileage = Mileage;
    }

    public String getDateModifiedUtc ()
    {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc (String DateModifiedUtc)
    {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getMake ()
    {
        return Make;
    }

    public void setMake (String Make)
    {
        this.Make = Make;
    }

    public String getInteriorColor ()
    {
        return InteriorColor;
    }

    public void setInteriorColor (String InteriorColor)
    {
        this.InteriorColor = InteriorColor;
    }

    public ArrayList<CustomerVehicleTradeDocs> getTradeInVehicleDocs ()
    {
        return TradeInVehicleDocs;
    }

    public void setTradeInVehicleDocs (ArrayList<CustomerVehicleTradeDocs> TradeInVehicleDocs)
    {
        this.TradeInVehicleDocs = TradeInVehicleDocs;
    }

    public String getCustomerVehicleTradeId ()
    {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId (String CustomerVehicleTradeId)
    {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public String getDateCreatedUtc ()
    {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc (String DateCreatedUtc)
    {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getModel ()
    {
        return Model;
    }

    public void setModel (String Model)
    {
        this.Model = Model;
    }

    public String getTradeDateTime ()
    {
        return TradeDateTime;
    }

    public void setTradeDateTime (String TradeDateTime)
    {
        this.TradeDateTime = TradeDateTime;
    }

    public String getVIN ()
    {
        return VIN;
    }

    public void setVIN (String VIN)
    {
        this.VIN = VIN;
    }

    public String getModelYear ()
    {
        return ModelYear;
    }

    public void setModelYear (String ModelYear)
    {
        this.ModelYear = ModelYear;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ExteriorColor = "+ExteriorColor+", Mileage = "+Mileage+", DateModifiedUtc = "+DateModifiedUtc+", Make = "+Make+", InteriorColor = "+InteriorColor+", TradeInVehicleDocs = "+TradeInVehicleDocs+", CustomerVehicleTradeId = "+CustomerVehicleTradeId+", DateCreatedUtc = "+DateCreatedUtc+", Model = "+Model+", TradeDateTime = "+TradeDateTime+", VIN = "+VIN+", ModelYear = "+ModelYear+", Id = "+Id+"]";
    }
}