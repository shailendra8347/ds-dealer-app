package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

public class VehicleTradeThirdPartyHistoryReports
{
    private String CustomerVehicleTradeId;

    private String Id;

    private String ThirdPartyHistoryReportId;

    public String getCustomerVehicleTradeId ()
    {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId (String CustomerVehicleTradeId)
    {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getThirdPartyHistoryReportId ()
    {
        return ThirdPartyHistoryReportId;
    }

    public void setThirdPartyHistoryReportId (String ThirdPartyHistoryReportId)
    {
        this.ThirdPartyHistoryReportId = ThirdPartyHistoryReportId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CustomerVehicleTradeId = "+CustomerVehicleTradeId+", Id = "+Id+", ThirdPartyHistoryReportId = "+ThirdPartyHistoryReportId+"]";
    }
}