package com.volga.dealershieldtablet.Retrofit;

import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.ForgotPassword;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.IncompleteDealDetailResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealList;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealPojo;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.TabUniqueName;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyListObject;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdPartyReportList;
import com.volga.dealershieldtablet.Retrofit.Pojo.UploadedDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehicleDataUploadResponse;
import com.volga.dealershieldtablet.screenRevamping.pojo.AddSalesperson;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckIDRequest;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.NoVerbalPromise;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.screenRevamping.pojo.SendIncompleteEmail;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface DS_Services {

    @FormUrlEncoded
    @POST("/token")
    Call<LoginData> login(@Field("grant_type") String grant_type, @Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("/token")
    Call<LoginData> refreshToken(@Field("grant_type") String grant_type, @Field("refresh_token") String refresh_token);


    @Headers("Content-Type: application/json")
    @GET("/api/User/GetUserDealerships")
    Call<DealershipData> get_dealerships(@Header("Authorization") String access_code);

    @POST("/api/Customer/uploadDocument")
    @Multipart
    Call<ImageUploadResponse> syncImagesToServer(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String CustomerVehicleTradeId, @Query("DocTypeName") String DocTypeName, @Query("CatupreDate") String CatupreDate, @Part MultipartBody.Part image, @Query("ImageFor") String ImageFor, @Query("IsAdditionalImage") boolean IsAdditionalImage, @Query("CatupreDateUTC") String CatupreDateUTC);

    @Headers("Content-Type: application/json")
    @POST("/api/IncompleteDeal/Send-report-email")
    Call<ImageUploadResponse> sendIncompleteDealEmail(@Header("Authorization") String access_code, @Body SendIncompleteEmail sendIncompleteEmail);
    /*/api/IncompleteDeal/Send-report-email*/

    @Headers("Content-Type: application/json")
    @POST("/api/Customer/SaveCustomerVehical")
    Call<ImageUploadResponse> uploadVehicleDetail(@Header("Authorization") String access_code, @Body VehicleDataUploadResponse response);

    @Headers("Content-Type: application/json")
    @POST("/api/Customer/SaveCustomer")
    Call<ImageUploadResponse> saveCustomer(@Header("Authorization") String access_code, @Body CheckIDRequest response);

    /*/api/User/add-sales-person*/
    @Headers("Content-Type: application/json")
    @POST("/api/User/add-sales-person")
    Call<ImageUploadResponse> addSalesPerson(@Header("Authorization") String access_code, @Body AddSalesperson addSalesperson);

    /**/
    @POST("/api/Customer/SendEmail")
    Call<ImageUploadResponse> sentMail(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String customerVehicleTradeId);

    @POST("/api/Customer/write-log")
    Call<ImageUploadResponse> writeLogs(@Header("Authorization") String access_code, @Body CustomLogs customLogs);

    //
    @Headers("Content-Type: application/json")
    @GET("/api/Customer/get-uploaded-documents")
    Call<ArrayList<UploadedDocs>> getUploadedDocs(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String CustomerVehicleTradeId);

    @Headers("Content-Type: application/json")
    @GET("/api/IncompleteDeal/AbortDeal")
    Call<Boolean> abortDeal(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String CustomerVehicleTradeId, @Query("TabletUniquenessId") String tabletUniquenessId);

    @Headers("Content-Type: application/json")
    @POST("/api/Account/ForgotPassword")
    Call<ForgotPassword> forgetPassword(@Query("Email") String Email);

    //    api/IncompleteDeal/GetTabletUniqueName
    @Headers("Content-Type: application/json")
    @GET("/api/IncompleteDeal/GetTabletUniqueName")
    Call<TabUniqueName> getUniqueTabName(@Header("Authorization") String access_code, @Query("deviceID") String Email);

    /*api/IncompleteDeal/GetIncompleteDealList*/
    @Headers("Content-Type: application/json")
    @GET("/api/IncompleteDeal/GetIncompleteDealList")
    Call<ArrayList<IncompleteDealList>> getRecordList(@Header("Authorization") String access_code, @Query("CompanyId") String CompanyId, @Query("TabletDateTime") String TabletDateTime, @Query("TabletUniquenessId") String tabletUniquenessId);
    /*api/IncompleteDeal/SaveIncompleteDeal*/

    @Headers("Content-Type: application/json")
    @POST("/api/IncompleteDeal/SaveIncompleteDeal")
    Call<ImageUploadResponse> uploadInCompleteDeal(@Header("Authorization") String access_code, @Body IncompleteDealPojo incompleteDealPojo);

    //
    @Headers("Content-Type: application/json")
    @POST("/api/IncompleteDeal/MarkSyncCompleted")
    Call<ImageUploadResponse> markIncompleteDealSync(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String customerVehicleTradeId, @Query("TabletUniquenessId") String tabletUniquenessId);

    @Headers("Content-Type: application/json")
    @POST("/api/IncompleteDeal/GetIncompleteDealData")
    Call<IncompleteDealDetailResponse> getIncompleteDealData(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String customerVehicleTradeId, @Query("TabletUniquenessId") String tabletUniquenessId);

    @Headers("Content-Type: application/json")
    @GET("/api/report/GetHistoryReportPdfBytes")
    Call<ThirdPartyReportBytes> getReportBytes(@Header("Authorization") String access_code, @Query("dealershipId") String dealershipId, @Query("VIN") String VIN, @Query("ThirdPartyID") String ThirdPartyID, @Query("lang") String lang);

    //api/Customer/get-sales-persons

    @Headers("Content-Type: application/json")
    @GET("/api/Customer/get-sales-persons")
    Call<ArrayList<SalesPersonList>> getSalesPerson(@Header("Authorization") String access_code, @Query("DealershipId") String DealershipId);

    @Headers("Content-Type: application/json")
    @GET("/api/User/get-dealership-VerbalPromise")
    Call<NoVerbalPromise> getVerbalPromise(@Header("Authorization") String access_code, @Query("DealershipId") String DealershipId);

    //
    @Headers("Content-Type: application/json")
    @GET("/api/User/get-dealership-checklist")
    Call<CheckListPojo> getCheckList(@Header("Authorization") String access_code, @Query("DealershipId") String DealershipId);

    //
    @Headers("Content-Type: application/json")
    @GET("/api/IncompleteDeal/CheckSaveAccess")
    Call<Boolean> getSaveAccess(@Header("Authorization") String access_code, @Query("CustomerVehicleTradeId") String customerVehicleTradeId, @Query("TabletUniquenessId") String tabletUniquenessId);

    ///api/User/get-dealership-ThirdParty

    @Headers("Content-Type: application/json")
    @GET("/api/User/get-dealership-ThirdParty")
    Call<ThirdPartyListObject> getDealerThirdParty(@Header("Authorization") String access_code, @Query("dealershipId") String dealershipId);

    @Headers("Content-Type: application/json")
    @GET("/api/report/get-history-report")
    Call<ResponseBody> getThirdPartyReports(@Header("Authorization") String access_code, @Query("dealershipId") String dealershipId, @Query("VIN") String VIN, @Query("ThirdPartyID") String ThirdPartyID);

    //api/report/get-Vehicle-history-disclosure-verification /api/report/get-Vehicle-condition-disclosure-verification?dealershipId=40335&VIN=1FDKF37G7RNB26735&lang=1
    @Headers("Content-Type: application/json")
    @GET("/api/report/get-Vehicle-history-disclosure-verification")
    Call<VehicleHistoryDisclosure> getVehicleHistoryDisclosure(@Header("Authorization") String access_code, @Query("dealershipId") String dealershipId, @Query("VIN") String VIN, @Query("lang") String lang, @Query("CarTypeId") String CarTypeId);

    @Headers("Content-Type: application/json")
    @GET("/api/report/get-Vehicle-condition-disclosure-verification")
    Call<VehicleConditionDisclosure> getVehicleConditionDisclosure(@Header("Authorization") String access_code, @Query("dealershipId") String dealershipId, @Query("VIN") String VIN, @Query("lang") String lang, @Query("CarTypeId") String CarTypeId);

    //?dealershipId=40335&VIN=yujk&lang=1&CarTypeId=1
    @Headers("Content-Type: application/json")
    @GET("/api/report/get-third-party-details")
    Call<ThirdPartyList> getThirdPartyList(@Header("Authorization") String access_code, @Query("lang") String lang, @Query("dealershipId") String dealershipId);

    @Headers("Content-Type: application/json")
    @GET("/api/report/get-additional-disclosure")
    Call<AdditionalDisclosure> getAdditionalDisclosures(@Header("Authorization") String access_code, @Query("VIN") String VIN, @Query("dealershipId") String dealershipId,@Query("disclosureType") String disclosureType);



    //api/report/InitiateThirdPartyReport
    @Headers("Content-Type: application/json")
    @GET("/api/report/InitiateThirdPartyReport")
    Call<InitiateThirdParty> initiateThirdParty(@Header("Authorization") String access_code, @Query("VIN") String VIN, @Query("dealershipId") String dealershipId);

    //    /api/report/GetHistoryReportRawPdfBytes GetHistoryReportRawPdfBytesList
    @Headers("Content-Type: application/json")
    @GET("/api/report/GetHistoryReportRawPdfBytes")
    Call<ThirdPartyReportBytes> getThirdPartyReportsBytes(@Header("Authorization") String access_code, @Query("thirdPartyReportRawId") String thirdPartyReportRawId);

    @Headers("Content-Type: application/json")
    @GET("/api/report/GetHistoryReportRawPdfBytesList")
    Call<ThirdPartyReportList> GetHistoryReportRawPdfBytesList(@Header("Authorization") String access_code, @Query("thirdPartyReportRawId") String thirdPartyReportRawId);


}

