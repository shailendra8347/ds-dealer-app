package com.volga.dealershieldtablet.Retrofit.Pojo.Vin;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "VINquery")
public class VINquery
{
    @Element(name = "Date")
    private String Date;

    @Element(name = "VIN")
    private VIN VIN;
    @Element(name = "Report_Type")
    private String Report_Type;
    @Element(name = "Version")
    private String Version;

    public String getDate ()
    {
        return Date;
    }

    public void setDate (String Date)
    {
        this.Date = Date;
    }

    public VIN getVIN ()
    {
        return VIN;
    }

    public void setVIN (VIN VIN)
    {
        this.VIN = VIN;
    }

    public String getReport_Type ()
    {
        return Report_Type;
    }

    public void setReport_Type (String Report_Type)
    {
        this.Report_Type = Report_Type;
    }

    public String getVersion ()
    {
        return Version;
    }

    public void setVersion (String Version)
    {
        this.Version = Version;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Date = "+Date+", VIN = "+VIN+", Report_Type = "+Report_Type+", Version = "+Version+"]";
    }
}
