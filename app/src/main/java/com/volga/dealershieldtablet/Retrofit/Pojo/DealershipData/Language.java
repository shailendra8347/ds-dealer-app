
package com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Language {

    @SerializedName("LanguageName")
    @Expose
    private String languageName;
    @SerializedName("LanguageId")
    @Expose
    private int languageId;
    @SerializedName("DisplayName")
    @Expose
    private String displayName;
    @SerializedName("Code")
    @Expose
    private Object code;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

}
