
package com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehiclePhotoType;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckList;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromise;

import java.util.ArrayList;
import java.util.List;

public class Dealership {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("DealershipName")
    @Expose
    private String dealershipName;
    @SerializedName("DealershipLicenseNumber")
    @Expose
    private Object dealershipLicenseNumber;
    @SerializedName("DocumentId")
    @Expose
    private Object documentId;
    @SerializedName("Location")
    @Expose
    private Location location;
    @SerializedName("FooterContent")
    @Expose
    private Object footerContent;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;
    @SerializedName("DateCreatedUtc")
    @Expose
    private String dateCreatedUtc;
    @SerializedName("DateModifiedUtc")
    @Expose
    private String dateModifiedUtc;
    @SerializedName("StateId")
    @Expose
    private Integer stateId;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("Document")
    @Expose
    private Document document;
    @SerializedName("UserCount")
    @Expose
    private Integer userCount;
    @SerializedName("IsSelected")
    @Expose
    private boolean isSelected;
    @SerializedName("IsTestDriveMandatory")
    @Expose
    private boolean isTestDriveMandatory;

    private ArrayList<VehiclePhotoType> DealershipVehiclePhotoTypes;

    public boolean isAllowCheckID() {
        return AllowCheckID;
    }

    public void setAllowCheckID(boolean allowCheckID) {
        AllowCheckID = allowCheckID;
    }

    private boolean AllowCheckID;

    public boolean isThirdPartyMultiSign() {
        return IsThirdPartyMultiSign;
    }

    public void setThirdPartyMultiSign(boolean thirdPartyMultiSign) {
        IsThirdPartyMultiSign = thirdPartyMultiSign;
    }

    private boolean IsThirdPartyMultiSign;

    public boolean isViewThirdPartyReportMandatory() {
        return IsViewThirdPartyReportMandatory;
    }

    public void setViewThirdPartyReportMandatory(boolean viewThirdPartyReportMandatory) {
        IsViewThirdPartyReportMandatory = viewThirdPartyReportMandatory;
    }

    private boolean IsViewThirdPartyReportMandatory;

    public boolean isUseThirdPartyAPI() {
        return UseThirdPartyAPI;
    }

    public void setUseThirdPartyAPI(boolean useThirdPartyAPI) {
        UseThirdPartyAPI = useThirdPartyAPI;
    }

    private boolean UseThirdPartyAPI;

    public boolean isAIFeatureEnabled() {
        return UseAILogic;
    }

    public void setAIFeatureEnabled(boolean UseAILogic) {
        UseAILogic = UseAILogic;
    }

    private boolean UseAILogic;
    private boolean IsCaptureCustomerPhotoOnSign;
    private boolean IsUsedCarOnly;

    public boolean isProcessTradeIn() {
        return ProcessTradeIn;
    }

    public void setProcessTradeIn(boolean processTradeIn) {
        ProcessTradeIn = processTradeIn;
    }

    private boolean ProcessTradeIn, TakePhotoOfProofOfInsurance, TakePhotoOfVehicle;

    public boolean isCaptureCustomerPhotoOnSign() {
        return IsCaptureCustomerPhotoOnSign;
    }

    public void setCaptureCustomerPhotoOnSign(boolean captureCustomerPhotoOnSign) {
        IsCaptureCustomerPhotoOnSign = captureCustomerPhotoOnSign;
    }

    public boolean isCaptureScreenOnSign() {
        return IsCaptureScreenOnSign;
    }

    public void setCaptureScreenOnSign(boolean captureScreenOnSign) {
        IsCaptureScreenOnSign = captureScreenOnSign;
    }

    private boolean IsCaptureScreenOnSign;


    @SerializedName("Languages")
    @Expose

    private List<Language> languages = null;

    public List<String> getAlerts() {
        return Alerts;
    }

    public void setAlerts(List<String> alerts) {
        Alerts = alerts;
    }

    private List<String> Alerts;

    private ArrayList<com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromise> VerbalPromise;

    public ArrayList<VerbalPromise> getVerbalPromise() {
        return VerbalPromise;
    }

    public void setVerbalPromise(ArrayList<VerbalPromise> VerbalPromise) {
        this.VerbalPromise = VerbalPromise;
    }

    private ArrayList<com.volga.dealershieldtablet.screenRevamping.pojo.CheckList> CheckList;

    public ArrayList<CheckList> getCheckList() {
        return CheckList;
    }

    public void setCheckList(ArrayList<CheckList> CheckList) {
        this.CheckList = CheckList;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDealershipName() {
        return dealershipName;
    }

    public void setDealershipName(String dealershipName) {
        this.dealershipName = dealershipName;
    }

    public Object getDealershipLicenseNumber() {
        return dealershipLicenseNumber;
    }

    public void setDealershipLicenseNumber(Object dealershipLicenseNumber) {
        this.dealershipLicenseNumber = dealershipLicenseNumber;
    }

    public Object getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Object documentId) {
        this.documentId = documentId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Object getFooterContent() {
        return footerContent;
    }

    public void setFooterContent(Object footerContent) {
        this.footerContent = footerContent;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getDateCreatedUtc() {
        return dateCreatedUtc;
    }

    public void setDateCreatedUtc(String dateCreatedUtc) {
        this.dateCreatedUtc = dateCreatedUtc;
    }

    public String getDateModifiedUtc() {
        return dateModifiedUtc;
    }

    public void setDateModifiedUtc(String dateModifiedUtc) {
        this.dateModifiedUtc = dateModifiedUtc;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean getIsTestDriveMandatory() {
        return isTestDriveMandatory;
    }

    public void setIsTestDriveMandatory(boolean isTestDriveMandatory) {
        this.isTestDriveMandatory = isTestDriveMandatory;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public boolean isUsedCarOnly() {
        return IsUsedCarOnly;
    }

    public void setUsedCarOnly(boolean usedCarOnly) {
        IsUsedCarOnly = usedCarOnly;
    }

    public boolean isTakePhotoOfProofOfInsurance() {
        return TakePhotoOfProofOfInsurance;
    }

    public void setTakePhotoOfProofOfInsurance(boolean takePhotoOfProofOfInsurance) {
        TakePhotoOfProofOfInsurance = takePhotoOfProofOfInsurance;
    }

    public boolean isTakePhotoOfVehicle() {
        return TakePhotoOfVehicle;
    }

    public void setTakePhotoOfVehicle(boolean takePhotoOfVehicle) {
        TakePhotoOfVehicle = takePhotoOfVehicle;
    }

    public ArrayList<VehiclePhotoType> getDealershipVehiclePhotoTypes() {
        return DealershipVehiclePhotoTypes;
    }

    public void setDealershipVehiclePhotoTypes(ArrayList<VehiclePhotoType> dealershipVehiclePhotoTypes) {
        DealershipVehiclePhotoTypes = dealershipVehiclePhotoTypes;
    }
}
