package com.volga.dealershieldtablet.Retrofit.Pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehiclePhotoType {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("DealershipId")
    @Expose
    private Integer dealershipId;
    @SerializedName("VehiclePhotoTypeId")
    @Expose
    private Integer vehiclePhotoTypeId;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("SampleUrl")
    @Expose
    private String sampleUrl;
    @SerializedName("DocumentType")
    @Expose
    private String documentType;
    @SerializedName("DocTypeTitle")
    @Expose
    private String docTypeTitle;
    @SerializedName("DocTypeHint")
    @Expose
    private String docTypeHint;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDealershipId() {
        return dealershipId;
    }

    public void setDealershipId(Integer dealershipId) {
        this.dealershipId = dealershipId;
    }

    public Integer getVehiclePhotoTypeId() {
        return vehiclePhotoTypeId;
    }

    public void setVehiclePhotoTypeId(Integer vehiclePhotoTypeId) {
        this.vehiclePhotoTypeId = vehiclePhotoTypeId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getSampleUrl() {
        return sampleUrl;
    }

    public void setSampleUrl(String sampleUrl) {
        this.sampleUrl = sampleUrl;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocTypeTitle() {
        return docTypeTitle;
    }

    public void setDocTypeTitle(String docTypeTitle) {
        this.docTypeTitle = docTypeTitle;
    }

    public String getDocTypeHint() {
        return docTypeHint;
    }

    public void setDocTypeHint(String docTypeHint) {
        this.docTypeHint = docTypeHint;
    }

}