package com.volga.dealershieldtablet.Retrofit.Pojo;

import java.io.Serializable;

/**
 * Created by ${Shailendra} on 07-09-2018.
 */
public class ConditionDisclosures implements Serializable {
    String VehicleConditionDisclosureId;

    public String getVehicleConditionDisclosureId() {
        return VehicleConditionDisclosureId;
    }

    public void setVehicleConditionDisclosureId(String vehicleConditionDisclosureId) {
        VehicleConditionDisclosureId = vehicleConditionDisclosureId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    String value;
    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    boolean IsVerified;
}
