package com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty;

public class DealershipThirdParties
{
    private String UserId;

    private String IsActive;

    private ThirdPartyHistoryReport ThirdPartyHistoryReport;

    private String DateCreatedUtc;

    private String DealershipDetailId;

    private String DateModifiedUtc;

    private String Id;

    private String ThirdPartyId;

    private String Password;

    public String getUserId ()
    {
        return UserId;
    }

    public void setUserId (String UserId)
    {
        this.UserId = UserId;
    }

    public String getIsActive ()
    {
        return IsActive;
    }

    public void setIsActive (String IsActive)
    {
        this.IsActive = IsActive;
    }

    public ThirdPartyHistoryReport getThirdPartyHistoryReport ()
    {
        return ThirdPartyHistoryReport;
    }

    public void setThirdPartyHistoryReport (ThirdPartyHistoryReport ThirdPartyHistoryReport)
    {
        this.ThirdPartyHistoryReport = ThirdPartyHistoryReport;
    }

    public String getDateCreatedUtc ()
    {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc (String DateCreatedUtc)
    {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getDealershipDetailId ()
    {
        return DealershipDetailId;
    }

    public void setDealershipDetailId (String DealershipDetailId)
    {
        this.DealershipDetailId = DealershipDetailId;
    }

    public String getDateModifiedUtc ()
    {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc (String DateModifiedUtc)
    {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getThirdPartyId ()
    {
        return ThirdPartyId;
    }

    public void setThirdPartyId (String ThirdPartyId)
    {
        this.ThirdPartyId = ThirdPartyId;
    }

    public String getPassword ()
    {
        return Password;
    }

    public void setPassword (String Password)
    {
        this.Password = Password;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [UserId = "+UserId+", IsActive = "+IsActive+", ThirdPartyHistoryReport = "+ThirdPartyHistoryReport+", DateCreatedUtc = "+DateCreatedUtc+", DealershipDetailId = "+DealershipDetailId+", DateModifiedUtc = "+DateModifiedUtc+", Id = "+Id+", ThirdPartyId = "+ThirdPartyId+", Password = "+Password+"]";
    }
}
	