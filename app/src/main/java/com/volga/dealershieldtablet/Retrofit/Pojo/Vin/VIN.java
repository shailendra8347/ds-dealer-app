package com.volga.dealershieldtablet.Retrofit.Pojo.Vin;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Root;

@Root(name = "VIN")
public class VIN
{
    @Element(name = "Status")
    private String Status;
    @Element(name = "Number")
    private String Number;
    @ElementArray(name = "Vehicle")
    private Vehicle[] Vehicle;

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public String getNumber ()
    {
        return Number;
    }

    public void setNumber (String Number)
    {
        this.Number = Number;
    }

    public Vehicle[] getVehicle ()
    {
        return Vehicle;
    }

    public void setVehicle (Vehicle[] Vehicle)
    {
        this.Vehicle = Vehicle;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Status = "+Status+", Number = "+Number+", Vehicle = "+Vehicle+"]";
    }
}