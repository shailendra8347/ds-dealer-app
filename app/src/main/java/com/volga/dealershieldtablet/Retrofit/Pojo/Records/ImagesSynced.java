package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import java.io.File;

/**
 * Created by ${Shailendra} on 13-11-2018.
 */
public class ImagesSynced {
    private String name;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    private File file;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

   private boolean isSynced;
}
