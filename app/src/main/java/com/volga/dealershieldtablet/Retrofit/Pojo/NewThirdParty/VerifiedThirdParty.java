package com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty;

public class VerifiedThirdParty
{
    private String LogoUrl;

    private String Title;

    private String Id;

    private String IsVerified;

    private String Result;

    public String getLogoUrl ()
    {
        return LogoUrl;
    }

    public void setLogoUrl (String LogoUrl)
    {
        this.LogoUrl = LogoUrl;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getIsVerified ()
    {
        return IsVerified;
    }

    public void setIsVerified (String IsVerified)
    {
        this.IsVerified = IsVerified;
    }

    public String getResult ()
    {
        return Result;
    }

    public void setResult (String Result)
    {
        this.Result = Result;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LogoUrl = "+LogoUrl+", Title = "+Title+", Id = "+Id+", IsVerified = "+IsVerified+", Result = "+Result+"]";
    }
}
			
			