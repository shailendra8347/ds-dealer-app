package com.volga.dealershieldtablet.Retrofit.Pojo.Vin;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Item")
public class Item {

    @Element(name = "Value")
    private String Value;
    @Element(name = "Key")
    private String Key;
    @Element(name = "Unit")
    private String Unit;

    public String getValue() {
        return Value;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String Key) {
        this.Key = Key;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String Unit) {
        this.Unit = Unit;
    }

    @Override
    public String toString() {
        return "ClassPojo [Value = " + Value + ", Key = " + Key + ", Unit = " + Unit + "]";
    }
}