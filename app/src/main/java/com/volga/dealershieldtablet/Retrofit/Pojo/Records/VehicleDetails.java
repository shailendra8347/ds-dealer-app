
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;

import java.util.ArrayList;
import java.util.Date;

public class VehicleDetails {

    @SerializedName("TypeOfPurchase")
    @Expose
    private String typeOfPurchase;
    @SerializedName("VINNumber")
    @Expose
    private String vINNumber;
    @SerializedName("CarMake")
    @Expose
    private String carMake;
    @SerializedName("CarModel")
    @Expose
    private String carModel;
    @SerializedName("CarYear")
    @Expose
    private String carYear;
//    @SerializedName("Mileage")
//    @Expose
    private String mileage;

    private boolean takeFront;
    private boolean takeBack;
    private boolean takeRight;
    private boolean takeWindow;
    private boolean takeOdometer;

    public boolean isNormalFlow() {
        return normalFlow;
    }

    public void setNormalFlow(boolean normalFlow) {
        this.normalFlow = normalFlow;
    }

    private boolean normalFlow;

    public boolean isTakeFront() {
        return takeFront;
    }

    public void setTakeFront(boolean takeFront) {
        this.takeFront = takeFront;
    }

    public boolean isTakeBack() {
        return takeBack;
    }

    public void setTakeBack(boolean takeBack) {
        this.takeBack = takeBack;
    }

    public boolean isTakeRight() {
        return takeRight;
    }

    public void setTakeRight(boolean takeRight) {
        this.takeRight = takeRight;
    }

    public boolean isTakeWindow() {
        return takeWindow;
    }

    public void setTakeWindow(boolean takeWindow) {
        this.takeWindow = takeWindow;
    }

    public boolean isTakeOdometer() {
        return takeOdometer;
    }

    public void setTakeOdometer(boolean takeOdometer) {
        this.takeOdometer = takeOdometer;
    }

    public boolean isTakeLeft() {
        return takeLeft;
    }

    public void setTakeLeft(boolean takeLeft) {
        this.takeLeft = takeLeft;
    }

    private boolean takeLeft;

    private boolean carFaxReportAvailable;

    public boolean isOfflineData() {
        return isOfflineData;
    }

    public void setOfflineData(boolean offlineData) {
        isOfflineData = offlineData;
    }

    private boolean isOfflineData;
    private boolean isConditionVerified;

    public boolean isConditionVerified() {
        return isConditionVerified;
    }

    public void setConditionVerified(boolean conditionVerified) {
        isConditionVerified = conditionVerified;
    }

    public boolean isHistoryVerified() {
        return isHistoryVerified;
    }

    public void setHistoryVerified(boolean historyVerified) {
        isHistoryVerified = historyVerified;
    }

    private boolean isHistoryVerified;

    public boolean isDataDownloading() {
        return isDataDownloading;
    }

    public void setDataDownloading(boolean dataDownloading) {
        isDataDownloading = dataDownloading;
    }

    private boolean isDataDownloading;

    public boolean isCarFaxReportAvailable() {
        return carFaxReportAvailable;
    }

    public void setCarFaxReportAvailable(boolean carFaxReportAvailable) {
        this.carFaxReportAvailable = carFaxReportAvailable;
    }

    public boolean isAutoCheckReportAvailable() {
        return autoCheckReportAvailable;
    }

    public void setAutoCheckReportAvailable(boolean autoCheckReportAvailable) {
        this.autoCheckReportAvailable = autoCheckReportAvailable;
    }

    private boolean autoCheckReportAvailable;

    public String getSafetyRecall() {
        return safetyRecall;
    }

    public void setSafetyRecall(String safetyRecall) {
        this.safetyRecall = safetyRecall;
    }

    public ArrayList<WarningAlerts> getWarningAlerts() {
        return warningAlerts;
    }

    public void setWarningAlerts(ArrayList<WarningAlerts> warningAlerts) {
        this.warningAlerts = warningAlerts;
    }

    ArrayList<WarningAlerts> warningAlerts;

    private String safetyRecall;
    private String title1;
    private String title2;
    private String title3;
    private String title4;

    private String buyerFistName;
    private String buyerLastName;
    private String coFirstName;

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    private String salesId;

    public AdditionalDisclosure getAdditionalDisclosure() {
        return additionalDisclosure;
    }

    public void setAdditionalDisclosure(AdditionalDisclosure additionalDisclosure) {
        this.additionalDisclosure = additionalDisclosure;
    }

    private AdditionalDisclosure additionalDisclosure;

    public AdditionalDisclosure getAdditionalHistoryDisclosure() {
        return additionalHistoryDisclosure;
    }

    public void setAdditionalHistoryDisclosure(AdditionalDisclosure additionalHistoryDisclosure) {
        this.additionalHistoryDisclosure = additionalHistoryDisclosure;
    }

    private AdditionalDisclosure additionalHistoryDisclosure;

    public CustomerVehicleDealAlerts getCarFaxAvailable() {
        return carFaxAvailable;
    }

    public void setCarFaxAvailable(CustomerVehicleDealAlerts carFaxAvailable) {
        this.carFaxAvailable = carFaxAvailable;
    }

    public CustomerVehicleDealAlerts getAutoCheckAvailable() {
        return autoCheckAvailable;
    }

    public void setAutoCheckAvailable(CustomerVehicleDealAlerts autoCheckAvailable) {
        this.autoCheckAvailable = autoCheckAvailable;
    }

    private CustomerVehicleDealAlerts carFaxAvailable, autoCheckAvailable;

    public ArrayList<ThirdPartyReportDocs> getThirdPartyReportDocs() {
        return thirdPartyReportDocs;
    }

    public void setThirdPartyReportDocs(ArrayList<ThirdPartyReportDocs> thirdPartyReportDocs) {
        this.thirdPartyReportDocs = thirdPartyReportDocs;
    }

    private ArrayList<ThirdPartyReportDocs> thirdPartyReportDocs;

    public String getBuyerFistName() {
        return buyerFistName;
    }

    public void setBuyerFistName(String buyerFistName) {
        this.buyerFistName = buyerFistName;
    }

    public String getBuyerLastName() {
        return buyerLastName;
    }

    public void setBuyerLastName(String buyerLastName) {
        this.buyerLastName = buyerLastName;
    }

    public String getCoFirstName() {
        return coFirstName;
    }

    public void setCoFirstName(String coFirstName) {
        this.coFirstName = coFirstName;
    }

    public String getCoLastName() {
        return coLastName;
    }

    public void setCoLastName(String coLastName) {
        this.coLastName = coLastName;
    }

    private String coLastName;

    public String getTitle1() {
        return title1;
    }


    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public String getTitle4() {
        return title4;
    }

    public void setTitle4(String title4) {
        this.title4 = title4;
    }

    public String getTitle5() {
        return title5;
    }

    public void setTitle5(String title5) {
        this.title5 = title5;
    }

    private String title5;

    private ArrayList<HistoryListPojo> usedVehicleHistory;

    public ArrayList<HistoryListPojo> getUsedVehicleCondition() {
        return usedVehicleCondition;
    }

    public void setUsedVehicleCondition(ArrayList<HistoryListPojo> usedVehicleCondition) {
        this.usedVehicleCondition = usedVehicleCondition;
    }

    private ArrayList<HistoryListPojo> usedVehicleCondition;
    private ArrayList<HistoryListPojo> conditionSelection;
    private ArrayList<HistoryListPojo> historySelection;
    private ArrayList<HistoryListPojo> coHistorySelection;
    private ArrayList<HistoryListPojo> coConditionSelection;

    public ArrayList<HistoryListPojo> getCoHistorySelection() {
        return coHistorySelection;
    }

    public void setCoHistorySelection(ArrayList<HistoryListPojo> coHistorySelection) {
        this.coHistorySelection = coHistorySelection;
    }

    public ArrayList<HistoryListPojo> getCoConditionSelection() {
        return coConditionSelection;
    }

    public void setCoConditionSelection(ArrayList<HistoryListPojo> coConditionSelection) {
        this.coConditionSelection = coConditionSelection;
    }

    public ArrayList<HistoryListPojo> getCoReportSelection() {
        return coReportSelection;
    }

    public void setCoReportSelection(ArrayList<HistoryListPojo> coReportSelection) {
        this.coReportSelection = coReportSelection;
    }

    private ArrayList<HistoryListPojo> coReportSelection;

    public ArrayList<HistoryListPojo> getConditionSelection() {
        return conditionSelection;
    }

    public void setConditionSelection(ArrayList<HistoryListPojo> conditionSelection) {
        this.conditionSelection = conditionSelection;
    }

    public ArrayList<HistoryListPojo> getHistorySelection() {
        return historySelection;
    }

    public void setHistorySelection(ArrayList<HistoryListPojo> historySelection) {
        this.historySelection = historySelection;
    }

    public ArrayList<HistoryListPojo> getReportSelection() {
        return reportSelection;
    }

    public void setReportSelection(ArrayList<HistoryListPojo> reportSelection) {
        this.reportSelection = reportSelection;
    }

    private ArrayList<HistoryListPojo> reportSelection;

    public ArrayList<HistoryListPojo> getUsedVehicleHistory() {
        return usedVehicleHistory;
    }

    public void setUsedVehicleHistory(ArrayList<HistoryListPojo> usedVehicleHistory) {
        this.usedVehicleHistory = usedVehicleHistory;
    }

    public ArrayList<HistoryListPojo> getUsedVehicleReport() {
        return usedVehicleReport;
    }

    public void setUsedVehicleReport(ArrayList<HistoryListPojo> usedVehicleReport) {
        this.usedVehicleReport = usedVehicleReport;
    }

    private ArrayList<HistoryListPojo> usedVehicleReport;

    public ArrayList<CustomerVehicleDealAlerts> getCustomerVehicleDealAlerts() {
        return customerVehicleDealAlerts;
    }

    public void setCustomerVehicleDealAlerts(ArrayList<CustomerVehicleDealAlerts> customerVehicleDealAlerts) {
        this.customerVehicleDealAlerts = customerVehicleDealAlerts;
    }

    private ArrayList<CustomerVehicleDealAlerts> customerVehicleDealAlerts;
    private String dmvFrontTime;
    private String carFrontTime;
    private String carRearTime;
    private String carLeftTime;
    private String carStickerTime;
    private String carOdoMeterTime;

    public String getAdditional1() {
        return Additional1;
    }

    public void setAdditional1(String additional1) {
        Additional1 = additional1;
    }

    public String getAdditional2() {
        return Additional2;
    }

    public void setAdditional2(String additional2) {
        Additional2 = additional2;
    }

    public String getAdditional3() {
        return Additional3;
    }

    public void setAdditional3(String additional3) {
        Additional3 = additional3;
    }

    public String getAdditional4() {
        return Additional4;
    }

    public void setAdditional4(String additional4) {
        Additional4 = additional4;
    }

    public String getAdditional5() {
        return Additional5;
    }

    public void setAdditional5(String additional5) {
        Additional5 = additional5;
    }

    private String Additional1;
    private String Additional2;
    private String Additional3;
    private String Additional4;
    private String Additional5;

    public String getInsurenceTime() {
        return insurenceTime;
    }

    public void setInsurenceTime(String insurenceTime) {
        this.insurenceTime = insurenceTime;
    }

    private String insurenceTime;

    private Date historyDate1;

    public Date getHistoryDate1() {
        return historyDate1;
    }

    public void setHistoryDate1(Date historyDate1) {
        this.historyDate1 = historyDate1;
    }

    public Date getHistoryDate2() {
        return historyDate2;
    }

    public void setHistoryDate2(Date historyDate2) {
        this.historyDate2 = historyDate2;
    }

    private Date historyDate2;

    public String getDmvFrontTime() {
        return dmvFrontTime;
    }

    public void setDmvFrontTime(String dmvFrontTime) {
        this.dmvFrontTime = dmvFrontTime;
    }

    public String getCarFrontTime() {
        return carFrontTime;
    }

    public void setCarFrontTime(String carFrontTime) {
        this.carFrontTime = carFrontTime;
    }

    public String getCarRearTime() {
        return carRearTime;
    }

    public void setCarRearTime(String carRearTime) {
        this.carRearTime = carRearTime;
    }

    public String getCarLeftTime() {
        return carLeftTime;
    }

    public void setCarLeftTime(String carLeftTime) {
        this.carLeftTime = carLeftTime;
    }

    public String getCarStickerTime() {
        return carStickerTime;
    }

    public void setCarStickerTime(String carStickerTime) {
        this.carStickerTime = carStickerTime;
    }

    public String getCarOdoMeterTime() {
        return carOdoMeterTime;
    }

    public void setCarOdoMeterTime(String carOdoMeterTime) {
        this.carOdoMeterTime = carOdoMeterTime;
    }

    public String getCarRightTime() {
        return carRightTime;
    }

    public void setCarRightTime(String carRightTime) {
        this.carRightTime = carRightTime;
    }

    String carRightTime;

    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    private String stockNumber;

    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    public void setTypeOfVehicle(String typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }

    private String typeOfVehicle;

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    private String vehicleTypeId;

    public String getTypeOfPurchase() {
        return typeOfPurchase;
    }

    public void setTypeOfPurchase(String typeOfPurchase) {
        this.typeOfPurchase = typeOfPurchase;
    }

    public String getVINNumber() {
        return vINNumber;
    }

    public void setVINNumber(String vINNumber) {
        this.vINNumber = vINNumber;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarYear() {
        return carYear;
    }

    public void setCarYear(String carYear) {
        this.carYear = carYear;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public VehicleConditionDisclosure getVehicleConditionDisclosure() {
        return vehicleConditionDisclosure;
    }

    public void setVehicleConditionDisclosure(VehicleConditionDisclosure vehicleConditionDisclosure) {
        this.vehicleConditionDisclosure = vehicleConditionDisclosure;
    }

    VehicleConditionDisclosure vehicleConditionDisclosure;

    public VehicleHistoryDisclosure getVehicleHistoryDisclosure() {
        return vehicleHistoryDisclosure;
    }

    public void setVehicleHistoryDisclosure(VehicleHistoryDisclosure vehicleHistoryDisclosure) {
        this.vehicleHistoryDisclosure = vehicleHistoryDisclosure;
    }

    VehicleHistoryDisclosure vehicleHistoryDisclosure;

    public InitiateThirdParty getInitiateThirdParty() {
        return initiateThirdParty;
    }

    public void setInitiateThirdParty(InitiateThirdParty initiateThirdParty) {
        this.initiateThirdParty = initiateThirdParty;
    }

    InitiateThirdParty initiateThirdParty;

    public ThirdPartyList getThirdPartyList() {
        return thirdPartyList;
    }

    public void setThirdPartyList(ThirdPartyList thirdPartyList) {
        this.thirdPartyList = thirdPartyList;
    }

    ThirdPartyList thirdPartyList;

}
