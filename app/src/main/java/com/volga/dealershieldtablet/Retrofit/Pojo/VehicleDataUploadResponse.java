package com.volga.dealershieldtablet.Retrofit.Pojo;

import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosureToUpload;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListUpload;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromiseUpload;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;

import java.util.ArrayList;

public class VehicleDataUploadResponse {
    private String FirstName;
    private String LastName;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs> getThirdPartyReportDocs() {
        return ThirdPartyReportDocs;
    }

    public void setThirdPartyReportDocs(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs> thirdPartyReportDocs) {
        ThirdPartyReportDocs = thirdPartyReportDocs;
    }
    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts> getCustomerVehicleDealAlerts() {
        return CustomerVehicleDealAlerts;
    }

    public void setCustomerVehicleDealAlerts(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts> customerVehicleDealAlerts) {
        CustomerVehicleDealAlerts = customerVehicleDealAlerts;
    }

    private ArrayList<CustomerVehicleDealAlerts> CustomerVehicleDealAlerts;
    private ArrayList<ThirdPartyReportDocs> ThirdPartyReportDocs;

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }
    public ArrayList<InitThirdParty> getThirdPartyReportRaws() {
        return ThirdPartyReportRaws;
    }

    public void setThirdPartyReportRaws(ArrayList<InitThirdParty> thirdPartyReportRaws) {
        ThirdPartyReportRaws = thirdPartyReportRaws;
    }

    ArrayList<InitThirdParty> ThirdPartyReportRaws;

    public ArrayList<AdditionalDisclosureToUpload> getAdditinalDisclosures() {
        return AdditinalDisclosures;
    }

    public void setAdditinalDisclosures(ArrayList<AdditionalDisclosureToUpload> additinalDisclosures) {
        AdditinalDisclosures = additinalDisclosures;
    }

    private ArrayList<AdditionalDisclosureToUpload> AdditinalDisclosures;

    private String MiddleName;
    private int GenderTypeId;
    private String DateOfBirth;
    private int LocationId;
    private int LanguageId;
    private String DrivingLicenseNumber;
    private String DrivingLicenseExpiryDate;
    private String Email;
    private String ContactNumber;
    private String CarTypeId;
    private String TradeTypeId;
    private String VIN;
    private String Make;
    private String Model;
    private String ModelYear;
    private String TestDriveDurationId;
    private boolean IsTestDriveTaken;
    private boolean IsAcceptedUsedCarPolicy ;
    private boolean Allow2RemoveWindowSticker;
    private String Mileage;
    private String TradeDateTime;
    private String Address1;
    private String Address2;
    private String City;
    private String Zipcode,TradeCompletionUTC;

    public boolean getIsRemote() {
        return IsRemote;
    }

    public void setIsRemote(boolean isRemote) {
        IsRemote = isRemote;
    }

    private boolean IsRemote;

    private String LastScreenName;
    private CoBuyerCustomerDetails CoBuyerDetail;
    private boolean IsTermConditionAccepted,IsLead;
    private ArrayList<VerbalPromiseUpload> VerbalPromises;

    public ArrayList<VerbalPromiseUpload> getVerbalPromises() {
        return VerbalPromises;
    }

    public void setVerbalPromises(ArrayList<VerbalPromiseUpload> verbalPromises) {
        VerbalPromises = verbalPromises;
    }

    public ArrayList<CheckListUpload> getCheckLists() {
        return CheckLists;
    }

    public void setCheckLists(ArrayList<CheckListUpload> checkLists) {
        CheckLists = checkLists;
    }

    private ArrayList<CheckListUpload> CheckLists;
    public boolean isAcceptedUsedCarPolicy() {
        return IsAcceptedUsedCarPolicy;
    }

    public void setAcceptedUsedCarPolicy(boolean acceptedUsedCarPolicy) {
        IsAcceptedUsedCarPolicy = acceptedUsedCarPolicy;
    }

    ArrayList<HistoryDisclosures> HistoryDisclosures;
    ArrayList<ThirdPartyHistoryReport> ThirdPartyHistoryReport;

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> getCustomerAgreement() {
        return CustomerAgreement;
    }

    public void setCustomerAgreement(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> customerAgreement) {
        CustomerAgreement = customerAgreement;
    }

    ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement> CustomerAgreement;

    public ArrayList<com.volga.dealershieldtablet.ui.coBuyer.SalesPersons> getSalesPersons() {
        return SalesPersons;
    }

    public void setSalesPersons(ArrayList<com.volga.dealershieldtablet.ui.coBuyer.SalesPersons> salesPersons) {
        SalesPersons = salesPersons;
    }

    ArrayList<com.volga.dealershieldtablet.ui.coBuyer.SalesPersons> SalesPersons;
    public String getLastScreenName() {
        return LastScreenName;
    }

    public void setLastScreenName(String lastScreenName) {
        LastScreenName = lastScreenName;
    }

    public boolean isLead() {
        return IsLead;
    }

    public void setLead(boolean lead) {
        IsLead = lead;
    }
    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures> getConditionDisclosures() {
        return ConditionDisclosures;
    }

    public void setConditionDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures> conditionDisclosures) {
        ConditionDisclosures = conditionDisclosures;
    }

    ArrayList<ConditionDisclosures> ConditionDisclosures;

    public CoBuyerCustomerDetails getCoBuyerDetail() {
        return CoBuyerDetail;
    }

    public void setCoBuyerDetail(CoBuyerCustomerDetails coBuyerDetail) {
        CoBuyerDetail = coBuyerDetail;
    }

    public com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle getTradeInVehicle() {
        return TradeInVehicle;
    }

    public void setTradeInVehicle(com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle tradeInVehicle) {
        TradeInVehicle = tradeInVehicle;
    }

    public TradeInVehicle TradeInVehicle;


    public boolean isTermConditionAccepted() {
        return IsTermConditionAccepted;
    }

    public void setTermConditionAccepted(boolean termConditionAccepted) {
        IsTermConditionAccepted = termConditionAccepted;
    }


    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures> getHistoryDisclosures() {
        return HistoryDisclosures;
    }

    public void setHistoryDisclosures(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures> historyDisclosures) {
        HistoryDisclosures = historyDisclosures;
    }

    public ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport> getThirdPartyHistoryReport() {
        return ThirdPartyHistoryReport;
    }

    public void setThirdPartyHistoryReport(ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport> thirdPartyHistoryReport) {
        ThirdPartyHistoryReport = thirdPartyHistoryReport;
    }


    public String getDealershipId() {
        return DealershipId;
    }

    public void setDealershipId(String dealershipId) {
        DealershipId = dealershipId;
    }

    private String DealershipId;

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    private String State;
    private String Country;
    private int StateProvinceId;

    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    private String stockNumber;


    // Getter Methods

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public int getGenderTypeId() {
        return GenderTypeId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public int getLocationId() {
        return LocationId;
    }

    public int getLanguageId() {
        return LanguageId;
    }

    public String getDrivingLicenseNumber() {
        return DrivingLicenseNumber;
    }

    public String getDrivingLicenseExpiryDate() {
        return DrivingLicenseExpiryDate;
    }

    public String getEmail() {
        return Email;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public String getCarTypeId() {
        return CarTypeId;
    }

    public String getTradeTypeId() {
        return TradeTypeId;
    }

    public String getVIN() {
        return VIN;
    }

    public String getMake() {
        return Make;
    }

    public String getModel() {
        return Model;
    }

    public String getModelYear() {
        return ModelYear;
    }

    public String getTestDriveDurationId() {
        return TestDriveDurationId;
    }

    public boolean getIsTestDriveTaken() {
        return IsTestDriveTaken;
    }

    public boolean getAllow2RemoveWindowSticker() {
        return Allow2RemoveWindowSticker;
    }

    public String getMileage() {
        return Mileage;
    }

    public String getTradeDateTime() {
        return TradeDateTime;
    }

    public String getAddress1() {
        return Address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public String getCity() {
        return City;
    }

    public String getZipcode() {
        return Zipcode;
    }

    public int getStateProvinceId() {
        return StateProvinceId;
    }

    // Setter Methods

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public void setGenderTypeId(int GenderTypeId) {
        this.GenderTypeId = GenderTypeId;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public void setLocationId(int LocationId) {
        this.LocationId = LocationId;
    }

    public void setLanguageId(int LanguageId) {
        this.LanguageId = LanguageId;
    }

    public void setDrivingLicenseNumber(String DrivingLicenseNumber) {
        this.DrivingLicenseNumber = DrivingLicenseNumber;
    }

    public void setDrivingLicenseExpiryDate(String DrivingLicenseExpiryDate) {
        this.DrivingLicenseExpiryDate = DrivingLicenseExpiryDate;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public void setCarTypeId(String CarTypeId) {
        this.CarTypeId = CarTypeId;
    }

    public void setTradeTypeId(String TradeTypeId) {
        this.TradeTypeId = TradeTypeId;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public void setModelYear(String ModelYear) {
        this.ModelYear = ModelYear;
    }

    public void setTestDriveDurationId(String TestDriveDurationId) {
        this.TestDriveDurationId = TestDriveDurationId;
    }

    public void setIsTestDriveTaken(boolean IsTestDriveTaken) {
        this.IsTestDriveTaken = IsTestDriveTaken;
    }

    public void setAllow2RemoveWindowSticker(boolean Allow2RemoveWindowSticker) {
        this.Allow2RemoveWindowSticker = Allow2RemoveWindowSticker;
    }

    public void setMileage(String Mileage) {
        this.Mileage = Mileage;
    }

    public void setTradeDateTime(String TradeDateTime) {
        this.TradeDateTime = TradeDateTime;
    }

    public void setAddress1(String Address1) {
        this.Address1 = Address1;
    }

    public void setAddress2(String Address2) {
        this.Address2 = Address2;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public void setZipcode(String Zipcode) {
        this.Zipcode = Zipcode;
    }

    public void setStateProvinceId(int StateProvinceId) {
        this.StateProvinceId = StateProvinceId;
    }

    public String getTradeCompletionUTC() {
        return TradeCompletionUTC;
    }

    public void setTradeCompletionUTC(String tradeCompletionUTC) {
        TradeCompletionUTC = tradeCompletionUTC;
    }
}