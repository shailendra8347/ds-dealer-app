package com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail;

public class VehicleTradeConditionDisclosures
{
    private String CustomerVehicleTradeId;

    private String VehicleConditionDisclosureId;

    private String Id;

    private String value;
    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    boolean IsVerified;

    public String getCustomerVehicleTradeId ()
    {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId (String CustomerVehicleTradeId)
    {
        this.CustomerVehicleTradeId = CustomerVehicleTradeId;
    }

    public String getVehicleConditionDisclosureId ()
    {
        return VehicleConditionDisclosureId;
    }

    public void setVehicleConditionDisclosureId (String VehicleConditionDisclosureId)
    {
        this.VehicleConditionDisclosureId = VehicleConditionDisclosureId;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CustomerVehicleTradeId = "+CustomerVehicleTradeId+", VehicleConditionDisclosureId = "+VehicleConditionDisclosureId+", Id = "+Id+", value = "+value+"]";
    }
}
	