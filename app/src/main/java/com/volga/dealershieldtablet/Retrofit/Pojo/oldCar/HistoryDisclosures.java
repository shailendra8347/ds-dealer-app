package com.volga.dealershieldtablet.Retrofit.Pojo.oldCar;

/**
 * Created by ${Shailendra} on 20-08-2018.
 */
public class HistoryDisclosures {
    String VehicleHistoryDisclosureId;

    public String getVehicleHistoryDisclosureId() {
        return VehicleHistoryDisclosureId;
    }

    public void setVehicleHistoryDisclosureId(String  vehicleHistoryDisclosureId) {
        VehicleHistoryDisclosureId = vehicleHistoryDisclosureId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    String Date;
    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    boolean IsVerified;
}
