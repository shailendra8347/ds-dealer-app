
package com.volga.dealershieldtablet.Retrofit.Pojo.Records;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SyncUp {

    @SerializedName("CarImages")
    @Expose
    private CarImages carImages;
    @SerializedName("License")
    @Expose
    private License license;
    @SerializedName("Signatures")
    @Expose
    private Signatures signatures;

    public CarImages getCarImages() {
        return carImages;
    }

    public void setCarImages(CarImages carImages) {
        this.carImages = carImages;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public Signatures getSignatures() {
        return signatures;
    }

    public void setSignatures(Signatures signatures) {
        this.signatures = signatures;
    }

}
