package com.volga.dealershieldtablet.Retrofit.Pojo;

public class ForgotPassword{
	private boolean Succeeded;
	private String Message;
	private boolean IsDuplicate;
	private Object Value;

	public void setSucceeded(boolean succeeded){
		this.Succeeded = succeeded;
	}

	public boolean isSucceeded(){
		return Succeeded;
	}

	public void setMessage(String message){
		this.Message = message;
	}

	public String getMessage(){
		return Message;
	}

	public void setIsDuplicate(boolean isDuplicate){
		this.IsDuplicate = isDuplicate;
	}

	public boolean isIsDuplicate(){
		return IsDuplicate;
	}

	public void setValue(Object value){
		this.Value = value;
	}

	public Object getValue(){
		return Value;
	}

	@Override
 	public String toString(){
		return 
			"ForgotPassword{" + 
			"succeeded = '" + Succeeded + '\'' +
			",message = '" + Message + '\'' +
			",isDuplicate = '" + IsDuplicate + '\'' +
			",value = '" + Value + '\'' +
			"}";
		}
}
