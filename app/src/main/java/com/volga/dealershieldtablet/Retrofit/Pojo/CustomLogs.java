package com.volga.dealershieldtablet.Retrofit.Pojo;

import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 20-01-2019.
 */
public class CustomLogs {
    private String CustomerVehicleTradeId;
    private boolean IsSendAlertEmail;

    public boolean getIsSendAlertEmail() {
        return IsSendAlertEmail;
    }

    public void setIsSendAlertEmail(boolean isSendAlertEmail) {
        IsSendAlertEmail = isSendAlertEmail;
    }

    public String getEmailSubject() {
        return EmailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        EmailSubject = emailSubject;
    }

    public ArrayList<String> getEmails() {
        return Emails;
    }

    public void setEmails(ArrayList<String> emails) {
        Emails = emails;
    }

    private String EmailSubject;

    public String getCustomerVehicleTradeId() {
        return CustomerVehicleTradeId;
    }

    public void setCustomerVehicleTradeId(String customerVehicleTradeId) {
        CustomerVehicleTradeId = customerVehicleTradeId;
    }

    public String getLogText() {
        return logText;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

    private String logText;

    public String getDealershipId() {
        return DealershipId;
    }

    public void setDealershipId(String dealershipId) {
        DealershipId = dealershipId;
    }

    private String DealershipId;
    private ArrayList<String> Emails;

}
