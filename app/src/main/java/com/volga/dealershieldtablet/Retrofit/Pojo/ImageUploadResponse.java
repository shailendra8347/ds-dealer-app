package com.volga.dealershieldtablet.Retrofit.Pojo;

/**
 * Created by ${Shailendra} on 07-06-2018.
 */
public class ImageUploadResponse {
    String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public boolean isSucceeded() {
        return Succeeded;
    }

    public void setSucceeded(boolean succeeded) {
        Succeeded = succeeded;
    }

    String Value;
    boolean Succeeded;
}
