package com.volga.dealershieldtablet.ui.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.manateeworks.BarcodeScanner;
import com.pushlink.android.PushLink;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.interfaceCallback.MWBScannerListener;
import com.volga.dealershieldtablet.ui.fragment.FragmentLogin;
import com.volga.dealershieldtablet.ui.fragment.PathSelection;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

//import com.bugclipper.android.BugClipper;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class MainActivity extends BaseActivity implements MWBScannerListener {

    final int DRAW_OVER_OTHER_APP_PERMISSION = 123;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,
//                MainActivity.class));

//        PreferenceManger.putBoolean("Syncing", false);
//        bcSDK();
        new Handler().post(() -> Lingver.getInstance().setLocale(MainActivity.this, "en"));

//        DSAPP.localeManager.setNewLocale(this, "en");

//        Locale locale = new Locale("en");
//        Locale.setDefault(locale);
//
//        Resources res = getResources();
//        Configuration config = new Configuration(res.getConfiguration());
//        if (Utility.isAtLeastVersion(JELLY_BEAN_MR1)) {
//            config.setLocale(locale);
//            createConfigurationContext(config);
//        } else {
//            config.locale = locale;
//            res.updateConfiguration(config, res.getDisplayMetrics());
//        }

//        Locale myLocale = new Locale("en");
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isDataPushed", getIntent().getBooleanExtra("isDataPushed", false));
        bundle.putBoolean("finalSave", getIntent().getBooleanExtra("finalSave", false));

        registerBroadcastReceiver();
//        PreferenceManger.putBoolean(AppConstant.IS_CO_BUYER, false);
//        PreferenceManger.putBoolean(AppConstant.IS_CO_BUYER_SELECTED, false);


        if (PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN).trim().length() != 0) {
            PathSelection pathSelection = new PathSelection();
            pathSelection.setArguments(bundle);
            callFragment(R.id.container, pathSelection, PathSelection.class.getSimpleName());
        } else {
            callFragment(R.id.container, new FragmentLogin(), FragmentLogin.class.getSimpleName());
        }
//        if (getIntent() != null && getIntent().getStringExtra("stacktrace") == null || getIntent().getStringExtra("stacktrace").length()== 0) {
//            throw new NullPointerException();
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void bcSDK() {

        String[] perms = {"android.permission.RECORD_AUDIO"};
        int permsRequestCode = 200;
        requestPermissions(perms, permsRequestCode);
//        askForSystemOverlayPermission();
//        BugClipper.setInvokerMode(BugClipper.InvokerMode.ProductionMode);
//        // BugClipper.setInvokerMode(BugClipper.InvokerMode.ProductionMode);
//        BugClipper.setEnv(BugClipper.Enviroment.Other,"https://demo.bugclipper.com/api/android/");
//        BugClipper.launchWithKey(this.getApplication(),"ahb1017f40ccbbc");

    }

    private void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }


    @Override
    public void onScannedResult(BarcodeScanner.MWResult result) {

    }

    public void registerBroadcastReceiver() {

        this.registerReceiver(customBroadCastReceiver, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        PushLink.setCurrentActivity(this);
    }
}
