package com.volga.dealershieldtablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.adapter.PrintsReceivedAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeMap;

import static android.os.Looper.getMainLooper;


public class FragmentReceivedPrint extends BaseFragment implements PrintsReceivedAdapter.AllChecked {

    IGoToNextPage iGoToNextPage;
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    private Button yes, no;
    private String[] list;
    private boolean allChecked = true;
    private DSTextView time;
    boolean isTaken;
    private DSTextView sign;
    private ImageView delete;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && allChecked && isSigned) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }
        }
    };

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (allChecked && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (allChecked && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && next != null) {
            time.setText(getCurrentTimeString());
//            next.setVisibility(View.GONE);
            signaturePad.clear();
            disableWithGray(next);
//            enableNext(next);
            isSigned = false;
            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
//            if (yes != null && vehicleDetails.getStickerRemovalPermission()) {
//                yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
//                no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_gray));
//            }
            //Code for displaying already selected Confirmation

            initPhoto();
        } else {
            isTaken = false;
        }
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
                    Gson gson = new GsonBuilder().create();
                    File file;
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file = getOutputFile("UserPic", "co_print_receive_selection_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "print_receive_selection_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.prints_recieved, container, false);
        isSigned = false;
        isAnsSelected = false;
        initView();
        return mView;
    }

    private void initView() {
        if (NewDealViewPager.currentPage == 80) {
            initPhoto();
        }
        next = mView.findViewById(R.id.text_next);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        if (decidePageNumber() == 10)
            text_cancelBack.setText(String.format(getString(R.string.page1), "9", decidePageNumber()));
        else if (decidePageNumber() == 11)
            text_cancelBack.setText(String.format(getString(R.string.page1), "10", decidePageNumber()));
        else if (decidePageNumber() == 12)
            text_cancelBack.setText(String.format(getString(R.string.page1), "11", decidePageNumber()));
        else
            text_cancelBack.setText(String.format(getString(R.string.page1), "12", decidePageNumber()));
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        time = mView.findViewById(R.id.time);
        time.setText(getCurrentTimeString());

        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
//        next.setVisibility(View.GONE);
        disableWithGray(next);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        AppConstant.printReceivedList.clear();
        list = getResources().getStringArray(R.array.printReceivedList);
        ArrayList<HistoryListPojo> historyListPojos = new ArrayList<>();
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        sign = mView.findViewById(R.id.signhere);
        final DSTextView title = mView.findViewById(R.id.title);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
            title.setText(Html.fromHtml(getString(R.string.printRecieveDisclosure)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
            title.setText(Html.fromHtml(getString(R.string.co_printRecieveDisclosure)));
        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("Vehicle: %s, %s, %s", make, model, year));
        }
        int id = 0;
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {

            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getPrintReceivedList() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getPrintReceivedList().size() == 0) {
                for (String aList : list) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setName(aList);
//                    id++;
                    pojo.setId(getIds(aList));
                    pojo.setChecked(false);
                    historyListPojos.add(pojo);

                }
                AppConstant.printReceivedList.addAll(historyListPojos);
            } else {
                AppConstant.printReceivedList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getPrintReceivedList());
            }
        } else {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getPrintReceivedList() == null || recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getPrintReceivedList().size() == 0) {
                for (String aList : list) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setName(aList);
//                    id++;
                    pojo.setId(getIds(aList));
                    pojo.setChecked(false);
                    historyListPojos.add(pojo);

                }
                AppConstant.printReceivedList.addAll(historyListPojos);
            } else {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null)
                    AppConstant.printReceivedList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCobuyerPrintReceivedList());
            }
        }
        //change for bullet point
//        for (int i = 0; i < AppConstant.printReceivedList.size(); i++) {
//            allChecked = AppConstant.printReceivedList.get(i).isChecked();
//        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguage() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguage().equalsIgnoreCase("English")) {
                if (AppConstant.printReceivedList.size() == 4) {
                    AppConstant.printReceivedList.remove(3);
                }
            }
        } else {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("English"))) {
                if (AppConstant.printReceivedList.size() == 4) {
                    AppConstant.printReceivedList.remove(3);
                }
            }
        }
        recyclerView.setAdapter(new PrintsReceivedAdapter(getActivity(), AppConstant.printReceivedList, this));
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!allChecked) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                    return;
                }
//                disableNext(next);
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
//                if (checkMandatoryImage())
                analyseTheImages();
                iGoToNextPage.whatNextClick();
                Gson gson = new GsonBuilder().create();
                RecordData recordData;
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
                File file;
                if (vehicleDetails.isCoBuyer()) {
                    file = getOutputFile("Screenshot", "co_print_receive_selection_screen");
                } else {
                    file = getOutputFile("Screenshot", "print_receive_selection_screen");
                }
                scrollableScreenshot(mainContent, file);
            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        svgFile = new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg");
                    } else {
                        svgFile = new File(path_signature + "/IMG_prints_recieved_selection.svg");
                    }
                    SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
                    File file;
                    if (vehicleDetails.isCoBuyer()) {
                        file = getOutputFile("Screenshot", "co_print_receive_selection_screen");
                    } else {
                        file = getOutputFile("Screenshot", "print_receive_selection_screen");
                    }
                    File file1;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file1 = getOutputFile("UserPic", "co_print_receive_selection_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "print_receive_selection_screen_pic");
                    }

                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    svgFile = new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg");
                } else {
                    svgFile = new File(path_signature + "/IMG_prints_recieved_selection.svg");
                }
                SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
                File file;
                if (vehicleDetails.isCoBuyer()) {
                    file = getOutputFile("Screenshot", "co_print_receive_selection_screen");
                } else {
                    file = getOutputFile("Screenshot", "print_receive_selection_screen");
                }
                File file1;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file1 = getOutputFile("UserPic", "co_print_receive_selection_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "print_receive_selection_screen_pic");
                }

                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                iGoToNextPage.goToBackIndex();
            }
        });
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
//                next.setVisibility(View.GONE);
                disableWithGray(next);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                if (allChecked) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    } else if (isTaken) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
                isSigned = true;
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
//                        next.setVisibility(View.GONE);
                        disableWithGray(next);
                        return true;
                    }
                }
                return true;
            }
        });

    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {


            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            File svgFile;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_prints_recieved_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "prints_recieved_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setPrintCopyRecieved(true);

            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCobuyerPrintReceivedList(AppConstant.printReceivedList);
                ArrayList<HistoryListPojo> listPojos = AppConstant.printReceivedList;
                ArrayList<CustomerAgreement> customerAgreements = new ArrayList<>();
                if (listPojos != null) {
                    for (int i = 0; i < listPojos.size(); i++) {
                        CustomerAgreement customerAgreement = new CustomerAgreement();
                        customerAgreement.setCustomerSignAgreementsId(listPojos.get(i).getId());
                        customerAgreement.setValue(listPojos.get(i).isValue());
                        customerAgreements.add(customerAgreement);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setPrintReceivedList(customerAgreements);
            } else {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setPrintReceivedList(AppConstant.printReceivedList);
            }
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }

    }

    @Override
    public void allChecked(boolean allChecked) {
        this.allChecked = allChecked;
        if (allChecked && isSigned) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    enableWithGreen(next);
                    next.setVisibility(View.VISIBLE);
                }
            }, 100);
        } else {
            disableWithGray(next);
//            next.setVisibility(View.GONE);
        }
    }


    private String getIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase(getString(R.string.print1))) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print2))) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print3))) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print4))) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print5))) {
            id = "5";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print6))) {
            id = "6";
        }
        return id;
    }

    private boolean checkMandatoryImage() {

        boolean allTrue = true;
        String path_licence, path_signature, path_car;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (!new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Buyer's DMV Front Image is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasCoBuyer() && !new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Co-Buyer's DMV Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_front.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_right.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Passengers side Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_rear.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Back Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_left.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Left Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_sticker.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Addendum Sticker/Buyers Guide Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Odometer Image side is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Odometer Image is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Licence Plate Image side is missing.");
            return false;
        } else {
            return true;
        }
    }


}
