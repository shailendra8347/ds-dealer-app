package com.volga.dealershieldtablet.ui.fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentAbortOrContinue extends BaseFragment {


    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.abort_or_not, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView abort = mView.findViewById(R.id.abort);
        DSTextView returnToVin = mView.findViewById(R.id.returnToVin);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView back = mView.findViewById(R.id.text_back_up);
//        text_cancelBack.setVisibility(View.GONE);
        returnToVin.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        abort.setOnClickListener(mOnClickListener);
//        next.setText(getString(R.string.next_la));
        back.setText(getString(R.string.back_la));

    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.returnToVin:
                    new AlertDialog.Builder(getActivity())
                            .setMessage("Are you sure you want to delete the vehicle record?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.yeslast, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Gson gson = new GsonBuilder().create();
                                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setBussinessLead(true);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setFromServer(false);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDeal(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0"));
                                    String recordDataString = gson.toJson(recordData);
                                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                                    Lingver.getInstance().setLocale(getActivity(), "en");
                                    getActivity().finishAffinity();
                                    callHomeActivity();


//                                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//                                    if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
//                                        if (DSAPP.getInstance().isNetworkAvailable()) {
//                                            deleteRecordFromServer(false, "Deleting vehicle record...");
//                                        } else {
//                                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                                        }
//                                    } else {
//                                        clearFolder(record);
//                                        recordData.getRecords().remove(record);
//                                        String recordDataString1 = gson.toJson(recordData);
//                                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                                        new CustomToast(getActivity()).alert(getString(R.string.record_aborted));
//                                        Lingver.getInstance().setLocale(getActivity(), "en");
//                                        getActivity().finishAffinity();
//                                        Intent intent1 = new Intent(getActivity(), NewDealViewPager.class);
//                                        intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
//                                        intent1.putExtra("page", 0);
//                                        startActivity(intent1);
//                                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        getActivity().finish();
//                                    }


                                }
                            })
                            .setNegativeButton(R.string.nolast, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                }
                            }).show().setCancelable(false);

//                    next(new FragmentScanVINSticker());
                    break;
                case R.id.text_back_up:
                    getActivity().finish();
                    break;

                case R.id.abort:
                    abort(getString(R.string.abortRecord));
                    break;
            }
        }
    };

    void abort(String title) {
        new AlertDialog.Builder(getActivity())
                .setMessage(title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yeslast, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setBussinessLead(true);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setFromServer(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDeal(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0"));
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Lingver.getInstance().setLocale(getActivity(), "en");
                        getActivity().finishAffinity();
                        callHomeActivity();


//                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//
//                        if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
//                            if (DSAPP.getInstance().isNetworkAvailable()) {
//                                deleteRecordFromServer(true, "Aborting deal...");
//                            } else {
//                                new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                            }
//                        } else {
//                            clearFolder(record);
//                            recordData.getRecords().remove(record);
//                            String recordDataString1 = gson.toJson(recordData);
//                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                            new CustomToast(getActivity()).alert(getString(R.string.record_aborted));
//
//                            Lingver.getInstance().setLocale(getActivity(), "en");
//                            getActivity().finishAffinity();
//                            callHomeActivity();
//                        }
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.nolast, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show().setCancelable(false);
    }

    private void writeCustomLogsAPI(String id, String logs) {
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setLogText(logs);
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void deleteRecordFromServer(final boolean abort, String msg) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(msg);
        progressDialog.show();
        final Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        final Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));

        RetrofitInitialization.getDs_services().abortDeal("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "").enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                progressDialog.dismiss();
                if (response.isSuccessful() && response.code() == 200) {
                    writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), " Record has been aborted: " + gson.toJson(record));
                    if (abort) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        clearFolder(record);
                        recordData.getRecords().remove(record);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                        new CustomToast(getActivity()).alert("Vehicle record deleted.");
                        Lingver.getInstance().setLocale(getActivity(), "en");
                        callHomeActivity();
                    } else {
                        /*....................*/
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        clearFolder(record);
                        recordData.getRecords().remove(record);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                        new CustomToast(getActivity()).alert(getString(R.string.record_aborted));
                        Lingver.getInstance().setLocale(getActivity(), "en");
//                        Locale myLocale = new Locale("en");
//                        Resources res = getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        Configuration conf = res.getConfiguration();
//                        conf.locale = myLocale;
//                        res.updateConfiguration(conf, dm);
                        getActivity().finishAffinity();
                        Intent intent1 = new Intent(getActivity(), NewDealViewPager.class);
                        intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                        intent1.putExtra("page", 0);
                        startActivity(intent1);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().finish();
//                        callHomeActivity();
                        /*..............................*/
//                        Gson gson1 = new GsonBuilder().create();
//                        RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                        VehicleDetails vehicleDetails = new VehicleDetails();
//                        TradeInVehicle tradeInVehicle = new TradeInVehicle();
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setTradeInVehicle(tradeInVehicle);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSelectedLanguage(null);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSelectedLanguageId(0);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCarImagesConfirmed(false);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCarDetailConfirmed(false);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(false);
//                        deleteVehicle(recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)));
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(0);
//                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
//                        String recordDataString2 = gson1.toJson(recordData1);
//                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString2);
//                        Locale myLocale = new Locale("en");
//                        Resources res = getResources();
//                        DisplayMetrics dm = res.getDisplayMetrics();
//                        Configuration conf = res.getConfiguration();
//                        conf.locale = myLocale;
//                        res.updateConfiguration(conf, dm);

                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e("Deleting deal.. ", call.toString());
            }
        });

    }

    private void clearFolder(Record record) {
        Log.e("Deleting record: ", "  :)");
        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
            deleteRecursive(main);
            //            if (main.isDirectory()) {
//                for (File child : main.listFiles())
//                    deleteRecursive(child);
//            }
//            Log.e("file deleted : ", main.delete() + "");
//            if (main.isDirectory()) {
//                String[] children = main.list();
//                for (String aChildren : children) {
//                    Log.e("aChildren: " + aChildren, " Child Folders deleted : " + new File(main, aChildren).delete());
////                    tab zentil 400mg once a time
////                    cap terramitin 250 mg 3 times
////                    tab metrozil 400mg 3 times
////                    tab entroquinol 3 times
////                    tab unienzime 2 times
//                }
//                Log.e("All folder deleted: ", " Parent Folders deleted : " + main.delete());
//            }
        }
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }

    private void deleteVehicle(Record record) {
        Log.e("Deleting  record: ", " Deleting only vehicle record and rescanning the vin :)");
        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages").exists()) {
            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages");
            if (main.isDirectory()) {
                String[] children = main.list();
                for (String aChildren : children) {
                    Log.e("aChildren: " + aChildren, " Child Folders deleted : " + new File(main, aChildren).delete());
                }
                Log.e("All folder deleted: ", " Parent Folders deleted : " + main.delete());
            }
        }
    }
}
