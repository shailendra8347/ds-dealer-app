package com.volga.dealershieldtablet.ui.scanner;
//Comment to commit
import com.manateeworks.BarcodeScanner;

import org.json.JSONException;

/**
 * Created by lazarilievski on 1/22/18.
 */

public interface MWBScannerListener {
    public void onScannedResult(BarcodeScanner.MWResult result) throws JSONException;
}
