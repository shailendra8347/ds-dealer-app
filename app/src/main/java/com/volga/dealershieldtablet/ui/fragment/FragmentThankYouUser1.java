package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentThankYouUser1 extends BaseFragment {


    private View mView;

    IGoToNextPage iGoToNextPage;
    private LinearLayout mainContent;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.thankyou_screen1, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView handover = mView.findViewById(R.id.submit);
        DSTextView handover1 = mView.findViewById(R.id.submit1);
        handover.setText(getString(R.string.almost1));
        handover1.setText(getString(R.string.almost2));
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        final DSTextView next = mView.findViewById(R.id.text_next);
        final TextView back = mView.findViewById(R.id.text_back_up);
//        text_cancelBack.setText(R.string.page8);
        text_cancelBack.setVisibility(View.INVISIBLE);
        Gson gson = new GsonBuilder().create();

        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                handover.setText(Html.fromHtml(getString(R.string.co_dealer_click_here1)));
//            } else {
//                handover.setText(Html.fromHtml(getString(R.string.dealer_click_here1)));
//            }
//        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("Vehicle: %s, %s, %s", make, model, year));
        }
        mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next.setEnabled(false);
                next.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        next.setEnabled(true);
                        next.setClickable(true);
                    }
                },3000);
                iGoToNextPage.whatNextClick();
                Gson gson = new GsonBuilder().create();
                File file;
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file = getOutputFile("Screenshot", "co_almost_done_screen");
                    } else {
                        file = getOutputFile("Screenshot", "almost_done_screen");
                    }
                    scrollableScreenshot(mainContent, file);
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back.setEnabled(false);
                back.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        back.setEnabled(true);
                        back.setClickable(true);
                    }
                },3000);
                iGoToNextPage.goToBackIndex();
            }
        });
//        handover.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
//                        .setMessage(R.string.r_u_dealer)
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setPositiveButton(R.string.yeslast, new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                iGoToNextPage.whatNextClick();
//                            }
//                        })
//                        .setNegativeButton(R.string.nolast, new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                dialog.dismiss();
//                            }
//                        }).show().setCancelable(false);
//            }
//        });
    }


}
