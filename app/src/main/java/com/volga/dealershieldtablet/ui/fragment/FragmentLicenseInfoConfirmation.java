package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentLicenseInfoConfirmation extends BaseFragment {


    private View mView;
    private DSTextView firstName, lastName, addressLine1, addressLine2, cityStateCountry, dmvNumber, dmvExpiry, gender, zip, middleName, dob;
    private RecordData recordData;
    private Gson gson;
    private File fileToDisplay;
    private DSTextView cancel;
    private DSTextView back;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.license_info_confirmation, container, false);
        initView();
        return mView;
    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    private void initView() {
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        back = mView.findViewById(R.id.text_cancelBack);

        DSTextView next = mView.findViewById(R.id.next);
        cancel = mView.findViewById(R.id.text_back_up);
        firstName = mView.findViewById(R.id.firstName);
        lastName = mView.findViewById(R.id.lastName);
        zip = mView.findViewById(R.id.zip);
        middleName = mView.findViewById(R.id.middleName);
        addressLine1 = mView.findViewById(R.id.addressLine1);
        addressLine2 = mView.findViewById(R.id.addressLine2);
        cityStateCountry = mView.findViewById(R.id.cityStateCountry);
        dmvNumber = mView.findViewById(R.id.DMV_Number);
        dmvExpiry = mView.findViewById(R.id.DMV_Expiry);
        gender = mView.findViewById(R.id.gender);
        dob = mView.findViewById(R.id.dateOfBirth);
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        ImageView imageView = mView.findViewById(R.id.dlImage);
        DSTextView title = mView.findViewById(R.id.title);
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String url = "";

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
            back.setVisibility(View.INVISIBLE);
        } else {
            back.setVisibility(View.VISIBLE);

        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            title.setText(Html.fromHtml(getString(R.string.co_please_confirm_the_information_below)));
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().size() > 0) {
                for (int i = 0; i < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().size(); i++) {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().get(i).getDocTypeName().equalsIgnoreCase("co_buyer_DLFRONT")) {
                        url = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().get(i).getDocUrl();
                        break;
                    }
                }
            }
        } else {
            title.setText(Html.fromHtml(getString(R.string.please_confirm_the_information_below)));
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size() > 0) {
                for (int i = 0; i < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size(); i++) {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().get(i).getDocTypeName().equalsIgnoreCase("DRIVERLICENSEIMG")) {
                        url = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().get(i).getDocUrl();
                        break;
                    }
                }
            }

        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            ShowImageFromPath("IMG_Co_buyer_DLFRONT.jpg", "LicenseFront");
        } else {
            ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront");
        }
        if (fileToDisplay != null && fileToDisplay.exists()) {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                title.setText(Html.fromHtml(getString(R.string.co_please_confirm_the_information_below)));
                loadImageToImageView(ShowImageFromPath("IMG_Co_buyer_DLFRONT.jpg", "LicenseFront"), imageView);
            } else {
                title.setText(Html.fromHtml(getString(R.string.please_confirm_the_information_below)));
                loadImageToImageView(ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront"), imageView);

            }
        } else {
            if (url != null && url.length() > 0) {
                loadImageToImageView(url, imageView);
                imageView.setVisibility(View.VISIBLE);

                final String finalUrl = url;
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDialog(finalUrl, "DLFRONT");
                    }
                });
            }
        }

        if (!visibleHintCalled) {
            updateData();
        }

        //Get the recordData object from the Shared Preference for displaying


        if (fileToDisplay != null && fileToDisplay.exists()) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                        showDialog(ShowImageFromPath("IMG_Co_buyer_DLFRONT.jpg", "LicenseFront").toString());
                    else {
                        showDialog(ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront").toString());
                    }
                }
            });
        }
    }

    void showDialog(String uri) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("url", uri);
        bundle.putString("name", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    void showDialog(String uri, String name) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showFragmentInPortrait();
            updateData();
        }
    }

    private String getCorrectText(String string) {
        if (string == null) {
            return "";
        } else if (string.equalsIgnoreCase("null")) {
            return "";
        } else {
            return string;
        }
    }

    private void updateData() {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//update logic for co buyer
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDrivingLicenseNumber() != null) {
            if (recordData != null && firstName != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getFirstName() != null) {
                visibleHintCalled = true;
                zip.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getZipCode())));
                middleName.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getMiddleName())));
                firstName.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getFirstName())));
                lastName.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getLastName())));

                gender.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getGender())));
                dob.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDateOfBirth())));
                addressLine1.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getAddressLineOne())));
                addressLine2.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getAddressLineTwo())));
                if (getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCity()).length() != 0 && getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getState()).length() != 0)
                    cityStateCountry.setText(MessageFormat.format(": {0}, {1}, {2}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCity()), getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getState()), getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCountry())));
                else {
                    cityStateCountry.setText(":");
                }
                //                String dmv = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDLNumber().substring(0, recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDLNumber().length() - 4);
                dmvNumber.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDrivingLicenseNumber())));
                dmvExpiry.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDLExpiryDate())));
            }
        } else {
            if (recordData != null && firstName != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getFirstName() != null) {
                visibleHintCalled = true;
                zip.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getZipCode())));
                middleName.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getMiddleName())));
                firstName.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getFirstName())));
                lastName.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getLastName())));
                gender.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getGender())));
                dob.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDateOfBirth())));
                addressLine1.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getAddressLineOne())));
                addressLine2.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getAddressLineTwo())));
                if (getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getCity()).length() != 0 && getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getState()).length() != 0)
                    cityStateCountry.setText(MessageFormat.format(": {0}, {1}, {2}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getCity()), getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getState()), getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getCountry())));
                else {
                    cityStateCountry.setText(":");
                }
                //                String dmv = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber().substring(0, recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber().length() - 4);
                dmvNumber.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDrivingLicenceNumber())));
                dmvExpiry.setText(MessageFormat.format(": {0}", getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLExpiryDate())));
            }
        }
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    cancel.setEnabled(false);
                    cancel.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cancel.setEnabled(true);
                            cancel.setClickable(true);
                        }
                    }, 6000);
                    clearDMVPreference();
                    cancel();
                    break;
                case R.id.next:
                    clearDMVPreference();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());

                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);

                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(60);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        } else {
                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyerAdded()&&!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isCoBuyerSigned()) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(true);
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                                ((NewDealViewPager) getActivity()).setCurrentPage(54);
                            } else {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                                    ((NewDealViewPager) getActivity()).setCurrentPage(82);
                                }else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isCoBuyerSigned()){
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasCoBuyer(false);
                                    ((NewDealViewPager) getActivity()).setCurrentPage(67);
                                }else{
                                    ((NewDealViewPager) getActivity()).setCurrentPage(67);
                                }
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                            }
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                        iGoToNextPage.whatNextClick();
                    } else {
                        boolean isAllowed = false;
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size() > 0) {
                            for (int i = 0; i < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size(); i++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().get(i).getDocTypeName().equalsIgnoreCase("DRIVERLICENSEIMG")) {
                                    isAllowed = true;
                                    break;
                                }
                            }
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        if ((fileToDisplay != null && fileToDisplay.exists()) || isAllowed) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal() && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                                ((NewDealViewPager) getActivity()).setCurrentPage(67);
                            } else
                                iGoToNextPage.whatNextClick();
                        } else {
                            new CustomToast(getActivity()).alert("Oops!! Please take photo of Driver License/ID again!");
                        }
                    }
//                    if (PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER_SELECTED)) {
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasCoBuyer(PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER_SELECTED));
//                        Intent intent = new Intent(getActivity(), NewDealViewPager.class);
//                        PreferenceManger.putBoolean(AppConstant.IS_CO_BUYER, true);
//                        PreferenceManger.putBoolean(AppConstant.IS_CO_BUYER_SELECTED, false);
//                        startActivity(intent);
//                        Objects.requireNonNull(getActivity()).finish();
//                    } else {
//                        PreferenceManger.putBoolean(AppConstant.IS_CO_BUYER, false);
//                        iGoToNextPage.whatNextClick();
//                    }
//                    String recordDataString = gson.toJson(recordData);
//                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    break;
                case R.id.text_back_up:
                    Gson gson1 = new GsonBuilder().create();
                    RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                    String recordDataString = gson1.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    iGoToNextPage.goToBackIndex();
                    break;
                case R.id.txt_logout:
                    logout();
                    break;
            }
        }
    };

    public Uri ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//            if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)) {
//                path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                        + "/DSXT/temp/LicenseFront/" + fileName;
//            } else {
            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                    + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/LicenseFront/" + fileName;
//            }
        } else {
            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
        }
        File imgFile = new File(path);
        Date lastModDate = new Date(imgFile.lastModified());
        fileToDisplay = imgFile;
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (imgFile.exists()) {
            return Uri.fromFile(imgFile);
        }
        return null;
    }

    private Bitmap rotateImages(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    IGoToNextPage iGoToNextPage;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }


}
