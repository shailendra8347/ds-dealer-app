package com.volga.dealershieldtablet.ui.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.ui.adapter.RecordsAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecordsYetToBeSynced extends BaseFragment implements RecordsAdapter.CallbackOnDelete {
    private View mView;
    private RecyclerView recyclerView;
    private TextView textView;
    private RecordData recordData;
    private Gson gson;
    private RecordsAdapter adapter;
    private ArrayList<Record> recordArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.unsynced_record_list, container, false);
        initView();
        return mView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Objects.requireNonNull(getActivity()).unregisterReceiver(this.broadCastNewMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BroadcastReceiver broadCastNewMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("Data update: ", "Data updated from service");
            if (getActivity() != null) {
                if (intent.getStringExtra("value").equalsIgnoreCase("refreshIncompleteDeal")) {

                } else if (intent.getStringExtra("value").equalsIgnoreCase("logOut")) {

                } else {
                    initView();
                    new CustomToast(getActivity()).alert(intent.getStringExtra("value"));
                }
            }
        }
    };

    private void initView() {
        Objects.requireNonNull(getActivity()).registerReceiver(this.broadCastNewMessage, new IntentFilter("notifyData"));
        textView = mView.findViewById(R.id.heading);
        textView.setText(getText(R.string.record_yet_to_be_sync_d));
        recyclerView = (RecyclerView) mView.findViewById(R.id.record_list);

        ImageView newDeal = mView.findViewById(R.id.hint);
        newDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(getString(R.string.need_to_upload));
            }
        });
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        gson = new GsonBuilder().create();
        mView.findViewById(R.id.swipe).setVisibility(View.GONE);
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        final SwipeRefreshLayout pullToRefresh = (SwipeRefreshLayout) mView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullToRefresh.setRefreshing(false);
            }
        });
        recordArrayList = new ArrayList<>();
        ArrayList<Record> syncedData = new ArrayList<>();
        if (recordData != null) {
            for (int i = 0; i < recordData.getRecords().size(); i++) {
                if (recordData.getRecords().get(i).getSettingsAndPermissions() != null && ((recordData.getRecords().get(i).getSettingsAndPermissions().getIsRecordComplete() && !recordData.getRecords().get(i).getSettingsAndPermissions().isDataSyncedWithServer()) || recordData.getRecords().get(i).getSettingsAndPermissions().isSentEmailForIncomplete())) {
                    recordArrayList.add(recordData.getRecords().get(i));
                } else {
                    if (recordData.getRecords().get(i).getSettingsAndPermissions().isDataSyncedWithServer())
                        syncedData.add(recordData.getRecords().get(i));
                }
            }
        }
        deleteSyncedDate(syncedData);
        adapter = new RecordsAdapter(getActivity(), true, recordArrayList, this);
        recyclerView.setAdapter(adapter);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        logout.setVisibility(View.GONE);
        DSTextView cancel = mView.findViewById(R.id.text_back_up);
        cancel.setText(R.string.back);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
    }

    private void deleteSyncedDate(ArrayList<Record> needToDelete) {
        for (int i = 0; i < needToDelete.size(); i++) {
            Record record = recordData.getRecords().get(i);
            recordData.getRecords().remove(record);
            String recordDataString1 = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
            if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
                File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
                deleteRecursive(main);
            }
        }

    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }

    @Override
    public void onDelete(int position, boolean b, boolean c, boolean isReport) {
        if (isReport) {
            showReportDialog(position);
//            FragmentTransaction ft = getFragmentManager().beginTransaction();
//            DialogFragment newFragment = new ReportDeveloperFragmentDialog();
//            Bundle bundle = new Bundle();
//            bundle.putString("data", recordArrayList.get(position).getSettingsAndPermissions().getErrorDesc());
//            newFragment.setArguments(bundle);
//            newFragment.show(ft, "dialog");
        }

    }

    private void showReportDialog(final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.report_error, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);

        DSTextView save = promptView.findViewById(R.id.save);
        final DSTextView close = promptView.findViewById(R.id.close);
        DSTextView title = promptView.findViewById(R.id.title);
//        title.setText(list.get(position).getName());
        final DSEdittext data = promptView.findViewById(R.id.addCondition);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyboardUtils.hideSoftKeyboard(data, getActivity());
                alertD.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(data, getActivity());
                alertD.dismiss();
                /*Upload all data to server*/
                String feedbackText = data.getText().toString();

                String subjectData;
                if (feedbackText.length() > 0) {
                    subjectData = recordArrayList.get(position).getSettingsAndPermissions().getErrorDesc() + " \n Feedback data: " + feedbackText + ": ";
                } else {
                    subjectData = recordArrayList.get(position).getSettingsAndPermissions().getErrorDesc();
                }
                writeCustomLogsAPI(recordArrayList.get(position).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordArrayList.get(position).getDealershipDetails().getDealershipId() + "", subjectData, recordArrayList.get(position).getVehicleDetails().getVINNumber(), recordArrayList.get(position).getVehicleDetails().getCarMake(), recordArrayList.get(position).getVehicleDetails().getCarModel());
                alertD.dismiss();

            }
        });
        alertD.setView(promptView);
        alertD.show();
    }

    private void writeCustomLogsAPI(String id, String dId, String logs, String vin, String make, String model) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Submitting feedback..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setDealershipId(dId);
        customLogs.setEmailSubject(null);
        customLogs.setIsSendAlertEmail(true);
//        ArrayList<String> emails=new ArrayList<>();
//        emails.add("shailendra.m@volgainfotech.com");
//        emails.add(getArguments().getString("data"));
//        customLogs.setEmails(emails);
        customLogs.setLogText("<div><b>VIN:</b> " + vin + " <br><b>Make:</b> " + make + " <br><b>Model:</b> " + model + " <br><b>JSON DATA: </b><br>" + logs + "</div>");
//        customLogs.setLogText(logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                progressDialog.dismiss();
                new CustomToast(getActivity()).alert("Submitted");
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    void showPopup(String title) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
        final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);
        final DSTextView titleTV = promptView.findViewById(R.id.title);
        DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
        titleTV.setVisibility(View.GONE);
        messageTV.setText(title);
        final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
        stopDeal.setVisibility(View.GONE);
        DSTextView ok = promptView.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertD.dismiss();
            }
        });
        alertD.setView(promptView);
        alertD.show();
//        new AlertDialog.Builder(getActivity())
//                .setMessage(title)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.dismiss();
//                    }
//                }).show();
    }

}
