package com.volga.dealershieldtablet.ui.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.volga.dealershieldtablet.utils.CustomBroadCastReceiver;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class BaseActivity2 extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
    }

    public void callFragment(int container, Fragment fragment, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(container, fragment, tag).commit();

    }

    public void unregisterBroadcastReceiver(CustomBroadCastReceiver broadCastReceiver) {

        this.unregisterReceiver(broadCastReceiver);

    }

    @Override
    protected void onDestroy() {
//        unregisterBroadcastReceiver(customBroadCastReceiver);
        super.onDestroy();
    }
    public void adjustFontScale(Configuration configuration) {
        if (configuration.fontScale > 1.30) {
//            LogUtil.log(LogUtil.WARN, TAG, "fontScale=" + configuration.fontScale); //Custom Log class, you can use Log.w
//            LogUtil.log(LogUtil.WARN, TAG, "font too big. scale down..."); //Custom Log class, you can use Log.w
            configuration.fontScale = (float) 1.30;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getBaseContext().getResources().updateConfiguration(configuration, metrics);
        }
    }
}
