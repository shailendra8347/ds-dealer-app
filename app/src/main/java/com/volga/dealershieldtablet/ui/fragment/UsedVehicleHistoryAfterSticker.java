package com.volga.dealershieldtablet.ui.fragment;

import static android.os.Looper.getMainLooper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosureAdapter;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeConditionDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeThirdPartyHistoryReports;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdPartyReportList;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.customViews.LockableScrollView;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.MultiSignViewPager;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.activity.CustomWebViewActivity;
import com.volga.dealershieldtablet.ui.adapter.HistoryAfterStickerAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class UsedVehicleHistoryAfterSticker extends BaseFragment implements HistoryAfterStickerAdapter.IOnLanguageSelect, AdditionalDisclosureAdapter.OnAdditionalSelect {
    private IGoToNextPage iGoToNextPage;
    private View mView;
    private String[] list;
    private boolean isAutoCheck = false;
    private DSTextView next;
    private SignaturePad signaturePad;
    private boolean isSigned;
    // change for bullet point
    private boolean allTrue = true, allAdditional = true;
    private boolean isCoBuyer = false;
    private HistoryAfterStickerAdapter thirdPartyAdapter;
    private boolean takePictureInitiated = false;
    private DSTextView time;
    boolean isTaken;
    private DSTextView sign;
    private ImageView delete;


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (!isAutoCheck) {
                        if (next != null && allTrue && isSigned && allAdditional) {
                            next.setVisibility(View.VISIBLE);
                            enableWithGreen(next);
                        }
                    } else {
                        if (next != null && allTrue && isSigned) {
                            next.setVisibility(View.VISIBLE);
                            enableWithGreen(next);
                        }
                    }
                }
            }
        }
    };

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (!isAutoCheck) {
                                    if (allTrue && isSigned && allAdditional) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                } else {
                                    if (allTrue && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (!isAutoCheck) {
                                        if (allTrue && isSigned && allAdditional) {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        }
                                    } else {
                                        if (allTrue && isSigned) {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        }
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            isTaken = false;
        } else {
            if (signaturePad != null) {
//                enableNext(next);
                time.setText(getCurrentTimeString());
                disableWithGray(next);
//                next.setVisibility(View.GONE);
                signaturePad.clear();
                isSigned = false;
                takePicture();
            }
        }
    }

    private void takePicture() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());
        Runnable myRunnable;// This is your code
        isTaken = false;
        if (!isAutoCheck) {

            myRunnable = new Runnable() {
                @Override
                public void run() {
                    isTaken = false;
                    if (!isTaken) {
                        File file;
                        if (!isCoBuyer) {
                            file = getOutputFile("UserPic", "history_disclosure_screen_pic");
                        } else {
                            file = getOutputFile("UserPic", "co_history_disclosure_screen_pic");
                        }

                        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                            takeUserPicture(file);
                        } else {
                            isTaken = true;
                        }
                    }
                } // This is your code
            };
        } else {
            myRunnable = new Runnable() {
                @Override
                public void run() {
                    isTaken = false;
                    if (!isTaken) {
                        File file;
                        if (!isCoBuyer) {
                            file = getOutputFile("UserPic", "third_party_report_screen_pic");
                        } else {
                            file = getOutputFile("UserPic", "co_third_party_report_screen_pic");
                        }
                        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                            takeUserPicture(file);
                        } else {
                            isTaken = true;
                        }
                    }
                } // This is your code
            };
        }
        mainHandler.post(myRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
        if (thirdPartyAdapter != null) {
            thirdPartyAdapter.notifyDataSetChanged();
            boolean isAllSelected = false;
            allTrue = true;
            for (int i = 0; i < AppConstant.report.size(); i++) {
                if (AppConstant.report.get(i).isMandatory() && AppConstant.report.get(i).isViewReport()) {
                    // remove if and else for bullet !AppConstant.report.get(i).isChecked()
                    if (!AppConstant.report.get(i).isReportSeen()) {
                        allTrue = false;
                        break;
                    }
                }
//                else {
//                    if (!AppConstant.report.get(i).isChecked()) {
//                        allTrue = AppConstant.report.get(i).isChecked();
//                        break;
//                    }
//                }
//                allTrue = AppConstant.report.get(i).isChecked();
            }
            if (isAutoCheck) {
                if (allTrue && isSigned) {
                    next.setVisibility(View.VISIBLE);
                    enableWithGreen(next);
                }
            }
        }
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    LinearLayout additionalLL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.used_vehicle_history_after_sticker, container, false);
        initView();
        return mView;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        final ImageButton carfax = mView.findViewById(R.id.carfax);
        final ImageButton autocheck = mView.findViewById(R.id.autocheck);
        DSTextView ttv1 = mView.findViewById(R.id.ttv1);
        DSTextView ttv2 = mView.findViewById(R.id.ttv2);
        autocheck.setVisibility(View.GONE);
        time = mView.findViewById(R.id.time);
        time.setText(getCurrentTimeString());
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        LinearLayout thirdPartyContainer = mView.findViewById(R.id.thirdPartyContainer);
        carfax.setVisibility(View.GONE);
        additionalLL = mView.findViewById(R.id.additionalLL);
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        RecyclerView additionalRecyclerView = mView.findViewById(R.id.additionalRecyclerView);
        DSTextView history = mView.findViewById(R.id.history1);
        DSTextView history1 = mView.findViewById(R.id.history);
        mView.findViewById(R.id.header).setVisibility(View.GONE);
//        final LockableScrollView nestedScrollView = mView.findViewById(R.id.temp);
//
//        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);
//
//                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
//                        .getScrollY()));
//
//                //                    getPlaylistFromServer("more");
//                nestedScrollView.setScrollingEnabled(diff != 0);
//            }
//        });
        mView.findViewById(R.id.header1).setVisibility(View.VISIBLE);
        DSTextView customerDisclosure = mView.findViewById(R.id.customerAcknowledge);
        final LinearLayout disclosureMain = mView.findViewById(R.id.maineContent);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager managers = new LinearLayoutManager(getActivity());
        ArrayList<HistoryListPojo> historyListPojos = new ArrayList<>();
        isAutoCheck = getArguments().getBoolean("isAutoCheck", false);
        next = mView.findViewById(R.id.text_next);
        Gson gson = new GsonBuilder().create();
        mView.findViewById(R.id.header).setVisibility(View.GONE);
        mView.findViewById(R.id.header1).setVisibility(View.VISIBLE);
        DSTextView back = mView.findViewById(R.id.header1).findViewById(R.id.text_back_up);
        LinearLayout noticeBox = mView.findViewById(R.id.noticeBox);
        DSTextView notice = mView.findViewById(R.id.notice);
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        sign = mView.findViewById(R.id.signhere);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            isCoBuyer = true;
        }

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.header1).findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.header1).findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }

        String newOrUsed;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
            newOrUsed = "";
        else
            newOrUsed = "USED";

        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory() != null && !isAutoCheck) {
            AppConstant.historyLists.clear();
            AppConstant.historyLists.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory());

        } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport() != null && isAutoCheck) {
            AppConstant.reportLists.clear();
            AppConstant.reportLists.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport());
        }

        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory() == null) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeHistoryDisclosures() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().size() > 0) {
                ArrayList<VehicleTradeConditionDisclosures> localList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures();
                AppConstant.historyLists.clear();
                for (int i = 0; i < localList.size(); i++) {
                    HistoryListPojo historyListPojo = new HistoryListPojo();
                    historyListPojo.setChecked(true);
                    historyListPojo.setId(localList.get(i).getId());
                    historyListPojo.setAdditionalData(localList.get(i).getValue());
                    AppConstant.historyLists.add(historyListPojo);
                }
            }
        }
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport() == null) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports().size() > 0) {
                ArrayList<VehicleTradeThirdPartyHistoryReports> localList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports();
                AppConstant.reportLists.clear();
                for (int i = 0; i < localList.size(); i++) {
                    HistoryListPojo historyListPojo = new HistoryListPojo();
                    historyListPojo.setChecked(true);
                    historyListPojo.setId(localList.get(i).getId());
                    AppConstant.reportLists.add(historyListPojo);
                }
            }
        }
        if (!isAutoCheck) {
            if (NewDealViewPager.currentPage == 75) {
                takePicture();
            }
            boolean showCF = false, showAC = false;
            ArrayList<ThirdPartyObj> tpList = new ArrayList<>();
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null) {
                tpList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
            }
            for (int i = 0; i < tpList.size(); i++) {
                if (tpList.get(i).getId().equalsIgnoreCase("1") && tpList.get(i).isViewReport()) {
                    showCF = true;
                }
                if (tpList.get(i).getId().equalsIgnoreCase("2") && tpList.get(i).isViewReport()) {
                    showAC = true;
                }
            }
            ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
            ArrayList<HistoryListPojo> localLi1 = new ArrayList<>();
            if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//                    if (record.getVehicleDetails().getUsedVehicleReport() != null) {
//                        localLi1 = new ArrayList<>(record.getVehicleDetails().getUsedVehicleReport());
//                    } else {
                    for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
//                        if (thirdPartyList.getValue().get(i).getId().equalsIgnoreCase("1")&&thirdPartyList.getValue().get(i).getThirdPartyReportBytes()!=null&&thirdPartyList.getValue().get(i).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")){
//                            cfByte=true;
//                        } if (thirdPartyList.getValue().get(i).getId().equalsIgnoreCase("2")&&thirdPartyList.getValue().get(i).getThirdPartyReportBytes()!=null&&thirdPartyList.getValue().get(i).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")){
//                            acByte=true;
//                        }
                        HistoryListPojo historyListPojo = new HistoryListPojo();
                        historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                        historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                        historyListPojo.setChecked(false);
//                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isViewReport());
                        historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                        historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                        historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                        localLi1.add(historyListPojo);
                    }
//                    }

                    boolean showCarFax = false, showAutoCheck = false;
                    for (int i = 0; i < localLi1.size(); i++) {
                        if (localLi1.get(i).getId().equalsIgnoreCase("1") && showCF && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable()) {
                            showCarFax = true;
                        } else if (localLi1.get(i).getId().equalsIgnoreCase("2") && showAC && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable()) {
                            showAutoCheck = true;
                        }
                    }
                    if (showAutoCheck && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        autocheck.setVisibility(View.VISIBLE);
                        ttv2.setVisibility(View.VISIBLE);
                    } else {
                        autocheck.setVisibility(View.GONE);
                        ttv2.setVisibility(View.GONE);
                    }
                    if (showCarFax && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        carfax.setVisibility(View.VISIBLE);
                        ttv1.setVisibility(View.VISIBLE);
                    } else {
                        carfax.setVisibility(View.GONE);
                        ttv1.setVisibility(View.GONE);
                    }

                    carfax.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            checkIfReportAvailable(true, "1", false);
                        }
                    });
                    autocheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            checkIfReportAvailable(false, "2", false);
                        }
                    });
                }
            }
            thirdPartyContainer.setVisibility(View.VISIBLE);
            noticeBox.setVisibility(View.GONE);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//            newOrUsed = "";
            {
                history1.setText(R.string.vehicle_history);
            } else {
                newOrUsed = "USED";
                history1.setText(R.string.used_vehicle_history);
            }
            history.setText(R.string.history_disclosure_vehicle);
            customerDisclosure.setText(R.string.condition_disclosure_customer);
//            list = getResources().getStringArray(R.array.historyList);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistorySelection() != null) {
                if (!isCoBuyer) {
                    historyListPojos.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistorySelection());
                } else {
                    historyListPojos.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCoHistorySelection());
                }
            }
            AppConstant.history.clear();

            String langId;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "";
            } else {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId() + "";
            }
            VehicleHistoryDisclosure disclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleHistoryDisclosure();
            if (disclosure == null) {
                disclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.HISTORY_DISCLOSURE), VehicleHistoryDisclosure.class);
            }
            ArrayList<Value> vList = disclosure.getValue();
            for (int i = 0; i < historyListPojos.size(); i++) {
                for (int j = 0; j < vList.size(); j++) {
                    if (historyListPojos.get(i).getId().equalsIgnoreCase(vList.get(j).getId())) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(historyListPojos.get(i).isChecked());
                        pojo.setVerified(historyListPojos.get(i).isVerified());

                        pojo.setAdditionalData(historyListPojos.get(i).getAdditionalData());
                        pojo.setId(historyListPojos.get(i).getId());
                        if (!langId.equalsIgnoreCase("1")) {
                            ArrayList<Translations> translations = vList.get(j).getTranslations();
                            if (translations != null && translations.size() > 0) {
                                for (int k = 0; k < translations.size(); k++) {
                                    if (langId.equalsIgnoreCase(translations.get(k).getDestinationLanguageId())) {
                                        pojo.setName(translations.get(k).getText());
                                    } else if (langId.equalsIgnoreCase("0")) {
                                        pojo.setName(vList.get(j).getTitle());
                                    }
                                }
                            } else {
                                pojo.setName(historyListPojos.get(i).getName());
                            }

                        } else
                            pojo.setName(historyListPojos.get(i).getName());
                        AppConstant.history.add(pojo);
                    }
                }

            }

            //commented for bullet
//            boolean isAllSelected = false;
//            for (int i = 0; i < AppConstant.history.size(); i++) {
//
//                if (!AppConstant.history.get(i).isChecked()) {
//                    isAllSelected = AppConstant.history.get(i).isChecked();
//                    break;
//                }
//                isAllSelected = AppConstant.history.get(i).isChecked();
//            }
//            allTrue = isAllSelected;
            if (allTrue && isSigned && allAdditional) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }, 100);
            } else {
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(new HistoryAfterStickerAdapter(getActivity(), this, AppConstant.history, isAutoCheck));


            AdditionalDisclosure additionalDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalHistoryDisclosure();
            if (additionalDisclosure != null && additionalDisclosure.getValue() != null && additionalDisclosure.getValue().size() > 0) {
                AppConstant.additionalHistory.clear();
                ArrayList<Value> addList = additionalDisclosure.getValue();
                for (int j = 0; j < addList.size(); j++) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setChecked(false);
                    pojo.setVerified(addList.get(j).getIsVerified());

                    pojo.setAdditionalData("");
                    pojo.setId(addList.get(j).getId());
                    if (!langId.equalsIgnoreCase("1")) {
                        ArrayList<Translations> translations = addList.get(j).getTranslations();
                        if (translations != null && translations.size() > 0) {
                            for (int k = 0; k < translations.size(); k++) {
                                if (langId.equalsIgnoreCase(translations.get(k).getDestinationLanguageId())) {
                                    pojo.setName(translations.get(k).getText());
                                } else if (langId.equalsIgnoreCase("0")) {
                                    pojo.setName(addList.get(j).getTitle());
                                }
                            }
                        } else {
                            pojo.setName(addList.get(j).getTitle());
                        }

                    } else
                        pojo.setName(addList.get(j).getTitle());
                    AppConstant.additionalHistory.add(pojo);
                }

                additionalLL.setVisibility(View.VISIBLE);
                additionalRecyclerView.setLayoutManager(managers);
//                allAdditional = false;
                additionalRecyclerView.setAdapter(new AdditionalDisclosureAdapter(getActivity(), this, AppConstant.additionalHistory, true));
            } else {
                allAdditional = true;
                additionalLL.setVisibility(View.GONE);
            }

        } else {
            if (NewDealViewPager.currentPage == 76) {
                takePicture();
            }
            ttv1.setVisibility(View.GONE);
            ttv2.setVisibility(View.GONE);
            noticeBox.setVisibility(View.GONE);
            notice.setText(Html.fromHtml(getString(R.string.reportNotice)));
            history.setText(R.string.thirdparty_report);
            history1.setText(getString(R.string.usedVehicleReport));
            customerDisclosure.setText(R.string.customer_thirdparty_report);
            list = getResources().getStringArray(R.array.autoCheckList);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getReportSelection() != null) {
                if (!isCoBuyer) {
                    historyListPojos.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getReportSelection());
                } else {
                    historyListPojos.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCoReportSelection());
                }
            }
            AppConstant.report.clear();
            String langId;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "";
            } else {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId() + "";
            }
            ThirdPartyList disclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList();
            if (disclosure == null) {
                disclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
            }
            ArrayList<ThirdPartyObj> vList = disclosure.getValue();
            for (int i = 0; i < historyListPojos.size(); i++) {
                for (int j = 0; j < vList.size(); j++) {
                    if (historyListPojos.get(i).getId().equalsIgnoreCase(vList.get(j).getId())) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(historyListPojos.get(i).isChecked());

                        /*Changed I to J list*/
                        pojo.setVerified(vList.get(j).isActive());

                        pojo.setAdditionalData(historyListPojos.get(i).getAdditionalData());
                        if ((!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable() && historyListPojos.get(i).getId().equalsIgnoreCase("1")) || !historyListPojos.get(i).isVerified()) {
                            pojo.setViewReport(false);
                            pojo.setMandatory(false);
                        } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable() && historyListPojos.get(i).getId().equalsIgnoreCase("1") && historyListPojos.get(i).isVerified()) {
                            pojo.setViewReport(vList.get(j).isViewReport());
                            pojo.setMandatory(vList.get(j).isMandatory());
                        }
                        if ((!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable() && historyListPojos.get(i).getId().equalsIgnoreCase("2")) || !historyListPojos.get(i).isVerified()) {
                            pojo.setViewReport(false);
                            pojo.setMandatory(false);
                        } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable() && historyListPojos.get(i).getId().equalsIgnoreCase("2") && historyListPojos.get(i).isVerified()) {
                            pojo.setViewReport(vList.get(j).isViewReport());
                            pojo.setMandatory(vList.get(j).isMandatory());
                        }

                        pojo.setReportSeen(historyListPojos.get(i).isReportSeen());
                        pojo.setId(historyListPojos.get(i).getId());
                        /*Add logic for Translations*/
                        if (!langId.equalsIgnoreCase("1")) {
                            ArrayList<Translations> translations = vList.get(j).getTranslations();
                            if (translations != null && translations.size() > 0) {
                                for (int k = 0; k < translations.size(); k++) {
                                    if (langId.equalsIgnoreCase(translations.get(k).getDestinationLanguageId())) {
                                        pojo.setName(translations.get(k).getText());
                                    } else if (langId.equalsIgnoreCase("0")) {
                                        pojo.setName(vList.get(j).getTitle());
                                    }
//                                    else {
//                                        pojo.setName(vList.get(j).getTitle());
//                                    }
                                }
                            } else {
                                pojo.setName(historyListPojos.get(i).getName());
                            }
                        } else
                            pojo.setName(historyListPojos.get(i).getName());
//                        pojo.setName(historyListPojos.get(i).getName());
                        AppConstant.report.add(pojo);
                    }
                }

            }

//            AppConstant.report.addAll(historyListPojos);
            //removed for bullet

//            boolean isAllSelected = false;
////            if (PreferenceManger.getBooleanValue(AppConstant.VIEW_MANDATORY)) {
////                for (int i = 0; i < AppConstant.report.size(); i++) {
////                    if (!AppConstant.report.get(i).isChecked() || !AppConstant.report.get(i).isReportSeen()) {
////                        isAllSelected = false;
////                        break;
////                    }
////                    isAllSelected = true;
////                }
////            } else {
//            for (int i = 0; i < AppConstant.report.size(); i++) {
//                if (!AppConstant.report.get(i).isChecked()) {
//                    isAllSelected = AppConstant.report.get(i).isChecked();
//                    break;
//                }
//                isAllSelected = AppConstant.report.get(i).isChecked();
//            }
////            }
//
//            allTrue = isAllSelected;
            if (allTrue && isSigned) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }, 100);
            } else {
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
            recyclerView.setLayoutManager(manager);

            final CustomerVehicleDealAlerts alertStr = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAutoCheckAvailable();
            final CustomerVehicleDealAlerts alert1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarFaxAvailable();

            if (alertStr != null) {
                for (int i = 0; i < AppConstant.report.size(); i++) {
                    if (AppConstant.report.get(i).getId().equalsIgnoreCase(alertStr.getDocId())) {
                        AppConstant.report.get(i).setViewReport(false);
                        AppConstant.report.get(i).setMandatory(false);
                    }
                }
            }
            if (alert1 != null) {
                for (int i = 0; i < AppConstant.report.size(); i++) {
                    if (AppConstant.report.get(i).getId().equalsIgnoreCase(alert1.getDocId())) {
                        AppConstant.report.get(i).setViewReport(false);
                        AppConstant.report.get(i).setMandatory(false);
                    }
                }
            }

            thirdPartyAdapter = new HistoryAfterStickerAdapter(getActivity(), this, AppConstant.report, isAutoCheck);
            recyclerView.setAdapter(thirdPartyAdapter);
//            for (int i = 0; i < AppCons tant.reportLists.size(); i++) {
//                if (AppConstant.reportLists.get(i).isChecked()) {
//                    HistoryListPojo pojo = new HistoryListPojo();
//                    pojo.setChecked(AppConstant.reportLists.get(i).isChecked());
//                    pojo.setAdditionalData(AppConstant.reportLists.get(i).getAdditionalData());
//                    pojo.setId(AppConstant.reportLists.get(i).getId());
//                    pojo.setName(list[i]);
//                    historyListPojos.add(pojo);
//                }
//            }
        }

        disableWithGray(next);
//        next.setVisibility(View.GONE);
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                if (allTrue) {
                    if (!isAutoCheck) {
                        if (allAdditional) {
                            if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSigned) {
                                            enableWithGreen(next);
                                            next.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }, AppConstant.NEXT_DELAY);
                            } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                                enableWithGreen(next);
                                next.setVisibility(View.VISIBLE);
                            } else if (isTaken) {
                                enableWithGreen(next);
                                next.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (isSigned) {
                                        enableWithGreen(next);
                                        next.setVisibility(View.VISIBLE);
                                    }
                                }
                            }, AppConstant.NEXT_DELAY);
                        } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                            enableWithGreen(next);
                            next.setVisibility(View.VISIBLE);
                        } else if (isTaken) {
                            enableWithGreen(next);
                            next.setVisibility(View.VISIBLE);
                        }
                    }
                }
                isSigned = true;
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });
        final DSTextView signature = mView.findViewById(R.id.signature);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
        });
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        disableWithGray(next);
//                        next.setVisibility(View.GONE);
                        return true;
                    }
                }
                return true;
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                if (!isAutoCheck) {
                    if (!allTrue || !allAdditional) {
                        new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                        return;
                    }
                } else {
                    if (!allTrue) {
                        new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                        return;
                    }
                }
//                disableNext(next);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
                if (!isAutoCheck) {
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "history_disclosure_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_history_disclosure_screen");
                    }
                    scrollableScreenshot(disclosureMain, file);
                } else {
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "third_party_report_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_third_party_report_screen");
                    }
                    scrollableScreenshot(disclosureMain, file);
                }

            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        if (!isAutoCheck) {
                            svgFile = new File(path_signature + "/IMG_co_buyer_history_disclosure.svg");
                        } else {
                            svgFile = new File(path_signature + "/IMG_co_buyer_history_report.svg");
                        }
                    } else {
                        if (!isAutoCheck) {
                            svgFile = new File(path_signature + "/IMG_history_disclosure.svg");
                        } else {
                            svgFile = new File(path_signature + "/IMG_history_report.svg");
                        }
                    }
                    File file, file1;
                    if (!isAutoCheck) {
                        if (!isCoBuyer) {
                            file = getOutputFile("Screenshot", "history_disclosure_screen");
                        } else {
                            file = getOutputFile("Screenshot", "co_history_disclosure_screen");
                        }
                    } else {
                        if (!isCoBuyer) {
                            file = getOutputFile("Screenshot", "third_party_report_screen");
                        } else {
                            file = getOutputFile("Screenshot", "co_third_party_report_screen");
                        }
                    }

                    if (!isAutoCheck) {
                        if (!isCoBuyer) {
                            file1 = getOutputFile("UserPic", "history_disclosure_screen_pic");
                        } else {
                            file1 = getOutputFile("UserPic", "co_history_disclosure_screen_pic");
                        }
                    } else {
                        if (!isCoBuyer) {
                            file1 = getOutputFile("UserPic", "third_party_report_screen_pic");
                        } else {
                            file1 = getOutputFile("UserPic", "co_third_party_report_screen_pic");
                        }
                    }

                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                    ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                    ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                    int languageId = 1;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                        languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
                    else {
                        languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
                    }
                    int page = 0;
                    if (isAutoCheck) {
                        if (history.get(history.size() - 1).isChecked()) {
                            page = 74;
                        }
                        if (history.get(history.size() - 1).isChecked() && condition.get(condition.size() - 1).isChecked()) {
                            page = 73;
                        }
                    } else {
                        if (condition.get(condition.size() - 1).isChecked()) {
                            page = 73;
                        }
                    }
                    if (page != 0) {
                        playSound(languageId);
                        ((NewDealViewPager) getActivity()).setCurrentPage(page);
                    } else
                        iGoToNextPage.goToBackIndex();
                    Timber.e(" Device back pressed: %s", keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    if (!isAutoCheck) {
                        svgFile = new File(path_signature + "/IMG_co_buyer_history_disclosure.svg");
                    } else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_history_report.svg");
                    }
                } else {
                    if (!isAutoCheck) {
                        svgFile = new File(path_signature + "/IMG_history_disclosure.svg");
                    } else {
                        svgFile = new File(path_signature + "/IMG_history_report.svg");
                    }
                }
                File file, file1;
                if (!isAutoCheck) {
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "history_disclosure_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_history_disclosure_screen");
                    }
                } else {
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "third_party_report_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_third_party_report_screen");
                    }
                }

                if (!isAutoCheck) {
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "history_disclosure_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_history_disclosure_screen_pic");
                    }
                } else {
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "third_party_report_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_third_party_report_screen_pic");
                    }
                }

                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                int page = 0;
                if (isAutoCheck) {
                    if (history.get(history.size() - 1).isChecked()) {
                        page = 74;
                    }
                    if (history.get(history.size() - 1).isChecked() && condition.get(condition.size() - 1).isChecked()) {
                        page = 73;
                    }
                } else {
                    if (condition.get(condition.size() - 1).isChecked()) {
                        page = 73;
                    }
                }
                if (page != 0) {
                    ((NewDealViewPager) getActivity()).setCurrentPage(page);
                } else
                    iGoToNextPage.goToBackIndex();
            }
        });
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
        final DSTextView cancel1 = mView.findViewById(R.id.text_cancelBack1);
        logout.setVisibility(View.INVISIBLE);
        cancel.setVisibility(View.VISIBLE);

        /*Uncomment this after UI Update*/
        if (!isAutoCheck) {

            if (decidePageNumber() == 12) {
                ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                if (condition != null && condition.get(condition.size() - 1).isChecked())
                    cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
                else
                    cancel1.setText(String.format(getString(R.string.page1), "8", decidePageNumber()));

            } else if (decidePageNumber() == 11) {
                cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
            } else {
                cancel1.setText(String.format(getString(R.string.page1), "8", decidePageNumber()));
            }
        } else {
            if (decidePageNumber() == 12) {
                cancel1.setText(String.format(getString(R.string.page1), "8", decidePageNumber()));
//                ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
//                ArrayList<HistoryListPojo> historys = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
//
//                if (condition != null && condition.get(condition.size() - 1).isChecked()&&historys != null && historys.get(historys.size() - 1).isChecked())
//                    cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
//                else if (condition != null && condition.get(condition.size() - 1).isChecked())
//                    cancel1.setText(String.format(getString(R.string.page1), "9", decidePageNumber()));

            } else if (decidePageNumber() == 11) {
                cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
            } else {
                cancel1.setText(String.format(getString(R.string.page1), "9", decidePageNumber()));
            }
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    private void decideToTakePicture() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
        int page = 0;
        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
            if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                takePicture();
            }
            if (condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                takePicture();
            } else {
                if (history == null) {
                    takePicture();
                }
            }
            if (condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked() && report != null && report.get(report.size() - 1).isChecked()) {
                page = 77;
            } else {
                if (history == null && report == null) {
                    page = 77;
                }
            }
        } else {
            if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                page = 77;
            }
        }
    }

    private void checkIfReportAvailable(boolean carfax, String thirdPartyId, boolean showBox) {
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new Gson();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String vin = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
        int index = 0;
        String name = null;
        ArrayList<ThirdPartyObj> listLangName = new ArrayList<>();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null)
            listLangName = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
        for (int i = 0; i < listLangName.size(); i++) {
            if (listLangName.get(i).getId().equalsIgnoreCase(thirdPartyId)) {
                name = listLangName.get(i).getTitle();
                index = i;
            }
        }
        if (name != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes() != null &&
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")) {
            Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
            intent.putExtra("url", name);
            intent.putExtra("id", thirdPartyId);
            intent.putExtra("customerResponse", true);
            intent.putExtra("isCoBuyer", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer());
            intent.putExtra("showBox", showBox);
            intent.putExtra("isDisclosure", false);
            Objects.requireNonNull(getActivity()).startActivity(intent);
        } else {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                ArrayList<InitThirdParty> initList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
                boolean idFound = false;
                for (int i = 0; i < initList.size(); i++) {
                    if (initList.get(i).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                        showAlert(initList.get(i).getErrorsString(), thirdPartyId);
                        idFound = true;
                        break;
                    }
                }
                if (!idFound) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getInitiateThirdParty() == null || record.getVehicleDetails().getInitiateThirdParty().getValue().size() == 0) {
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            callThirdPartyInitiateAPI(vin, thirdPartyId, carfax);
                        } else {
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }
                    } else {
                        if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                            ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                            boolean isActive = false;
                            String fileName = "";
                            for (int i = 0; i < locallist.size(); i++) {
                                if (locallist.get(i).getId().equalsIgnoreCase(thirdPartyId) && locallist.get(i).isActive()) {
                                    isActive = true;
                                    fileName = locallist.get(i).getTitle();
                                    break;
                                }
                            }
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(fileName.trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (isActive) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    if (DSAPP.getInstance().isNetworkAvailable()) {
                                                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                                        progressDialog.setMessage("Please wait... We are downloading the report.");
                                                        progressDialog.show();
                                                        progressDialog.setCancelable(false);
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), thirdPartyId, fileName.trim(), progressDialog);
                                                    } else {
                                                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                    }
                                                }
                                            } else {
                                                Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                                intent.putExtra("url", fileName);
                                                intent.putExtra("id", thirdPartyId);
                                                intent.putExtra("isCoBuyer", false);
                                                intent.putExtra("customerResponse", carfax);
                                                intent.putExtra("showBox", showBox);
                                                intent.putExtra("isDisclosure", true);
                                                Objects.requireNonNull(getActivity()).startActivity(intent);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
//                            }
                        }
                    }
                }

            } else {
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                if (record.getVehicleDetails().getInitiateThirdParty() == null || record.getVehicleDetails().getInitiateThirdParty().getValue().size() == 0) {
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        callThirdPartyInitiateAPI(vin, thirdPartyId, carfax);
                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }
                } else {
                    if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                        ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < locallist.size(); i++) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(locallist.get(i).getId()) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(locallist.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (locallist.get(i).isActive()) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    if (DSAPP.getInstance().isNetworkAvailable()) {
                                                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                                        progressDialog.setMessage("Please wait... We are downloading the report.");
                                                        progressDialog.show();
                                                        progressDialog.setCancelable(false);
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), locallist.get(i).getId(), locallist.get(i).getTitle().trim(), progressDialog);
                                                    } else {
                                                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                    }
                                                }
                                            } else {
                                                Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                                intent.putExtra("url", locallist.get(i).getTitle());
                                                intent.putExtra("id", locallist.get(i).getId());
                                                intent.putExtra("isCoBuyer", false);
                                                intent.putExtra("customerResponse", carfax);
                                                intent.putExtra("isDisclosure", true);
                                                Objects.requireNonNull(getActivity()).startActivity(intent);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void showAlert(String errorsString, final String tpID) {
        if (getActivity() != null) {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
            titleTV.setVisibility(View.GONE);
            messageTV.setText(errorsString);
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file;
                    if (isCoBuyer) {
                        if (tpID.equalsIgnoreCase("1")) {
                            file = getOutputFile("Screenshot", "alert_history_response_report_fetching_error_co_buyer_cf");
                        } else {
                            file = getOutputFile("Screenshot", "alert_history_response_report_fetching_error_co_buyer_ac");
                        }
                    } else {
                        if (tpID.equalsIgnoreCase("1"))
                            file = getOutputFile("Screenshot", "alert_history_response_report_fetching_error_buyer_cf");
                        else {
                            file = getOutputFile("Screenshot", "alert_history_response_report_fetching_error_buyer_ac");
                        }
                    }
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(tpID);
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(tpID);
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    alertD.dismiss();
                }
            });

//            AlertDialog.Builder alertD = new AlertDialog.Builder(getActivity());
//            final AlertDialog alert = alertD.create();
//            alertD.setMessage(errorsString)
//                    .setIcon(android.R.drawable.ic_dialog_alert)
//                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        public void onClick(final DialogInterface dialog, int whichButton) {
//
//
//
//                        }
//                    }).show().setCancelable(false);


        }
    }

    private void callThirdPartyInitiateAPI(final String vin, final String thirdPartyId, final boolean carfax) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait... We are downloading the report.");
        progressDialog.show();
        progressDialog.setCancelable(false);
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().initiateThirdParty("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vin, id).enqueue(new Callback<InitiateThirdParty>() {
            @Override
            public void onResponse(Call<InitiateThirdParty> call, Response<InitiateThirdParty> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new Gson();
                    Log.e("Initiate TP: ", gson.toJson(response.body()));
                    if (response.body().getSucceeded().equalsIgnoreCase("true")) {
                        boolean download = false;
                        for (int i = 0; i < response.body().getValue().size(); i++) {
                            if (response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                download = true;
                            } else {
                                showAlert(response.body().getValue().get(i).getErrorsString(), thirdPartyId);
                            }
                            break;
                        }
                        if (download) {
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(response.body());
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            getThirdParty(thirdPartyId, progressDialog, carfax);
                        } else {
                            progressDialog.dismiss();
//                            new CustomToast(getActivity()).alert("Autocheck Login Expired");
                        }

                    } else {
                        progressDialog.dismiss();
                        showAlert(response.body().getErrorsString(), thirdPartyId);
//                        new CustomToast(getActivity()).alert(response.body().getErrorsString());
                    }
                }
            }

            @Override
            public void onFailure(Call<InitiateThirdParty> call, Throwable t) {
                Log.e("Initiate Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void getThirdParty(final String vin, final ProgressDialog progressDialog, final boolean carfax) {

//        new CustomToast(getActivity()).alert("Please wait we are downloading the report.");
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(@NotNull Call<ThirdPartyList> call, @NotNull Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    ThirdPartyList vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new ThirdPartyList();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setViewReport(false);
                            list.get(i).setActive(false);
                            list.get(i).setMandatory(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Timber.i(gson.toJson(vehicleConditionDisclosure));
                    }
                    String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
                    for (int i = 0; i < list.size(); i++) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                            for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(list.get(i).getId())) {
                                    File file = ShowImageFromPath(list.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                    if (list.get(i).isActive()) {
                                        if (file == null || !file.exists()) {
                                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0"))
                                                getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim(), progressDialog);
                                        } else {
                                            Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                            intent.putExtra("url", list.get(i).getTitle());
                                            intent.putExtra("id", list.get(i).getId());
                                            intent.putExtra("isCoBuyer", false);
                                            intent.putExtra("customerResponse", carfax);
                                            intent.putExtra("isDisclosure", true);
                                            Objects.requireNonNull(getActivity()).startActivity(intent);
                                            progressDialog.dismiss();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Log.e("Size of reports: ", response.body().getValue().size() + "");
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {

            }
        });
    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(getActivity())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    private void getAllThirdPartyReports(final String TPRRID, final String tpId, final String name, final ProgressDialog progressDialog) {

//        "40334", "2g1fd1e34f9207229", "1", lng
        RetrofitInitialization.getDs_services().getThirdPartyReportsBytes("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), TPRRID).enqueue(new Callback<ThirdPartyReportBytes>() {
            @Override
            public void onResponse(Call<ThirdPartyReportBytes> call, Response<ThirdPartyReportBytes> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage() != null && response.body().getMessage().length() > 0) {
                        File dwldsPath = getOutputMediaFilePDF("localPdf", name.trim() + TPRRID);
                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
                        FileOutputStream os;
                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
                        PreferenceManger.putString(AppConstant.ALERT, "");
                        try {
                            os = new FileOutputStream(dwldsPath, false);
                            os.write(pdfAsBytes);
                            os.flush();
                            os.close();
//                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {

//                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(tpId)) {
                                list.get(i).setThirdPartyReportBytes(response.body());
                            }
                        }
                        if (tpId.equalsIgnoreCase("1") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            alerts.setAlertName("CarFax Report is Not Available");
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getErrorMessage());
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                        } else if (tpId.equalsIgnoreCase("2") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getErrorMessage());
                            alerts.setAlertName("AutoCheck Report is Not Available");
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
//                        initView();
//                        adapter.notifyDataSetChanged();
                        new CustomToast(getActivity()).alert("Report Downloaded. Click again to view.");
                        progressDialog.dismiss();

//                        Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
//                        intent.putExtra("url", name);
//                        intent.putExtra("id", tpId);
//                        intent.putExtra("isCoBuyer", false);
//                        intent.putExtra("isDisclosure", true);
//                        Objects.requireNonNull(getActivity()).startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportBytes> call, Throwable t) {
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    public File getOutputMediaFilePDF(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            File svgFile;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                if (!isAutoCheck) {
                    svgFile = getOutputMediaFile("Signatures", "co_buyer_history_disclosure");
                } else {
                    svgFile = getOutputMediaFile("Signatures", "co_buyer_history_report");
                }
            } else {
                if (!isAutoCheck) {
                    svgFile = getOutputMediaFile("Signatures", "history_disclosure");
                } else {
                    svgFile = getOutputMediaFile("Signatures", "history_report");
                }
            }

            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    @Override
    public void languageSelected(boolean allSelected, boolean isLogoClicked, String thirdPartyId) {
//        allTrue = allSelected;
        if (!isAutoCheck) {
            if (allTrue && isSigned && allAdditional) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }, 100);
            } else {
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
        } else {
            if (allTrue && isSigned) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }, 100);
            } else {
//                next.setVisibility(View.GONE);
                disableWithGray(next);
            }
        }
        if (isLogoClicked) {
//            new CustomToast(getActivity()).alert("Please wait...", false);
//            FragmentTransaction ft = getFragmentManager().beginTransaction();
            final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
            Gson gson = new Gson();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            String vin = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
//
//            DialogFragment newFragment = new ThirdPartyWebView();
//            Bundle bundle = new Bundle();
//
            String url;
            if (isCoBuyer)
                url = AppConstant.BASE_URL1 + "/Resource/GetHistoryReport?dealershipId=" + id + "&VIN=" + vin + "&ThirdPartyID=" + thirdPartyId + "&lang=" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "";
            else {
                url = AppConstant.BASE_URL1 + "/Resource/GetHistoryReport?dealershipId=" + id + "&VIN=" + vin + "&ThirdPartyID=" + thirdPartyId + "&lang=" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId() + "";
            }

////            String url = "http://stg.dealerxt.com/Resource/GetHistoryReport?dealershipId=40335&VIN=WBAFR9C59BC270614&ThirdPartyID=2";
//            bundle.putString("url", url);
//            bundle.putString("id", thirdPartyId);
//            newFragment.setArguments(bundle);
//            newFragment.show(ft, "dialog");

//            if (DSAPP.getInstance().isNetworkAvailable()) {
//                Intent intent = new Intent(getActivity(), WebViewActivity.class);
//                intent.putExtra("url", url);
//                intent.putExtra("id", thirdPartyId);
//                intent.putExtra("isCoBuyer", isCoBuyer);
//                Objects.requireNonNull(getActivity()).startActivity(intent);
//            } else {


//            enableNext(next);
            int index = 0;
            String name = null;
            ArrayList<ThirdPartyObj> listLangName = new ArrayList<>();
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null)
                listLangName = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
            for (int i = 0; i < listLangName.size(); i++) {
                if (listLangName.get(i).getId().equalsIgnoreCase(thirdPartyId)) {
                    name = listLangName.get(i).getTitle();
                    index = i;
                }
            }
//            for (int i = 0; i < AppConstant.report.size(); i++) {
//                if (AppConstant.report.get(i).getId().equalsIgnoreCase(thirdPartyId)) {
//                    name = AppConstant.report.get(i).getName();
//                    index = i;
//                    break;
//                }
//            }
            if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_MULTI_SIGN) && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).isMandatory()) {
                if (name != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyList() != null &&
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyList().getSucceeded().equalsIgnoreCase("true")) {
                    Intent intent = new Intent(getActivity(), MultiSignViewPager.class);
                    intent.putExtra("url", name);
                    intent.putExtra("id", thirdPartyId);
                    intent.putExtra("isCoBuyer", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer());
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                } else {
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        getThirdParty();
                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }
                }
            } else {
                if (name != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes() != null &&
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")) {
                    Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                    intent.putExtra("url", name);
                    intent.putExtra("id", thirdPartyId);
                    intent.putExtra("isCoBuyer", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer());
                    intent.putExtra("customerResponse", true);
                    intent.putExtra("showBox", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).isMandatory());
                    intent.putExtra("isDisclosure", false);
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                } else {
                    checkIfReportAvailable(true, thirdPartyId, recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).isMandatory());
                }
            }
        }

    }


    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
        if (isCoBuyer) {
            if (!isAutoCheck) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoHistorySelection(AppConstant.history);
            } else {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoReportSelection(AppConstant.report);
            }
        } else {
            if (!isAutoCheck) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistorySelection(AppConstant.history);
            } else {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setReportSelection(AppConstant.report);
            }
        }
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
        int page = 0;
        int languageId = 1;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
            languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
        else {
            languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
        }
        if (!isAutoCheck) {
            if (report != null && report.get(report.size() - 1).isChecked()) {
                page = 77;
                playSound(languageId);
            } else {
                if (report == null) {
                    page = 77;
                    playSound(languageId);
                }
            }
            if (page != 0) {
                ((NewDealViewPager) getActivity()).setCurrentPage(page);
            } else
                iGoToNextPage.whatNextClick();
        } else
            iGoToNextPage.whatNextClick();
    }

    private void getThirdParty() {
        new CustomToast(getActivity()).alert("Initiating download...");
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    if (list != null) {
                        ThirdPartyList vehicleConditionDisclosure;
                        if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                            vehicleConditionDisclosure = new ThirdPartyList();
                            for (int i = 0; i < list.size(); i++) {
                                list.get(i).setViewReport(false);
                                list.get(i).setActive(false);
                                list.get(i).setMandatory(false);
                            }
                            vehicleConditionDisclosure.setValue(list);
                        } else
                            vehicleConditionDisclosure = response.body();
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            Timber.i(gson.toJson(vehicleConditionDisclosure));
                        }
//                    ArrayList<ThirdPartyObj> list = record.getVehicleDetails().getThirdPartyList().getValue();
                        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
//                    for (int i = 0; i < list.size(); i++) {
//                        File file = ShowImageFromPath(list.get(i).getTitle().trim() + "1.pdf", "localPdf");
//                        if (list.get(i).isViewReport())
//                            if (file == null || !file.exists()) {
//                                getAllThirdPartyReports(id, vin, list.get(i).getId(), "1", list.get(i).getTitle().trim());
//                                getAllThirdPartyReports(id, vin, list.get(i).getId(), "3", list.get(i).getTitle().trim());
//                            }
//                    }

                        for (int i = 0; i < list.size(); i++) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {

                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(list.get(i).getId()) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(list.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (list.get(i).isActive()) {
//                                            if (file == null || !file.exists()) {
//                                                getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim());
//                                            }
                                            if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_MULTI_SIGN)) {
                                                if (DSAPP.getInstance().isNetworkAvailable())
                                                    getAllThirdPartyReportList(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim());
                                                else {
                                                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
//                    Log.e("Size of reports: ", response.body().getValue().size() + "");
                    }
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {

            }
        });
    }

    private void getAllThirdPartyReportList(String thirdPartyReportRawId, String id, String name) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait... We are downloading the report.");
        progressDialog.show();
        progressDialog.setCancelable(false);
        RetrofitInitialization.getDs_services().GetHistoryReportRawPdfBytesList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), thirdPartyReportRawId).enqueue(new Callback<ThirdPartyReportList>() {
            @Override
            public void onResponse(Call<ThirdPartyReportList> call, Response<ThirdPartyReportList> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage().size() != 0) {
                        ArrayList<String> reportList = new ArrayList<>(response.body().getMessage());
                        for (int i = 0; i < reportList.size(); i++) {
                            File dwldsPath = getOutputMediaFile(name + thirdPartyReportRawId, name.trim() + i);
                            byte[] pdfAsBytes = Base64.decode(reportList.get(i), 0);
                            FileOutputStream os;
                            try {
                                os = new FileOutputStream(dwldsPath, false);
                                os.write(pdfAsBytes);
                                os.flush();
                                os.close();
                                Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
//                        File dwldsPath = getOutputMediaFile(name, name.trim() + TPRRID);
//                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
//                        FileOutputStream os;
//                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
//                        PreferenceManger.putString(AppConstant.ALERT, "");
//                        if (tpId.equalsIgnoreCase("1") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
//                        } else if (tpId.equalsIgnoreCase("2") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
//                        }
//                        String recordDataString = gson.toJson(recordData);
//                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                        try {
//                            os = new FileOutputStream(dwldsPath, false);
//                            os.write(pdfAsBytes);
//                            os.flush();
//                            os.close();
////                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    }

                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(id)) {
                                list.get(i).setThirdPartyList(response.body());
                            }
                        }

                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        progressDialog.dismiss();
                        Intent intent = new Intent(getActivity(), MultiSignViewPager.class);
                        intent.putExtra("url", name);
                        intent.putExtra("id", thirdPartyReportRawId);
                        intent.putExtra("isCoBuyer", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer());
                        Objects.requireNonNull(getActivity()).startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportList> call, Throwable t) {
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void languageSelected(boolean allSelected, String id) {
//        allAdditional = allSelected;
        if (allTrue && isSigned && allAdditional) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    enableWithGreen(next);
                    next.setVisibility(View.VISIBLE);
                }
            }, 100);
        } else {
            disableWithGray(next);
//            next.setVisibility(View.GONE);
        }
    }
}
