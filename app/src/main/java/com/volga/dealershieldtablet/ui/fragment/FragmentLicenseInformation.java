package com.volga.dealershieldtablet.ui.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckIDRequest;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentLicenseInformation extends BaseFragment {


    private View mView;
    private DSTextView dateOfBirth, dmvExpiry, fname, lname, dlname;
    private DSEdittext firstName, middleName, lastName, addressLine1, addressLine2, city, state, country, dmvNumber, zip;
    private Spinner genderSpinner;
    private Gson gson = new GsonBuilder().create();
    private RecordData recordData;
    private boolean checkId = false;
    private int mYear, mMonth, mDay;
    private DSTextView back;
    private DSTextView next;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.license_information, container, false);
        initView();
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (fname != null) {
                setEmptyData();
                Gson gson = new GsonBuilder().create();
                next.setVisibility(View.VISIBLE);
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                updateData();
                textColorManage();
            }
        }

    }

    private void updateData() {
//        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
//            CustomerDetails customerDetails = null;
//            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
//                customerDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
//            }
//            if (customerDetails == null) {
//                setEmptyData();
//            } else {
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                    updateCoBuyerDataWithDB();
//                } else {
//                    updateDataWithDB();
//                }
//            }
//
//        } else {
        if (firstName != null && !com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity.skipped) {

//                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
//                customerDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
//            }
//            if (customerDetails == null) {
//                setEmptyData();
//            } else {
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                    updateCoBuyerDataWithDB();
//                } else {
//                    updateDataWithDB();
//                }
//            }
            if (PreferenceManger.getStringValue("ID_Number").trim().length() > 0) {
                visibleHintCalled = true;
                RecordData recordData;
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                    CoBuyerCustomerDetails coBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails();
                    CustomerDetails buyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && coBuyer != null) {
                        firstName.setText(coBuyer.getFirstName());
                        lastName.setText(coBuyer.getLastName());
                        firstName.setEnabled(false);
                        lastName.setEnabled(false);
                    } else if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && buyer != null) {
                        firstName.setText(buyer.getFirstName());
                        lastName.setText(buyer.getLastName());
                        firstName.setEnabled(false);
                        lastName.setEnabled(false);
                    }
                } else {
                    firstName.setEnabled(true);
                    lastName.setEnabled(true);
                    firstName.setText(PreferenceManger.getStringValue("Customer_First_Name"));
                    lastName.setText(PreferenceManger.getStringValue("Customer_Family_Name"));
                }
                middleName.setText(PreferenceManger.getStringValue("Customer_Middle_Name(s)"));
                dateOfBirth.setText(PreferenceManger.getStringValue("Date_of_Birth"));
                addressLine1.setText(PreferenceManger.getStringValue("Address1"));
                city.setText(PreferenceManger.getStringValue("City"));
                zip.setText(PreferenceManger.getStringValue("Postal_Code"));
                state.setText(PreferenceManger.getStringValue("State"));
                country.setText(PreferenceManger.getStringValue("Country"));
                dmvNumber.setText(PreferenceManger.getStringValue("ID_Number"));
                dmvExpiry.setText(PreferenceManger.getStringValue("Document_Expiration_Date"));
                if (PreferenceManger.getStringValue("Gender").trim().length() > 0 && !PreferenceManger.getStringValue("Gender").trim().contains("*")) {
                    int gender_position = 3;
                    try {
                        gender_position = Integer.parseInt(PreferenceManger.getStringValue("Gender"));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        genderSpinner.setSelection(gender_position - 1);
                    }
                    if (gender_position == 9) {
                        gender_position = 3;
                    }
                    genderSpinner.setSelection(gender_position - 1);
                } else {
                    genderSpinner.setSelection(2);
                }
            } else {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    updateCoBuyerDataWithDB();
                } else {
                    updateDataWithDB();
                }
            }
        }
        if (firstName != null && com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity.skipped) {
            com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity.skipped = false;
            clearDMVPreference();

//            CustomerDetails customerDetails = null;
//            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
//                customerDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
//            }
//            if (customerDetails == null) {
            setEmptyData();
//            } else {
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                    updateCoBuyerDataWithDB();
//                } else {
//                    updateDataWithDB();
//                }
//            }
        }
    }
//    }

    private void setEmptyData() {
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
            CoBuyerCustomerDetails coBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails();
            CustomerDetails buyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && coBuyer != null) {
                firstName.setText(coBuyer.getFirstName());
                lastName.setText(coBuyer.getLastName());
                firstName.setEnabled(false);
                lastName.setEnabled(false);
            } else if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && buyer != null) {
                firstName.setText(buyer.getFirstName());
                lastName.setText(buyer.getLastName());
                firstName.setEnabled(false);
                lastName.setEnabled(false);
            }
        } else {
            firstName.setEnabled(true);
            lastName.setEnabled(true);
            firstName.setText("");
            lastName.setText("");
        }
//        firstName.setText("");
        middleName.setText("");
//        lastName.setText("");
        dateOfBirth.setText("");
        addressLine1.setText("");
        addressLine2.setText("");
        city.setText("");
        zip.setText("");
        state.setText("");
        country.setText("");
        dmvNumber.setText("");
        dmvExpiry.setText("");
        genderSpinner.setSelection(2);
    }

    private void updateDataWithDB() {
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        CustomerDetails customerDetails = null;
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
            customerDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
        }

        if (firstName != null && customerDetails != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDrivingLicenceNumber() != null && !com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity.skipped) {
            visibleHintCalled = true;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                CoBuyerCustomerDetails coBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails();
                CustomerDetails buyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
                if (  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && coBuyer != null) {
                    firstName.setText(coBuyer.getFirstName());
                    lastName.setText(coBuyer.getLastName());
                    firstName.setEnabled(false);
                    lastName.setEnabled(false);
                } else if ( !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && buyer != null) {
                    firstName.setText(buyer.getFirstName());
                    lastName.setText(buyer.getLastName());
                    firstName.setEnabled(false);
                    lastName.setEnabled(false);
                }
            } else {
                firstName.setEnabled(true);
                lastName.setEnabled(true);
                firstName.setText(customerDetails.getFirstName());
                lastName.setText(customerDetails.getLastName());
            }
//            firstName.setText(getCorrectText(customerDetails.getFirstName()));
            middleName.setText(getCorrectText(customerDetails.getMiddleName()));
//            lastName.setText(getCorrectText(customerDetails.getLastName()));
            dateOfBirth.setText(getCorrectText(customerDetails.getDateOfBirth()));
            addressLine1.setText(getCorrectText(customerDetails.getAddressLineOne()));
            addressLine2.setText(getCorrectText(customerDetails.getAddressLineTwo()));
            city.setText(getCorrectText(customerDetails.getCity()));
            zip.setText(getCorrectText(customerDetails.getZipCode()));
            state.setText(getCorrectText(customerDetails.getState()));
            country.setText(getCorrectText(customerDetails.getCountry()));
            dmvNumber.setText(getCorrectText(customerDetails.getDrivingLicenceNumber()));
            dmvExpiry.setText(getCorrectText(customerDetails.getDLExpiryDate()));
            if (customerDetails.getGender()!=null&&customerDetails.getGender().equalsIgnoreCase("male")) {
                genderSpinner.setSelection(0);
            } else if (customerDetails.getGender()!=null&&customerDetails.getGender().equalsIgnoreCase("female")) {
                genderSpinner.setSelection(1);
            } else {
                genderSpinner.setSelection(2);
            }
//            if (PreferenceManger.getStringValue("Gender").trim().length() > 0 && !PreferenceManger.getStringValue("Gender").trim().contains("*")) {
//                int gender_position = 3;
//                try {
//                    gender_position = Integer.parseInt(PreferenceManger.getStringValue("Gender"));
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();
//                    genderSpinner.setSelection(gender_position - 1);
//                }
//                if (gender_position == 9) {
//                    gender_position = 3;
//                }
//                genderSpinner.setSelection(gender_position - 1);
//            } else {
//                genderSpinner.setSelection(2);
//            }
        } else {
            setEmptyData();
        }
    }

    private String getCorrectText(String string) {
        if (string == null) {
            return "";
        } else if (string.equalsIgnoreCase("null")) {
            return "";
        } else {
            return string;
        }
    }

    private void updateCoBuyerDataWithDB() {
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        CoBuyerCustomerDetails customerDetails = null;
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
            customerDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails();
        }

        if (firstName != null && customerDetails != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getDrivingLicenseNumber() != null) {
            visibleHintCalled = true;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                CoBuyerCustomerDetails coBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails();
                CustomerDetails buyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && coBuyer != null) {
                    firstName.setText(coBuyer.getFirstName());
                    lastName.setText(coBuyer.getLastName());
                    firstName.setEnabled(false);
                    lastName.setEnabled(false);
                } else if ( !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer() && buyer != null) {
                    firstName.setText(buyer.getFirstName());
                    lastName.setText(buyer.getLastName());
                    firstName.setEnabled(false);
                    lastName.setEnabled(false);
                }
            } else {
                firstName.setEnabled(true);
                lastName.setEnabled(true);
                firstName.setText(getCorrectText(customerDetails.getFirstName()));
                lastName.setText(getCorrectText(customerDetails.getLastName()));
//                firstName.setText(PreferenceManger.getStringValue("Customer_First_Name"));
//                lastName.setText(PreferenceManger.getStringValue("Customer_Family_Name"));
            }
//            firstName.setText(getCorrectText(customerDetails.getFirstName()));
            middleName.setText(getCorrectText(customerDetails.getMiddleName()));
//            lastName.setText(getCorrectText(customerDetails.getLastName()));
            dateOfBirth.setText(getCorrectText(customerDetails.getDateOfBirth()));
            addressLine1.setText(getCorrectText(customerDetails.getAddressLineOne()));
            addressLine2.setText(getCorrectText(customerDetails.getAddressLineTwo()));
            city.setText(getCorrectText(customerDetails.getCity()));
            zip.setText(getCorrectText(customerDetails.getZipCode()));
            state.setText(getCorrectText(customerDetails.getState()));
            country.setText(getCorrectText(customerDetails.getCountry()));
            dmvNumber.setText(getCorrectText(customerDetails.getDrivingLicenseNumber()));
            dmvExpiry.setText(getCorrectText(customerDetails.getDLExpiryDate()));
//            genderSpinner.setSelection(customerDetails.getGender());
            if (customerDetails.getGender().equalsIgnoreCase("male")) {
                genderSpinner.setSelection(0);
            } else if (customerDetails.getGender().equalsIgnoreCase("female")) {
                genderSpinner.setSelection(1);
            } else {
                genderSpinner.setSelection(2);
            }
//            if (PreferenceManger.getStringValue("Gender").trim().length() > 0 && !PreferenceManger.getStringValue("Gender").trim().contains("*")) {
//                int gender_position = 3;
//                try {
//                    gender_position = Integer.parseInt(PreferenceManger.getStringValue("Gender"));
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();
//                    genderSpinner.setSelection(gender_position - 1);
//                }
//                if (gender_position == 9) {
//                    gender_position = 3;
//                }
//                genderSpinner.setSelection(gender_position - 1);
//            } else {
//                genderSpinner.setSelection(2);
//            }
        } else {
            setEmptyData();
        }
    }

    private void initView() {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        back = mView.findViewById(R.id.text_cancelBack);
        next = mView.findViewById(R.id.text_next);
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//recordData.where(PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN)).getDealershipDetails().g

        if (getArguments() != null)
            checkId = getArguments().getBoolean("checkId");
        if (checkId) {
            back.setVisibility(View.INVISIBLE);
            logout.setVisibility(View.INVISIBLE);
            if (!PreferenceManger.getBooleanValue(AppConstant.CHECK_ID)) {
                next.setText("Save");
            }
        }
        if (checkId || (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())) {
            back.setVisibility(View.INVISIBLE);
        } else {
            back.setVisibility(View.VISIBLE);
        }
//        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
//            back.setVisibility(View.INVISIBLE);
//        } else {
//            back.setVisibility(View.VISIBLE);
//
//        }
        DSTextView cancel = mView.findViewById(R.id.text_back_up);
//        back.setVisibility(View.GONE);
        firstName = mView.findViewById(R.id.firstName);
        middleName = mView.findViewById(R.id.middleName);
        lastName = mView.findViewById(R.id.lastName);
        fname = mView.findViewById(R.id.fNameTitle);
        lname = mView.findViewById(R.id.lNameTitle);
        dlname = mView.findViewById(R.id.dlNameTitle);
        dateOfBirth = mView.findViewById(R.id.dateOfBirth);
        textColorManage();
        dateOfBirth.setClickable(true);
        DSTextView title = mView.findViewById(R.id.title);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            title.setText(Html.fromHtml(getString(R.string.edit_information_if_incorrect)));
        } else {
            title.setText(Html.fromHtml(getString(R.string.co_edit_information_if_incorrect)));
        }
        if (checkId) {
            title.setText(Html.fromHtml(getString(R.string.edit_information_if_incorrect)));
        }
        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseDate(dateOfBirth);
            }
        });
        addressLine1 = mView.findViewById(R.id.addressLine1);
        addressLine2 = mView.findViewById(R.id.addressLine2);
        city = mView.findViewById(R.id.city);
        state = mView.findViewById(R.id.state);
        country = mView.findViewById(R.id.country);
        dmvNumber = mView.findViewById(R.id.DMV_Number);
        zip = mView.findViewById(R.id.zip);
        dmvExpiry = mView.findViewById(R.id.DMV_Expiry);
        dmvExpiry.setClickable(true);
        dmvExpiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseBDate(dmvExpiry);
            }
        });
        genderSpinner = mView.findViewById(R.id.genderSpinner);


        String[] items = new String[]{"Male", "Female", "Not Specified"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_row, items);
        genderSpinner.setAdapter(adapter);
        genderSpinner.setSelection(2);
        adapter.notifyDataSetChanged();

        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        if (!visibleHintCalled) {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                updateDataWithDB();
            } else {
                updateCoBuyerDataWithDB();
            }
        }
        mView.findViewById(R.id.container).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });
    }

    private void textColorManage() {
        if (PreferenceManger.getBooleanValue(AppConstant.SKIPPED)) {
            fname.setTextColor(getResources().getColor(R.color.back_red));
            lname.setTextColor(getResources().getColor(R.color.back_red));
            dlname.setTextColor(getResources().getColor(R.color.back_red));
        } else {
            fname.setTextColor(getResources().getColor(R.color.white));
            lname.setTextColor(getResources().getColor(R.color.white));
            dlname.setTextColor(getResources().getColor(R.color.white));
        }
    }


    private DealershipDetails dealershipDetails;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    back.setEnabled(false);
                    back.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            back.setEnabled(true);
                            back.setClickable(true);
                        }
                    }, 6000);
                    if (isDataValid()) {
                        if (checkId) {
                            next.setEnabled(false);
                            next.setClickable(false);

                            KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                            SimpleDateFormat originalFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
//                            SimpleDateFormat originalFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                            SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                            SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                            /*yyyy-MM-dd HH:mm:ss*/
                            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

                            CheckIDRequest checkIDRequest = new CheckIDRequest();

                            checkIDRequest.setDrivingLicenseNumber(dmvNumber.getText().toString().trim());
                            checkIDRequest.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
                            checkIDRequest.setFirstName(firstName.getText().toString().trim());
                            checkIDRequest.setMiddleName(middleName.getText().toString().trim());
                            checkIDRequest.setLastName(lastName.getText().toString().trim());
                            checkIDRequest.setAddress1(addressLine1.getText().toString().trim());
                            checkIDRequest.setAddress2(addressLine2.getText().toString().trim());
                            checkIDRequest.setCity(city.getText().toString().trim());
                            checkIDRequest.setState(state.getText().toString().trim());
                            checkIDRequest.setCountry(country.getText().toString().trim());
                            String format = "yyyy-MM-dd HH:mm:ss";
                            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                            long dateInMillis = System.currentTimeMillis();
                            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                            String dateString = sdf.format(new Date(dateInMillis));
                            checkIDRequest.setTradeCompletionUTC(dateString);
                            try {
                                checkIDRequest.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(dateOfBirth.getText().toString().trim())));
                                checkIDRequest.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString())));
                                checkIDRequest.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(dmvExpiry.getText().toString().trim())));

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            int genderID;
                            if (genderSpinner.getSelectedItem().toString().trim().toLowerCase().equalsIgnoreCase("male"))
                                genderID = 1;
                            else if (genderSpinner.getSelectedItem().toString().trim().toLowerCase().equalsIgnoreCase("female"))
                                genderID = 2;
                            else {
                                genderID = 3;
                            }
                            checkIDRequest.setGenderTypeId(genderID);
                            checkIDRequest.setZipcode(zip.getText().toString().trim());

                            Log.e("check id request ", gson.toJson(checkIDRequest));
//                            checkIDRequest
                            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Saving data...");
                            progressDialog.show();
                            RetrofitInitialization.getDs_services().saveCustomer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), checkIDRequest).enqueue(new Callback<ImageUploadResponse>() {
                                @Override
                                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {

                                    if (response.code() == 200 && response.isSuccessful()) {
//                                        "IMG_DLFRONT.jpg", "LicenseFront"
                                        String path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + "LicenseFront" + "/" + "IMG_DLFRONT.jpg";
//        }
                                        File imgFile = new File(path);
                                        startImageSyncForRecursion(imgFile, response.body().getValue(), progressDialog, response.body().getMessage());

                                    }
                                }

                                @Override
                                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                                    next.setEnabled(true);
                                    next.setClickable(true);
                                    next.setVisibility(View.VISIBLE);
                                    progressDialog.dismiss();
                                }
                            });
                        } else {
                            KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                                record.getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                                record.getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                                record.getCustomerDetails().setDrivingLicenceNumber(dmvNumber.getText().toString().trim());
                                record.getCustomerDetails().setFirstName(firstName.getText().toString().trim());
                                record.getCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                                record.getCustomerDetails().setLastName(lastName.getText().toString().trim());
                                record.getCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                                record.getCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                                record.getCustomerDetails().setCity(city.getText().toString().trim());
                                record.getCustomerDetails().setState(state.getText().toString().trim());
                                record.getCustomerDetails().setCountry(country.getText().toString().trim());
                                record.getCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                                record.getCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                                record.getCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                                record.getCustomerDetails().setZipCode(zip.getText().toString().trim());
                                record.getVehicleDetails().setBuyerFistName(firstName.getText().toString().trim());
                                record.getVehicleDetails().setBuyerLastName(lastName.getText().toString().trim());
                                int index = recordData.getIndexNumber(record.getCustomerDetails().getDLNumber());
                                recordData.getRecords().set(index, record);
                            } else {
                                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));

                                record.getCoBuyerCustomerDetails().setDLNumber(dmvNumber.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setFirstName(firstName.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setLastName(lastName.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setCity(city.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setState(state.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setCountry(country.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                                record.getCoBuyerCustomerDetails().setZipCode(zip.getText().toString().trim());
                                record.getVehicleDetails().setCoFirstName(firstName.getText().toString().trim());
                                record.getVehicleDetails().setCoLastName(lastName.getText().toString().trim());
                                int index = recordData.getIndexNumber(record.getCustomerDetails().getDLNumber());
                                recordData.getRecords().set(index, record);
                            }
                            //Add the recordData object to the SharedPreference after converting it to string
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            clearDMVPreference();
                            cancel();
                        }
                    }

                    break;
                case R.id.text_next:
                    if (isDataValid()) {
                        if (checkId) {
                            next.setVisibility(View.GONE);
                            SimpleDateFormat originalFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
//                            SimpleDateFormat originalFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                            SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                            SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                            /*yyyy-MM-dd HH:mm:ss*/
                            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

                            CheckIDRequest checkIDRequest = new CheckIDRequest();

                            checkIDRequest.setDrivingLicenseNumber(dmvNumber.getText().toString().trim());
                            checkIDRequest.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
                            checkIDRequest.setFirstName(firstName.getText().toString().trim());
                            checkIDRequest.setMiddleName(middleName.getText().toString().trim());
                            checkIDRequest.setLastName(lastName.getText().toString().trim());
                            checkIDRequest.setAddress1(addressLine1.getText().toString().trim());
                            checkIDRequest.setAddress2(addressLine2.getText().toString().trim());
                            checkIDRequest.setCity(city.getText().toString().trim());
                            checkIDRequest.setState(state.getText().toString().trim());
                            checkIDRequest.setCountry(country.getText().toString().trim());
                            String format = "yyyy-MM-dd HH:mm:ss";
                            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                            long dateInMillis = System.currentTimeMillis();
                            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                            String dateString = sdf.format(new Date(dateInMillis));
                            checkIDRequest.setTradeCompletionUTC(dateString);
                            try {
                                if (dateOfBirth.getText().toString().trim().length() > 0) {
                                    checkIDRequest.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(dateOfBirth.getText().toString().trim())));
                                }
                                checkIDRequest.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString())));
                                if (dmvExpiry.getText().toString().trim().length() > 0) {
                                    checkIDRequest.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(dmvExpiry.getText().toString().trim())));
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            int genderID;
                            if (genderSpinner.getSelectedItem().toString().trim().toLowerCase().equalsIgnoreCase("male"))
                                genderID = 1;
                            else if (genderSpinner.getSelectedItem().toString().trim().toLowerCase().equalsIgnoreCase("female"))
                                genderID = 2;
                            else {
                                genderID = 3;
                            }
                            checkIDRequest.setGenderTypeId(genderID);
                            checkIDRequest.setZipcode(zip.getText().toString().trim());
                            if (!PreferenceManger.getBooleanValue(AppConstant.CHECK_ID)) {

                                Log.e("check id request ", gson.toJson(checkIDRequest));
                                if (DSAPP.getInstance().isNetworkAvailable()) {
                                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                    progressDialog.setMessage("Saving data...");
                                    progressDialog.show();
                                    RetrofitInitialization.getDs_services().saveCustomer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), checkIDRequest).enqueue(new Callback<ImageUploadResponse>() {
                                        @Override
                                        public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
//                                    progressDialog.dismiss();
                                            if (response.code() == 200 && response.isSuccessful()) {
                                                Log.e("Vehicle Trade ID :", "onResponse: " + response.body().getValue());
                                                String path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + "LicenseFront" + "/" + "IMG_DLFRONT.jpg";
//        }
                                                File imgFile = new File(path);
                                                startImageSyncForRecursion(imgFile, response.body().getValue(), progressDialog, response.body().getMessage());
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                                            progressDialog.dismiss();
                                            next.setVisibility(View.VISIBLE);
                                        }
                                    });
                                } else {
                                    next.setVisibility(View.VISIBLE);
                                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                }
                            } else {
                                Gson gson = new GsonBuilder().create();
                                String dealershipDataString = gson.toJson(checkIDRequest);
                                PreferenceManger.putString(AppConstant.CHECK_ID_DATA, dealershipDataString);
//                                clearDMVPreference();
                                iGoToNextPage.whatNextClick();
                            }
                        } else {
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                                record.getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                                record.getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                                record.getCustomerDetails().setDrivingLicenceNumber(dmvNumber.getText().toString().trim());
                                record.getCustomerDetails().setFirstName(firstName.getText().toString().trim());
                                record.getCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                                record.getCustomerDetails().setLastName(lastName.getText().toString().trim());
                                record.getCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                                record.getCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                                record.getCustomerDetails().setCity(city.getText().toString().trim());
                                record.getCustomerDetails().setState(state.getText().toString().trim());
                                record.getCustomerDetails().setCountry(country.getText().toString().trim());
                                record.getCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                                record.getCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                                record.getCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                                record.getCustomerDetails().setZipCode(zip.getText().toString().trim());
                                record.getVehicleDetails().setBuyerFistName(firstName.getText().toString().trim());
                                record.getVehicleDetails().setBuyerLastName(lastName.getText().toString().trim());
                                int index = recordData.getIndexNumber(record.getCustomerDetails().getDLNumber());
                                recordData.getRecords().set(index, record);
//                            recordData.getRecords().add(record);
                            } else {
                                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));

                                record.getCoBuyerCustomerDetails().setDLNumber(dmvNumber.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setFirstName(firstName.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setLastName(lastName.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setCity(city.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setState(state.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setCountry(country.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                                record.getCoBuyerCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                                record.getCoBuyerCustomerDetails().setZipCode(zip.getText().toString().trim());
                                record.getVehicleDetails().setCoFirstName(firstName.getText().toString().trim());
                                record.getVehicleDetails().setCoLastName(lastName.getText().toString().trim());
                                int index = recordData.getIndexNumber(record.getCustomerDetails().getDLNumber());
                                recordData.getRecords().set(index, record);
                            }

                            //Add the recordData object to the SharedPreference after converting it to string
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            clearDMVPreference();
                            iGoToNextPage.whatNextClick();
                        }

                    }

                    break;
                case R.id.text_back_up:
                    iGoToNextPage.goToBackIndex();
                    break;
                case R.id.txt_logout:
                    if (isDataValid()) {
                        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                            record.getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                            record.getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                            record.getCustomerDetails().setDrivingLicenceNumber(dmvNumber.getText().toString().trim());
                            record.getCustomerDetails().setFirstName(firstName.getText().toString().trim());
                            record.getCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                            record.getCustomerDetails().setLastName(lastName.getText().toString().trim());
                            record.getCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                            record.getCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                            record.getCustomerDetails().setCity(city.getText().toString().trim());
                            record.getCustomerDetails().setState(state.getText().toString().trim());
                            record.getCustomerDetails().setCountry(country.getText().toString().trim());
                            record.getCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                            record.getCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                            record.getCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                            record.getCustomerDetails().setZipCode(zip.getText().toString().trim());
                            int index = recordData.getIndexNumber(record.getCustomerDetails().getDLNumber());
                            recordData.getRecords().set(index, record);
                        } else {
                            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));

                            record.getCoBuyerCustomerDetails().setDLNumber(dmvNumber.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setFirstName(firstName.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setLastName(lastName.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setCity(city.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setState(state.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setCountry(country.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                            record.getCoBuyerCustomerDetails().setZipCode(zip.getText().toString().trim());
                            int index = recordData.getIndexNumber(record.getCustomerDetails().getDLNumber());
                            recordData.getRecords().set(index, record);
                        }

                        //Add the recordData object to the SharedPreference after converting it to string
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                        logout();
                        clearDMVPreference();
                    }

                    break;
            }
        }
    };

    IGoToNextPage iGoToNextPage;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    private boolean isDataValid() {
        String regx = "^[\\p{L} .'-]+$";
        int month = 0, date = 0, year = 0;
        if (dateOfBirth.getText().toString().length() > 0) {
            String dob = dateOfBirth.getText().toString();
            String str[] = dob.split("-");
            month = Integer.parseInt(str[0]);
            date = Integer.parseInt(str[1]);
            year = Integer.parseInt(str[2]);
        }
        int month1 = 0, date1 = 0, year1 = 0;
        if (dmvExpiry.getText().toString().length() > 0) {
            String dob = dmvExpiry.getText().toString();
            String str[] = dob.split("-");
            month1 = Integer.parseInt(str[0]);
            date1 = Integer.parseInt(str[1]);
            year1 = Integer.parseInt(str[2]);
        }
        if (firstName.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.first_name_empty));
            return false;
        } else if (!firstName.getText().toString().trim().matches(regx)) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_first_name));
            return false;
        } else if (lastName.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.lastName_empty));
            return false;
        } else if (!lastName.getText().toString().trim().matches(regx)) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_last_name));
            return false;
        } else if (dmvNumber.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_dmv_no));
            return false;
        } else if (addressLine1.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_address));
            return false;
        } else if (zip.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_zip));
            return false;
        } else if (dateOfBirth.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_bday));
            return false;
        } else if (dateOfBirth.getText().toString().length() > 0 && !((month > 0 && month <= 12) && (date > 0 && date <= 31) && (year > 1900 && year < Calendar.getInstance().get(Calendar.YEAR)))) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_bday));
            return false;
        } else if (city.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_city));
            return false;
        } else if (state.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_state));
            return false;
        } else if (country.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_country));
            return false;
        } else if (dmvNumber.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_dmv_no));
            return false;
        } else if (dmvExpiry.getText().toString().contains("*")) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_expiry));
            return false;
        } else if (dmvExpiry.getText().toString().length() > 0 && !((month1 > 0 && month1 <= 12) && (date1 > 0 && date1 <= 31) && (year1 >= Calendar.getInstance().get(Calendar.YEAR)))) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_expiry));
            return false;
        }
//        else if (dmvNumber.getText().toString().trim().contains("*")) {
//            new CustomToast(getActivity()).alert(getString(R.string.valid_dmv_number));
//            return false;
//        }
        else {
            return true;
        }

        /*else if (dateOfBirth.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_bday));
            return false;
        } else if (addressLine1.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_address));
            return false;
        } else if (zip.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_zip));
            return false;
        } else if (city.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_city));
            return false;
        } else if (state.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_state));
            return false;
        } else if (country.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_country));
            return false;
        } else if (dmvNumber.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_dmv_no));
            return false;
        } else if (dmvExpiry.getText().toString().length() == 0) {
            new CustomToast(getActivity()).alert(getString(R.string.valid_expiry));
            return false;
        } */
    }

    private void chooseDate(final DSTextView edittext) {
        Calendar mcurrentDate = Calendar.getInstance();
        mcurrentDate.add(Calendar.DATE, -5840);
//        Calendar mcurrentDate = Calendar.getInstance();
        mYear = mcurrentDate.get(Calendar.YEAR);
        mMonth = mcurrentDate.get(Calendar.MONTH);
        mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                Calendar myCalendar = Calendar.getInstance();
                myCalendar.set(Calendar.YEAR, selectedyear);
                myCalendar.set(Calendar.MONTH, selectedmonth);
                myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);

                String myFormat = "MM-dd-yyyy"; //Change as you need
                SimpleDateFormat sdf = new SimpleDateFormat(
                        myFormat, Locale.getDefault());
                edittext.setText(sdf.format(myCalendar.getTime()));

                mDay = selectedday;
                mMonth = selectedmonth;
                mYear = selectedyear;
            }
        }, mYear, mMonth, mDay);
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, -6570);//Year,Mounth -1,Day
        mDatePicker.setTitle("Select date");
        mDatePicker.getDatePicker().setMaxDate(mcurrentDate.getTimeInMillis());
        mDatePicker.show();
    }

    private void chooseBDate(final DSTextView dateOfBirth) {
        Calendar mcurrentDate = Calendar.getInstance();
        mYear = mcurrentDate.get(Calendar.YEAR);
        mMonth = mcurrentDate.get(Calendar.MONTH);
        mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                Calendar myCalendar = Calendar.getInstance();
                myCalendar.set(Calendar.YEAR, selectedyear);
                myCalendar.set(Calendar.MONTH, selectedmonth);
                myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);

                String myFormat = "MM-dd-yyyy"; //Change as you need
                SimpleDateFormat sdf = new SimpleDateFormat(
                        myFormat, Locale.getDefault());
                dateOfBirth.setText(sdf.format(myCalendar.getTime()));

                mDay = selectedday;
                mMonth = selectedmonth;
                mYear = selectedyear;
            }
        }, mYear, mMonth, mDay);
        //mDatePicker.setTitle("Select date");
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 2000);
        mDatePicker.show();
    }
    /*Old logic to save the customer data*/

    /*if (isDataValid()) {
                        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                        Random rand = new Random();

                        // Generate random integers in range 0 to 9999
                        String rand_int1 = String.format(Locale.getDefault(), "%04d", rand.nextInt(1001));
                        if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)) {
                            PreferenceManger.putString(AppConstant.DMV_NUMBER, dmvNumber.getText().toString().trim() + rand_int1);

                            //Renaming the temp folder with the DMV Number's folder
                            File file1 = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/temp");
                            File file12 = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + dmvNumber.getText().toString().trim() + rand_int1);
                            //Start adding the records to the list
                            boolean success = file1.renameTo(file12);
//                        File sig = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/sig");
//                        File sig1 = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + dmvNumber.getText().toString().trim());
//                        //Start adding the records to the list
//                        boolean succcs = sig.renameTo(sig1);
                            //Check if the recordData object already exists else create a new recordData object
                            if (PreferenceManger.getStringValue(AppConstant.RECORD_DATA).length() != 0) {
                                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            } else {
                                recordData = new RecordData();
                            }
                            //Create new record object and add the Information to it
//                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() == null) {
                            record = new Record();
                            customerDetails = new CustomerDetails();
                            coBuyerCustomerDetails = new CoBuyerCustomerDetails();
                            vehicleDetails = new VehicleDetails();
                            tradeInVehicle = new TradeInVehicle();
                            settingsAndPermissions = new SettingsAndPermissions();
                            syncUp = new SyncUp();
                            dealershipDetails = new DealershipDetails();
//                        } else {
//                            record = new Record();
//                            customerDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
//                            vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
//                            settingsAndPermissions = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
//                            syncUp = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSyncUp();
//                            dealershipDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getDealershipDetails();
//                        }
                            dealershipDetails.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
                            record.setCustomerDetails(customerDetails);
                            record.setCoBuyerCustomerDetails(coBuyerCustomerDetails);
                            record.setDealershipDetails(dealershipDetails);
                            record.setTradeInVehicle(tradeInVehicle);
                            record.setSettingsAndPermissions(settingsAndPermissions);
                            record.setSyncUp(syncUp);
                            record.setVehicleDetails(vehicleDetails);

                            record.getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                            record.getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                            record.getCustomerDetails().setDLNumber(dmvNumber.getText().toString().trim() + rand_int1);
                            record.getCustomerDetails().setFirstName(firstName.getText().toString().trim());
                            record.getCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                            record.getCustomerDetails().setLastName(lastName.getText().toString().trim());
                            record.getCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                            record.getCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                            record.getCustomerDetails().setCity(city.getText().toString().trim());
                            record.getCustomerDetails().setState(state.getText().toString().trim());
                            record.getCustomerDetails().setCountry(country.getText().toString().trim());
                            record.getCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                            record.getCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                            record.getCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                            record.getCustomerDetails().setZipCode(zip.getText().toString().trim());
                            record.getCustomerDetails().setEmail(PreferenceManger.getStringValue("EmailTemp"));
//                            record.getCustomerDetails().setEmail(null);
                            record.getCustomerDetails().setTermConditionAccepted(PreferenceManger.getBooleanValue("opt"));
                            record.getCustomerDetails().setMobileNumber(PreferenceManger.getStringValue("MobileTemp"));
                            SalesPersons salesPersons = new SalesPersons();
                            salesPersons.setFirstName(PreferenceManger.getStringValue("SFName"));
                            salesPersons.setLastName(PreferenceManger.getStringValue("SLName"));
                            salesPersons.setMiddleName("");
                            record.getCustomerDetails().setSalesPersons(salesPersons);

                            recordData.getRecords().add(record);
                        } else {
                            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));

                            record.getCoBuyerCustomerDetails().setDLNumber(dmvNumber.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setFirstName(firstName.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setMiddleName(middleName.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setLastName(lastName.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setAddressLineOne(addressLine1.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setAddressLineTwo(addressLine2.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setCity(city.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setState(state.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setCountry(country.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setDateOfBirth(dateOfBirth.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setDLExpiryDate(dmvExpiry.getText().toString().trim());
                            record.getCoBuyerCustomerDetails().setGender(genderSpinner.getSelectedItem().toString().trim());
                            record.getCoBuyerCustomerDetails().setZipCode(zip.getText().toString().trim());
//                            recordData.getRecords().add(record);
//                            record.getCoBuyerCustomerDetails().setEmail(PreferenceManger.getStringValue("EmailTemp"));
//                            record.getCoBuyerCustomerDetails().setTermConditionAccepted(PreferenceManger.getBooleanValue("opt"));
//
//                            record.getCoBuyerCustomerDetails().setMobileNumber(PreferenceManger.getStringValue("MobileTemp"));
                        }

//
                        //Add the recordData object to the SharedPreference after converting it to string


                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);*/

    private void startImageSyncForRecursion(final File file, final String id, final ProgressDialog progressDialog, final String msg) {
//        System.gc();
        boolean additional = false;

        String imageFor = "1";
        /*Write logic for ImageFor */
        //TODO

        double size = (file.length() / (1024 * 1024));
        String imageDateTime = null;
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

        File filenew = null;
        Log.e("File size prev : ", file.length() / (1024 * 1024) + " M.B");
        if (size >= 1) {
            try {
                filenew = new Compressor(DSAPP.getInstance()).compressToFile(file);
                Log.e("File size new : ", filenew.length() / (1024 * 1024) + " M.B");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        MultipartBody.Part body;
        RequestBody reqFile;
        if (filenew != null) {
            reqFile = RequestBody.create(MediaType.parse("image/*"), filenew);
            body = MultipartBody.Part.createFormData("upload", filenew.getName(), reqFile);
        } else {
            reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
        }
        String utcForImg = null;
        try {
            imageDateTime = tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString()));
            long timeForUTC = new Date(file.lastModified()).getTime();
            utcForImg = tradeTimeFormatter.format(new Date(TimeZoneUtils.toUTC(timeForUTC, TimeZone.getDefault())));
            Log.e("UTC time for img: ", utcForImg);
            Log.e("Normal time for img: ", imageDateTime);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().syncImagesToServer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, "DRIVERLICENSEIMG", imageDateTime, body, imageFor, additional, utcForImg);
        final String finalDoctype = "DRIVERLICENSEIMG";
        imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(@NonNull Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                assert response.body() != null;
                if (response.isSuccessful() && response.body().isSucceeded()) {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    Log.e("Image upload type :", "onResponse: " + finalDoctype + " " + response.body().getValue());
                    if (file.exists()) {
                        Log.e("File deleted :", "onResponse: " + finalDoctype + " " + file.delete());
                    }
                    new CustomToast(getActivity()).alert(msg);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callHomeActivity();
                            clearDMVPreference();
                        }
                    }, 3100);

                } else {
                    progressDialog.dismiss();
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString() + " Image Name: " + finalDoctype);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        /*Push logs here every upload*/
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ImageUploadResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Image upload error :", "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

}
