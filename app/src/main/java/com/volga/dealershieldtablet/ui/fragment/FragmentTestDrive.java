package com.volga.dealershieldtablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.TreeMap;

import timber.log.Timber;

import static android.os.Looper.getMainLooper;


public class FragmentTestDrive extends BaseFragment {

    private View mView;
    private SignaturePad signaturePad;
    private Button fiveMins, twentyMins, maxMins;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    int testDriveTimeId = 0;

    IGoToNextPage iGoToNextPage;
    private Button yes, no;
    private LinearLayout durationLayout;
    private SettingsAndPermissions vehicleDetails;
    private CheckBox checkBox;
    private boolean isOld = false, isCoBuyer = false;
    private DSTextView time;
    boolean isTaken = false;
    private DSTextView sign;
    private ImageView delete;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && isAnsSelected && testDriveTimeId > 0 && isSigned) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            signaturePad.clear();

            disableWithGray(next);
//            enableNext(next);
            isSigned = false;
            updateData();
            initPhoto();
            //Code for displaying already selected Confirmation
        } else {
            isTaken = false;
        }

    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Timber.e("Pic URL: %s", pictureUrl);
                                isTaken = true;
                                if (isAnsSelected && testDriveTimeId > 0 && isSigned) {
                                    if (isOld) {
                                        if (checkBox.isChecked()) {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        } else {

                                            disableWithGray(next);
                                        }
                                    } else {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isAnsSelected && testDriveTimeId > 0 && isSigned) {
                                        if (isOld) {
                                            if (checkBox.isChecked()) {
                                                next.setVisibility(View.VISIBLE);
                                                enableWithGreen(next);
                                            } else {

                                                disableWithGray(next);
                                            }
                                        } else {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        }
                                    }
                                }
                                Timber.e("Pic size: %s", picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
            }
        }
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        time.setText(getCurrentTimeString());
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        isCoBuyer = vehicleDetails.isCoBuyer();
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }
        if (yes != null) {
            if (vehicleDetails.isCoBuyer()) {
                if (vehicleDetails.isCoBuyerTestDriveTaken()) {
                    visibleHintCalled = true;
                    isAnsSelected = true;
                    yes.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.rounded_corner_appcolor));
                    yes.setTextColor(getResources().getColor(R.color.white));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    durationLayout.setVisibility(View.VISIBLE);
                } else {
                    visibleHintCalled = true;
                    isAnsSelected = false;
                    yes.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ractangle_border_blue));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    yes.setTextColor(getResources().getColor(R.color.button_color_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    durationLayout.setVisibility(View.INVISIBLE);
                }
            } else {
                if (yes != null && vehicleDetails.getTestDriveTaken()) {
                    visibleHintCalled = true;
                    isAnsSelected = true;
                    yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    yes.setTextColor(getResources().getColor(R.color.white));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    durationLayout.setVisibility(View.VISIBLE);
                } else {
                    visibleHintCalled = true;
                    isAnsSelected = false;
                    yes.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ractangle_border_blue));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    yes.setTextColor(getResources().getColor(R.color.button_color_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    durationLayout.setVisibility(View.INVISIBLE);
                }
            }
        }

        if (vehicleDetails.isCoBuyer()) {
            if (fiveMins != null) {
                if (vehicleDetails.getCoBuyerTestDriveTimeId() > 0 && vehicleDetails.isCoBuyerTestDriveTaken()) {
                    isAnsSelected = true;
                    testDriveTimeId = vehicleDetails.getCoBuyerTestDriveTimeId();
                }
                if (vehicleDetails.getCoBuyerTestDriveTimeId() == 1) {
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    fiveMins.setTextColor(getResources().getColor(R.color.white));
                    twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                    maxMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                } else if (vehicleDetails.getCoBuyerTestDriveTimeId() == 2) {
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setTextColor(getResources().getColor(R.color.white));
                    maxMins.setTextColor(getResources().getColor(R.color.font_color));
                } else if (vehicleDetails.getCoBuyerTestDriveTimeId() == 3) {
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                    maxMins.setTextColor(getResources().getColor(R.color.white));
                }
            }
        } else {
            if (fiveMins != null) {
                if (vehicleDetails.getTestDriveTimeId() > 0 && vehicleDetails.getTestDriveTaken()) {
                    isAnsSelected = true;
                    testDriveTimeId = vehicleDetails.getTestDriveTimeId();
                }
                if (vehicleDetails.getTestDriveTimeId() == 1) {
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    fiveMins.setTextColor(getResources().getColor(R.color.white));
                    twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                    maxMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                } else if (vehicleDetails.getTestDriveTimeId() == 2) {
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setTextColor(getResources().getColor(R.color.white));
                    maxMins.setTextColor(getResources().getColor(R.color.font_color));
                } else if (vehicleDetails.getTestDriveTimeId() == 3) {
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                    maxMins.setTextColor(getResources().getColor(R.color.white));
                }
            }


        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.test_drive_question, container, false);
        isSigned = false;
        isAnsSelected = false;
        testDriveTimeId = 0;
        if (NewDealViewPager.currentPage == 72) {
            initPhoto();
        }
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        sign = mView.findViewById(R.id.signhere);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
        }
        initView();
        return mView;
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("UserPic", "test_drive_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_test_drive_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        if (FragmentConfirmTestDrive.isNextClicked) {
            FragmentConfirmTestDrive.isNextClicked = false;
            if (vehicleDetails.isCoBuyer()) {
                if (!vehicleDetails.isCoBuyerTestDriveTaken() && !vehicleDetails.isCoBuyerNoTestDriveConfirm()) {
                    yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    isAnsSelected = vehicleDetails.getTestDriveTaken();

                    disableWithGray(next);
                    FragmentTestDriveMandetory.nowTestDriveTaken = false;
                    testDriveTimeId = 0;
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                    maxMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    signaturePad.clear();
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iGoToNextPage.whatNextClick();
//                            SetRecordData();
                        }
                    }, 300);
                }
            } else {
                if (!vehicleDetails.getTestDriveTaken() && !vehicleDetails.getNoTestDriveConfirm()) {
                    yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    isAnsSelected = vehicleDetails.getTestDriveTaken();

                    disableWithGray(next);
                    FragmentTestDriveMandetory.nowTestDriveTaken = false;
                    testDriveTimeId = 0;
                    fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                    maxMins.setTextColor(getResources().getColor(R.color.font_color));
                    twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                    signaturePad.clear();
                    maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iGoToNextPage.whatNextClick();
//                            SetRecordData();
                        }
                    }, 300);
                }
            }

        }
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        time = mView.findViewById(R.id.time);
        TextView back = mView.findViewById(R.id.text_back_up);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        boolean isPhotoAvailable = isPhotoAvailable();
//        boolean isPhotoAvailable = record.getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
        if (!isPhotoAvailable) {
            text_cancelBack.setText(String.format(getString(R.string.page1), "4", decidePageNumber()));
        }else {
            text_cancelBack.setText(String.format(getString(R.string.page1), "5", decidePageNumber()));
        }
//        text_cancelBack.setText(String.format(getString(R.string.page1), "5", decidePageNumber()));
        durationLayout = mView.findViewById(R.id.durationLayout);
        durationLayout.setVisibility(View.INVISIBLE);
        fiveMins = mView.findViewById(R.id.fiveMins);
        twentyMins = mView.findViewById(R.id.twentyMins);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        RelativeLayout oldcarDisclosure = mView.findViewById(R.id.oldCarDisclosure);
        DSTextView name = mView.findViewById(R.id.name);
//        name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkBox.setChecked(!checkBox.isChecked());
//            }
//        });
        checkBox = mView.findViewById(R.id.checkBox);
        checkBox.setChecked(true);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isAnsSelected && isSigned && isChecked && testDriveTimeId > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            next.setVisibility(View.VISIBLE);
                            enableWithGreen(next);
                        }
                    }, 100);
                } else {
                    disableWithGray(next);
                }
            }
        });
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
            oldcarDisclosure.setVisibility(View.GONE);
        } else {
            isOld = true;
        }
        maxMins = mView.findViewById(R.id.maxMins);
        final DSTextView signature = mView.findViewById(R.id.signature);
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
        if (!visibleHintCalled) {
            updateData();
        }
//        if (!vehicleDetails.getTestDriveTaken()) {
        fiveMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testDriveTimeId = 1;
                if (isOld) {
                    if (isAnsSelected && isSigned && checkBox.isChecked()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    } else {

                        disableWithGray(next);
                    }
                } else {
                    if (isAnsSelected && isSigned) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    } else {

                        disableWithGray(next);
                    }
                }
                fiveMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                fiveMins.setTextColor(getResources().getColor(R.color.white));
                twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                maxMins.setTextColor(getResources().getColor(R.color.font_color));
                twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
            }
        });
//        }

//        if (!vehicleDetails.getTestDriveTaken()) {
        twentyMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOld) {
                    if (isAnsSelected && isSigned && checkBox.isChecked()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    } else {

                        disableWithGray(next);
                    }
                } else {
                    if (isAnsSelected && isSigned) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    } else {

                        disableWithGray(next);
                    }
                }
                fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                twentyMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                maxMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                twentyMins.setTextColor(getResources().getColor(R.color.white));
                maxMins.setTextColor(getResources().getColor(R.color.font_color));
                testDriveTimeId = 2;
            }
        });
//        }

//        if (!vehicleDetails.getTestDriveTaken()) {
        maxMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOld) {
                    if (isAnsSelected && isSigned && checkBox.isChecked()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    } else {

                        disableWithGray(next);
                    }
                } else {
                    if (isAnsSelected && isSigned) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    } else {

                        disableWithGray(next);
                    }
                }
                fiveMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                twentyMins.setBackground(getActivity().getDrawable(R.drawable.customborder));
                maxMins.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                fiveMins.setTextColor(getResources().getColor(R.color.font_color));
                twentyMins.setTextColor(getResources().getColor(R.color.font_color));
                maxMins.setTextColor(getResources().getColor(R.color.white));
                testDriveTimeId = 3;
            }
        });
//        }

//        if (!vehicleDetails.getTestDriveTaken()) {
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                yes.setTextColor(getResources().getColor(R.color.white));
                no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                no.setTextColor(getResources().getColor(R.color.button_color_blue));
//                vehicleDetails.setTestDriveTaken(true);
                durationLayout.setVisibility(View.VISIBLE);
                if (isSigned && testDriveTimeId > 0) {
                    if (isOld) {
                        if (checkBox.isChecked()) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }, 100);
                        } else {

                            disableWithGray(next);
                        }
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    }
                }
                isAnsSelected = true;
            }
        });
//        }
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                yes.setTextColor(getResources().getColor(R.color.button_color_blue));
                no.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.rounded_corner_appcolor));
                no.setTextColor(getResources().getColor(R.color.white));
                isAnsSelected = false;

                disableWithGray(next);
//                durationLayout.setVisibility(View.INVISIBLE);
                //                no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                /*show confirmtion pop up*/
//                if (!vehicleDetails.isCoBuyer()) {
//                    vehicleDetails.setTestDriveTaken(false);
//                } else {
//                    vehicleDetails.setCoBuyerTestDriveTaken(false);
//                }
                final Gson gson = new GsonBuilder().create();
                DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
                if (dealershipData.where(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)).getIsTestDriveMandatory() && !vehicleDetails.getTestDriveTaken() && vehicleDetails.isCoBuyer()) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(R.string.test_drive_manda)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                    intent.putExtra(AppConstant.FROM_FRAGMENT, "testDrive");
                                    getActivity().startActivity(intent);
                                }
                            }).show().setCancelable(false);
                } else {
                    if (dealershipData.where(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)).getIsTestDriveMandatory() && !vehicleDetails.isHasCoBuyer() && !vehicleDetails.isCoBuyer()) {
                        new AlertDialog.Builder(getActivity())
                                .setMessage(R.string.test_drive_manda)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                        intent.putExtra(AppConstant.FROM_FRAGMENT, "testDrive");
                                        getActivity().startActivity(intent);
                                    }
                                }).show().setCancelable(false);
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setMessage(R.string.are_you_sure)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                        intent.putExtra(AppConstant.FROM_FRAGMENT, "confirmTestDrive");
                                        getActivity().startActivity(intent);
                                    }
                                })
                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                        no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                                        no.setTextColor(getResources().getColor(R.color.button_color_blue));
//                                        Gson gson = new GsonBuilder().create();
//                                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//                                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setNoTestDriveConfirm(false);
//                                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTestDriveTaken(false);
//                                        String recordDataString = gson.toJson(recordData);
//                                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                                        Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
//                                        intent.putExtra(AppConstant.FROM_FRAGMENT, "testDrive");
//                                        getActivity().startActivity(intent);


                                    }
                                }).show().setCancelable(false);
                    }
                }
            }
        });


        disableWithGray(next);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOld && !checkBox.isChecked()) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_all_disclosure));
                    return;
                }
//                disableNext(next);
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
                ArrayList<HistoryListPojo> bh = vehicleDetails.getHistorySelection();
                ArrayList<HistoryListPojo> ch = vehicleDetails.getCoHistorySelection();
                ArrayList<HistoryListPojo> bc = vehicleDetails.getConditionSelection();
                ArrayList<HistoryListPojo> cc = vehicleDetails.getCoConditionSelection();
                ArrayList<HistoryListPojo> br = vehicleDetails.getReportSelection();
                ArrayList<HistoryListPojo> cr = vehicleDetails.getCoReportSelection();
                if (bh != null) {
                    for (int i = 0; i < bh.size(); i++) {
                        if (!bh.get(i).getId().equalsIgnoreCase("20")) {
                            bh.get(i).setChecked(false);
                        }
                    }
                }
                if (ch != null)
                    for (int i = 0; i < ch.size(); i++) {
                        if (!ch.get(i).getId().equalsIgnoreCase("20"))
                            ch.get(i).setChecked(false);
                    }
                if (bc != null)
                    for (int i = 0; i < bc.size(); i++) {
                        if (!bc.get(i).getId().equalsIgnoreCase("0"))
                            bc.get(i).setChecked(false);
                    }
                if (cc != null)
                    for (int i = 0; i < cc.size(); i++) {
                        if (!cc.get(i).getId().equalsIgnoreCase("0"))
                            cc.get(i).setChecked(false);
                    }
                if (br != null)
                    for (int i = 0; i < br.size(); i++) {
                        if (!br.get(i).getId().equalsIgnoreCase("5"))
                            br.get(i).setChecked(false);
                    }
                if (cr != null)
                    for (int i = 0; i < cr.size(); i++) {
                        if (!cr.get(i).getId().equalsIgnoreCase("5"))
                            cr.get(i).setChecked(false);
                    }
                vehicleDetails.setCoConditionSelection(cc);
                vehicleDetails.setConditionSelection(bc);
                vehicleDetails.setHistorySelection(bh);
                vehicleDetails.setCoHistorySelection(ch);
                vehicleDetails.setReportSelection(br);
                vehicleDetails.setCoReportSelection(cr);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (testDriveTimeId == 0 && isAnsSelected) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_time_dure));
                } else {
                    addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                    SetRecordData();
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "test_drive_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_test_drive_screen");
                    }
                    scrollableScreenshot(mainContent, file);
                    disableNext(next);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            enableNext(next);
                        }
                    }, 5000);
                }
            }
        });

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (!vehicleDetails.isCoBuyer())
                        svgFile = new File(path_signature + "/IMG_test_drive_taken_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg");
                    }
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "test_drive_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_test_drive_screen");
                    }
                    File file1;
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "test_drive_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_test_drive_screen_pic");
                    }
                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (!vehicleDetails.isCoBuyer())
                    svgFile = new File(path_signature + "/IMG_test_drive_taken_selection.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg");
                }
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "test_drive_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_test_drive_screen");
                }
                File file1;
                if (!isCoBuyer) {
                    file1 = getOutputFile("UserPic", "test_drive_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "co_test_drive_screen_pic");
                }
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                iGoToNextPage.goToBackIndex();
            }
        });
        signaturePad = mView.findViewById(R.id.signaturePad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                isSigned = true;
                if (isAnsSelected && testDriveTimeId > 0) {
                    if (isOld) {
                        if (checkBox.isChecked()) {
                            if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isSigned) {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        }
                                    }
                                }, AppConstant.NEXT_DELAY);
                            } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            } else if (isTaken) {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        } else {

                            disableWithGray(next);
                        }
                    } else {
                        if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }, AppConstant.NEXT_DELAY);
                        } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                            next.setVisibility(View.VISIBLE);
                            enableWithGreen(next);
                        } else if (isTaken) {
                            next.setVisibility(View.VISIBLE);
                            enableWithGreen(next);
                        }
                    }
                }
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;

                disableWithGray(next);
            }
        });
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {
                        isSigned = false;
                        signaturePad.clear();

                        disableWithGray(next);
                        return true;
                    }
                }
                return true;
            }
        });


    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile;
            if (vehicleDetails.isCoBuyer()) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_test_drive_taken_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "test_drive_taken_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        if (!vehicleDetails.isCoBuyer()) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setNoTestDriveConfirm(false);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTestDriveTaken(true);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTestDriveTimeId(testDriveTimeId);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setAcceptedUsedCarPolicy(checkBox.isChecked());
        } else {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerNoTestDriveConfirm(false);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerTestDriveTaken(true);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerTestDriveTimeId(testDriveTimeId);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setAcceptedUsedCarPolicy(checkBox.isChecked());
        }
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        iGoToNextPage.whatNextClick();

    }

}
