package com.volga.dealershieldtablet.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.TreeMap;

import timber.log.Timber;

import static android.os.Looper.getMainLooper;


public class FragmentConfirmTestDrive extends BaseFragment {

    private View mView;
    private SignaturePad signaturePad;
    public static boolean isNextClicked, isBackClicked;
    private DSTextView next;
    private File svgFile;
    private CheckBox checkBox;
    private boolean isSigned = false;
    private boolean isOld = false;
    private boolean isCoBuyer;
    private DSTextView time;
    private ImageView delete;
    private DSTextView sign;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            isTaken = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.confirm_no_test_drive, container, false);
        isNextClicked = false;
        isBackClicked = false;
        initView();
        return mView;
    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Timber.e("Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Timber.e("Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);
            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    private void initView() {
        showProgressFor2Sec();
        initPhoto();
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setVisibility(View.INVISIBLE);
        next = mView.findViewById(R.id.text_next);
        time = mView.findViewById(R.id.time);
        time.setText(getCurrentTimeString());
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        logout.setVisibility(View.GONE);
//        next.setVisibility(View.GONE);
        disableWithGray(next);
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        sign = mView.findViewById(R.id.signhere);
        RelativeLayout oldcarDisclosure = mView.findViewById(R.id.oldCarDisclosure);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        isCoBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
            oldcarDisclosure.setVisibility(View.GONE);
        } else {
            isOld = true;
        }
        DSTextView name = mView.findViewById(R.id.name);
//        name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkBox.setChecked(!checkBox.isChecked());
//            }
//        });
        checkBox = mView.findViewById(R.id.checkBox);
        checkBox.setChecked(true);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isSigned && isChecked) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            enableWithGreen(next);
                            next.setVisibility(View.VISIBLE);
                        }
                    }, AppConstant.NEXT_DELAY);
                } else {
                    disableWithGray(next);
//                    next.setVisibility(View.GONE);
                }
            }
        });
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOld && !checkBox.isChecked()) {
                    new CustomToast(getActivity()).alert("Please select disclosure");
                    return;
                }
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "no_test_drive_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_no_test_drive_screen");
                }
                scrollableScreenshot(mainContent, file);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                isNextClicked = true;
                SetRecordData();
                getActivity().finish();
//                next(new FragmentRemoveStickers());
            }
        });
        back.setEnabled(false);
        back.setClickable(false);
        back.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                back.setEnabled(true);
                back.setVisibility(View.VISIBLE);
                back.setClickable(true);
            }
        }, 6000);
        back.setOnClickListener(view -> {
            deletePics();
            getActivity().finish();
            isBackClicked = true;
//                goBack();
        });
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSigned = false;

            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    deletePics();
                    return false;
                }
                return false;
            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
                disableDelete(delete);
                sign.setVisibility(View.VISIBLE);
//                next.setVisibility(View.GONE);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                isSigned = true;
                if (isOld) {
                    if (checkBox.isChecked() && isSigned) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    enableWithGreen(next);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    }
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isSigned) {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }
                    }, AppConstant.NEXT_DELAY);
                }


            }

            @Override
            public void onClear() {
                disableDelete(delete);
                disableNext(next);
                sign.setVisibility(View.VISIBLE);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        disableWithGray(next);
//                        next.setVisibility(View.GONE);
                        return true;
                    }
                }
                return true;
            }
        });

    }

    private void deletePics() {
        String path_signature;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
        File svgFile;

        if (!record.getSettingsAndPermissions().isCoBuyer())
            svgFile = new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg");
        else {
            svgFile = new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg");
        }
        File file;
        if (!isCoBuyer) {
            file = getOutputFile("Screenshot", "no_test_drive_screen");
        } else {
            file = getOutputFile("Screenshot", "co_no_test_drive_screen");
        }
        File file1;
        if (!isCoBuyer) {
            file1 = getOutputFile("UserPic", "no_test_drive_screen_pic");
        } else {
            file1 = getOutputFile("UserPic", "co_no_test_drive_screen_pic");
        }
        Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
        Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
        Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
//        Log.e("Device back: ", " Device back pressed: " + keyCode);
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                File file;
                if (!isTaken) {
                    if (!isCoBuyer) {
                        file = getOutputFile("UserPic", "no_test_drive_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_no_test_drive_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }


    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                svgFile = getOutputMediaFile("Signatures", "co_buyer_no_test_drive_confirm_selection");
            else {
                svgFile = getOutputMediaFile("Signatures", "no_test_drive_confirm_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        File mediaStorageDir;
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setNoTestDriveConfirm(true);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTestDriveTaken(false);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setAcceptedUsedCarPolicy(checkBox.isChecked());
        } else {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerNoTestDriveConfirm(true);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerTestDriveTaken(false);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setAcceptedUsedCarPolicy(checkBox.isChecked());

        }
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

    }

}
