package com.volga.dealershieldtablet.ui.coBuyer;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.ViewPagerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.Calendar;

/**
 * Created by ${Shailendra} on 22-08-2018.
 */
public class FragmentIsCoBuyer extends BaseFragment {
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    IGoToNextPage iGoToNextPage;
    private RecordData recordData;
    private Gson gson;
    private boolean isCoBuyer = false;
    private Button yes, no;
    private DSTextView text_cancelBack;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            updateData();
            //Code for displaying already selected Confirmation
        }
    }

    private void updateData() {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.is_co_buyer, container, false);
        initView();
        return mView;
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        TextView back = mView.findViewById(R.id.text_back_up);
        text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setVisibility(View.INVISIBLE);
        DSTextView title = mView.findViewById(R.id.title);
        title.setText(Html.fromHtml(getString(R.string.is_co_buyer)));
        text_cancelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_cancelBack.setEnabled(false);
                text_cancelBack.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        text_cancelBack.setEnabled(true);
                        text_cancelBack .setClickable(true);
                    }
                }, 6000);
                cancel();
            }
        });
//        text_cancelBack.setVisibility(View.INVISIBLE);
//        DSTextView txt_logout = mView.findViewById(R.id.txt_logout);
//        txt_logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                logout();
//            }
//        });
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
        updateData();
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
                next.setVisibility(View.VISIBLE);
                isAnsSelected = true;
                isCoBuyer = true;
                SetRecordData(true);
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*show confirmation pop up*/
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                next.setVisibility(View.VISIBLE);
                isCoBuyer = false;
                SetRecordData(false);

            }
        });
        next.setVisibility(View.GONE);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetRecordData(isCoBuyer);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.goToBackIndex();
            }
        });

    }

    public void SetRecordData(boolean isCoBuyer) {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
       boolean webDeal= recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal();
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasCoBuyer(isCoBuyer);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(isCoBuyer);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(ViewPagerActivity.currentPage);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        if (isCoBuyer) {
            ((NewDealViewPager) getActivity()).setCurrentPage(54);
        }else {
            if (!webDeal) {
                iGoToNextPage.whatNextClick();
            } else {
                ((NewDealViewPager) getActivity()).setCurrentPage(68);
            }
        }

    }
}
