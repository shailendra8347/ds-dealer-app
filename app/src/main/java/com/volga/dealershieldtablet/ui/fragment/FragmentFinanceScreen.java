package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.adapter.RecyclerViewAdapter;
import com.volga.dealershieldtablet.screenRevamping.pojo.VehicleImagePojo;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class FragmentFinanceScreen extends BaseFragment {

    private View mView;
    private DSTextView next;

    IGoToNextPage iGoToNextPage;
    private Gson gson;
    private RecordData recordData;
    private DSTextView make, model, year, type, mileage, vin, extc, intc, clickEnlarge;
    private ScrollView scrollView;
    private RecyclerView recyclerView;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_finance_mngr, container, false);
        initView();
        return mView;
    }

    private void showAlert() {
        if (getActivity() != null) {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
            titleTV.setVisibility(View.GONE);
            messageTV.setText("CARFAX/AutoCheck Not Available. Please check your Internet connection or make manual disclosures.");
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            ok.setText("I understand");
            alertD.setView(promptView);
            alertD.show();

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file = getOutputFile("Screenshot", "alert_finance_page");
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("No Internet access");
                            customerVehicleDealAlerts.setAlertDesc("At continue to F&I screen internet was not available.");
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("No Internet access");
                            customerVehicleDealAlerts.setAlertDesc("At continue to F&I screen internet was not available.");
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    alertD.dismiss();
                }
            });

//            new AlertDialog.Builder(getActivity())
//                    .setMessage("CARFAX/AutoCheck Not Available. Please check your Internet connection or make manual disclosures.")
//                    .setIcon(android.R.drawable.ic_dialog_alert)
//                    .setPositiveButton("I understand", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int whichButton) {
//
//
//
//                            /*Take screen shot and send to the server.*/
//
//                            dialog.dismiss();
//                        }
//                    }).show().setCancelable(false);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            showFragmentInPortrait();
            if (getActivity() != null) {
                showPopUpDialog();
            }
            updateData();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    void showPopUpDialog() {
        if (NewDealViewPager.currentPage == 61) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (!DSAPP.getInstance().isNetworkAvailable()) {
                boolean show = false;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null) {
                    ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).isActive()) {
                            show = true;
                            break;
                        }
                    }
                }
                if (PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API) && show && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used"))
                    showAlert();
            } else {
                CustomerVehicleDealAlerts alertStr = new CustomerVehicleDealAlerts();
                CustomerVehicleDealAlerts alert1 = new CustomerVehicleDealAlerts();
                try {
                    alertStr = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAutoCheckAvailable();
                    alert1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarFaxAvailable();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (alertStr != null && alertStr.getAlertDesc() != null) {
                    final File file = getOutputFile("Screenshot", "alert_finance_page_ac");
//                    takeScreenshotForAlert(alert, file);

                    String strAlert;
                    if (alertStr.getAlertDesc() == null) {
                        strAlert = "";
                    } else {
                        strAlert = alertStr.getAlertDesc();
                    }
                    LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                    View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
                    final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
                    alertD.setCancelable(false);
                    final DSTextView titleTV = promptView.findViewById(R.id.title);
                    DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
                    titleTV.setText("AutoCheck Not Available. Please make manual disclosures.");
                    messageTV.setText(strAlert);
                    final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
                    stopDeal.setVisibility(View.GONE);
                    DSTextView ok = promptView.findViewById(R.id.ok);
                    ok.setText("I understand");
                    alertD.setView(promptView);
                    alertD.show();
                    CustomerVehicleDealAlerts finalAlertStr = alertStr;
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            takeScreenshotForAlert(file);
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                                ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                                if (alert == null || alert.size() == 0) {
                                    ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                                    CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                                    customerVehicleDealAlerts.setAlertName("AutoCheck not available");
                                    customerVehicleDealAlerts.setAlertDesc(finalAlertStr.getAlertDesc());
                                    arrayList.add(customerVehicleDealAlerts);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                                } else {
                                    CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                                    customerVehicleDealAlerts.setAlertName("AutoCheck not available");
                                    customerVehicleDealAlerts.setAlertDesc(finalAlertStr.getAlertDesc());
                                    alert.add(customerVehicleDealAlerts);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                                }
                            }
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            alertD.dismiss();
                        }
                    });
//                    alertD.setTitle("AutoCheck Not Available. Please make manual disclosures.")
//                            .setMessage(strAlert)
//                            .setIcon(android.R.drawable.ic_dialog_alert)
//                            .setPositiveButton("I understand", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int whichButton) {
//
//
//                                    /*Take screen shot and send to the server.*/
//
//                                    dialog.dismiss();
//                                }
//                            }).show().setCancelable(false);
                }
                if (alert1 != null && alert1.getAlertDesc() != null) {
                    final File file = getOutputFile("Screenshot", "alert_finance_page_cf");

                    String strAlert;
                    if (alert1.getAlertDesc() == null) {
                        strAlert = "";
                    } else {
                        strAlert = alert1.getAlertDesc();
                    }

                    LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                    View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
                    final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
                    alertD.setCancelable(false);
                    final DSTextView titleTV = promptView.findViewById(R.id.title);
                    DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
                    titleTV.setText("CARFAX Not Available. Please make manual disclosures.");
                    messageTV.setText(strAlert);
                    final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
                    stopDeal.setVisibility(View.GONE);
                    DSTextView ok = promptView.findViewById(R.id.ok);
                    ok.setText("I understand");
                    alertD.setView(promptView);
                    alertD.show();
                    CustomerVehicleDealAlerts finalAlert = alert1;
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            takeScreenshotForAlert(file);
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                                ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                                if (alert == null || alert.size() == 0) {
                                    ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                                    CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                                    customerVehicleDealAlerts.setAlertName("CARFAX not available");
                                    customerVehicleDealAlerts.setAlertDesc(finalAlert.getAlertDesc());
                                    arrayList.add(customerVehicleDealAlerts);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                                } else {
                                    CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                                    customerVehicleDealAlerts.setAlertName("CARFAX not available");
                                    customerVehicleDealAlerts.setAlertDesc(finalAlert.getAlertDesc());
                                    alert.add(customerVehicleDealAlerts);
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                                }
                            }
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            /*Take screen shot and send to the server.*/

                            alertD.dismiss();
                        }
                    });

                }
            }
        }
    }

    private void updateData() {
        if (make != null) {
            visibleHintCalled = true;
            gson = new GsonBuilder().create();
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs()==null||recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs().size()==0) {
//                    clickEnlarge.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.GONE);
//                } else {
//                    clickEnlarge.setVisibility(View.VISIBLE);
//                    recyclerView.setVisibility(View.VISIBLE);
//                }
//            }
            updateRecycler();
            VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
            TradeInVehicle tradeInVehicle = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle();
            if (vehicleDetails != null) {
                if (NewDealViewPager.currentPage == 61) {
                    make.setText(MessageFormat.format(": {0}", vehicleDetails.getCarMake()));
                    model.setText(MessageFormat.format(": {0}", vehicleDetails.getCarModel()));
                    year.setText(MessageFormat.format(": {0}", vehicleDetails.getCarYear()));
                    mileage.setText(MessageFormat.format(": {0}", vehicleDetails.getMileage()));
                    type.setText(MessageFormat.format(": {0}", vehicleDetails.getTypeOfVehicle()));
                    vin.setText(MessageFormat.format(": {0}", vehicleDetails.getVINNumber()));
                    mView.findViewById(R.id.ExteriorColor).setVisibility(View.GONE);
                    mView.findViewById(R.id.type).setVisibility(View.VISIBLE);
                    mView.findViewById(R.id.InteriorColor).setVisibility(View.GONE);
                    ((DSTextView) mView.findViewById(R.id.title)).setText(Html.fromHtml(getString(R.string.finance_manager_Screen)));
//                    loadImagesForConfirmation();
                    updateRecycler();
                } else if (NewDealViewPager.currentPage == 62) {
                    make.setText(MessageFormat.format(": {0}", tradeInVehicle.getCarMake()));
                    model.setText(MessageFormat.format(": {0}", tradeInVehicle.getCarModel()));
                    year.setText(MessageFormat.format(": {0}", tradeInVehicle.getCarYear()));
                    mileage.setText(MessageFormat.format(": {0}", tradeInVehicle.getMileage()));
                    type.setText(MessageFormat.format(": {0}", tradeInVehicle.getTypeOfVehicle()));
                    extc.setText(MessageFormat.format(": {0}", getCorrectText(tradeInVehicle.getExteriorColor())));
                    intc.setText(MessageFormat.format(": {0}", getCorrectText(tradeInVehicle.getInteriorColor())));
                    mView.findViewById(R.id.type).setVisibility(View.GONE);
                    vin.setText(MessageFormat.format(": {0}", tradeInVehicle.getVINNumber()));
                    mView.findViewById(R.id.ExteriorColor).setVisibility(View.VISIBLE);
                    mView.findViewById(R.id.InteriorColor).setVisibility(View.VISIBLE);
                    ((DSTextView) mView.findViewById(R.id.title)).setText(Html.fromHtml(getString(R.string.tradeinsumm)));
//                    loadImagesForConfirmation();
                    updateRecycler();
                }

            }
        }

    }

    private String getCorrectText(String string) {
        if (string == null) {
            return "";
        } else if (string.equalsIgnoreCase("null")) {
            return "";
        } else {
            return string;
        }
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
//        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
//        text_cancelBack.setText(R.string.page3);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView yes = mView.findViewById(R.id.yes);
        final DSTextView no = mView.findViewById(R.id.no);
        yes.setVisibility(View.GONE);
        no.setVisibility(View.GONE);
/* */
        //
        make = mView.findViewById(R.id.make);
        scrollView = mView.findViewById(R.id.scrollView);
        clickEnlarge = mView.findViewById(R.id.clickEnlarge);
        recyclerView = mView.findViewById(R.id.imageRecycler);
        model = mView.findViewById(R.id.model);
        year = mView.findViewById(R.id.year);
        mileage = mView.findViewById(R.id.mileageKm);
        type = mView.findViewById(R.id.vehicleType);
        vin = mView.findViewById(R.id.vinNumber);
        extc = mView.findViewById(R.id.extc);
        intc = mView.findViewById(R.id.intc);
        if (!visibleHintCalled) {
            updateData();
        }
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next.setVisibility(View.VISIBLE);
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getActivity())
                        .setMessage(getString(R.string.this_not_vehicle))
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                intent.putExtra(AppConstant.FROM_FRAGMENT, "handover");
                                getActivity().startActivity(intent);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();

            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((NewDealViewPager) getActivity()).getCurrentPage() == 61) {
                    gson = new GsonBuilder().create();
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                        ((NewDealViewPager) getActivity()).setCurrentPage(63);
                    } else {
                        iGoToNextPage.whatNextClick();
                    }
                } else
                    iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                iGoToNextPage.goToBackIndex();
            }
        });
//        loadImagesForConfirmation();
        updateRecycler();
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel.setEnabled(false);
                cancel.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cancel.setEnabled(true);
                        cancel.setClickable(true);
                    }
                }, 6000);
                cancel();
            }
        });


    }

    private void updateRecycler() {
        final ArrayList<CustomerVehicleTradeDocs> docsArrayList;
        ArrayList<VehicleImagePojo> imagePojoArrayList = new ArrayList<>();
        boolean isIncomplete = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isIncompleteDeal() || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal() || (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().trim().length() > 0);
        if (NewDealViewPager.currentPage == 61) {
            if (isIncomplete) {
                docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs();
                for (int i = 0; i < docsArrayList.size(); i++) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    if (docsArrayList.get(i).getIsAdditionalImage()) {
                        vehicleImagePojo.setName(docsArrayList.get(i).getDocTypeName());
                    }else if (docsArrayList.get(i).isInventoryImage()){
                        vehicleImagePojo.setName(docsArrayList.get(i).getDocTypeName());
                    } else {
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("FRONTIMG")) {
                            vehicleImagePojo.setName("Front");
                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("REARIMG")) {
                            vehicleImagePojo.setName("Back");
                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("RIGHTIMG")) {
                            vehicleImagePojo.setName("Passenger Side");
                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LEFTIMG")) {
                            vehicleImagePojo.setName("Driver Side");
                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("MILEAGEIMG")) {
                            vehicleImagePojo.setName("Odometer Reading");
                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails()!=null&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used")) {
                                vehicleImagePojo.setName("Buyer's Guide");
                            } else {
                                vehicleImagePojo.setName("MSRP/Addendum sticker");
                            }
                        }
//                   if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
//                       vehicleImagePojo.setName("Buyer's Guide");
//                   }
                    }
                    vehicleImagePojo.setUrl(docsArrayList.get(i).getDocUrl());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                Uri odometer = ShowImageFromPath("IMG_mileage.jpg", "CarImages");
                if (odometer != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(odometer);
                    vehicleImagePojo.setName("Odometer Reading");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
            } else {
                Uri front = ShowImageFromPath("IMG_front.jpg", "CarImages");
                Uri passenger = ShowImageFromPath("IMG_right.jpg", "CarImages");
                Uri back = ShowImageFromPath("IMG_rear.jpg", "CarImages");
                Uri driver = ShowImageFromPath("IMG_left.jpg", "CarImages");
                Uri window = ShowImageFromPath("IMG_sticker.jpg", "CarImages");
                Uri odometer = ShowImageFromPath("IMG_mileage.jpg", "CarImages");
                if (front != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(front);
                    vehicleImagePojo.setName("Front");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (back != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(back);
                    vehicleImagePojo.setName("Back");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (passenger != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(passenger);
                    vehicleImagePojo.setName("Passenger Side");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (driver != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(driver);
                    vehicleImagePojo.setName("Driver Side");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (window != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(window);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails()!=null&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used")) {
                        vehicleImagePojo.setName("Buyer's Guide");
                    } else {
                        vehicleImagePojo.setName("MSRP/Addendum sticker");
                    }
//                    vehicleImagePojo.setName("Buyers Guide");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (odometer != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(odometer);
                    vehicleImagePojo.setName("Odometer Reading");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5());
                    imagePojoArrayList.add(vehicleImagePojo);
                }

            }
        } else if (NewDealViewPager.currentPage == 62) {
            docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTradeInVehicleDocs();
//            ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages");

            if (isIncomplete && docsArrayList != null) {
                for (int i = 0; i < docsArrayList.size(); i++) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    if (docsArrayList.get(i).getIsAdditionalImage()) {
                        vehicleImagePojo.setName(docsArrayList.get(i).getDocTypeName());
                    }else if (docsArrayList.get(i).isInventoryImage()){
                        vehicleImagePojo.setName(docsArrayList.get(i).getDocTypeName());
                    } else {
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("TradeInFront")) {
                            vehicleImagePojo.setName("Front");
                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LICENCEPLATENUMBER")) {
                            vehicleImagePojo.setName("License Plate");
                        }
//                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("RIGHTIMG")) {
//                            vehicleImagePojo.setName("Passenger Side");
//                        }
//                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LEFTIMG")) {
//                            vehicleImagePojo.setName("Driver Side");
//                        }
                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("TRADEINMILEAGE")) {
                            vehicleImagePojo.setName("Odometer Reading");
                        }
//                        if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
//                            vehicleImagePojo.setName("Buyer's Guide");
//                        }
                    }
                    vehicleImagePojo.setUrl(docsArrayList.get(i).getDocUrl());
                    imagePojoArrayList.add(vehicleImagePojo);

                }
                if (ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle1());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle2());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle3());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle4());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle5());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
            } else {
                Uri front = ShowImageFromPath("IMG_trade_front.jpg", "CarImages");
                Uri licenseplate = ShowImageFromPath("IMG_licencePlate.jpg", "CarImages");
                Uri odometer = ShowImageFromPath("IMG_trade_mileage.jpg", "CarImages");
//                Uri driver = ShowImageFromPath("IMG_left.jpg", "CarImages");
//                Uri window = ShowImageFromPath("IMG_sticker.jpg", "CarImages");
//                Uri odometer = ShowImageFromPath("IMG_mileage.jpg", "CarImages");
                if (front != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(front);
                    vehicleImagePojo.setName("Front");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (licenseplate != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(licenseplate);
                    vehicleImagePojo.setName("License Plate");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
//                if (passenger != null) {
//                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
//                    vehicleImagePojo.setUri(passenger);
//                    vehicleImagePojo.setName("Passenger Side");
//                    imagePojoArrayList.add(vehicleImagePojo);
//                }
//                if (driver != null) {
//                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
//                    vehicleImagePojo.setUri(driver);
//                    vehicleImagePojo.setName("Driver Side");
//                    imagePojoArrayList.add(vehicleImagePojo);
//                }
//                if (window != null) {
//                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
//                    vehicleImagePojo.setUri(window);
//                    vehicleImagePojo.setName("Buyers Guide");
//                    imagePojoArrayList.add(vehicleImagePojo);
//                }
                if (odometer != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(odometer);
                    vehicleImagePojo.setName("Odometer Reading");
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle1());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle2());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle3());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle4());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages") != null) {
                    VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                    vehicleImagePojo.setUri(ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages"));
                    vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle5());
                    imagePojoArrayList.add(vehicleImagePojo);
                }
            }
        }

        if (imagePojoArrayList.size() == 0) {
            clickEnlarge.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        } else {
            clickEnlarge.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(imagePojoArrayList, getActivity(), isIncomplete, getFragmentManager().beginTransaction());

        // setting grid layout manager to implement grid view.
        // in this method '2' represents number of columns to be displayed in grid view.
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);

        // at last set adapter to recycler view.
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void loadImagesForConfirmation() {
        ImageView front = mView.findViewById(R.id.front);
        ImageView rear = mView.findViewById(R.id.rear);
        ImageView right = mView.findViewById(R.id.right);
        ImageView left = mView.findViewById(R.id.left);
        ImageView sticker = mView.findViewById(R.id.windowSticker);
        ImageView mileage = mView.findViewById(R.id.mileage);
        ImageView add1 = mView.findViewById(R.id.additional1);
        ImageView add2 = mView.findViewById(R.id.additional2);
        ImageView add3 = mView.findViewById(R.id.additional3);
        ImageView add4 = mView.findViewById(R.id.additional4);
        ImageView add5 = mView.findViewById(R.id.additional5);
        add1.setVisibility(View.GONE);
        add2.setVisibility(View.GONE);
        add3.setVisibility(View.GONE);
        add4.setVisibility(View.GONE);
        add5.setVisibility(View.GONE);
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isIncompleteDeal()
        if (NewDealViewPager.currentPage == 61) {
            final ArrayList<CustomerVehicleTradeDocs> docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs();
            if (docsArrayList != null && docsArrayList.size() >= 6) {
                for (int i = 0; i < docsArrayList.size(); i++) {
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 1")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add1);
                        add1.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 2")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add2);
                        add2.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 3")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add3);
                        add3.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 4")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add4);
                        add4.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 5")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add5);
                        add5.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }

                }
            } else {
                if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional1.jpg", "CarImages").getPath()).exists()) {
                    loadImageToImageView(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"), add1);
                    add1.setVisibility(View.VISIBLE);
                } else {
                    add1.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional2.jpg", "CarImages").getPath()).exists()) {
                    loadImageToImageView(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"), add2);
                    add2.setVisibility(View.VISIBLE);
                } else {
                    add2.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional3.jpg", "CarImages").getPath()).exists()) {
                    loadImageToImageView(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"), add3);
                    add3.setVisibility(View.VISIBLE);
                } else {
                    add3.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional4.jpg", "CarImages").getPath()).exists()) {
                    add4.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"), add4);
                } else {
                    add4.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional5.jpg", "CarImages").getPath()).exists()) {
                    add5.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"), add5);
                } else {
                    add5.setVisibility(View.GONE);
                }
            }

        } else if (NewDealViewPager.currentPage == 62) {

            final ArrayList<CustomerVehicleTradeDocs> docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTradeInVehicleDocs();
            if (docsArrayList != null && docsArrayList.size() > 3) {
                for (int i = 0; i < docsArrayList.size(); i++) {
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 1")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add1);
                        add1.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 2")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add2);
                        add2.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 3")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add3);
                        add3.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 4")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add4);
                        add4.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 5")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add5);
                        add5.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                }
            } else {
                if (ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages").getPath()).exists()) {
                    add1.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages"), add1);
                } else {
                    add1.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages").getPath()).exists()) {
                    add2.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages"), add2);
                } else {
                    add2.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages").getPath()).exists()) {
                    add3.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages"), add3);
                } else {
                    add3.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages").getPath()).exists()) {
                    add4.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages"), add4);
                } else {
                    add4.setVisibility(View.GONE);
                }
                if (ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages").getPath()).exists()) {
                    add5.setVisibility(View.VISIBLE);
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages"), add5);
                } else {
                    add5.setVisibility(View.GONE);
                }
            }

        }
        if (NewDealViewPager.currentPage == 61) {
            final ArrayList<CustomerVehicleTradeDocs> docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs();
            if (docsArrayList != null && docsArrayList.size() > 0) {
                for (int i = 0; i < docsArrayList.size(); i++) {
                    final int finalI = i;
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("MILEAGEIMG")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), mileage);
                        mileage.setVisibility(View.VISIBLE);
                        final String name = "mileage";
                        mileage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LEFTIMG")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), left);
                        left.setVisibility(View.VISIBLE);
                        left.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String name = "left";
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("RIGHTIMG")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), right);
                        right.setVisibility(View.VISIBLE);
                        right.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String name = "right";
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), sticker);
                        sticker.setVisibility(View.VISIBLE);
                        sticker.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String name = "sticker";
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("REARIMG")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), rear);
                        rear.setVisibility(View.VISIBLE);
                        rear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String name = "rear";
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("FRONTIMG")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), front);
                        front.setVisibility(View.VISIBLE);
                        front.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String name = "front";
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    }
                }
                if (ShowImageFromPath("IMG_mileage.jpg", "CarImages") != null) {
                    loadImageToImageView(ShowImageFromPath("IMG_mileage.jpg", "CarImages"), mileage);
                    mileage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ShowImageFromPath("IMG_mileage.jpg", "CarImages") != null) {
                                showDialog(ShowImageFromPath("IMG_mileage.jpg", "CarImages").toString());
                            } else {
                                loadImagesForConfirmation();
                            }
                        }
                    });

                }


            } else {
                loadImageToImageView(ShowImageFromPath("IMG_front.jpg", "CarImages"), front);
                loadImageToImageView(ShowImageFromPath("IMG_right.jpg", "CarImages"), right);
                loadImageToImageView(ShowImageFromPath("IMG_rear.jpg", "CarImages"), rear);
                loadImageToImageView(ShowImageFromPath("IMG_left.jpg", "CarImages"), left);
                loadImageToImageView(ShowImageFromPath("IMG_sticker.jpg", "CarImages"), sticker);
                loadImageToImageView(ShowImageFromPath("IMG_mileage.jpg", "CarImages"), mileage);
                sticker.setVisibility(View.VISIBLE);
                mileage.setVisibility(View.VISIBLE);
                right.setVisibility(View.VISIBLE);
                left.setVisibility(View.VISIBLE);
                loadImageToImageView(ShowImageFromPath("IMG_front.jpg", "CarImages"), front);
                front.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_front.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_front.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                right.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_right.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_right.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                rear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_rear.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_rear.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                left.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_left.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_left.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                sticker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_sticker.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_sticker.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                mileage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_mileage.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_mileage.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_Additional1.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_Additional2.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_Additional3.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_Additional4.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_Additional5.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
            }

        } else if (NewDealViewPager.currentPage == 62) {
//            sticker.setVisibility(View.GONE);
            mileage.setVisibility(View.GONE);
            right.setVisibility(View.GONE);
            left.setVisibility(View.GONE);
            final ArrayList<CustomerVehicleTradeDocs> docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTradeInVehicleDocs();
            if (docsArrayList != null && docsArrayList.size() > 0) {
                for (int i = 0; i < docsArrayList.size(); i++) {
                    final int finalI = i;
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("TradeInFront")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), sticker);
                        sticker.setVisibility(View.VISIBLE);

                        sticker.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final String name = "trade_front";
                                showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("TRADEINMILEAGE")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), rear);
                        rear.setVisibility(View.VISIBLE);
                        rear.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                showDialog(docsArrayList.get(finalI).getDocUrl(), "mileage");
                            }
                        });
                    } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LICENCEPLATENUMBER")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), front);
                        front.setVisibility(View.VISIBLE);
                        front.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), "licence");
                            }
                        });
                    }
                }
            } else {
                loadImageToImageView(ShowImageFromPath("IMG_licencePlate.jpg", "CarImages"), front);
                loadImageToImageView(ShowImageFromPath("IMG_trade_mileage.jpg", "CarImages"), rear);
                loadImageToImageView(ShowImageFromPath("IMG_trade_front.jpg", "CarImages"), sticker);

                front.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_licencePlate.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_licencePlate.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });

                rear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_trade_mileage.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_trade_mileage.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                sticker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_trade_front.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_trade_front.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
                add5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages") != null) {
                            showDialog(ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages").toString());
                        } else {
                            loadImagesForConfirmation();
                        }
                    }
                });
            }
        }

    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        if (carImages != null) {
            imageView.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
            Glide.with(getActivity())
                    .load(carImages)
                    .apply(requestOptions)
                    .into(imageView);
        } else {
            imageView.setVisibility(View.GONE);
//            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.no_img_pic).error(R.drawable.car_placeholder);
//            Glide.with(getActivity())
//                    .load(carImages)
//                    .apply(requestOptions)
//                    .into(imageView);
        }

    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        if (new File(carImages).exists()) {
            imageView.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
            Glide.with(getActivity())
                    .load(carImages)
                    .apply(requestOptions)
                    .into(imageView);
        } else {
            imageView.setVisibility(View.GONE);
//            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.no_img_pic).error(R.drawable.car_placeholder);
//            Glide.with(getActivity())
//                    .load(carImages)
//                    .apply(requestOptions)
//                    .into(imageView);
        }

    }

    public Uri ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (imgFile.exists()) {
            return Uri.fromFile(imgFile);
//            return rotateImage(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 90);
        }
        return null;
    }

    public String ShowImageFromPathString(String fileName, String mainFolder) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (imgFile.exists()) {
            return Uri.fromFile(imgFile).toString();
//            return rotateImage(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 90);
        }
        return "";
    }

    private Bitmap rotateImages(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
        private int data = 0;

        public BitmapWorkerTask() {
            // Use a WeakReference to ensure the ImageView can be garbage collected
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            loadImagesForConfirmation();
            return null;
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {

        }
    }


    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile = getOutputMediaFile("Signatures", "confirm_car_images_selection");
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    void showDialog(String uri) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", uri);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    void showDialog(String uri, String name) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }
}
