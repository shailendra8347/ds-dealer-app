package com.volga.dealershieldtablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.adapter.RecyclerViewAdapter;
import com.volga.dealershieldtablet.screenRevamping.pojo.VehicleImagePojo;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;

import timber.log.Timber;

import static android.os.Looper.getMainLooper;


public class FragmentConfirmCarImages extends BaseFragment {

    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isConfirmSelected = false;

    IGoToNextPage iGoToNextPage;
    private Button yes, no;
    private boolean isCobuyer;
    private boolean isRemote;
    private DSTextView time;
    boolean isTaken = false;
    private DSTextView sign;
    private ImageView delete;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && isConfirmSelected && isSigned) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    };

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isConfirmSelected && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isConfirmSelected && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);
            } else {
                //all phones
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            signaturePad.clear();
            time.setText(getCurrentTimeString());
//            enableNext(next);
            isSigned = false;
            updateData();
            initPhoto();
            disableWithGray(next);
            //Code for displaying already selected Confirmation
        } else {
            isTaken = false;
        }
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
//                        isTaken = true;
                    File file;
                    if (!isCobuyer) {
                        file = getOutputFile("UserPic", "car_images_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_car_images_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    private void updateData() {
        RecordData recordData;
        Gson gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        SettingsAndPermissions setting = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        isCobuyer = setting.isCoBuyer();
        isRemote = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote();
//        if (isCobuyer){
//            isConfirmSelected = true;
//            yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
//            no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_gray));
//        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }
        if (yes != null) {
            sign = mView.findViewById(R.id.signhere);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                sign.setText(Html.fromHtml(getString(R.string.signature)));
            } else {
                sign.setText(Html.fromHtml(getString(R.string.co_signature)));
            }
            if (setting.isCarDetailConfirmed()) {
                isConfirmSelected = true;
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                yes.setTextColor(getResources().getColor(R.color.white));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.ractangle_border_blue));
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.car_image_combine, container, false);
        isSigned = false;
        isConfirmSelected = false;
        initView();
        return mView;
    }

    private void initView() {
        if (NewDealViewPager.currentPage == 70) {
            initPhoto();
        }
        next = mView.findViewById(R.id.text_next);
        time = mView.findViewById(R.id.time);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setText(String.format(getString(R.string.page1), "3", decidePageNumber()));
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
        delete = mView.findViewById(R.id.deleteButton);
        recyclerView = mView.findViewById(R.id.imageRecycler);
        disableDelete(delete);

//        enableNext(next);
        updateData();
        if (!isCobuyer || isRemote) {
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    yes.setTextColor(getResources().getColor(R.color.white));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    if (isSigned) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                enableWithGreen(next);
                                next.setVisibility(View.VISIBLE);
                            }
                        }, 100);
                    }
                    isConfirmSelected = true;
                }
            });
        }
        if (!isCobuyer || isRemote) {
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    disableWithGray(next);
                    isConfirmSelected = false;
                    no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    no.setTextColor(getResources().getColor(R.color.white));
                    yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    yes.setTextColor(getResources().getColor(R.color.button_color_blue));
                    new AlertDialog.Builder(getActivity())
                            .setMessage(R.string.are_you_sure)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                    intent.putExtra(AppConstant.FROM_FRAGMENT, "handover");
                                    getActivity().startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                                    dialog.dismiss();
                                }
                            }).show();

                }
            });
        }

        disableWithGray(next);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();

                File file;
                if (!isCobuyer) {
                    file = getOutputFile("Screenshot", "car_images_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_car_images_screen");
                }
                scrollableScreenshot(mainContent, file);
//                disableNext(next);
                iGoToNextPage.whatNextClick();
            }
        });

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (!isCobuyer)
                        svgFile = new File(path_signature + "/IMG_confirm_car_images_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg");
                    }
                    File file;
                    if (!isCobuyer) {
                        file = getOutputFile("Screenshot", "car_images_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_car_images_screen");
                    }
                    File file1;
                    if (!isCobuyer) {
                        file1 = getOutputFile("UserPic", "car_images_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_car_images_screen_pic");
                    }
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (!isCobuyer)
                    svgFile = new File(path_signature + "/IMG_confirm_car_images_selection.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg");
                }
                File file;
                if (!isCobuyer) {
                    file = getOutputFile("Screenshot", "car_images_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_car_images_screen");
                }
                File file1;
                if (!isCobuyer) {
                    file1 = getOutputFile("UserPic", "car_images_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "co_car_images_screen_pic");
                }
                Timber.e(svgFile.getName() + " is deleted : " + svgFile.delete());
                Timber.e(file.getName() + " is deleted : " + file.delete());
                Timber.e(file1.getName() + " is deleted : " + file1.delete());
                iGoToNextPage.goToBackIndex();
            }
        });

        updateRecycler();
//        loadImagesForConfirmation();
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                isSigned = true;
                if (isConfirmSelected) {
                    int timeDelay = 50;
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    enableWithGreen(next);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    } else if (isTaken) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
                disableWithGray(next);
            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
            }
        });
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        disableWithGray(next);
                        return true;
                    }
                }
                return true;
            }
        });

    }

    private void updateRecycler() {
        final ArrayList<CustomerVehicleTradeDocs> docsArrayList;
        ArrayList<VehicleImagePojo> imagePojoArrayList = new ArrayList<>();
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        boolean isIncomplete = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isIncompleteDeal() || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()||(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId()!=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().length()>0);
//        if (NewDealViewPager.currentPage == 61) {
        if (isIncomplete) {
            docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs();
            for (int i = 0; i < docsArrayList.size(); i++) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                if (docsArrayList.get(i).getIsAdditionalImage()) {
                    vehicleImagePojo.setName(docsArrayList.get(i).getDocTypeName());
                }else if (docsArrayList.get(i).isInventoryImage()){
                    vehicleImagePojo.setName(docsArrayList.get(i).getDocTypeName());
                }else {
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("FRONTIMG")) {
                        vehicleImagePojo.setName(getString(R.string.front_img));
                    }
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("REARIMG")) {
                        vehicleImagePojo.setName(getString(R.string.back_img));
                    }
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("RIGHTIMG")) {
                        vehicleImagePojo.setName(getString(R.string.passenger_img));
                    }
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LEFTIMG")) {
                        vehicleImagePojo.setName(getString(R.string.driver_img));
                    }
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("MILEAGEIMG")) {
                        vehicleImagePojo.setName(getString(R.string.mileage));
                    }
                    if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails()!=null&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used")) {
                            vehicleImagePojo.setName(getString(R.string.buyer_img));
                        } else {
                            vehicleImagePojo.setName(getString(R.string.msrp_img));
                        }

                    }
//                   if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
//                       vehicleImagePojo.setName("Buyer's Guide");
//                   }
                }
                vehicleImagePojo.setUrl(docsArrayList.get(i).getDocUrl());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            Uri odometer = ShowImageFromPath("IMG_mileage.jpg", "CarImages");
            if (odometer != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(odometer);
                vehicleImagePojo.setName(getString(R.string.mileage));
                imagePojoArrayList.add(vehicleImagePojo);
            }if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5());
                imagePojoArrayList.add(vehicleImagePojo);
            }
        } else {
            Uri front = ShowImageFromPath("IMG_front.jpg", "CarImages");
            Uri passenger = ShowImageFromPath("IMG_right.jpg", "CarImages");
            Uri back = ShowImageFromPath("IMG_rear.jpg", "CarImages");
            Uri driver = ShowImageFromPath("IMG_left.jpg", "CarImages");
            Uri window = ShowImageFromPath("IMG_sticker.jpg", "CarImages");
            Uri odometer = ShowImageFromPath("IMG_mileage.jpg", "CarImages");
            if (front != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(front);
                vehicleImagePojo.setName(getString(R.string.front_img));
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (back != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(back);
                vehicleImagePojo.setName(getString(R.string.back_img));
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (passenger != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(passenger);
                vehicleImagePojo.setName(getString(R.string.passenger_img));
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (driver != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(driver);
                vehicleImagePojo.setName(getString(R.string.driver_img));
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (window != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(window);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails()!=null&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used")) {
                    vehicleImagePojo.setName(getString(R.string.buyer_img));
                } else {
                    vehicleImagePojo.setName(getString(R.string.msrp_img));
                }
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (odometer != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(odometer);
                vehicleImagePojo.setName(getString(R.string.mileage));
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4());
                imagePojoArrayList.add(vehicleImagePojo);
            }
            if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null) {
                VehicleImagePojo vehicleImagePojo = new VehicleImagePojo();
                vehicleImagePojo.setUri(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"));
                vehicleImagePojo.setName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5());
                imagePojoArrayList.add(vehicleImagePojo);
            }

        }

//        }

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(imagePojoArrayList, getActivity(), isIncomplete,getFragmentManager().beginTransaction());

        // setting grid layout manager to implement grid view.
        // in this method '2' represents number of columns to be displayed in grid view.
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);

        // at last set adapter to recycler view.
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        if (new File(carImages).exists()) {
            imageView.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
            Glide.with(getActivity())
                    .load(carImages)
                    .apply(requestOptions)
                    .into(imageView);
        } else {
            imageView.setVisibility(View.GONE);
        }

    }

    private void loadImagesForConfirmation() {
        ImageView front = mView.findViewById(R.id.front);
        ImageView rear = mView.findViewById(R.id.rear);
        ImageView right = mView.findViewById(R.id.right);
        ImageView left = mView.findViewById(R.id.left);
        ImageView sticker = mView.findViewById(R.id.windowSticker);
        ImageView mileage = mView.findViewById(R.id.mileage);

        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        ImageView add1 = mView.findViewById(R.id.additional1);
        ImageView add2 = mView.findViewById(R.id.additional2);
        ImageView add3 = mView.findViewById(R.id.additional3);
        ImageView add4 = mView.findViewById(R.id.additional4);
        ImageView add5 = mView.findViewById(R.id.additional5);
        add1.setVisibility(View.GONE);
        add2.setVisibility(View.GONE);
        add3.setVisibility(View.GONE);
        add4.setVisibility(View.GONE);
        add5.setVisibility(View.GONE);
        final ArrayList<CustomerVehicleTradeDocs> docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs();
        if (docsArrayList != null && docsArrayList.size() > 0) {
            front.setVisibility(View.GONE);
            rear.setVisibility(View.GONE);
            right.setVisibility(View.GONE);
            left.setVisibility(View.GONE);
            sticker.setVisibility(View.GONE);
            mileage.setVisibility(View.GONE);
            for (int i = 0; i < docsArrayList.size(); i++) {
                final int finalI = i;
                if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("MILEAGEIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), mileage);
                    mileage.setVisibility(View.VISIBLE);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHideMileage(false);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    mileage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDialog(docsArrayList.get(finalI).getDocUrl(), "mileage");
                        }
                    });
                } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("LEFTIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), left);
                    left.setVisibility(View.VISIBLE);
                    left.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDialog(docsArrayList.get(finalI).getDocUrl(), "left");
                        }
                    });
                } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("RIGHTIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), right);
                    right.setVisibility(View.VISIBLE);
                    right.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDialog(docsArrayList.get(finalI).getDocUrl(), "right");
                        }
                    });
                } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), sticker);
                    sticker.setVisibility(View.VISIBLE);
                    sticker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDialog(docsArrayList.get(finalI).getDocUrl(), "sticker");
                        }
                    });
                } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("REARIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), rear);
                    rear.setVisibility(View.VISIBLE);
                    rear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDialog(docsArrayList.get(finalI).getDocUrl(), "rear");
                        }
                    });
                } else if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("FRONTIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), front);
                    front.setVisibility(View.VISIBLE);
                    front.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showDialog(docsArrayList.get(finalI).getDocUrl(), "front");
                        }
                    });
                }
            }

            if (ShowImageFromPath("IMG_mileage.jpg", "CarImages") != null) {
                loadImageToImageView(ShowImageFromPath("IMG_mileage.jpg", "CarImages"), mileage);
                mileage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDialog(ShowImageFromPath("IMG_mileage.jpg", "CarImages").toString());
                    }
                });
            }

            if (docsArrayList.size() >= 6) {
                for (int i = 0; i < docsArrayList.size(); i++) {
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 1")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add1);
                        add1.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 2")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add2);
                        add2.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 3")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add3);
                        add3.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 4")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add4);
                        add4.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }
                    if (docsArrayList.get(i).getDocTypeName().contains("Additional Image 5")) {
                        loadImageToImageView(docsArrayList.get(i).getDocUrl(), add5);
                        add5.setVisibility(View.VISIBLE);
                        final int finalI = i;
                        add5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDialog(docsArrayList.get(finalI).getDocUrl(), docsArrayList.get(finalI).getDocTypeName());
                            }
                        });
                    }

                }
            }
        } else {
            if (ShowImageFromPath("IMG_Additional1.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional1.jpg", "CarImages").getPath()).exists()) {
                loadImageToImageView(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"), add1);
                add1.setVisibility(View.VISIBLE);
            } else {
                add1.setVisibility(View.GONE);
            }
            if (ShowImageFromPath("IMG_Additional2.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional2.jpg", "CarImages").getPath()).exists()) {
                loadImageToImageView(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"), add2);
                add2.setVisibility(View.VISIBLE);
            } else {
                add2.setVisibility(View.GONE);
            }
            if (ShowImageFromPath("IMG_Additional3.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional3.jpg", "CarImages").getPath()).exists()) {
                loadImageToImageView(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"), add3);
                add3.setVisibility(View.VISIBLE);
            } else {
                add3.setVisibility(View.GONE);
            }
            if (ShowImageFromPath("IMG_Additional4.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional4.jpg", "CarImages").getPath()).exists()) {
                loadImageToImageView(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"), add4);
                add4.setVisibility(View.VISIBLE);
            } else {
                add4.setVisibility(View.GONE);
            }
            if (ShowImageFromPath("IMG_Additional5.jpg", "CarImages") != null && new File(ShowImageFromPath("IMG_Additional5.jpg", "CarImages").getPath()).exists()) {
                loadImageToImageView(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"), add5);
                add5.setVisibility(View.VISIBLE);
            } else {
                add5.setVisibility(View.GONE);
            }
            loadImageToImageView(ShowImageFromPath("IMG_front.jpg", "CarImages"), front);
            loadImageToImageView(ShowImageFromPath("IMG_right.jpg", "CarImages"), right);
            loadImageToImageView(ShowImageFromPath("IMG_rear.jpg", "CarImages"), rear);
            loadImageToImageView(ShowImageFromPath("IMG_left.jpg", "CarImages"), left);
            loadImageToImageView(ShowImageFromPath("IMG_sticker.jpg", "CarImages"), sticker);
            loadImageToImageView(ShowImageFromPath("IMG_mileage.jpg", "CarImages"), mileage);

            front.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_front.jpg", "CarImages").toString());
                }
            });
            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_right.jpg", "CarImages").toString());
                }
            });
            rear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_rear.jpg", "CarImages").toString());
                }
            });
            left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_left.jpg", "CarImages").toString());
                }
            });
            sticker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_sticker.jpg", "CarImages").toString());
                }
            });
            mileage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_mileage.jpg", "CarImages").toString());
                }
            });
            add1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_Additional1.jpg", "CarImages").toString());
                }
            });
            add2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_Additional2.jpg", "CarImages").toString());
                }
            });
            add3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_Additional3.jpg", "CarImages").toString());
                }
            });
            add4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_Additional4.jpg", "CarImages").toString());
                }
            });
            add5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(ShowImageFromPath("IMG_Additional5.jpg", "CarImages").toString());
                }
            });
        }
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        if (carImages != null) {
            imageView.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
            Glide.with(getActivity())
                    .load(carImages)
                    .apply(requestOptions)
                    .into(imageView);
        } else {
            imageView.setVisibility(View.GONE);
        }

    }

    public Uri ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (imgFile.exists()) {
            return Uri.fromFile(imgFile);
//            return rotateImage(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 90);
        }
        return null;
    }

    Bitmap rotateImages(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
        private int data = 0;

        public BitmapWorkerTask() {
            // Use a WeakReference to ensure the ImageView can be garbage collected
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            loadImagesForConfirmation();
            return null;
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {

        }
    }


    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile;
            if (isCobuyer) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_confirm_car_images_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "confirm_car_images_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    void showDialog(String uri) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", uri);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    void showDialog(String uri, String name) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCarImagesConfirmed(true);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCarImagesConfirmed(true);
//        String recordDataString = gson.toJson(recordData);
//        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

    }
}
