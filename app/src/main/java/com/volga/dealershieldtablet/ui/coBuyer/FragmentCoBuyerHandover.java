package com.volga.dealershieldtablet.ui.coBuyer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentCoBuyerHandover extends BaseFragment {


    private View mView;
    IGoToNextPage iGoToNextPage;
    private AppCompatButton title, esign;
    private boolean isWebDeal;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_customer_handover1, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setVisibility(View.GONE);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
//        DSTextView title = mView.findViewById(R.id.title);
        String a = "Please hand tablet to ";
        String b = "to complete questions";
        String c = "<font color='#F54337'>Co-Buyer </font>";
//        title.setText(Html.fromHtml(a + c + b));
        logout.setVisibility(View.GONE);
//        Button logout = (Button) mView.findViewById(R.id.logout);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        DSTextView next = mView.findViewById(R.id.text_next);
        DSTextView or = mView.findViewById(R.id.or);
        title = mView.findViewById(R.id.moveNext);
        esign = mView.findViewById(R.id.esignDoc);
        title.setOnClickListener(mOnClickListener);
        esign.setOnClickListener(mOnClickListener);
        next.setVisibility(View.GONE);
//        String a = "Please hand tablet to ";
//        String b = "to complete questions";
//        String c = "<font color='#F54337'>Buyer </font>";
        title.setText(Html.fromHtml(getString(R.string.co_buyer_handover)));
        esign.setText(Html.fromHtml(getString(R.string.co_buyer_esign)));
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        isWebDeal = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal();
        if (isWebDeal) {
            esign.setVisibility(View.GONE);
            or.setVisibility(View.GONE);
        } else {
            esign.setVisibility(View.VISIBLE);
            or.setVisibility(View.VISIBLE);
        }
//        Button cancel = (Button) mView.findViewById(R.id.cancel);
//        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        next.setVisibility(View.GONE);
        next.setText(getString(R.string.next_la));
        back.setText(getString(R.string.back_la));
//        cancel.setOnClickListener(mOnClickListener);
    }

    private void openSpecificPage(String selectedLanguage) {

        if (selectedLanguage != null) {
            if (selectedLanguage.equalsIgnoreCase("Chinese")) {
                setLocale("zh", 81);
            } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
                setLocale("es", 81);
            } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
                setLocale("vi", 81);
            } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
                setLocale("ko", 81);
            } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
                setLocale("tl", 81);
            } else if (selectedLanguage.equalsIgnoreCase("English")) {
                setLocale("en", 81);
            }
        } else {
            setLocale("en", 81);
        }
    }

    public void setLocale(String localeName, int page) {
        Lingver.getInstance().setLocale(getActivity(), localeName);
        Log.e("OnLanguageSelect Next: ", " Current locale: " + localeName);

    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.moveNext:
                    title.setClickable(false);
                    title.setEnabled(false);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            title.setEnabled(true);
                            title.setClickable(true);
                        }
                    }, 6000);
                    iGoToNextPage.whatNextClick();
                    AppConstant.TAKE_PHOTO_OR_NOT = true;
                    break;
                case R.id.text_back_up:
                    Gson gson1 = new GsonBuilder().create();
                    RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                        if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                            String recordDataString1 = gson1.toJson(recordData1);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            ((NewDealViewPager) getActivity()).setCurrentPage(58);
                        } else {
                            if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null)
                                openSpecificPage(recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString1 = gson1.toJson(recordData1);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            iGoToNextPage.goToBackIndex();
                        }
                    } else {
                        if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && !recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote()) {
//                        Lingver.getInstance().setLocale(getActivity(), recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                            if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null)
                                openSpecificPage(recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString1 = gson1.toJson(recordData1);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            iGoToNextPage.goToBackIndex();
                        } else if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
//                        Lingver.getInstance().setLocale(getActivity(), recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                            if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null)
                                openSpecificPage(recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString1 = gson1.toJson(recordData1);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            ((NewDealViewPager) getActivity()).setCurrentPage(67);
//                        Intent intent = new Intent(getActivity(), NewDealViewPager.class);
//                        intent.putExtra("page", 66);
//                        getActivity().finish();
                        }
                    }
                    break;
                case R.id.esignDoc:
                    esign.setEnabled(false);
                    esign.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            esign.setEnabled(true);
                            esign.setClickable(true);
                        }
                    }, 6000);
                    Lingver.getInstance().setLocale(getActivity(), "en");
                    Gson gson2 = new GsonBuilder().create();
                    Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                    intent.putExtra(AppConstant.FROM_FRAGMENT, "esignContact");
                    RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    SettingsAndPermissions settingsAndPermissions = recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
                    recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                    recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                    String recordDataString2 = gson2.toJson(recordData2);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString2);
                    intent.putExtra("hasCobuyer", settingsAndPermissions.isHasCoBuyer());
                    intent.putExtra("isCobuyer", true);
                    getActivity().startActivity(intent);
                    break;

            }
        }
    };
}
