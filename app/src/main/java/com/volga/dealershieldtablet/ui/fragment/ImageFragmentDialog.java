package com.volga.dealershieldtablet.ui.fragment;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.Objects;

public class ImageFragmentDialog extends DialogFragment {
    private ImageView imageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);
        TextView imageName = v.findViewById(R.id.imageName);
        imageView = v.findViewById(R.id.imagePreview);
        imageView.setOnTouchListener(new ImageMatrixTouchHandler(getContext()));
        String path = getArguments().getString("url");

        if (!Objects.requireNonNull(path).equalsIgnoreCase("FromCarImagesSample")) {
            if (!path.startsWith("http")) {
                setTitle(path, imageName);
                loadImageToImageView(Uri.parse(path), imageView);
            } else {
                String name = getArguments().getString("name");
//                assert ;
                if (name != null) {
                    if (!name.contains("Additional Image"))
                        setTitle(name, imageName);
                    else {
                        setTitleAdditional(name, imageName);
                    }
                }
                loadImageToImageView(path, imageView);
            }
        } else {
            loadSampleImages(Objects.requireNonNull(getArguments().getString("SampleName")));

        }

        TextView close = v.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        return v;
    }


    private void setTitle(String path, TextView imageName) {
        if (path.contains("left")) {
            imageName.setText(getString(R.string.driverSide));
        } else if (path.contains("right")) {
            imageName.setText(getString(R.string.passengerSide));
        } else if (path.contains("rear")) {
            imageName.setText(getString(R.string.backSide));
        } else if (path.contains("trade_front")) {
            imageName.setText(getString(R.string.picture_front_trade));
        } else if (path.contains("front")) {
            imageName.setText(getString(R.string.frontName));
        } else if (path.contains("sticker")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                imageName.setText(getString(R.string.attendum_sticker));
            } else {
                imageName.setText(getString(R.string.buyers_guide));
            }
//            imageName.setText(getString(R.string.windowSide));
        } else if (path.contains("licence")) {
            imageName.setText(getString(R.string.licenceplateimage));
        } else if (path.contains("mileage")) {
            imageName.setText(getString(R.string.odometerImage));
        } else if (path.contains("DLFRONT")) {
            imageName.setText(getString(R.string.dmv_image));
        } else if (path.contains("IMG_Additional1")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title"))
                imageName.setText(getString(R.string.additionalImage));
            else {
                imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1().trim().length() <= 0 ? getString(R.string.additionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle1());
            }
        } else if (path.contains("IMG_Additional2")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title"))
                imageName.setText(getString(R.string.additionalImage));
            else {
                imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2().trim().length() <= 0 ? getString(R.string.additionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle2());
            }
        } else if (path.contains("IMG_Additional3")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title"))
                imageName.setText(getString(R.string.additionalImage));
            else {
                imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3().trim().length() <= 0 ? getString(R.string.additionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle3());
            }
        } else if (path.contains("IMG_Additional4")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title"))
                imageName.setText(getString(R.string.additionalImage));
            else {
                imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4().trim().length() <= 0 ? getString(R.string.additionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle4());
            }
        } else if (path.contains("IMG_Additional5")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title"))
                imageName.setText(getString(R.string.additionalImage));
            else {
                imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5().trim().length() <= 0 ? getString(R.string.additionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTitle5());
            }
        } else if (path.contains("TradeInAdditional1")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle1().trim().length() <= 0 ? getString(R.string.tradeInAdditionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle1());
        } else if (path.contains("TradeInAdditional2")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle2().trim().length() <= 0 ? getString(R.string.tradeInAdditionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle2());
        } else if (path.contains("TradeInAdditional3")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle3().trim().length() <= 0 ? getString(R.string.tradeInAdditionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle3());
        } else if (path.contains("TradeInAdditional4")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle4().trim().length() <= 0 ? getString(R.string.tradeInAdditionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle4());
        } else if (path.contains("TradeInAdditional5")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            imageName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle5().trim().length() <= 0 ? getString(R.string.tradeInAdditionalImage) : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getTitle5());
        } else {
            imageName.setText(path);
        }

//        else if (path.contains("left")) {
//            imageName.setText(getString(R.string.driverSide));
//        }else if (path.contains("left")) {
//            imageName.setText(getString(R.string.driverSide));
//        }else if (path.contains("left")) {
//            imageName.setText(getString(R.string.driverSide));
//        }else if (path.contains("left")) {
//            imageName.setText(getString(R.string.driverSide));
//        }
    }

    private void setTitleAdditional(String path, TextView imageName) {
        imageName.setText(path);
    }

    private void loadSampleImages(String sampleName) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);

        if (sampleName.equalsIgnoreCase("DMVFront")) {
            Glide.with(getActivity())
                    .load(R.drawable.dmv_fornt)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("Front")) {
            Glide.with(getActivity())
                    .load(R.drawable.front)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("Rear")) {
            Glide.with(getActivity())
                    .load(R.drawable.back_side)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("dmv_back_lipng")) {
            Glide.with(getActivity())
                    .load(R.drawable.back_of_dmv)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("Passenger")) {
            Glide.with(getActivity())
                    .load(R.drawable.passenger_side)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("Driver")) {
            Glide.with(getActivity())
                    .load(R.drawable.drivers_side)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("Mileage")) {
            Glide.with(getActivity())
                    .load(R.drawable.odometer)
                    .apply(requestOptions)
                    .into(imageView);
        } else if (sampleName.equalsIgnoreCase("Sticker")) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            if (!(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().trim().length() == 0)) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used"))
                //Code for displaying already selected Vehicle Type
                {
                    Glide.with(getActivity())
                            .load(R.drawable.window_sticker_old_car)
                            .apply(requestOptions)
                            .into(imageView);
                } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    Glide.with(getActivity())
                            .load(R.drawable.window_sticker_new_car)
                            .apply(requestOptions)
                            .into(imageView);
                }
            }

        }
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

}