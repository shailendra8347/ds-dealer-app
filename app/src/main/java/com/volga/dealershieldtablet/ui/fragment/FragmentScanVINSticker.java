package com.volga.dealershieldtablet.ui.fragment;

import static com.volga.dealershieldtablet.R.layout.layout_processing;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class FragmentScanVINSticker extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(layout_processing, container, false);
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }

    @Override
    public void setUserVisibleHint(final boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (getActivity()!=null) {
                        Intent intent = new Intent(getActivity(), PartialViewContainerActivity.class);
                        Bundle b = new Bundle();
                        b.putBoolean("isVinScanner", true);
                        b.putBoolean("isDMVScanner", false);
                        intent.putExtras(b);
//            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                       getActivity().startActivity(intent);
                    }
                }
            }, 200);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
//        CameraManager.init(getActivity());
//        CameraManager.get().stopPreview();
//        CameraManager.get().closeDriver();
        if (PartialViewContainerActivity.scanningDone) {
            PartialViewContainerActivity.scanningDone = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    iGoToNextPage.whatNextClick();
                }
            }, 300);
        }
        if (PartialViewContainerActivity.skipped) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    iGoToNextPage.whatNextClick();
                }
            }, 300);
        }
        if (PartialViewContainerActivity.logout) {
            PartialViewContainerActivity.logout = false;
            logout();
        }
        if (PartialViewContainerActivity.back) {
            PartialViewContainerActivity.back = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                        iGoToNextPage.goToBackIndex();
                }
            }, 300);
        }

    }

    IGoToNextPage iGoToNextPage;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

}
