package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.Dealership;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.TabUniqueName;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.LocalSalesPerson;
import com.volga.dealershieldtablet.screenRevamping.pojo.NoVerbalPromise;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.ui.adapter.DealerShipTypeAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Select_Dealership extends BaseFragment {

    private View mView;
    private RecyclerView recyclerView;
    private DSTextView next;
    private DSTextView cancel;
    private SwipeRefreshLayout pullToRefresh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_dealership_type, container, false);
        initView();
        return mView;
    }

    public void getDealerships() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Fetching Dealerships");
        progressDialog.show();
        Call<DealershipData> get_dealerships = RetrofitInitialization.getDs_services().get_dealerships("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        get_dealerships.enqueue(new Callback<DealershipData>() {
            @Override
            public void onResponse(Call<DealershipData> call, Response<DealershipData> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    DealershipData dealershipData = response.body();

                    Gson gson = new GsonBuilder().create();
                    String dealershipDataString = gson.toJson(response.body());
                    Log.e("Dealership data: ", dealershipDataString);
                    if (recyclerView != null)
                        recyclerView.setAdapter(new DealerShipTypeAdapter(getActivity(), dealershipData, dealershipSelected));
                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, dealershipDataString);
                    if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        new CustomToast(getActivity()).alert(json.getString("error_description"));
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
                                new CustomToast(getActivity()).toast(arr[1]);
                                logoutInBase();
                            } else {
                                if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
//                                    callRefreshTokenAPI();
                                } else {
                                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                    Locale myLocale = new Locale("en");
                                    Resources res = getResources();
                                    DisplayMetrics dm = res.getDisplayMetrics();
                                    Configuration conf = res.getConfiguration();
                                    conf.locale = myLocale;
                                    res.updateConfiguration(conf, dm);
                                    callHomeActivity();
                                }
                            }

                        }
                        Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());
                        if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<DealershipData> call, Throwable t) {
                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
                if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void initView() {
        recyclerView = (RecyclerView) mView.findViewById(R.id.dealerShipType);
        next = mView.findViewById(R.id.next);
        final DSTextView logout = mView.findViewById(R.id.txt_logout);
        cancel = mView.findViewById(R.id.text_back_up);
        mView.findViewById(R.id.text_cancelBack).setVisibility(View.GONE);
//        cancel.setText(R.string.back);
        next.setVisibility(View.GONE);
        getDealerships();
        pullToRefresh = (SwipeRefreshLayout) mView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (DSAPP.getInstance().isNetworkAvailable()) {
                    getDealerships();
                } else {
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
                pullToRefresh.setRefreshing(false);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(logout, getActivity());
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                next(new FragmentScanDMVFront());
            }
        });
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);

        Gson gson = new GsonBuilder().create();
        DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);

        recyclerView.setAdapter(new DealerShipTypeAdapter(getActivity(), dealershipData, dealershipSelected));
    }

    public void onDestroy() {

        super.onDestroy();
        recyclerView = null;
        mView = null;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            String deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
            if (PreferenceManger.getUniqueTabName() == null || PreferenceManger.getUniqueTabName().getDeviceName() == null || PreferenceManger.getUniqueTabName().getDeviceName().length() == 0)
                getUniqueName("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), deviceId);
        }

    }

    private void getUniqueName(String access_token, String deviceId) {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setMessage("Getting deals...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
        RetrofitInitialization.getDs_services().getUniqueTabName(access_token, deviceId).enqueue(new Callback<TabUniqueName>() {
            @Override
            public void onResponse(Call<TabUniqueName> call, Response<TabUniqueName> response) {
//                progressDialog.dismiss();
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.UNIQU_TAB_OBJECT, json);
//                    uniqueDName = response.body().getDeviceName();
//                    uniqueName.setText(MessageFormat.format("Tab Name: {0}", response.body().getDeviceName()));
//                    getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                } else {
                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("Response ", "onResponse: " + json);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TabUniqueName> call, Throwable t) {
//                progressDialog.dismiss();
                Log.e("API failed tab unique: ", t.getLocalizedMessage());
            }
        });
    }

    public void generateNoteOnSD(Context context, String sBody) {
        try {
            File root = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM), "/DSXT/DeviceID");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "DeviceID.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void callRefreshTokenAPI(final Dealership id) {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
                    getSalesPersonList(id);

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
            }
        });
    }

    private void getSalesPersonList(final Dealership dealershipName) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading " + dealershipName.getDealershipName() + " configuration...");
        progressDialog.show();
        progressDialog.setCancelable(true);
        final String id = dealershipName.getId() + "";
        RetrofitInitialization.getDs_services().getSalesPerson("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<ArrayList<SalesPersonList>>() {
            @Override
            public void onResponse(Call<ArrayList<SalesPersonList>> call, Response<ArrayList<SalesPersonList>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    LocalSalesPerson localSalesPerson = new LocalSalesPerson();
                    localSalesPerson.setLocalSales(response.body());
                    String list = gson.toJson(localSalesPerson);
                    PreferenceManger.putString(AppConstant.SALES_PERSON, list);
                    getVerbalPromiseList(progressDialog, id);
                } else {
                    if (response.code() == 401 || response.code() == 403) {
                        if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                            callRefreshTokenAPI(dealershipName);
                        } else {
                            PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                            PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                            Locale myLocale = new Locale("en");
                            Resources res = getResources();
                            DisplayMetrics dm = res.getDisplayMetrics();
                            Configuration conf = res.getConfiguration();
                            conf.locale = myLocale;
                            res.updateConfiguration(conf, dm);
                            callHomeActivity();
                        }
                    }
                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("Response ", "onResponse: " + json);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SalesPersonList>> call, Throwable t) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void getCheckListItems(final ProgressDialog progressDialog, String id) {
        RetrofitInitialization.getDs_services().getCheckList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<CheckListPojo>() {
            @Override
            public void onResponse(Call<CheckListPojo> call, Response<CheckListPojo> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.CHECK_LIST, list);
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (getActivity() != null && !getActivity().isFinishing())
                                progressDialog.dismiss();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }, 2000);
                    }

                } else {
                    if (getActivity() != null && !getActivity().isFinishing())
                        progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CheckListPojo> call, Throwable t) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void getVerbalPromiseList(final ProgressDialog progressDialog, final String id) {
        RetrofitInitialization.getDs_services().getVerbalPromise("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<NoVerbalPromise>() {
            @Override
            public void onResponse(Call<NoVerbalPromise> call, Response<NoVerbalPromise> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.VERBAL_PROMISE, list);
                    getCheckListItems(progressDialog, id);
                }
            }

            @Override
            public void onFailure(Call<NoVerbalPromise> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    DealerShipTypeAdapter.DealershipSelected dealershipSelected = new DealerShipTypeAdapter.DealershipSelected() {
        @Override
        public void onSelected(int pos) {
            cancel.setEnabled(false);
            cancel.setClickable(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    cancel.setEnabled(true);
                    cancel.setClickable(true);
                }
            }, 3000);
            Gson gson = new GsonBuilder().create();
            DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
            List<Dealership> dealerships = dealershipData.getDealerships();
            if (DSAPP.getInstance().isNetworkAvailable()) {
                getThirdParty(dealerships.get(pos));
                getSalesPersonList(dealerships.get(pos));
                getConditionDisclosure(dealerships.get(pos));
                getConditionDisclosureNew(dealerships.get(pos));
                getHistoryDisclosure(dealerships.get(pos));
//                getThirdParty(dealerships.get(pos));
            } else {
                new CustomToast(getActivity()).alert("To load " + dealerships.get(pos).getDealershipName() + " configuration internet is required.");
            }
        }
    };

    private void getThirdParty(Dealership dealership) {
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    ThirdPartyList vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)&&list!=null) {
                        vehicleConditionDisclosure = new ThirdPartyList();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setViewReport(false);
                            list.get(i).setMandatory(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    ThirdPartyList vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData!=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                    Log.i("ThirdParty: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.THIRD_PARTY_REPORT, dealershipDataString);
//                    Log.e("Size of reports: ", list.size() + "");
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {

            }
        });
    }

    private void getHistoryDisclosure(Dealership dealership) {
        RetrofitInitialization.getDs_services().getVehicleHistoryDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), dealership.getId()+"", "", "0", "1").enqueue(new Callback<VehicleHistoryDisclosure>() {
            @Override
            public void onResponse(Call<VehicleHistoryDisclosure> call, Response<VehicleHistoryDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<Value> list;
                    list = response.body().getValue();
                    VehicleHistoryDisclosure vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)&&list!=null) {
                        vehicleConditionDisclosure = new VehicleHistoryDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    VehicleHistoryDisclosure vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    Log.i("HistoryDisclosure: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                    Log.e("Size of history: ", list.size() + "");
                }
            }

            @Override
            public void onFailure(Call<VehicleHistoryDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
            }
        });
    }

    private void getConditionDisclosure(Dealership dealership) {

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), dealership.getId()+"", "", "0", "1").enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)&&list!=null) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    Log.i("ConditionDisclosure: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE, dealershipDataString);
//                    Log.e("Size of common: ", list.size() + "");
                }
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
            }
        });
    }

    private void getConditionDisclosureNew(Dealership dealership) {

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), dealership.getId()+"", "", "0", "2").enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)&&list!=null) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    Log.i("ConditionDisclosure: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE_NEW, dealershipDataString);
//                    Log.e("Size of common: ", list.size() + "");
                }
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
            }
        });
    }

}
