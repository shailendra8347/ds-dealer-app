package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentThankYouUser2 extends BaseFragment {


    private View mView;

    IGoToNextPage iGoToNextPage;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
            if (visibleHintCalled) {
                if (getActivity() != null) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData;
                    setLocale("en", 0);
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSentEmailForIncomplete(true);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDeal(true);
                            String format = "yyyy-MM-dd HH:mm:ss";
                            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                            long dateInMillis = System.currentTimeMillis();
                            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                            String dateString = sdf.format(new Date(dateInMillis));
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDealSynced(false);
                            ArrayList<ImagesSynced>  arrayList=makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)));
                            arrayList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getMultiSignImages());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(arrayList);
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                        } else {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIsRecordComplete(true);
                            String format = "yyyy-MM-dd HH:mm:ss";
                            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                            long dateInMillis = System.currentTimeMillis();
                            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                            String dateString = sdf.format(new Date(dateInMillis));
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
                            ArrayList<ImagesSynced> arrayList=makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)));
                            arrayList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getMultiSignImages());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(arrayList);
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                        }

                    }

                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote() || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getIsRemote()) {
                            new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew_dis));
                        } else {
                            new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew));
                        }
                    }
//                    new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew));
//                    try {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                            if (!DSAPP.isAppIsInBackground(getActivity())) {
//                                getActivity().startService(new Intent(getActivity(), MyNewService.class));
//                            } else {
//                                getActivity().startForegroundService(new Intent(getActivity(), MyNewService.class));
//                            }
//                        } else {
//                            getActivity().startService(new Intent(getActivity(), MyNewService.class));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            visibleHintCalled = true;
            if (getActivity() != null) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData;
                setLocale("en", 0);
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0&& !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0")) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSentEmailForIncomplete(true);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDeal(true);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDealSynced(false);
                        ArrayList<ImagesSynced>  arrayList=makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)));
                        arrayList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getMultiSignImages());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(arrayList);
//
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                    } else {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIsRecordComplete(true);
                        ArrayList<ImagesSynced>  arrayList=makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)));
                        arrayList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getMultiSignImages());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(arrayList);
//
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                    }

                }
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                try {
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null&&(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails()!=null)) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote() || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getIsRemote()) {
                            new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew_dis));
                        } else {
                            new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew));
                        }
                    }
                    new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew));
                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.thankyou_screen2, container, false);
        initView();
        return mView;
    }


    private void initView() {
        RetrofitInitialization.okHttpClient.build().dispatcher().cancelAll();
        final DSTextView handover = mView.findViewById(R.id.submit);
//        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView copy_text = mView.findViewById(R.id.copy_text);
        DSTextView copy_text1 = mView.findViewById(R.id.copy_text1);
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        try {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote() || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getIsRemote()) {
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        copy_text.setText(getString(R.string.esignDisclosure));
                    } else {
                        copy_text.setText(getString(R.string.esignDisclosureNoInternet));
                    }
                    copy_text1.setText(getString(R.string.esignDisclosure1));
                }
            }
        } catch (Exception e) {
            copy_text.setText(getString(R.string.esignDisclosure));
            copy_text1.setText(getString(R.string.esignDisclosure1));
            e.printStackTrace();
        }

//        text_cancelBack.setText(R.string.page13);
//        text_cancelBack.setVisibility(View.INVISIBLE);
        handover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handover.setEnabled(false);
                handover.setClickable(false);
                setLocale("en", 0);
                getActivity().finishAffinity();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("finalSave", true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                requireActivity().finish();

//                new AlertDialog.Builder(getActivity())
//                        .setMessage(getString(R.string.r_u_dealer))
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setPositiveButton(R.string.yeslast, new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                Gson gson = new GsonBuilder().create();
//                                RecordData recordData;
//                                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIsRecordComplete(true);
//                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
//                                String recordDataString = gson.toJson(recordData);
//                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                                new CustomToast(getActivity()).alert(getString(R.string.data_will_syncnew));
//
//                                setLocale("en", 0);
//                                Intent intent = new Intent(getActivity(), MainActivity.class);
//                                intent.putExtra("isDataPushed", true);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent);
//                                Objects.requireNonNull(getActivity()).finish();
////                                showPopup(getString(R.string.data_will_sync));
//                            }
//                        })
//                        .setNegativeButton(R.string.nolast, new DialogInterface.OnClickListener() {
//
//                            public void onClick(DialogInterface dialog, int whichButton) {
//                                dialog.dismiss();
//                            }
//                        }).show().setCancelable(false);
            }
        });
//        if (PreferenceManger.getBooleanValue(AppConstant.IS_NO_EMAIL_SELECTED)) {
//            copy_text.setText(R.string.copy_prited);
//        } else {
//            copy_text.setText(R.string.copy_mailed);
//        }
    }


    public void setLocale(String localeName, int page) {
        Lingver.getInstance().setLocale(getActivity(), localeName);
//        Locale myLocale = new Locale(localeName);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
    }

    private void showPopup(String textMessage) {
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(R.layout.popup_view);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(textMessage);
        dialog.show();
        dialog.setCancelable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                setLocale("en", 0);
                callHomeActivity();
            }
        }, 3000);
    }

}
