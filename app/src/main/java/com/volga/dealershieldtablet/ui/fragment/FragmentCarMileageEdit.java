package com.volga.dealershieldtablet.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.ViewPagerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.text.MessageFormat;
import java.util.Calendar;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentCarMileageEdit extends BaseFragment {


    private View mView;
    IGoToNextPage iGoToNextPage;
    private DSEdittext mileageEntry;
    private RecordData recordData;
    private Gson gson;
    private View viewRoot;
    private boolean keyBoardcalled = false;
    private DSTextView back;

    public View createView(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.layout_milage_entry, null, false);

        //viewpager

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //retains fragment instance across Activity re-creation
        setRetainInstance(true);

        viewRoot = createView(savedInstanceState);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showFragmentInPortrait();
            updateData();
            if (mileageEntry != null) {
                mileageEntry.requestFocus();
                mileageEntry.setFocusable(true);
                mileageEntry.setInputType(InputType.TYPE_CLASS_NUMBER);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!keyBoardcalled) {
                            keyBoardcalled = true;
                            if (getActivity() != null)
                                KeyboardUtils.showSoftKeyboard(getActivity());
                        }
                    }
                }, 1000);
            }
        }
    }

    private void updateData() {

        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
            if (mileageEntry != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getMileage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getMileage().equalsIgnoreCase("null")) {
                visibleHintCalled = true;
                mileageEntry.setText(MessageFormat.format("{0}", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getMileage()));
            }
        } else {
            if (mileageEntry != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getMileage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getMileage().equalsIgnoreCase("null")) {
                visibleHintCalled = true;
                mileageEntry.setText(MessageFormat.format("{0}", recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getMileage()));
            } else {
                if (mileageEntry != null) {
                    mileageEntry.setText("");
                }
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        mView = inflater.inflate(R.layout.layout_milage_entry, container, false);
        if (viewRoot != null && viewRoot.getParent() != null) {
            ViewGroup parent = (ViewGroup) viewRoot.getParent();
            parent.removeView(viewRoot);
        }
        mView = viewRoot;
        initView();
        return viewRoot;
    }

    private void initView() {
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        back = mView.findViewById(R.id.text_cancelBack);
        DSTextView next = mView.findViewById(R.id.text_next);
        DSTextView cancel = mView.findViewById(R.id.text_back_up);
        Button addMoreImages = mView.findViewById(R.id.addMoreImage);
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        addMoreImages.setVisibility(View.GONE);
        addMoreImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                if (mileageEntry.getText().toString().trim().length() > 0) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((ViewPagerActivity) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setMileage(mileageEntry.getText().toString());
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                    if (checkMandatoryImage()) {
                    iGoToNextPage.whatNextClick();
//                    }
                } else {
                    new CustomToast(getActivity()).alert("Please enter valid miles.");
                }
            }
        });
        mileageEntry = mView.findViewById(R.id.mileageEntry);

        DSTextView title = mView.findViewById(R.id.title);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
            title.setText(Html.fromHtml(getString(R.string.co_milage_entry)));
        } else {
            title.setText(Html.fromHtml(getString(R.string.milage_entry)));
        }
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        if (!visibleHintCalled) {
            updateData();
        }
        mView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });

    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    back.setEnabled(false);
                    back.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            back.setEnabled(true);
                            back.setClickable(true);
                        }
                    }, 6000);
                    KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                    saveData();
                    break;
                case R.id.text_next:
                    KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (mileageEntry.getText().toString().length() > 0) {
                        try {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setMileage(mileageEntry.getText().toString());
                            } else {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setMileage(mileageEntry.getText().toString());
                            }
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(true);
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(false);
//                        } else {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
//                        }

                        /*Write logic to skip additional image page */
                        if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
                            iGoToNextPage.whatNextClick();
                        } else {
                            ((NewDealViewPager) getActivity()).setCurrentPage(54);
                        }
//                        PreferenceManger.putInt(AppConstant.SKIPPED_PAGE,((NewDealViewPager) getActivity()).getCurrentPage());
//                        goToSpecificPage();
                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.enter_miles));
                    }
                    break;
                case R.id.text_back_up:
                    KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                    Gson gson2 = new GsonBuilder().create();
                    RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    boolean takeOdometer = recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeOdometer();
                    boolean isNormal = false;
                    if (recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                        isNormal = recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isNormalFlow();

                    if (!PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(29);
                        } else if (!isNormal && !takeOdometer) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(29);
                        } else
                            iGoToNextPage.goToBackIndex();
                    } else {
                        iGoToNextPage.goToBackIndex();
                    }
                    break;
                case R.id.txt_logout:
                    KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                    Gson gson1 = new GsonBuilder().create();
                    RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (mileageEntry.getText().toString().length() > 0) {
                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                        recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                        if (recordData1 != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setMileage(mileageEntry.getText().toString());
                        } else {
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setMileage(mileageEntry.getText().toString());
                        }
                        String recordDataString = gson1.toJson(recordData1);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        logout();
                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.enter_miles));
                    }
                    break;
            }
        }
    };

    private void goToSpecificPage() {
        PreferenceManger.putInt(AppConstant.SKIPPED_PAGE, ((NewDealViewPager) getActivity()).getCurrentPage());
        ((NewDealViewPager) getActivity()).setCurrentPage(32);
        iGoToNextPage.whatNextClick();
//
    }

    private void saveData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (mileageEntry.getText().toString().length() > 0) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setMileage(mileageEntry.getText().toString());
            } else {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setMileage(mileageEntry.getText().toString());
            }
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            cancel();
        } else {
            new CustomToast(getActivity()).alert(getString(R.string.enter_miles));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
        }
        super.onConfigurationChanged(newConfig);
    }

    private boolean checkMandatoryImage() {

        boolean allTrue = true;
        String path_licence, path_signature, path_car;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (!new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Buyer's DMV Front Image is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasCoBuyer() && !new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Co-Buyer's DMV Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_front.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_right.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Passengers side Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_rear.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Back Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_left.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Left Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_sticker.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Addendum Sticker/Buyers Guide Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Odometer Image side is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Odometer Image side is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Licence Plate Image side is missing.");
            return false;
        } else {

            return true;
        }
    }
}
