package com.volga.dealershieldtablet.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.DecisionMaker;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdPartyReportList;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehiclePhotoType;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.ViewPagerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentVehicleInfo extends BaseFragment {


    private View mView;
    private DSEdittext vinNumber, make, model, year, ExteriorColor, InteriorColor;
    private RecordData recordData;
    private DSEdittext stockNumber;
    DSTextView cancel, back;

    private ProgressDialog progressDialog;
    private boolean isTradeIn = false, isNew = false;
    private LinearLayout main;
    Gson gson = new GsonBuilder().create();
    private boolean takeFront, takeBack, takeRight, takeWindow, takeOdometer, takeLeft, takeAddenmum;
    private DecisionMaker dMaker = new DecisionMaker();
    private ArrayList<VehiclePhotoType> list;
    private boolean isCalled = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_vin_detail_edit, container, false);
        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
        setRetainInstance(true);
        initView();
        return mView;
    }

    private void showAlert() {
        if (getActivity() != null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
            titleTV.setVisibility(View.GONE);
            messageTV.setText("No Internet Connection. Please connect to Internet and try again.");
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    File file = getOutputFile("Screenshot", "alert_vehicle_info_page");
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("No Internet access");
                            customerVehicleDealAlerts.setAlertDesc("After scanning of VIN bar code Internet was not available at vehicle info screen");
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("No Internet access");
                            customerVehicleDealAlerts.setAlertDesc("After scanning of VIN bar code Internet was not available at vehicle info screen");
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    alertD.dismiss();
                }
            });


        }
    }

    private void initView() {

        DSTextView logout = mView.findViewById(R.id.txt_logout);
        back = mView.findViewById(R.id.text_cancelBack);
        back.setVisibility(View.INVISIBLE);
        DSTextView next = mView.findViewById(R.id.text_next);
        cancel = mView.findViewById(R.id.text_back_up);
        DSTextView searchVin = mView.findViewById(R.id.searchVin);
        main = mView.findViewById(R.id.main);
        LinearLayout stock = mView.findViewById(R.id.stock);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                nullyFyData();
                Log.e("Device back: ", " Device back pressed: " + keyCode);
                return false;
            }
            return false;
        });

        vinNumber = mView.findViewById(R.id.vinNumber);
        make = mView.findViewById(R.id.make);
        model = mView.findViewById(R.id.model);
        year = mView.findViewById(R.id.year);
        InteriorColor = mView.findViewById(R.id.InteriorColor);
        ExteriorColor = mView.findViewById(R.id.ExteriorColor);
        DSTextView title = mView.findViewById(R.id.title);
//        visibleHintCalled = false;

        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        try {
            list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getDealershipDetails().getVehiclePhotoTypes();
        } catch (Exception e) {
            Timber.tag("Crashed: ").e(e.toString());
        }

        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getDocumentType().equalsIgnoreCase("FRONTIMG") && list.get(i).getIsActive()) {
                    takeFront = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("REARIMG") && list.get(i).getIsActive()) {
                    takeBack = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("RIGHTIMG") && list.get(i).getIsActive()) {
                    takeRight = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("LEFTIMG") && list.get(i).getIsActive()) {
                    takeLeft = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("MILEAGEIMG") && list.get(i).getIsActive()) {
                    takeOdometer = true;
                }
                if (list.get(i).getVehiclePhotoTypeId() == 6 && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
                    takeWindow = true;
                }
                if (list.get(i).getVehiclePhotoTypeId() == 7 && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
                    takeAddenmum = true;
                }
//                if (list.get(i).getDocTypeTitle().equalsIgnoreCase("Buyers Guide") && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
//                    takeWindow = true;
//                }
//                if (list.get(i).getDocTypeTitle().equalsIgnoreCase("Addendums Sticker") && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
//                    takeAddenmum = true;
//                }
                //recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new")
            }
            boolean normalFlow = true;
            if (takeFront || takeBack || takeRight || takeLeft || takeOdometer || takeWindow) {
                normalFlow = false;
            }
            boolean onlyWindow = false;
            if (!takeOdometer && !takeBack && !takeRight && !takeLeft && !takeFront) {
                onlyWindow = true;
            }
            if (takeAddenmum && takeWindow) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new")) {
                    takeWindow = false;
                } else {
                    takeAddenmum = false;
                }
            }
            //case 5:
//            if (takeAddenmum && takeWindow) {
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && normalFlow) {
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
//                    normalFlow = false;
//                } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
////                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
//                    normalFlow = true;
//                } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && normalFlow) {
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
//                    normalFlow = false;
//                } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && !normalFlow) {
////                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
//                    normalFlow = true;
//                }
//            } else
            if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && !normalFlow) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
                takeWindow = false;
            } else
                //case 6:
                if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
                    takeWindow = true;
                } else
                    //case 3&4
                    if (onlyWindow && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
                        normalFlow = true;
                    } else
                        //case: 7&8
                        if (takeWindow && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
                            takeWindow = false;
//                normalFlow = true;
                        } else

                            //case :
//            if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && normalFlow){
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
//            }
                            //case 1&2:
                            if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && !normalFlow) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
                                takeWindow = false;
                            } else if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && normalFlow) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
                                takeWindow = true;
                                normalFlow = false;
                            } else {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new")) {
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(takeAddenmum);
                                } else {
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(takeWindow);
                                }
                            }


            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeFront(takeFront);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeBack(takeBack);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeLeft(takeLeft);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeOdometer(takeOdometer);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeRight(takeRight);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setNormalFlow(normalFlow);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

            if (!normalFlow) {
                dMaker = MakeDecision();
            }
        }
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
            title.setText(Html.fromHtml(getString(R.string.confirm_edit)));
            year.setImeOptions(EditorInfo.IME_ACTION_DONE);
            isTradeIn = false;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null)
                isNew = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New");
            else {
                isNew = false;
            }
            stock.setVisibility(View.VISIBLE);
        } else {
            stock.setVisibility(View.GONE);
            isTradeIn = true;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null)
                isNew = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New");
            else {
                isNew = false;
            }
            title.setText(Html.fromHtml(getString(R.string.co_confirm_edit)));
        }
        stockNumber = mView.findViewById(R.id.stockNumber);
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);


        if (!visibleHintCalled) {
            updateData();
        }
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });

//        mView.setFocusableInTouchMode(true);
//        mView.requestFocus();
//        mView.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    nullyFiData();
//                    return false;
//                }
//                return false;
//            }
//        });

        searchVin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                if (DSAPP.getInstance().isNetworkAvailable()) {
                    if (vinNumber.getText().toString().trim().length() > 0) {
                        callVinAPI(vinNumber.getText().toString());
                    } else {
                        new CustomToast(getActivity()).alert("Please Enter VIN");
                    }
                } else {
                    insertDataInCaseDeviceOffline();
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
            }
        });
    }

    private DecisionMaker MakeDecision() {
        DecisionMaker decisionMaker = new DecisionMaker();
        if (takeFront) {
            decisionMaker.setIndex(-1);
            decisionMaker.setGoNext(true);
//            iGoToNextPage.whatNextClick();
        } else {
            if (takeRight) {
                decisionMaker.setIndex(5);
                decisionMaker.setGoNext(false);
//                ((NewDealViewPager) getActivity()).setCurrentPage(5);
            } else if (takeBack) {
                decisionMaker.setIndex(7);
                decisionMaker.setGoNext(false);
//                ((NewDealViewPager) getActivity()).setCurrentPage(7);
            } else if (takeLeft) {
                decisionMaker.setIndex(9);
                decisionMaker.setGoNext(false);
//                ((NewDealViewPager) getActivity()).setCurrentPage(9);
            } else if (takeWindow) {
                decisionMaker.setIndex(11);
                decisionMaker.setGoNext(false);
//                ((NewDealViewPager) getActivity()).setCurrentPage(11);
            } else if (takeOdometer) {
                decisionMaker.setIndex(13);
                decisionMaker.setGoNext(false);
            } else {
                decisionMaker.setIndex(-1);
                decisionMaker.setGoNext(true);
            }
        }
        return decisionMaker;
    }

    private void insertDataInCaseDeviceOffline() {
        /*Vehicle condition disclosure*/
        VehicleConditionDisclosure vehicleConditionDisclosure1;
        RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
            vehicleConditionDisclosure1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE_NEW), VehicleConditionDisclosure.class);
        else {
            vehicleConditionDisclosure1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE), VehicleConditionDisclosure.class);
        }
        if (vehicleConditionDisclosure1 != null && vehicleConditionDisclosure1.getValue() != null && vehicleConditionDisclosure1.getValue().size() > 0) {
            ArrayList<Value> list = vehicleConditionDisclosure1.getValue();
            if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                vehicleConditionDisclosure1 = new VehicleConditionDisclosure();
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setIsVerified(false);
                }
            }
            vehicleConditionDisclosure1.setValue(list);
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleConditionDisclosure(vehicleConditionDisclosure1);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }

        /*Vehicle history disclosure*/
        VehicleHistoryDisclosure vehicleHistoryDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.HISTORY_DISCLOSURE), VehicleHistoryDisclosure.class);
        if (vehicleHistoryDisclosure != null && vehicleHistoryDisclosure.getValue() != null && vehicleHistoryDisclosure.getValue().size() > 0) {
            ArrayList<Value> list;
            list = vehicleHistoryDisclosure.getValue();
            VehicleHistoryDisclosure vehicleConditionDisclosure = new VehicleHistoryDisclosure();
            if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setIsVerified(false);
                }
            }
            vehicleConditionDisclosure.setValue(list);
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleHistoryDisclosure(vehicleConditionDisclosure);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(true);

            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }


        /*Adding third party in case device is offline*/
        ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
        if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
            ArrayList<ThirdPartyObj> list = thirdPartyList.getValue();
            ThirdPartyList vehicleConditionDisclosure = new ThirdPartyList();
            if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setViewReport(false);
                    list.get(i).setActive(false);
                    list.get(i).setMandatory(false);
                }
            }
            vehicleConditionDisclosure.setValue(list);
//                            vehicleConditionDisclosure = response.body();
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    back.setClickable(false);
                    back.setEnabled(false);
                    RetrofitInitialization.okHttpClient.build().dispatcher().cancelAll();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            back.setClickable(true);
                            back.setEnabled(true);
                        }
                    }, 3000);
                    if (isDataValid()) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setVINNumber(vinNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarMake(make.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarModel(model.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarYear(year.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setStockNumber(stockNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setExteriorColor(ExteriorColor.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setInteriorColor(InteriorColor.getText().toString().trim());

                        } else if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {

                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVINNumber(vinNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarMake(make.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarModel(model.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarYear(year.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setStockNumber(stockNumber.getText().toString().trim());

                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        cancel();
                    }
                    break;
                case R.id.text_next:
                    if (isDataValid()) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setVINNumber(vinNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarMake(make.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarModel(model.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarYear(year.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setStockNumber(stockNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setExteriorColor(ExteriorColor.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setInteriorColor(InteriorColor.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(ViewPagerActivity.currentPage);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString().trim());

                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        } else {
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVINNumber(vinNumber.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarMake(make.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarModel(model.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarYear(year.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setStockNumber(stockNumber.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(ViewPagerActivity.currentPage);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());

                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            }
                        }

                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                            if (!isTradeIn)
                                ((NewDealViewPager) getActivity()).setCurrentPage(24);
                            else {
//                                ((NewDealViewPager) getActivity()).setCurrentPage(54);
                                iGoToNextPage.whatNextClick();
                            }
                        } else {
                            if (!isTradeIn) {
                                if (list != null && list.size() > 0) {
                                    if (dMaker.isGoNext()) {
                                        iGoToNextPage.whatNextClick();
                                    } else if (dMaker.getIndex() != -1) {
                                        ((NewDealViewPager) getActivity()).setCurrentPage(dMaker.getIndex());
                                    } else {
                                        iGoToNextPage.whatNextClick();
                                    }
                                } else {
                                    iGoToNextPage.whatNextClick();
                                }
                            } else {
                                iGoToNextPage.whatNextClick();
                            }
                        }
                    }
                    break;
                case R.id.text_back_up:
                    nullyFyData();
                    iGoToNextPage.goToBackIndex();
                    break;
                case R.id.txt_logout:
                    if (isDataValid()) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setVINNumber(vinNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarMake(make.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarModel(model.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarYear(year.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setStockNumber(stockNumber.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setExteriorColor(ExteriorColor.getText().toString().trim());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setInteriorColor(InteriorColor.getText().toString().trim());
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        } else {
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVINNumber(vinNumber.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarMake(make.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarModel(model.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarYear(year.getText().toString().trim());
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setStockNumber(stockNumber.getText().toString().trim());
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            }

                        }
                        logout();
                    }
                    break;
            }
        }
    };

    private void nullyFyData() {
        RetrofitInitialization.okHttpClient.build().dispatcher().cancelAll();
        Gson gson1 = new GsonBuilder().create();
        RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
        if (recordData1 != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setVINNumber(null);
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarMake(null);
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarModel(null);
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setCarYear(null);
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setStockNumber(null);
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setExteriorColor(null);
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setInteriorColor(null);
            String recordDataString = gson1.toJson(recordData1);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

        } else {
            if (recordData1 != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVINNumber(null);
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarMake(null);
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarModel(null);
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarYear(null);
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setStockNumber(null);
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(null);
                String recordDataString = gson1.toJson(recordData1);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            }
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            visibleHintCalled = true;
            showFragmentInPortrait();
            if (vinNumber != null) {
                vinNumber.setText("");
                make.setText("");
                model.setText("");
                year.setText("");
                InteriorColor.setText("");
                ExteriorColor.setText("");
                stockNumber.setText("");
            }
            if (!DSAPP.getInstance().isNetworkAvailable() && !isTradeIn) {
                showAlert();
            }
            updateData();
        }
    }

    private String getCorrectText(String string) {
        if (string == null) {
            return "";
        } else if (string.equalsIgnoreCase("null")) {
            return "";
        } else {
            return string;
        }
    }

    private void updateData() {
//        if (NewDealViewPager.currentPage == 2)
//            showFragmentInPortrait();
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (vinNumber != null) {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                mView.findViewById(R.id.intc).setVisibility(View.GONE);
                mView.findViewById(R.id.extc).setVisibility(View.GONE);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber() != null && !PartialViewContainerActivity.skipped) {

                    vinNumber.setText(getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()));
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake() != null) {
                        make.setText(getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake()));
                        model.setText(getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel()));
                        stockNumber.setText(getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getStockNumber()));
                        year.setText(getCorrectText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear()));

                    } else {
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            callVinAPI(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber());
                        } else {
                            insertDataInCaseDeviceOffline();
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }

                    }
                } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber() != null && PartialViewContainerActivity.skipped) {
                    PartialViewContainerActivity.skipped = false;
                    vinNumber.setText("");
                    make.setText("");
                    model.setText("");
                    year.setText("");
                    stockNumber.setText("");
                    if (PartialViewContainerActivity.skipped) {
                        PartialViewContainerActivity.skipped = false;
                    }
                } else {
                    vinNumber.setText("");
                    make.setText("");
                    model.setText("");
                    year.setText("");
                    stockNumber.setText("");
                    if (PartialViewContainerActivity.skipped) {
                        PartialViewContainerActivity.skipped = false;
                    }
                }
            } else {
                vinNumber.setText("");
                make.setText("");
                model.setText("");
                year.setText("");
                InteriorColor.setText("");
                ExteriorColor.setText("");
                stockNumber.setText("");
//                stockNumber.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getStockNumber());
                mView.findViewById(R.id.intc).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.extc).setVisibility(View.VISIBLE);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getVINNumber() != null && !PartialViewContainerActivity.skipped) {
                    vinNumber.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getVINNumber());
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getCarMake() != null) {
                        make.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getCarMake());
                        model.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getCarModel());
                        stockNumber.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getStockNumber());
                        year.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getCarYear());
                        ExteriorColor.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getExteriorColor());
                        InteriorColor.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getInteriorColor());
                    } else {
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            callVinAPI(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getVINNumber());
                        } else {
                            insertDataInCaseDeviceOffline();
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }

                    }
                } else if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().getVINNumber() != null && PartialViewContainerActivity.skipped) {
                    PartialViewContainerActivity.skipped = false;
                    make.setText("");
                    model.setText("");
                    year.setText("");
                    InteriorColor.setText("");
                    ExteriorColor.setText("");
                    vinNumber.setText("");
                    stockNumber.setText("");
                    if (PartialViewContainerActivity.skipped) {
                        PartialViewContainerActivity.skipped = false;
                    }
                } else {
                    make.setText("");
                    model.setText("");
                    year.setText("");
                    InteriorColor.setText("");
                    ExteriorColor.setText("");
                    vinNumber.setText("");
                    stockNumber.setText("");
                    if (PartialViewContainerActivity.skipped) {
                        PartialViewContainerActivity.skipped = false;
                    }
                }
            }

        }

    }

    private void getThirdParty(final String vin) {
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    if (list != null) {
                        ThirdPartyList vehicleConditionDisclosure;
                        if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                            vehicleConditionDisclosure = new ThirdPartyList();
                            for (int i = 0; i < list.size(); i++) {
                                list.get(i).setViewReport(false);
                                list.get(i).setActive(false);
                                list.get(i).setMandatory(false);
                            }
                            vehicleConditionDisclosure.setValue(list);
                        } else
                            vehicleConditionDisclosure = response.body();
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            Timber.i(gson.toJson(vehicleConditionDisclosure));
                        }
//                    ArrayList<ThirdPartyObj> list = record.getVehicleDetails().getThirdPartyList().getValue();
                        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
//                    for (int i = 0; i < list.size(); i++) {
//                        File file = ShowImageFromPath(list.get(i).getTitle().trim() + "1.pdf", "localPdf");
//                        if (list.get(i).isViewReport())
//                            if (file == null || !file.exists()) {
//                                getAllThirdPartyReports(id, vin, list.get(i).getId(), "1", list.get(i).getTitle().trim());
//                                getAllThirdPartyReports(id, vin, list.get(i).getId(), "3", list.get(i).getTitle().trim());
//                            }
//                    }

                        for (int i = 0; i < list.size(); i++) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {

                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(list.get(i).getId()) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(list.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (list.get(i).isActive()) {
                                            if (file == null || !file.exists()) {
                                                getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim());
                                            }
                                            if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_MULTI_SIGN)) {
                                                getAllThirdPartyReportList(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim());
                                            }
                                        }
                                    }
                                }

                            }
                        }
//                    Log.e("Size of reports: ", response.body().getValue().size() + "");
                    }
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {

            }
        });
    }

    private void getAllThirdPartyReportList(String thirdPartyReportRawId, String id, String name) {
        RetrofitInitialization.getDs_services().GetHistoryReportRawPdfBytesList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), thirdPartyReportRawId).enqueue(new Callback<ThirdPartyReportList>() {
            @Override
            public void onResponse(Call<ThirdPartyReportList> call, Response<ThirdPartyReportList> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage().size() != 0) {
                       ArrayList<String> reportList = new ArrayList<>(response.body().getMessage());
                        for (int i = 0; i < reportList.size(); i++) {
                            File dwldsPath = getOutputMediaFile(name+thirdPartyReportRawId, name.trim() + i);
                            byte[] pdfAsBytes = Base64.decode(reportList.get(i), 0);
                            FileOutputStream os;
                            try {
                                os = new FileOutputStream(dwldsPath, false);
                                os.write(pdfAsBytes);
                                os.flush();
                                os.close();
                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
//                        File dwldsPath = getOutputMediaFile(name, name.trim() + TPRRID);
//                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
//                        FileOutputStream os;
//                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
//                        PreferenceManger.putString(AppConstant.ALERT, "");
//                        if (tpId.equalsIgnoreCase("1") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
//                        } else if (tpId.equalsIgnoreCase("2") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
//                        }
//                        String recordDataString = gson.toJson(recordData);
//                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                        try {
//                            os = new FileOutputStream(dwldsPath, false);
//                            os.write(pdfAsBytes);
//                            os.flush();
//                            os.close();
////                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    }

                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(id)) {
                                list.get(i).setThirdPartyList(response.body());
                            }
                        }

                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportList> call, Throwable t) {
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(getActivity())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Timber.tag("LoG..").i("File last modified @ : %s", lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    private void getAllThirdPartyReports(final String TPRRID, final String tpId, final String name) {

//        "40334", "2g1fd1e34f9207229", "1", lng
        RetrofitInitialization.getDs_services().getThirdPartyReportsBytes("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), TPRRID).enqueue(new Callback<ThirdPartyReportBytes>() {
            @Override
            public void onResponse(Call<ThirdPartyReportBytes> call, Response<ThirdPartyReportBytes> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage() != null && response.body().getMessage().length() > 0) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        File dwldsPath = getOutputMediaFile("localPdf", name.trim() + TPRRID);
                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
                        FileOutputStream os;
                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
                        PreferenceManger.putString(AppConstant.ALERT, "");
                        if (tpId.equalsIgnoreCase("1") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
                        } else if (tpId.equalsIgnoreCase("2") && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        try {
                            os = new FileOutputStream(dwldsPath, false);
                            os.write(pdfAsBytes);
                            os.flush();
                            os.close();
//                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(tpId)) {
                                list.get(i).setThirdPartyReportBytes(response.body());
                            }
                        }
                        if (tpId.equalsIgnoreCase("1") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            alerts.setAlertName("CarFax Report is Not Available");
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getErrorMessage());
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(false);
                        } else if (tpId.equalsIgnoreCase("2") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getErrorMessage());
                            alerts.setAlertName("AutoCheck Report is Not Available");
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(false);
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportBytes> call, Throwable t) {
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void callVinAPI(String vin) {
        //Call the API here and fill the textviews!
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.getting_vehicleinfo));
        progressDialog.show();
        if (!isTradeIn && !isNew) {
            callThirdPartyInitiateAPI(vin.toUpperCase());
        } else if (isNew) {
            getConditionDisclosure(vin);
        }


        Call<String> vinDataCall = RetrofitInitialization.getVin_query_services().get_vinData(AppConstant.VIN_QUERY_ACCESS_TOKEN, 3, vin.toUpperCase());
        vinDataCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressDialog.dismiss();
//                new CustomToast(getActivity()).alert("Please wait while we are verifying Disclosures from third parties", false);
                if (response.code() == 200) {
                    try {
                        assert response.body() != null;
                        JSONObject jsonObject = XML.toJSONObject(response.body().toString());
                        JSONObject vinQuery = jsonObject.getJSONObject("VINquery");
                        JSONObject vin = vinQuery.getJSONObject("VIN");

                        if (!vin.getString("Status").equalsIgnoreCase("failed")) {
                            Object vehicle = vin.get("Vehicle");
                            if (vehicle instanceof JSONObject) {
                                JSONObject jsonArray = vin.getJSONObject("Vehicle");
                                year.setText(jsonArray.getString("Model_Year"));
                                make.setText(jsonArray.getString("Make"));
                                model.setText(jsonArray.getString("Model"));
                            } else if (vehicle instanceof JSONArray) {
                                JSONArray jsonArray = vin.getJSONArray("Vehicle");
                                year.setText(jsonArray.getJSONObject(0).getString("Model_Year"));
                                make.setText(jsonArray.getJSONObject(0).getString("Make"));
                                model.setText(jsonArray.getJSONObject(0).getString("Model"));
                            }
                        } else {
                            make.setText("");
                            model.setText("");
                            year.setText("");
                            stockNumber.setText("");
                            new CustomToast(getActivity()).alert(vin.getJSONObject("Message").getString("Value"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        new CustomToast(getActivity()).alert(json.getString("error_description"));
                        progressDialog.dismiss();
                    } catch (IOException | JSONException e) {
                        progressDialog.dismiss();
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                progressDialog.dismiss();
            }
        });
    }

    private void callThirdPartyInitiateAPI(final String vin) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.getting_vehicleinfo));
        progressDialog.show();
        progressDialog.setCancelable(false);
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().initiateThirdParty("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vin, id).enqueue(new Callback<InitiateThirdParty>() {
            @Override
            public void onResponse(Call<InitiateThirdParty> call, Response<InitiateThirdParty> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    gson = new GsonBuilder().create();
                    Timber.e(gson.toJson(response.body()));

                    if (!isTradeIn) {
                        assert response.body() != null;
                        if (response.body().getSucceeded().equalsIgnoreCase("true")) {
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(response.body());
                                boolean hasReport = false;

                                if (response.body().getValue().size() > 0) {
                                    for (int i = 0; i < response.body().getValue().size(); i++) {
                                        if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                            alerts.setAlertName("CarFax Report is Not Available");
                                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                                            PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getValue().get(i).getErrorsString());
                                            alerts.setAlertDesc(response.body().getValue().get(i).getErrorsString());
                                            alerts.setDocId(response.body().getValue().get(i).getThirdPartyID());

                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(false);
                                        } else if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("1") && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxReportAvailable(true);
                                            hasReport = true;
                                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                                        }
                                        if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("2") && response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                                            PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getValue().get(i).getErrorsString());
                                            alerts.setAlertName("AutoCheck Report is Not Available");
                                            alerts.setAlertDesc(response.body().getValue().get(i).getErrorsString());
                                            alerts.setDocId(response.body().getValue().get(i).getThirdPartyID());
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(false);
                                        } else if (response.body().getValue().get(i).getLanguageId().equalsIgnoreCase("1") && response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase("2") && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckReportAvailable(true);
                                            hasReport = true;
                                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                                        }
                                    }
                                }
//                                if (record.getVehicleDetails().getThirdPartyList() == null || record.getVehicleDetails().getThirdPartyList().getValue().size() == 0)
                                getThirdParty(vin);
//                                if (record.getVehicleDetails().getVehicleConditionDisclosure() == null || record.getVehicleDetails().getVehicleConditionDisclosure().getValue().size() == 0)
                                getConditionDisclosure(vin);
//                                if (record.getVehicleDetails().getVehicleHistoryDisclosure() == null || record.getVehicleDetails().getVehicleHistoryDisclosure().getValue().size() == 0)
                                getHistoryDisclosure(vin);

                                //                        if (record.getVehicleDetails().getAdditionalDisclosure() == null || record.getVehicleDetails().getAdditionalDisclosure().getValue().size() == 0)
                                getAdditionalDisclosures(vin, "CONDITION");
                                getAdditionalDisclosures(vin, "HISTORY");

                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                                int duration = 5000;
                                if (hasReport) {
                                    duration = 7000;
                                }
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (getActivity() != null && !getActivity().isFinishing())
                                            progressDialog.dismiss();
                                    }
                                }, duration);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<InitiateThirdParty> call, Throwable t) {
                progressDialog.dismiss();
                Timber.e(t.getLocalizedMessage());
            }
        });
    }

    private void getAdditionalDisclosures(String vin, final String type) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getAdditionalDisclosures("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), /*"1FM5K8D84EGB73657"*/vin, /*"50348"*/id, type).enqueue(new Callback<AdditionalDisclosure>() {
            @Override
            public void onResponse(Call<AdditionalDisclosure> call, Response<AdditionalDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        if (type.equalsIgnoreCase("CONDITION"))
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(response.body());
                        else if (type.equalsIgnoreCase("HISTORY")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalHistoryDisclosure(response.body());
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Log.i("AdditionalDisclosure: ", gson.toJson(response.body()));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                        Log.e("Size of history: ", response.body().getValue().size() + "");
                    }
                } else {
                    try {
                        Log.e("Error Addtnl ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AdditionalDisclosure> call, Throwable t) {

            }
        });
    }
/**/
    private void getHistoryDisclosure(String vin) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String vehicleType = "1";
        ArrayList<HistoryListPojo> localLi = new ArrayList<>();
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
            }

            if (record.getVehicleDetails().getTypeOfVehicle() != null) {
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                    vehicleType = "1";
                } else {
                    vehicleType = "2";
                }
            }
        }
        RetrofitInitialization.getDs_services().getVehicleHistoryDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, vin, "0", vehicleType).enqueue(new Callback<VehicleHistoryDisclosure>() {
            @Override
            public void onResponse(Call<VehicleHistoryDisclosure> call, Response<VehicleHistoryDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<Value> list;
                    list = response.body().getValue();
                    VehicleHistoryDisclosure vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleHistoryDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleHistoryDisclosure(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistoryVerified(true);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Timber.i(gson.toJson(vehicleConditionDisclosure));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                        Timber.e(response.body().getValue().size() + "");
                    }
                }
            }

            @Override
            public void onFailure(Call<VehicleHistoryDisclosure> call, Throwable t) {
                Timber.e(t.getLocalizedMessage());
            }
        });
    }

    private void getConditionDisclosure(String vin) {
        back.setClickable(false);
        back.setEnabled(false);

        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(true);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        String vehicleType = "1";
        ArrayList<HistoryListPojo> localLi = new ArrayList<>();
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
            }

            if (record.getVehicleDetails().getTypeOfVehicle() != null) {
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                    vehicleType = "1";
                } else {
                    vehicleType = "2";
                }
            }
        }

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, vin, "0", vehicleType).enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        back.setClickable(true);
                        back.setEnabled(true);
                    }
                }, 2000);
                if (response.isSuccessful() && response.code() == 200) {
                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleConditionDisclosure(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setWarningAlerts(response.body().getWarningAlerts());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionVerified(true);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Timber.i(gson.toJson(vehicleConditionDisclosure));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE, dealershipDataString);
//                        Timber.e("%s", response.body().getValue().size());
                    }
                }
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        back.setClickable(true);
                        back.setEnabled(true);
                    }
                }, 2000);
            }
        });
    }

    private boolean isDataValid() {
        Calendar mcurrentDate = Calendar.getInstance();
//        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        boolean isTradeIn = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn();
//        String used = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle();
        if (vinNumber.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert("Please Enter VIN");
            return false;
        } else if (vinNumber.getText().toString().trim().length() != 17) {
            new CustomToast(getActivity()).alert("Invalid VIN Number: A Valid VIN Number must be exactly 17 digits.");
            return false;
        } else if (make.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert("Please Enter Make");
            return false;
        } else if (model.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert("Please Enter Model");
            return false;
        } else if (year.getText().toString().trim().length() == 0) {
            new CustomToast(getActivity()).alert("Please Enter Year");
            return false;
        }
//        else if (used.equalsIgnoreCase("used") || isTradeIn) {
//            if ((year.getText().toString().length() != 0 && Integer.parseInt(year.getText().toString()) > mYear)) {
//                new CustomToast(getActivity()).alert("Please Enter Valid Year");
//                return false;
//            }
//        }
        /*Previous condition*/
//          else if ((year.getText().toString().length() != 0 && Integer.parseInt(year.getText().toString()) > mYear + 2) || Integer.parseInt(year.getText().toString()) < 1885) {
        else if (year.getText().toString().length() != 0 && Integer.parseInt(year.getText().toString()) < 1885) {
            new CustomToast(getActivity()).alert("Please Enter Valid Year");
            return false;
        }
        return true;
    }

    IGoToNextPage iGoToNextPage;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

}
