package com.volga.dealershieldtablet.ui.additionalImages;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.cameraview.CameraView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import timber.log.Timber;

/**
 * Created by ${Shailendra} on 23-08-2018.
 */
public class AdditionalImageFragment extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final String TAG = "MainActivity";
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final String FRAGMENT_DIALOG = "dialog";
    IGoToNextPage iGoToNextPage;
    private View mView;
    private DSTextView next;
    private DSEdittext desc;
    private Button takePicture;
    private ProgressDialog progressDialog;
    private String addImagePage;
    private TextView textView;
    private CameraView mCameraView;
    private Handler mBackgroundHandler;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.take_picture:
                    KeyboardUtils.hideSoftKeyboard(desc, getActivity());
                    progressDialog.show();
//                    takePicture.setVisibility(View.GONE);
//                    takePicture.setEnabled(false);
                    disableNext(takePicture);
                    if (mCameraView != null&&mCameraView.isCameraOpened()) {
                        mCameraView.takePicture();
                    }
                    break;
                case R.id.text_cancelBack:
                    KeyboardUtils.hideSoftKeyboard(desc, getActivity());
                    cancel();
                    break;
                case R.id.text_next:
                    KeyboardUtils.hideSoftKeyboard(desc, getActivity());
                    iGoToNextPage.whatNextClick();
                    break;
                case R.id.text_back:
                    KeyboardUtils.hideSoftKeyboard(desc, getActivity());
                    iGoToNextPage.goToBackIndex();
                    break;
                case R.id.txt_logout:
                    KeyboardUtils.hideSoftKeyboard(desc, getActivity());
                    logout();
                    break;
            }
        }
    };
    private CameraView.Callback mCallback
            = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
            Timber.d("onCameraOpened");
            new Handler().postDelayed(() -> takePicture.setVisibility(View.VISIBLE),1600);

        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            Log.d(TAG, "onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] data) {
            Log.d(TAG, "onPictureTaken " + data.length);
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            String imageName = "";
            if (textView.getText().toString().trim().equalsIgnoreCase("Tap to edit title")) {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    textView.setText(getText(R.string.additionalImage));
                else {
                    textView.setText(getText(R.string.tradeInAdditionalImage));
                }
            }
            if (addImagePage.equalsIgnoreCase("1")) {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    imageName = "additional1";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTitle1(getString(R.string.additionalImage) + " 1" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                } else {
                    imageName = "TradeInAdditional1";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setTitle1(getString(R.string.tradeInAdditionalImage) + " 1" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                }
            } else if (addImagePage.equalsIgnoreCase("2")) {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    imageName = "additional2";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTitle2(getString(R.string.additionalImage) + " 2" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                } else {
                    imageName = "TradeInAdditional2";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setTitle2(getString(R.string.tradeInAdditionalImage) + " 2" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                }
            } else if (addImagePage.equalsIgnoreCase("3")) {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    imageName = "additional3";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTitle3(getString(R.string.additionalImage) + " 3" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                } else {
                    imageName = "TradeInAdditional3";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setTitle3(getString(R.string.tradeInAdditionalImage) + " 3" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                }
            } else if (addImagePage.equalsIgnoreCase("4")) {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    imageName = "additional4";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTitle4(getString(R.string.additionalImage) + " 4" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                } else {
                    imageName = "TradeInAdditional4";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setTitle4(getString(R.string.tradeInAdditionalImage) + " 4" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                }
            } else if (addImagePage.equalsIgnoreCase("5")) {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    imageName = "additional5";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTitle5(getString(R.string.additionalImage) + " 5" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                } else {
                    imageName = "TradeInAdditional5";
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setTitle5(getString(R.string.tradeInAdditionalImage) + " 5" + (desc.getText().toString().trim().length() > 0 ? ": " + desc.getText().toString().trim() : desc.getText().toString().trim()));
                }
            } else {
                imageName = addImagePage;
            }

            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            File file = getOutputMediaFile("CarImages", imageName);
            SaveImageToMemory saveImageToMemory = new SaveImageToMemory(data, file);
            saveImageToMemory.execute();
//            compressImage(data, file);
        }

    };

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(final boolean isVisibleToUser) {
        if (mCameraView != null) {
            if (isVisibleToUser) {
                enableNext(takePicture);
                visibleHintCalled = true;
                if (Build.MODEL.equalsIgnoreCase("SM-T380")||Build.MODEL.equalsIgnoreCase("SM-T385")){
                    forMajorDevices();
                }else {
                    forMinorDevices();
                }

                showFragmentInLandscape();
                mCameraView.setOrientation(90);
            } else {
                if (mCameraView.isCameraOpened())
                    mCameraView.stop();
            }

        }
        super.setUserVisibleHint(isVisibleToUser);
    }
    private void forMajorDevices() {
            mCameraView.start();
    }

    private void forMinorDevices() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    mCameraView.start();
            }
        }, 300);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.capture_additional_images, container, false);
        initView();
        return mView;
    }

    private void initView() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.optimizing_image));
        progressDialog.setCancelable(false);
        addImagePage = getArguments().getString("pageNumber", "");
        mCameraView = (CameraView) mView.findViewById(R.id.camera);
        if (mCameraView != null) {
            mCameraView.addCallback(mCallback);
        }
        takePicture = (Button) mView.findViewById(R.id.take_picture);
        takePicture.setText(R.string.take_pic);
        takePicture.setVisibility(View.GONE);
        desc = mView.findViewById(R.id.desc);
        textView = mView.findViewById(R.id.text);
        final ImageView flash = mView.findViewById(R.id.flashButton);
        if (hasFlash()) {
            mCameraView.setFlash(CameraView.FLASH_OFF);
            flash.setVisibility(View.VISIBLE);
            flash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCameraView.getFlash() == CameraView.FLASH_OFF) {
                        flash.setImageResource(R.drawable.flashbuttonon);
                        mCameraView.setFlash(CameraView.FLASH_TORCH);
                    } else {
                        flash.setImageResource(R.drawable.flashbuttonoff);
                        mCameraView.setFlash(CameraView.FLASH_OFF);
                    }
                }
            });
        }else {
            flash.setVisibility(View.GONE);
        }
        if (!addImagePage.equalsIgnoreCase("insurance")) {
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textView.setVisibility(View.GONE);
                    desc.setVisibility(View.VISIBLE);
                    desc.setInputType(InputType.TYPE_CLASS_TEXT);
                    desc.setFocusable(true);
                    desc.setFocusableInTouchMode(true);
                    desc.setEnabled(true);
                    desc.setHint("");

                    if (!textView.getText().toString().equalsIgnoreCase("Tap to edit title")) {
                        desc.setText(textView.getText().toString());
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                desc.setSelection(textView.getText().toString().length());
                            }
                        });
                    }
                    desc.requestFocus();
                    KeyboardUtils.showSoftKeyboard(getActivity());
                }
            });
        } else {
            textView.setOnClickListener(null);
        }
        desc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView text, int i, KeyEvent keyEvent) {
                if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                    Log.i(TAG, "Enter pressed");
                    textView.setVisibility(View.VISIBLE);
                    desc.setVisibility(View.GONE);
                    if (desc.getText().toString().trim().length() > 0) {
                        textView.setText(desc.getText().toString());
                    } else {
                        textView.setText(getText(R.string.additionalImage));
                    }
                    KeyboardUtils.hideSoftKeyboard(desc, getActivity());
                }
                return false;
            }
        });
        if (addImagePage.equalsIgnoreCase("insurance")) {
            desc.setText(R.string.insurence_image_new);
            textView.setText(R.string.insurence_image_new);
            desc.setOnClickListener(null);
        } else {
//            desc.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    textView.setVisibility(View.VISIBLE);
//                    desc.setVisibility(View.GONE);
//                    textView.setText(desc.getText().toString());
//
//                }
//            });
        }

//        desc.setText(R.string.window_sticker_pic);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        DSTextView back = mView.findViewById(R.id.text_cancelBack);
        next = mView.findViewById(R.id.text_next);
        DSTextView cancel = mView.findViewById(R.id.text_back);
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        next.setVisibility(View.GONE);
        back.setOnClickListener(mOnClickListener);

        cancel.setOnClickListener(mOnClickListener);
        if (takePicture != null) {
            takePicture.setOnClickListener(mOnClickListener);
        }
//        ImageView sampleImage=mView.findViewById(R.id.sampleImage);
//        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarStickerTime(Calendar.getInstance().getTime().toString());
//        if (!(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().length() == 0)) {
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase(getString(R.string.used)))
//            //Code for displaying already selected Vehicle Type
//            {
//                Glide.with(getActivity())
//                        .load(R.drawable.window_sticker_old_car)
//                        .apply(requestOptions)
//                        .into(sampleImage);
//            } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
//                Glide.with(getActivity())
//                        .load(R.drawable.window_sticker_new_car)
//                        .apply(requestOptions)
//                        .into(sampleImage);
//            }
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (visibleHintCalled)
                    mCameraView.start();
                }
            }, 300);
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCameraView.isCameraOpened())
            mCameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBackgroundHandler != null) {
            mBackgroundHandler.getLooper().quitSafely();
            mBackgroundHandler = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
                    throw new RuntimeException("Error on requesting camera permission.");
                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), R.string.camera_permission_not_granted,
                            Toast.LENGTH_SHORT).show();
                }
                // No need to start camera here; it is handled by onResume
                break;
        }
    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    /**
     * Create a File for saving an image or video
     */
    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public Bitmap ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        if (imgFile.exists()) {
            return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return null;
    }

    public void compressImage(byte[] data, File file) {
        OutputStream os = null;
        try {

            os = new FileOutputStream(file);
            os.write(data);
            os.close();
            Log.e(TAG, "doInBackground: " + file.length() / 1024);

        } catch (IOException e) {
            Log.w(TAG, "Cannot write to " + file, e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // Ignore
                }
            }

        }
//        optimizeImage(file,data);
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static ConfirmationDialogFragment newInstance(@StringRes int message,
                                                             String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

    }

    private class SaveImageToMemory extends AsyncTask<String, Void, String> {
        byte[] data;
        File file;

        SaveImageToMemory(byte[] data, File file) {
            this.data = data;
            this.file = file;
        }


        @Override
        protected String doInBackground(String... params) {

            compressImage(data, file);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            iGoToNextPage.whatNextClick();
//            enableNext(takePicture);
        }


    }

}
