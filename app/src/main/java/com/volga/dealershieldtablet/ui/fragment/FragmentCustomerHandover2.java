package com.volga.dealershieldtablet.ui.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentCustomerHandover2 extends BaseFragment {


    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_customer_handover2, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        DSTextView back = mView.findViewById(R.id.text_back);
        DSTextView next = mView.findViewById(R.id.text_next);
        DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
//        cancel.setVisibility(View.GONE);
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    cancel();
                    break;
                case R.id.text_next:
                    getActivity().finish();
                    break;
                case R.id.text_back:
                    getActivity().finish();
                    break;
                case R.id.txt_logout:
                    logout();
                    break;
            }
        }
    };
}
