package com.volga.dealershieldtablet.ui.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeHistoryDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeThirdPartyHistoryReports;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.NetworkChangeReceiver;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.activity.ReviewEditDisclosureViewPager;
import com.volga.dealershieldtablet.ui.activity.CustomWebViewActivity;
import com.volga.dealershieldtablet.ui.adapter.HistoryAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.volga.dealershieldtablet.interfaceCallback.NetworkChangeReceiver.IS_NETWORK_AVAILABLE;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class FragmentUsedVehicleHistory extends BaseFragment implements HistoryAdapter.IOnLanguageSelect {
    private IGoToNextPage iGoToNextPage;
    private View mView;
    private boolean isAutoCheck = false;
    private boolean shouldNextShow;
    private View next;
    private HistoryAdapter adapter;
    private boolean isALI3Played = false;
    private ImageView imageView;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    IntentFilter intentFilter;
    BroadcastReceiver br;
    BroadcastReceiver broadcastReceiverAPI = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra(NetworkChangeReceiver.STARTED_API_CALLS, false) && NewDealViewPager.currentPage == 26) {
                    progressDialog1 = new ProgressDialog(getActivity());
                    progressDialog1.setMessage("Verifying Disclosures");
                    progressDialog1.setCancelable(false);
                    progressDialog1.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog1.setCancelable(true);
                        }
                    }, 14000);
                }
            }
        }
    };
    private ProgressDialog progressDialog1;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && next != null && !isAutoCheck) {
            Gson gson = new GsonBuilder().create();
            //addded
            shouldNextShow = false;
            initView();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                ArrayList<InitThirdParty> initList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
                for (int i = 0; i < initList.size(); i++) {
                    if (initList.get(i).getLanguageId().equalsIgnoreCase("1") && initList.get(i).getThirdPartyID().equalsIgnoreCase("1") && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                        showAlert(initList.get(i).getErrorsString(), "1", "CARFAX Not Available. Please make manual disclosures.");
                    }
                    if (initList.get(i).getLanguageId().equalsIgnoreCase("1") && initList.get(i).getThirdPartyID().equalsIgnoreCase("2") && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                        showAlert(initList.get(i).getErrorsString(), "2", "AutoCheck Not Available. Please make manual disclosures.");
                    }
                }
            }
        }
        if (isVisibleToUser && next != null && !isALI3Played) {

            isALI3Played = true;
            ali3Logic(imageView);
        }
        if (isVisibleToUser && next != null) {
            Gson gson = new GsonBuilder().create();
            final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isConditionVerified() && !isAutoCheck) {
                Log.e(" Called: ", "getHistoryDisclosure");
                getHistoryDisclosure(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber());
            }
        }
        if (!isVisibleToUser) {
            isALI3Played = false;
        }
//        if ()
    }

    private void getHistoryDisclosure(String vin) {
        getAdditionalDisclosures(vin, "CONDITION");
        getAdditionalDisclosures(vin, "HISTORY");
        ProgressDialog progressDialog1 = new ProgressDialog(getActivity());
        progressDialog1.setMessage("Verifying Disclosures...");
        progressDialog1.setCancelable(false);
        progressDialog1.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog1.setCancelable(true);
            }
        }, 14000);
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String vehicleType = "1";
        ArrayList<HistoryListPojo> localLi = new ArrayList<>();
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
            }

            if (record.getVehicleDetails().getTypeOfVehicle() != null) {
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                    vehicleType = "1";
                } else {
                    vehicleType = "2";
                }
            }
        }
        RetrofitInitialization.getDs_services().getVehicleHistoryDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, vin, "0", vehicleType).enqueue(new Callback<VehicleHistoryDisclosure>() {
            @Override
            public void onResponse(Call<VehicleHistoryDisclosure> call, Response<VehicleHistoryDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<Value> list;
                    list = response.body().getValue();
                    VehicleHistoryDisclosure vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleHistoryDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleHistoryDisclosure(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistoryVerified(true);
                        String recordDataString = gson.toJson(recordData);

                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        initView();

                        Timber.i(gson.toJson(vehicleConditionDisclosure));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                        Timber.e(response.body().getValue().size() + "");
                    }
                    progressDialog1.dismiss();
                }
            }

            @Override
            public void onFailure(Call<VehicleHistoryDisclosure> call, Throwable t) {
                progressDialog1.dismiss();
                Timber.e(t.getLocalizedMessage());
            }
        });
    }
    private void getAdditionalDisclosures(String vin, final String type) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getAdditionalDisclosures("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), /*"1FM5K8D84EGB73657"*/vin, /*"50348"*/id, type).enqueue(new Callback<AdditionalDisclosure>() {
            @Override
            public void onResponse(Call<AdditionalDisclosure> call, Response<AdditionalDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        if (type.equalsIgnoreCase("CONDITION") /*&& (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue().size() == 0)*/) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue().size() > 0) {
                                ArrayList<Value> local = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue();
                                ArrayList<Value> local1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue();
                                if (response.body() != null && response.body().getValue() != null && response.body().getValue().size() > 0) {
                                    for (int i = 0; i < response.body().getValue().size(); i++) {
                                        for (int j = 0; j < local.size(); j++) {
                                            if (response.body().getValue().get(i).getId().equalsIgnoreCase(local.get(j).getId())) {
                                                local1.get(j).setManual(false);
                                            }
                                        }
                                    }
                                }
                                AdditionalDisclosure additionalDisclosure1 = new AdditionalDisclosure();
                                additionalDisclosure1.setValue(local1);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(additionalDisclosure1);
                            } else {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(response.body());
                            }
                        } else if (type.equalsIgnoreCase("HISTORY")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalHistoryDisclosure(response.body());
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Log.i("AdditionalDisclosure: ", gson.toJson(response.body()));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                        Log.e("Size of history: ", response.body().getValue().size() + "");
                    }
                } else {
                    try {
                        Log.e("Error Addtnl ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AdditionalDisclosure> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("NetworkChangeReceiver: ", "Registered");
        intentFilter = new IntentFilter(NetworkChangeReceiver.NETWORK_AVAILABLE_ACTION);
        getActivity().registerReceiver(broadcastReceiverAPI, new IntentFilter("StartedAPI"));
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                String networkStatus = isNetworkAvailable ? "connected" : "disconnected";
                Log.e("Status: ", networkStatus);
                saveRecord();
                initView();
                if (progressDialog1 != null && progressDialog1.isShowing()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog1.dismiss();
                        }
                    }, 3500);
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(br, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.tag("NetworkChangeReceiver: ").e("UnRegistered");
        getActivity().unregisterReceiver(broadcastReceiverAPI);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(br);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.used_vehicle_history, container, false);
        initView();
        return mView;
    }

    private void initView() {
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        DSTextView history = mView.findViewById(R.id.history1);
        mView.findViewById(R.id.recyclerView).setFocusable(false);
        imageView = mView.findViewById(R.id.gif);
//
        mView.findViewById(R.id.temp).requestFocus();
        DSTextView history1 = mView.findViewById(R.id.history);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        next = mView.findViewById(R.id.text_next);
        LinearLayout thirdPartyContainer = mView.findViewById(R.id.thirdPartyContainer);
        final ImageButton carfax = mView.findViewById(R.id.carfax);
        final ImageButton autocheck = mView.findViewById(R.id.autocheck);
        ArrayList<HistoryListPojo> historyListPojos = new ArrayList<>();
        isAutoCheck = getArguments().getBoolean("isAutoCheck", false);
        if (!isALI3Played && isAutoCheck && NewDealViewPager.currentPage == 27) {
            isALI3Played = true;
            ali3Logic(imageView);
        }
        if (!isALI3Played && !isAutoCheck && NewDealViewPager.currentPage == 26) {
            isALI3Played = true;
            ali3Logic(imageView);
        }
        Gson gson = new GsonBuilder().create();
        LinearLayout ali3Logo = mView.findViewById(R.id.ali3Logo);
        if (PreferenceManger.getBooleanValue(AppConstant.IS_AI_ENABLED)) {
            ali3Logo.setVisibility(View.VISIBLE);
        } else {
            ali3Logo.setVisibility(View.GONE);
        }
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String newOrUsed;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
            newOrUsed = "";
        else
            newOrUsed = "USED";

//        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isHistoryVerified()){
//            getHistoryDisclosure(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber());
//        }

        String[] list = new String[0];
        if (!isAutoCheck) {

            ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
            ArrayList<HistoryListPojo> localLi1 = new ArrayList<>();
            if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//                    if (record.getVehicleDetails().getUsedVehicleReport() != null) {
//                        localLi1 = new ArrayList<>(record.getVehicleDetails().getUsedVehicleReport());
//                    } else {
                    for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                        HistoryListPojo historyListPojo = new HistoryListPojo();
                        historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                        historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                        historyListPojo.setChecked(false);
//                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isViewReport());
                        historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                        historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                        historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                        localLi1.add(historyListPojo);
                    }
//                    }
                    boolean showCarFax = false, showAutoCheck = false;
                    for (int i = 0; i < localLi1.size(); i++) {
                        if (localLi1.get(i).getId().equalsIgnoreCase("1") && localLi1.get(i).isVerified()) {
                            showCarFax = true;
                        } else if (localLi1.get(i).getId().equalsIgnoreCase("2") && localLi1.get(i).isVerified()) {
                            showAutoCheck = true;
                        }
                    }
                    if (showAutoCheck && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        autocheck.setVisibility(View.VISIBLE);
                    } else {
                        autocheck.setVisibility(View.GONE);
                    }
                    if (showCarFax && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        carfax.setVisibility(View.VISIBLE);
                    } else {
                        carfax.setVisibility(View.GONE);
                    }

                    carfax.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            carfax.setEnabled(false);
                            carfax.setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    carfax.setEnabled(true);
                                    carfax.setClickable(true);
                                }
                            }, 3000);
                            checkIfReportAvailable(true, "1");
                        }
                    });
                    autocheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            autocheck.setEnabled(false);
                            autocheck.setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    autocheck.setEnabled(true);
                                    autocheck.setClickable(true);
                                }
                            }, 3000);
                            checkIfReportAvailable(false, "2");
                        }
                    });
                }
            }
            thirdPartyContainer.setVisibility(View.VISIBLE);

            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//            newOrUsed = "";
            {
                history1.setText(R.string.vehicle_history);
            } else {
                newOrUsed = "USED";
                history1.setText(R.string.used_vehicle_history);
            }
            AppConstant.historyLists.clear();
            AppConstant.history1Time = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate1();
            AppConstant.history2Time = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate2();
            history.setVisibility(View.VISIBLE);
            history.setText(Html.fromHtml(getString(R.string.history_apply)));
//            ViewGroup.LayoutParams params = recyclerView.getLayoutParams();
//            params.height = (int) getActivity().getResources().getDimension(R.dimen._310sdp);
//            recyclerView.setLayoutParams(params);
//            list = getResources().getStringArray(R.array.historyList);
            int id = 0;
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory() == null) {
//                assert AppConstant.historyLists != null;
//                VehicleHistoryDisclosure historyDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleHistoryDisclosure();
//                if (historyDisclosure != null && historyDisclosure.getValue() != null && historyDisclosure.getValue().size() > 0) {
//                    AppConstant.historyLists.clear();
//
//                    for (int i = 0; i < historyDisclosure.getValue().size(); i++) {
//                        HistoryListPojo historyListPojo = new HistoryListPojo();
//                        historyListPojo.setId(historyDisclosure.getValue().get(i).getId());
//                        historyListPojo.setName(historyDisclosure.getValue().get(i).getTitle());
//                        historyListPojo.setVerified(historyDisclosure.getValue().get(i).getIsVerified());
//                        AppConstant.historyLists.add(historyListPojo);
//                    }
////                    adapter.notifyDataSetChanged();
//                }
////                if (AppConstant.historyLists.size() == 0) {
////                    getHistoryDisclosure();
////                }
//
//                //                for (String aList : list) {
////                    HistoryListPojo pojo = new HistoryListPojo();
////                    pojo.setName(aList);
////                    id++;
////                    pojo.setId(id + "");
////                    pojo.setChecked(false);
////                    historyListPojos.add(pojo);
////
////                }
////                AppConstant.historyLists.addAll(historyListPojos);
//            } else {
//                assert AppConstant.historyLists != null;
//                VehicleHistoryDisclosure historyDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleHistoryDisclosure();
//                if (historyDisclosure != null && historyDisclosure.getValue() != null && historyDisclosure.getValue().size() > 0) {
//                    AppConstant.historyLists.clear();
//
//                    for (int i = 0; i < historyDisclosure.getValue().size(); i++) {
//                        HistoryListPojo historyListPojo = new HistoryListPojo();
//                        historyListPojo.setId(historyDisclosure.getValue().get(i).getId());
//                        historyListPojo.setName(historyDisclosure.getValue().get(i).getTitle());
//                        historyListPojo.setVerified(historyDisclosure.getValue().get(i).getIsVerified());
//                        AppConstant.historyLists.add(historyListPojo);
//                    }
////                    adapter.notifyDataSetChanged();
//                }
////                AppConstant.historyLists.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory());
//            }
            assert AppConstant.historyLists != null;
            VehicleHistoryDisclosure historyDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleHistoryDisclosure();
            if (historyDisclosure != null && historyDisclosure.getValue() != null && historyDisclosure.getValue().size() > 0) {
                AppConstant.historyLists.clear();

                for (int i = 0; i < historyDisclosure.getValue().size(); i++) {
                    HistoryListPojo historyListPojo = new HistoryListPojo();
                    historyListPojo.setId(historyDisclosure.getValue().get(i).getId());
                    historyListPojo.setName(historyDisclosure.getValue().get(i).getTitle());
                    historyListPojo.setVerified(historyDisclosure.getValue().get(i).getIsVerified());
                    AppConstant.historyLists.add(historyListPojo);
                }
                ArrayList<HistoryListPojo> localLi = new ArrayList<>();
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getUsedVehicleHistory() != null) {
                        localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleHistory());
                    }

                }
                for (int i = 0; i < localLi.size(); i++) {
                    for (int j = 0; j < AppConstant.historyLists.size(); j++) {
                        if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.historyLists.get(j).getId()) && localLi.get(i).isChecked()) {
                            AppConstant.historyLists.get(j).setChecked(true);
                            AppConstant.historyLists.get(j).setAdditionalData(localLi.get(i).getAdditionalData());
                        }
                    }
                }
//                    adapter.notifyDataSetChanged();
            } else {
                VehicleHistoryDisclosure vehicleHistoryDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.HISTORY_DISCLOSURE), VehicleHistoryDisclosure.class);
                if (vehicleHistoryDisclosure != null && vehicleHistoryDisclosure.getValue() != null && vehicleHistoryDisclosure.getValue().size() > 0) {
                    ArrayList<HistoryListPojo> localLi = new ArrayList<>();
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        if (record.getVehicleDetails().getUsedVehicleHistory() != null) {
                            localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleHistory());
                            AppConstant.historyLists.clear();
                            for (int i = 0; i < vehicleHistoryDisclosure.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(vehicleHistoryDisclosure.getValue().get(i).getId());
                                historyListPojo.setName(vehicleHistoryDisclosure.getValue().get(i).getTitle());
                                historyListPojo.setChecked(false);
                                historyListPojo.setVerified(vehicleHistoryDisclosure.getValue().get(i).getIsVerified());
                                AppConstant.historyLists.add(historyListPojo);
                            }
                            for (int i = 0; i < localLi.size(); i++) {
                                for (int j = 0; j < AppConstant.historyLists.size(); j++) {
                                    if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.historyLists.get(j).getId()) && localLi.get(i).isChecked()) {
                                        AppConstant.historyLists.get(j).setChecked(true);
                                        AppConstant.historyLists.get(j).setAdditionalData(localLi.get(i).getAdditionalData());
                                    }
                                }
                            }
                            Timber.e(AppConstant.historyLists.size() + "");
                            boolean atLeastOneVerified = false;
                            for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                                if (AppConstant.historyLists.get(i).isVerified()) {
                                    atLeastOneVerified = true;
                                    break;
                                }
                            }
                            if (atLeastOneVerified)
                                AppConstant.historyLists.get(AppConstant.historyLists.size() - 1).setVerified(true);
                            if (!isAutoCheck && adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            AppConstant.historyLists.clear();
                            for (int i = 0; i < vehicleHistoryDisclosure.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(vehicleHistoryDisclosure.getValue().get(i).getId());
                                historyListPojo.setChecked(false);
                                historyListPojo.setName(vehicleHistoryDisclosure.getValue().get(i).getTitle());
                                historyListPojo.setVerified(vehicleHistoryDisclosure.getValue().get(i).getIsVerified());
                                AppConstant.historyLists.add(historyListPojo);
                            }
                            boolean atLeastOneVerified = false;
                            for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                                if (AppConstant.historyLists.get(i).isVerified()) {
                                    atLeastOneVerified = true;
                                    break;
                                }
                            }
                            if (atLeastOneVerified)
                                AppConstant.historyLists.get(AppConstant.historyLists.size() - 1).setVerified(true);
                            if (!isAutoCheck && adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeHistoryDisclosures() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().size() > 0) {
                ArrayList<VehicleTradeHistoryDisclosures> localList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeHistoryDisclosures();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < AppConstant.historyLists.size(); j++) {
                        if (localList.get(i).getVehicleHistoryDisclosureId().equalsIgnoreCase(AppConstant.historyLists.get(j).getId())) {
//                            AppConstant.historyLists.get(j).setAdditionalData(localList.get(i).getValue());
                            AppConstant.historyLists.get(j).setChecked(true);
                        }
                    }
                }
            }
            if (AppConstant.historyLists != null && AppConstant.historyLists.size() > 0) {
                if (AppConstant.historyLists.get(AppConstant.historyLists.size() - 1).isChecked()) {
                    for (int i = 0; i < AppConstant.historyLists.size() - 1; i++) {
                        AppConstant.historyLists.get(i).setChecked(false);
                    }
                }
            }
            boolean atLeastOneVerified = false;
            for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                if (AppConstant.historyLists.get(i).isVerified()) {
                    atLeastOneVerified = true;
                    break;
                }
            }
            if (atLeastOneVerified)
                AppConstant.historyLists.get(AppConstant.historyLists.size() - 1).setVerified(true);
            adapter = new HistoryAdapter(getActivity(), this, AppConstant.historyLists, !isAutoCheck);
            recyclerView.setAdapter(adapter);
            ViewCompat.setNestedScrollingEnabled(recyclerView, false);
            for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                if (AppConstant.historyLists.get(i).isChecked() || AppConstant.historyLists.get(i).isVerified()) {
                    shouldNextShow = true;
                }
            }
            if (shouldNextShow) {
                next.setVisibility(View.VISIBLE);
            } else {
                next.setVisibility(View.INVISIBLE);
            }
        } else {
            history1.setText(Html.fromHtml(getString(R.string.usedVehicleReport)));
            AppConstant.reportLists.clear();
            thirdPartyContainer.setVisibility(View.GONE);
            history.setVisibility(View.VISIBLE);
            int id = 0;
            assert AppConstant.reportLists != null;
            ThirdPartyList partyList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList();
            if (partyList != null && partyList.getValue() != null && partyList.getValue().size() > 0) {
                AppConstant.reportLists.clear();
                for (int i = 0; i < partyList.getValue().size(); i++) {
                    HistoryListPojo historyListPojo = new HistoryListPojo();
                    historyListPojo.setId(partyList.getValue().get(i).getId());
                    historyListPojo.setName(partyList.getValue().get(i).getTitle());
                    historyListPojo.setVerified(partyList.getValue().get(i).isActive());
                    historyListPojo.setAutoMarked(partyList.getValue().get(i).isAutoMarked());
                    historyListPojo.setViewReport(partyList.getValue().get(i).isViewReport());
                    historyListPojo.setMandatory(partyList.getValue().get(i).isMandatory());
                    AppConstant.reportLists.add(historyListPojo);
                }
                ArrayList<HistoryListPojo> localLi = new ArrayList<>();
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getUsedVehicleReport() != null) {
                        localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleReport());
                    }

                }
                for (int i = 0; i < localLi.size(); i++) {
                    for (int j = 0; j < AppConstant.reportLists.size(); j++) {
                        if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.reportLists.get(j).getId()) && localLi.get(i).isChecked()) {
                            AppConstant.reportLists.get(j).setChecked(true);
                            AppConstant.reportLists.get(j).setAdditionalData(localLi.get(i).getAdditionalData());
                        }
                    }
                }
            } else {
                ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
                if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
                    ArrayList<HistoryListPojo> localLi;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        if (record.getVehicleDetails().getUsedVehicleReport() != null) {
                            localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleReport());
                            AppConstant.reportLists.clear();
                            for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                                historyListPojo.setChecked(false);
                                historyListPojo.setAutoMarked(thirdPartyList.getValue().get(i).isAutoMarked());
                                historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                                historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                                historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                                AppConstant.reportLists.add(historyListPojo);
                            }
                            for (int i = 0; i < localLi.size(); i++) {
                                for (int j = 0; j < AppConstant.reportLists.size(); j++) {
                                    if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.reportLists.get(j).getId()) && localLi.get(i).isChecked()) {
                                        AppConstant.reportLists.get(j).setChecked(true);
                                    }
                                }
                            }
                            Log.e("Size of reports: ", AppConstant.reportLists.size() + "");
                            removeAutoMarkIfReportNotAvailable();
                            boolean atLeastOneVerified = false;
                            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                                if (AppConstant.reportLists.get(i).isVerified() || AppConstant.reportLists.get(i).isAutoMarked()) {
                                    atLeastOneVerified = true;
                                    break;
                                }
                            }
                            if (atLeastOneVerified)
                                AppConstant.reportLists.get(AppConstant.reportLists.size() - 1).setVerified(true);
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                                historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                                historyListPojo.setChecked(false);
                                historyListPojo.setAutoMarked(thirdPartyList.getValue().get(i).isAutoMarked());
//                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isViewReport());
                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                                historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                                historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                                AppConstant.reportLists.add(historyListPojo);
                            }
                            boolean atLeastOneVerified = false;
                            removeAutoMarkIfReportNotAvailable();
                            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                                if (AppConstant.reportLists.get(i).isVerified() || AppConstant.reportLists.get(i).isAutoMarked()) {
                                    atLeastOneVerified = true;
                                    break;
                                }
                            }
                            if (atLeastOneVerified)
                                AppConstant.reportLists.get(AppConstant.reportLists.size() - 1).setVerified(true);
                            if (adapter != null) {

                                adapter.notifyDataSetChanged();
                            }

                        }
                    }
                }
            }
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports().size() > 0) {
                ArrayList<VehicleTradeThirdPartyHistoryReports> localList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < AppConstant.reportLists.size(); j++) {
                        if (localList.get(i).getThirdPartyHistoryReportId().equalsIgnoreCase(AppConstant.reportLists.get(j).getId())) {
//                            AppConstant.historyLists.get(j).setAdditionalData(localList.get(i).getValue());
                            AppConstant.reportLists.get(j).setChecked(true);
                        }
                    }
                }
            }
            if (AppConstant.reportLists != null && AppConstant.reportLists.size() > 0) {
                if (AppConstant.reportLists.get(AppConstant.reportLists.size() - 1).isChecked()) {
                    for (int i = 0; i < AppConstant.reportLists.size() - 1; i++) {
                        AppConstant.reportLists.get(i).setChecked(false);
                    }

                }
            }
            boolean atLeastOneVerified = false;

            removeAutoMarkIfReportNotAvailable();
            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                if (AppConstant.reportLists.get(i).isVerified() || AppConstant.reportLists.get(i).isAutoMarked()) {
                    atLeastOneVerified = true;
                    break;
                }
            }
            if (atLeastOneVerified)
                AppConstant.reportLists.get(AppConstant.reportLists.size() - 1).setVerified(true);

            adapter = new HistoryAdapter(getActivity(), this, AppConstant.reportLists, !isAutoCheck);
            recyclerView.setAdapter(adapter);
            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                if (AppConstant.reportLists.get(i).isChecked() || AppConstant.reportLists.get(i).isVerified()) {
                    shouldNextShow = true;
                }
            }
            if (shouldNextShow) {
                next.setVisibility(View.VISIBLE);
            } else {
                next.setVisibility(View.INVISIBLE);
            }
        }
        recyclerView.setLayoutManager(manager);


        DSTextView back = mView.findViewById(R.id.text_back_up);
//        final String[] finalList = list;
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean goNext = false;
                if (isAutoCheck) {
                    for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                        if (AppConstant.reportLists.get(i).isChecked()) {
                            goNext = true;
                        }
                    }
                } else {
                    for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                        if (AppConstant.historyLists.get(i).isChecked()) {
                            goNext = true;
                        }
                    }
                }
                boolean confirmGo = true;
//                if (AppConstant.historyLists.get(14).isChecked() && AppConstant.history1Time == null && !isAutoCheck) {
//                    new CustomToast(getActivity()).alert(getString(R.string.select_date));
//                    cnfirmGo = false;
//                } else if (AppConstant.historyLists.get(15).isChecked() && AppConstant.history2Time == null && !isAutoCheck) {
//                    new CustomToast(getActivity()).alert(getString(R.string.select_date));
//                    cnfirmGo = false;
//                } else

                if (!goNext) {
                    confirmGo = false;
                    new CustomToast(getActivity()).alert(getString(R.string.select_one_option));
                }
                if (goNext && confirmGo) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (getActivity() instanceof NewDealViewPager)
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleHistory(AppConstant.historyLists);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleReport(AppConstant.reportLists);
                    if (!isAutoCheck) {
                        ArrayList<HistoryListPojo> history = new ArrayList<>();

                        for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                            if (AppConstant.historyLists.get(i).isChecked()) {
                                HistoryListPojo pojo = new HistoryListPojo();
                                pojo.setChecked(!AppConstant.historyLists.get(i).isChecked());
                                pojo.setAdditionalData(AppConstant.historyLists.get(i).getAdditionalData());
                                pojo.setId(AppConstant.historyLists.get(i).getId());
                                pojo.setName(AppConstant.historyLists.get(i).getName());
                                pojo.setVerified(AppConstant.historyLists.get(i).isVerified());
                                history.add(pojo);
                            }
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistorySelection(history);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoHistorySelection(history);
                    } else {
                        ArrayList<HistoryListPojo> report = new ArrayList<>();
                        for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                            if (AppConstant.reportLists.get(i).isChecked()) {
                                HistoryListPojo pojo = new HistoryListPojo();
                                pojo.setChecked(!AppConstant.reportLists.get(i).isChecked());
                                pojo.setAdditionalData(AppConstant.reportLists.get(i).getAdditionalData());
                                pojo.setId(AppConstant.reportLists.get(i).getId());
                                pojo.setName(AppConstant.reportLists.get(i).getName());
                                pojo.setVerified(AppConstant.reportLists.get(i).isVerified());
                                pojo.setViewReport(AppConstant.reportLists.get(i).isViewReport());
                                pojo.setMandatory(AppConstant.reportLists.get(i).isMandatory());
                                report.add(pojo);
                            }
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setReportSelection(report);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoReportSelection(report);
                    }

                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    iGoToNextPage.whatNextClick();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.goToBackIndex();
            }
        });
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
        if (getActivity() instanceof ReviewEditDisclosureViewPager)
            cancel.setVisibility(View.GONE);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (getActivity() instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleHistory(AppConstant.historyLists);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleReport(AppConstant.reportLists);
                ArrayList<HistoryListPojo> history = new ArrayList<>();
                ArrayList<HistoryListPojo> report = new ArrayList<>();
                for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                    if (AppConstant.historyLists.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.historyLists.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.historyLists.get(i).getAdditionalData());
                        pojo.setId(AppConstant.historyLists.get(i).getId());
                        pojo.setName(AppConstant.historyLists.get(i).getName());
                        history.add(pojo);
                    }
                }
                for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                    if (AppConstant.reportLists.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.reportLists.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.reportLists.get(i).getAdditionalData());
                        pojo.setId(AppConstant.reportLists.get(i).getId());
                        pojo.setName(AppConstant.reportLists.get(i).getName());
                        report.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistorySelection(history);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setReportSelection(report);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel.setEnabled(false);
                cancel.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cancel.setEnabled(true);
                        cancel.setClickable(true);
                    }
                }, 6000);
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (getActivity() instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleHistory(AppConstant.historyLists);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleReport(AppConstant.reportLists);
                ArrayList<HistoryListPojo> history = new ArrayList<>();
                ArrayList<HistoryListPojo> report = new ArrayList<>();
                for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                    if (AppConstant.historyLists.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.historyLists.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.historyLists.get(i).getAdditionalData());
                        pojo.setId(AppConstant.historyLists.get(i).getId());
                        pojo.setName(AppConstant.historyLists.get(i).getName());
                        history.add(pojo);
                    }
                }
                for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                    if (AppConstant.reportLists.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.reportLists.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.reportLists.get(i).getAdditionalData());
                        pojo.setId(AppConstant.reportLists.get(i).getId());
                        pojo.setName(AppConstant.reportLists.get(i).getName());
                        report.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistorySelection(history);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setReportSelection(report);

                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                cancel();
            }
        });
    }

    private void saveRecord() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (getActivity() instanceof NewDealViewPager)
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleHistory(AppConstant.historyLists);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleReport(AppConstant.reportLists);
        if (!isAutoCheck) {
            ArrayList<HistoryListPojo> history = new ArrayList<>();

            for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                if (AppConstant.historyLists.get(i).isChecked()) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setChecked(!AppConstant.historyLists.get(i).isChecked());
                    pojo.setAdditionalData(AppConstant.historyLists.get(i).getAdditionalData());
                    pojo.setId(AppConstant.historyLists.get(i).getId());
                    pojo.setName(AppConstant.historyLists.get(i).getName());
                    pojo.setVerified(AppConstant.historyLists.get(i).isVerified());
                    history.add(pojo);
                }
            }
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistorySelection(history);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoHistorySelection(history);
        } else {
            ArrayList<HistoryListPojo> report = new ArrayList<>();
            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                if (AppConstant.reportLists.get(i).isChecked()) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setChecked(!AppConstant.reportLists.get(i).isChecked());
                    pojo.setAdditionalData(AppConstant.reportLists.get(i).getAdditionalData());
                    pojo.setId(AppConstant.reportLists.get(i).getId());
                    pojo.setName(AppConstant.reportLists.get(i).getName());
                    pojo.setVerified(AppConstant.reportLists.get(i).isVerified());
                    pojo.setViewReport(AppConstant.reportLists.get(i).isViewReport());
                    pojo.setMandatory(AppConstant.reportLists.get(i).isMandatory());
                    report.add(pojo);
                }
            }
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setReportSelection(report);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoReportSelection(report);
        }

        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
    }

    private void removeAutoMarkIfReportNotAvailable() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        //carfax not available and net connected
        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable() /*&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isWasInternetAvailable()*/) {
            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                if (AppConstant.reportLists.get(i).getId().equalsIgnoreCase("1") && AppConstant.reportLists.get(i).isVerified()) {
//                    AppConstant.reportLists.get(i).setChecked(false);
//                    if (getActivity() instanceof NewDealViewPager)
                    AppConstant.reportLists.get(i).setVerified(false);
                    AppConstant.reportLists.get(i).setAutoMarked(false);
//                    AppConstant.reportLists.get(i).setMandatory(false);
//                    AppConstant.reportLists.get(i).setViewReport(false);
                }
            }
        }
        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable() /*&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isWasInternetAvailable()*/) {
            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                if (AppConstant.reportLists.get(i).getId().equalsIgnoreCase("2") && AppConstant.reportLists.get(i).isVerified()) {
//                    AppConstant.reportLists.get(i).setChecked(false);
//                    if (getActivity() instanceof NewDealViewPager)
                    AppConstant.reportLists.get(i).setVerified(false);
                    AppConstant.reportLists.get(i).setAutoMarked(false);
//                    AppConstant.reportLists.get(i).setMandatory(false);
//                    AppConstant.reportLists.get(i).setViewReport(false);
                }
            }
        }
    }

    private String getIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase("CarFax Report")) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase("AutoCheck Report")) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase("Auction Announcement")) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase("Other Vehicle History Report")) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase("NMVTIS Report")) {
            id = "6";
        } else if (aList.trim().equalsIgnoreCase("None")) {
            id = "5";
        }
        return id;
    }

    private void checkIfReportAvailable(boolean carfax, String thirdPartyId) {
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new Gson();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String vin = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
        int index = 0;
        String name = null;
        ArrayList<ThirdPartyObj> listLangName = new ArrayList<>();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null)
            listLangName = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
        for (int i = 0; i < listLangName.size(); i++) {
            if (listLangName.get(i).getId().equalsIgnoreCase(thirdPartyId)) {
                name = listLangName.get(i).getTitle();
                index = i;
            }
        }
        if (name != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes() != null &&
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")) {
            Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
            intent.putExtra("url", name);
            intent.putExtra("id", thirdPartyId);
            intent.putExtra("isCoBuyer", false);
            intent.putExtra("showBox", false);
            intent.putExtra("isDisclosure", true);
            Objects.requireNonNull(getActivity()).startActivity(intent);
        } else {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                ArrayList<InitThirdParty> initList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
                boolean idFound = false;
                for (int i = 0; i < initList.size(); i++) {
                    if (initList.get(i).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
//                        showAlert(initList.get(i).getErrorsString(), thirdPartyId);
                        if (thirdPartyId.equalsIgnoreCase("1"))
                            showAlert(initList.get(i).getErrorsString(), thirdPartyId, "CARFAX Not Available. Please make manual disclosures.");
                        else if (thirdPartyId.equalsIgnoreCase("2")) {
                            showAlert(initList.get(i).getErrorsString(), thirdPartyId, "AutoCheck Not Available. Please make manual disclosures.");
                        }
                        idFound = true;
                        break;
                    }
                }
                if (!idFound) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getInitiateThirdParty() == null || record.getVehicleDetails().getInitiateThirdParty().getValue().size() == 0) {
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            callThirdPartyInitiateAPI(vin, thirdPartyId, carfax);
                        } else {
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }
                    } else {
                        if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                            ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                            boolean isActive = false;
                            String fileName = "";
                            for (int i = 0; i < locallist.size(); i++) {
                                if (locallist.get(i).getId().equalsIgnoreCase(thirdPartyId) && locallist.get(i).isActive()) {
                                    isActive = true;
                                    fileName = locallist.get(i).getTitle();
                                    break;
                                }
                            }
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(fileName.trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (isActive) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    if (DSAPP.getInstance().isNetworkAvailable()) {
                                                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                                        progressDialog.setMessage("Please wait... We are downloading the report.");
                                                        progressDialog.show();
                                                        progressDialog.setCancelable(false);
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), thirdPartyId, fileName.trim(), progressDialog);
                                                    } else {
                                                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                    }
                                                }
                                            } else {
                                                Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                                intent.putExtra("url", fileName);
                                                intent.putExtra("id", thirdPartyId);
                                                intent.putExtra("isCoBuyer", false);
                                                intent.putExtra("customerResponse", carfax);
                                                intent.putExtra("isDisclosure", true);
                                                Objects.requireNonNull(getActivity()).startActivity(intent);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
//                            }
                        }
                    }
                }
            } else {
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                if (record.getVehicleDetails().getInitiateThirdParty() == null || record.getVehicleDetails().getInitiateThirdParty().getValue().size() == 0) {
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        callThirdPartyInitiateAPI(vin, thirdPartyId, carfax);
                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }
                } else {
                    if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                        ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < locallist.size(); i++) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(locallist.get(i).getId()) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(locallist.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (locallist.get(i).isActive()) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    if (DSAPP.getInstance().isNetworkAvailable()) {
                                                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                                        progressDialog.setMessage("Please wait... We are downloading the report.");
                                                        progressDialog.show();
                                                        progressDialog.setCancelable(false);
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), locallist.get(i).getId(), locallist.get(i).getTitle().trim(), progressDialog);
                                                    } else {
                                                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                    }
                                                }
                                            } else {
                                                Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                                intent.putExtra("url", locallist.get(i).getTitle());
                                                intent.putExtra("id", locallist.get(i).getId());
                                                intent.putExtra("isCoBuyer", false);
                                                intent.putExtra("customerResponse", carfax);
                                                intent.putExtra("isDisclosure", true);
                                                Objects.requireNonNull(getActivity()).startActivity(intent);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void showAlert(String errorsString, final String tpID) {
        if (getActivity() != null) {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
            titleTV.setVisibility(View.GONE);
            messageTV.setText(errorsString);
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file;
                    if (tpID.equalsIgnoreCase("1")) {
                        file = getOutputFile("Screenshot", "alert_history_report_fetching_error_cf");
                    } else {
                        file = getOutputFile("Screenshot", "alert_history_report_fetching_error_ac");
                    }
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(tpID);
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(tpID);
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);


                    alertD.dismiss();
                }
            });


        }
    }

    private void showAlert(String errorsString, final String tpID, String title) {
        if (getActivity() != null) {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
            titleTV.setText(title);
            titleTV.setVisibility(View.VISIBLE);
            messageTV.setText(errorsString);
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file;
                    if (tpID.equalsIgnoreCase("1")) {
                        file = getOutputFile("Screenshot", "alert_history_report_fetching_error_cf");
                    } else {
                        file = getOutputFile("Screenshot", "alert_history_report_fetching_error_ac");
                    }
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(tpID);
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(tpID);
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);


                    alertD.dismiss();
                }
            });


        }
    }

    private void callThirdPartyInitiateAPI(final String vin, final String thirdPartyId, final boolean carfax) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait... We are downloading the report.");
        progressDialog.show();
        progressDialog.setCancelable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDialog!=null&&progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        },20000);
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().initiateThirdParty("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vin, id).enqueue(new Callback<InitiateThirdParty>() {
            @Override
            public void onResponse(Call<InitiateThirdParty> call, Response<InitiateThirdParty> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new Gson();
                    Log.e("Initiate TP: ", gson.toJson(response.body()));
                    if (response.body().getSucceeded().equalsIgnoreCase("true")) {
                        boolean download = false;
                        for (int i = 0; i < response.body().getValue().size(); i++) {
                            if (response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                download = true;
                                break;
                            } else {
                                if (thirdPartyId.equalsIgnoreCase("1"))
                                    showAlert(response.body().getValue().get(i).getErrorsString(), thirdPartyId, "CARFAX Not Available. Please make manual disclosures.");
                                else if (thirdPartyId.equalsIgnoreCase("2")) {
                                    showAlert(response.body().getValue().get(i).getErrorsString(), thirdPartyId, "AutoCheck Not Available. Please make manual disclosures.");
                                }
//                                showAlert(response.body().getValue().get(i).getErrorsString(), thirdPartyId);
                            }
                        }
                        if (download) {
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(response.body());
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            getThirdParty(thirdPartyId, progressDialog, carfax);
                        } else {
                            progressDialog.dismiss();
                        }

                    } else {
                        progressDialog.dismiss();
                        if (thirdPartyId.equalsIgnoreCase("1"))
                            showAlert(response.body().getErrorsString(), thirdPartyId, "CARFAX Not Available. Please make manual disclosures.");
                        else if (thirdPartyId.equalsIgnoreCase("2")) {
                            showAlert(response.body().getErrorsString(), thirdPartyId, "AutoCheck Not Available. Please make manual disclosures.");
                        }
//                        showAlert(response.body().getErrorsString(), thirdPartyId);
//                        new CustomToast(getActivity()).alert(response.body().getErrorsString());
                    }
                }
            }

            @Override
            public void onFailure(Call<InitiateThirdParty> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Initiate Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void getThirdParty(final String vin, final ProgressDialog progressDialog, final boolean carfax) {

//        new CustomToast(getActivity()).alert("Please wait we are downloading the report.");
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    ThirdPartyList vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new ThirdPartyList();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setViewReport(false);
                            list.get(i).setActive(false);
                            list.get(i).setMandatory(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Log.i("ThirdParty: ", gson.toJson(vehicleConditionDisclosure));
                    }
                    String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
                    for (int i = 0; i < list.size(); i++) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                            for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(list.get(i).getId())) {
                                    File file = ShowImageFromPath(list.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                    if (list.get(i).isActive()) {
                                        if (file == null || !file.exists()) {
                                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0"))
                                                getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim(), progressDialog);
                                        } else {
                                            Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                            intent.putExtra("url", list.get(i).getTitle());
                                            intent.putExtra("id", list.get(i).getId());
                                            intent.putExtra("isCoBuyer", false);
                                            intent.putExtra("customerResponse", carfax);
                                            intent.putExtra("isDisclosure", true);
                                            requireActivity().startActivity(intent);
                                            progressDialog.dismiss();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Log.e("Size of reports: ", response.body().getValue().size() + "");
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(getActivity())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    private void getAllThirdPartyReports(final String TPRRID, final String tpId, final String name, final ProgressDialog progressDialog) {

//        "40334", "2g1fd1e34f9207229", "1", lng
        RetrofitInitialization.getDs_services().getThirdPartyReportsBytes("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), TPRRID).enqueue(new Callback<ThirdPartyReportBytes>() {
            @Override
            public void onResponse(Call<ThirdPartyReportBytes> call, Response<ThirdPartyReportBytes> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage() != null && response.body().getMessage().length() > 0) {
                        File dwldsPath = getOutputMediaFilePDF("localPdf", name.trim() + TPRRID);
                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
                        FileOutputStream os;
                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
                        PreferenceManger.putString(AppConstant.ALERT, "");
                        try {
                            os = new FileOutputStream(dwldsPath, false);
                            os.write(pdfAsBytes);
                            os.flush();
                            os.close();
//                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {

//                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(tpId)) {
                                list.get(i).setThirdPartyReportBytes(response.body());
                            }
                        }
                        if (tpId.equalsIgnoreCase("1") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            alerts.setAlertName("CarFax Report is Not Available");
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getErrorMessage());
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                        } else if (tpId.equalsIgnoreCase("2") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getErrorMessage());
                            alerts.setAlertName("AutoCheck Report is Not Available");
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        next.setVisibility(View.VISIBLE);
//                        initView();
//                        adapter.notifyDataSetChanged();
                        new CustomToast(getActivity()).alert("Report Downloaded. Click again to view.");
                        progressDialog.dismiss();

//                        Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
//                        intent.putExtra("url", name);
//                        intent.putExtra("id", tpId);
//                        intent.putExtra("isCoBuyer", false);
//                        intent.putExtra("isDisclosure", true);
//                        Objects.requireNonNull(getActivity()).startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(Call<ThirdPartyReportBytes> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Failure: ", "While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    public File getOutputMediaFilePDF(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    @Override
    public void languageSelected(String selectedLanguage, int selectedLanguageId) {
        shouldNextShow = false;
        if (isAutoCheck) {
            for (int i = 0; i < AppConstant.reportLists.size(); i++) {
                if (AppConstant.reportLists.get(i).isChecked()) {
                    shouldNextShow = true;
                }
            }
        } else {
            for (int i = 0; i < AppConstant.historyLists.size(); i++) {
                if (AppConstant.historyLists.get(i).isChecked()) {
                    shouldNextShow = true;
                }
            }
        }
        if (shouldNextShow) {
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.INVISIBLE);
        }
    }

}
