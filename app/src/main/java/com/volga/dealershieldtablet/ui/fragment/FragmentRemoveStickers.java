package com.volga.dealershieldtablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.CustomerVehicleTradeDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeMap;

import static android.os.Looper.getMainLooper;


public class FragmentRemoveStickers extends BaseFragment {

    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    IGoToNextPage iGoToNextPage;
    private boolean stickerRemovalPermission = false;
    private Button yes, no;
    private boolean isCoBuyer;
    private boolean isRemote;
    private DSTextView time;
    boolean isTaken;
    private boolean isAlreadyPlayed = false;
    private DSTextView sign;
    private ImageView delete;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && isAnsSelected && isSigned) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            //    next.setVisibility(View.GONE);
            disableWithGray(next);
            isAlreadyPlayed = false;
            signaturePad.clear();
            time.setText(getCurrentTimeString());
            isSigned = false;
//            enableNext(next);
            updateData();
            initPhoto();
            //Code for displaying already selected Confirmation
        } else {
            isTaken = false;
        }
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
//                        isTaken = true;
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("UserPic", "remove_sticker_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_remove_sticker_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        isCoBuyer = vehicleDetails.isCoBuyer();
        isRemote = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }
        if (yes != null) {
            if (vehicleDetails.getStickerRemovalString() != null && vehicleDetails.getStickerRemovalString().equalsIgnoreCase("1")) {
                isAnsSelected = true;
                stickerRemovalPermission = true;
                yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                yes.setTextColor(getActivity().getResources().getColor(R.color.white));
                no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
            } else if (vehicleDetails.getStickerRemovalString() != null && vehicleDetails.getStickerRemovalString().equalsIgnoreCase("2")) {
                isAnsSelected = true;
                stickerRemovalPermission = false;
                yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                no.setTextColor(getActivity().getResources().getColor(R.color.white));
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.confirm_remove_sticker, container, false);
        isSigned = false;
        isAnsSelected = false;
        initView();
        return mView;
    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isAnsSelected && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isAnsSelected && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);
            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
            }
        }
    }

    private void initView() {
        if (NewDealViewPager.currentPage == 73) {
            initPhoto();
        }
        next = mView.findViewById(R.id.text_next);
        time = mView.findViewById(R.id.time);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        TextView back = mView.findViewById(R.id.text_back_up);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView mayRemove = mView.findViewById(R.id.mayRemove);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        boolean isPhotoAvailable = isPhotoAvailable();
//        boolean isPhotoAvailable = record.getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
        if (!isPhotoAvailable) {
            text_cancelBack.setText(String.format(getString(R.string.page1), "5", decidePageNumber()));
        }else {
            text_cancelBack.setText(String.format(getString(R.string.page1), "6", decidePageNumber()));
        }
//        text_cancelBack.setText(String.format(getString(R.string.page1), "6", decidePageNumber()));
        final DSTextView signature = mView.findViewById(R.id.signature);
        ImageView imageView = mView.findViewById(R.id.dlImage);
        imageView.setVisibility(View.INVISIBLE);
        if (ShowImageFromPath("IMG_sticker.jpg", "CarImages") != null) {
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.INVISIBLE);
        }

        loadImageToImageView(ShowImageFromPath("IMG_sticker.jpg", "CarImages"), imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = ShowImageFromPath("IMG_sticker.jpg", "CarImages");
                if (uri != null)
                    showDialog(ShowImageFromPath("IMG_sticker.jpg", "CarImages").toString());
            }
        });
        yes = mView.findViewById(R.id.removeSticker);
        no = mView.findViewById(R.id.dontRemoveSticker);
//        Gson gson = new GsonBuilder().create();
//        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null) {
            final ArrayList<CustomerVehicleTradeDocs> docsArrayList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs();

            for (int i = 0; i < docsArrayList.size(); i++) {
                if (docsArrayList.get(i).getDocTypeName().equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                    loadImageToImageView(docsArrayList.get(i).getDocUrl(), imageView);
                    if (docsArrayList.get(i).getDocUrl() != null)
                        imageView.setVisibility(View.VISIBLE);
                    else
                        imageView.setVisibility(View.INVISIBLE);
                    final int finalI = i;
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String name = "sticker";
                            showDialog(docsArrayList.get(finalI).getDocUrl(), name);
                        }
                    });

                }
            }
        }
        sign = mView.findViewById(R.id.signhere);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
        }
        SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
            mayRemove.setText(getString(R.string.may_remove_new));
            yes.setText(getString(R.string.remove_stickers));
            no.setText(getString(R.string.dont_remove_stickers));
        } else {
//            mayRemove.setText(getString(R.string.may_remove_old));
//            yes.setText(getString(R.string.old_remove_stickers));
//            no.setText(getString(R.string.old_dont_remove_stickers));
            mayRemove.setText(getString(R.string.newbuyer_guide));
            yes.setText(getString(R.string.yes));
            no.setText(getString(R.string.no));
        }

        updateData();
        if (!isCoBuyer | isRemote) {
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    yes.setTextColor(getResources().getColor(R.color.white));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    if (isSigned) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    }
                    isAnsSelected = true;
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setStickerRemovalString("1");
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    stickerRemovalPermission = true;
                }
            });
        }
        if (!isCoBuyer || isRemote) {
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*show confirmtion pop up*/
                    //     next.setVisibility(View.GONE);
                    no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    no.setTextColor(getResources().getColor(R.color.white));
                    yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    yes.setTextColor(getResources().getColor(R.color.button_color_blue));
                    disableWithGray(next);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {

                        new AlertDialog.Builder(getActivity())
                                .setMessage(R.string.are_you_sure)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.ractangle_border_blue));
                                        no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                                        no.setTextColor(getActivity().getResources().getColor(R.color.white));
                                        yes.setTextColor(getActivity().getResources().getColor(R.color.button_color_blue));
                                        Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                        intent.putExtra(AppConstant.FROM_FRAGMENT, "handover");
                                        getActivity().startActivity(intent);
                                        isAnsSelected = false;
                                    }
                                })
                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        dialog.dismiss();
                                        isAnsSelected = false;
                                    }
                                }).show().setCancelable(false);
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setMessage(R.string.are_you_sure)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.ractangle_border_blue));
                                        no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                                        no.setTextColor(getActivity().getResources().getColor(R.color.white));
                                        yes.setTextColor(getActivity().getResources().getColor(R.color.button_color_blue));
                                        isAnsSelected = true;
                                        if (isSigned) {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        }
                                        Gson gson = new GsonBuilder().create();
                                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setStickerRemovalString("2");
                                        String recordDataString = gson.toJson(recordData);
                                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                                        stickerRemovalPermission = false;
                                    }
                                })
                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        if (isSigned && isAnsSelected) {
                                            next.setVisibility(View.VISIBLE);
                                            enableWithGreen(next);
                                        }
                                        dialog.dismiss();
                                    }
                                }).show().setCancelable(false);

                    }
                }
            });

        }
        //   next.setVisibility(View.GONE);
        disableWithGray(next);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                disableNext(next);
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                int languageId = 1;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                    languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
                else {
                    languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
                }
                isAlreadyPlayed = false;
                int page = 0;
                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                        page = 75;
                    }
                    if (condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                        page = 76;
                    } else {
                        if (history == null) {
                            page = 76;
                        }
                    }
                    if (condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked() && report != null && report.get(report.size() - 1).isChecked()) {
                        page = 77;
                    } else {
                        if (history == null && report == null) {
                            page = 77;
                        }
                    }
                } else {
                    if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                        page = 77;
                    }
                }
                if (page != 0) {
                    playSound(languageId);
                    ((NewDealViewPager) getActivity()).setCurrentPage(page);
                } else
                    iGoToNextPage.whatNextClick();

                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "remove_sticker_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_remove_sticker_screen");
                }
                scrollableScreenshot(mainContent, file);
            }
        });

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (!isCoBuyer)
                        svgFile = new File(path_signature + "/IMG_remove_stickers_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg");
                    }
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "remove_sticker_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_remove_sticker_screen");
                    }
                    File file1;
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "remove_sticker_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_remove_sticker_screen_pic");
                    }
                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (!isCoBuyer)
                    svgFile = new File(path_signature + "/IMG_remove_stickers_selection.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg");
                }
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "remove_sticker_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_remove_sticker_screen");
                }
                File file1;
                if (!isCoBuyer) {
                    file1 = getOutputFile("UserPic", "remove_sticker_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "co_remove_sticker_screen_pic");
                }
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                iGoToNextPage.goToBackIndex();
            }
        });
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                isSigned = true;
                if (isAnsSelected) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    } else if (isTaken) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }

            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
                //  next.setVisibility(View.GONE);
            }
        });
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {
                        isSigned = false;
                        signaturePad.clear();
                        disableWithGray(next);
                        //    next.setVisibility(View.GONE);
                        return true;
                    }
                }
                return true;
            }
        });
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    void showDialog(String uri) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", uri);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    void showDialog(String uri, String name) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    public Uri ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (imgFile.exists()) {
            return Uri.fromFile(imgFile);
//            return rotateImage(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 90);
        }
        return null;
    }


    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile;
            if (isCoBuyer) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_remove_stickers_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "remove_stickers_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setStickerRemovalPermission(stickerRemovalPermission);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
    }
}
