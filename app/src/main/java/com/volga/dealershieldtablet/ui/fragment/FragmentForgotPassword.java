package com.volga.dealershieldtablet.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.ForgotPassword;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 22-08-2018.
 */
public class FragmentForgotPassword extends BaseFragment {
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    IGoToNextPage iGoToNextPage;
    private boolean isTradeIn = false;
    private Button yes, no;
    private DSTextView sendPassword;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
//        try {
//            iGoToNextPage = (IGoToNextPage) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
//        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            updateData();
            showFragmentInPortrait();
            //Code for displaying already selected Confirmation
        }
    }

    private void updateData() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.forgot_password_layout, container, false);
        initView();
        return mView;
    }

    private void initView() {
        TextView back = mView.findViewById(R.id.text_back);
        sendPassword = mView.findViewById(R.id.sendPassword);
        RelativeLayout relativeLayout = mView.findViewById(R.id.relativeLayout);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
            }
        });
        final DSEdittext email = mView.findViewById(R.id.et_email);
        final String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        sendPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DSAPP.getInstance().isNetworkAvailable()) {
                    final String emailS = email.getText().toString().trim();
                    if (emailS.equals("")) {
                        new CustomToast(getActivity()).alert(getString(R.string.empty_email));
                    } else if (!emailS.trim().matches(emailPattern)) {
                        new CustomToast(getActivity()).alert(getString(R.string.valid_email));
                    } else
                        callForgotPassAPI(email.getText().toString().trim());
                } else {
                    sendPassword.setClickable(true);
                    sendPassword.setEnabled(true);
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

    }

    private void callForgotPassAPI(String email) {
        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
        sendPassword.setClickable(false);
        sendPassword.setEnabled(false);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.sendingPassword));
        progressDialog.show();
        RetrofitInitialization.getDs_services().forgetPassword(email).enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                sendPassword.setClickable(true);
                sendPassword.setEnabled(true);
                if (response.code() == 200) {
                    if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Log.e("Forgot password: ", response.body().toString());
                    new CustomToast(getActivity()).alert(response.body().getMessage(), false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    }, 3000);
                } else {
                    if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        new CustomToast(getActivity()).alert(json.getString("Message"));
                        Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());
                        if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                sendPassword.setClickable(true);
                sendPassword.setEnabled(true);
                if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
            }
        });
    }

}
