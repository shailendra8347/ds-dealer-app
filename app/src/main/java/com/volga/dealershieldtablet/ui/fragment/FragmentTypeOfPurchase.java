package com.volga.dealershieldtablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.TreeMap;

import static android.os.Looper.getMainLooper;


public class FragmentTypeOfPurchase extends BaseFragment {

    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;              // made true from false
    private boolean isSigned = false, isTypeSelected = true;

    IGoToNextPage iGoToNextPage;
    private AppCompatButton purchase, lease;
    private boolean isCoBuyer, isRemote;
    private CheckBox checkBox, checkBox1, checkBox2;
    //Added while changing chekbox to bullet point
    private boolean check1=true, check2=true, check3=true;
    private boolean runningSign = false;
    private DSTextView time;
    private DSTextView sign;
    private ImageView delete;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isSigned = false;
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && isTypeSelected && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked() && isSigned) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.type_of_purchase, container, false);
        String locale = PreferenceManger.getStringValue(AppConstant.CURRENT_LANG).length() == 0 ? "en" : PreferenceManger.getStringValue(AppConstant.CURRENT_LANG);
        Lingver.getInstance().setLocale(getActivity(), locale);
        Gson gson = new GsonBuilder().create();

        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String lang = "1";
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getSettingsAndPermissions().isCoBuyer() && record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() > 0) {
                lang = record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "";
            } else {
                lang = record.getSettingsAndPermissions().getSelectedLanguageId() + "";
            }
        }
        if (!lang.equalsIgnoreCase("1")) {
            if (lang.equalsIgnoreCase("0"))
                lang = "1";
//            getConditionDisclosure(lang);
//            getHistoryDisclosure(lang);
//            getThirdParty(lang);
        }
        isSigned = false;
        // made true from false
        isTypeSelected = true;
        initView();
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            signaturePad.clear();
            isSigned = false;
            time.setText(getCurrentTimeString());
            enableNext(next);

            disableWithGray(next);
            updateData();
        } else {
            runningSign = false;
        }
    }


    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        isCoBuyer = recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();
        isRemote = recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote();
        assert recordData != null;
        if (lease != null && !(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfPurchase() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfPurchase().length() == 0)) {
            visibleHintCalled = true;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfPurchase() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfPurchase().equalsIgnoreCase("1")) {
                isTypeSelected = true;
                lease.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                lease.setTextColor(getActivity().getColor(R.color.button_color_blue));
                purchase.setTextColor(getActivity().getColor(R.color.white));
                purchase.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
            } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfPurchase() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfPurchase().equalsIgnoreCase("2")) {
                lease.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                purchase.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                purchase.setTextColor(getActivity().getColor(R.color.button_color_blue));
                lease.setTextColor(getActivity().getColor(R.color.white));
                isTypeSelected = true;
            }
        }
    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isTypeSelected && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked() && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isTypeSelected && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked() && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    private void initView() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        next = mView.findViewById(R.id.text_next);
        time = mView.findViewById(R.id.time);
        if (NewDealViewPager.currentPage == 69) {
            showProgressFor2Sec();
        }
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        time.setText(getCurrentTimeString());
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setText(String.format(getString(R.string.page1), "2", decidePageNumber()));
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        purchase = mView.findViewById(R.id.purchase);
        lease = mView.findViewById(R.id.lease);
        DSTextView name = mView.findViewById(R.id.name);
        DSTextView name1 = mView.findViewById(R.id.name1);
        DSTextView name2 = mView.findViewById(R.id.name2);

        //Added while changing chekbox to bullet point

//        name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkBox.setChecked(!checkBox.isChecked());
//            }
//        });
//        name1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkBox1.setChecked(!checkBox1.isChecked());
//            }
//        });
//        name2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkBox2.setChecked(!checkBox2.isChecked());
//            }
//        });

        checkBox = mView.findViewById(R.id.checkBox);
        checkBox1 = mView.findViewById(R.id.checkBox1);
        checkBox2 = mView.findViewById(R.id.checkBox2);

        //Added while changing chekbox to bullet point

        checkBox.setChecked(true);
        checkBox1.setChecked(true);
        checkBox2.setChecked(true);
        sign = mView.findViewById(R.id.signhere);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }
        //Added while changing chekbox to bullet point

//        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                check1 = isChecked;
//                if (check1) {
//                    name.setBackground(getResources().getDrawable(R.drawable.ractangle_border_typeof));
//                } else {
//                    name.setBackground(getResources().getDrawable(R.drawable.rounded_corner_white));
//                }
//                name.setPadding(10, 10, 10, 10);
//                if (check3 && check2 && isTypeSelected && isSigned && isChecked) {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            next.setVisibility(View.VISIBLE);
//                            enableWithGreen(next);
//                        }
//                    }, 100);
//                } else {
//                    disableWithGray(next);
//                }
//            }
//        });
//        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                check2 = isChecked;
//                if (check2) {
//                    name1.setBackground(getResources().getDrawable(R.drawable.ractangle_border_typeof));
//                } else {
//                    name1.setBackground(getResources().getDrawable(R.drawable.rounded_corner_white));
//                }
//                name1.setPadding(10, 10, 10, 10);
//                if (check1 && check3 && isTypeSelected && isSigned && isChecked) {
//
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            next.setVisibility(View.VISIBLE);
//                            enableWithGreen(next);
//                        }
//                    }, 100);
//                } else {
//
//                    disableWithGray(next);
//                }
//            }
//        });
//        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                check3 = isChecked;
//
//                if (check3) {
//                    name2.setBackground(getResources().getDrawable(R.drawable.ractangle_border_typeof));
//                } else {
//                    name2.setBackground(getResources().getDrawable(R.drawable.rounded_corner_white));
//                }
//                name2.setPadding(10, 10, 10, 10);
//                if (check1 && check2 && isTypeSelected && isSigned && isChecked) {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            next.setVisibility(View.VISIBLE);
//
//                            enableWithGreen(next);
//                        }
//                    }, 100);
//                } else {
//
//                    disableWithGray(next);
//                }
//            }
//        });
        if (!visibleHintCalled) {
            updateData();
        }
        if (!isCoBuyer || isRemote) {
            purchase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lease.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    lease.setTextColor(getActivity().getColor(R.color.button_color_blue));
                    purchase.setTextColor(getActivity().getColor(R.color.white));
                    purchase.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    if (isSigned && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    }
                    isTypeSelected = true;
//                    typeOfPurchse = getString(R.string.purchase);
                    RecordData recordData;
                    Gson gson = new GsonBuilder().create();
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase("1");

                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                }
            });
        }
        if (!isCoBuyer || isRemote) {
            lease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lease.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    purchase.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    purchase.setTextColor(getActivity().getColor(R.color.button_color_blue));
                    lease.setTextColor(getActivity().getColor(R.color.white));
                    if (isSigned && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    }
                    isTypeSelected = true;
                    RecordData recordData;
                    Gson gson = new GsonBuilder().create();
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase("2");

                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                }
            });
        }

        disableWithGray(next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check1 && check2 && check3 && isTypeSelected) {
                    disableNext(next);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            enableNext(next);
                        }
                    }, 5000);
                    addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                    SetRecordData();

                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "purchase_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_purchase_screen");
                    }
                    scrollableScreenshot(mainContent, file);
                    iGoToNextPage.whatNextClick();
                } else {
                    new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                }
//                next(new FragmentConfirmCarImages());
            }
        });

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

                    if (!isCoBuyer) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase(null);
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    File svgFile;
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "purchase_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_purchase_screen");
                    }
                    if (!isCoBuyer)
                        svgFile = new File(path_signature + "/IMG_type_of_purchase_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg");
                    }
                    File file1;
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "purchase_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_purchase_screen_pic");
                    }

                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

                if (!isCoBuyer) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase(null);
                }
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                File svgFile;
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "purchase_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_purchase_screen");
                }
                if (!isCoBuyer)
                    svgFile = new File(path_signature + "/IMG_type_of_purchase_selection.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg");
                }
                File file1;
                if (!isCoBuyer) {
                    file1 = getOutputFile("UserPic", "purchase_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "co_purchase_screen_pic");
                }

                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                setLocale("en", 68);
//                goBack();
            }
        });

        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;

                disableWithGray(next);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
                Handler mainHandler = new Handler(getActivity().getMainLooper());

                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (!runningSign) {
                            runningSign = true;
                            File file;
                            if (!isCoBuyer) {
                                file = getOutputFile("UserPic", "purchase_screen_pic");
                            } else {
                                file = getOutputFile("UserPic", "co_purchase_screen_pic");
                            }

                            if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO))
                                takeUserPicture(file);
                            else {
                                isTaken = true;
                                if (isTypeSelected && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked()) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }
                        }
                    } // This is your code
                };
                mainHandler.post(myRunnable);
            }

            @Override
            public void onSigned() {
                if (isTypeSelected && checkBox.isChecked() && checkBox1.isChecked() && checkBox2.isChecked()) {
                    isSigned = true;
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    } else if (isTaken) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }

                } else {
                    isSigned = true;
                }
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;

                        disableWithGray(next);
                        return true;
                    }
                }
                return true;
            }
        });
//        gson = new GsonBuilder().create();
//        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
    }

    public void setLocale(String localeName, int page) {
//        Locale myLocale = new Locale(localeName);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
        Lingver.getInstance().setLocale(getActivity(), localeName);
        Log.e("OnLanguageSelect Next: ", " Current locale: " + localeName);
        Intent refresh = new Intent(getActivity(), NewDealViewPager.class);
        refresh.putExtra("page", page);
        PreferenceManger.putString(AppConstant.CURRENT_LANG, localeName);
        refresh.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
        startActivity(refresh);
        getActivity().finish();
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile;
            if (!isCoBuyer) {
                svgFile = getOutputMediaFile("Signatures", "type_of_purchase_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_type_of_purchase_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        RecordData recordData;
        Gson gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase(typeOfPurchse);

        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
    }
}
