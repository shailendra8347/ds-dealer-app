package com.volga.dealershieldtablet.ui.scanner;
//Comment to commit

import android.content.Intent;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.manateeworks.BarcodeScanner;
import com.manateeworks.MWParser;
import com.pushlink.android.PushLink;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.ImageFragmentDialog;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import timber.log.Timber;

public class PartialViewContainerActivity extends AppCompatActivity implements com.volga.dealershieldtablet.ui.scanner.MWBScannerListener {
    public static final int USE_RESULT_TYPE = BarcodeScanner.MWB_RESULT_TYPE_MW;
    public static final Rect RECT_LANDSCAPE_1D = new Rect(3, 20, 94, 60);
    public static final Rect RECT_LANDSCAPE_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_PORTRAIT_1D = new Rect(20, 3, 60, 94);
    public static final Rect RECT_PORTRAIT_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_FULL_1D = new Rect(3, 3, 94, 94);
    public static final Rect RECT_FULL_2D = new Rect(20, 5, 60, 90);
    public static final Rect RECT_DOTCODE = new Rect(30, 20, 40, 60);

    public static boolean scanningDone = false;
    public static boolean logout = false;
    public static boolean back = false;
    public static boolean tradeInBack = false;
    public static boolean cancel = false;
    public static boolean skipped = false;

    public static final int MWPARSER_MASK = MWParser.MWP_PARSER_MASK_AAMVA;

    com.volga.dealershieldtablet.ui.scanner.ScannerFragment scannerFragment;
    ToneGenerator beep_sound;

    public boolean isVinScanner = false;
    private boolean isHistoryCheck = false;
    public boolean isDMVScanner = false;

    //    public void callFragment(int container, Fragment fragment, String tag) {
//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.replace(container, fragment, tag).commit();
//    }
    @Override
    protected void onResume() {
        super.onResume();
        PushLink.setCurrentActivity(this);
    }

    public void clearDMVPreference() {
        PreferenceManger.putString("Customer_First_Name", "");
        PreferenceManger.putString("Customer_Middle_Name(s)", "");
        PreferenceManger.putString("Customer_Family_Name", "");
        PreferenceManger.putString("Date_of_Birth", "");
        PreferenceManger.putString("Address1", "");
        PreferenceManger.putString("City", "");
        PreferenceManger.putString("Postal_Code", "");
        PreferenceManger.putString("State", "");
        PreferenceManger.putString("Country", "");
        PreferenceManger.putString("ID_Number", "");
        PreferenceManger.putString("Document_Expiration_Date", "");
        PreferenceManger.putString("Gender", "");
    }

    public boolean isDMVScanner() {
        return isDMVScanner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_partial_view_container);
        LinearLayout sampleContainer = findViewById(R.id.sampleContainer);
        clearDMVPreference();

        Bundle b = getIntent().getExtras();
        if (b != null) {
            isDMVScanner = b.getBoolean("isDMVScanner", false);
            isVinScanner = b.getBoolean("isVinScanner", false);
            isHistoryCheck = b.getBoolean("historyCheck", false);
        }
        if (!isDMVScanner) {
            sampleContainer.setVisibility(View.GONE);
        }
        sampleContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogCar("dmv_back_lipng");
            }
        });
        scanningDone = false;
        logout = false;
        back = false;
        cancel = false;
        skipped = false;

        DSTextView logout = findViewById(R.id.txt_logout);
        DSTextView back = findViewById(R.id.text_back);
        DSTextView next = findViewById(R.id.text_next);
        DSTextView cancel = findViewById(R.id.text_cancelBack);
        ImageView sampleImage = findViewById(R.id.sampleImage);

        DSTextView desc = findViewById(R.id.desc);
        Button skipScanning = findViewById(R.id.skipScanning);
        skipScanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skipped = true;
                if (isDMVScanner) {
                    PreferenceManger.putBoolean(AppConstant.SKIPPED, true);
                }
//                if (!isVinScanner && !PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER))
//                    PreferenceManger.putString(AppConstant.DMV_NUMBER, "");
                finish();
            }
        });
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        if (isDMVScanner) {
            Glide.with(this)
                    .load(R.drawable.back_of_dmv)
                    .apply(requestOptions)
                    .into(sampleImage);
        } else {
            Glide.with(this)
                    .load(R.drawable.vin_sticker)
                    .apply(requestOptions)
                    .into(sampleImage);
        }
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        next.setVisibility(View.GONE);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        scannerFragment = (com.volga.dealershieldtablet.ui.scanner.ScannerFragment) getSupportFragmentManager().findFragmentById(R.id.scannerFragment);
        scannerFragment.setData(isDMVScanner);
        int registerResult = BarcodeScanner.MWBregisterSDK(AppConstant.MANATEEWORKS_KEY, this);

        switch (registerResult) {
            case BarcodeScanner.MWB_RTREG_OK:
                Log.i("MWBregisterSDK", "Registration OK");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_KEY:
                Log.e("MWBregisterSDK", "Registration Invalid Key");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_CHECKSUM:
                Log.e("MWBregisterSDK", "Registration Invalid Checksum");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_APPLICATION:
                Log.e("MWBregisterSDK", "Registration Invalid VehicleDataUploadResponse");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_SDK_VERSION:
                Log.e("MWBregisterSDK", "Registration Invalid SDK Version");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_KEY_VERSION:
                Log.e("MWBregisterSDK", "Registration Invalid Key Version");
                break;
            case BarcodeScanner.MWB_RTREG_INVALID_PLATFORM:
                Log.e("MWBregisterSDK", "Registration Invalid Platform");
                break;
            case BarcodeScanner.MWB_RTREG_KEY_EXPIRED:
                Log.e("MWBregisterSDK", "Registration Key Expired");
                break;

            default:
                Log.e("MWBregisterSDK", "Registration Unknown Error");
                break;
        }
        int ver = BarcodeScanner.MWBgetLibVersion();
        int v1 = (ver >> 16);
        int v2 = (ver >> 8) & 0xff;
        int v3 = (ver & 0xff);
        String libVersion = "Lib version: " + String.valueOf(v1) + "."
                + String.valueOf(v2) + "." + String.valueOf(v3);
        Timber.e(libVersion);

        Timber.e(BarcodeScanner.MWBgetLibVersionText());
//        Timber.e(String.valueOf(MWParser.MWBgetLibVersion()));


//        Toast.makeText(this, libVersion, Toast.LENGTH_SHORT).show();

        //Scan Only PDF417 or only Code 39 according to the requirement

        BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_VERTICAL | BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL);

        if (isDMVScanner) {
            cancel.setVisibility(View.VISIBLE);
            BarcodeScanner.MWBsetActiveCodes(BarcodeScanner.MWB_CODE_MASK_PDF);
            BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_PDF, 1);
//            BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_DM, 1);
//            BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_128, 1);
            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                desc.setText(Html.fromHtml(getString(R.string.dmv_back_scan)));
            } else {
                desc.setText(Html.fromHtml(getString(R.string.co_dmv_back_scan)));
            }
            if (getIntent().getBooleanExtra("checkId", false)) {
                skipScanning.setVisibility(View.GONE);
                desc.setText(Html.fromHtml(getString(R.string.dmv_back_scan)));
            }
        } else if (isVinScanner) {
            cancel.setVisibility(View.VISIBLE);
//            BarcodeScanner.MWBsetActiveCodes(BarcodeScanner.MWB_CODE_MASK_39);
            BarcodeScanner.MWBsetActiveCodes(
                    BarcodeScanner.MWB_CODE_MASK_25 | BarcodeScanner.MWB_CODE_MASK_39 | BarcodeScanner.MWB_CODE_MASK_93
                            | BarcodeScanner.MWB_CODE_MASK_128 | BarcodeScanner.MWB_CODE_MASK_AZTEC
                            | BarcodeScanner.MWB_CODE_MASK_DM | BarcodeScanner.MWB_CODE_MASK_EANUPC
                            | BarcodeScanner.MWB_CODE_MASK_QR
                            | BarcodeScanner.MWB_CODE_MASK_CODABAR | BarcodeScanner.MWB_CODE_MASK_11
                            | BarcodeScanner.MWB_CODE_MASK_MAXICODE | BarcodeScanner.MWB_CODE_MASK_POSTAL
                            | BarcodeScanner.MWB_CODE_MASK_MSI | BarcodeScanner.MWB_CODE_MASK_RSS);
            BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_39, 1);
            BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_DM, 1);
            BarcodeScanner.MWBsetFlags(BarcodeScanner.MWB_CODE_MASK_128, 1);
            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
//                desc.setText(Html.fromHtml(getString(R.string.bar_scn_text)));
                desc.setText(Html.fromHtml(getString(R.string.co_bar_scn_text)));
            else {
                desc.setText(Html.fromHtml(getString(R.string.bar_scn_text)));

            }
        }
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF, RECT_LANDSCAPE_1D);
//        // choose code type or types you want to search for
//        // Our sample app is configured by default to search both directions...
//        BarcodeScanner.MWBsetDirection(
//                BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL | BarcodeScanner.MWB_SCANDIRECTION_VERTICAL);
//
//        // Our sample app is configured by default to search all supported barcodes...
//        // But for better performance, only activate the symbologies your application requires...
//        BarcodeScanner.MWBsetActiveCodes(
//                BarcodeScanner.MWB_CODE_MASK_25 | BarcodeScanner.MWB_CODE_MASK_39 | BarcodeScanner.MWB_CODE_MASK_93
//                        | BarcodeScanner.MWB_CODE_MASK_128 | BarcodeScanner.MWB_CODE_MASK_AZTEC
//                        | BarcodeScanner.MWB_CODE_MASK_DM | BarcodeScanner.MWB_CODE_MASK_EANUPC
//                        | BarcodeScanner.MWB_CODE_MASK_PDF | BarcodeScanner.MWB_CODE_MASK_QR
//                        | BarcodeScanner.MWB_CODE_MASK_CODABAR | BarcodeScanner.MWB_CODE_MASK_11
//                        | BarcodeScanner.MWB_CODE_MASK_MAXICODE | BarcodeScanner.MWB_CODE_MASK_POSTAL
//                        | BarcodeScanner.MWB_CODE_MASK_MSI | BarcodeScanner.MWB_CODE_MASK_RSS);
//
//        // set the scanning rectangle based on scan direction(format in pct:
//        // set the scanning rectangle based on scan direction(format in pct:
//        // x, y, width, height)
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_93, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC, RECT_FULL_2D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM, RECT_FULL_2D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR, RECT_FULL_2D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_CODABAR, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DOTCODE, RECT_DOTCODE);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_11, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MSI, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MAXICODE, RECT_FULL_1D);
//        BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_POSTAL, RECT_FULL_1D);


//        if (OVERLAY_MODE == ActivityCapture.OverlayMode.OM_IMAGE) {
//            imageOverlay.setVisibility(View.VISIBLE);
//        }

        /* Analytics */
        /*
         * Register with given apiUser and apiKey
         */
        //
        // if (USE_MWANALYTICS) {
        // MWBAnalytics.init(getApplicationContext(), "apiUser", "apiKey");
        // }

        // For better performance, set like this for PORTRAIT scanning...
        // BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_VERTICAL);
        // set the scanning rectangle based on scan direction(format in pct: x,
        // y, width, height)
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_93,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,
        // RECT_PORTRAIT_2D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,
        // RECT_PORTRAIT_2D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,
        // RECT_PORTRAIT_2D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_CODABAR,RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DOTCODE,RECT_DOTCODE);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_11,
        // RECT_PORTRAIT_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MSI,
        // RECT_PORTRAIT_1D);

        // or like this for LANDSCAPE scanning - Preferred for dense or wide
        // codes...
        // BarcodeScanner.MWBsetDirection(BarcodeScanner.MWB_SCANDIRECTION_HORIZONTAL);
        // set the scanning rectangle based on scan direction(format in pct: x,
        // y, width, height)
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_25,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_39,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_93,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_128,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_AZTEC,
        // RECT_LANDSCAPE_2D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DM,
        // RECT_LANDSCAPE_2D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_EANUPC,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_PDF,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_QR,
        // RECT_LANDSCAPE_2D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_RSS,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_CODABAR,RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_DOTCODE,RECT_DOTCODE);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_11,
        // RECT_LANDSCAPE_1D);
        // BarcodeScanner.MWBsetScanningRect(BarcodeScanner.MWB_CODE_MASK_MSI,
        // RECT_LANDSCAPE_1D);

        // set decoder effort level (1 - 5)
        // for live scanning scenarios, a setting between 1 to 3 will suffice
        // levels 4 and 5 are typically reserved for batch scanning
        BarcodeScanner.MWBsetLevel(3);
        BarcodeScanner.MWBsetResultType(USE_RESULT_TYPE);

        // Set minimum result length for low-protected barcode types
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_25, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_MSI, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_39, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_CODABAR, 5);
        BarcodeScanner.MWBsetMinLength(BarcodeScanner.MWB_CODE_MASK_11, 5);

    }

    @Override
    public void onScannedResult(BarcodeScanner.MWResult result) throws JSONException {

        if (isDMVScanner) {

            String typeName = result.typeName;
            String barcode = result.text;
//              barcode = Base64.encodeToString(barcode, Base64.);
            if (MWPARSER_MASK != MWParser.MWP_PARSER_MASK_NONE &&
                    BarcodeScanner.MWBgetResultType() ==
                            BarcodeScanner.MWB_RESULT_TYPE_MW) {
                barcode = MWParser.MWPgetJSON(MWPARSER_MASK,
                        result.encryptedResult.getBytes());
                if (barcode == null) {
                    String parserMask = "";
                    switch (MWPARSER_MASK) {
                        case MWParser.MWP_PARSER_MASK_AAMVA:
                            parserMask = "AAMVA";
                            break;
                        case MWParser.MWP_PARSER_MASK_GS1:
                            parserMask = "GS1";
                            break;
                        case MWParser.MWP_PARSER_MASK_ISBT:
                            parserMask = "ISBT";
                            break;
                        case MWParser.MWP_PARSER_MASK_IUID:
                            parserMask = "IUID";
                            break;
                        case MWParser.MWP_PARSER_MASK_HIBC:
                            parserMask = "HIBC";
                            break;
                        case MWParser.MWP_PARSER_MASK_SCM:
                            parserMask = "SCM";
                            break;
                        default:
                            parserMask = "unknown";
                            break;
                    }
                    barcode = result.text + "\n*Not a valid " + parserMask +
                            " formatted barcode";
                }
            }
//            if (MWPARSER_MASK != MWParser.MWP_PARSER_MASK_NONE &&
//                    BarcodeScanner.MWBgetResultType() ==
//                            BarcodeScanner.MWB_RESULT_TYPE_MW) {
//
//                barcode = MWParser.MWPgetJSON(MWPARSER_MASK,
//                        result.encryptedResult.getBytes());
//                if (barcode == null) {
//                    String parserMask = "";
//
//                    switch (MWPARSER_MASK) {
//                        case MWParser.MWP_PARSER_MASK_AAMVA:
//                            parserMask = "AAMVA";
//                            break;
//                        case MWParser.MWP_PARSER_MASK_GS1:
//                            parserMask = "GS1";
//                            break;
//                        case MWParser.MWP_PARSER_MASK_ISBT:
//                            parserMask = "ISBT";
//                            break;
//                        case MWParser.MWP_PARSER_MASK_IUID:
//                            parserMask = "IUID";
//                            break;
//                        case MWParser.MWP_PARSER_MASK_HIBC:
//                            parserMask = "HIBC";
//                            break;
//                        case MWParser.MWP_PARSER_MASK_SCM:
//                            parserMask = "SCM";
//                            break;
//
//                        default:
//                            parserMask = "unknown";
//                            break;
//                    }
//
//                    barcode = result.text + "\n*Not a valid " + parserMask +
//                            " formatted barcode";
//                }
//
//            }


            if (result.isGS1) {
                typeName += " (GS1)";
            }

            try {
                JSONObject jsonObject = new JSONObject(barcode);
                JSONArray fields = jsonObject.getJSONArray("Fields");
                String country = "", fName = "", mName = "";
                for (int i = 0; i < fields.length(); i++) {
                    JSONObject fieldElement = (JSONObject) fields.get(i);
                    if (fieldElement.getString("Description").equalsIgnoreCase("Customer Family Name")) {
                        PreferenceManger.putString("Customer_Family_Name", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Customer First Name")) {
                        fName = fieldElement.getString("Value").trim();
                        PreferenceManger.putString("Customer_First_Name", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Customer Middle Name(s)")) {
                        mName = fieldElement.getString("Value").trim();
                        if (!fieldElement.getString("Value").trim().equalsIgnoreCase("none")) {
                            PreferenceManger.putString("Customer_Middle_Name(s)", fieldElement.getString("Value").trim());
                        }
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Country Identification")) {
                        country = fieldElement.getString("Value").trim();
                        PreferenceManger.putString("Country", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Document Expiration Date")) {
                        SimpleDateFormat originalFormatter;
                        if (country.equalsIgnoreCase("CAN"))
                            originalFormatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
                        else {
                            originalFormatter = new SimpleDateFormat("MMddyyyy", Locale.getDefault());
                        }
                        SimpleDateFormat newFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
                        Date date;
                        try {
                            date = originalFormatter.parse(fieldElement.getString("Value").trim());
                            String dob = newFormatter.format(date);
                            PreferenceManger.putString("Document_Expiration_Date", dob);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Date of Birth")) {
                        SimpleDateFormat originalFormatter;
                        if (country.equalsIgnoreCase("CAN"))
                            originalFormatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
                        else {
                            originalFormatter = new SimpleDateFormat("MMddyyyy", Locale.getDefault());
                        }
                        SimpleDateFormat newFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
                        Date date;
                        try {
                            date = originalFormatter.parse(fieldElement.getString("Value").trim());
                            String dob = newFormatter.format(date);
                            PreferenceManger.putString("Date_of_Birth", dob);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
    //                PreferenceManger.putString("Date_of_Birth", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Physical Description - Sex")) {
                        PreferenceManger.putString("Gender", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Address - Street 1")) {
                        PreferenceManger.putString("Address1", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Address - City")) {
                        PreferenceManger.putString("City", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Address - Jurisdiction Code")) {
                        PreferenceManger.putString("State", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Address - Postal Code")) {
                        PreferenceManger.putString("Postal_Code", fieldElement.getString("Value").trim());
                    } else if (fieldElement.getString("Description").equalsIgnoreCase("Customer ID Number")) {
                        PreferenceManger.putString("ID_Number", fieldElement.getString("Value").trim());
                    }
                }
                if (fName.length() == 0) {
                    JSONArray fieldName = jsonObject.getJSONArray("Adapted");
                    //Description
                    for (int i = 0; i < fieldName.length(); i++) {
                        JSONObject fieldElement = (JSONObject) fieldName.get(i);
                        if (fieldElement.getString("Description").equalsIgnoreCase("Complete name of cardholder")) {
                            try {
                                String fullName = fieldElement.getString("Value").trim();
                                String[] name = fullName.split(" ");
                                if (name.length > 2) {
                                    PreferenceManger.putString("Customer_First_Name", name[0]);
                                    if (!name[1].equalsIgnoreCase("none")) {
                                        PreferenceManger.putString("Customer_Middle_Name(s)", name[1]);
                                    }
                                    PreferenceManger.putString("Customer_Family_Name", name[2]);
                                    if (name.length == 4) {
                                        PreferenceManger.putString("Customer_Family_Name", name[2] + " " + name[3]);
                                    }
                                } else {
                                    if (name.length > 1) {
                                        PreferenceManger.putString("Customer_Family_Name", name[1]);
                                        PreferenceManger.putString("Customer_First_Name", name[0]);
                                    }
                                    if (name.length == 1)
                                        PreferenceManger.putString("Customer_First_Name", name[0]);
                                    //                            PreferenceManger.putString("Customer_Middle_Name(s)", name[1]);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (fieldElement.getString("Description").equalsIgnoreCase("Date of Birth")) {
                            SimpleDateFormat originalFormatter;
    //                        if (country.equalsIgnoreCase("CAN"))
    //                            originalFormatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
    //                        else {
                            originalFormatter = new SimpleDateFormat("MMddyyyy", Locale.getDefault());
    //                        }
                            SimpleDateFormat newFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
                            Date date;
                            try {
                                date = originalFormatter.parse(fieldElement.getString("Value_US").trim());
                                String dob = newFormatter.format(date);
                                PreferenceManger.putString("Date_of_Birth", dob);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else if (fieldElement.getString("Description").equalsIgnoreCase("Document Expiration Date")) {
                            SimpleDateFormat originalFormatter;
    //                        if (country.equalsIgnoreCase("CAN"))
    //                            originalFormatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
    //                        else {
                            originalFormatter = new SimpleDateFormat("MMddyyyy", Locale.getDefault());
    //                        }
                            SimpleDateFormat newFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
                            Date date;
                            try {
                                date = originalFormatter.parse(fieldElement.getString("Value_US").trim());
                                String dob = newFormatter.format(date);
                                PreferenceManger.putString("Document_Expiration_Date", dob);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else if (fieldElement.getString("Description").equalsIgnoreCase("Complete address of cardholder")) {
                            String fullName = fieldElement.getString("Value").trim();
                            String[] name = fullName.split(",");
                            PreferenceManger.putString("Address1", name[0]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
//                new CustomToast(this).alert("Unable to parse this DMV");
                skipped = true;
                if (isDMVScanner) {
                    PreferenceManger.putBoolean(AppConstant.SKIPPED, true);
                }
                finish();
            }


            // Add code to switch to next fragment

            scanningDone = true;
            PreferenceManger.putBoolean(AppConstant.SKIPPED, false);
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
//                PreferenceManger.putString(AppConstant.DMV_NUMBER, "");
            finish();
//        new AlertDialog.Builder(this)
//                .setOnDismissListener(new DialogInterface.OnDismissListener() {
//                    @Override
//                    public void onDismiss(DialogInterface dialog) {
//                        scannerFragment.resumeScanner();
//                    }
//                })
//                .setTitle(typeName)
//                .setMessage(jsonObject.getString("Card Type"))
//                .setNegativeButton("Close", null)
//                .show();
            beep_sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 200);
            beep_sound.startTone(ToneGenerator.TONE_PROP_BEEP);
        } else if (isVinScanner) {
//            String typeName = result.typeName;
            String barcode = result.text;

            if (barcode != null) {

                //Remove "i" from the beggening as it is for imported

                if (barcode.length() == 18) {
                    if (barcode.trim().toLowerCase().startsWith("i")) {
                        StringBuilder sb = new StringBuilder(barcode);
                        sb.deleteCharAt(0);
                        barcode = sb.toString();
                    } else if (barcode.trim().toLowerCase().endsWith("%")) {
                        StringBuilder sb = new StringBuilder(barcode);
                        sb.deleteCharAt(barcode.length() - 1);
                        barcode = sb.toString().toUpperCase();
                    }
                }
                if (barcode.length() > 17) {
                    barcode = barcode.substring(0, 17);
                }

                Gson gson = new GsonBuilder().create();


                RecordData recordData;
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                try {
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVINNumber(barcode);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                            File mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                                    + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/localPdf");
                            deleteRecursive(mediaStorageDir);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleConditionDisclosure(null);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleHistoryDisclosure(null);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(null);
                        } else {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getTradeInVehicle().setVINNumber(barcode);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                scanningDone = true;
                finish();
            }
        }
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.text_cancelBack:
                    Gson gson1 = new GsonBuilder().create();
                    RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                    recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    String recordDataString = gson1.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    Intent intent = new Intent(PartialViewContainerActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Objects.requireNonNull(PartialViewContainerActivity.this).finish();
                    break;
                case R.id.text_next:

                    break;
                case R.id.text_back:
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData;
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            back = true;

                            if (PreferenceManger.getBooleanValue(AppConstant.IS_USEDCAR_ONLY) && NewDealViewPager.currentPage == 1) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                Intent intent1 = new Intent(PartialViewContainerActivity.this, MainActivity.class);
                                NewDealViewPager.currentPage = -1;
                                PartialViewContainerActivity.back = false;
                                PartialViewContainerActivity.logout = false;
                                PartialViewContainerActivity.scanningDone = false;
                                PartialViewContainerActivity.skipped = false;
//                                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent1);
                            }

                        } else {
                            if (NewDealViewPager.currentPage < 56) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(false);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                tradeInBack = true;
                                Intent intent1 = new Intent(PartialViewContainerActivity.this, NewDealViewPager.class);
                                PreferenceManger.putString(AppConstant.DMV_NUMBER, PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                                intent1.putExtra("page", 33);
                                startActivity(intent1);
                            } else {
                                back = true;
                            }

                        }
                    } else {
                        back = true;
                    }
                    PartialViewContainerActivity.this.finish();

                    break;
                case R.id.txt_logout:
                    Gson gson2 = new GsonBuilder().create();
                    RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                    recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    String recordDataString1 = gson2.toJson(recordData2);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    logout = true;
                    finish();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                back = true;
                if (PreferenceManger.getBooleanValue(AppConstant.IS_USEDCAR_ONLY)&& NewDealViewPager.currentPage == 1) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    Intent intent1 = new Intent(PartialViewContainerActivity.this, MainActivity.class);
                    NewDealViewPager.currentPage = -1;
                    PartialViewContainerActivity.back = false;
                    PartialViewContainerActivity.logout = false;
                    PartialViewContainerActivity.scanningDone = false;
                    PartialViewContainerActivity.skipped = false;
//                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);
                }
            } else {
                if (NewDealViewPager.currentPage < 56) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(false);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    tradeInBack = true;
                    Intent intent1 = new Intent(PartialViewContainerActivity.this, NewDealViewPager.class);
                    PreferenceManger.putString(AppConstant.DMV_NUMBER, PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                    intent1.putExtra("page", 33);
                    startActivity(intent1);
                } else {
                    back = true;
                }
            }
        } else {
            back = true;
        }
        finish();

        super.onBackPressed();
    }

    public void showDialogCar(String type) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("url", "FromCarImagesSample");
        bundle.putString("SampleName", type);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }
}
