package com.volga.dealershieldtablet.ui.fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.customUI.EULAWebView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.ArrayList;

public class ThirdPartyWebView extends DialogFragment {

    private DSTextView text_next, text_back;
    private int height;
    private String redirectURL = "http://stg.dealerxt.com/Resource/ThirdPartyReportViewed?docId=123440";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_web_thirdparty, container, false);

        final EULAWebView mWebview = v.findViewById(R.id.webview);

//        mWebview.setOnBottomReachedListener(this, 50);

        String path = getArguments().getString("url");

//        mWebview.loadData(path, "text/html", "UTF-8");

        text_next = v.findViewById(R.id.text_next);
        text_back = v.findViewById(R.id.text_back);
        text_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        text_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        text_next.setVisibility(View.GONE);
        mWebview.loadUrl(path);
        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.setVerticalScrollBarEnabled(true);
        final ProgressBar progressBar = v.findViewById(R.id.progressBar);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        Log.e("Device height: ", "H: " + height);
        int webH = (int) Math.floor(mWebview.getContentHeight() * mWebview.getScale());
        ;
        Log.e("Web height: ", "W H: " + webH);
        progressBar.setVisibility(View.VISIBLE);
        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
//                Log.e("url", "String url: " + req.getUrl().toString());
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                Log.e("url", "String url: " + url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (url.contains("ThirdPartyReportViewed")) {
                    Gson gson = new Gson();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();

                    ArrayList<ThirdPartyReportDocs> reportDocs = vehicleDetails.getThirdPartyReportDocs();
                    if (reportDocs == null) {
                        reportDocs = new ArrayList<>();
                    }
                    ThirdPartyReportDocs partyReportDocs = new ThirdPartyReportDocs();
                    String substring = url.trim().substring(url.trim().indexOf("docId=") + 6);
                    partyReportDocs.setDocId(substring);
                    partyReportDocs.setThirdPartyId(getArguments().getString("id"));
                    reportDocs.add(partyReportDocs);
                    vehicleDetails.setThirdPartyReportDocs(reportDocs);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                    AppConstant.report.
                    int index = 0;
                    for (int i = 0; i < AppConstant.report.size(); i++) {
                        if (AppConstant.report.get(i).getId().equalsIgnoreCase(getArguments().getString("id"))) {
                            index = i;
                            break;
                        }
                    }
                    AppConstant.report.get(index).setReportSeen(true);
                    AppConstant.report.get(index).setChecked(true);
                    Log.e("url", "String url: " + url + " ID: " + substring);
                    getDialog().dismiss();
                }
                progressBar.setVisibility(View.GONE);
            }
        });

        ViewTreeObserver viewTreeObserver = mWebview.getViewTreeObserver();

        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                int webH = mWebview.getMeasuredHeight();
                if (webH != 0) {
                    Log.e("Web height: ", "W H: " + webH);
//                    Toast.makeText(getActivity(), "height:"+webH,Toast.LENGTH_SHORT).show();
                    mWebview.getViewTreeObserver().removeOnPreDrawListener(this);
                }
                return false;
            }
        });

//        mWebview.loadUrl(path);
        return v;
    }


}