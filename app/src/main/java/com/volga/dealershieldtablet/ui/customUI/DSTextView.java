package com.volga.dealershieldtablet.ui.customUI;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.volga.dealershieldtablet.R;


/**
 * Created by skm
 */

public class DSTextView extends androidx.appcompat.widget.AppCompatTextView {
    public DSTextView(Context context) {
        super(context);

    }

    public DSTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DSTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    public void init(Context context, AttributeSet attrs) {
        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView, 0, 0);

            String font_name = a.getString(R.styleable.CustomFontTextView_custom_ds_font);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), font_name);
            a.recycle();
            setTypeface(typeface);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
