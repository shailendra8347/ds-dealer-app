package com.volga.dealershieldtablet.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.ArrayList;


public class FragmentTestDriveMandetory extends BaseFragment {

    private View mView;
    private DSTextView next;
    public static boolean nowTestDriveTaken;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.force_test_drive, container, false);
        nowTestDriveTaken = false;
        initView();
        return mView;
    }

    private void initView() {
        final DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        DSTextView title = mView.findViewById(R.id.title);
        logout.setVisibility(View.GONE);
        text_cancelBack.setText(getText(R.string.cancel));

        text_cancelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_cancelBack.setEnabled(false);
                text_cancelBack.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        text_cancelBack.setEnabled(true);
                        text_cancelBack.setClickable(true);
                    }
                }, 4000);
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);


                VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
                ArrayList<HistoryListPojo> bh = vehicleDetails.getHistorySelection();
                ArrayList<HistoryListPojo> ch = vehicleDetails.getCoHistorySelection();
                ArrayList<HistoryListPojo> bc = vehicleDetails.getConditionSelection();
                ArrayList<HistoryListPojo> cc = vehicleDetails.getCoConditionSelection();
                ArrayList<HistoryListPojo> br = vehicleDetails.getReportSelection();
                ArrayList<HistoryListPojo> cr = vehicleDetails.getCoReportSelection();
                if (bh != null) {
                    for (int i = 0; i < bh.size(); i++) {
                        bh.get(i).setChecked(false);
                    }
                }
                if (ch != null)
                    for (int i = 0; i < ch.size(); i++) {
                        ch.get(i).setChecked(false);
                    }
                if (bc != null)
                    for (int i = 0; i < bc.size(); i++) {
                        bc.get(i).setChecked(false);
                    }
                if (cc != null)
                    for (int i = 0; i < cc.size(); i++) {
                        cc.get(i).setChecked(false);
                    }
                if (br != null)
                    for (int i = 0; i < br.size(); i++) {
                        br.get(i).setChecked(false);
                    }
                if (cr != null)
                    for (int i = 0; i < cr.size(); i++) {
                        cr.get(i).setChecked(false);
                    }
                vehicleDetails.setCoConditionSelection(cc);
                vehicleDetails.setConditionSelection(bc);
                vehicleDetails.setHistorySelection(bh);
                vehicleDetails.setCoHistorySelection(ch);
                vehicleDetails.setReportSelection(br);
                vehicleDetails.setCoReportSelection(cr);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                cancel();
            }
        });
//        text_cancelBack.setVisibility(View.GONE);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
            text_cancelBack.setVisibility(View.INVISIBLE);
            title.setText(getText(R.string.take_test_drive));
        } else {
            text_cancelBack.setVisibility(View.VISIBLE);
            title.setText(getText(R.string.please_take_test_drive_new));

        }
        next = mView.findViewById(R.id.text_next);
        TextView back = mView.findViewById(R.id.text_back_up);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nowTestDriveTaken = true;
                getActivity().finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

    }


}
