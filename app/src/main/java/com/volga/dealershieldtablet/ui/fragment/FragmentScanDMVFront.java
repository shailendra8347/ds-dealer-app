package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.cameraview.CameraView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.CheckIDViewPager;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class FragmentScanDMVFront extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final int REQUEST_STORAGE = 2;
    private View mView;
    private static final String TAG = "MainActivity";

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final String FRAGMENT_DIALOG = "dialog";
    private DSTextView next;
    private Button takePicture;
    private ProgressDialog progressDialog;
    private boolean isAutoFocus, isCheckID;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.capture_dmv, container, false);
        if (AppConstant.shouldCall) {
            AppConstant.shouldCall = false;
            showFragmentInPortrait();
        }
        initView();
        return mView;
    }

    private void initView() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.optimizing_image));
        progressDialog.setCancelable(false);
        assert getArguments() != null;
        isCheckID = getArguments().getBoolean("checkId", false);
//        if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)) {
//            PreferenceManger.putString(AppConstant.DMV_NUMBER, "");
//        }
        isAutoFocus = false;
        mCameraView = mView.findViewById(R.id.camera);
        if (mCameraView != null) {
            mCameraView.addCallback(mCallback);
        }
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            /**
             * Called when a hardware key is dispatched to a view. This allows listeners to
             * get a chance to respond before the target view.
             * <p>Key presses in software keyboards will generally NOT trigger this method,
             * although some may elect to do so in some situations. Do not assume a
             * software input method has to be key-based; even if it is, it may use key presses
             * in a different way than you expect, so there is no way to reliably catch soft
             * input key presses.
             *
             * @param v       The view the key has been dispatched to.
             * @param keyCode The code for the physical key that was pressed
             * @param event   The KeyEvent object containing full information about
             *                the event.
             * @return True if the listener has consumed the event, false otherwise.
             */
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Log.i("DMV Front", "onKey Back listener is working!!!");
                    goBackPage();
                    return true;
                }
                return false;
            }


        });
        takePicture = (Button) mView.findViewById(R.id.take_picture);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        back.setClickable(true);
        back.setEnabled(true);
        DSTextView desc = mView.findViewById(R.id.desc);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            desc.setText(Html.fromHtml(getString(R.string.co_please_take_a_picture_of_the_front_of_the_drivers_license)));
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned())
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(true);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        } else {
            desc.setText(Html.fromHtml(getString(R.string.please_take_a_picture_of_the_front_of_the_drivers_license)));
//            assert recordData != null;
            if (!isCheckID&&recordData!=null) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            }
        }
        next = mView.findViewById(R.id.text_next);
        DSTextView cancel = mView.findViewById(R.id.text_cancelBack);

        if (isCheckID ||(recordData!=null&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())) {
            cancel.setVisibility(View.INVISIBLE);
        } else {
            cancel.setVisibility(View.VISIBLE);
        }
//        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
//            cancel.setVisibility(View.INVISIBLE);
//        } else {
//            cancel.setVisibility(View.VISIBLE);
//        }

//        cancel.setVisibility(View.GONE);
        next.setVisibility(View.GONE);
        takePicture.setVisibility(View.GONE);
        mView.findViewById(R.id.sampleContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogCar("DMVFront");
            }
        });
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
//        back.setOnClickListener(mOnClickListener);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBackPage();
            }
        });


        cancel.setOnClickListener(mOnClickListener);
        if (takePicture != null) {
            takePicture.setOnClickListener(mOnClickListener);
        }
    }

    private CameraView mCameraView;

    private Handler mBackgroundHandler;
//    private static final int STORAGE_PERMISSION_CODE = 100;

//    private static final String TAG = "PERMISSION_TAG";

//    private void requestPermission(){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
//            //Android is 11(R) or above
//            try {
//                Log.d(TAG, "requestPermission: try");
//
//                Intent intent = new Intent();
//                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
//                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
//                intent.setData(uri);
//                storageActivityResultLauncher.launch(intent);
//            }
//            catch (Exception e){
//                Log.e(TAG, "requestPermission: catch", e);
//                Intent intent = new Intent();
//                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
//                storageActivityResultLauncher.launch(intent);
//            }
//        }
//        else {
//            //Android is below 11(R)
//            ActivityCompat.requestPermissions(
//                    getActivity(),
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
//                    STORAGE_PERMISSION_CODE
//            );
//        }
//    }

//    private ActivityResultLauncher<Intent> storageActivityResultLauncher = registerForActivityResult(
//            new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>() {
//                @Override
//                public void onActivityResult(ActivityResult result) {
//                    Log.d(TAG, "onActivityResult: ");
//                    //here we will handle the result of our intent
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
//                        //Android is 11(R) or above
//                        if (Environment.isExternalStorageManager()){
//                            //Manage External Storage Permission is granted
////                            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
////            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
////                            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
////                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is granted");
////                            createFolder();
//                        }
//                        else{
//                            requestPermission();
//                            //Manage External Storage Permission is denied
////                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is denied");
////                            Toast.makeText(MainActivity.this, "Manage External Storage Permission is denied", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else {
//                        //Android is below 11(R)
//                    }
//                }
//            }
//    );
//    public boolean checkPermission(){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
//            //Android is 11(R) or above
//            return Environment.isExternalStorageManager();
//        }
//        else{
//            //Android is below 11(R)
//            int write = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
//            int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
//
//            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
//        }
//    }

    /*Handle permission request results*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE){
            if (grantResults.length > 0){
                //check each permission if granted or not
                boolean write = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean read = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (write && read){
                    //External Storage permissions granted
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permissions granted");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            takePicture.setVisibility(View.VISIBLE);
                        }
                    }, 1500);
                }
                else{
                    //External Storage permission denied
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permission denied");
//                    Toast.makeText(this, "External Storage permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.take_picture:
                    //ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //                            == PackageManager.PERMISSION_GRANTED
                    if (checkPermission()) {
                        progressDialog.show();
                        disableNext(takePicture);
                        takePicture.setVisibility(View.GONE);
                        takePicture.setEnabled(false);
                        if (mCameraView != null) {
                            mCameraView.takePicture();
                        }
                    } else {
                        requestPermission();
//                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                            FragmentScanDMVFront.ConfirmationDialogFragment
//                                    .newInstance(R.string.storage_permission_confirmation,
//                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                            REQUEST_STORAGE,
//                                            R.string.storage_permission_not_granted)
//                                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
//                        } else {
//                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                    REQUEST_STORAGE);
//                        }
                    }
                    break;
                case R.id.text_cancelBack:
                    cancel();
                    break;
                case R.id.text_next:
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//                    if ( recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()&& recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer())
//                    iGoToNextPage.whatNextClick();
//                    else{
                    ((NewDealViewPager) getActivity()).setCurrentPage(55);
//                    }
                    break;
                case R.id.text_back:
                    goBackPage();

                    break;
                case R.id.txt_logout:
                    logout();
                    break;
            }
        }
    };

    private void goBackPage() {
        if (getActivity() instanceof NewDealViewPager) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {

                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setDrivingLicenceNumber("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setFirstName("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setMiddleName("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setLastName("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setAddressLineOne("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setAddressLineTwo("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setCity("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setState("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setCountry("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setDateOfBirth("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setDLExpiryDate("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setGender("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setZipCode("");
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                }
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                    iGoToNextPage.goToBackIndex();
                    return;
                }
//                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isIncompleteDeal()) {
                if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
                    //                    if (!PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                    iGoToNextPage.goToBackIndex();
                    //                    } else {
                    //                        ((NewDealViewPager) getActivity()).setCurrentPage(33);
                    //                    }
                } else {
                    if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS))
                        ((NewDealViewPager) getActivity()).setCurrentPage(29);
                    else {
                        ((NewDealViewPager) getActivity()).setCurrentPage(31);
                    }
                }
//                }
            } else {
                assert recordData != null;

                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setDLNumber("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setFirstName("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setMiddleName("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setLastName("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setAddressLineOne("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setAddressLineTwo("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setCity("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setState("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setCountry("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setDateOfBirth("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setDLExpiryDate("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setGender("");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setZipCode("");

                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasCoBuyer(false);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                } else {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
                        new CustomToast(getActivity()).alert("You can not edit prepped deal. Please continue.");
                        return;
                    }
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                }


//                else {
//                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasCoBuyer(false);
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
//                    }
//                }
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                    ((NewDealViewPager) getActivity()).setCurrentPage(59);
                } else {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
                        new CustomToast(getActivity()).alert("You can not edit prepped deal. Please continue.");
                    } else {
                        ((NewDealViewPager) getActivity()).setCurrentPage(58);
                    }

                }
            }
        } else if (getActivity() instanceof CheckIDViewPager) {
            callHomeActivity();
        }
    }

    private void goNext() {
        FragmentImagePreview nextFragment = new FragmentImagePreview();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.DMV_FRONT);
        nextFragment.setArguments(bundle);
        next(nextFragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (NewDealViewPager.currentPage == 54 || CheckIDViewPager.currentPage == 0)
                        mCameraView.start();
                }
            }, 300);
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                == PackageManager.PERMISSION_GRANTED)
        if (checkPermission()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    takePicture.setVisibility(View.VISIBLE);
                }
            }, 1500);
        } else if (!checkPermission()) {
            //ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
            //                Manifest.permission.WRITE_EXTERNAL_STORAGE)
//            ConfirmationDialogFragment
//                    .newInstance(R.string.storage_permission_confirmation,
//                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            REQUEST_STORAGE,
//                            R.string.storage_permission_not_granted)
//                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
            requestPermission();
        } else {
            requestPermission();
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    REQUEST_STORAGE);
        }
    }

    @Override
    public void onPause() {
        mCameraView.stop();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBackgroundHandler != null) {
            mBackgroundHandler.getLooper().quitSafely();
            mBackgroundHandler = null;
        }
    }

    @Override
    public void setUserVisibleHint(final boolean isVisibleToUser) {
        if (mCameraView != null) {
            if (isVisibleToUser) {
                enableNext(takePicture);
                showFragmentInPortrait();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (NewDealViewPager.currentPage == 54 || CheckIDViewPager.currentPage == 0)
                            mCameraView.start();
                    }
                }, 300);
//                mCameraView.setAutoFocus(true);
            } else {
                mCameraView.stop();
//                mCameraView.setAutoFocus(false);
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case REQUEST_CAMERA_PERMISSION:
//                if (permissions.length != 1 || grantResults.length != 1) {
//                    throw new RuntimeException("Error on requesting camera permission.");
//                }
//                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getActivity(), R.string.camera_permission_not_granted,
//                            Toast.LENGTH_SHORT).show();
//                }
//                // No need to start camera here; it is handled by onResume
//                break;
//            case REQUEST_STORAGE:
//                if (permissions.length != 1 || grantResults.length != 1) {
//                    throw new RuntimeException("Error on requesting storage permission.");
//                }
//                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getActivity(), R.string.storage_permission_not_granted,
//                            Toast.LENGTH_SHORT).show();
//                }
//                // No need to start camera here; it is handled by onResume
//                break;
//        }
//    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }


    private CameraView.Callback mCallback
            = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
            new Handler().postDelayed(() -> takePicture.setVisibility(View.VISIBLE), 1600);
            Log.d(TAG, "onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            Log.d(TAG, "onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] data) {
            Log.d(TAG, "onPictureTaken " + data.length);
            SaveImageToMemory imageToMemory = new SaveImageToMemory(data);
            imageToMemory.execute();
        }

    };

    @SuppressLint("StaticFieldLeak")
    private class SaveImageToMemory extends AsyncTask<Void, Void, Void> {
        byte[] data;

        SaveImageToMemory(byte[] data) {
            this.data = data;
        }


        @Override
        protected Void doInBackground(Void... params) {
            compressImage(data);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            progressDialog.dismiss();
//            iGoToNextPage.whatNextClick();
            if (getActivity() instanceof NewDealViewPager) {
                ((NewDealViewPager) getActivity()).setCurrentPage(55);
            }else {
                iGoToNextPage.whatNextClick();
            }
            takePicture.setEnabled(true);

        }


    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static ConfirmationDialogFragment newInstance(@StringRes int message,
                                                             String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

    }

    /**
     * Create a File for saving an image or video
     */
    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        String middle;
        if (!isCheckID) {
            middle = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        } else {
            middle = "CheckID";
        }
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + middle + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    IGoToNextPage iGoToNextPage;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    public void compressImage(byte[] data) {

        File file;
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (!isCheckID) {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                file = getOutputMediaFile("LicenseFront", "co_buyer_DLFRONT");
            } else {
                file = getOutputMediaFile("LicenseFront", "DLFRONT");
            }
        } else {
            file = getOutputMediaFile("LicenseFront", "DLFRONT");
        }
        /* = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            "frontPicture.jpg");*/
        OutputStream os = null;
        try {

            os = new FileOutputStream(file);
            os.write(data);
            os.close();
            Log.e(TAG, "doInBackground: " + file.length() / 1024);

        } catch (IOException e) {
            Log.e(TAG, "Cannot write to " + file, e);
            saveLogsInToDB("Scan DMV Front: Error in saving Front Image " + e.getLocalizedMessage() + "\n");
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    saveLogsInToDB("Scan DMV Front: Error in saving Front Image " + e.getLocalizedMessage() + "\n");

                }
            }

        }
        optimizeImage(file, data);
    }
}
