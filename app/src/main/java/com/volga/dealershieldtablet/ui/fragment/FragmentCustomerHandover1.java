package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentCustomerHandover1 extends BaseFragment {


    private View mView;
    IGoToNextPage iGoToNextPage;
    private DSTextView cancel;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            showFragmentInPortrait();
    }

    private AppCompatButton title, esign;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_customer_handover1, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        back.setText("Back");
        DSTextView next = mView.findViewById(R.id.text_next);
        cancel = mView.findViewById(R.id.text_cancelBack);
        title = mView.findViewById(R.id.moveNext);
        esign = mView.findViewById(R.id.esignDoc);
        title.setOnClickListener(mOnClickListener);
        esign.setOnClickListener(mOnClickListener);
        next.setVisibility(View.GONE);
        DSTextView or = mView.findViewById(R.id.or);
//        String a = "Please hand tablet to ";
//        String b = "to complete questions";
//        String c = "<font color='#F54337'>Buyer </font>";
        title.setText(Html.fromHtml(getString(R.string.buyer_handover)));
        esign.setText(Html.fromHtml(getString(R.string.buyer_esign)));
        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);

        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        boolean isWebDeal = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal();
        if (isWebDeal) {
            esign.setVisibility(View.GONE);
            cancel.setVisibility(View.INVISIBLE);
            or.setVisibility(View.GONE);
        } else {
            esign.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
            or.setVisibility(View.VISIBLE);
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    cancel.setEnabled(false);
                    cancel.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cancel.setEnabled(true);
                            cancel.setClickable(true);
                        }
                    }, 6000);
                    cancel();
                    break;
//                case R.id.text_next:
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
//                    if (vehicleDetails != null) {
//                        ArrayList<HistoryListPojo> bh = vehicleDetails.getHistorySelection();
//                        ArrayList<HistoryListPojo> ch = vehicleDetails.getCoHistorySelection();
//                        ArrayList<HistoryListPojo> bc = vehicleDetails.getConditionSelection();
//                        ArrayList<HistoryListPojo> cc = vehicleDetails.getCoConditionSelection();
//                        ArrayList<HistoryListPojo> br = vehicleDetails.getReportSelection();
//                        ArrayList<HistoryListPojo> cr = vehicleDetails.getCoReportSelection();
//                        if (bh != null) {
//                            for (int i = 0; i < bh.size(); i++) {
//                                bh.get(i).setChecked(false);
//                            }
//                        }
//                        if (ch != null)
//                            for (int i = 0; i < ch.size(); i++) {
//                                ch.get(i).setChecked(false);
//                            }
//                        if (bc != null)
//                            for (int i = 0; i < bc.size(); i++) {
//                                bc.get(i).setChecked(false);
//                            }
//                        if (cc != null)
//                            for (int i = 0; i < cc.size(); i++) {
//                                cc.get(i).setChecked(false);
//                            }
//                        if (br != null)
//                            for (int i = 0; i < br.size(); i++) {
//                                br.get(i).setChecked(false);
//                            }
//                        if (cr != null)
//                            for (int i = 0; i < cr.size(); i++) {
//                                cr.get(i).setChecked(false);
//                            }
//                        vehicleDetails.setCoConditionSelection(cc);
//                        vehicleDetails.setConditionSelection(bc);
//                        vehicleDetails.setHistorySelection(bh);
//                        vehicleDetails.setCoHistorySelection(ch);
//                        vehicleDetails.setReportSelection(br);
//                        vehicleDetails.setCoReportSelection(cr);
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
//                        String recordDataString = gson.toJson(recordData);
//                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                    }
//                    iGoToNextPage.whatNextClick();
//                    break;

                case R.id.moveNext:
                    title.setEnabled(false);
                    title.setClickable(false);

                    title.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    title.setTextColor(getActivity().getColor(R.color.white));
                    esign.setTextColor(getActivity().getColor(R.color.black));
                    esign.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
                    title.setPadding(20, 20, 20, 20);
                    esign.setPadding(20, 20, 20, 20);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
                    if (vehicleDetails != null) {
                        ArrayList<HistoryListPojo> bh = vehicleDetails.getHistorySelection();
                        ArrayList<HistoryListPojo> ch = vehicleDetails.getCoHistorySelection();
                        ArrayList<HistoryListPojo> bc = vehicleDetails.getConditionSelection();
                        ArrayList<HistoryListPojo> cc = vehicleDetails.getCoConditionSelection();
                        ArrayList<HistoryListPojo> br = vehicleDetails.getReportSelection();
                        ArrayList<HistoryListPojo> cr = vehicleDetails.getCoReportSelection();
                        if (bh != null) {
                            for (int i = 0; i < bh.size(); i++) {
                                bh.get(i).setChecked(false);
                            }
                        }
                        if (ch != null)
                            for (int i = 0; i < ch.size(); i++) {
                                ch.get(i).setChecked(false);
                            }
                        if (bc != null)
                            for (int i = 0; i < bc.size(); i++) {
                                bc.get(i).setChecked(false);
                            }
                        if (cc != null)
                            for (int i = 0; i < cc.size(); i++) {
                                cc.get(i).setChecked(false);
                            }
                        if (br != null)
                            for (int i = 0; i < br.size(); i++) {
                                br.get(i).setChecked(false);
                            }
                        if (cr != null)
                            for (int i = 0; i < cr.size(); i++) {
                                cr.get(i).setChecked(false);
                            }
                        vehicleDetails.setCoConditionSelection(cc);
                        vehicleDetails.setConditionSelection(bc);
                        vehicleDetails.setHistorySelection(bh);
                        vehicleDetails.setCoHistorySelection(ch);
                        vehicleDetails.setReportSelection(br);
                        vehicleDetails.setCoReportSelection(cr);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            title.setEnabled(true);
                            title.setClickable(true);
                        }
                    }, 6000);
                    iGoToNextPage.whatNextClick();
                    break;
                case R.id.text_back_up:
                    Gson gson1 = new GsonBuilder().create();
                    RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                        if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
//                            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                            String recordDataString = gson1.toJson(recordData1);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        }
                        ((NewDealViewPager) getActivity()).setCurrentPage(58);
                    } else {
                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(63);
                        } else {
                            if (PreferenceManger.getBooleanValue(AppConstant.SKIP_INSURANCE)) {
                                ((NewDealViewPager) getActivity()).setCurrentPage(63);
                            } else {
                                String path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber() + "/CarImages";
                                if (!new File(path_car + "/IMG_insurance.jpg").exists()) {
                                    ((NewDealViewPager) getActivity()).setCurrentPage(64);
                                } else {
                                    iGoToNextPage.goToBackIndex();
                                }
                            }
                        }
                    }

                    break;
                case R.id.esignDoc:
                    esign.setEnabled(false);
                    esign.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            esign.setEnabled(true);
                            esign.setClickable(true);
                        }
                    }, 6000);
                    esign.setTextColor(getActivity().getColor(R.color.white));
                    title.setTextColor(getActivity().getColor(R.color.black));
                    esign.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    title.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
                    esign.setPadding(20, 20, 20, 20);
                    title.setPadding(20, 20, 20, 20);
                    Gson gson2 = new GsonBuilder().create();
                    Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                    intent.putExtra(AppConstant.FROM_FRAGMENT, "esignContact");
                    RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    SettingsAndPermissions settingsAndPermissions = recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
                    recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                    String recordDataString = gson2.toJson(recordData2);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    intent.putExtra("hasCobuyer", settingsAndPermissions.isHasCoBuyer());
                    intent.putExtra("isCobuyer", false);
                    getActivity().startActivity(intent);
//Write logic to esign
                    break;
                case R.id.txt_logout:
                    logout();
                    break;
            }
        }
    };
}
