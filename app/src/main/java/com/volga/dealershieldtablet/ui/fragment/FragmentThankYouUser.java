package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentThankYouUser extends BaseFragment {


    private View mView;
    IGoToNextPage iGoToNextPage;
    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.thankyou_screen1, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView handover = mView.findViewById(R.id.submit);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setText("Page 8 of 9");
        handover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Are you Dealer representative?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
//                                next(new FragmentFinalReport());
                                iGoToNextPage.whatNextClick();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                               dialog.dismiss();
                            }
                        }).show();
            }
        });
    }


}
