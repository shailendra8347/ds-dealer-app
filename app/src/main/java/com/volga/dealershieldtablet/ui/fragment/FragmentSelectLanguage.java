package com.volga.dealershieldtablet.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.Language;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.adapter.LanguageSelectionAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TreeMap;

import timber.log.Timber;

import static android.os.Looper.getMainLooper;


public class FragmentSelectLanguage extends BaseFragment implements LanguageSelectionAdapter.IOnLanguageSelect {

    private View mView;
    private RecyclerView recyclerView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isLanguageSelected = false;
    private String selectedLanguage = "";
    private int selectedLanguageId = -1;

    IGoToNextPage iGoToNextPage;
    ImageView delete;
    private Locale myLocale;
    private boolean isCoBuyer = false;
    private DSTextView sign, time;
    private boolean runningSign = false;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isSigned = false;
        isLanguageSelected = false;
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && selectedLanguage != null && selectedLanguage.length() > 0 && isSigned) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }
        }
    };

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (selectedLanguage != null && selectedLanguage.length() > 0 && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (selectedLanguage != null && selectedLanguage.length() > 0 && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            signaturePad.clear();
            enableNext(next);

            disableWithGray(next);
            isSigned = false;
            updateData();
        } else {
            runningSign = false;
        }
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        isCoBuyer = recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();

        if (sign != null) {
            time.setText(getCurrentTimeString());
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                sign.setText(Html.fromHtml(getString(R.string.signature_la)));
            } else {
                sign.setText(Html.fromHtml(getString(R.string.co_signature_la)));
            }

            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                DSTextView vin = mView.findViewById(R.id.vin);
                DSTextView vehicle = mView.findViewById(R.id.vehicle);
                String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
                String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
                String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
                String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
                vin.setText(String.format("VIN: %s", vinNumber));
                vehicle.setText(String.format("%s, %s, %s", make, model, year));
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.language_selection, container, false);
        initView();
        return mView;
    }


    private Handler mBackgroundHandler;

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    private void initView() {
        recyclerView = (RecyclerView) mView.findViewById(R.id.languageRecycler);
        next = mView.findViewById(R.id.text_next);
        time = mView.findViewById(R.id.time);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setText(String.format(getString(R.string.page1), "1", decidePageNumber()));
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        //disble
        next.setVisibility(View.VISIBLE);
        disableWithGray(next);
        if (NewDealViewPager.currentPage == 68) {
            showProgressFor2Sec();
        }
        next.setText(getString(R.string.next_la));
        back.setText(getString(R.string.back_la));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSound(selectedLanguageId);
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "language_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_language_screen");
                }
                scrollableScreenshot(mainContent, file);

            }
        });

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;

                    File fileScr;
                    if (!isCoBuyer) {
                        fileScr = getOutputFile("Screenshot", "language_screen");
                    } else {
                        fileScr = getOutputFile("Screenshot", "co_language_screen");
                    }
                    Timber.e(fileScr.getName() + " is deleted : " + fileScr.delete());
                    if (!isCoBuyer)
                        svgFile = new File(path_signature + "/IMG_language_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_language_selection.svg");
                    }
                    File file1;
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "language_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_language_screen_pic");
                    }
                    Timber.e(file1.getName() + " is deleted : " + file1.delete());
                    Timber.e(svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Lingver.getInstance().setLocale(DSAPP.getInstance(), "en");
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;

                File fileScr;
                if (!isCoBuyer) {
                    fileScr = getOutputFile("Screenshot", "language_screen");
                } else {
                    fileScr = getOutputFile("Screenshot", "co_language_screen");
                }
                Timber.e(fileScr.getName() + " is deleted : " + fileScr.delete());
                if (!isCoBuyer)
                    svgFile = new File(path_signature + "/IMG_language_selection.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_language_selection.svg");
                }
                File file1;
                if (!isCoBuyer) {
                    file1 = getOutputFile("UserPic", "language_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "co_language_screen_pic");
                }
                Timber.e(file1.getName() + " is deleted : " + file1.delete());
                Timber.e(svgFile.getName() + " is deleted : " + svgFile.delete());
                if (!isCoBuyer) {
                    //change logic as pager
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null) {
                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
//                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                                setLocale("en", 67);
//                            } else {
//                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
//                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
//                                    String recordDataString = gson.toJson(recordData);
//                                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                                }
//                                setLocale("en", 58);
//                            }
                        } else {
                            iGoToNextPage.goToBackIndex();
                        }
                    } else {
//                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())
                            iGoToNextPage.goToBackIndex();
//                        else {
//                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
//                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
//                                String recordDataString = gson.toJson(recordData);
//                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                            }
//                            setLocale("en", 58);
//                        }
                    }
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSelectedLanguage(null);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSelectedLanguageId(0);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                } else {/**/
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()){
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            }
                            setLocale("en", 58);
                        }else {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null) {
                                openSpecificPage(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            } else {
                                openSpecificPage("English");
                            }
                        }
//                        iGoToNextPage.goToBackIndex();

                    }else{
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null) {
                            openSpecificPage(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                        } else {
                            openSpecificPage("English");
                        }
                    }

//                    ((NewDealViewPager) getActivity()).setCurrentPage(58);
                }
            }
        });
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        sign = mView.findViewById(R.id.signhere);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);


        DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);

        if (!visibleHintCalled) {
            updateData();
        }
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(new LanguageSelectionAdapter(getActivity(), this, dealershipData));
        DealershipDetails dealershipDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getDealershipDetails();
        if (dealershipData.where(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)).getLanguages() != null) {
            SettingsAndPermissions settingPermission = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
            String selectedLanguage;
            isCoBuyer = settingPermission.isCoBuyer();
            if (!settingPermission.isCoBuyer()) {
                selectedLanguage = settingPermission.getSelectedLanguage();
            } else {
                selectedLanguage = settingPermission.getCoBuyerSelectedLanguage();
            }

            List<Language> languageList = dealershipData.where(dealershipDetails.getDealershipId()).getLanguages();
            if (selectedLanguage != null) {
                for (int i = 0; i < languageList.size(); i++) {

                    if (selectedLanguage.equalsIgnoreCase(languageList.get(i).getLanguageName())) {
                        isLanguageSelected = true;
                        this.selectedLanguage = languageList.get(i).getLanguageName();
                        this.selectedLanguageId = languageList.get(i).getLanguageId();
                        break;
                    }
                }
            }
        }

        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        signaturePad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSigned = false;

            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
                sign.setVisibility(View.VISIBLE);
            }
        });

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                sign.setVisibility(View.GONE);
                enableDelete(delete);
                time.setText(getCurrentTimeString());
                Log.e("signing start: ", " >>>>>>>>>>>>>>");
//                Handler mainHandler = new Handler(getActivity().getMainLooper());
//                Runnable myRunnable = new Runnable() {
//                    @Override
//                    public void run() {
                if (!runningSign) {
                    runningSign = true;
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("UserPic", "language_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_language_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                        if (selectedLanguage != null && selectedLanguage.length() > 0) {
                            next.setVisibility(View.VISIBLE);
                            enableWithGreen(next);
                        }
                    }
                }
//                    } // This is your code
//                };
//                mainHandler.post(myRunnable);

            }

            @Override
            public void onSigned() {
                isSigned = true;

                if (selectedLanguage != null && selectedLanguage.length() > 0) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isSigned) {
                                    return;
                                }
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    } else if (isTaken) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }

                }
            }

            @Override
            public void onClear() {
                Log.e("Clear: ", "Cleared");
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {
                        signaturePad.clear();
                        isSigned = false;
                        disableWithGray(next);

                        sign.setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return true;
            }
        });

    }


    private void openSpecificPage(String selectedLanguage) {

        if (selectedLanguage.equalsIgnoreCase("Chinese")) {
            setLocale("zh", 82);
        } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
            setLocale("es", 82);
        } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
            setLocale("vi", 82);
        } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
            setLocale("ko", 82);
        } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
            setLocale("tl", 82);
        } else if (selectedLanguage.equalsIgnoreCase("English")) {
            setLocale("en", 82);
        }
    }

    @Override
    public void languageSelected(String selectedLanguage, int selectedLanguageId) {
//        playSound(selectedLanguageId);
        if (isSigned) {
            enableWithGreen(next);
            next.setVisibility(View.VISIBLE);
        }
        this.selectedLanguage = selectedLanguage;
        this.selectedLanguageId = selectedLanguageId;
    }

    public void setLocale(String localeName, int page) {
        /*loading lng from lib*/
        Lingver.getInstance().setLocale(getActivity(), localeName);
        Timber.e(" Current locale: %s", localeName);
        getActivity().finish();
        Intent refresh = new Intent(getActivity(), NewDealViewPager.class);
        refresh.putExtra("page", page);
        PreferenceManger.putString(AppConstant.CURRENT_LANG, localeName);
        refresh.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        refresh.putExtra("CurrentLang", localeName);
        startActivity(refresh);
        Objects.requireNonNull(getActivity()).finish();


//        Resources res = context.getResources();
//// Change locale settings in the app.
//        DisplayMetrics dm = res.getDisplayMetrics();
//        android.content.res.Configuration conf = res.getConfiguration();
//        conf.setLocale(new Locale(language_code.toLowerCase())); // API 17+ only.
//// Use conf.locale = new Locale(...) if targeting lower versions
//        res.updateConfiguration(conf, dm);
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {

            File svgFile;
            if (!isCoBuyer) {
                svgFile = getOutputMediaFile("Signatures", "language_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_language_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            if (!isCoBuyer) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) Objects.requireNonNull(getActivity())).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSelectedLanguage(this.selectedLanguage);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setIsRemote(false);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setSelectedLanguageId(this.selectedLanguageId);
            } else {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setIsRemote(false);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerSelectedLanguage(this.selectedLanguage);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerSelectedLanguageId(this.selectedLanguageId);
            }
        }
        Timber.e(selectedLanguageId + " " + selectedLanguage);
        assert recordData != null;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used")) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleTypeId("1");
        } else {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleTypeId("2");
        }
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//        if (!selectedLanguage.equalsIgnoreCase("English")) {
        if (this.selectedLanguage.equalsIgnoreCase("Chinese")) {
            setLocale("zh", 69);
        } else if (this.selectedLanguage.equalsIgnoreCase("Spanish")) {
            setLocale("es", 69);
        } else if (this.selectedLanguage.equalsIgnoreCase("Vietnamese")) {
            setLocale("vi", 69);
        } else if (this.selectedLanguage.equalsIgnoreCase("Korean")) {
            setLocale("ko", 69);
        } else if (this.selectedLanguage.equalsIgnoreCase("Tagalog")) {
            setLocale("tl", 69);
        } else if (this.selectedLanguage.equalsIgnoreCase("English")) {
            setLocale("en", 69);
        }

    }


}
