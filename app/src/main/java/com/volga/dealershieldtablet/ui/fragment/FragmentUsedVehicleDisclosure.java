package com.volga.dealershieldtablet.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class FragmentUsedVehicleDisclosure extends BaseFragment {
    private IGoToNextPage iGoToNextPage;
    private View mView;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showFragmentInPortrait();
            if (getActivity() != null)
                showProgressFor2Sec();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.hand_to_finance, container, false);
        initView();
        return mView;
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        if (NewDealViewPager.currentPage == 28) {
            showProgressFor2Sec();
        }
        View next = mView.findViewById(R.id.text_next);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        AppCompatButton continueDeal = mView.findViewById(R.id.continue_deal);
        final AppCompatButton preppedDeal = mView.findViewById(R.id.prepped_deal);
        preppedDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preppedDeal.setClickable(false);
                preppedDeal.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        preppedDeal.setClickable(true);
                        preppedDeal.setEnabled(true);
                    }
                }, 3000);
                cancel();
            }
        });
        continueDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueDeal.setClickable(false);
                continueDeal.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        continueDeal.setClickable(true);
                        continueDeal.setEnabled(true);
                    }
                }, 6000);
                iGoToNextPage.whatNextClick();
            }
        });
//        DSTextView disclosure = mView.findViewById(R.id.disclosure);
//        Gson gson = new GsonBuilder().create();
//        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        String newOrUsed;
//        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//            newOrUsed = "";
//        else
//            newOrUsed = "USED";
//        disclosure.setText(String.format(getString(R.string.used_vehicle_disclosure), newOrUsed));
//        disclosure.setText("Hand to Finance Department");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (checkMandatoryImage())
                iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setDisclosureSkipped(true);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
                    ((NewDealViewPager) getActivity()).setCurrentPage(24);
                } else
                    iGoToNextPage.goToBackIndex();
//                showFragmentInLandscape();
            }
        });
//        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
        cancel.setVisibility(View.INVISIBLE);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                logout();
//            }
//        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

}
