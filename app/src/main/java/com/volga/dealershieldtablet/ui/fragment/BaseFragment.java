package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.jraska.falcon.Falcon;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.customViews.DProgressbar;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;
import com.yariksoffice.lingver.Lingver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class BaseFragment extends Fragment {
    public boolean visibleHintCalled = false;
    public boolean isTaken = false;
    public File file;
    int size = 0;
    private Handler mBackgroundHandler;

    public static Bitmap rotateBitMap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (NewDealViewPager.currentPage > 67 && isVisibleToUser) {
            if (getActivity() != null) {
                if (getActivity() instanceof NewDealViewPager) {
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO))
                        showProgressFor2Sec();
                }
            }
        }
    }

    public void showProgressFor2Sec() {
        final DProgressbar progressDialog = new DProgressbar(getActivity());
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setCancelable(false);
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null && !getActivity().isFinishing())
                    progressDialog.dismiss();
            }
        }, 2500);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        visibleHintCalled = false;
        // Added to avoid fragment recreation
//        setRetainInstance(true);
//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(getActivity(),
//                MainActivity.class));
    }

    public void replaceFragment(int container, Fragment fragment, String tag) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(container, fragment, tag);
        ft.addToBackStack(tag);
        ft.commitAllowingStateLoss();

    }

    public static final int STORAGE_PERMISSION_CODE = 100;

    public static final String TAG = "PERMISSION_TAG";

    public void requestPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            //Android is 11(R) or above
            try {
                Log.d(TAG, "requestPermission: try");

                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                storageActivityResultLauncher.launch(intent);
            }
            catch (Exception e){
                Log.e(TAG, "requestPermission: catch", e);
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                storageActivityResultLauncher.launch(intent);
            }
        }
        else {
            //Android is below 11(R)
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_CODE
            );
        }
    }

    private ActivityResultLauncher<Intent> storageActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    Log.d(TAG, "onActivityResult: ");
                    //here we will handle the result of our intent
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        //Android is 11(R) or above
                        if (Environment.isExternalStorageManager()){
                            //Manage External Storage Permission is granted
//                            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
//                            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
//                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is granted");
//                            createFolder();
                        }
                        else{
                            requestPermission();
                            //Manage External Storage Permission is denied
//                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is denied");
//                            Toast.makeText(MainActivity.this, "Manage External Storage Permission is denied", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        //Android is below 11(R)
                    }
                }
            }
    );
    public boolean checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            //Android is 11(R) or above
            return Environment.isExternalStorageManager();
        }
        else{
            //Android is below 11(R)
            int write = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
        }
    }

    /*Handle permission request results*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE){
            if (grantResults.length > 0){
                //check each permission if granted or not
                boolean write = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean read = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (write && read){
                    //External Storage permissions granted
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permissions granted");
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            takePicture.setVisibility(View.VISIBLE);
//                        }
//                    }, 1500);
                }
                else{
                    //External Storage permission denied
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permission denied");
//                    Toast.makeText(this, "External Storage permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public void addFragment(int container, Fragment fragment, String tag) {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(container, fragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }

    //    private boolean checkWeatherSignedOrNot() {
//        Bitmap signedTransparentBitmap = mSignaturePad.getTransparentSignatureBitmap();
//        Bitmap emptyBitmap = Bitmap.createBitmap(signedTransparentBitmap.getWidth(), signedTransparentBitmap.getHeight(), signedTransparentBitmap.getConfig());
//        if (signedTransparentBitmap.sameAs(emptyBitmap)) {
//            // non signed bitmap.
//            return false;
//        }
//        return true;
//    }
    public boolean checkWeatherSignedOrNot(SignaturePad signaturePad) {
        Bitmap signedTransparentBitmap = signaturePad.getTransparentSignatureBitmap(true);
        Bitmap emptyBitmap = Bitmap.createBitmap(signedTransparentBitmap.getWidth(), signedTransparentBitmap.getHeight(), signedTransparentBitmap.getConfig());
        Log.e("sign height & Width: ", "Height : " + emptyBitmap.getHeight() + " Width : " + emptyBitmap.getWidth());
        // non signed bitmap.
        return !signedTransparentBitmap.sameAs(emptyBitmap);
    }

    @Override
    public void onResume() {
        super.onResume();
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            if (NewDealViewPager.currentPage > 2 && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isBussinessLead() && getActivity() instanceof NewDealViewPager) {
                Timber.tag("Base Fragment: ").e("onResume: Called and redirected to path selection.");
//                Locale myLocale = new Locale("en");
//
//                Resources res = getResources();
//                DisplayMetrics dm = res.getDisplayMetrics();
//                Configuration conf = res.getConfiguration();
//                conf.setLocale(myLocale);
//                res.updateConfiguration(conf, dm);
                new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                        .setMessage("As 12 hours completed this deal marked as Lead, the deal will be automatically uploaded and will appear in the \"Lead\" reports in your DealerXT account.")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Lingver.getInstance().setLocale(getActivity(), "en");
                                callHomeActivity();
                            }
                        }).show().setCancelable(false);

            }
        }else {
//            Lingver.getInstance().setLocale(getActivity(), "en");
//            callHomeActivity();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBackgroundHandler != null) {
            mBackgroundHandler.getLooper().quitSafely();
            mBackgroundHandler = null;
        }
    }

    public void goBack() {
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStack();
    }

    public int getPageIndex(String uniqueDName) {
        if (uniqueDName == null || uniqueDName.length() == 0) {
            return 0;
        }
        if (uniqueDName.equalsIgnoreCase("Vehicle Type Used/New")) {
            return 0;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle VIN Scanner Capture")) {
            return 1;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle info after Scan")) {
            return 2;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Front of Vehicle")) {
            return 3;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Front of Vehicle")) {
            return 4;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Passenger Side")) {
            return 5;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Passenger Side")) {
            return 6;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Back Vehicle")) {
            return 7;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Back Vehicle")) {
            return 8;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Driver Side")) {
            return 9;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Driver Side")) {
            return 10;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo  Buyers Guide")) {
            return 11;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Buyers Guide")) {
            return 12;
        } else if (uniqueDName.equalsIgnoreCase("Add More Vehicle Images")) {
            return 13;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional Photo 1")) {
            return 14;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 1")) {
            return 15;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 2")) {
            return 16;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 2")) {
            return 17;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 3")) {
            return 18;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 3")) {
            return 19;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 4")) {
            return 20;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 4")) {
            return 21;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 5")) {
            return 22;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 5")) {
            return 23;
        } else if (uniqueDName.equalsIgnoreCase("Skip Disclosures")) {
            return 24;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle Condition Disclosure Capture")) {
            return 25;
        }  else if (uniqueDName.equalsIgnoreCase("New Vehicle History Disclosure Capture")) {
            return 25;
        }else if (uniqueDName.equalsIgnoreCase("Vehicle Use Disclosure Capture")) {
            return 26;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle Third Party Reports Capture")) {
            return 27;
        } else if (uniqueDName.equalsIgnoreCase("Save deal or continue deal")) {
            return 28;
        } else if (uniqueDName.equalsIgnoreCase("Salesperson Selection Dropdown")) {
            return 29;
        } else if (uniqueDName.equalsIgnoreCase("Odometer Photo Capture")) {
            return 30;
        } else if (uniqueDName.equalsIgnoreCase("Odometer Photo Preview")) {
            return 31;
        } else if (uniqueDName.equalsIgnoreCase("Manual Vehicle Odometer Capture")) {
            return 32;
        } else if (uniqueDName.equalsIgnoreCase("Is There Trade-in Capture")) {
            return 33;
        } else if (uniqueDName.equalsIgnoreCase("Scan Trade-in Capture")) {
            return 34;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Info After Scan")) {
            return 35;
        } else if (uniqueDName.equalsIgnoreCase("Capture Front Photo of Trade-in")) {
            return 36;
        } else if (uniqueDName.equalsIgnoreCase("Preview Front Photo of Trade-in")) {
            return 37;
        } else if (uniqueDName.equalsIgnoreCase("Capture License Plate Photo of Trade-in")) {
            return 38;
        } else if (uniqueDName.equalsIgnoreCase("Preview License Plate Photo of trade-in")) {
            return 39;
        } else if (uniqueDName.equalsIgnoreCase("Capture Odometer Photo of Trade-in")) {
            return 40;
        } else if (uniqueDName.equalsIgnoreCase("Preview Odometer Photo of Trade-in")) {
            return 41;
        } else if (uniqueDName.equalsIgnoreCase("Manual Trade-in Odometer Capture")) {
            return 42;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Add More Images")) {
            return 43;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 1")) {
            return 44;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 1")) {
            return 45;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 2")) {
            return 46;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 2")) {
            return 47;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 3")) {
            return 48;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 3")) {
            return 49;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 4")) {
            return 50;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 4")) {
            return 51;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 5")) {
            return 52;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 5")) {
            return 53;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Front Capture")) {
            return 54;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Front Capture")) {
            return 54;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Front Preview")) {
            return 55;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Front Preview")) {
            return 55;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Scan Back Capture")) {
            return 56;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Scan Back Capture")) {
            return 56;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Scanned Back Result")) {
            return 57;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Scanned Back Result")) {
            return 57;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Info Confirmation")) {
            return 58;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Info Confirmation")) {
            return 58;
        } else if (uniqueDName.equalsIgnoreCase("Is There Co-buyer?")) {
            return 59;
        } else if (uniqueDName.equalsIgnoreCase("Continue F&I Different Tablet?")) {
            return 60;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle Summary Review Dealer")) {
            return 61;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Summary Dealer")) {
            return 62;
        } else if (uniqueDName.equalsIgnoreCase("Finalize Disclosures?")) {
            return 63;
        } else if (uniqueDName.equalsIgnoreCase("Proof of Insurance Photo?")) {
            return 64;
        } else if (uniqueDName.equalsIgnoreCase("Proof of Insurance Photo Capture")) {
            return 65;
        } else if (uniqueDName.equalsIgnoreCase("Proof of Insurance Photo Preview")) {
            return 66;
        } else if (uniqueDName.equalsIgnoreCase("Handover Tablet to Buyer")) {
            return 67;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Language Selection")) {
            return 68;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Language Selection")) {
            return 68;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Disclosures")) {
            return 69;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Disclosures")) {
            return 69;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Confirm Vehicle Photos")) {
            return 70;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Confirm Vehicle Photos")) {
            return 70;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Confirm vehicle details")) {
            return 71;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Confirm vehicle details")) {
            return 71;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Test Drive Screen")) {
            return 72;
        } else if (uniqueDName.equalsIgnoreCase("Buyer New Vehicle Test Drive Confirmation")) {
            return 72;
        }else if (uniqueDName.equalsIgnoreCase("Co-Buyer Test Drive Screen")) {
            return 72;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer New Vehicle Test Drive Confirmation")) {
            return 72;
        }  else if (uniqueDName.equalsIgnoreCase("Buyer Buyers Guide")) {
            return 73;
        } else if (uniqueDName.equalsIgnoreCase("Buyer MSRP/Addendum Sticker Authorization")) {
            return 73;
        }else if (uniqueDName.equalsIgnoreCase("Co-Buyer Buyers Guide")) {
            return 73;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer MSRP/Addendum Sticker Authorization")) {
            return 73;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Vehicle History Disclosure")) {
            return 74;
        } else if (uniqueDName.equalsIgnoreCase("Buyer New Vehicle History Disclosure")) {
            return 74;
        }else if (uniqueDName.equalsIgnoreCase("Co-Buyer Vehicle History Disclosure")) {
            return 74;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer New Vehicle History Disclosure")) {
            return 74;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Vehicle Use Disclosure")) {
            return 75;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Vehicle Use Disclosure")) {
            return 75;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Third Party Report Confirmation")) {
            return 76;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Third Party Report Confirmation")) {
            return 76;
        } else if (uniqueDName.equalsIgnoreCase("Buyer No Verbal Promises")) {
            return 77;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer No Verbal Promises")) {
            return 77;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Almost Done")) {
            return 78;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Almost Done")) {
            return 78;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Document Checklist")) {
            return 79;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Document Checklist")) {
            return 79;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Final Approval")) {
            return 80;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Final Approval")) {
            return 80;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Contact Info")) {
            return 81;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Contact Info")) {
            return 81;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Selection")) {
            return 82;
        } else if (uniqueDName.equalsIgnoreCase("Final Thank You Page Deal Completed")) {
            return 82;
        }
        /*Trade-In License Plate Image Preview*/
        return 0;
    }

    public void cancel() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Checking save access...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);


        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setEditing(false);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
            if (DSAPP.getInstance().isNetworkAvailable()) {
                RetrofitInitialization.okHttpClient.build().dispatcher().cancelAll();
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0"))
                    getAccess(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)), progressDialog);
                else {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDeal(true);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDealTextSynced(false);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDealSynced(false);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    String format = "yyyy-MM-dd HH:mm:ss";
                    final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                    long dateInMillis = System.currentTimeMillis();
                    sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                    String dateString = sdf.format(new Date(dateInMillis));
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setDealStatus("Syncing");
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setEditing(false);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    Lingver.getInstance().setLocale(requireActivity(), "en");
                    progressDialog.dismiss();
                    callHomeActivity();
                }
            } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().length() == 0 || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0")) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                String format = "yyyy-MM-dd HH:mm:ss";
                final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                long dateInMillis = System.currentTimeMillis();
                sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                String dateString = sdf.format(new Date(dateInMillis));
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setEditing(false);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                Lingver.getInstance().setLocale(requireActivity(), "en");
                progressDialog.dismiss();
                callHomeActivity();
            } else {
                progressDialog.dismiss();
                new CustomToast(getActivity()).alert("Please connect to the internet to sync incomplete deal.");
            }
        }
    }

    private void getAccess(Record where, final ProgressDialog progressDialog) {

        RetrofitInitialization.getDs_services().getSaveAccess("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), where.getSettingsAndPermissions().getCustomerVehicleTradeId(), PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "").enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                progressDialog.dismiss();
                if (response.code() == 200) {
                    try {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDeal(true);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setEditing(false);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDealTextSynced(false);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setIncompleteDealSynced(false);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setDealStatus("Syncing");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                            String format = "yyyy-MM-dd HH:mm:ss";
                            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
                            long dateInMillis = System.currentTimeMillis();
                            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
                            String dateString = sdf.format(new Date(dateInMillis));
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        }
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    }
//                    Locale myLocale = new Locale("en");
//                    Resources res = getResources();
//                    DisplayMetrics dm = res.getDisplayMetrics();
//                    Configuration conf = res.getConfiguration();
//                    conf.locale = myLocale;
//                    res.updateConfiguration(conf, dm);

                    Lingver.getInstance().setLocale(getActivity(), "en");
                    callHomeActivity();

                } else {
                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
                                new CustomToast(getActivity()).toast(arr[1]);
                                Log.e("Logout: ", "logout from base during Inactive" + json.toString());
                                logoutInBase();
                            } else {
                                if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                    callRefreshTokenAPI();
                                } else {
                                    Log.e("Logout: ", "logout from base during refresh token length was 0: " + json.toString());
                                    logoutInBase();
                                }
                            }

                        } else {
                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            callHomeActivity();
                                            //Yes button clicked
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            dialog.dismiss();
                                            //No button clicked
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(json.getString("Message")).setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();
//                            new CustomToast(getActivity()).toast(json.getString("Message"));

                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Response ", "onResponse: " + json);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
        String path_licence, path_signature, path_car, path_user_pic, path_screenshots;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
        path_user_pic = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/UserPic";
        path_screenshots = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Screenshot";


        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        }

        /*Car Images*/
        if (new File(path_car + "/IMG_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();

            synced.setName("FRONTIMG");
            synced.setFile(new File(path_car + "/IMG_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        }
        if (new File(path_car + "/IMG_right.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("RIGHTIMG");
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_right.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        }
        if (new File(path_car + "/IMG_rear.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("REARIMG");
            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        }
        if (new File(path_car + "/IMG_left.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LEFTIMG");
            synced.setFile(new File(path_car + "/IMG_left.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        }
        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("WINDOWWSTICKERIMG");
            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);

//            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        }
        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("MILEAGEIMG");
            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TRADEINMILEAGE");
            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        }
        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LICENCEPLATENUMBER");
            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/

        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("Customer_Insurance");
            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
            synced.setName("language_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_1.svg"));
            synced.setName("third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg"));
            synced.setName("co_buyer_third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_2.svg"));
            synced.setName("third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg"));
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
            synced.setName("type_of_purchase_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
            synced.setName("confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
            synced.setName("test_drive_taken_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
            synced.setName("no_test_drive_confirm_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
            synced.setName("remove_stickers_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
            synced.setName("CONTACTINFOSIGNATUREIMG");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        }
        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
            synced.setName("prints_recieved_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure");
            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
            synced.setName("history_report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        }
        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure");
            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_language_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_type_of_purchase_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
            synced.setName("co_buyer_confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_test_drive_taken_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_no_test_drive_confirm_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_remove_stickers_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_contact_info");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_prints_recieved_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
            synced.setName("co_buyer_condition_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {

            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_history_report");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
            synced.setName("co_buyer_history_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_no_verble_promise.svg"));
            synced.setName("buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg"));
            synced.setName("co_buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_checklist.svg"));
            synced.setName("co_buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_checklist.svg"));
            synced.setName("buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_DLFRONT");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("DRIVERLICENSEIMG");
            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TradeInFront");
            synced.setFile(new File(path_car + "/IMG_trade_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }

        /*Buyers screenshots*/
        if (new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_co_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_safety_recall.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_safety_recall");
            synced.setFile(new File(path_screenshots + "/IMG_alert_safety_recall.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_warning_disclosures");
            synced.setFile(new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_vehicle_info_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_co_buyer_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        if (new File(path_screenshots + "/IMG_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers screenshots*/

        if (new File(path_screenshots + "/IMG_co_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_co_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


//        /*Buyers pictures*/ third_party_sign_CARFAX_Report_pic

        if (new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }if (new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers picture*/

        if (new File(path_user_pic + "/IMG_co_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        return imagesSynceds;
    }

    public void takeScreenshotForAlert(File mPath) {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
//            String mPath = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM).toString() + "/" + now + ".jpg";

//            View v1 = alert.getWindow().getDecorView().getRootView();
//
//            v1.setDrawingCacheEnabled(true);
//            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
//            v1.setDrawingCacheEnabled(false);
////            File imageFile = new File(mPath);
//            FileOutputStream outputStream = new FileOutputStream(mPath);
//            int quality = 100;
//            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
//            outputStream.flush();
//            outputStream.close();

//            View v1 = getActivity().getWindow().getDecorView().getRootView();
//            v1.setDrawingCacheEnabled(true);
//            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
//            v1.setDrawingCacheEnabled(false);
//
//            FileOutputStream outputStream = new FileOutputStream(mPath);
//            int quality = 100;
//            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
//            outputStream.flush();
//            outputStream.close();
            Falcon.takeScreenshot(getActivity(), mPath);

//            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    public void savePageNumber() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            String format = "yyyy-MM-dd HH:mm:ss";
            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            long dateInMillis = System.currentTimeMillis();
            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
            String dateString = sdf.format(new Date(dateInMillis));
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }
//        getActivity().getSupportFragmentManager().popBackStack();
    }

    public void logout() {
        showAlertDialog();

    }

    public boolean isPhotoAvailable() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        boolean isPhotoAvailable;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())
            isPhotoAvailable = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
        else isPhotoAvailable = !PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS);
        return isPhotoAvailable;
    }

    public int decidePageNumber() {
        ArrayList<Integer> integers = new ArrayList<>();
        int page = 13;
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
            ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
            ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
            boolean isPhotoAvailable = isPhotoAvailable();
            if (!isPhotoAvailable) {
                page = 12;
            }
            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 11;
                    } else
                        page = 12;
                }
                if (report != null && report.get(report.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 11;
                    } else
                        page = 12;
                }
                if (history != null && history.get(history.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 11;
                    } else
                        page = 12;
                }
                if (condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 10;
                    } else
                        page = 11;
                } else {
                    if (history == null) {
                        if (!isPhotoAvailable) {
                            page = 10;
                        } else
                            page = 11;
                    }
                }
                if (report != null && report.get(report.size() - 1).isChecked() && condition != null && condition.get(condition.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 10;
                    } else
                        page = 11;
                }
                if (report != null && report.get(report.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 10;
                    } else
                        page = 11;
                }

                if (condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked() && report != null && report.get(report.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 9;
                    } else
                        page = 10;
                } else {
                    if (history == null && report == null) {
                        if (!isPhotoAvailable) {
                            page = 9;
                        } else
                            page = 10;
                    }
                }
            } else {
                if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                    if (!isPhotoAvailable) {
                        page = 9;
                    } else
                        page = 10;
                } else {
                    if (!isPhotoAvailable) {
                        page = 10;
                    } else
                        page = 11;
                }
            }
        }
        return page;
    }

    public void logoutInBase() {
        if (getActivity() instanceof NewDealViewPager && NewDealViewPager.currentPage > 0) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());

            String format = "yyyy-MM-dd HH:mm:ss";
            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            long dateInMillis = System.currentTimeMillis();
            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
            String dateString = sdf.format(new Date(dateInMillis));
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }
        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
        PreferenceManger.putString(AppConstant.UNIQU_TAB_OBJECT, "");
        PreferenceManger.putString(AppConstant.SALES_PERSON, "");
        PreferenceManger.putString(AppConstant.CHECK_LIST, "");
        PreferenceManger.putString(AppConstant.VERBAL_PROMISE, "");
        PreferenceManger.putString(AppConstant.THIRD_PARTIES, "");
        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
        PreferenceManger.putString(AppConstant.ALERT, "");
//        Locale myLocale = new Locale("en");
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
        if (getActivity() != null && !getActivity().isFinishing()) {
            Lingver.getInstance().setLocale(getActivity(), "en");
            callHomeActivity();
        }
    }

    public void callHomeActivity() {
        if (getActivity() != null && !getActivity().isFinishing())
            getActivity().finishAffinity();
        Intent intent = new Intent(getActivity(), MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        if (getActivity() != null)
            getActivity().finish();
    }

    public void next(Fragment fragment) {
        replaceFragment(R.id.container, fragment, fragment.getClass().getSimpleName());
    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    Log.e("Refresh: ", "refreshed ");
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Checking save access...");
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    getAccess(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)), progressDialog);
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Logout: ", "logout from base during refresh" + json.toString());
                        logoutInBase();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
            }
        });
    }

    public void playGIF(ImageView imageView) {
        Glide.with(DSAPP.getContext())
                .asGif()
                .load(R.drawable.ali3_gf) // Replace with a valid url
                .addListener(new RequestListener<GifDrawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        resource.setLoopCount(2); // Place your loop count here.
                        return false;
                    }
                })
                .into(imageView);
    }

    public void playSound(int selectedLanguage) {
        final MediaPlayer mp;

        ArrayList<Integer> numbers = new ArrayList();

        if (selectedLanguage == 3) {
            for (int i = 0; i < 9; i++) {
                numbers.add(i + 1);
            }
            Collections.shuffle(numbers);

//        int random = new Random().nextInt(10) + 1;
            int random = numbers.get(0);
            String soundname = "es_" + random;
            int res_sound_id = getActivity().getResources().getIdentifier(soundname, "raw", getActivity().getPackageName());
            Uri u = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + res_sound_id);
            Log.e("URI: + Random ", u.getPath() + " Random " + random);
            mp = MediaPlayer.create(getActivity(), u);
        } else {
            for (int i = 0; i < 7; i++) {
                numbers.add(i + 1);
            }
            Collections.shuffle(numbers);

//        int random = new Random().nextInt(10) + 1;
            int random = numbers.get(0);
            String soundname = "en_" + random;
            int res_sound_id = getActivity().getResources().getIdentifier(soundname, "raw", getActivity().getPackageName());
            Uri u = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + res_sound_id);
            Log.e("URI: + Random ", u.getPath() + " Random " + random);
            mp = MediaPlayer.create(getActivity(), u);
        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.reset();
                mp.release();
                mp = null;
            }
        });
        mp.start();
    }

    public void playSoundALI3() {
//        if (PreferenceManger.getBooleanValue(AppConstant.IS_AI_ENABLED)) {
        final MediaPlayer mp;
        int res_sound_id = DSAPP.getInstance().getResources().getIdentifier("ali3", "raw", DSAPP.getInstance().getPackageName());
        Uri u = Uri.parse("android.resource://" + DSAPP.getInstance().getPackageName() + "/" + res_sound_id);
        mp = MediaPlayer.create(DSAPP.getInstance(), u);
//        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.reset();
                mp.release();
                mp = null;
            }
        });
        mp.start();
//        }
    }

    public void ali3Logic(ImageView imageView) {
        if (imageView!=null) {
            if (PreferenceManger.getBooleanValue(AppConstant.IS_AI_ENABLED)) {
                playGIF(imageView);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        playSoundALI3();
                    }
                }, 1000);
            }
        }
    }

    public String getCurrentTimeString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.getDefault());
        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.getDefault());
        SimpleDateFormat outputFormatTime = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.getDefault());
//        Date dateValue = new Date(Calendar.getInstance().getTime().toString());
//        TimeZone pstTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
//        outputFormatTime.setTimeZone(pstTimeZone);
        outputFormatTime.setTimeZone(TimeZone.getTimeZone("PST"));
//        outputFormatTime.setTimeZone(TimeZone.getDefault());
        Date parsedDate = null;
        //            parsedDate = dateFormat.parse(dateValue.toString());
        long longs = TimeZoneUtils.convertTime(Calendar.getInstance().getTimeInMillis(), TimeZone.getTimeZone("UTC"), TimeZone.getTimeZone("America/Los_Angeles"));
        String returnDate = outputFormatTime.format(longs);
        returnDate = returnDate + " PST";
//        returnDate = returnDate.replace("a. m.", "AM");
//        returnDate = returnDate.replace("p. m.", "PM");
//        returnDate = returnDate.replace("a.m.", "AM");
//        returnDate = returnDate.replace("p. m.", "PM");
//        returnDate = returnDate.replace("GMT", "PST");
//        returnDate = returnDate.replace("PDT", "PST");
//        returnDate = returnDate.replace("+00:00", "");

        return returnDate;


//        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a zzz", Locale.getDefault());
//        /*yyyy-MM-dd HH:mm:ss*/
//        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
//        String time = "";
//        try {
//            Log.e("Current Time: ", tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString())));
//            time = tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return time;
//        return "";
    }

    private void showAlertDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);
        final DSTextView logout = promptView.findViewById(R.id.userFName);
        DSTextView role = promptView.findViewById(R.id.role);
        role.setVisibility(View.GONE);
        role.setText(PreferenceManger.getStringValue(AppConstant.USER_ROLE));
        logout.setText(PreferenceManger.getStringValue(AppConstant.USER_NAME));
        DSTextView btnAdd1 = promptView.findViewById(R.id.yes);

        DSTextView btnAdd2 = promptView.findViewById(R.id.no);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                logoutInBase();

            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (getActivity() != null && !getActivity().isFinishing() && alertD.isShowing()) {
                    alertD.dismiss();
                }

            }
        });

        alertD.setView(promptView);

        alertD.show();
    }

    public int getCorrectAngleBitmap(String photoPath) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        int rotation = 0;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotation = 90;
//                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotation = 180;
//                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotation = 270;
//                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
                rotation = 0;
            default:
                rotation = 0;
//                rotatedBitmap = bitmap;
        }
        return rotation;
    }

    public void optimizeImage(final File file, final byte[] data) {

//        getBackgroundHandlerCpy().post(new Runnable() {
//            @Override
//            public void run() {
        int angle = getCorrectAngleBitmap(file.getPath());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap imagemOriginal = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        Matrix rotationMatrix = new Matrix();
        rotationMatrix.postRotate(angle);
        int height = 0;
        int width = 0;
        if (angle == 0) {
            width = imagemOriginal.getWidth();
            height = (int) (imagemOriginal.getHeight() / 1.8);
        } else if (angle == 90) {
            width = (int) (imagemOriginal.getWidth() / 1.8);
            height = imagemOriginal.getHeight();
        }
        Bitmap imagemCortada = Bitmap.createBitmap(imagemOriginal, 0, 0, width, height, rotationMatrix, false);
        imagemOriginal.recycle();
        // Scale down to the output size
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(imagemCortada, imagemOriginal.getWidth(), (imagemOriginal.getHeight()), true);
        if (!imagemCortada.isRecycled()) {
            imagemCortada.recycle();
            imagemCortada = null;
        }
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file.getAbsoluteFile());
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                String log = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getLocalLogs();
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLocalLogs(log + "Scan DMV Front: Error in optimizing Front Image" + e.getLocalizedMessage() + "\n");
            }
        }
//            }
//        });

    }

    public Handler getBackgroundHandlerCpy() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    public void clearDMVPreference() {
        PreferenceManger.putString("Customer_First_Name", "");
        PreferenceManger.putString("Customer_Middle_Name(s)", "");
        PreferenceManger.putString("Customer_Family_Name", "");
        PreferenceManger.putString("Date_of_Birth", "");
        PreferenceManger.putString("Address1", "");
        PreferenceManger.putString("City", "");
        PreferenceManger.putString("Postal_Code", "");
        PreferenceManger.putString("State", "");
        PreferenceManger.putString("Country", "");
        PreferenceManger.putString("ID_Number", "");
        PreferenceManger.putString("Document_Expiration_Date", "");
        PreferenceManger.putString("Gender", "");
    }

    public void showFragmentInLandscape() {
        Activity a = getActivity();
        if (a != null) {
            a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    public void showSensorOrientation() {
        Activity a = getActivity();
        if (a != null) {
            a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        }
    }

    public void showFragmentInPortrait() {
        Activity a = getActivity();
        if (a != null) {
            a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

    }

    public void showDialogCar(String type) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("url", "FromCarImagesSample");
        bundle.putString("SampleName", type);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    public void saveLogsInToDB(String logsData) {
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            String log = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getLocalLogs();
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLocalLogs(log + " " + Calendar.getInstance().getTime().toString() + " " + logsData);
        }
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
    }

    public void analyseTheImages() {

        String path_licence, path_signature, path_car;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (!new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            saveLogsInToDB("Buyer's DMV Front Image is missing.\n");

        }
        if (record.getSettingsAndPermissions().isHasCoBuyer() && !new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            saveLogsInToDB("Co-Buyer's DMV Front Image is missing.\n");
        }
        if (!new File(path_car + "/IMG_front.jpg").exists()) {
//
            saveLogsInToDB("Vehicle Front Image is missing.\n");
        }
        if (!new File(path_car + "/IMG_right.jpg").exists()) {
            saveLogsInToDB("Vehicle Passengers side Image is missing.\n");
        }
        if (!new File(path_car + "/IMG_rear.jpg").exists()) {
            saveLogsInToDB("Vehicle Back Image side is missing.\n");
        }
        if (!new File(path_car + "/IMG_left.jpg").exists()) {
            saveLogsInToDB("Vehicle Left Image side is missing.\n");
        }
        if (!new File(path_car + "/IMG_sticker.jpg").exists()) {
            saveLogsInToDB("Vehicle Addendum Sticker/Buyers Guide Image side is missing.\n");
        }
        if (!new File(path_car + "/IMG_mileage.jpg").exists()) {
            saveLogsInToDB("Vehicle Odometer Image side is missing.\n");
        }
        if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            saveLogsInToDB("Trade-In Vehicle Odometer Image is missing.\n");
        }
        if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            saveLogsInToDB("Trade-In Vehicle Licence Plate Image side is missing.\n");
        }
        saveLogsInToDB("All the validations are noted when the deals is marked as complete and ready to sync with server.\n");

    }

    public void scrollableScreenshot(LinearLayout ll_linear, File file) {
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_SCREENSHOT)) {
            Bitmap bitmap1 = loadBitmapFromView(ll_linear, ll_linear.getWidth(), ll_linear.getHeight());
            saveBitmap(bitmap1, file);
        }
    }

    public void disableNext(DSTextView next) {
        next.setEnabled(false);
        next.setClickable(false);
    }

    public void enableNext(DSTextView next) {
        next.setEnabled(true);
        next.setClickable(true);
    }

    public void disableNext(Button next) {
        next.setEnabled(false);
        next.setClickable(false);
    }

    public void enableNext(Button next) {
        next.setEnabled(true);
        next.setClickable(true);
    }

    public void disableWithGray(DSTextView next) {
        if (getActivity()!=null) {
            next.setBackground(AppCompatDrawableManager.get().getDrawable(requireActivity(),R.drawable.rounded_corner_next_gray));
            next.setClickable(false);
            next.setPadding(0, 15, 0, 15);
            next.setEnabled(false);
        }

    }

    public void enableWithGreen(DSTextView next) {
        if (getActivity()!=null) {
            next.setBackground(AppCompatDrawableManager.get().getDrawable(requireActivity(),R.drawable.rounded_corner_green));
            next.setPadding(0, 15, 0, 15);
            next.setClickable(true);
            next.setEnabled(true);
        }
    }

    public void disableDelete(ImageView delete) {
        delete.setImageResource(R.drawable.ic_delete_black_24dp);
//        delete.setClickable(false);
//        delete.setEnabled(false);
//        delete.setOnClickListener(null);
    }

    public void enableDelete(ImageView delete) {
        delete.setImageResource(R.drawable.delete_red);
//        delete.setClickable(true);
//        delete.setEnabled(true);

    }

    public boolean hasFlash() {
        return getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void saveBitmap(Bitmap bitmap, File fileName) {

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(fileName);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
//            Toast.makeText(getApplicationContext(),imagePath.getAbsolutePath()+"", Toast.LENGTH_SHORT).show();
//            boolean_save = true;
//
//            btn_screenshot.setText("Check image");

            Log.e("ImageSave: ", " SaveImage");
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
    }

    public File getOutputFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


}
