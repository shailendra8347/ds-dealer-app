package com.volga.dealershieldtablet.ui.scanner;
//Comment to commit
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.RectF;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.manateeworks.BarcodeScanner;
import com.manateeworks.CameraManager;
import com.manateeworks.MWOverlay;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


class Preview extends RelativeLayout {
    private final String TAG = "Preview";

    AppCompatActivity mContext;
    ViewGroup rlCameraContainer;

    private ArrayList<RectF> rects;
    private boolean USE_AUTO_RECT = true;

    SurfaceHolder surfaceHolder;

    public Preview(Context context) {
        super(context);
        init();
    }

    public Preview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Preview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public enum OverlayMode {
        OM_IMAGE, OM_MWOVERLAY, OM_NONE
    }

    public static final OverlayMode OVERLAY_MODE = OverlayMode.OM_MWOVERLAY;


    public void init() {
        mContext = (AppCompatActivity) getContext();
        rlCameraContainer = this;
        startScannerView();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        if (changed) {
            partialViewWidth = r;
            partialViewHeight = b;

            refreshScannerViewUI();
        }

    }

    double partialViewWidth = 0;
    double partialViewHeight = 0;
    ImageView overlayImage;
    RelativeLayout rlFullScreen;
    RelativeLayout rlSurfaceContainer;
    ScrollView scrollView;
    SurfaceView surfaceView;
    ProgressBar pBar;


    private void refreshScannerViewUI() {
        if (partialViewWidth == 0 || partialViewHeight == 0)
            return;

        if (rlFullScreen != null) {
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            final Point size = new Point();
            display.getSize(size);


            final float screenAR = (float) size.y / (float) size.x;

            final int cameraPreviewHeight;
            final int cameraPreviewWidth;
            if (partialViewWidth * screenAR >= partialViewHeight) {
                cameraPreviewHeight = (int) Math.round(partialViewWidth * screenAR);
                cameraPreviewWidth = (int) Math.round(partialViewWidth);
            } else {
                cameraPreviewWidth = (int) Math.round(partialViewHeight / screenAR);
                cameraPreviewHeight = (int) Math.round(partialViewHeight);
            }

            final float finalCameraHeight = cameraPreviewHeight;
            final float finalCameraWidth = cameraPreviewWidth;


            LayoutParams lps = new LayoutParams((int) Math.round(partialViewWidth), (int) Math.round(partialViewHeight));

            scrollView.setLayoutParams(lps);

            rlSurfaceContainer.setLayoutParams(
                    new FrameLayout.LayoutParams(Math.round(cameraPreviewWidth), Math.round(cameraPreviewHeight)));
            surfaceView.setLayoutParams(new LayoutParams(Math.round(cameraPreviewWidth), Math.round(cameraPreviewHeight)));


            new Timer().schedule(new TimerTask() {

                @Override
                public void run() {
                    mContext.runOnUiThread(new Runnable() {
                        public void run() {
                            if (rlFullScreen != null) {
                                setAutoRect();
                                scrollView.scrollTo((int) (finalCameraWidth / 2 - partialViewWidth / 2), (int) (finalCameraHeight / 2 - partialViewHeight / 2));

                                if (OVERLAY_MODE == OverlayMode.OM_IMAGE) {
                                    if (overlayImage == null) {
                                        overlayImage = new ImageView(mContext);
                                        overlayImage.setScaleType(ImageView.ScaleType.FIT_XY);
                                        overlayImage.setImageResource(mContext.getResources().getIdentifier(
                                                "overlay_mw", "drawable", mContext.getPackageName()));
                                        rlSurfaceContainer.addView(overlayImage);
                                    }

                                    LayoutParams lps2 = new LayoutParams((int) Math.round(cameraPreviewWidth),
                                            (int) Math.round(cameraPreviewHeight));
                                    lps2.topMargin = (int) Math.round(finalCameraHeight / 2 - cameraPreviewHeight / 2);
                                    lps2.width = (int) Math.round(cameraPreviewWidth);
                                    lps2.height = (int) Math.round(cameraPreviewHeight);
                                    lps2.topMargin = (int) Math.round(finalCameraHeight / 2 - cameraPreviewHeight / 2);
                                    overlayImage.setLayoutParams(lps2);

                                    overlayImage.setVisibility(View.VISIBLE);

                                } else if (overlayImage != null) {
                                    overlayImage.setVisibility(View.GONE);
                                }

                                if (OVERLAY_MODE == OverlayMode.OM_MWOVERLAY) {
                                    MWOverlay.removeOverlay();
                                    MWOverlay.addOverlay(mContext, surfaceView);
                                    MWOverlay.setPaused(mContext, false);


                                }

                                rlFullScreen.requestLayout();
                                rlSurfaceContainer.setVisibility(View.VISIBLE);

                            }
                        }
                    });
                }
            }, 300);
        }
}

    private void startScannerView() {
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            if (rlFullScreen == null) {

                MWOverlay.setPaused(mContext, false);

                CameraManager.init(mContext,true);

                rlFullScreen = new RelativeLayout(mContext);
                rlSurfaceContainer = new RelativeLayout(mContext);
                scrollView = new ScrollView(mContext);
                scrollView.setFillViewport(true);
                scrollView.setVerticalScrollBarEnabled(false);
                scrollView.setOnTouchListener(new OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
                scrollView.setVisibility(View.INVISIBLE);
                surfaceView = new SurfaceView(mContext);
                pBar = new ProgressBar(mContext);
                LayoutParams pBarParams = new LayoutParams(
                        LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                pBarParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                pBar.setLayoutParams(pBarParams);
                pBar.setVisibility(View.VISIBLE);

                rlFullScreen.setLayoutParams(new WindowManager.LayoutParams(rlCameraContainer.getWidth(), rlCameraContainer
                        .getHeight()/*
                                             * LayoutParams.MATCH_PARENT,
											 * LayoutParams.MATCH_PARENT
											 */));

                rlSurfaceContainer.addView(surfaceView);

                scrollView.addView(rlSurfaceContainer);

                scrollView.setClipToPadding(true);
                rlFullScreen.addView(scrollView);

                rlCameraContainer.addView(rlFullScreen);

                rlFullScreen.addView(pBar);

                surfaceHolder = surfaceView.getHolder();


                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                if (scrollView != null)
                    scrollView.setVisibility(View.VISIBLE);

                pBar.setVisibility(View.GONE);
                refreshScannerViewUI();
            }
        }
    }




    // private methods except the ones that need to be accessed
    private void setAutoRect() {
        if (rlFullScreen != null) {

            float p1x;
            float p1y;

            float p2x;
            float p2y;

            p1x = (float) (surfaceView.getWidth() - scrollView.getWidth()) / 2 / surfaceView.getWidth();
            p1y = (float) (surfaceView.getHeight() - scrollView.getHeight()) / 2 / surfaceView.getHeight();

            p2x = (float) scrollView.getWidth() / surfaceView.getWidth();
            p2y = (float) scrollView.getHeight() / surfaceView.getHeight();

            if (surfaceView.getWidth() < surfaceView.getHeight()) {
                float tmp = p1x;
                p1x = p1y;
                p1y = tmp;
                tmp = p2x;
                p2x = p2y;
                p2y = tmp;

            }

            int[] masks = new int[]{BarcodeScanner.MWB_CODE_MASK_128, BarcodeScanner.MWB_CODE_MASK_25,
                    BarcodeScanner.MWB_CODE_MASK_39, BarcodeScanner.MWB_CODE_MASK_93,
                    BarcodeScanner.MWB_CODE_MASK_AZTEC, BarcodeScanner.MWB_CODE_MASK_DM,
                    BarcodeScanner.MWB_CODE_MASK_EANUPC, BarcodeScanner.MWB_CODE_MASK_PDF,
                    BarcodeScanner.MWB_CODE_MASK_QR, BarcodeScanner.MWB_CODE_MASK_RSS,
                    BarcodeScanner.MWB_CODE_MASK_CODABAR, BarcodeScanner.MWB_CODE_MASK_DOTCODE,
                    BarcodeScanner.MWB_CODE_MASK_11, BarcodeScanner.MWB_CODE_MASK_MSI,
                    BarcodeScanner.MWB_CODE_MASK_MAXICODE, BarcodeScanner.MWB_CODE_MASK_POSTAL};

            if (USE_AUTO_RECT) {

                p1x += 0.02f;
                p1y += 0.02f;
                p2x -= 0.04f;
                p2y -= 0.04f;

                for (int i = 0; i < masks.length; i++) {
                    BarcodeScanner.MWBsetScanningRect(masks[i], p1x * 100, p1y * 100, (p2x) * 100, (p2y) * 100);
                }

            } else {

                if (rects == null) {

                    rects = new ArrayList<RectF>();

                    for (int i = 0; i < masks.length; i++) {
                        rects.add(i, BarcodeScanner.MWBgetScanningRect(masks[i]));
                    }

                } else {

                    for (int i = 0; i < masks.length; i++) {
                        BarcodeScanner.MWBsetScanningRect(masks[i], rects.get(i).left, rects.get(i).top,
                                rects.get(i).right, rects.get(i).bottom);

                    }
                }

                for (int i = 0; i < masks.length; i++) {
                    BarcodeScanner.MWBsetScanningRect(masks[i],
                            (p1x + ((BarcodeScanner.MWBgetScanningRectArray(masks[i])[0] / 100)
                                    * (surfaceView.getWidth() * p2x)) / surfaceView.getWidth()) * 100,
                            (p1y + ((BarcodeScanner.MWBgetScanningRectArray(masks[i])[1] / 100)
                                    * (surfaceView.getHeight() * p2y)) / surfaceView.getHeight()) * 100,
                            (((BarcodeScanner.MWBgetScanningRectArray(masks[i])[2] / 100)
                                    * (surfaceView.getWidth() * p2x)) / surfaceView.getWidth()) * 100,
                            (((BarcodeScanner.MWBgetScanningRectArray(masks[i])[3] / 100)
                                    * (surfaceView.getWidth() * p2y)) / surfaceView.getWidth()) * 100);

                }

            }

        }
    }

}
