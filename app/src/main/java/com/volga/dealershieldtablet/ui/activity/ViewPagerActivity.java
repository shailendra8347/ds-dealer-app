package com.volga.dealershieldtablet.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pushlink.android.PushLink;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.ui.adapter.SectionsPagerAdapter;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.SwipeDirection;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class ViewPagerActivity extends AppCompatActivity implements IGoToNextPage {

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private SingleDirectionViewPager mViewPager;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        ViewPagerActivity.currentPage = currentPage;
        mViewPager.setCurrentItem(currentPage, false);
    }

    public static int currentPage = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pager);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        /*
      The {@link android.support.v4.view.PagerAdapter} that will provide
      fragments for each of the sections. We use a
      {@link FragmentPagerAdapter} derivative, which will keep every
      loaded fragment in memory. If this becomes too memory intensive, it
      may be best to switch to a
      {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

        // Set up the ViewPager with the sections adapter.
        mViewPager = (SingleDirectionViewPager) findViewById(R.id.viewPager);
        registerReceiver(this.broadCastNewMessage, new IntentFilter("notifyData"));
        mViewPager.setAllowedSwipeDirection(SwipeDirection.none);
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), mViewPager);

//        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int oldPos = mViewPager.getCurrentItem();
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

//                if (currentPage == 8) {
//                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setAdapter(mSectionsPagerAdapter);
        if (getIntent() != null) {
            currentPage = getIntent().getIntExtra("page", 0);
//            currentPage=34;
            mViewPager.setCurrentItem(currentPage, false);
        }

    }
    BroadcastReceiver broadCastNewMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("Data update: ", "Data updated from service");
                if (intent.getStringExtra("value").trim().equalsIgnoreCase("logOut")) {
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                    Locale myLocale = new Locale("en");
                    Resources res = getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    conf.locale = myLocale;
                    res.updateConfiguration(conf, dm);
                    callHomeActivity();
                } else {
                    doubleTap = true;
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleTap = false;
                        }
                    }, 5000);
                    new CustomToast(ViewPagerActivity.this).alert(intent.getStringExtra("value"));
                }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        PushLink.setCurrentActivity(this);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadCastNewMessage);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    boolean doubleTap = false;

    @Override
    public void whatNextClick() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleTap = false;
            }
        }, 1200);
        if (!doubleTap) {
            doubleTap = true;
            saveDataOnback();
            if (currentPage > 23) {
                analyseTheImages();

            }
            if (currentPage == 39) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    doubleTap = true;
                    currentPage = 42;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage++;
                    mViewPager.setCurrentItem(currentPage);
                }

            } else if (currentPage == 52) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    doubleTap = true;
                    currentPage = 55;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage++;
                    mViewPager.setCurrentItem(currentPage);
                }
            } else if (currentPage == 58) {
                currentPage = 46;
                mViewPager.setCurrentItem(currentPage, false);
            } else if (currentPage == 10) {
                Gson gson2 = new GsonBuilder().create();
                RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData2 != null && recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    currentPage = 19;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage++;
                    mViewPager.setCurrentItem(currentPage);
                }

            }
//            else if (currentPage == 24) {
//                Gson gson1 = new GsonBuilder().create();
//                RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
//                    currentPage = 10;
//                    mViewPager.setCurrentItem(currentPage, false);
//                }else{
//                    doubleTap = true;
//                    currentPage++;
//                    mViewPager.setCurrentItem(currentPage);
//
//                }
//            }
            else if (currentPage == 57) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                    currentPage = 59;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        currentPage = 59;
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {
                        doubleTap = true;
                        currentPage++;
                        mViewPager.setCurrentItem(currentPage);
                    }
                }
            }
//            else if (currentPage==46){
//                Gson gson = new GsonBuilder().create();
//                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                ArrayList<HistoryListPojo> condition=recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
//                ArrayList<HistoryListPojo> history=recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
//                ArrayList<HistoryListPojo> report=recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
//                if (condition.get(condition.size()-1).isChecked()&&history.get(history.size()-1).isChecked()&&report.get(report.size()-1).isChecked()){
//                    doubleTap = true;
//                    currentPage = 50;
//                    mViewPager.setCurrentItem(currentPage, false);
//                }else if (condition.get(condition.size()-1).isChecked()&&!history.get(history.size()-1).isChecked()&&!report.get(report.size()-1).isChecked()){
//                    doubleTap = true;
//                    currentPage = 50;
//                    mViewPager.setCurrentItem(currentPage, false);
//                }
//            }
            else {
                doubleTap = true;
                currentPage++;
                mViewPager.setCurrentItem(currentPage);
            }
        }
    }

    @Override
    public void goToBackIndex() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleTap = false;
            }
        }, 1200);
        if (!doubleTap) {
            doubleTap = true;
            if (currentPage != 38) {
                saveDataOnback();
            }
            if (currentPage == 0) {
                finish();
            }
            if (mViewPager.getCurrentItem() == 46) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
                    setLocale("en", 45);
                }
            }
            if (currentPage == 42) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    doubleTap = true;
                    currentPage = 39;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage--;
                    mViewPager.setCurrentItem(currentPage);
                }

            } else if (currentPage == 55) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    doubleTap = true;
                    currentPage = 52;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage--;
                    mViewPager.setCurrentItem(currentPage);
                }

            } else if (currentPage == 19) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    doubleTap = true;
                    currentPage = 10;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage--;
                    mViewPager.setCurrentItem(currentPage);
                }

            } else if (currentPage == 35) {
                doubleTap = true;
                currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                mViewPager.setCurrentItem(currentPage, false);
            } else if (currentPage == 45) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                String path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber() + "/CarImages";
                if (!new File(path_car + "/IMG_insurance.jpg").exists()) {
                    currentPage = 42;
                    mViewPager.setCurrentItem(currentPage, false);
                } else {
                    doubleTap = true;
                    currentPage--;
                    mViewPager.setCurrentItem(currentPage);
                }
            } else if (currentPage == 59) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                    doubleTap = true;
                    currentPage--;
                    mViewPager.setCurrentItem(currentPage);
                } else {
                    doubleTap = true;
                    currentPage = 57;
                    mViewPager.setCurrentItem(currentPage, false);
                }

            } else {
                doubleTap = true;
                currentPage--;
                mViewPager.setCurrentItem(currentPage);
            }
        }
    }

    private void saveDataOnback() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String recordDataString;
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(currentPage);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
            recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }
    }

    public void saveLogsInToDB(String logsData) {
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            String log = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getLocalLogs();
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLocalLogs(log + " " + Calendar.getInstance().getTime().toString() + " : PageName: " + getScreenName(currentPage, recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))) + " " + logsData);
        }
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
    }

    public void analyseTheImages() {

        String path_licence, path_signature, path_car;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        if (record != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null) {
            path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
            path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
            path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

            if (!new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
                saveLogsInToDB("Buyer's DMV Front Image is missing.\n");

            }
            if (record.getSettingsAndPermissions().isHasCoBuyer() && !new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
                saveLogsInToDB("Co-Buyer's DMV Front Image is missing.\n");
            }
            if (!new File(path_car + "/IMG_front.jpg").exists()) {
                saveLogsInToDB("Vehicle Front Image is missing.\n");
            }
            if (!new File(path_car + "/IMG_right.jpg").exists()) {
                saveLogsInToDB("Vehicle Passengers side Image is missing.\n");
            }
            if (!new File(path_car + "/IMG_rear.jpg").exists()) {
                saveLogsInToDB("Vehicle Back Image side is missing.\n");
            }
            if (!new File(path_car + "/IMG_left.jpg").exists()) {
                saveLogsInToDB("Vehicle Left Image side is missing.\n");
            }
            if (!new File(path_car + "/IMG_sticker.jpg").exists()) {
                saveLogsInToDB("Vehicle Addendum Sticker/Buyers Guide Image side is missing.\n");
            }
            if (!new File(path_car + "/IMG_mileage.jpg").exists()) {
                saveLogsInToDB("Vehicle Odometer Image  is missing.\n");
            }
            if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
                saveLogsInToDB("Trade-In Vehicle Odometer Image is missing.\n");
            }
            if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_licencePlate.jpg").exists()) {
                saveLogsInToDB("Trade-In Vehicle Licence Plate Image side is missing.\n");
            }
        }
//        saveLogsInToDB("All the validations are noted when the deals is marked as complete and ready to sync with server.\n");

    }

    private String getScreenName(int lastPageIndex, Record record) {
        String pageName = "Default Page";
        switch (lastPageIndex) {
            case 0:
                pageName = "Salesperson info";
                break;
            case 1:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer Contact info";
                } else {
                    pageName = "Co-Buyer Contact info";
                }
                break;
            case 2:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer DMV Front";
                } else {
                    pageName = "Co-Buyer DMV Front";
                }
//                pageName = "DMV Front";
                break;
            case 3:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer DMV Front Preview";
                } else {
                    pageName = "Co-Buyer DMV Front Preview";
                }
//                pageName = "DMV Front Preview";
                break;
            case 4:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer Scan DMV Back";
                } else {
                    pageName = "Co-Buyer Scan DMV Back";
                }
//                pageName = "Scan DMV Back";
                break;
            case 5:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer License info";
                } else {
                    pageName = "Co-Buyer License info";
                }
//                pageName = "License info";
                break;
            case 6:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer License info confirmation";
                } else {
                    pageName = "Co-Buyer License info confirmation";
                }
//                pageName = "License info confirmation";
                break;
            case 7:
                pageName = "Co-Buyer Selection";
                break;
            case 8:
                pageName = "Vehicle Type Used/New";
                break;
            case 9:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle VIN Scanner Screen";
                } else {
                    pageName = "Trade-In Vehicle VIN Scanner Screen";
                }
//                pageName = "Vehicle VIN Scanner Screen";
                break;
            case 10:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle info after Scan";
                } else {
                    pageName = "Trade-In Vehicle info after Scan";
                }
//                pageName = "Vehicle info after Scan";
                break;
            case 11:
                pageName = "Vehicle Front Image";
                break;
            case 12:
                pageName = "Vehicle Front Image Preview";
                break;
            case 13:
                pageName = "Vehicle Passengers Side Image";
                break;
            case 14:
                pageName = "Vehicle Passengers Side Image Preview";
                break;
            case 15:
                pageName = "Vehicle Back Side Image";
                break;
            case 16:
                pageName = "Vehicle Back Side Image Preview";
                break;
            case 17:
                pageName = "Vehicle Drivers Side Image";
                break;
            case 18:
                pageName = "Vehicle Drivers Side Image Preview";
                break;

            case 19:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Window Sticker Image";
                } else {
                    pageName = "Trade-In License Plate Image";
                }
//                pageName = "Vehicle Window Sticker Image";
                break;
            case 20:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Window Sticker Image Preview";
                } else {
                    pageName = "Trade-In License Plate Image Preview";
                }
//                pageName = "Window Sticker Image Preview";
                break;
            case 21:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Odometer Image";
                } else {
                    pageName = "Trade-In Vehicle Odometer Image";
                }
//                pageName = "Odometer Image";
                break;
            case 22:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Odometer Image Preview";
                } else {
                    pageName = "Trade-In Vehicle Odometer Image Preview";
                }
//                pageName = "Odometer Image Preview";
                break;
            case 23:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Mileage entry";
                } else {
                    pageName = "Trade-In Vehicle Mileage entry";
                }
//                pageName = "Mileage entry";
                break;
            case 24:

                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image";
                } else {
                    pageName = "Trade-In Vehicle Add More Image";
                }
//                pageName = "Add More Image";
                break;
            case 25:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 1";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 1";
                }
//                pageName = "Add More Image 1";
                break;
            case 26:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 1 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 1 Preview";
                }
//                pageName = "Add More Image 1 Preview";
                break;
            case 27:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 2";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 2";
                }
//                pageName = "Add More Image 2";
                break;
            case 28:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 2 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 2 Preview";
                }
//                pageName = "Add More Image 2 Preview";
                break;
            case 29:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 3";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 3";
                }
//                pageName = "Add More Image 3";
                break;
            case 30:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 3 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 3 Preview";
                }
//                pageName = "Add More Image 3 Preview";
                break;
            case 31:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 4";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 4";
                }
//                pageName = "Add More Image 4";
                break;
            case 32:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 4 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 4 Preview";
                }
//                pageName = "Add More Image 4 Preview";
                break;
            case 33:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 5";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 5";
                }
//                pageName = "Add More Image 5";
                break;
            case 34:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Add More Image 5 Preview";
                } else {
                    pageName = "Trade-In Add More Image 5 Preview";
                }
//                pageName = "Add More Image 5 Preview";
                break;
            case 35:
//                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
//                    pageName = "Vehicle VIN Scanner Screen";
//                } else {
//                    pageName = "";
//                }
                pageName = "Trade-In Selection";
                break;
            case 36:
//                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
//                    pageName = "Vehicle VIN Scanner Screen";
//                } else {
//                    pageName = "";
//                }
                pageName = "Hand Tablet to Finance Vehicle Disclosure";
                break;
            case 37:
                pageName = "Vehicle Summary";
                break;
            case 38:
                pageName = "Trade-In Vehicle Summary";
                break;
            case 39:
                pageName = "Vehicle Condition Disclosure Selection";
                break;
            case 40:
                pageName = "Vehicle History Disclosure Selection";
                break;
            case 41:
                pageName = "Vehicle Third Party Reports Selection";
                break;
            case 42:
                pageName = "Add Insurance Image";
                break;
            case 43:
                pageName = "Capture Insurance Image";
                break;
            case 44:
                pageName = "Insurance Image Preview";
                break;
            case 45:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Handover Screen for Disclosure Signatures";
                } else {
                    pageName = "Co-Buyer Handover Screen for Disclosure Signatures";
                }
//                pageName = "Customer Handover Screen";
                break;
            case 46:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Language Selection Screen";
                } else {
                    pageName = "Co-Buyer Language Selection Screen";
                }
//                pageName = "Language Selection Screen";
                break;
            case 47:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Purchase/Lease Confirmation";
                } else {
                    pageName = "Co-Buyer Purchase/Lease Confirmation";
                }
//                pageName = "Type of Purchase of the vehicle";
                break;
            case 48:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle Image Confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle Image Confirmation";
                }
//                pageName = "Vehicle Image Confirmation Page";
                break;
            case 49:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Make, model, year & mileage confirmation";
                } else {
                    pageName = "Co-Buyer Make, model, year & mileage confirmation";
                }
//                pageName = "Make, model, year & mileage confirmation";
                break;
            case 50:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Test drive confirmation";
                } else {
                    pageName = "Co-Buyer Test drive confirmation";
                }
//                pageName = "Test drive Screen";
                break;
            case 51:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Buyers Guide / New Car/Addendum Sticker(s) Confirmation";
                } else {
                    pageName = "Co-Buyer Buyers Guide / New Car/Addendum Sticker(s) Confirmation";
                }
//                pageName = "Buyers Guide / New Car/Addendum Sticker(s) Screen";
                break;
            case 52:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle Condition Disclosure confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle Condition Disclosure confirmation";
                }
//                pageName = "Vehicle Condition Disclosure confirmation";
                break;
            case 53:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle History Disclosure confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle History Disclosure confirmation";
                }
//                pageName = "Vehicle History Disclosure confirmation";
                break;
            case 54:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle Third Party Report confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle Third Party Report confirmation";
                }
//                pageName = "Vehicle Third Party Report confirmation";
                break;
            case 55:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Thank you page after Disclosure Confirmation";
                } else {
                    pageName = "Co-Buyer Thank you page after Disclosure Confirmation";
                }
//                pageName = "Thank you page after Vehicle Disclosure";
                break;
            case 56:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Final Report Screen";
                } else {
                    pageName = "Co-Buyer Final Report Screen";
                }
//                pageName = "Final Report Screen";
                break;
            case 57:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Print Received Checkbox Selection";
                } else {
                    pageName = "Co-Buyer Print Received Checkbox Selection";
                }
//                pageName = "Print Received Checkbox Selection";
                break;
            case 58:
                pageName = "Co-Buyer Handover Screen";
                break;
            case 59:

                pageName = "Final Thank You Page";
                break;
        }
        return pageName;
    }

    private ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
//        HashMap<String,boolean> stringHashMap=new HashMap<>();
        String path_licence, path_signature, path_car;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("DRIVERLICENSEIMG");
            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_DLFRONT");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        /*Car Images*/
        if (new File(path_car + "/IMG_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();

            synced.setName("FRONTIMG");
            synced.setFile(new File(path_car + "/IMG_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        }
        if (new File(path_car + "/IMG_right.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("RIGHTIMG");
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_right.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        }
        if (new File(path_car + "/IMG_rear.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("REARIMG");
            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        }
        if (new File(path_car + "/IMG_left.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LEFTIMG");
            synced.setFile(new File(path_car + "/IMG_left.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        }
        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("WINDOWWSTICKERIMG");
            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);

//            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        }
        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("MILEAGEIMG");
            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TRADEINMILEAGE");
            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        }
        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LICENCEPLATENUMBER");
            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/
        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("Customer_Insurance");
            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
            synced.setName("language_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
            synced.setName("type_of_purchase_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
            synced.setName("confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
            synced.setName("test_drive_taken_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
            synced.setName("no_test_drive_confirm_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
            synced.setName("remove_stickers_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
            synced.setName("CONTACTINFOSIGNATUREIMG");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        }
        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
            synced.setName("prints_recieved_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure");
            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
            synced.setName("history_report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        }
        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure");
            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_language_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_type_of_purchase_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
            synced.setName("co_buyer_confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_test_drive_taken_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_no_test_drive_confirm_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_remove_stickers_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_contact_info");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_prints_recieved_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
            synced.setName("co_buyer_condition_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {

            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_history_report");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
            synced.setName("co_buyer_history_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData;
//        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        String recordDataString = gson.toJson(recordData);
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(imagesSynceds);
//        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        return imagesSynceds;
    }


    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            RecordData recordData;
            Gson gson = new GsonBuilder().create();
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (currentPage != 38) {
                saveDataOnback();
            }
            if (mViewPager.getCurrentItem() == 46) {

                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
                    setLocale("en", 45);
                }
            } else {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleTap = false;
                    }
                }, 800);
                if (!doubleTap) {
                    if (currentPage == 42) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                            doubleTap = true;
                            currentPage = 39;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }

                    } else if (currentPage == 55) {
//                        Gson gson = new GsonBuilder().create();
//                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                            ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();

                            if (condition != null && condition.get(condition.size() - 1).isChecked()) {
                                doubleTap = true;
                                setCurrentPage(51);
//                                mViewPager.setCurrentItem(51, false);
                            } else {
                                doubleTap = true;
                                setCurrentPage(52);
//                                mViewPager.setCurrentItem(52, false);
                            }
//                            doubleTap = true;
//                            currentPage = 52;
//                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                            ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                            ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                            int page = 0;
                            if (report != null && report.get(report.size() - 1).isChecked()) {
                                page = 53;
                            }
                            if (report != null && report.get(report.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                                page = 52;
                            }

                            if (report != null && report.get(report.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked() && condition != null && condition.get(condition.size() - 1).isChecked()) {
                                page = 51;
                            }
                            if (page != 0) {
                                setCurrentPage(page);
                            } else {
                                doubleTap = true;
                                currentPage--;
                                mViewPager.setCurrentItem(currentPage);
                            }
                        }

                    } else if (currentPage == 53) {
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        if (condition.get(condition.size() - 1).isChecked()) {
                            page = 51;
                            setCurrentPage(page);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }

                    } else if (currentPage == 54) {
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        if (history.get(history.size() - 1).isChecked()) {
                            page = 52;

                        }
                        if (history.get(history.size() - 1).isChecked() && condition.get(condition.size() - 1).isChecked()) {
                            page = 51;
                        }
                        if (page != 0) {
                            setCurrentPage(page);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }

                    } else if (currentPage == 19) {
                        Gson gson1 = new GsonBuilder().create();
                        RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                            doubleTap = true;
                            currentPage = 10;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }

                    } else if (currentPage == 39) {
                        doubleTap = true;
                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                            currentPage = 37;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                        }

//                        currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
//                        mViewPager.setCurrentItem(currentPage,false);
                    } else if (currentPage == 35) {
                        doubleTap = true;
                        currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                        mViewPager.setCurrentItem(currentPage, false);
                    } else if (currentPage == 45) {
                        String path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber() + "/CarImages";
                        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
                            currentPage = 42;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
                    } else if (currentPage == 59) {
                        setLocale("en");
//                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer() && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                            doubleTap = true;
//                            currentPage--;
//                            mViewPager.setCurrentItem(currentPage);
//                        } else {
//                            doubleTap = true;
//                            currentPage = 57;
//                            mViewPager.setCurrentItem(currentPage, false);
//                        }

                    }
//                    else if (currentPage==60){
//
//                    }
                    else {
                        doubleTap = true;
                        currentPage--;
                        mViewPager.setCurrentItem(currentPage);
                    }
                }
            }

        }

    }

    public void setLocale(String localeName, int page) {
        Locale myLocale = new Locale(localeName);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, ViewPagerActivity.class);
        refresh.putExtra("page", page);
        PreferenceManger.putString(AppConstant.CURRENT_LANG, localeName);
        refresh.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
        startActivity(refresh);
        finish();
    }

    public void callHomeActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void setLocale(String localeName) {
        Locale myLocale = new Locale(localeName);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        callHomeActivity();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
