package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SyncUp;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealList;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.TabUniqueName;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.fragment.FragmentSendIncompleteEmailToBuyer;
import com.volga.dealershieldtablet.ui.adapter.RecordsAdapter;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IncompleteCustomers extends BaseFragment implements RecordsAdapter.CallbackOnDelete {
    private View mView;
    private RecyclerView recyclerView;
    private TextView textView;
    private ArrayList<Record> recordArrayList;
    private RecordsAdapter adapter;
    private Gson gson = new Gson();
    private RecordData recordData;
    private List<IncompleteDealList> dealList = new ArrayList<>();
    private SwipeRefreshLayout pullToRefresh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.unsynced_record_list, container, false);

        return mView;
    }

    @Override
    public void onResume() {
        if (DSAPP.getInstance().isNetworkAvailable()) {
            dealList.clear();
            if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() > 0) {
                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Refreshing pending deals...");
                progressDialog.show();
                progressDialog.setCancelable(false);
                getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
            } else if (checkPermission()) {
                String deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
                if (PreferenceManger.getUniqueTabName() == null || PreferenceManger.getUniqueTabName().getDeviceName() == null || PreferenceManger.getUniqueTabName().getDeviceName().length() == 0)
                    getUniqueName("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), deviceId);
            }
        } else {
            if (getActivity() != null && !getActivity().isFinishing())
                new CustomToast(getActivity()).alert(getString(R.string.connection_check));
        }
        initView();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(this.broadCastNewMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BroadcastReceiver broadCastNewMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("Data update: ", "Data updated from service");
            if (getActivity() != null) {
                if (intent.getStringExtra("value").equalsIgnoreCase("refreshIncompleteDeal")) {
                    dealList.clear();
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() > 0) {
                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Refreshing pending deals...");
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                    } else if (checkPermission()) {
                        String deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                        generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
                        if (PreferenceManger.getUniqueTabName() == null || PreferenceManger.getUniqueTabName().getDeviceName() == null || PreferenceManger.getUniqueTabName().getDeviceName().length() == 0)
                            getUniqueName("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), deviceId);
                    }
                } else if (intent.getStringExtra("value").equalsIgnoreCase("logOut")) {
                    logoutInBase();
                } else {
                    new CustomToast(getActivity()).alert(intent.getStringExtra("value"));
                }

            }
        }
    };

    private void getUniqueName(String access_token, String deviceId) {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setMessage("Getting deals...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
        RetrofitInitialization.getDs_services().getUniqueTabName(access_token, deviceId).enqueue(new Callback<TabUniqueName>() {
            @Override
            public void onResponse(Call<TabUniqueName> call, Response<TabUniqueName> response) {
//                progressDialog.dismiss();
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.UNIQU_TAB_OBJECT, json);
//                    uniqueDName = response.body().getDeviceName();
//                    uniqueName.setText(MessageFormat.format("Tab Name: {0}", response.body().getDeviceName()));
//                    getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                } else {

                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
                                new CustomToast(getActivity()).toast(arr[1]);
                                logoutInBase();
                            } else {
                                if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                    callRefreshTokenAPI();
                                } else {
                                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                    Locale myLocale = new Locale("en");
                                    Resources res = getResources();
                                    DisplayMetrics dm = res.getDisplayMetrics();
                                    Configuration conf = res.getConfiguration();
                                    conf.locale = myLocale;
                                    res.updateConfiguration(conf, dm);
                                    callHomeActivity();
                                }
                            }

                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("Response ", "onResponse: " + json);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TabUniqueName> call, Throwable t) {
//                progressDialog.dismiss();
                Log.e("API failed tab unique: ", t.getLocalizedMessage());
            }
        });
    }

    public void generateNoteOnSD(Context context, String sBody) {
        try {
            File root = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM), "/DSXT/DeviceID");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "DeviceID.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        if (getActivity() != null && !getActivity().isFinishing()) {
            getActivity().registerReceiver(this.broadCastNewMessage, new IntentFilter("notifyData"));
        }
        textView = mView.findViewById(R.id.heading);
        textView.setText(R.string.current_customer);
        recyclerView = (RecyclerView) mView.findViewById(R.id.record_list);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordArrayList = new ArrayList<>();
        ImageView newDeal = mView.findViewById(R.id.hint);
        newDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(getString(R.string.pending_start_new));
            }
        });
        //remove data if last save time was more than two hours
        pullToRefresh = (SwipeRefreshLayout) mView.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (DSAPP.getInstance().isNetworkAvailable()) {
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() > 0) {
                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Refreshing pending deals...");
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                    } else if (checkPermission()) {
                        //ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        //                            == PackageManager.PERMISSION_GRANTED
                        String deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                        generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
                        if (PreferenceManger.getUniqueTabName() == null || PreferenceManger.getUniqueTabName().getDeviceName() == null || PreferenceManger.getUniqueTabName().getDeviceName().length() == 0)
                            getUniqueName("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), deviceId);
                    }
                } else {
                    if (getActivity() != null && !getActivity().isFinishing())
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
                pullToRefresh.setRefreshing(false);
            }
        });

        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        ArrayList<Record> needToDelete = new ArrayList<>();

        if (recordData != null) {
            for (int i = 0; i < recordData.getRecords().size(); i++) {
                if (recordData.getRecords().get(i).getSettingsAndPermissions().isDataSyncedWithServer()) {
                    needToDelete.add(recordData.getRecords().get(i));
                }
                if (recordData.getRecords().get(i).getSettingsAndPermissions() != null && !recordData.getRecords().get(i).getSettingsAndPermissions().getIsRecordComplete() && !recordData.getRecords().get(i).getSettingsAndPermissions().isSentEmailForIncomplete()) {
                    try {
                        Date date = formatter.parse(recordData.getRecords().get(i).getSettingsAndPermissions().getLatestTimeStamp());
                        long lasttime = date.getTime();
                        long currentTime = System.currentTimeMillis();
                        long diff = currentTime - lasttime;
                        int hours = (int) ((currentTime - lasttime) / (1000 * 60 * 60));
//                        int hours = (int) ((currentTime - lasttime) / (1000 * 60));
//                        if (hours <= 12) {
//                            recordArrayList.add(recordData.getRecords().get(i));
//                        } else {
//                            needToDelete.add(recordData.getRecords().get(i));
//                        }
                        if (!recordData.getRecords().get(i).getSettingsAndPermissions().isFromServer()) {
                            if (hours <= 12) {
                                recordArrayList.add(recordData.getRecords().get(i));
                            } else {
                                {
                                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                    recordData.where(recordData.getRecords().get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLead(true);
                                    String recordDataString1 = gson.toJson(recordData);
                                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
//                                    homeIntent.addCategory( Intent.CATEGORY_HOME );
//                                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                    startActivity(homeIntent);
                                }
                            }
                        } else {
                            recordArrayList.add(recordData.getRecords().get(i));
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }
            }
        }
        deleteRecordsOlderThan2Hours(needToDelete);
//        Collections.reverse(Collections.singletonList(recordArrayList));
        ArrayList<Record> dealershipSpecificDeals = new ArrayList<>();

        for (int i = 0; i < recordArrayList.size(); i++) {
            if (recordArrayList.get(i).getDealershipDetails().getDealershipId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                dealershipSpecificDeals.add(recordArrayList.get(i));
            }
        }
        recordArrayList.clear();
        recordArrayList.addAll(dealershipSpecificDeals);
        Collections.sort(recordArrayList, new Comparator<Record>() {
            @Override
            public int compare(Record o1, Record o2) {
                return Integer.compare(o1.getSettingsAndPermissions().getSequenceOrder(), o2.getSettingsAndPermissions().getSequenceOrder());
            }
        });

//        Collections.reverse(recordArrayList);
        adapter = new RecordsAdapter(getActivity(), false, recordArrayList, this);
        recyclerView.setAdapter(adapter);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        logout.setVisibility(View.GONE);
        DSTextView cancel = mView.findViewById(R.id.text_back_up);
        cancel.setText(R.string.back);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
    }

    private void getIncompleteDealList(int companyId, final ProgressDialog progressDialog) {
        Log.e("getIncompleteDealList", " In Function");
        dealList.clear();
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        String currentTime = null;
        try {
            currentTime = tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RetrofitInitialization.getDs_services().getRecordList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), companyId + "", currentTime, PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "").enqueue(new Callback<ArrayList<IncompleteDealList>>() {
            @Override
            public void onResponse(Call<ArrayList<IncompleteDealList>> call, Response<ArrayList<IncompleteDealList>> response) {

                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("getIncompleteDealList", " In onResponse success");
                    dealList.addAll(response.body());
                    gson = new GsonBuilder().create();
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null) {
                        List<Record> records = recordData.getRecords();
                        ArrayList<Record> needToDelete = new ArrayList<>();
                        for (int i = 0; i < records.size(); i++) {
                            if (records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && (!records.get(i).getSettingsAndPermissions().isSentEmailForIncomplete() && !records.get(i).getSettingsAndPermissions().getIsRecordComplete() && records.get(i).getSettingsAndPermissions().isFromServer())) {
                                boolean isExist = false;
                                for (int j = 0; j < dealList.size(); j++) {
                                    if (!records.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                                        if (Integer.parseInt(records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId()) == dealList.get(j).getCustomerVehicleTradeId()) {
                                            isExist = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExist) {
                                    Log.e(" Not Exist: ", "Deal is dead");
                                    needToDelete.add(records.get(i));
                                }
                            }
                        }
                        deleteRecordsOlderThan2Hours(needToDelete);
                    }
                    for (int i = 0; i < dealList.size(); i++) {
                        updateRecordThroughVTD(dealList.get(i), i);
                    }
                    initView();
                    if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                        progressDialog.dismiss();
                } else {
                    Log.e("getIncompleteDealList", " In onResponse failure");
                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
                                new CustomToast(getActivity()).toast(arr[1]);
                                logoutInBase();
                            } else {
                                if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                    callRefreshTokenAPI();
                                } else {
                                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                    Locale myLocale = new Locale("en");
                                    Resources res = getResources();
                                    DisplayMetrics dm = res.getDisplayMetrics();
                                    Configuration conf = res.getConfiguration();
                                    conf.locale = myLocale;
                                    res.updateConfiguration(conf, dm);
                                    callHomeActivity();
                                }
                            }

                        }
                        if (json.getString("Message").equalsIgnoreCase("No incomplete deals found")) {
                            gson = new GsonBuilder().create();
                            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null) {
                                List<Record> records = recordData.getRecords();
                                ArrayList<Record> needToDelete = new ArrayList<>();
                                for (int i = 0; i < records.size(); i++) {
                                    if (!records.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                                        if (records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && (!records.get(i).getSettingsAndPermissions().isSentEmailForIncomplete() && !records.get(i).getSettingsAndPermissions().getIsRecordComplete() && records.get(i).getSettingsAndPermissions().isFromServer())) {
                                            Log.e(" Not Exist: ", "Deal is dead");
                                            needToDelete.add(records.get(i));
                                        }
                                    }
                                }
                                deleteRecordsOlderThan2Hours(needToDelete);
                            }
                        }

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("Response ", "onResponse: " + json);
                    if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                        progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<IncompleteDealList>> call, Throwable t) {
                if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                    progressDialog.dismiss();
            }
        });
    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    Log.e("Refresh: ", gson.toJson(loginData));
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() != 0)
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), null);

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        logoutInBase();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
            }
        });
    }

    private void updateRecordThroughVTD(IncompleteDealList incompleteDealList, int order) {
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = null;
        if (recordData != null)
            record = recordData.whereVTID(incompleteDealList.getCustomerVehicleTradeId() + "");


        if (record != null && record.getVehicleDetails() != null) {
            if (record.getSettingsAndPermissions() != null && !record.getSettingsAndPermissions().isSyncingInProgress()) {
                if (!record.getSettingsAndPermissions().isEditing()) {
                    VehicleDetails vehicleDetails = record.getVehicleDetails() == null ? new VehicleDetails() : record.getVehicleDetails();
                    vehicleDetails.setVINNumber(incompleteDealList.getVIN() == null ? "" : incompleteDealList.getVIN());
                    vehicleDetails.setCarMake(incompleteDealList.getMake() == null ? "" : incompleteDealList.getMake());
                    vehicleDetails.setCarModel(incompleteDealList.getModel() == null ? "" : incompleteDealList.getModel());
                    vehicleDetails.setCarYear(incompleteDealList.getModelYear() == 0 ? "" : incompleteDealList.getModelYear() + "");
//                    record.setWebDeal(incompleteDealList.isWebDeal());
//                    record.setDealStatusText(incompleteDealList.getDealStatusText());
//                    record.setDealStatusId(incompleteDealList.getDealStatusId());

                    record.setWebDeal(incompleteDealList.isWebDeal());
                    record.setBuyerSigned(incompleteDealList.isBuyerSigned());
                    record.setCoBuyerSigned(incompleteDealList.isCoBuyerSigned());
                    record.setCoBuyer(incompleteDealList.isCoBuyer());
                    record.setDealStatusText(incompleteDealList.getDealStatusText());
                    record.setDealStatusId(incompleteDealList.getDealStatusId());

                    CustomerDetails customerDetails = record.getCustomerDetails();
                    customerDetails.setFirstName(incompleteDealList.getCustomerFirstName() == null ? "" : incompleteDealList.getCustomerFirstName());
                    customerDetails.setLastName(incompleteDealList.getCustomerLastName() == null ? "" : incompleteDealList.getCustomerLastName());
                    SalesPersons salesPersons = customerDetails.getSalesPersons() == null ? new SalesPersons() : customerDetails.getSalesPersons();
                    salesPersons.setFirstName(incompleteDealList.getSalesPersonFirstName() == null ? "" : incompleteDealList.getSalesPersonFirstName());
                    salesPersons.setEmail(incompleteDealList.getSalesPersonEmail() == null ? "" : incompleteDealList.getSalesPersonEmail());
                    salesPersons.setLastName(incompleteDealList.getSalesPersonLastName() == null ? "" : incompleteDealList.getSalesPersonLastName());
                    salesPersons.setUserId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
                    vehicleDetails.setSalesId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
                    customerDetails.setSalesPersons(salesPersons);

                    DealershipDetails dealershipDetails = new DealershipDetails();
                    dealershipDetails.setDealershipName(incompleteDealList.getDealershipName());
                    dealershipDetails.setDealershipId(incompleteDealList.getDealershipId());
                    record.setDealershipDetails(dealershipDetails);

                    SettingsAndPermissions settingsAndPermissions = record.getSettingsAndPermissions() == null ? new SettingsAndPermissions() : record.getSettingsAndPermissions();
                    settingsAndPermissions.setCustomerVehicleTradeId(incompleteDealList.getCustomerVehicleTradeId() + "");
                    settingsAndPermissions.setScreenName(incompleteDealList.getLastScreenName());
                    settingsAndPermissions.setSequenceOrder(order);
                    settingsAndPermissions.setTabName(incompleteDealList.getLastAccessingTabletName());
                    if (getPageIndex(incompleteDealList.getLastScreenName()) > 81) {
                        settingsAndPermissions.setDataSyncedWithServer(true);
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                    Date date = null;
                    try {
                        date = sdf.parse(incompleteDealList.getTradeDateTime());
//                Log.e("Date from server: ", date.toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    assert date != null;
                    settingsAndPermissions.setLatestTimeStamp(date.toString());
                    settingsAndPermissions.setFrontUrl(incompleteDealList.getFrontImageUrl());
                    settingsAndPermissions.setFromServer(true);
                    if (incompleteDealList.getDealStatus() != null && incompleteDealList.getDealStatus().length() > 0) {
                        settingsAndPermissions.setDealStatus(incompleteDealList.getDealStatus());
                    }
                    if (settingsAndPermissions.isSentEmailForIncomplete()) {
                        settingsAndPermissions.setDealStatus("Syncing");
                    }
//            settingsAndPermissions.setIncompleteDeal(true);

                    record.setSettingsAndPermissions(settingsAndPermissions);
                    record.setCustomerDetails(customerDetails);
                    record.setVehicleDetails(vehicleDetails);

                    int index = recordData.getIndexNumber(customerDetails.getDLNumber());
                    recordData.getRecords().set(index, record);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                } else {
                    Log.e("Deal is Editing: ", incompleteDealList.getCustomerVehicleTradeId() + " This id is in edit mode discarding merge.");
                }
            } else {
                Log.e("Syncing in progress: ", incompleteDealList.getCustomerVehicleTradeId() + " This id is in sync mode discarding merge.");
            }
        } else {
            String rand_int1 = System.currentTimeMillis() + "";/*String.format(Locale.getDefault(), "%09d", rand.nextInt(100000001));*/
            Log.e("Random number: ", rand_int1);
            /*Commented on 31/02/2020 to fix crash while capturing image on window sticker on finance page*/
//            PreferenceManger.putString(AppConstant.DMV_NUMBER, rand_int1);
            Gson gson = new GsonBuilder().create();
            if (PreferenceManger.getStringValue(AppConstant.RECORD_DATA).length() != 0) {
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            } else {
                recordData = new RecordData();
            }
            record = new Record();
            CustomerDetails customerDetails1 = new CustomerDetails();
            CoBuyerCustomerDetails coBuyerCustomerDetails1 = new CoBuyerCustomerDetails();
            VehicleDetails vehicleDetails1 = new VehicleDetails();
            TradeInVehicle tradeInVehicle1 = new TradeInVehicle();
            SettingsAndPermissions settingsAndPermissions1 = new SettingsAndPermissions();
            settingsAndPermissions1.setLocalLogs("");
            SyncUp syncUp = new SyncUp();
            DealershipDetails dealershipDetails = new DealershipDetails();
            dealershipDetails.setDealershipName(incompleteDealList.getDealershipName());
            record.setWebDeal(incompleteDealList.isWebDeal());
            record.setDealStatusText(incompleteDealList.getDealStatusText());
            record.setDealStatusId(incompleteDealList.getDealStatusId());
            record.setBuyerSigned(incompleteDealList.isBuyerSigned());
            record.setCoBuyerSigned(incompleteDealList.isCoBuyerSigned());
            record.setCoBuyer(incompleteDealList.isCoBuyer());
            dealershipDetails.setDealershipId(incompleteDealList.getDealershipId());
//            dealershipDetails.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
            record.setCustomerDetails(customerDetails1);
            record.setCoBuyerCustomerDetails(coBuyerCustomerDetails1);
            record.setDealershipDetails(dealershipDetails);
            record.setTradeInVehicle(tradeInVehicle1);
            record.setSettingsAndPermissions(settingsAndPermissions1);
            record.setSyncUp(syncUp);
            record.setVehicleDetails(vehicleDetails1);
            record.getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            record.getCustomerDetails().setDLNumber(rand_int1);
            SalesPersons salesPersons1 = new SalesPersons();

            record.getCustomerDetails().setSalesPersons(salesPersons1);
            VehicleDetails vehicleDetails = record.getVehicleDetails();
            vehicleDetails.setVINNumber(incompleteDealList.getVIN() == null ? "" : incompleteDealList.getVIN());
            vehicleDetails.setCarMake(incompleteDealList.getMake() == null ? "" : incompleteDealList.getMake());
            vehicleDetails.setCarModel(incompleteDealList.getModel() == null ? "" : incompleteDealList.getModel());
            vehicleDetails.setCarYear(incompleteDealList.getModelYear() == 0 ? "" : incompleteDealList.getModelYear() + "");

            CustomerDetails customerDetails = record.getCustomerDetails();
            customerDetails.setFirstName(incompleteDealList.getCustomerFirstName() == null ? "" : incompleteDealList.getCustomerFirstName());
            customerDetails.setLastName(incompleteDealList.getCustomerLastName() == null ? "" : incompleteDealList.getCustomerLastName());
            SalesPersons salesPersons = customerDetails.getSalesPersons();
            salesPersons.setFirstName(incompleteDealList.getSalesPersonFirstName() == null ? "" : incompleteDealList.getSalesPersonFirstName());
            salesPersons.setEmail(incompleteDealList.getSalesPersonEmail() == null ? "" : incompleteDealList.getSalesPersonEmail());
            salesPersons.setLastName(incompleteDealList.getSalesPersonLastName() == null ? "" : incompleteDealList.getSalesPersonLastName());
            salesPersons.setUserId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
            vehicleDetails.setSalesId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
            customerDetails.setSalesPersons(salesPersons);

            SettingsAndPermissions settingsAndPermissions = record.getSettingsAndPermissions();
            settingsAndPermissions.setCustomerVehicleTradeId(incompleteDealList.getCustomerVehicleTradeId() + "");
            settingsAndPermissions.setScreenName(incompleteDealList.getLastScreenName());
            settingsAndPermissions.setSequenceOrder(order);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            Date date = null;
            try {
                date = sdf.parse(incompleteDealList.getTradeDateTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            assert date != null;
            settingsAndPermissions.setLatestTimeStamp(date.toString());
            settingsAndPermissions.setFrontUrl(incompleteDealList.getFrontImageUrl());
//            settingsAndPermissions.setIncompleteDeal(true);
            settingsAndPermissions.setDealStatus(incompleteDealList.getDealStatus());
            record.setSettingsAndPermissions(settingsAndPermissions);
            record.setCustomerDetails(customerDetails);
            record.setVehicleDetails(vehicleDetails);
            settingsAndPermissions.setFromServer(true);
            recordData.getRecords().add(record);
            //Add the recordData object to the SharedPreference after converting it to string
            String recordDataString1 = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
        }
    }

    private void deleteRecordsOlderThan2Hours(ArrayList<Record> needToDelete) {
        for (int i = 0; i < needToDelete.size(); i++) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            Record record = recordData.where(needToDelete.get(i).getCustomerDetails().getDLNumber());

            if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
                File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
                deleteRecursive(main);
            }
            recordData.getRecords().remove(record);
            String recordDataString1 = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
        }

    }

    @Override
    public void onDelete(int position, boolean isSentEmail, boolean refresh, boolean isDev) {
        if (isSentEmail) {
            /*Call Sent Incomplete Email*/
            int pos = recordData.getIndexNumber(recordArrayList.get(position).getCustomerDetails().getDLNumber());
            Record record = recordData.getRecords().get(pos);
            PreferenceManger.putString(AppConstant.DMV_NUMBER, record.getCustomerDetails().getDLNumber());
            FragmentSendIncompleteEmailToBuyer emailToBuyer = new FragmentSendIncompleteEmailToBuyer();
            Bundle bundle = new Bundle();
            bundle.putString("tradeId", recordArrayList.get(position).getSettingsAndPermissions().getCustomerVehicleTradeId());
            bundle.putString("dlnumber", recordArrayList.get(position).getCustomerDetails().getDLNumber());
            emailToBuyer.setArguments(bundle);
            replaceFragment(R.id.container, emailToBuyer, FragmentSendIncompleteEmailToBuyer.class.getCanonicalName());
        } else if (refresh) {
            Log.e("OnRefresh: ", " The deal which was completed by other tab.");
            if (DSAPP.getInstance().isNetworkAvailable()) {
                if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() > 0) {
                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Refreshing pending deals...");
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                } else if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    String deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                    generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
                    if (PreferenceManger.getUniqueTabName() == null || PreferenceManger.getUniqueTabName().getDeviceName() == null || PreferenceManger.getUniqueTabName().getDeviceName().length() == 0)
                        getUniqueName("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), deviceId);
                }
            } else {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
            }
        } else if (isDev) {
            showReportDialog(position);
//            FragmentTransaction ft = getFragmentManager().beginTransaction();
//            DialogFragment newFragment = new ReportDeveloperFragmentDialog();
//            Bundle bundle = new Bundle();
//            bundle.putString("data", recordArrayList.get(position).getSettingsAndPermissions().getErrorDesc());
//            bundle.putString("email", recordArrayList.get(position).getCustomerDetails().getSalesPersons().getEmail());
//            newFragment.setArguments(bundle);
//            newFragment.show(ft, "dialog");
        } else {
            showDeletePopup(getString(R.string.delete_alert), position);
        }

    }

    private void showReportDialog(final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.report_error, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);

        DSTextView save = promptView.findViewById(R.id.save);
        final DSTextView close = promptView.findViewById(R.id.close);
        DSTextView title = promptView.findViewById(R.id.title);
//        title.setText(list.get(position).getName());
        final DSEdittext data = promptView.findViewById(R.id.addCondition);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyboardUtils.hideSoftKeyboard(data, getActivity());
                alertD.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(data, getActivity());
                alertD.dismiss();
                /*Upload all data to server*/
                String feedbackText = data.getText().toString();

                String subjectData;
                if (feedbackText.length() > 0) {
                    subjectData = recordArrayList.get(position).getSettingsAndPermissions().getErrorDesc() + " \n Feedback data: " + feedbackText + ": ";
                } else {
                    subjectData = recordArrayList.get(position).getSettingsAndPermissions().getErrorDesc();
                }
                writeCustomLogsAPI(recordArrayList.get(position).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordArrayList.get(position).getDealershipDetails().getDealershipId() + "", subjectData, recordArrayList.get(position).getVehicleDetails().getVINNumber(), recordArrayList.get(position).getVehicleDetails().getCarMake(), recordArrayList.get(position).getVehicleDetails().getCarModel());
                alertD.dismiss();

            }
        });
        alertD.setView(promptView);
        alertD.show();
    }

    private void writeCustomLogsAPI(String id, String dId, String logs, String vin, String make, String model) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Submitting feedback..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setDealershipId(dId);
        customLogs.setEmailSubject(null);
        customLogs.setIsSendAlertEmail(true);
//        ArrayList<String> emails=new ArrayList<>();
//        emails.add("shailendra.m@volgainfotech.com");
//        emails.add(getArguments().getString("data"));
//        customLogs.setEmails(emails);
        customLogs.setLogText("<div><b>VIN:</b> " + vin + " <br><b>Make:</b> " + make + " <br><b>Model:</b> " + model + " <br><b>JSON DATA: </b><br>" + logs + "</div>");
//        customLogs.setLogText(logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                progressDialog.dismiss();
                new CustomToast(getActivity()).alert("Submitted");
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    void showPopup(String title) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
        final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);
        final DSTextView titleTV = promptView.findViewById(R.id.title);
        DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
        titleTV.setVisibility(View.GONE);
        messageTV.setText(title);
        final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
        stopDeal.setVisibility(View.GONE);
        DSTextView ok = promptView.findViewById(R.id.ok);
        alertD.setView(promptView);
        alertD.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertD.dismiss();
            }
        });

//        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
//                .setMessage(title)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int whichButton) {
//                        dialog.dismiss();
//                    }
//                }).show();
    }

    void showDeletePopup(String title, final int position) {
        new AlertDialog.Builder(getActivity())
                .setMessage(title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        if (recordArrayList.size() > 0) {
                            int pos = recordData.getIndexNumber(recordArrayList.get(position).getCustomerDetails().getDLNumber());
                            Record record = recordData.getRecords().get(pos);

                            if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
                                File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
                                deleteRecursive(main);
                            }
                            recordData.getRecords().remove(record);
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            adapter.removeAt(position);
                            adapter.notifyDataSetChanged();
                        }
                        dialog.dismiss();

                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show().setCancelable(false);
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }
}
