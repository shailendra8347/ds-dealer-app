package com.volga.dealershieldtablet.ui.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.volga.dealershieldtablet.R;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class ActivityDealershipType extends BaseActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dealership_type);
        initView();
    }

    private void initView() {
        RecyclerView dealerShipType = findViewById(R.id.dealerShipType);
        dealerShipType.setLayoutManager(new LinearLayoutManager(this));
        dealerShipType.setItemAnimator(new DefaultItemAnimator());
//        dealerShipType.setAdapter(new DealerShipTypeAdapter(this));
    }
}
