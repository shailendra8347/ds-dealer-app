package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import id.zelory.compressor.Compressor;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class FragmentDMVPreview extends BaseFragment {
    private View mView;
    private File[] listFile;
    private String isFromFragment = "";
    private ImageView imagePreview, sampleImage;
    private Gson gson;
    private RecordData recordData;
    private LinearLayout containerSample;
    private DSTextView cancel;
    private File fileToDelete;
    private DSTextView back, next;
    private boolean isCheckId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.check_dmv, container, false);

        initView();
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        visibleHintCalled = isVisibleToUser;
        if (isVisibleToUser) {

//            if ((NewDealViewPager.currentPage > 7 && NewDealViewPager.currentPage < 20)) {
            showFragmentInPortrait();
//            }
            if (imagePreview != null) {
                back.setEnabled(true);
                back.setClickable(true);
                next.setVisibility(View.VISIBLE);
                next.setEnabled(true);
                next.setClickable(true);
                updateData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    private void updateData() {


        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        try {
            if (isFromFragment.equalsIgnoreCase(AppConstant.DMV_FRONT)) {
                if (!isCheckId) {
                    gson = new GsonBuilder().create();
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFrontTime(Calendar.getInstance().getTime().toString());

                    //
// containerSample.setVisibility(View.GONE);
//                cancel.setVisibility(View.GONE);
                    String url = "";
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                        cancel.setVisibility(View.INVISIBLE);
                    } else {
                        cancel.setVisibility(View.VISIBLE);

                    }
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                      title.setText(Html.fromHtml(getString(R.string.co_please_confirm_the_information_below)));
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().size() > 0) {
                            for (int i = 0; i < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().size(); i++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().get(i).getDocTypeName().equalsIgnoreCase("co_buyer_DLFRONT")) {
                                    url = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getCoBuyerDocs().get(i).getDocUrl();
                                    break;
                                }
                            }
                        }
                    } else {
//                      title.setText(Html.fromHtml(getString(R.string.please_confirm_the_information_below)));
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size() > 0) {
                            for (int i = 0; i < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size(); i++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().get(i).getDocTypeName().equalsIgnoreCase("DRIVERLICENSEIMG")) {
                                    url = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().get(i).getDocUrl();
                                    break;
                                }
                            }
                        }
                    }
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        ShowImageFromPath("IMG_Co_buyer_DLFRONT.jpg", "LicenseFront");
                    } else {
                        ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront");
                    }
                    if ((fileToDelete != null && fileToDelete.exists())) {
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            loadImageToImageView(ShowImageFromPath("IMG_Co_buyer_DLFRONT.jpg", "LicenseFront"), imagePreview);
                        } else {
                            loadImageToImageView(ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront"), imagePreview);
                        }
                    } else {
                        if (url != null && url.length() > 0) {
                            loadImageToImageView(url, imagePreview);
                            imagePreview.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    loadImageToImageView(ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront"), imagePreview);
                }
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_FRONT)) {
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFrontTime(Calendar.getInstance().getTime().toString());
                loadImageToImageView(ShowImageFromPath("IMG_front.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.front)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_RIGHT)) {
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarRightTime(Calendar.getInstance().getTime().toString());
                loadImageToImageView(ShowImageFromPath("IMG_right.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.passenger_side)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_REAR)) {
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarRearTime(Calendar.getInstance().getTime().toString());
                loadImageToImageView(ShowImageFromPath("IMG_rear.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.back_side)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_LEFT)) {
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarLeftTime(Calendar.getInstance().getTime().toString());
                loadImageToImageView(ShowImageFromPath("IMG_left.jpg", "CarImages"), imagePreview);
                Glide.with(Objects.requireNonNull(getActivity()))
                        .load(R.drawable.drivers_side)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_WINDOW_VIN)) {
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarStickerTime(Calendar.getInstance().getTime().toString());
                loadImageToImageView(ShowImageFromPath("IMG_sticker.jpg", "CarImages"), imagePreview);

                if (!(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().length() == 0)) {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase(getString(R.string.used)))
                    //Code for displaying already selected Vehicle Type
                    {
                        Glide.with(getActivity())
                                .load(R.drawable.window_sticker_old_car)
                                .apply(requestOptions)
                                .into(sampleImage);
                    } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                        Glide.with(getActivity())
                                .load(R.drawable.window_sticker_new_car)
                                .apply(requestOptions)
                                .into(sampleImage);
                    }
                }
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_MILEAGE)) {
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarOdoMeterTime(Calendar.getInstance().getTime().toString());
                loadImageToImageView(ShowImageFromPath("IMG_mileage.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.odometer)
                        .apply(requestOptions)
                        .into(sampleImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initView() {
        assert getArguments() != null;
        isFromFragment = getArguments().getString(AppConstant.FROM_FRAGMENT);

        imagePreview = mView.findViewById(R.id.imagePreview);
        sampleImage = mView.findViewById(R.id.sampleImage);
        containerSample = mView.findViewById(R.id.containerSampleImage);
        ImageView deletePic = mView.findViewById(R.id.deletePic);
        isCheckId = getArguments().getBoolean("checkId", false);
        deletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeletePopup(getString(R.string.delete_image_alert));
            }
        });


        DSTextView logout = mView.findViewById(R.id.txt_logout);
        back = mView.findViewById(R.id.text_back_up);
        next = mView.findViewById(R.id.text_next);
        cancel = mView.findViewById(R.id.text_cancelBack);
        if (isCheckId) {
            cancel.setVisibility(View.INVISIBLE);
        } else {
            cancel.setVisibility(View.VISIBLE);
        }
//        cancel.setVisibility(View.GONE);

        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);
        if (!visibleHintCalled) {
            updateData();
        }

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (fileToDelete != null)
                        Log.e("File Delete Request: ", "Success : >> " + fileToDelete.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });

    }

    private View.OnClickListener mOnClickListener;

    {
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.text_cancelBack:
                        cancel.setClickable(false);
                        cancel.setEnabled(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                cancel.setEnabled(true);
                                cancel.setClickable(true);
                            }
                        },6000);
                        cancel();
                        break;
                    case R.id.text_next:
                        disableNext(next);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                enableNext(next);
                            }
                        }, 2500);
                        if (gson != null && recordData != null) {
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        }
                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            ((NewDealViewPager) Objects.requireNonNull(getActivity())).setCurrentPage(56);
                        } else {
                            boolean isAllowed = false;
                            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size() > 0) {
                                for (int i = 0; i < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().size(); i++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocsAll().get(i).getDocTypeName().equalsIgnoreCase("DRIVERLICENSEIMG")) {
                                        isAllowed = true;
                                        break;
                                    }
                                }
                            }
                            if ((fileToDelete != null && fileToDelete.exists()) || isAllowed) {

//                                next.setEnabled(false);
//                                next.setClickable(false);
                                iGoToNextPage.whatNextClick();
//
                            } else {
                                enableNext(next);
                                new CustomToast(getActivity()).alert("Oops!! Please take photo of Driver License/ID again!");
                            }
                        }
                        break;
                    case R.id.text_back_up:
//                        back.setEnabled(false);
//                        back.setClickable(false);
                        if (fileToDelete != null)
                            Log.e("File Delete Request: ", "Success : >> " + fileToDelete.delete());
                        iGoToNextPage.goToBackIndex();
                        break;
                    case R.id.txt_logout:
                        logout();
                        break;
                }
            }
        };
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(getActivity())
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    void showDeletePopup(String title) {
        new AlertDialog.Builder(getActivity())
                .setMessage(title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (fileToDelete != null) {
                            Log.e("File Delete Request: ", "Success : >> " + fileToDelete.delete());
                        }
                        iGoToNextPage.goToBackIndex();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    public Uri ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
        if (!isCheckId)
            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
        else
            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
        fileToDelete = imgFile;
        File filenew = null;
        try {
            filenew = new Compressor(getActivity())
                    .compressToFile(imgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (filenew != null) {
            if (filenew.exists()) {
                return Uri.fromFile(filenew);
            }
        } else {
            if (imgFile.exists()) {
                return Uri.fromFile(imgFile);
            }
        }
        return null;
    }

    IGoToNextPage iGoToNextPage;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }
}
