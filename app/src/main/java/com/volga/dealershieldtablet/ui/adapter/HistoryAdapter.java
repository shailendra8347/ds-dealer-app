package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.TypeRow> {

    private final Activity activity;
    private RecordData recordData;
    private Gson gson;
    private ArrayList<HistoryListPojo> list;
    private boolean isHistory;
    String myFormat = "MM-dd-yyyy"; //Change as you need
    SimpleDateFormat sdf = new SimpleDateFormat(
            myFormat, Locale.getDefault());

    public interface IOnLanguageSelect {
        void languageSelected(String selectedLanguage, int selectedLanguageId);
    }

    IOnLanguageSelect iOnLanguageSelect;

    public HistoryAdapter(Activity activity, IOnLanguageSelect iOnLanguageSelect, ArrayList<HistoryListPojo> list, boolean isHistory) {
        this.activity = activity;
        this.list = list;
        this.iOnLanguageSelect = iOnLanguageSelect;
        this.isHistory = isHistory;

    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.history_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (position == 14 && isHistory) {
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate1() == null) {
//                holder.name.setText(Html.fromHtml(String.format(activity.getString(R.string.history15), "_________ ")));
//            } else {
//                holder.name.setText(Html.fromHtml(String.format(activity.getString(R.string.history15), sdf.format(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate1()))));
//            }
//        } else if (position == 15 && isHistory) {
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate2() == null) {
//                holder.name.setText(Html.fromHtml(String.format(activity.getString(R.string.history16), "_________ ")));
//            } else {
//                holder.name.setText(Html.fromHtml(String.format(activity.getString(R.string.history16), sdf.format(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate2()))));
//            }
//        } else {
        String append1 = "";
        if (list.get(position).isVerified() && position != list.size() - 1 && isHistory) {
            append1 = "<br/><font color=\"#484747\">(See third party reports for details)</font>";
        }
        holder.name.setText(Html.fromHtml(list.get(position).getName() + append1));
//        }
        if (position == list.size() - 1) {
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        } else {
            holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }
//
        if (!list.get(position).isVerified()) {
            holder.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCheckedOnclicked(position, !list.get(position).isChecked(), holder);
                }
            });
        } else {
            holder.name.setOnClickListener(null);
        }
        holder.checkBox.setOnCheckedChangeListener(null);
        if (isHistory) {
            if (list.get(position).isVerified()) {
                if (position == list.size() - 1) {
                    holder.checkBox.setChecked(false);
                    AppConstant.historyLists.get(position).setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                    AppConstant.historyLists.get(position).setChecked(true);
                }
                holder.checkBox.setEnabled(false);
            } else {
                holder.checkBox.setEnabled(true);
                holder.checkBox.setChecked(list.get(position).isChecked());
            }
        } else {
            if (list.get(position).isAutoMarked()) {
                holder.checkBox.setChecked(true);
                AppConstant.reportLists.get(position).setChecked(true);
                holder.checkBox.setEnabled(false);
            } else {
                holder.checkBox.setEnabled(true);
                holder.checkBox.setChecked(list.get(position).isChecked());
            }
            if (list.get(position).isVerified()) {
                if (position == list.size() - 1) {
                    holder.checkBox.setChecked(false);
                    AppConstant.reportLists.get(position).setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                    AppConstant.reportLists.get(position).setChecked(true);
                }
                holder.checkBox.setEnabled(false);
            } else {
                if (!list.get(position).isAutoMarked())
                    holder.checkBox.setEnabled(true);
                holder.checkBox.setChecked(list.get(position).isChecked());
            }
        }
//        holder.checkBox.setChecked(list.get(position).isChecked());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                setCheckedOnclicked(position, b, holder);
            }
        });
    }

    private void setCheckedOnclicked(int position, boolean b, TypeRow holder) {
        list.get(position).setChecked(b);
        iOnLanguageSelect.languageSelected("", 0);
//        if (isHistory) {
//            if (position == 14 || position == 15) {
//                if (b) {
//                    openCalender(holder.name, position);
//                } else {
//                    if (position == 14) {
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistoryDate1(null);
//                    } else {
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setHistoryDate2(null);
//                    }
//                }
//            }
//        }
        if (isHistory) {
            list.get(position).setChecked(b);
            AppConstant.historyLists.get(position).setChecked(b);
            if (list.size() - 1 != position) {
                list.get(list.size() - 1).setChecked(false);
                AppConstant.historyLists.get(AppConstant.historyLists.size() - 1).setChecked(false);
                notifyByHandler();
            } else {
                for (int i = 0; i < list.size() - 1; i++) {
                    AppConstant.historyLists.get(i).setChecked(false);
                    list.get(i).setChecked(false);
                    notifyByHandler();
                }
            }
        } else {
            list.get(position).setChecked(b);
            AppConstant.reportLists.get(position).setChecked(b);
            if (list.size() - 1 != position) {
                list.get(list.size() - 1).setChecked(false);
                AppConstant.reportLists.get(AppConstant.reportLists.size() - 1).setChecked(false);
                notifyByHandler();
            } else {
                for (int i = 0; i < list.size() - 1; i++) {
                    AppConstant.reportLists.get(i).setChecked(false);
                    list.get(i).setChecked(false);
                    notifyByHandler();
                }
            }
        }
    }

    private void notifyByHandler() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        }, 60);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name;
        CheckBox checkBox;

        TypeRow(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            name = itemView.findViewById(R.id.name);
        }
    }
}
