package com.volga.dealershieldtablet.ui.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentDealershipHandover extends BaseFragment {


    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.handover_tablet_to_dealer, container, false);
        initView();
        return mView;
    }

    private void initView() {
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        DSTextView heading = mView.findViewById(R.id.handoverText);
//        text_cancelBack.setVisibility(View.GONE);
        DSTextView handover = mView.findViewById(R.id.handover);
        handover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getActivity())
                        .setMessage(R.string.r_u_dealer)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.yeslast, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (getArguments().getBoolean("isFromPrints", false)) {
                                    next(new FragmentCustomerHandover2());
                                } else {
                                    next(new FragmentAbortOrContinue());
                                }
                            }
                        })
                        .setNegativeButton(R.string.nolast, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show().setCancelable(false);

            }
        });
    }


}
