package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DecisionMaker;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import id.zelory.compressor.Compressor;
import timber.log.Timber;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class FragmentImagePreview extends BaseFragment {
    private View mView;
    private File[] listFile;
    private String isFromFragment = "";
    private ImageView imagePreview, sampleImage;
    private Gson gson;
    private RecordData recordData;
    private LinearLayout containerSample;
    private DSTextView cancel;
    private Button addtionalImages;
    private boolean isAdditional = false;
    private File fileToDelete;
    private View progressDialog;
    private DSTextView next;
    private DSTextView back;
    private DecisionMaker maker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.check_dmv_front, container, false);
        initView();
        if ((NewDealViewPager.currentPage > 2 && NewDealViewPager.currentPage < 13)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 13 && NewDealViewPager.currentPage < 24)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 29 && NewDealViewPager.currentPage < 33)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 43 && NewDealViewPager.currentPage < 54)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 35 && NewDealViewPager.currentPage < 43)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 64 && NewDealViewPager.currentPage < 67)) {
            showFragmentInLandscape();
        }
//        else {
//            showFragmentInPortrait();
//        }

        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        visibleHintCalled = isVisibleToUser;
        if (isVisibleToUser) {
            if ((NewDealViewPager.currentPage > 2 && NewDealViewPager.currentPage < 13)) {
                showFragmentInLandscape();
            } else if ((NewDealViewPager.currentPage > 13 && NewDealViewPager.currentPage < 24)) {
                showFragmentInLandscape();
            } else if ((NewDealViewPager.currentPage > 29 && NewDealViewPager.currentPage < 33)) {
                showFragmentInLandscape();
            } else if ((NewDealViewPager.currentPage > 43 && NewDealViewPager.currentPage < 54)) {
                showFragmentInLandscape();
            } else if ((NewDealViewPager.currentPage > 35 && NewDealViewPager.currentPage < 43)) {
                showFragmentInLandscape();
            } else if ((NewDealViewPager.currentPage > 64 && NewDealViewPager.currentPage < 67)) {
                showFragmentInLandscape();
            }
//            else {
//                showFragmentInPortrait();
//            }
            if (imagePreview != null) {
                updateData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void updateData() {
        enableNext(next);
        enableNext(back);
        next.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);

        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        try {
            if (isFromFragment.equalsIgnoreCase(AppConstant.DMV_FRONT)) {
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDmvFrontTime(Calendar.getInstance().getTime().toString());
                containerSample.setVisibility(View.GONE);
                cancel.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.GONE);
                loadImageToImageView(ShowImageFromPath("IMG_DLFRONT.jpg", "LicenseFront"), imagePreview);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_FRONT)) {
                addtionalImages.setVisibility(View.GONE);
                containerSample.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFrontTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                File file;
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    loadImageToImageView(ShowImageFromPath("IMG_front.jpg", "CarImages"), imagePreview);
                } else {
                    loadImageToImageView(ShowImageFromPath("IMG_trade_front.jpg", "CarImages"), imagePreview);
                }


//                loadImageToImageView(ShowImageFromPath("IMG_trade_front.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.front)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_RIGHT)) {
                containerSample.setVisibility(View.VISIBLE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarRightTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                loadImageToImageView(ShowImageFromPath("IMG_right.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.passenger_side)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_REAR)) {
                containerSample.setVisibility(View.VISIBLE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarRearTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                loadImageToImageView(ShowImageFromPath("IMG_rear.jpg", "CarImages"), imagePreview);
                Glide.with(getActivity())
                        .load(R.drawable.back_side)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_LEFT)) {
                containerSample.setVisibility(View.VISIBLE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarLeftTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                loadImageToImageView(ShowImageFromPath("IMG_left.jpg", "CarImages"), imagePreview);
                Glide.with(requireActivity())
                        .load(R.drawable.drivers_side)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_WINDOW_VIN)) {
                containerSample.setVisibility(View.VISIBLE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarStickerTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    loadImageToImageView(ShowImageFromPath("IMG_sticker.jpg", "CarImages"), imagePreview);

                    if (!(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().length() == 0)) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used"))
                        //Code for displaying already selected Vehicle Type
                        {
                            Glide.with(getActivity())
                                    .load(R.drawable.window_sticker_old_car)
                                    .apply(requestOptions)
                                    .into(sampleImage);
                        } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                            Glide.with(getActivity())
                                    .load(R.drawable.window_sticker_new_car)
                                    .apply(requestOptions)
                                    .into(sampleImage);
                        }
                    }
                } else {
                    loadImageToImageView(ShowImageFromPath("IMG_licencePlate.jpg", "CarImages"), imagePreview);

                    if (!(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().length() == 0)) {

                        Glide.with(getActivity())
                                .load(R.drawable.licenseplate)
                                .apply(requestOptions)
                                .into(sampleImage);

                    }
                }

            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_MILEAGE)) {
                containerSample.setVisibility(View.VISIBLE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarOdoMeterTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    loadImageToImageView(ShowImageFromPath("IMG_mileage.jpg", "CarImages"), imagePreview);
                } else {
                    loadImageToImageView(ShowImageFromPath("IMG_trade_mileage.jpg", "CarImages"), imagePreview);
                }
                Glide.with(getActivity())
                        .load(R.drawable.odometer)
                        .apply(requestOptions)
                        .into(sampleImage);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_ADDITIONAL1)) {
                containerSample.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditional1(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    loadImageToImageView(ShowImageFromPath("IMG_Additional1.jpg", "CarImages"), imagePreview);
                else
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional1.jpg", "CarImages"), imagePreview);

            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_ADDITIONAL2)) {
                containerSample.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditional2(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    loadImageToImageView(ShowImageFromPath("IMG_Additional2.jpg", "CarImages"), imagePreview);
                else
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional2.jpg", "CarImages"), imagePreview);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_ADDITIONAL3)) {
                containerSample.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditional3(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    loadImageToImageView(ShowImageFromPath("IMG_Additional3.jpg", "CarImages"), imagePreview);
                else
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional3.jpg", "CarImages"), imagePreview);
            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_ADDITIONAL4)) {
                containerSample.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.VISIBLE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditional4(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    loadImageToImageView(ShowImageFromPath("IMG_Additional4.jpg", "CarImages"), imagePreview);
                else
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional4.jpg", "CarImages"), imagePreview);

            } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_ADDITIONAL5)) {
                containerSample.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditional5(Calendar.getInstance().getTime().toString());
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    loadImageToImageView(ShowImageFromPath("IMG_Additional5.jpg", "CarImages"), imagePreview);
                else
                    loadImageToImageView(ShowImageFromPath("IMG_TradeInAdditional5.jpg", "CarImages"), imagePreview);


            } else if (isFromFragment.equalsIgnoreCase(AppConstant.INSURANCE)) {
                containerSample.setVisibility(View.GONE);
                addtionalImages.setVisibility(View.GONE);
                gson = new GsonBuilder().create();
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInsurenceTime(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                loadImageToImageView(ShowImageFromPath("IMG_insurance.jpg", "CarImages"), imagePreview);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initView() {
//        showFragmentInLandscape();
        assert getArguments() != null;
        isFromFragment = getArguments().getString(AppConstant.FROM_FRAGMENT);
        isAdditional = getArguments().getBoolean("isAdditional", false);
        addtionalImages = mView.findViewById(R.id.addMoreImage);
        addtionalImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.whatNextClick();
            }
        });
        progressDialog = mView.findViewById(R.id.progressBar);
        imagePreview = mView.findViewById(R.id.imagePreview);
        sampleImage = mView.findViewById(R.id.sampleImage);
        containerSample = mView.findViewById(R.id.containerSampleImage);
        ImageView deletePic = mView.findViewById(R.id.deletePic);
        deletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeletePopup(getString(R.string.delete_image_alert));
            }
        });

        DSTextView logout = mView.findViewById(R.id.txt_logout);
        back = mView.findViewById(R.id.text_back);
        next = mView.findViewById(R.id.text_next);
        cancel = mView.findViewById(R.id.text_cancelBack);

        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);

        back.setOnClickListener(mOnClickListener);

        cancel.setOnClickListener(mOnClickListener);
        if (!visibleHintCalled) {
            updateData();
        }
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (fileToDelete != null)
                        Log.e("File Delete Request: ", "Success : >> " + fileToDelete.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
    }

    private View.OnClickListener mOnClickListener;

    {
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.text_cancelBack:
                        if (!isAdditional)
                            cancel();
                        break;
                    case R.id.text_next:
                        gson = new GsonBuilder().create();
                        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        disableNext(next);
                        disableNext(back);
                        boolean isNormal = false;
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                            isNormal = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isNormalFlow();
                        if (!isNormal)
                            maker = decideNextPage();
//                        next.setEnabled(false);
//                        next.setClickable(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                enableNext(next);
                                enableNext(back);
                            }
                        }, 2000);
//                        next.setVisibility(View.GONE);
                        savePageNumber();
                        if (!isAdditional) {
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            if (fileToDelete != null && fileToDelete.exists()) {
                                if (NewDealViewPager.currentPage == 31) {
                                    if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
                                        ((NewDealViewPager) getActivity()).setCurrentPage(33);
                                    } else {
                                        ((NewDealViewPager) getActivity()).setCurrentPage(54);
                                    }
                                } else if (NewDealViewPager.currentPage == 41) {
                                    ((NewDealViewPager) getActivity()).setCurrentPage(43);
                                } else {
                                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                                        if (maker != null) {
                                            if (maker.getIndex() != -1) {
                                                ((NewDealViewPager) getActivity()).setCurrentPage(maker.getIndex());
                                            } else if (maker.isGoNext()) {
                                                iGoToNextPage.whatNextClick();
                                            } else {
                                                iGoToNextPage.whatNextClick();
                                            }
                                        } else {
                                            iGoToNextPage.whatNextClick();
                                        }
                                    } else {
                                        iGoToNextPage.whatNextClick();
                                    }
                                }
//                                decideNextPage();
                            } else {
                                enableNext(next);
                                new CustomToast(getActivity()).toast("Oops!! Please take photo again!");
                            }
                        } else {
                            goToSpecificPage();
                            enableNext(next);
                        }
                        break;
                    case R.id.text_back:
                        disableNext(back);
                        disableNext(next);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                enableNext(next);
                                enableNext(back);
                            }
                        }, 2000);
                        if (fileToDelete != null) {
                            Log.e("File Delete Request: ", "Success : >> " + fileToDelete.delete());
                        }
//                        back.setEnabled(false);
//                        back.setClickable(false);
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                back.setEnabled(true);
//                                back.setClickable(true);
//                            }
//                        }, 1100);
//                        back.setVisibility(View.GONE);
                        if (NewDealViewPager.currentPage == 33) {
//                            if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
//                                ((NewDealViewPager) getActivity()).setCurrentPage(33);
//                            } else {
                                ((NewDealViewPager) getActivity()).setCurrentPage(31);
//                            }
                        } else if (NewDealViewPager.currentPage == 43) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(41);
                        } else
                            iGoToNextPage.goToBackIndex();
//                       decideBackPage();
                        break;
                    case R.id.txt_logout:
                        logout();
                        break;
                }
            }
        };
    }

    private DecisionMaker decideNextPage() {
//        Log.e("Time1 ", "time: " + System.currentTimeMillis());
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        boolean hasRight = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeRight();
        boolean hasLeft = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeLeft();
        boolean hasBack = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeBack();
        boolean hasWindow = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeWindow();
        boolean hasOdoMeter = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeOdometer();
        DecisionMaker decisionMaker = new DecisionMaker();
        if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_FRONT)) {
            if (hasRight) {
                decisionMaker.setGoNext(true);
                decisionMaker.setIndex(-1);
//                iGoToNextPage.whatNextClick();
            } else if (hasBack) {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(7);
//                ((NewDealViewPager) getActivity()).setCurrentPage(7);
            } else if (hasLeft) {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(9);
//                ((NewDealViewPager) getActivity()).setCurrentPage(9);
            } else if (hasWindow) {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(11);
//                ((NewDealViewPager) getActivity()).setCurrentPage(11);
            } else {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(13);
            }
        } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_RIGHT)) {
            if (hasBack) {
                decisionMaker.setGoNext(true);
                decisionMaker.setIndex(-1);
//                iGoToNextPage.whatNextClick();
            } else if (hasLeft) {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(9);
//                ((NewDealViewPager) getActivity()).setCurrentPage(9);
            } else if (hasWindow) {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(11);
//                ((NewDealViewPager) getActivity()).setCurrentPage(11);
            } else {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(13);
            }
        } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_REAR)) {
            if (hasLeft) {
                decisionMaker.setGoNext(true);
                decisionMaker.setIndex(-1);
//                iGoToNextPage.whatNextClick();
            } else if (hasWindow) {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(11);
//                ((NewDealViewPager) getActivity()).setCurrentPage(11);
            } else {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(13);
            }
        } else if (isFromFragment.equalsIgnoreCase(AppConstant.CAR_LEFT)) {
            if (hasWindow) {
                decisionMaker.setGoNext(true);
                decisionMaker.setIndex(-1);
//                iGoToNextPage.whatNextClick();
            } else {
                decisionMaker.setGoNext(false);
                decisionMaker.setIndex(13);
            }
        } else {
            decisionMaker.setGoNext(true);
            decisionMaker.setIndex(-1);
//            iGoToNextPage.whatNextClick();
        }
//        Log.e("Time2 ", "time: " + System.currentTimeMillis());
        return decisionMaker;
    }


    private void goToSpecificPage() {
//        Intent intent1 = new Intent(getActivity(), NewDealViewPager.class);
        PreferenceManger.putInt(AppConstant.SKIPPED_PAGE, ((NewDealViewPager) getActivity()).getCurrentPage());
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        AppConstant.shouldCall = true;
        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
            ((NewDealViewPager) getActivity()).setCurrentPage(24);
        } else {
            ((NewDealViewPager) getActivity()).setCurrentPage(54);
        }
//        iGoToNextPage.whatNextClick();
//        PreferenceManger.putInt(AppConstant.SKIPPED_PAGE,((NewDealViewPager) getActivity()).getCurrentPage());
//        intent1.putExtra("page", 33);
//        startActivity(intent1);
//        getActivity().finish();
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {

        if (carImages != null) {
            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
            Glide.with(getActivity())
                    .load(carImages)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressDialog.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressDialog.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .apply(requestOptions)
                    .into(imageView);
        }
    }

    void showDeletePopup(String title) {
        new AlertDialog.Builder(getActivity())
                .setMessage(title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (fileToDelete != null) {
                            Log.e("File Delete Request: ", "Success : >> " + fileToDelete.delete());
                        }
                        iGoToNextPage.goToBackIndex();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    public Uri ShowImageFromPath(String fileName, String mainFolder) {
        String path;

        if (mainFolder.equalsIgnoreCase("LicenseFront")) {
            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                    + "/DSXT/temp/LicenseFront/" + fileName;
        } else {
            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
        }
        File imgFile = new File(path);
        fileToDelete = imgFile;
        File filenew = null;
        try {
            if (imgFile.exists()) {
                filenew = new Compressor(requireActivity())
                        .compressToFile(imgFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Date lastModDate = new Date(imgFile.lastModified());
        Timber.i("File last modified @ : %s", lastModDate.toString());

        if (filenew != null) {
            if (filenew.exists()) {
                return Uri.fromFile(filenew);
            }
        } else {
            if (imgFile.exists()) {
                return Uri.fromFile(imgFile);
            }
        }
        return null;
    }

    IGoToNextPage iGoToNextPage;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }
}
