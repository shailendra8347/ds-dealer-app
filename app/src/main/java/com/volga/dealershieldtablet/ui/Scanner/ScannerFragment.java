package com.volga.dealershieldtablet.ui.scanner;
//Comment to commit

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.manateeworks.BarcodeScanner;
import com.manateeworks.CameraManager;
import com.manateeworks.MWOverlay;
import com.manateeworks.MWParser;
import com.volga.dealershieldtablet.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


public class ScannerFragment extends Fragment implements SurfaceHolder.Callback {
    private static final String MSG_CAMERA_FRAMEWORK_BUG = "Sorry, the Android camera encountered a problem: ";
    public static final int MWPARSER_MASK = MWParser.MWP_PARSER_MASK_NONE;
    public static final int ID_AUTO_FOCUS = 0x01;
    public static final int ID_DECODE = 0x02;
    public static final int ID_RESTART_PREVIEW = 0x04;
    public static final int ID_DECODE_SUCCEED = 0x08;
    public static final int ID_DECODE_FAILED = 0x10;

    public static final int REQUEST_CODE = 12322;

    private int maxThreads = Runtime.getRuntime().availableProcessors();
    private boolean pdfOptimised = false;
    private int activeThreads = 0;

    boolean flashOn = false;

    private int zoomLevel = 0;
    private int firstZoom = 150;
    private int secondZoom = 300;

    private Handler decodeHandler;
    private boolean hasSurface;

    private com.volga.dealershieldtablet.ui.scanner.MWBScannerListener mListener;

    private ImageButton zoomButton;
    private ImageButton buttonFlash;
    private ImageView imageOverlay;

    private enum State {
        STOPPED, PREVIEW, DECODING
    }

    public enum OverlayMode {
        OM_IMAGE, OM_MWOVERLAY, OM_NONE
    }


    private OverlayMode overlayMode = OverlayMode.OM_MWOVERLAY;
    State state = State.STOPPED;

    public Handler getHandler() {
        return decodeHandler;
    }

    public ScannerFragment() {
        // Required empty public constructor
    }

    boolean isDMVScanner;

    public void setData(boolean isDMVScanner) {
        this.isDMVScanner = isDMVScanner;
    }

    public static ScannerFragment newInstance(String param1, String param2) {
        ScannerFragment fragment = new ScannerFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        CameraManager.init(getActivity(),true);

        // Hide ActionBar
//        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        hasSurface = false;
        state = State.STOPPED;
        decodeHandler = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case ID_DECODE:
                        decode((byte[]) msg.obj, msg.arg1, msg.arg2);
                        break;

                    case ID_AUTO_FOCUS:
                        if (state == State.PREVIEW || state == State.DECODING) {
                            CameraManager.get().camera2Refocus(true);
//                            CameraManager.get().requestAutoFocus(decodeHandler, ID_AUTO_FOCUS);
                        }
                        break;
                    case ID_RESTART_PREVIEW:
                        restartPreviewAndDecode();
                        break;
                    case ID_DECODE_SUCCEED:
                        state = State.STOPPED;
                        try {
                            if (msg.obj != null) {
                                handleDecode((BarcodeScanner.MWResult) msg.obj);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case ID_DECODE_FAILED:
                        break;
                }
                return false;
            }
        });
    }

    private void decode(final byte[] data, final int width, final int height) {
        if (activeThreads >= maxThreads || state == State.STOPPED) {
            return;
        }

        new Thread(new Runnable() {
            public void run() {
                activeThreads++;

                byte[] rawResult = BarcodeScanner.MWBscanGrayscaleImage(data, width, height);

                if (state == State.STOPPED) {
                    activeThreads--;
                    return;
                }

                BarcodeScanner.MWResult mwResult = null;

                if (rawResult != null && BarcodeScanner.MWBgetResultType() == BarcodeScanner.MWB_RESULT_TYPE_MW) {

                    BarcodeScanner.MWResults results = new BarcodeScanner.MWResults(rawResult);

                    if (results.count > 0) {
                        mwResult = results.getResult(0);
                        rawResult = mwResult.bytes;
                    }

                } else if (rawResult != null
                        && BarcodeScanner.MWBgetResultType() == BarcodeScanner.MWB_RESULT_TYPE_RAW) {
                    mwResult = new BarcodeScanner.MWResult();
                    mwResult.bytes = rawResult;
                    mwResult.text = rawResult.toString();
                    mwResult.type = BarcodeScanner.MWBgetLastType();
                    mwResult.bytesLength = rawResult.length;
                }

                if (mwResult != null) {
                    state = State.STOPPED;
                    Message message = Message.obtain(getHandler(), ID_DECODE_SUCCEED, mwResult);
                    message.arg1 = mwResult.type;
                    message.sendToTarget();
                } else {
                    Message message = Message.obtain(getHandler(), ID_DECODE_FAILED);
                    message.sendToTarget();
                }

                activeThreads--;
            }
        }).start();
    }

    public void handleDecode(BarcodeScanner.MWResult result) throws JSONException {

        String typeName = result.typeName;
        String barcode = result.text;

        /* Parser */
        /*
         * Parser result handler. Edit this code for custom handling of the
         * parser result. Use MWParser.MWPgetJSON(MWPARSER_MASK,
         * result.encryptedResult.getBytes()); to get JSON formatted result
         */
//        if (MWPARSER_MASK != MWParser.MWP_PARSER_MASK_NONE &&
//                BarcodeScanner.MWBgetResultType() ==
//                        BarcodeScanner.MWB_RESULT_TYPE_MW) {
//
//            barcode = MWParser.MWPgetFormattedText(MWPARSER_MASK,
//                    result.encryptedResult.getBytes());
//            if (barcode == null) {
//                String parserMask = "";
//
//                switch (MWPARSER_MASK) {
//                    case MWParser.MWP_PARSER_MASK_AAMVA:
//                        parserMask = "AAMVA";
//                        break;
//                    case MWParser.MWP_PARSER_MASK_GS1:
//                        parserMask = "GS1";
//                        break;
//                    case MWParser.MWP_PARSER_MASK_ISBT:
//                        parserMask = "ISBT";
//                        break;
//                    case MWParser.MWP_PARSER_MASK_IUID:
//                        parserMask = "IUID";
//                        break;
//                    case MWParser.MWP_PARSER_MASK_HIBC:
//                        parserMask = "HIBC";
//                        break;
//                    case MWParser.MWP_PARSER_MASK_SCM:
//                        parserMask = "SCM";
//                        break;
//
//                    default:
//                        parserMask = "unknown";
//                        break;
//                }
//
//                barcode = result.text + "\n*Not a valid " + parserMask +
//                        " formatted barcode";
//            }
//
//        }
        /* Parser */


        if (result.locationPoints != null && CameraManager.get().

                getCurrentResolution()

                != null
                && overlayMode == OverlayMode.OM_MWOVERLAY) {
            MWOverlay.showLocation(result.locationPoints.points, result.imageWidth, result.imageHeight);
        }


        mListener.onScannedResult(result);
    }

    private void restartPreviewAndDecode() {
        if (state == State.STOPPED) {
            state = State.PREVIEW;
            Log.i("preview", "requestPreviewFrame.");
            CameraManager.get().requestPreviewFrame(getHandler(), ID_DECODE);
//            CameraManager.get().requestAutoFocus(getHandler(), ID_AUTO_FOCUS);
            CameraManager.get().camera2Refocus(true);
        }
    }

    FrameLayout previewFrame;
    com.volga.dealershieldtablet.ui.scanner.Preview preview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        previewFrame = (FrameLayout) inflater.inflate(R.layout.preview, container, false);
        preview = (com.volga.dealershieldtablet.ui.scanner.Preview) previewFrame.findViewById(R.id.rlCameraContainer);
        preview.surfaceHolder.addCallback(this);
        zoomButton = (ImageButton) previewFrame.findViewById(R.id.zoomButton);
        buttonFlash = (ImageButton) previewFrame.findViewById(R.id.flashButton);
        imageOverlay = (ImageView) previewFrame.findViewById(R.id.imageOverlay);

        return previewFrame;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        zoomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                zoomLevel++;
                if (zoomLevel > 2) {
                    zoomLevel = 0;
                }

                switch (zoomLevel) {
                    case 0:
                        CameraManager.get().setZoom(100,false);
                        break;
                    case 1:
                        CameraManager.get().setZoom(firstZoom,false);
                        break;
                    case 2:
                        CameraManager.get().setZoom(secondZoom,false);
                        break;

                    default:
                        break;
                }

            }
        });
        buttonFlash.setOnClickListener(new View.OnClickListener() {
            // @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            @Override
            public void onClick(View v) {
                toggleFlash();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof com.volga.dealershieldtablet.ui.scanner.MWBScannerListener) {
            mListener = (com.volga.dealershieldtablet.ui.scanner.MWBScannerListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MWBScannerListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        stopScaner();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        CameraManager.get().stopPreview();
//        CameraManager.get().closeDriver();
//        stopScaner();
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        stopScaner();
//    }

    private void stopScaner() {
        /* Stops the scanner when the activity goes in background */
        if (overlayMode == OverlayMode.OM_MWOVERLAY) {
            MWOverlay.removeOverlay();
        }

        imageOverlay.setImageDrawable(null);
        CameraManager.get().stopPreview(false);
        CameraManager.get().closeDriver();
//
        state = State.STOPPED;
        flashOn = false;
        updateFlash();
    }


    public void initCamera(SurfaceHolder sh) {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            /* WHEN TARGETING ANDROID 6 OR ABOVE, PERMISSION IS NEEDED */
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                new AlertDialog.Builder(getActivity()).setMessage("You need to allow access to the Camera")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.CAMERA}, 12322);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().getFragmentManager().popBackStack();
                        //TODO: Handle permission denied
                    }
                }).create().show();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                        REQUEST_CODE);
            }
        } else {
            try {
                // Select desired camera resoloution. Not all devices
                // supports all
                // resolutions, closest available will be chosen
                // If not selected, closest match to screen resolution will
                // be
                // chosen
                // High resolutions will slow down scanning proccess on
                // slower
                // devices
                if (maxThreads > 2 || pdfOptimised) {
                    CameraManager.setDesiredPreviewSize(1280, 720);
                } else {
                    CameraManager.setDesiredPreviewSize(800, 480);
                }

                CameraManager.get().openDriver(sh,
                        (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT));

                int maxZoom = CameraManager.get().getMaxZoom();
                if (maxZoom < 100) {
                    zoomButton.setVisibility(View.GONE);
                } else {
                    zoomButton.setVisibility(View.VISIBLE);
                    if (maxZoom < 300) {
                        secondZoom = maxZoom;
                        firstZoom = (maxZoom - 100) / 2 + 100;

                    }

                }
            } catch (IOException | RuntimeException ioe) {
                displayFrameworkBugMessageAndExit(ioe.getMessage());
                return;
            }

            Log.i("preview", "start preview.");
            flashOn = !isDMVScanner;

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    switch (zoomLevel) {
                        case 0:
                            CameraManager.get().setZoom(100,false);
                            break;
                        case 1:
                            CameraManager.get().setZoom(firstZoom,false);
                            break;
                        case 2:
                            CameraManager.get().setZoom(secondZoom,false);
                            break;

                        default:
                            break;
                    }

                }
            }, 300);

            //Fix for camera sensor rotation bug
            android.hardware.Camera.CameraInfo cameraInfo = new android.hardware.Camera.CameraInfo();
            android.hardware.Camera.getCameraInfo(CameraManager.USE_FRONT_CAMERA ? 1 : 0, cameraInfo);
            if (cameraInfo.orientation == 270) {
                BarcodeScanner.MWBsetFlags(0, BarcodeScanner.MWB_CFG_GLOBAL_ROTATE180);
            }

            CameraManager.get().startPreview(false);
            restartPreviewAndDecode();
            updateFlash();
        }
    }

    private void toggleFlash() {
        flashOn = !flashOn;
        updateFlash();
    }

    private void updateFlash() {
        if (!CameraManager.get().isTorchAvailable()) {
            buttonFlash.setVisibility(View.GONE);
            return;

        } else {
            buttonFlash.setVisibility(View.VISIBLE);
        }

        if (flashOn) {
            buttonFlash.setImageResource(R.drawable.flashbuttonon);
        } else {
            buttonFlash.setImageResource(R.drawable.flashbuttonoff);
        }

        CameraManager.get().setTorch(flashOn);

        buttonFlash.postInvalidate();

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(final SurfaceHolder holder, int format, int width, int height) {
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        if (!hasSurface) {

                            hasSurface = true;
                            initCamera(holder);
                        }
                    }
                });
            }
        }, 1);

    }

    private void displayFrameworkBugMessageAndExit(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(MSG_CAMERA_FRAMEWORK_BUG + message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().getFragmentManager().popBackStack();
            }
        });

        builder.show();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    public void setPdfOptimised(boolean pdfOptimised) {
        this.pdfOptimised = pdfOptimised;
    }

    public void setOverlayMode(OverlayMode overlayMode) {
        this.overlayMode = overlayMode;
    }

    public void resumeScanner() {
        if (decodeHandler != null) {
            decodeHandler.sendEmptyMessage(ID_RESTART_PREVIEW);
        }
    }
}
