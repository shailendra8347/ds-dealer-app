package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pushlink.android.PushLink;
import com.volga.dealershieldtablet.BuildConfig;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SyncUp;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealList;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.TabUniqueName;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.NetworkChangeReceiver;
import com.volga.dealershieldtablet.screenRevamping.activity.CheckIDViewPager;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PathSelection extends BaseFragment {

    private View mView;
    private RecordData recordData;
    private Gson gson = new Gson();
    private static final String FRAGMENT_DIALOG = "dialog";
    private static final int REQUEST_CAMERA_PERMISSION = 3;
    private static final int REQUEST_STORAGE = 1;
    private static final int REQUEST_PHONE_STATE = 2;
    private String deviceId;
    private static final int STORAGE_PERMISSION_CODE = 100;

    private static final String TAG = "PERMISSION_TAG";
    private DSTextView uniqueName = null;
    private ArrayList<IncompleteDealList> dealList = new ArrayList<>();
    private String uniqueDName;
    List<Record> allRecords;
    private SwipeRefreshLayout pullToRefresh;
    private LinearLayout containerNewDeal, containerPendingDeal;
    private ArrayList<Record> recordArrayList1, firstTimeDelete;
    private LinearLayout containerUploadDeal;

    private DSTextView alert;

    BroadcastReceiver broadcastReceiverAPI = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
//                if (intent.getBooleanExtra(NetworkChangeReceiver.STARTED_API_CALLS, false)) {
                if (DSAPP.getInstance().isNetworkAvailable()) {
                    showNoInterNetAlert();
                    getDealershipsSilently(false);
                    setCustomData();
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() > 0) {
                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Getting deals...");
                        progressDialog.show();
//                        progressDialog.setCancelable(false);
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                    }
                } else {
                    showNoInterNetAlert();
                    if (getActivity() != null && !getActivity().isFinishing())
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
//                }
            }
        }
    };
    private DSTextView logout, role;

    @Override
    public void onStart() {
        super.onStart();
        askCameraPermission();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_path_selection, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
        dealList.clear();
        if (getActivity() != null && !getActivity().isFinishing()) {
            getActivity().registerReceiver(broadCastNewMessage, new IntentFilter("notifyData"));
//            getActivity().registerReceiver(broadcastReceiverAPI, new IntentFilter("StartedAPI"));
        }
//                DateUtils.getTimeZone();
        String authKey = PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN);
//        Log.e("Auth-key ", "onCreateView: " + authKey);
        try {
            initView(false);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//            }
//        });
    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    Log.e("Refresh: ", gson.toJson(loginData));
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
//                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//                    progressDialog.setMessage("Refreshing pending deals...");
//                    progressDialog.show();
//                    progressDialog.setCancelable(false);
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() != 0) {
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), null);
                    }
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Logout: ", "logout from path during refresh" + json.toString());
                        logoutInBase();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
            }
        });
    }

    private void getUniqueName(String access_token, String deviceId) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Getting deals...");
        progressDialog.show();
//        progressDialog.setCancelable(false);
        RetrofitInitialization.getDs_services().getUniqueTabName(access_token, deviceId).enqueue(new Callback<TabUniqueName>() {
            @Override
            public void onResponse(Call<TabUniqueName> call, Response<TabUniqueName> response) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    progressDialog.dismiss();
                }
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.UNIQU_TAB_OBJECT, json);
                    uniqueDName = response.body().getDeviceName();
                    uniqueName.setText(MessageFormat.format("Tab Name: {0}", response.body().getDeviceName()));
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() != 0)
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                } else {

                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
                                new CustomToast(getActivity()).toast(arr[1]);
                                Log.e("Logout: ", "logout from path during inactive" + json.toString());
                                logoutInBase();
                            } else {
                                if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                    callRefreshTokenAPI();
                                } else {
                                    Log.e("Logout: ", "logout from path during zero length" + json.toString());
                                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                    Locale myLocale = new Locale("en");
                                    Resources res = getResources();
                                    DisplayMetrics dm = res.getDisplayMetrics();
                                    Configuration conf = res.getConfiguration();
                                    conf.locale = myLocale;
                                    res.updateConfiguration(conf, dm);
                                    callHomeActivity();
                                }
                            }

                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    Log.e("Response ", "onResponse: " + json);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TabUniqueName> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
                if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                    progressDialog.dismiss();
//                Log.e("API failed tab unique: ", t.getLocalizedMessage());
            }

        });
    }

    BroadcastReceiver broadCastNewMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("Data update: ", "Data updated from service");
            if (getActivity() != null) {
                try {
                    if (intent.getStringExtra("value").trim().equalsIgnoreCase("logOut")) {
                        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                        Locale myLocale = new Locale("en");
                        Resources res = getResources();
                        DisplayMetrics dm = res.getDisplayMetrics();
                        Configuration conf = res.getConfiguration();
                        conf.locale = myLocale;
                        res.updateConfiguration(conf, dm);
                        callHomeActivity();
                    } else if (intent.getStringExtra("value").equalsIgnoreCase("refreshIncompleteDeal")) {
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() != 0) {
                                dealList.clear();
                                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                progressDialog.setMessage("Getting deals...");
                                progressDialog.show();
//                            progressDialog.setCancelable(false);
                                getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                            }
                        } else {
                            if (getActivity() != null && !getActivity().isFinishing())
                                new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }
                    } else {
                        initView(true);
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() != 0) {
                                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                progressDialog.setMessage("Getting deals...");
                                progressDialog.show();
//                            progressDialog.setCancelable(false);
                                dealList.clear();
                                new CustomToast(getActivity()).alert(intent.getStringExtra("value"));
                                getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                            }
                        } else {
                            if (getActivity() != null && !getActivity().isFinishing())
                                new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private NetworkChangeReceiver receiver;

    private void setCustomData() {
        String tabName = "", pendingDeal, CompletedDeal;
        int pending = 0, complete = 0;
        try {
            tabName = PreferenceManger.getUniqueTabName().getDeviceName() + "";
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            List<Record> list = recordData.getRecords();
            for (int i = 0; i < list.size(); i++) {
                if (!list.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                    pending++;
                } else {
                    complete++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        PushLink.addMetadata("Brand", Build.BRAND);
        PushLink.addMetadata("Model", Build.MODEL);
        PushLink.addMetadata("OS Version", Build.VERSION.RELEASE);
        PushLink.addMetadata("Logged in user", PreferenceManger.getStringValue(AppConstant.USER_NAME));
        PushLink.addMetadata("Email", PreferenceManger.getStringValue(AppConstant.USER_MAIL));
        PushLink.addMetadata("DealershipName", PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
        PushLink.addMetadata("Tab Name", tabName);
        PushLink.addMetadata("Pending Deals", pending + "");
        PushLink.addMetadata("Deals to be Uploaded", complete + "");

//        PushLink.addMetadata("Error", "");
    }

    @SuppressLint("HardwareIds")
    @Override
    public void onResume() {
        super.onResume();
//        getActivity().registerReceiver(broadcastReceiverAPI, new IntentFilter("StartedAPI"));
        try {
            logout = mView.findViewById(R.id.userFName);
            role = mView.findViewById(R.id.role);
            logout.setHorizontallyScrolling(true);
            logout.setSelected(true);
            role.setText(PreferenceManger.getStringValue(AppConstant.USER_ROLE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        PackageManager pm = DSAPP.getInstance().getPackageManager();
        String pkgName = DSAPP.getInstance().getPackageName();
        PackageInfo pkgInfo = null;
        try {
            pkgInfo = pm.getPackageInfo(pkgName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String ver = pkgInfo.versionName;
        /*With latest release change with live one for live <25*/
        if (pkgInfo.versionCode < 25) {
            role.setVisibility(View.GONE);
            logout.setText("Logout");
        } else {
            role.setVisibility(View.VISIBLE);
            logout.setText(PreferenceManger.getStringValue(AppConstant.USER_NAME));
        }

//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED)
//            askStoragePermission();
        if (!checkPermission()) {
            requestPermission();
        }
        showNoInterNetAlert();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        receiver = new NetworkChangeReceiver();
        requireActivity().registerReceiver(receiver, filter);
        requireActivity().registerReceiver(broadcastReceiverAPI, new IntentFilter("StartedAPI"));
        setCustomData();
        getDealershipsSilently(false);
        dealList.clear();

        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {
//            initPushLink();
        }
//        else if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            FragmentLogin.ConfirmationDialogFragment
//                    .newInstance(R.string.phone_state_permission,
//                            new String[]{Manifest.permission.READ_PHONE_STATE},
//                            REQUEST_PHONE_STATE,
//                            R.string.phone_state_not_granted)
//                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
//        } else {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE},
//                    REQUEST_PHONE_STATE);
//        }
        if (!checkPermission()) {
            requestPermission();
        }

    }

    private void showNoInterNetAlert() {
        if (alert != null) {
            Gson gson = new GsonBuilder().create();
            DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
            if (dealershipData != null) {
                for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                    if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                        PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(i).getDealershipName());
                        PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(i).isAllowCheckID());
                        PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(i).isCaptureCustomerPhotoOnSign());
                        PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(i).isCaptureScreenOnSign());
                        PreferenceManger.putBoolean(AppConstant.VIEW_MANDATORY, dealershipData.getDealerships().get(i).isViewThirdPartyReportMandatory());
                        PreferenceManger.putBoolean(AppConstant.USE_THIRD_PARTY_API, dealershipData.getDealerships().get(i).isUseThirdPartyAPI());
                        PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(i).isUsedCarOnly());
                        PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(i).isProcessTradeIn());
                        PreferenceManger.putBoolean(AppConstant.PROCESS_MULTI_SIGN, dealershipData.getDealerships().get(i).isThirdPartyMultiSign());
                        PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(i).isTakePhotoOfProofOfInsurance());
                        PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(i).isTakePhotoOfVehicle());
                        PreferenceManger.putBoolean(AppConstant.IS_AI_ENABLED, dealershipData.getDealerships().get(i).isAIFeatureEnabled());
                        if (dealershipData.getDealerships().get(i).getAlerts() != null && dealershipData.getDealerships().get(i).getAlerts().size() > 0) {
//            showAlert(PreferenceManger.getStringValue(AppConstant.ALERT));
                            StringBuilder main = new StringBuilder();
                            if (!DSAPP.getInstance().isNetworkAvailable()) {
                                main.append("Internet is not available").append("\n");
                            }
                            for (int j = 0; j < dealershipData.getDealerships().get(i).getAlerts().size(); j++) {
                                main.append(dealershipData.getDealerships().get(i).getAlerts().get(j)).append("\n");
                            }
                            if (dealershipData.getDealerships().get(i).getAlerts().size() == 1)
                                alert.setText("Alert");
                            else if (dealershipData.getDealerships().get(i).getAlerts().size() > 1) {
                                alert.setText(String.format("Alerts(%d)", dealershipData.getDealerships().get(i).getAlerts().size()));
                            }
                            final String finalMain = main.toString();
                            alert.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showPopup(finalMain);
                                }
                            });
                            alert.setVisibility(View.VISIBLE);
                        } else {
                            if (!DSAPP.getInstance().isNetworkAvailable()) {
                                alert.setText("Alert");
                                final String finalMain = "Internet is not available";
                                alert.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showPopup(finalMain);
                                    }
                                });
                                alert.setVisibility(View.VISIBLE);
                            } else
                                alert.setVisibility(View.GONE);
                        }
                    }
                }
            }

        }
    }

    private void askCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            FragmentScanDMVFront.ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    private void askStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            FragmentLogin.ConfirmationDialogFragment
                    .newInstance(R.string.storage_permission_confirmation,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_STORAGE,
                            R.string.storage_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            requireActivity().unregisterReceiver(this.broadCastNewMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        requireActivity().unregisterReceiver(receiver);
        requireActivity().unregisterReceiver(this.broadcastReceiverAPI);
    }

    @SuppressLint({"HardwareIds", "SetTextI18n"})
    private void initView(boolean fromService) throws ParseException {
        final AppCompatButton new_customer_button = mView.findViewById(R.id.new_customer_button);
        AppCompatButton current_customer_button = mView.findViewById(R.id.current_customer_button);
        final AppCompatButton checkIDButton = mView.findViewById(R.id.check_id);
        AppCompatButton historyCheckButton = mView.findViewById(R.id.history_check);
        alert = mView.findViewById(R.id.alert);
        AppCompatButton view_synced_records_button = mView.findViewById(R.id.view_synced_records_button);
        String versionName = BuildConfig.VERSION_NAME;
        pullToRefresh = (SwipeRefreshLayout) mView.findViewById(R.id.pullToRefresh);
        LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        uniqueName = mView.findViewById(R.id.uniqueName);
//        String base64 = "";
//        ImageView imageView = null;
//        Glide.with(this).load(base64).into(imageView);
        if (PreferenceManger.getUniqueTabName() != null) {
            uniqueName.setText(MessageFormat.format("Tab Name: {0}", PreferenceManger.getUniqueTabName().getDeviceName()));
        }
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (DSAPP.getInstance().isNetworkAvailable()) {
                    getThirdParty();
                    showNoInterNetAlert();
                    getDealershipsSilently(true);
                    if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() > 0) {
                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setMessage("Getting deals...");
//                        progressDialog.show();
//                        progressDialog.setCancelable(false);
                        getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                    }
                } else {
                    showNoInterNetAlert();
                    if (getActivity() != null && !getActivity().isFinishing())
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
                pullToRefresh.setRefreshing(false);
            }
        });

        DSTextView vName = mView.findViewById(R.id.versionNumber) /*= null*/;

        checkIDButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIDButton.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        checkIDButton.setEnabled(true);
                    }
                }, 3000);
                Gson gson = new GsonBuilder().create();
                DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
                if (dealershipData != null) {
//                    if (dealershipData.getDealerships() != null && dealershipData.getDealerships().size() > 1) {
//                        Select_Dealership select_dealership = new Select_Dealership();
//                        Bundle bundle = new Bundle();
//                        bundle.putBoolean("checkId", true);
//                        select_dealership.setArguments(bundle);
//                        next(select_dealership);
//                    } else if (dealershipData.getDealerships() != null && dealershipData.getDealerships().size() == 1) {

//                    }

                    if (PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) != 0) {
                        Intent intent1 = new Intent(getActivity(), CheckIDViewPager.class);
//                        PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(0).getId());
                        intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                        intent1.putExtra("page", 0);
                        startActivity(intent1);
                    } else {
                        new CustomToast(getActivity()).alert("Please select the Dealership.");
                    }
                } else {
                    getDealerships(true);
                }

            }
        });
        historyCheckButton.setVisibility(View.GONE);
        historyCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        /*Staging push link*/
//        vName.setText(MessageFormat.format("Current Versbion: {0}.4_minLead_Staging", versionName));
        /*................*/
        vName.setText(MessageFormat.format("Current Version: {0}.275_Staging", versionName));
//        vName.setText(MessageFormat.format("Current Version: {0}.1_Dev", versionName));
//        vName.setText(MessageFormat.format("Current Version: {0}", versionName));
        if (!fromService) {
            PreferenceManger.putString(AppConstant.DMV_NUMBER, "");
        }
        getThirdParty();
        getConditionDisclosure();
        getConditionDisclosureNew(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "");
        getHistoryDisclosure();
        ImageView newDeal = mView.findViewById(R.id.newdeal);
        ImageView pending = mView.findViewById(R.id.pendingDeal);
        ImageView upload = mView.findViewById(R.id.uploadDeal);
        ImageView checkID = mView.findViewById(R.id.checkid_info);
        ImageView vehicleHistoryCheck = mView.findViewById(R.id.historycheck_info);
        containerNewDeal = mView.findViewById(R.id.containerNewDeal);
        containerPendingDeal = mView.findViewById(R.id.containerPendingDeal);
        containerUploadDeal = mView.findViewById(R.id.containeruploadDeal);
        new_customer_button.setOnClickListener(view -> {

            new_customer_button.setEnabled(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new_customer_button.setEnabled(true);
                }
            }, 3000);
            AppConstant.restrictedPage = -1;
//            if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    == PackageManager.PERMISSION_GRANTED)
            if (checkPermission()) {
                Gson gson = new GsonBuilder().create();
                DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
                if (dealershipData != null) {
                    if (PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) != 0) {
                        Intent intent1 = new Intent(getActivity(), NewDealViewPager.class);
//                        PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(0).getId());
                        intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                        if (PreferenceManger.getBooleanValue(AppConstant.IS_USEDCAR_ONLY)) {
                            intent1.putExtra("page", 1);
                            Log.e("Is Used car", "true");
                            initiateDeal();
                        } else {
                            intent1.putExtra("page", 0);
                            Log.e("Is Used car", "false");
                        }
                        intent1.putExtra("usedCarOnly", PreferenceManger.getBooleanValue(AppConstant.IS_USEDCAR_ONLY));
                        startActivity(intent1);
                    } else {
                        new CustomToast(getActivity()).alert("Please select the Dealership.");
                    }
                } else {
                    getDealerships(false);
                }
            } else {
                requestPermission();
            }

        });


        current_customer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppConstant.restrictedPage = -1;
                replaceFragment(R.id.container, new IncompleteCustomers(), IncompleteCustomers.class.getSimpleName());
//                throw new ArithmeticException("/ by zero");
            }
        });


        view_synced_records_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(R.id.container, new RecordsYetToBeSynced(), RecordsYetToBeSynced.class.getSimpleName());
            }
        });
        Timber.tag("User path selection: ").e(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        refreshLocalData();

//        if (getActivity() != null && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                == PackageManager.PERMISSION_GRANTED)
        if (checkPermission()) {
            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
            if (PreferenceManger.getUniqueTabName() == null || PreferenceManger.getUniqueTabName().getDeviceName() == null || PreferenceManger.getUniqueTabName().getDeviceName().length() == 0)
                getUniqueName("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), deviceId);
            else {
                if (dealList == null || dealList.size() == 0) {
                    uniqueName.setText(MessageFormat.format("Tab Name: {0}", PreferenceManger.getUniqueTabName().getDeviceName()));
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getCompanyId() != 0) {
                            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Getting deals...");
                            progressDialog.show();
//                        progressDialog.setCancelable(false);
                            getIncompleteDealList(PreferenceManger.getUniqueTabName().getCompanyId(), progressDialog);
                        }
                    } else {
                        if (getActivity() != null && !getActivity().isFinishing())
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }
                }
            }
        }

        newDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showReportDialog();
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                DialogFragment newFragment = new ReportDeveloperFragmentDialog();
//                Bundle bundle = new Bundle();
//                bundle.putString("data", "test mages");
//                bundle.putString("email", "shailendra.m@volgainfotech.com");
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "dialog");
                showPopup(getString(R.string.start_new));
            }
        });
        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(getString(R.string.pending_start));
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(getString(R.string.need_to_upload));
            }
        });
        checkID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup("For ID scanning only. No deals will be uploaded.");
//                showPopup(getString(R.string.pending_start));
            }
        });
        vehicleHistoryCheck.setVisibility(View.GONE);
        vehicleHistoryCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showPopup(getString(R.string.pending_start));
            }
        });
        logout = mView.findViewById(R.id.userFName);
        role = mView.findViewById(R.id.role);
        logout.setHorizontallyScrolling(true);
        logout.setSelected(true);
        role.setText(PreferenceManger.getStringValue(AppConstant.USER_ROLE));
        PackageManager pm = DSAPP.getInstance().getPackageManager();
        String pkgName = DSAPP.getInstance().getPackageName();
        PackageInfo pkgInfo = null;
        try {
            pkgInfo = pm.getPackageInfo(pkgName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String ver = pkgInfo.versionName;
        /*With latest release change with live one for live <25*/
        if (pkgInfo.versionCode < 25) {
            role.setVisibility(View.GONE);
            logout.setText("Logout");
        } else {
            role.setVisibility(View.VISIBLE);
            logout.setText(PreferenceManger.getStringValue(AppConstant.USER_NAME));
        }
        DSTextView cancel = mView.findViewById(R.id.text_cancelBack);

        DSTextView change = mView.findViewById(R.id.change);
        cancel.setHorizontallyScrolling(true);
        cancel.setSelected(true);
//        cancel.setTextSize(R.dimen._9ssp);
        cancel.setVisibility(View.VISIBLE);

        Gson gson = new GsonBuilder().create();
        DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
        if (dealershipData != null) {
            for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                    PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(i).getDealershipName());
                    PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(i).isAllowCheckID());
                    PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(i).isCaptureCustomerPhotoOnSign());

                    PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(i).isUsedCarOnly());
                    PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(i).isCaptureScreenOnSign());
                    PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(i).isProcessTradeIn());
                    PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(i).isTakePhotoOfProofOfInsurance());
                    PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(i).isTakePhotoOfVehicle());
                    PreferenceManger.putBoolean(AppConstant.VIEW_MANDATORY, dealershipData.getDealerships().get(i).isViewThirdPartyReportMandatory());
                    PreferenceManger.putBoolean(AppConstant.USE_THIRD_PARTY_API, dealershipData.getDealerships().get(i).isUseThirdPartyAPI());
                    PreferenceManger.putBoolean(AppConstant.IS_AI_ENABLED, dealershipData.getDealerships().get(i).isAIFeatureEnabled());
                    PreferenceManger.putBoolean(AppConstant.PROCESS_MULTI_SIGN, dealershipData.getDealerships().get(i).isThirdPartyMultiSign());
                    if (dealershipData.getDealerships().get(i).getAlerts() != null && dealershipData.getDealerships().get(i).getAlerts().size() > 0) {
//            showAlert(PreferenceManger.getStringValue(AppConstant.ALERT));
                        StringBuilder main = new StringBuilder();
                        if (!DSAPP.getInstance().isNetworkAvailable()) {
                            main.append("Internet is not available").append("\n");
                        }
                        for (int j = 0; j < dealershipData.getDealerships().get(i).getAlerts().size(); j++) {
                            main.append(dealershipData.getDealerships().get(i).getAlerts().get(j)).append("\n");
                        }
                        if (dealershipData.getDealerships().get(i).getAlerts().size() == 1)
                            alert.setText("Alert");
                        else if (dealershipData.getDealerships().get(i).getAlerts().size() > 1) {
                            alert.setText(String.format("Alerts(%d)", dealershipData.getDealerships().get(i).getAlerts().size()));
                        }
                        final String finalMain = main.toString();
                        alert.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showPopup(finalMain);
                            }
                        });
                        alert.setVisibility(View.VISIBLE);
                    } else {
                        if (!DSAPP.getInstance().isNetworkAvailable()) {
                            alert.setText("Alert");
                            final String finalMain = "Internet is not available";
                            alert.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showPopup(finalMain);
                                }
                            });
                            alert.setVisibility(View.VISIBLE);
                        } else
                            alert.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            getDealerships(false);
        }
        if (PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME).trim().length() == 0) {
            if (dealershipData != null) {
                for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                    if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                        cancel.setText(dealershipData.getDealerships().get(i).getDealershipName());
                    }
                }
            } else {
                getDealerships(false);
            }
        } else {
            cancel.setText(PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
        }
        if (dealershipData != null && dealershipData.getDealerships().size() == 1) {
            change.setVisibility(View.GONE);
        } else {
            if (PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) == 0) {
                change.setText("Select Dealership");
            } else {
                change.setText(getString(R.string.change_dealer));
            }
            change.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.changeDealer).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceFragment(R.id.container, new Select_Dealership(), Select_Dealership.class.getSimpleName());
                }
            });
        }
        LinearLayout userName = mView.findViewById(R.id.userName);
        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(logout, getActivity());
                logout();
            }
        });
    }

    private void initiateDeal() {
        String rand_int1;
//        if (PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//            rand_int1 = String.format(Locale.getDefault(), "%09d", rand.nextInt(100000001));
        rand_int1 = System.currentTimeMillis() + "";/*String.format(Locale.getDefault(), "%09d", rand.nextInt(100000001));*/
//            System.currentTimeMillis();
        Log.e("Random number: ", rand_int1);
        PreferenceManger.putString(AppConstant.DMV_NUMBER, rand_int1);
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        if (PreferenceManger.getStringValue(AppConstant.RECORD_DATA).length() != 0) {
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        } else {
            recordData = new RecordData();
        }
        Record record = new Record();

        CustomerDetails customerDetails = new CustomerDetails();
        CoBuyerCustomerDetails coBuyerCustomerDetails = new CoBuyerCustomerDetails();
        VehicleDetails vehicleDetails = new VehicleDetails();
        TradeInVehicle tradeInVehicle = new TradeInVehicle();
        SettingsAndPermissions settingsAndPermissions = new SettingsAndPermissions();
        settingsAndPermissions.setLocalLogs("");
        SyncUp syncUp = new SyncUp();
        DealershipDetails dealershipDetails = new DealershipDetails();
        dealershipDetails.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
        dealershipDetails.setDealershipName(PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
        DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
        if (dealershipData != null) {
            for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                    dealershipDetails.setVehiclePhotoTypes(dealershipData.getDealerships().get(i).getDealershipVehiclePhotoTypes());
                    break;
                }
            }
        }
//        settingsAndPermissions.setLastPageIndex(((NewDealViewPager) Objects.requireNonNull(getActivity())).getCurrentPage());
        settingsAndPermissions.setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        Timber.e(Calendar.getInstance().getTime().toString());
        vehicleDetails.setTypeOfVehicle("Used");
        customerDetails.setDLNumber(rand_int1);
        record.setCustomerDetails(customerDetails);
        record.setCoBuyerCustomerDetails(coBuyerCustomerDetails);
        record.setDealershipDetails(dealershipDetails);
        record.setTradeInVehicle(tradeInVehicle);
        record.setSettingsAndPermissions(settingsAndPermissions);
        record.setSyncUp(syncUp);
        record.setVehicleDetails(vehicleDetails);
        recordData.getRecords().add(record);
        //Add the recordData object to the SharedPreference after converting it to string
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
    }

    private void showAlert(String stringValue) {
        AlertDialog.Builder alertD = new AlertDialog.Builder(getActivity());
        final AlertDialog alert = alertD.create();
        alertD.setTitle("Alert.")
                .setMessage(stringValue)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("I understand", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).show().setCancelable(false);
    }

    private void showReportDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.report_error, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);

        DSTextView save = promptView.findViewById(R.id.save);
        final DSTextView close = promptView.findViewById(R.id.close);
        DSTextView title = promptView.findViewById(R.id.title);
//        title.setText(list.get(position).getName());
        final DSEdittext data = promptView.findViewById(R.id.addCondition);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyboardUtils.hideSoftKeyboard(data, getActivity());
                alertD.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(data, getActivity());
                alertD.dismiss();
                /*Upload all data to server*/
                String feedbackText = data.getText().toString();
                if (getArguments() != null) {
                    String subjectData;
                    if (feedbackText.length() > 0) {
                        subjectData = getArguments().getString("data") + " \n Feedback data: " + feedbackText + ": ";
                    } else {
                        subjectData = getArguments().getString("data");
                    }
                    writeCustomLogsAPI("0", subjectData, "XBDHK874NFJDJKS", "HONDA", "JAZZ");
                }
                alertD.dismiss();

            }
        });
        alertD.setView(promptView);
        alertD.show();
    }

    private void writeCustomLogsAPI(String id, String logs, String vin, String make, String
            model) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Submitting feedback..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setDealershipId("50354");
        customLogs.setEmailSubject(null);
        customLogs.setIsSendAlertEmail(true);
//        ArrayList<String> emails=new ArrayList<>();
//        emails.add("shailendra.m@volgainfotech.com");
//        emails.add(getArguments().getString("data"));
//        customLogs.setEmails(emails);
        customLogs.setLogText("<div><b>VIN:</b> " + vin + " <br><b>Make:</b> " + make + " <br><b>Model:</b> " + model + " <br><b>JSON DATA: </b><br>" + logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + "</div>");
//        customLogs.setLogText(logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                progressDialog.dismiss();
                new CustomToast(getActivity()).alert("Submitted");
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void refreshLocalData() throws ParseException {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        ArrayList<Record> recordArrayList = new ArrayList<>();
        recordArrayList1 = new ArrayList<>();
        firstTimeDelete = new ArrayList<>();
        ArrayList<Record> needToDelete = new ArrayList<>();
        ArrayList<Record> syncedRecord = new ArrayList<>();
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

        updateEditFlag();
        if (recordData != null) {
            allRecords = recordData.getRecords();
            for (int i = 0; i < allRecords.size(); i++) {
                if (allRecords.get(i).getSettingsAndPermissions() != null && ((allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete() && !allRecords.get(i).getSettingsAndPermissions().isDataSyncedWithServer()) || allRecords.get(i).getSettingsAndPermissions().isSentEmailForIncomplete())) {
                    recordArrayList.add(allRecords.get(i));
                }
//                if (allRecords.get(i).getSettingsAndPermissions() != null && !allRecords.get(i).getSettingsAndPermissions().isIncompleteDeal() && allRecords.get(i).getSettingsAndPermissions().isFromServer()) {
//                    needToDelete.add(allRecords.get(i));
//                }
                //replaces is bussiness lead from isbussinesslead synced v1.3.0
                if (!allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete() && !allRecords.get(i).getSettingsAndPermissions().isDataSyncedWithServer() && !allRecords.get(i).getSettingsAndPermissions().isBussinessLead() && !allRecords.get(i).getSettingsAndPermissions().isSentEmailForIncomplete()) {
                    Date date = formatter.parse(allRecords.get(i).getSettingsAndPermissions().getLatestTimeStamp());
                    long lastTime = date.getTime();
                    long currentTime = System.currentTimeMillis();
                    long diff = currentTime - lastTime;
                    int hours = (int) ((currentTime - lastTime) / (1000 * 60 * 60));
//                    int hours = (int) ((currentTime - lastTime) / (1000 * 60));
                    if (!allRecords.get(i).getSettingsAndPermissions().isFromServer()) {
                        if (hours <= 12) {
                            recordArrayList1.add(allRecords.get(i));
                            firstTimeDelete.add(allRecords.get(i));
                        } else {
                            {
                                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                recordData.where(allRecords.get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLead(true);
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
//                                homeIntent.addCategory( Intent.CATEGORY_HOME );
//                                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(homeIntent);
                            }
                        }
                    } else {
                        recordArrayList1.add(allRecords.get(i));
                    }
                }
                if (allRecords.get(i).getSettingsAndPermissions().isDataSyncedWithServer()) {
                    syncedRecord.add(allRecords.get(i));
                }
                //|| (allRecords.get(i).getSettingsAndPermissions().isIncompleteDeal() && allRecords.get(i).getSettingsAndPermissions().isIncompleteDealSynced())
//                add this later in itrcntvity
                if ((allRecords.get(i).getSettingsAndPermissions().isBussinessLead() && allRecords.get(i).getSettingsAndPermissions().isBussinessLeadSynced())) {
                    needToDelete.add(allRecords.get(i));
                }
                if ((allRecords.get(i).getSettingsAndPermissions().isFromServer() && allRecords.get(i).getSettingsAndPermissions().isIncompleteDealSynced() && !allRecords.get(i).getSettingsAndPermissions().isSentEmailForIncomplete())) {
                    needToDelete.add(allRecords.get(i));
                }
            }

        }
        if (!PreferenceManger.getBooleanValue("DeleteLocal")) {
            PreferenceManger.putBoolean("DeleteLocal", true);
            deleteRecordsOlderThan2Hours(firstTimeDelete);
        }
        deleteRecordsOlderThan2Hours(needToDelete);
        deleteRecordsOlderThan2Hours(syncedRecord);
        ArrayList<Record> dealershipSpecificDeals = new ArrayList<>();

        for (int i = 0; i < recordArrayList1.size(); i++) {
            if (recordArrayList1.get(i).getDealershipDetails().getDealershipId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                dealershipSpecificDeals.add(recordArrayList1.get(i));
            }
        }
        recordArrayList1.clear();
        recordArrayList1.addAll(dealershipSpecificDeals);
        Collections.sort(recordArrayList, new Comparator<Record>() {
            @Override
            public int compare(Record o1, Record o2) {
                return Integer.compare(o1.getSettingsAndPermissions().getSequenceOrder(), o2.getSettingsAndPermissions().getSequenceOrder());
            }
        });
        if (recordArrayList.size() == 0) {
            containerUploadDeal.setVisibility(View.GONE);
        } else {
            containerUploadDeal.setVisibility(View.VISIBLE);
        }
        if (recordArrayList1.size() > 0) {
            containerPendingDeal.setVisibility(View.VISIBLE);
        } else {
            containerPendingDeal.setVisibility(View.GONE);
        }

    }

    private void updateEditFlag() {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null) {
            List<Record> list = recordData.getRecords();
            for (int i = 0; i < list.size(); i++) {
                list.get(i).getSettingsAndPermissions().setEditing(false);
            }
            recordData.setRecords(list);
            String recordDataString1 = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
        }
    }

    private void getIncompleteDealList(int companyId, final ProgressDialog progressDialog) {
        if (getArguments() != null && getArguments().getBoolean("finalSave")) {
            progressDialog.dismiss();
            return;
        }
        Log.e("getIncompleteDeal Path", " In function");
        dealList.clear();
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        String currentTime = null;
        try {
            currentTime = tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        RetrofitInitialization.getDs_services().getRecordList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), companyId + "", currentTime, PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "").enqueue(new Callback<ArrayList<IncompleteDealList>>() {
            @Override
            public void onResponse(Call<ArrayList<IncompleteDealList>> call, Response<ArrayList<IncompleteDealList>> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("getIncompleteDeal Path", " In success");
                    dealList.addAll(response.body());
                    gson = new GsonBuilder().create();
                    recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null) {
                        List<Record> records = recordData.getRecords();
                        ArrayList<Record> needToDelete = new ArrayList<>();
                        for (int i = 0; i < records.size(); i++) {
                            if (!records.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                                if (records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && (!records.get(i).getSettingsAndPermissions().isSentEmailForIncomplete() && !records.get(i).getSettingsAndPermissions().getIsRecordComplete() && records.get(i).getSettingsAndPermissions().isFromServer())) {
                                    boolean isExist = false;
                                    for (int j = 0; j < dealList.size(); j++) {
                                        if (Integer.parseInt(records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId()) == dealList.get(j).getCustomerVehicleTradeId()) {
                                            isExist = true;
                                            break;
                                        }
                                    }
                                    if (!isExist) {
                                        Log.e(" Not Exist path: ", "Deal is dead");
                                        needToDelete.add(records.get(i));
                                    }
                                }
                            }
                        }
                        deleteRecordsOlderThan2Hours(needToDelete);
                    }
                    for (int i = 0; i < dealList.size(); i++) {
                        updateRecordThroughVTD(dealList.get(i), i);
                    }
                    try {
                        refreshLocalData();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                        progressDialog.dismiss();
//                    if (dealList.size() > 0 || recordArrayList1.size() > 0) {
//                        containerPendingDeal.setVisibility(View.VISIBLE);
//                    } else {
//                        containerPendingDeal.setVisibility(View.GONE);
//                    }
                } else {
                    Timber.e(" In failure");
                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
                                new CustomToast(getActivity()).toast(arr[1]);
                                Log.e("Logout: ", "logout from path during inactive" + json.toString());
                                logoutInBase();
                            } else {
                                if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                    callRefreshTokenAPI();
                                } else {
                                    Log.e("Logout: ", "logout from path during zero length" + json.toString());
                                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                    Locale myLocale = new Locale("en");
                                    Resources res = getResources();
                                    DisplayMetrics dm = res.getDisplayMetrics();
                                    Configuration conf = res.getConfiguration();
                                    conf.locale = myLocale;
                                    res.updateConfiguration(conf, dm);
                                    callHomeActivity();
                                }
                            }

                        }
                        if (json.getString("Message").equalsIgnoreCase("No incomplete deals found")) {
                            gson = new GsonBuilder().create();
                            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null) {
                                List<Record> records = recordData.getRecords();
                                ArrayList<Record> needToDelete = new ArrayList<>();
                                for (int i = 0; i < records.size(); i++) {
                                    if (!records.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                                        if (records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && records.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && ((!records.get(i).getSettingsAndPermissions().isSentEmailForIncomplete()) && records.get(i).getSettingsAndPermissions().isFromServer())) {
                                            Log.e(" Not Exist: ", "Deal is dead");
                                            needToDelete.add(records.get(i));
                                        }
                                    }
                                }
                                deleteRecordsOlderThan2Hours(needToDelete);
                            }
                        }

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        refreshLocalData();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Log.e("Response ", "onResponse: " + json);
                    if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                        progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<IncompleteDealList>> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
                if (progressDialog != null && getActivity() != null && !getActivity().isFinishing())
                    progressDialog.dismiss();
            }
        });
    }


    void showPopup(String title) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
        final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);
        final DSTextView titleTV = promptView.findViewById(R.id.title);
        DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
        titleTV.setVisibility(View.GONE);
        messageTV.setText(title);
        final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
        stopDeal.setVisibility(View.GONE);
        DSTextView ok = promptView.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertD.dismiss();
            }
        });
        alertD.setView(promptView);
        alertD.show();
//        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
//                .setMessage(title)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//
//                    public void onClick(DialogInterface dialog, int whichButton) {
////                        showNoInterNetAlert();
//                        dialog.dismiss();
//                    }
//                }).show();
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }

    public void getDealerships(final boolean b) {
        String time = PreferenceManger.getStringValue(AppConstant.TIME_STAMP);
        int min = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
            Date date = formatter.parse(time);
            long lastTime = date.getTime();
            long currentTime = System.currentTimeMillis();
            long diff = currentTime - lastTime;
            min = (int) ((currentTime - lastTime) / (1000 * 60));
        } catch (ParseException e) {
            Timber.e(e.getLocalizedMessage());
        }
        if (min >= 1 || time.length() == 0) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Fetching Dealerships");
            progressDialog.show();
            progressDialog.setCancelable(false);
            Call<DealershipData> get_dealerships = RetrofitInitialization.getDs_services().get_dealerships("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
            get_dealerships.enqueue(new Callback<DealershipData>() {
                @Override
                public void onResponse(Call<DealershipData> call, Response<DealershipData> response) {
                    if (response.code() == 200 && response.isSuccessful()) {
                        DealershipData dealershipData = response.body();
                        Gson gson = new GsonBuilder().create();
                        String dealershipDataString = gson.toJson(response.body());
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, dealershipDataString);
                        if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        assert dealershipData != null;
                        if (dealershipData.getDealerships() != null && dealershipData.getDealerships().size() > 1) {
                            Select_Dealership select_dealership = new Select_Dealership();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("checkId", b);
                            select_dealership.setArguments(bundle);
                            next(select_dealership);
                        } else if (dealershipData.getDealerships() != null && dealershipData.getDealerships().size() == 1) {
                            Intent intent1;
                            if (b) {
                                intent1 = new Intent(getActivity(), CheckIDViewPager.class);
                            } else {
                                intent1 = new Intent(getActivity(), NewDealViewPager.class);
                            }
                            PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(0).getId());
                            intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                            intent1.putExtra("page", 0);
                            startActivity(intent1);
                        }
                    } else {
                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());

                            showPopup(json.getString("error_description"));
                            Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());

                            if (response.code() == 203) {
                                logoutInBase();
                            }
                            if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<DealershipData> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    if (getActivity() != null && !getActivity().isFinishing())
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
                    if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        }
    }


    public void getDealershipsSilently(boolean b) {
        // condition for recursive calls
        String time = PreferenceManger.getStringValue(AppConstant.TIME_STAMP);
        int min = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
            Date date = formatter.parse(time);
            long lastTime = date.getTime();
            long currentTime = System.currentTimeMillis();
            long diff = currentTime - lastTime;
            min = (int) ((currentTime - lastTime) / (1000 * 60));
        } catch (ParseException e) {
            Timber.e(e.getLocalizedMessage());
        }
        if (min >= 1 || time.length() == 0) {
            Log.e("called ", "Dealership called");
            PreferenceManger.putString(AppConstant.TIME_STAMP, Calendar.getInstance().getTime().toString());
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Fetching Dealerships");
            if (b) progressDialog.show();
            progressDialog.setCancelable(false);
            Call<DealershipData> get_dealerships = RetrofitInitialization.getDs_services().get_dealerships("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
            get_dealerships.enqueue(new Callback<DealershipData>() {
                @Override
                public void onResponse(Call<DealershipData> call, Response<DealershipData> response) {
                    if (response.code() == 200 && response.isSuccessful()) {
                        DealershipData dealershipData = response.body();
                        Gson gson = new GsonBuilder().create();
                        String dealershipDataString = gson.toJson(response.body());
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, dealershipDataString);
                        DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
                        DSTextView change = mView.findViewById(R.id.change);
                        cancel.setHorizontallyScrolling(true);
                        cancel.setSelected(true);
                        cancel.setVisibility(View.VISIBLE);
                        boolean isDealershipActive = false;
                        if (dealershipData != null) {
                            for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                                if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                                    isDealershipActive = true;
                                    PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(i).getDealershipName());
                                    PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(i).getId());
                                    PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(i).isAllowCheckID());
                                    PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(i).isCaptureCustomerPhotoOnSign());
                                    PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(i).isUsedCarOnly());
                                    PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(i).isProcessTradeIn());
                                    PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(i).isTakePhotoOfProofOfInsurance());
                                    PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(i).isTakePhotoOfVehicle());
                                    PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(i).isCaptureScreenOnSign());
                                    PreferenceManger.putBoolean(AppConstant.VIEW_MANDATORY, dealershipData.getDealerships().get(i).isViewThirdPartyReportMandatory());
                                    PreferenceManger.putBoolean(AppConstant.USE_THIRD_PARTY_API, dealershipData.getDealerships().get(i).isUseThirdPartyAPI());
                                    PreferenceManger.putBoolean(AppConstant.IS_AI_ENABLED, dealershipData.getDealerships().get(i).isAIFeatureEnabled());
                                }
                            }
                        } else {
                            getDealerships(false);
                        }
                        if (PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME).trim().length() == 0) {
                            if (dealershipData != null) {
                                for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                                    if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                                        cancel.setText(dealershipData.getDealerships().get(i).getDealershipName());
                                    }
                                }
                            } else {
                                getDealerships(false);
                            }
                        } else {
                            cancel.setText(PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
                        }
                        if (dealershipData != null && dealershipData.getDealerships().size() == 1) {
                            change.setVisibility(View.GONE);
                            if (!isDealershipActive) {
                                PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(0).getDealershipName());
                                PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(0).getId());
                                PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(0).isAllowCheckID());
                                PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(0).isCaptureCustomerPhotoOnSign());
                                PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(0).isUsedCarOnly());
                                PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(0).isProcessTradeIn());

                                PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(0).isTakePhotoOfProofOfInsurance());
                                PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(0).isCaptureScreenOnSign());
                                PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(0).isTakePhotoOfVehicle());
                                PreferenceManger.putBoolean(AppConstant.VIEW_MANDATORY, dealershipData.getDealerships().get(0).isViewThirdPartyReportMandatory());
                                PreferenceManger.putBoolean(AppConstant.USE_THIRD_PARTY_API, dealershipData.getDealerships().get(0).isUseThirdPartyAPI());
                                PreferenceManger.putBoolean(AppConstant.IS_AI_ENABLED, dealershipData.getDealerships().get(0).isAIFeatureEnabled());
                            }
                        } else {
                            if (PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) == 0) {
                                change.setText("Select Dealership");
                            } else {
                                if (getActivity() != null) {
                                    change.setText(getString(R.string.change_dealer));
                                }
                            }
                            change.setVisibility(View.VISIBLE);
                            if (!isDealershipActive) {
                                replaceFragment(R.id.container, new Select_Dealership(), Select_Dealership.class.getSimpleName());
                            }
                            mView.findViewById(R.id.changeDealer).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    replaceFragment(R.id.container, new Select_Dealership(), Select_Dealership.class.getSimpleName());
                                }
                            });
                        }
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());
                            if (response.code() == 401 || response.code() == 403) {
                                if (json.getString("Message").contains("INACTIVE")) {
                                    String[] arr = json.getString("Message").split("\\|");
                                    new CustomToast(getActivity()).toast(arr[1]);
                                    Log.e("Logout: ", "logout from path during inactive" + json.toString());
                                    logoutInBase();
                                } else {
                                    if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                        callRefreshTokenAPI();
                                    } else {
                                        Log.e("Logout: ", "logout from path during zero length" + json.toString());
                                        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                        Locale myLocale = new Locale("en");
                                        Resources res = getResources();
                                        DisplayMetrics dm = res.getDisplayMetrics();
                                        Configuration conf = res.getConfiguration();
                                        conf.locale = myLocale;
                                        res.updateConfiguration(conf, dm);
                                        callHomeActivity();
                                    }
                                }

                            }
                            showPopup(json.getString("error_description"));
                            Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<DealershipData> call, Throwable t) {
//                if ( t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    if (getActivity() != null && !getActivity().isFinishing()) {
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }
//                }

                }
            });
        } else {
            Log.e("called ", "Dealership not called");
        }
    }

    private void deleteRecordsOlderThan2Hours(ArrayList<Record> needToDelete) {
        for (int i = 0; i < needToDelete.size(); i++) {
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (needToDelete.get(i).getCustomerDetails() != null && needToDelete.get(i).getCustomerDetails().getDLNumber() != null) {
                Record record = recordData.where(needToDelete.get(i).getCustomerDetails().getDLNumber());

                if (record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null) {
                    if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
                        File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
                        deleteRecursive(main);
                    }
                    recordData.getRecords().remove(record);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                } else {
                    Log.e("No Data found", " To Delete" + needToDelete.get(i).getCustomerDetails());
                }
            }

        }
    }

    private void updateRecordThroughVTD(IncompleteDealList incompleteDealList, int order) {
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = null;
        if (recordData != null)
            record = recordData.whereVTID(incompleteDealList.getCustomerVehicleTradeId() + "");



        if (record != null && record.getVehicleDetails() != null) {
            if (record.getSettingsAndPermissions() != null && !record.getSettingsAndPermissions().isSyncingInProgress()) {
                if (!record.getSettingsAndPermissions().isEditing() && !record.getSettingsAndPermissions().getDealStatus().equalsIgnoreCase("Syncing")) {
                    VehicleDetails vehicleDetails = record.getVehicleDetails() == null ? new VehicleDetails() : record.getVehicleDetails();
                    vehicleDetails.setVINNumber(incompleteDealList.getVIN() == null ? "" : incompleteDealList.getVIN());
                    vehicleDetails.setCarMake(incompleteDealList.getMake() == null ? "" : incompleteDealList.getMake());
                    vehicleDetails.setCarModel(incompleteDealList.getModel() == null ? "" : incompleteDealList.getModel());
                    vehicleDetails.setCarYear(incompleteDealList.getModelYear() == 0 ? "" : incompleteDealList.getModelYear() + "");
                    record.setWebDeal(incompleteDealList.isWebDeal());
                    record.setBuyerSigned(incompleteDealList.isBuyerSigned());
                    record.setCoBuyerSigned(incompleteDealList.isCoBuyerSigned());
                    record.setCoBuyer(incompleteDealList.isCoBuyer());
                    record.setDealStatusText(incompleteDealList.getDealStatusText());
                    record.setDealStatusId(incompleteDealList.getDealStatusId());
                    CustomerDetails customerDetails = record.getCustomerDetails();
                    customerDetails.setFirstName(incompleteDealList.getCustomerFirstName() == null ? "" : incompleteDealList.getCustomerFirstName());
                    customerDetails.setLastName(incompleteDealList.getCustomerLastName() == null ? "" : incompleteDealList.getCustomerLastName());
                    SalesPersons salesPersons = customerDetails.getSalesPersons() == null ? new SalesPersons() : customerDetails.getSalesPersons();
                    salesPersons.setFirstName(incompleteDealList.getSalesPersonFirstName() == null ? "" : incompleteDealList.getSalesPersonFirstName());
                    salesPersons.setEmail(incompleteDealList.getSalesPersonEmail() == null ? "" : incompleteDealList.getSalesPersonEmail());
                    salesPersons.setLastName(incompleteDealList.getSalesPersonLastName() == null ? "" : incompleteDealList.getSalesPersonLastName());
                    salesPersons.setUserId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
                    vehicleDetails.setSalesId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
                    customerDetails.setSalesPersons(salesPersons);

                    DealershipDetails dealershipDetails = new DealershipDetails();
                    dealershipDetails.setDealershipName(incompleteDealList.getDealershipName());
                    dealershipDetails.setDealershipId(incompleteDealList.getDealershipId());
                    record.setDealershipDetails(dealershipDetails);

                    SettingsAndPermissions settingsAndPermissions = record.getSettingsAndPermissions() == null ? new SettingsAndPermissions() : record.getSettingsAndPermissions();
                    settingsAndPermissions.setCustomerVehicleTradeId(incompleteDealList.getCustomerVehicleTradeId() + "");
                    settingsAndPermissions.setScreenName(incompleteDealList.getLastScreenName());
                    settingsAndPermissions.setSequenceOrder(order);
                    settingsAndPermissions.setTabName(incompleteDealList.getLastAccessingTabletName());
                    if (getPageIndex(incompleteDealList.getLastScreenName()) > 82) {
                        settingsAndPermissions.setDataSyncedWithServer(true);
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                    Date date = null;
                    try {
                        date = sdf.parse(incompleteDealList.getTradeDateTime());
//                Log.e("Date from server: ", date.toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    assert date != null;
                    settingsAndPermissions.setLatestTimeStamp(date.toString());
                    settingsAndPermissions.setFrontUrl(incompleteDealList.getFrontImageUrl());
                    settingsAndPermissions.setFromServer(true);
                    if (incompleteDealList.getDealStatus() != null && incompleteDealList.getDealStatus().length() > 0) {
                        settingsAndPermissions.setDealStatus(incompleteDealList.getDealStatus());
                    }
                    if (settingsAndPermissions.isSentEmailForIncomplete()) {
                        settingsAndPermissions.setDealStatus("Syncing");
                    }
//            settingsAndPermissions.setIncompleteDeal(true);

                    record.setSettingsAndPermissions(settingsAndPermissions);
                    record.setCustomerDetails(customerDetails);
                    record.setVehicleDetails(vehicleDetails);

                    int index = recordData.getIndexNumber(customerDetails.getDLNumber());
                    recordData.getRecords().set(index, record);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                } else {
                    Log.e("Deal is Editing: ", incompleteDealList.getCustomerVehicleTradeId() + " This id is in edit mode discarding merge.");
                }
            } else {
                Log.e("Syncing in progress: ", incompleteDealList.getCustomerVehicleTradeId() + " This id is in sync mode discarding merge.");
            }
        } else {
            String rand_int1 = System.currentTimeMillis() + "";/*String.format(Locale.getDefault(), "%09d", rand.nextInt(100000001));*/
            Log.e("Random number: ", rand_int1);
            /*Commented on 31/02/2020 to fix crash while capturing image on window sticker on finance page*/
//            PreferenceManger.putString(AppConstant.DMV_NUMBER, rand_int1);
            Gson gson = new GsonBuilder().create();
            if (PreferenceManger.getStringValue(AppConstant.RECORD_DATA).length() != 0) {
                recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            } else {
                recordData = new RecordData();
            }
            record = new Record();
            CustomerDetails customerDetails1 = new CustomerDetails();
            CoBuyerCustomerDetails coBuyerCustomerDetails1 = new CoBuyerCustomerDetails();
            VehicleDetails vehicleDetails1 = new VehicleDetails();
            TradeInVehicle tradeInVehicle1 = new TradeInVehicle();
            SettingsAndPermissions settingsAndPermissions1 = new SettingsAndPermissions();
            settingsAndPermissions1.setLocalLogs("");
            SyncUp syncUp = new SyncUp();
            DealershipDetails dealershipDetails = new DealershipDetails();
            dealershipDetails.setDealershipName(incompleteDealList.getDealershipName());
            dealershipDetails.setDealershipId(incompleteDealList.getDealershipId());
            record.setWebDeal(incompleteDealList.isWebDeal());
            record.setDealStatusText(incompleteDealList.getDealStatusText());
            record.setDealStatusId(incompleteDealList.getDealStatusId());
            record.setBuyerSigned(incompleteDealList.isBuyerSigned());
            record.setCoBuyerSigned(incompleteDealList.isCoBuyerSigned());
            record.setCoBuyer(incompleteDealList.isCoBuyer());
//            dealershipDetails.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
            record.setCustomerDetails(customerDetails1);
            record.setCoBuyerCustomerDetails(coBuyerCustomerDetails1);
            record.setDealershipDetails(dealershipDetails);
            record.setTradeInVehicle(tradeInVehicle1);
            record.setSettingsAndPermissions(settingsAndPermissions1);
            record.setSyncUp(syncUp);
            record.setVehicleDetails(vehicleDetails1);
            record.getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            record.getCustomerDetails().setDLNumber(rand_int1);
            SalesPersons salesPersons1 = new SalesPersons();

            record.getCustomerDetails().setSalesPersons(salesPersons1);
            VehicleDetails vehicleDetails = record.getVehicleDetails();
            vehicleDetails.setVINNumber(incompleteDealList.getVIN() == null ? "" : incompleteDealList.getVIN());
            vehicleDetails.setCarMake(incompleteDealList.getMake() == null ? "" : incompleteDealList.getMake());
            vehicleDetails.setCarModel(incompleteDealList.getModel() == null ? "" : incompleteDealList.getModel());
            vehicleDetails.setCarYear(incompleteDealList.getModelYear() == 0 ? "" : incompleteDealList.getModelYear() + "");

            CustomerDetails customerDetails = record.getCustomerDetails();
            customerDetails.setFirstName(incompleteDealList.getCustomerFirstName() == null ? "" : incompleteDealList.getCustomerFirstName());
            customerDetails.setLastName(incompleteDealList.getCustomerLastName() == null ? "" : incompleteDealList.getCustomerLastName());
            SalesPersons salesPersons = customerDetails.getSalesPersons();
            salesPersons.setFirstName(incompleteDealList.getSalesPersonFirstName() == null ? "" : incompleteDealList.getSalesPersonFirstName());
            salesPersons.setEmail(incompleteDealList.getSalesPersonEmail() == null ? "" : incompleteDealList.getSalesPersonEmail());
            salesPersons.setLastName(incompleteDealList.getSalesPersonLastName() == null ? "" : incompleteDealList.getSalesPersonLastName());
            salesPersons.setUserId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
            vehicleDetails.setSalesId(incompleteDealList.getSalesPersonUserId() == null ? "" : incompleteDealList.getSalesPersonUserId());
            customerDetails.setSalesPersons(salesPersons);

            SettingsAndPermissions settingsAndPermissions = record.getSettingsAndPermissions();
            settingsAndPermissions.setCustomerVehicleTradeId(incompleteDealList.getCustomerVehicleTradeId() + "");
            settingsAndPermissions.setScreenName(incompleteDealList.getLastScreenName());
            settingsAndPermissions.setTabName(incompleteDealList.getLastAccessingTabletName());
            settingsAndPermissions.setSequenceOrder(order);
            if (getPageIndex(incompleteDealList.getLastScreenName()) > 82) {
                settingsAndPermissions.setDataSyncedWithServer(true);
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            Date date = null;
            try {
                date = sdf.parse(incompleteDealList.getTradeDateTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            assert date != null;
            settingsAndPermissions.setLatestTimeStamp(date.toString());
            settingsAndPermissions.setFrontUrl(incompleteDealList.getFrontImageUrl());
//            settingsAndPermissions.setIncompleteDeal(true);
            settingsAndPermissions.setDealStatus(incompleteDealList.getDealStatus());
            record.setSettingsAndPermissions(settingsAndPermissions);
            record.setCustomerDetails(customerDetails);
            record.setVehicleDetails(vehicleDetails);
            settingsAndPermissions.setFromServer(true);
            recordData.getRecords().add(record);
            //Add the recordData object to the SharedPreference after converting it to string
            String recordDataString1 = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
        }
    }

    private void showBottomSheet(String textMessage) {
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(R.layout.popup_view);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        assert text != null;
        text.setText(textMessage);
        dialog.show();
        dialog.setCancelable(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 3000);
    }

//    private void setCustomData() {
//        String tabName = "";
//        try {
//            tabName = PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "";
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        PushLink.addMetadata("Brand", Build.BRAND);
//        PushLink.addMetadata("Model", Build.MODEL);
//        PushLink.addMetadata("OS Version", Build.VERSION.RELEASE);
//        PushLink.addMetadata("Logged in user", PreferenceManger.getStringValue(AppConstant.USER_NAME));
//        PushLink.addMetadata("Email", PreferenceManger.getStringValue(AppConstant.USER_MAIL));
//        PushLink.addMetadata("DealershipName", PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
//        PushLink.addMetadata("Tab Name", tabName);
//    }

    public void generateNoteOnSD(Context context, String sBody) {
        try {
            File root = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM), "/DSXT/DeviceID");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "DeviceID.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static FragmentScanDMVFront.ConfirmationDialogFragment newInstance(@StringRes int message,
                                                                                  String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            FragmentScanDMVFront.ConfirmationDialogFragment fragment = new FragmentScanDMVFront.ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

    }

    @SuppressLint("HardwareIds")
    private void initPushLink() {
        deviceId = Settings.Secure.getString(requireActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("Device Id: ", deviceId);
    }

    private void getThirdParty() {
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    assert response.body() != null;
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    ThirdPartyList vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API) && list != null) {
                        vehicleConditionDisclosure = new ThirdPartyList();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setViewReport(false);
                            list.get(i).setMandatory(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    ThirdPartyList vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
//                        String recordDataString = gson.toJson(recordData);
//                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                    }
                    Log.i("ThirdParty: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.THIRD_PARTY_REPORT, dealershipDataString);
//                    Log.e("Size of reports: ", list.size() + "");
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {

            }
        });
    }

    private void getHistoryDisclosure() {
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getVehicleHistoryDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, "dealerxt", "0", "1").enqueue(new Callback<VehicleHistoryDisclosure>() {
            @Override
            public void onResponse(Call<VehicleHistoryDisclosure> call, Response<VehicleHistoryDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<Value> list;
                    list = response.body().getValue();
                    VehicleHistoryDisclosure vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API) && list != null) {
                        vehicleConditionDisclosure = new VehicleHistoryDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    VehicleHistoryDisclosure vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    Log.i("HistoryDisclosure: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                    Log.e("Size of history: ", AppConstant.historyLists.size() + "");
                }
            }

            @Override
            public void onFailure(Call<VehicleHistoryDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
            }
        });
    }

    private void getConditionDisclosure() {

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, "dealerxt", "0", "1").enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API) && list != null) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    Timber.tag("ConditionDisclosure: ").i(gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE, dealershipDataString);
//                    Log.e("Size of common: ", AppConstant.commonList.size() + "");
                }
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
            }
        });
    }

    private void getConditionDisclosureNew(String id) {

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, "dealerxt", "0", "2").enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
//                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    Log.i("ConditionDisclosure: ", gson.toJson(vehicleConditionDisclosure));
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE_NEW, dealershipDataString);
//                    Log.e("Size of common: ", AppConstant.commonList.size() + "");
                }
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                Log.e("error: ", t.getLocalizedMessage());
            }
        });
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            //Android is 11(R) or above
            try {
                Log.d(TAG, "requestPermission: try");

                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                storageActivityResultLauncher.launch(intent);
            } catch (Exception e) {
                Log.e(TAG, "requestPermission: catch", e);
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                storageActivityResultLauncher.launch(intent);
            }
        } else {
            //Android is below 11(R)
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_CODE
            );
        }
    }

    public ActivityResultLauncher<Intent> storageActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    Log.d(TAG, "onActivityResult: ");
                    //here we will handle the result of our intent
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        //Android is 11(R) or above
                        if (Environment.isExternalStorageManager()) {
                            //Manage External Storage Permission is granted
                            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
//                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is granted");
//                            createFolder();
                        } else {
                            requestPermission();
                            //Manage External Storage Permission is denied
//                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is denied");
//                            Toast.makeText(MainActivity.this, "Manage External Storage Permission is denied", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //Android is below 11(R)
                    }
                }
            }
    );

    public boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            //Android is 11(R) or above
            return Environment.isExternalStorageManager();
        } else {
            //Android is below 11(R)
            int write = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
        }
    }

    /*Handle permission request results*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0) {
                //check each permission if granted or not
                boolean write = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean read = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (write && read) {
                    //External Storage permissions granted
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permissions granted");
                    deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                    generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
                } else {
                    //External Storage permission denied
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permission denied");
//                    Toast.makeText(this, "External Storage permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
