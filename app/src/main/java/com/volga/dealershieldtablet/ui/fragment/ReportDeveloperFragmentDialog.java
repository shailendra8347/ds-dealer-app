package com.volga.dealershieldtablet.ui.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportDeveloperFragmentDialog extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG_FLOATING);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.report_error, container, false);
        DSTextView submit = v.findViewById(R.id.submit);
        final DSEdittext editText = v.findViewById(R.id.addCondition);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(v, getActivity());
                /*Upload all data to server*/
                String feedbackText = editText.getText().toString();
                if (getArguments() != null) {
                    String subjectData;
                    if (feedbackText.length() > 0) {
                        subjectData = getArguments().getString("data") + " \nFeedback data" + feedbackText;
                    } else {
                        subjectData = getArguments().getString("data");
                    }
                    writeCustomLogsAPI("0", subjectData);
                }
            }
        });
        DSTextView close = v.findViewById(R.id.text_back_up);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(v, getActivity());
                getDialog().dismiss();
            }
        });
        return v;
    }

    private void writeCustomLogsAPI(String id, String logs) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Submitting feedback..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setEmailSubject(null);
        customLogs.setIsSendAlertEmail(true);
        ArrayList<String> emails = new ArrayList<>();
        emails.add("shailendra.m@volgainfotech.com");
        emails.add(getArguments().getString("data"));
        customLogs.setEmails(emails);
        customLogs.setLogText(logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                progressDialog.dismiss();
                getDialog().dismiss();
                new CustomToast(getActivity()).alert("Submitted");
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }
}