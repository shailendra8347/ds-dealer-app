package com.volga.dealershieldtablet.ui.fragment;

import static android.os.Looper.getMainLooper;
import static com.volga.dealershieldtablet.interfaceCallback.NetworkChangeReceiver.IS_NETWORK_AVAILABLE;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosureAdapter;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Translations;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeConditionDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.ThirdPartyReportBytes;
import com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.NetworkChangeReceiver;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.activity.ReviewEditDisclosureViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.activity.CustomWebViewActivity;
import com.volga.dealershieldtablet.ui.adapter.ConditionAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class FragmentConditionDisclosure extends BaseFragment implements ConditionAdapter.IOnLanguageSelect, AdditionalDisclosureAdapter.OnAdditionalSelect {
    IntentFilter intentFilter;
    BroadcastReceiver br;
    boolean isTaken;
    private IGoToNextPage iGoToNextPage;
    private View mView;
    private boolean first = false;
    private boolean isSigned = false;
    private boolean shouldNextShow = false;
    private DSTextView next;
    //Changed false to true for bullet point
    private boolean allTrue = true;
    private boolean isCobuyer = false;
    private SignaturePad signaturePad;
    private ConditionAdapter adapter;
    private ProgressDialog progressDialog1, progressDialog2;
    BroadcastReceiver broadcastReceiverAPI = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra(NetworkChangeReceiver.STARTED_API_CALLS, false) && (NewDealViewPager.currentPage == 25 || ReviewEditDisclosureViewPager.currentPage == 0)) {
                    progressDialog1 = new ProgressDialog(getActivity());
                    progressDialog1.setMessage("Verifying Disclosures");
                    progressDialog1.setCancelable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog1.setCancelable(true);
                        }
                    },14000);
                    if (progressDialog1 != null)
                        progressDialog1.show();
                }
            }
        }
    };
    private DSTextView time;
    private LinearLayout additionalLL;
    //Changed false to true for bullet point
    private boolean allAdditional = true;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && allTrue && isSigned && allAdditional) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }
        }
    };
    private boolean safetyShown, isALI3Played = false;
    private ImageView imageView;
    private boolean isProgressShown = false;
    private DSTextView sign;
    private ImageView delete;
    private boolean cfByte,acByte;


    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
        getActivity().registerReceiver(broadcastReceiverAPI, new IntentFilter("StartedAPI"));
        Timber.e("Registered");
        intentFilter = new IntentFilter(NetworkChangeReceiver.NETWORK_AVAILABLE_ACTION);
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                String networkStatus = isNetworkAvailable ? "connected" : "disconnected";
                Timber.e(networkStatus);
                saveRecordData();

                if (!safetyShown && NewDealViewPager.currentPage == 25) {
                    initView();
                }
                if (!safetyShown && getActivity() instanceof ReviewEditDisclosureViewPager && ReviewEditDisclosureViewPager.currentPage == 0) {
                    initView();
                }
                if (progressDialog1 != null && progressDialog1.isShowing()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog1.dismiss();
                        }
                    }, 3500);
                }
                if (progressDialog2 != null) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog2.dismiss();
                        }
                    }, 3500);
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(br, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.e("UnRegistered");
        requireActivity().unregisterReceiver(broadcastReceiver);
        getActivity().unregisterReceiver(broadcastReceiverAPI);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(br);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        final APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Timber.e("Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (allTrue && isSigned && allAdditional) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (allTrue && isSigned && allAdditional) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                } else if (picturesTaken.size() == 0 && retryCount[0] > 2) {
                                    if (allTrue && isSigned && allAdditional) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null && !first) {
            signaturePad.clear();
            if (!safetyShown && NewDealViewPager.currentPage == 25) {
                initView();
            }
//            enableNext(next);
            time.setText(getCurrentTimeString());
            isSigned = false;
//            next.setVisibility(View.GONE);
            disableWithGray(next);
            initPhoto();

        }
        if (isVisibleToUser && next != null) {
//            enableNext(next);
            Gson gson = new GsonBuilder().create();
            final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails()!=null&&!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isConditionVerified()&&first){
                Log.e(" Called: ","getConditionDisclosure");
                getConditionDisclosure(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber());
            }
        }
        if (isVisibleToUser) {
            showFragmentInPortrait();
        } else {
            isTaken = false;
        }
        if (isVisibleToUser && first && time != null) {

            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (!isProgressShown && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isDataDownloading()) {
                progressDialog2 = new ProgressDialog(getActivity());
                progressDialog2.setMessage("Verifying Disclosures");
                progressDialog2.setCancelable(false);
                isProgressShown = true;
                progressDialog2.show();
            }
            showDialogForRecall();
            showAlertDirectly();
            ali3Logic(imageView);
        }
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                File file;
                if (!isCobuyer) {
                    file = getOutputFile("UserPic", "condition_disclosure_screen_pic");
                } else {
                    file = getOutputFile("UserPic", "co_condition_disclosure_screen_pic");
                }

                if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                    takeUserPicture(file);
                } else {
                    isTaken = true;
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }


    private void showAlertDirectly() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
            ArrayList<InitThirdParty> initList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
            for (int i = 0; i < initList.size(); i++) {
                if (initList.get(i).getLanguageId().equalsIgnoreCase("1") && initList.get(i).getThirdPartyID().equalsIgnoreCase("1") && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                    showAlert(initList.get(i).getErrorsString(), false, "1", "CARFAX Not Available. Please make manual disclosures.");
                }
                if (initList.get(i).getLanguageId().equalsIgnoreCase("1") && initList.get(i).getThirdPartyID().equalsIgnoreCase("2") && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                    showAlert(initList.get(i).getErrorsString(), false, "2", "AutoCheck Not Available. Please make manual disclosures.");
                }
            }
        }
    }

    private void showSafetyRecall(String title, String msg) {

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
        final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);
        final DSTextView titleTV = promptView.findViewById(R.id.title);
        DSTextView messageTV = promptView.findViewById(R.id.message);
        titleTV.setText("WARNING: " + title);
        messageTV.setText(msg);
        final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);

        DSTextView ok = promptView.findViewById(R.id.ok);
        alertD.setView(promptView);
        alertD.show();
        stopDeal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stopDeal.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopDeal.setEnabled(true);
                    }
                }, 4000);
                if (getActivity() != null && !getActivity().isFinishing() && alertD.isShowing()) {
                    alertD.dismiss();
                }
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (getActivity() instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                        pojo.setId(AppConstant.commonList.get(i).getId());
                        pojo.setName(AppConstant.commonList.get(i).getName());
                        historyListPojos1.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                cancel();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                File file = getOutputFile("Screenshot", "alert_safety_recall");
                takeScreenshotForAlert(file);
//                commented as per saftry recall logic
                /*for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if ((AppConstant.commonList.get(i).getId().equalsIgnoreCase("13") || AppConstant.commonList.get(i).getId().equalsIgnoreCase("8")) && AppConstant.commonList.get(i).getName().equalsIgnoreCase("Open Recall") && AppConstant.commonList.get(i).isVerified()) {
                        AppConstant.commonList.get(i).setVerified(false);
                        AppConstant.commonList.get(i).setChecked(false);
                    }
                }
                boolean verified = false;
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isVerified() && i != AppConstant.commonList.size() - 1) {
                        verified = true;
                        break;
                    }
                }
                if (adapter != null) {
                    if (!verified)
                        AppConstant.commonList.get(AppConstant.commonList.size() - 1).setVerified(false);
                    adapter.notifyDataSetChanged();
                }*/
                if (getActivity() != null && !getActivity().isFinishing() && alertD.isShowing()) {
                    alertD.dismiss();
                }

            }
        });

    }

    private void showDialogForRecall() {
        Gson gson = new GsonBuilder().create();
        ArrayList<WarningAlerts> msg = null;
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getWarningAlerts() == null) {
                VehicleConditionDisclosure vehicleConditionDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleConditionDisclosure();
                if (vehicleConditionDisclosure != null && vehicleConditionDisclosure.getWarningAlerts() != null) {
                    msg = vehicleConditionDisclosure.getWarningAlerts();
                } else {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
                        vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE_NEW), VehicleConditionDisclosure.class);
                    else {
                        vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE), VehicleConditionDisclosure.class);
                    }
                    if (vehicleConditionDisclosure != null && vehicleConditionDisclosure.getMessage() != null) {
                        msg = vehicleConditionDisclosure.getWarningAlerts();
                    }
                }
            } else {
                msg = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getWarningAlerts();
            }

        }

        if (msg != null && msg.size() > 0) {
            safetyShown = true;
            StringBuilder title = new StringBuilder();
            StringBuilder desc = new StringBuilder();
            if (msg.size() > 1) {
                for (int i = 0; i < msg.size(); i++) {
                    int localCount = i;
                    title.append(msg.get(i).getTitle()).append(",");
                    desc.append(localCount + 1).append(": ").append(msg.get(i).getMessage()).append("\n");
                }

            } else {
                title = new StringBuilder(msg.get(0).getTitle());
                desc = new StringBuilder(msg.get(0).getMessage());
            }
//            for (int i = 0; i < msg.size(); i++) {
            showSafetyRecall(title.toString(), desc.toString());
//            }

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.used_vehicle_history_after_sticker, container, false);
        if (AppConstant.shouldCall) {
            AppConstant.shouldCall = false;
            showFragmentInPortrait();
        }
        initView();
        return mView;
    }

    public void disableWithGray(DSTextView next) {

        next.setBackground(AppCompatDrawableManager.get().getDrawable(requireActivity(),R.drawable.rounded_corner_next_gray));
        next.setClickable(false);
        if (!first) {
            next.setPadding(0, 15, 0, 15);
        } else {
            next.setPadding(60, 30, 60, 30);
        }
        next.setEnabled(false);

    }

    public void enableWithGreen(DSTextView next) {
        next.setBackground(AppCompatDrawableManager.get().getDrawable(requireActivity(),R.drawable.rounded_corner_green));
        if (!first) {
            next.setPadding(0, 15, 0, 15);
        } else {
            next.setPadding(60, 30, 60, 30);
        }
        next.setClickable(true);
        next.setEnabled(true);

    }

    private void initView() {
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        DSTextView history = mView.findViewById(R.id.history1);
        first = getArguments().getBoolean("first", false);
        if (!first) {
            if (NewDealViewPager.currentPage == 74) {
                initPhoto();
            }
        }
        Gson gson = new GsonBuilder().create();
        imageView = mView.findViewById(R.id.gif);
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            isCobuyer = true;
        }
        if (getActivity() instanceof NewDealViewPager) {
            if (!safetyShown && NewDealViewPager.currentPage == 25) {
                showDialogForRecall();
                showAlertDirectly();
            }
            if (!isALI3Played && NewDealViewPager.currentPage == 25) {
                isALI3Played = true;
                ali3Logic(imageView);
            }
        }
        if (getActivity() instanceof ReviewEditDisclosureViewPager) {
            if (!safetyShown && ReviewEditDisclosureViewPager.currentPage == 0) {

                assert recordData != null;
                if (!isProgressShown && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isDataDownloading()) {
                    progressDialog2 = new ProgressDialog(getActivity());
                    progressDialog2.setMessage("Verifying Disclosures");
                    progressDialog2.setCancelable(true);
                    isProgressShown = true;
                    progressDialog2.show();
                }
                showDialogForRecall();
                showAlertDirectly();
            }
            if (!isALI3Played && ReviewEditDisclosureViewPager.currentPage == 0) {
                isALI3Played = true;
                ali3Logic(imageView);
            }
        }
        time = mView.findViewById(R.id.time);
        time.setText(getCurrentTimeString());
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        DSTextView history1 = mView.findViewById(R.id.history);
        DSTextView customerAcknowledge = mView.findViewById(R.id.customerAcknowledge);
        customerAcknowledge.setVisibility(View.GONE);
        mView.findViewById(R.id.bulletPoint).setVisibility(View.GONE);
        LinearLayout frameLayout = mView.findViewById(R.id.boxSign);
        LinearLayout noticeBox = mView.findViewById(R.id.noticeBox);
        DSTextView notice = mView.findViewById(R.id.notice);
        NestedScrollView nestedScrollView1 = mView.findViewById(R.id.nestedScroll);
        nestedScrollView1.setNestedScrollingEnabled(true);
//        final LockableScrollView nestedScrollView = mView.findViewById(R.id.temp);
//        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                View view = nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);
//
//                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
//                        .getScrollY()));
//
//                //                    getPlaylistFromServer("more");
//                nestedScrollView.setScrollingEnabled(diff != 0);
//            }
//        });
        mView.findViewById(R.id.recyclerView).setFocusable(false);
//        mView.findViewById(R.id.temp).requestFocus();
//        nestedScrollView.fullScroll(View.FOCUS_UP);
//        nestedScrollView.scrollTo(0,0);
        sign = mView.findViewById(R.id.signhere);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.header1).findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.header1).findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));

        }
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager managers = new LinearLayoutManager(getActivity());
        DSTextView ttv1 = mView.findViewById(R.id.ttv1);
        DSTextView ttv2 = mView.findViewById(R.id.ttv2);
        additionalLL = mView.findViewById(R.id.additionalLL);
        additionalLL.setVisibility(View.GONE);
        RecyclerView additionalRecyclerView = mView.findViewById(R.id.additionalRecyclerView);
        additionalRecyclerView.setFocusable(false);
//        mView.findViewById(R.id.temp).requestFocus();
        if (first) {
            next = mView.findViewById(R.id.text_next_sales);
        } else {
            next = mView.findViewById(R.id.text_next);
        }

//        enableNext(next);
        final ArrayList<HistoryListPojo> historyListPojos = new ArrayList<>();
        ArrayList<HistoryListPojo> listOnUserPage = new ArrayList<>();


        final LinearLayout disclosureMain = mView.findViewById(R.id.maineContent);
        LinearLayout thirdPartyContainer = mView.findViewById(R.id.thirdPartyContainer);
        LinearLayout ali3Logo = mView.findViewById(R.id.ali3Logo);
        final ImageButton carfax = mView.findViewById(R.id.carfax);
        final ImageButton autocheck = mView.findViewById(R.id.autocheck);
        if (first) {
            if (PreferenceManger.getBooleanValue(AppConstant.IS_AI_ENABLED)) {
                ali3Logo.setVisibility(View.VISIBLE);
            } else {
                ali3Logo.setVisibility(View.GONE);
            }
            next.setVisibility(View.GONE);
            ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
            ArrayList<HistoryListPojo> localLi = new ArrayList<>();
            if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
                assert recordData != null;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//                    if (record.getVehicleDetails().getUsedVehicleReport() != null) {
//                        localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleReport());
//                    } else {
                    for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                        HistoryListPojo historyListPojo = new HistoryListPojo();
                        historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                        historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                        historyListPojo.setChecked(false);
//                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isViewReport());
                        historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                        historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                        historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                        localLi.add(historyListPojo);
                    }
//                    }
                    boolean showCarFax = false, showAutoCheck = false;
                    for (int i = 0; i < localLi.size(); i++) {
                        if (localLi.get(i).getId().equalsIgnoreCase("1") && localLi.get(i).isVerified()) {
                            showCarFax = true;
                        } else if (localLi.get(i).getId().equalsIgnoreCase("2") && localLi.get(i).isVerified()) {
                            showAutoCheck = true;
                        }
                    }
                    if (showAutoCheck && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        autocheck.setVisibility(View.VISIBLE);
                    } else {
                        autocheck.setVisibility(View.GONE);
                    }
                    ttv2.setVisibility(View.GONE);
                    if (showCarFax && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        carfax.setVisibility(View.VISIBLE);
                    } else {
                        carfax.setVisibility(View.GONE);
                    }
                    ttv1.setVisibility(View.GONE);
                    carfax.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            carfax.setEnabled(false);
                            carfax.setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    carfax.setEnabled(true);
                                    carfax.setClickable(true);
                                }
                            }, 3000);
                            checkIfReportAvailable(true, "1");
                        }
                    });
                    autocheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            autocheck.setEnabled(false);
                            autocheck.setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    autocheck.setEnabled(true);
                                    autocheck.setClickable(true);
                                }
                            }, 3000);
                            checkIfReportAvailable(false, "2");
                        }
                    });
                }
            }
            thirdPartyContainer.setVisibility(View.VISIBLE);
            noticeBox.setVisibility(View.GONE);
            noticeBox.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);
            history.setText(Html.fromHtml(getString(R.string.vehicle_condition)));
            sign.setVisibility(View.GONE);
//            disclosureMain.setBackgroundResource(R.drawable.screen_bg);
        } else {
//            disclosureMain.setBackgroundResource(R.drawable.screen_bg_whitegrey);
            boolean showCF = false, showAC = false;
            ArrayList<ThirdPartyObj> tpList = new ArrayList<>();
            assert recordData != null;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null) {
                tpList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
            }
            for (int i = 0; i < tpList.size(); i++) {
                if (tpList.get(i).getId().equalsIgnoreCase("1") && tpList.get(i).isViewReport() && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable()) {
                    showCF = true;
                }
                if (tpList.get(i).getId().equalsIgnoreCase("2") && tpList.get(i).isViewReport() && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable()) {
                    showAC = true;
                }
//                if (tpList.get(i).getId().equalsIgnoreCase("1")&&tpList.get(i).getThirdPartyReportBytes()!=null&&tpList.get(i).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")){
//                    cfByte=true;
//                } if (tpList.get(i).getId().equalsIgnoreCase("2")&&tpList.get(i).getThirdPartyReportBytes()!=null&&tpList.get(i).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")){
//                    acByte=true;
//                }
            }
            ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
            ArrayList<HistoryListPojo> localLi = new ArrayList<>();
            if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getUsedVehicleReport() != null) {
                        localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleReport());
                    } else {
                        for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                            HistoryListPojo historyListPojo = new HistoryListPojo();
                            historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                            historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                            historyListPojo.setChecked(false);
//                                historyListPojo.setVerified(thirdPartyList.getValue().get(i).isViewReport());
                            historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                            historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                            historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                            localLi.add(historyListPojo);
                        }
                    }
                    boolean showCarFax = false, showAutoCheck = false;
                    for (int i = 0; i < localLi.size(); i++) {
                        if (localLi.get(i).getId().equalsIgnoreCase("1") && showCF) {
                            showCarFax = true;
                        } else if (localLi.get(i).getId().equalsIgnoreCase("2") && showAC) {
                            showAutoCheck = true;
                        }
                    }
                    if (showAutoCheck && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        autocheck.setVisibility(View.VISIBLE);
                        ttv2.setVisibility(View.VISIBLE);
                    } else {
                        autocheck.setVisibility(View.GONE);
                        ttv2.setVisibility(View.GONE);
                    }
                    if (showCarFax && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                        carfax.setVisibility(View.VISIBLE);
                        ttv1.setVisibility(View.VISIBLE);
                    } else {
                        carfax.setVisibility(View.GONE);
                        ttv1.setVisibility(View.GONE);
                    }

                    carfax.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            carfax.setEnabled(false);
                            carfax.setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    carfax.setEnabled(true);
                                    carfax.setClickable(true);
                                }
                            }, 3000);
                            checkIfReportAvailable(true, "1");
                        }
                    });
                    autocheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            autocheck.setEnabled(false);
                            autocheck.setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    autocheck.setEnabled(true);
                                    autocheck.setClickable(true);
                                }
                            }, 3000);
                            checkIfReportAvailable(true, "2");
                        }
                    });
                }
            }
            thirdPartyContainer.setVisibility(View.VISIBLE);
//            mView.findViewById(R.id.bulletPoint).setVisibility(View.VISIBLE);
            noticeBox.setVisibility(View.GONE);
            notice.setText(Html.fromHtml(getString(R.string.disclosureNotice)));
            customerAcknowledge.setVisibility(View.VISIBLE);
            history.setText(Html.fromHtml(getString(R.string.condition_disclosure)));
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                customerAcknowledge.setText(Html.fromHtml(getString(R.string.condition_disclosure_customer_new)));
            } else {
                customerAcknowledge.setText(Html.fromHtml(getString(R.string.condition_disclosure_customer)));
            }
            frameLayout.setVisibility(View.VISIBLE);
            sign.setVisibility(View.VISIBLE);
        }
        String newOrUsed;
        boolean isNew = true;

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
            history1.setText(R.string.vehicle_history);
            allAdditional = true;
        } else {
            isNew = false;
            newOrUsed = "USED";
            history1.setText(R.string.used_vehicle_history);
        }
        history.setVisibility(View.VISIBLE);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition() == null) {
            assert AppConstant.commonList != null;
            VehicleConditionDisclosure vehicleConditionDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleConditionDisclosure();
            if (vehicleConditionDisclosure != null && vehicleConditionDisclosure.getValue() != null && vehicleConditionDisclosure.getValue().size() > 0) {
                AppConstant.commonList.clear();

                for (int i = 0; i < vehicleConditionDisclosure.getValue().size(); i++) {
                    HistoryListPojo historyListPojo = new HistoryListPojo();
                    historyListPojo.setId(vehicleConditionDisclosure.getValue().get(i).getId());
                    historyListPojo.setChecked(false);
                    historyListPojo.setAdditionalData("");
                    historyListPojo.setWarningAlerts(vehicleConditionDisclosure.getValue().get(i).getWarningAlerts());
                    historyListPojo.setName(vehicleConditionDisclosure.getValue().get(i).getTitle());
                    historyListPojo.setVerified(vehicleConditionDisclosure.getValue().get(i).getIsVerified());
                    historyListPojo.setManualAdditionalDisclosures(vehicleConditionDisclosure.getValue().get(i).getManualAdditionalDisclosure());
                    historyListPojo.setDependentIds(vehicleConditionDisclosure.getValue().get(i).getDependentDislosureIds());
                    AppConstant.commonList.add(historyListPojo);
                }
            } else {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
                    vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE_NEW), VehicleConditionDisclosure.class);
                else {
                    vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE), VehicleConditionDisclosure.class);
                }
                if (vehicleConditionDisclosure != null && vehicleConditionDisclosure.getValue() != null && vehicleConditionDisclosure.getValue().size() > 0) {
                    ArrayList<HistoryListPojo> localLi = new ArrayList<>();
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                            localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
                            AppConstant.commonList.clear();
                            for (int i = 0; i < vehicleConditionDisclosure.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(vehicleConditionDisclosure.getValue().get(i).getId());
                                historyListPojo.setChecked(false);
                                historyListPojo.setWarningAlerts(vehicleConditionDisclosure.getValue().get(i).getWarningAlerts());
                                historyListPojo.setName(vehicleConditionDisclosure.getValue().get(i).getTitle());
                                historyListPojo.setVerified(vehicleConditionDisclosure.getValue().get(i).getIsVerified());
                                historyListPojo.setManualAdditionalDisclosures(vehicleConditionDisclosure.getValue().get(i).getManualAdditionalDisclosure());
                                historyListPojo.setDependentIds(vehicleConditionDisclosure.getValue().get(i).getDependentDislosureIds());
                                AppConstant.commonList.add(historyListPojo);
                            }
                            for (int i = 0; i < localLi.size(); i++) {
                                for (int j = 0; j < AppConstant.commonList.size(); j++) {
                                    if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.commonList.get(j).getId()) && localLi.get(i).isChecked()) {
                                        AppConstant.commonList.get(j).setChecked(true);
                                        AppConstant.commonList.get(j).setAdditionalData(localLi.get(i).getAdditionalData());
                                    }
                                }
                            }
                            Log.e("Size of common: ", AppConstant.commonList.size() + "");
                            if (adapter != null)
                                adapter.notifyDataSetChanged();
                        } else {
                            AppConstant.commonList.clear();
                            for (int i = 0; i < vehicleConditionDisclosure.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(vehicleConditionDisclosure.getValue().get(i).getId());
                                historyListPojo.setChecked(false);
                                historyListPojo.setWarningAlerts(vehicleConditionDisclosure.getValue().get(i).getWarningAlerts());
                                historyListPojo.setName(vehicleConditionDisclosure.getValue().get(i).getTitle());
                                historyListPojo.setVerified(vehicleConditionDisclosure.getValue().get(i).getIsVerified());
                                historyListPojo.setManualAdditionalDisclosures(vehicleConditionDisclosure.getValue().get(i).getManualAdditionalDisclosure());
                                historyListPojo.setDependentIds(vehicleConditionDisclosure.getValue().get(i).getDependentDislosureIds());
                                AppConstant.commonList.add(historyListPojo);
                            }
                            if (adapter != null)
                                adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

        } else {
            AppConstant.commonList.clear();
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                AppConstant.commonList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition());
        }
        ArrayList<HistoryListPojo> listPojos = new ArrayList<>();
        assert recordData != null;
        if (!first) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getConditionSelection() != null) {
                if (!isCobuyer) {
                    listPojos = new ArrayList<>(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getConditionSelection());
                } else {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCoConditionSelection() == null)
                        listPojos = new ArrayList<>(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getConditionSelection());
                    else {
                        listPojos = new ArrayList<>(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCoConditionSelection());
                    }
                }
            }
            AppConstant.condition.clear();
            String langId;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "";
            } else {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId() + "";
            }
            VehicleConditionDisclosure disclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleConditionDisclosure();

            if (disclosure == null) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
                    disclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE_NEW), VehicleConditionDisclosure.class);
                else {
                    disclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE), VehicleConditionDisclosure.class);
                }
            }
            ArrayList<Value> vList = disclosure.getValue();

            for (int i = 0; i < vList.size(); i++) {
                for (int j = 0; j < listPojos.size(); j++) {
                    if (vList.get(i).getId().equalsIgnoreCase(listPojos.get(j).getId())) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(listPojos.get(j).isChecked());
                        pojo.setAdditionalData(listPojos.get(j).getAdditionalData());
                        pojo.setId(listPojos.get(j).getId());
                        pojo.setVerified(listPojos.get(j).isVerified());
                        if (!langId.equalsIgnoreCase("1")) {
                            ArrayList<Translations> translations = vList.get(i).getTranslations();
                            if (translations != null && translations.size() > 0) {
                                for (int k = 0; k < translations.size(); k++) {
                                    if (langId.equalsIgnoreCase(translations.get(k).getDestinationLanguageId())) {
                                        pojo.setName(translations.get(k).getText());
                                    } else if (langId.equalsIgnoreCase("0")) {
                                        pojo.setName(vList.get(j).getTitle());
                                    }
                                }
                            } else {
                                pojo.setName(listPojos.get(j).getName());
                            }
                        } else
                            pojo.setName(listPojos.get(j).getName());
//
                        AppConstant.condition.add(pojo);
                    }
                }

            }

            boolean isAllSelected = false;
            for (int i = 0; i < AppConstant.condition.size(); i++) {

                if (!AppConstant.condition.get(i).isChecked()) {
                    isAllSelected = AppConstant.condition.get(i).isChecked();
                    break;
                }
                isAllSelected = AppConstant.condition.get(i).isChecked();
            }
            // changed for bullet point
            allTrue=true;
//            allTrue = isAllSelected;
            if (allTrue && isSigned && allAdditional) {
                enableWithGreen(next);
                next.setVisibility(View.VISIBLE);
            } else {
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
//            boolean shouldDelete = removeIndex6(AppConstant.condition);
//            if (shouldDelete) {
//                AppConstant.condition.remove(6);
//            }
            adapter = new ConditionAdapter(getActivity(), this, AppConstant.condition, first);
//            ViewCompat.setNestedScrollingEnabled(recyclerView, false);
            AdditionalDisclosure additionalDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure();
            if (additionalDisclosure != null && additionalDisclosure.getValue() != null && additionalDisclosure.getValue().size() > 0) {
                AppConstant.additional.clear();
                ArrayList<Value> addList = additionalDisclosure.getValue();
                for (int j = 0; j < addList.size(); j++) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setChecked(false);
                    pojo.setVerified(addList.get(j).getIsVerified());

                    pojo.setAdditionalData("");
                    pojo.setId(addList.get(j).getId());
                    if (!langId.equalsIgnoreCase("1")) {
                        ArrayList<Translations> translations = addList.get(j).getTranslations();
                        if (translations != null && translations.size() > 0) {
                            for (int k = 0; k < translations.size(); k++) {
                                if (langId.equalsIgnoreCase(translations.get(k).getDestinationLanguageId())) {
                                    pojo.setName(translations.get(k).getText());
                                } else if (langId.equalsIgnoreCase("0")) {
                                    pojo.setName(addList.get(j).getTitle());
                                }
                            }
                        } else {
                            pojo.setName(addList.get(j).getTitle());
                        }

                    } else
                        pojo.setName(addList.get(j).getTitle());
                    AppConstant.additional.add(pojo);
                }
                additionalLL.setVisibility(View.VISIBLE);
                additionalRecyclerView.setLayoutManager(managers);
//                for bullet point
                allAdditional = true;
//                allAdditional = false;
                additionalRecyclerView.setAdapter(new AdditionalDisclosureAdapter(getActivity(), this, AppConstant.additional, false));
                ViewCompat.setNestedScrollingEnabled(additionalRecyclerView, false);
            } else {
                additionalLL.setVisibility(View.GONE);
                allAdditional = true;
            }
        } else {
            additionalLL.setVisibility(View.GONE);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures().size() > 0) {
                ArrayList<VehicleTradeConditionDisclosures> localList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < AppConstant.commonList.size(); j++) {
                        if (localList.get(i).getVehicleConditionDisclosureId().equalsIgnoreCase(AppConstant.commonList.get(j).getId())) {
                            AppConstant.commonList.get(j).setAdditionalData(localList.get(i).getValue());
                            AppConstant.commonList.get(j).setChecked(true);
//                            AppConstant.commonList.get(j).setName(localList.get(i).get);
                        }
                    }
                }
            }

            if (AppConstant.commonList != null && AppConstant.commonList.size() > 0) {
                if (AppConstant.commonList.get(AppConstant.commonList.size() - 1).isChecked()) {
                    for (int i = 0; i < AppConstant.commonList.size() - 1; i++) {
                        AppConstant.commonList.get(i).setChecked(false);
                        AppConstant.commonList.get(i).setAdditionalData("");
                    }
                }
            }
            adapter = new ConditionAdapter(getActivity(), this, AppConstant.commonList, first);
//            ViewCompat.setNestedScrollingEnabled(recyclerView, false);
            VehicleConditionDisclosure conditionDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleConditionDisclosure();
            if (conditionDisclosure != null && conditionDisclosure.getValue() != null && conditionDisclosure.getValue().size() > 0) {
                AppConstant.commonList.clear();

                for (int i = 0; i < conditionDisclosure.getValue().size(); i++) {
                    HistoryListPojo historyListPojo = new HistoryListPojo();
                    historyListPojo.setId(conditionDisclosure.getValue().get(i).getId());
                    historyListPojo.setName(conditionDisclosure.getValue().get(i).getTitle());
                    historyListPojo.setVerified(conditionDisclosure.getValue().get(i).getIsVerified());
                    historyListPojo.setWarningAlerts(conditionDisclosure.getValue().get(i).getWarningAlerts());
                    historyListPojo.setManualAdditionalDisclosures(conditionDisclosure.getValue().get(i).getManualAdditionalDisclosure());
                    historyListPojo.setDependentIds(conditionDisclosure.getValue().get(i).getDependentDislosureIds());

                    AppConstant.commonList.add(historyListPojo);
                }
                ArrayList<HistoryListPojo> localLi = new ArrayList<>();
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                    isCobuyer = true;
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                        localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
                    }

                }
                for (int i = 0; i < localLi.size(); i++) {
                    for (int j = 0; j < AppConstant.commonList.size(); j++) {
                        if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.commonList.get(j).getId()) && localLi.get(i).isChecked()) {
                            AppConstant.commonList.get(j).setChecked(true);
                            AppConstant.commonList.get(j).setAdditionalData(localLi.get(i).getAdditionalData());
                        }
                    }
                }
                boolean atLeastOneVerified = false;
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isVerified()) {
                        atLeastOneVerified = true;
                        break;
                    }
                }
                if (atLeastOneVerified)
                    AppConstant.commonList.get(AppConstant.commonList.size() - 1).setVerified(true);
                adapter.notifyDataSetChanged();
//                if (AppConstant.isNetworkAvail(getActivity())) {
//                    getConditionDisclosure();
//                } else {
//                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
            } else {
                VehicleConditionDisclosure vehicleConditionDisclosure = null;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
                    vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE_NEW), VehicleConditionDisclosure.class);
                else {
                    vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE), VehicleConditionDisclosure.class);
                }
                if (vehicleConditionDisclosure != null && vehicleConditionDisclosure.getValue() != null && vehicleConditionDisclosure.getValue().size() > 0) {
                    ArrayList<HistoryListPojo> localLi = new ArrayList<>();
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                            localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
                            AppConstant.commonList.clear();
                            for (int i = 0; i < vehicleConditionDisclosure.getValue().size(); i++) {
                                HistoryListPojo historyListPojo = new HistoryListPojo();
                                historyListPojo.setId(vehicleConditionDisclosure.getValue().get(i).getId());
                                historyListPojo.setName(vehicleConditionDisclosure.getValue().get(i).getTitle());
                                historyListPojo.setVerified(vehicleConditionDisclosure.getValue().get(i).getIsVerified());
                                AppConstant.commonList.add(historyListPojo);
                            }
                            for (int i = 0; i < localLi.size(); i++) {
                                for (int j = 0; j < AppConstant.commonList.size(); j++) {
                                    if (localLi.get(i).getId().equalsIgnoreCase(AppConstant.commonList.get(j).getId()) && localLi.get(i).isChecked()) {
                                        AppConstant.commonList.get(j).setChecked(true);
                                        AppConstant.commonList.get(j).setAdditionalData(localLi.get(i).getAdditionalData());
                                    }
                                }
                            }
                            Log.e("Size of common: ", AppConstant.commonList.size() + "");
                            boolean atLeastOneVerified = false;
                            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                                if (AppConstant.commonList.get(i).isVerified()) {
                                    atLeastOneVerified = true;
                                    break;
                                }
                            }
                            if (atLeastOneVerified)
                                AppConstant.commonList.get(AppConstant.commonList.size() - 1).setVerified(true);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }
            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                if (AppConstant.commonList.get(i).isChecked() || AppConstant.commonList.get(i).isVerified()) {
                    shouldNextShow = true;
                    break;
                }
            }

            if (shouldNextShow) {
                enableWithGreen(next);
                next.setVisibility(View.VISIBLE);
            } else {
                disableWithGray(next);
//                next.setVisibility(View.INVISIBLE);
            }
            //            recyclerView.setAdapter(new ConditionAdapter(getActivity(), this, AppConstant.commonList, first));
        }
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);

        DSTextView back;
        if (first) {
            back = mView.findViewById(R.id.text_back_up);
        } else {
            back = mView.findViewById(R.id.header1).findViewById(R.id.text_back_up);
        }
        if (getActivity() instanceof NewDealViewPager) {
            mView.setFocusableInTouchMode(true);
            mView.requestFocus();
            mView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        String path_signature;
                        Gson gson = new GsonBuilder().create();
                        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                        File svgFile;
                        if (!isCobuyer)
                            svgFile = new File(path_signature + "/IMG_condition_disclosure.svg");
                        else {
                            svgFile = new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg");
                        }
                        File file;
                        if (!isCobuyer) {
                            file = getOutputFile("UserPic", "condition_disclosure_screen_pic");
                        } else {
                            file = getOutputFile("UserPic", "co_condition_disclosure_screen_pic");
                        }
                        File file1;
                        if (!isCobuyer) {
                            file1 = getOutputFile("Screenshot", "condition_disclosure_screen");
                        } else {
                            file1 = getOutputFile("Screenshot", "co_condition_disclosure_screen");
                        }
                        Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                        Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                        Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                        if (NewDealViewPager.currentPage == 39) {
                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                                ((NewDealViewPager) requireActivity()).setCurrentPage(37);
                            } else {
                                iGoToNextPage.goToBackIndex();
                            }
//                    showFragmentInLandscape();
                        } else {
                            iGoToNextPage.goToBackIndex();
                        }
                        Log.e("Device back: ", " Device back pressed: " + keyCode);
                        return false;
                    }
                    return false;
                }
            });
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = getActivity().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (!isCobuyer)
                    svgFile = new File(path_signature + "/IMG_condition_disclosure.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg");
                }
                File file;
                if (!isCobuyer) {
                    file = getOutputFile("UserPic", "condition_disclosure_screen_pic");
                } else {
                    file = getOutputFile("UserPic", "co_condition_disclosure_screen_pic");
                }
                File file1;
                if (!isCobuyer) {
                    file1 = getOutputFile("Screenshot", "condition_disclosure_screen");
                } else {
                    file1 = getOutputFile("Screenshot", "co_condition_disclosure_screen");
                }
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                if (NewDealViewPager.currentPage == 39) {
                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                        ((NewDealViewPager) requireActivity()).setCurrentPage(37);
                    } else {
                        iGoToNextPage.goToBackIndex();
                    }
//                    showFragmentInLandscape();
                } else {
                    iGoToNextPage.goToBackIndex();
                }

            }
        });
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
        final DSTextView cancel1 =  mView.findViewById(R.id.text_cancelBack1);
//        final DSTextView cancel1 = mView.findViewById(R.id.pageno).findViewById(R.id.text_cancelBack);
        if (!first) {
            cancel1.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.header).setVisibility(View.GONE);
            mView.findViewById(R.id.header1).setVisibility(View.VISIBLE);
            logout.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.VISIBLE);
//            Gson gson = new GsonBuilder().create();
//            final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//            boolean isPhotoAvailable = record.getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
            boolean isPhotoAvailable = isPhotoAvailable();
            if (!isPhotoAvailable) {
                cancel1.setText(String.format(getString(R.string.page1), "6", decidePageNumber()));
            }else {
                cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
            }
//            cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
        } else {
//            cancel.setTextSize(28);
            cancel1.setVisibility(View.GONE);
            mView.findViewById(R.id.header).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.header1).setVisibility(View.GONE);
            logout.setVisibility(View.INVISIBLE);
            if (getActivity() instanceof NewDealViewPager)
                cancel.setVisibility(View.VISIBLE);
            else {
                cancel.setVisibility(View.INVISIBLE);
            }
            cancel.setText(getString(R.string.cancel));
        }
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (getActivity() instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                        pojo.setId(AppConstant.commonList.get(i).getId());
                        pojo.setName(AppConstant.commonList.get(i).getName());
                        historyListPojos1.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                logout();
            }
        });
        cancel.setOnClickListener(view -> {
            cancel.setEnabled(false);
            cancel.setClickable(false);
            new Handler().postDelayed(() -> {
                cancel.setEnabled(true);
                cancel.setClickable(true);
            }, 6000);
            Gson gson1 = new GsonBuilder().create();
            RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (getActivity() instanceof NewDealViewPager)
                recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
            ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                if (AppConstant.commonList.get(i).isChecked()) {
                    HistoryListPojo pojo = new HistoryListPojo();
                    pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                    pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                    pojo.setId(AppConstant.commonList.get(i).getId());
                    pojo.setName(AppConstant.commonList.get(i).getName());
                    historyListPojos1.add(pojo);
                }
            }
            recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
            String recordDataString = gson1.toJson(recordData1);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            cancel();
        });

        signaturePad = mView.findViewById(R.id.signaturePad);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
//                next.setVisibility(View.GONE);
                sign.setVisibility(View.VISIBLE);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                sign.setVisibility(View.GONE);
                time.setText(getCurrentTimeString());
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
//                remove if condition for bullet point
//                if (allTrue && allAdditional) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    enableWithGreen(next);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    } else if (isTaken) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
//                }
                isSigned = true;
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });
        final DSTextView signature = mView.findViewById(R.id.signature);
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        disableWithGray(next);
//                        next.setVisibility(View.GONE);
                        sign.setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return true;
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                boolean goNext = false;
                if (first) {
                    for (int i = 0; i < AppConstant.commonList.size(); i++) {
                        if (AppConstant.commonList.get(i).isChecked()) {
                            goNext = true;
                            break;
                        }
                    }
                } else {
                    goNext = true;
                }

                if (!goNext) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_one_option));
                } else {

                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (getActivity() instanceof NewDealViewPager)
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                    ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                    for (int i = 0; i < AppConstant.commonList.size(); i++) {
                        if (AppConstant.commonList.get(i).isChecked()) {
                            HistoryListPojo pojo = new HistoryListPojo();
                            pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                            pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                            pojo.setId(AppConstant.commonList.get(i).getId());
                            pojo.setName(AppConstant.commonList.get(i).getName());
                            pojo.setVerified(AppConstant.commonList.get(i).isVerified());
                            historyListPojos1.add(pojo);
                        }
                    }
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
//                    else
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoConditionSelection(historyListPojos1);

                    if (!first) {
                        if (!allTrue || !allAdditional) {
                            new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                            enableNext(next);
                            return;
                        }
                        disableNext(next);
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(AppConstant.condition);
                        else
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoConditionSelection(AppConstant.condition);
                        addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        int languageId = 1;
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                            languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
                        else {
                            languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
                        }
                        if (history != null && history.get(history.size() - 1).isChecked()) {
                            page = 76;
                        }
                        if (history != null && history.get(history.size() - 1).isChecked() && report != null && report.get(report.size() - 1).isChecked()) {
                            page = 77;
                        }
                        if (page != 0) {
                            playSound(languageId);
                            ((NewDealViewPager) getActivity()).setCurrentPage(page);
                        } else {
                            iGoToNextPage.whatNextClick();
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                        File file;
                        if (!isCobuyer) {
                            file = getOutputFile("Screenshot", "condition_disclosure_screen");
                        } else {
                            file = getOutputFile("Screenshot", "co_condition_disclosure_screen");
                        }
                        scrollableScreenshot(disclosureMain, file);
                    } else {
//                        enableNext(next);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        if (getActivity() instanceof NewDealViewPager) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                                ((NewDealViewPager) getActivity()).setCurrentPage(28);
                            } else {
                                iGoToNextPage.whatNextClick();
                            }
                        } else {
                            iGoToNextPage.whatNextClick();
                        }
                    }

                }
            }
        });
    }

    private void saveRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (getActivity() instanceof NewDealViewPager)
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
        ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
        for (int i = 0; i < AppConstant.commonList.size(); i++) {
            if (AppConstant.commonList.get(i).isChecked()) {
                HistoryListPojo pojo = new HistoryListPojo();
                pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                pojo.setId(AppConstant.commonList.get(i).getId());
                pojo.setName(AppConstant.commonList.get(i).getName());
                pojo.setVerified(AppConstant.commonList.get(i).isVerified());
                historyListPojos1.add(pojo);
            }
        }
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
//                    else
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoConditionSelection(historyListPojos1);

        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
    }

    private void checkIfReportAvailable(boolean carfax, String thirdPartyId) {
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new Gson();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String vin = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
        int index = 0;
        String name = null;
        ArrayList<ThirdPartyObj> listLangName = new ArrayList<>();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null)
            listLangName = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
        for (int i = 0; i < listLangName.size(); i++) {
            if (listLangName.get(i).getId().equalsIgnoreCase(thirdPartyId)) {
                name = listLangName.get(i).getTitle();
                index = i;
            }
        }
        if (name != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes() != null &&
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue().get(index).getThirdPartyReportBytes().getSucceeded().equalsIgnoreCase("true")) {
            Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
            intent.putExtra("url", name);
            intent.putExtra("id", thirdPartyId);
            intent.putExtra("isCoBuyer", isCobuyer);
            intent.putExtra("customerResponse", !first);
            intent.putExtra("showBox", false);
            intent.putExtra("isDisclosure", first);
            requireActivity().startActivity(intent);
        } else {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                ArrayList<InitThirdParty> initList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
                boolean idFound = false;
                for (int i = 0; i < initList.size(); i++) {
                    if (initList.get(i).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && initList.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                        if (thirdPartyId.equalsIgnoreCase("1"))
                            showAlert(initList.get(i).getErrorsString(), carfax, thirdPartyId, "CARFAX Not Available. Please make manual disclosures.");
                        else if (thirdPartyId.equalsIgnoreCase("2")) {
                            showAlert(initList.get(i).getErrorsString(), carfax, thirdPartyId, "AutoCheck Not Available. Please make manual disclosures.");
                        }
                        idFound = true;
                        break;
                    }
                }
                if (!idFound) {
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (record.getVehicleDetails().getInitiateThirdParty() == null || record.getVehicleDetails().getInitiateThirdParty().getValue().size() == 0) {
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            callThirdPartyInitiateAPI(vin, thirdPartyId, carfax);
                        } else {
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }
                    } else {
                        if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                            ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                            boolean isActive = false;
                            String fileName = "";
                            for (int i = 0; i < locallist.size(); i++) {
                                if (locallist.get(i).getId().equalsIgnoreCase(thirdPartyId) && locallist.get(i).isActive()) {
                                    isActive = true;
                                    fileName = locallist.get(i).getTitle();
                                    break;
                                }
                            }
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(fileName.trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (isActive) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    if (DSAPP.getInstance().isNetworkAvailable()) {
                                                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                                        progressDialog.setMessage("Please wait... We are downloading the report.");
                                                        progressDialog.show();
                                                        progressDialog.setCancelable(false);
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), thirdPartyId, fileName.trim(), progressDialog);
                                                    } else {
                                                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                    }
                                                    break;
                                                }
                                            } else {
                                                Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                                intent.putExtra("url", fileName);
                                                intent.putExtra("id", thirdPartyId);
                                                intent.putExtra("isCoBuyer", false);
                                                intent.putExtra("customerResponse", carfax);
                                                intent.putExtra("isDisclosure", true);
                                                requireActivity().startActivity(intent);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
//                            }
                        }
                    }
                }

            } else {
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                if (record.getVehicleDetails().getInitiateThirdParty() == null || record.getVehicleDetails().getInitiateThirdParty().getValue().size() == 0) {
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        callThirdPartyInitiateAPI(vin, thirdPartyId, carfax);
                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }
                } else {
                    if (record.getVehicleDetails().getThirdPartyList() != null && record.getVehicleDetails().getThirdPartyList().getValue().size() != 0) {
                        ArrayList<ThirdPartyObj> locallist = record.getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < locallist.size(); i++) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                                for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(locallist.get(i).getId()) && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                        File file = ShowImageFromPath(locallist.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                        if (locallist.get(i).isActive()) {
                                            if (file == null || !file.exists()) {
                                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                                    if (DSAPP.getInstance().isNetworkAvailable()) {
                                                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                                                        progressDialog.setMessage("Please wait... We are downloading the report.");
                                                        progressDialog.show();
                                                        progressDialog.setCancelable(false);
                                                        getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), locallist.get(i).getId(), locallist.get(i).getTitle().trim(), progressDialog);
                                                    } else {
                                                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //"AutoCheck Not Available. Please make manual disclosures."
    private void showAlert(final String msg, final boolean carfax, final String thirdPartyId) {
        if (getActivity() != null) {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
            titleTV.setVisibility(View.GONE);
            messageTV.setText(msg);
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file;
                    if (carfax) {
                        if (isCobuyer) {
                            if (thirdPartyId.equalsIgnoreCase("1"))
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_co_buyer_cf");
                            else {
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_co_buyer_ac");
                            }
                        } else {
                            if (thirdPartyId.equalsIgnoreCase("1"))
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_buyer_cf");
                            else {
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_buyer_ac");
                            }
                        }
                    } else {
                        if (thirdPartyId.equalsIgnoreCase("1"))
                            file = getOutputFile("Screenshot", "alert_report_fetching_error_cf");
                        else {
                            file = getOutputFile("Screenshot", "alert_report_fetching_error_ac");
                        }
                    }
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(msg);
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(msg);
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    alertD.cancel();
                }
            });


        }
    }

    private void showAlert(final String msg, final boolean carfax, final String thirdPartyId, String title) {
        if (getActivity() != null) {

            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
            titleTV.setText(title);
            titleTV.setVisibility(View.VISIBLE);
            messageTV.setText(msg);
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file;
                    if (carfax) {
                        if (isCobuyer) {
                            if (thirdPartyId.equalsIgnoreCase("1"))
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_co_buyer_cf");
                            else {
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_co_buyer_ac");
                            }
                        } else {
                            if (thirdPartyId.equalsIgnoreCase("1"))
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_buyer_cf");
                            else {
                                file = getOutputFile("Screenshot", "alert_condition_response_report_fetching_error_buyer_ac");
                            }
                        }
                    } else {
                        if (thirdPartyId.equalsIgnoreCase("1"))
                            file = getOutputFile("Screenshot", "alert_report_fetching_error_cf");
                        else {
                            file = getOutputFile("Screenshot", "alert_report_fetching_error_ac");
                        }
                    }
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(msg);
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("Report fetching Error");
                            customerVehicleDealAlerts.setAlertDesc(msg);
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    alertD.cancel();
                }
            });


        }
    }

    private void callThirdPartyInitiateAPI(final String vin, final String thirdPartyId, final boolean carfax) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait... We are downloading the report.");
        progressDialog.show();
        progressDialog.setCancelable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        },20000);
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().initiateThirdParty("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vin, id).enqueue(new Callback<InitiateThirdParty>() {
            @Override
            public void onResponse(Call<InitiateThirdParty> call, Response<InitiateThirdParty> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new Gson();
                    Log.e("Initiate TP: ", gson.toJson(response.body()));

                    if (response.body().getSucceeded().equalsIgnoreCase("true")) {
                        boolean download = false;
                        for (int i = 0; i < response.body().getValue().size(); i++) {
                            if (response.body().getValue().get(i).getThirdPartyID().equalsIgnoreCase(thirdPartyId) && !response.body().getValue().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                                download = true;
                            } else {
//                                showAlert(response.body().getValue().get(i).getErrorsString(), carfax, thirdPartyId);
                                if (thirdPartyId.equalsIgnoreCase("1"))
                                    showAlert(response.body().getValue().get(i).getErrorsString(), carfax, thirdPartyId, "CARFAX Not Available. Please make manual disclosures.");
                                else if (thirdPartyId.equalsIgnoreCase("2")) {
                                    showAlert(response.body().getValue().get(i).getErrorsString(), carfax, thirdPartyId, "AutoCheck Not Available. Please make manual disclosures.");
                                }
                            }
                            break;
                        }
                        if (download) {
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setInitiateThirdParty(response.body());
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            getThirdParty(thirdPartyId, progressDialog, carfax);
                        } else {
                            progressDialog.dismiss();
//                            new CustomToast(getActivity()).alert("AutoCheck Login Expired");
                        }

                    } else {
                        progressDialog.dismiss();
                        if (thirdPartyId.equalsIgnoreCase("1"))
                            showAlert(response.body().getErrorsString(), carfax, thirdPartyId, "CARFAX Not Available. Please make manual disclosures.");
                        else if (thirdPartyId.equalsIgnoreCase("2")) {
                            showAlert(response.body().getErrorsString(), carfax, thirdPartyId, "AutoCheck Not Available. Please make manual disclosures.");
                        }
//                        showAlert(response.body().getErrorsString(), carfax, thirdPartyId);
//                        new CustomToast(getActivity()).alert(response.body().getErrorsString());
                    }
                }
            }

            @Override
            public void onFailure(Call<InitiateThirdParty> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Initiate Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void getThirdParty(final String vin, final ProgressDialog progressDialog, final boolean carfax) {

//        new CustomToast(getActivity()).alert("Please wait we are downloading the report.");
        RetrofitInitialization.getDs_services().getThirdPartyList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), "0", PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "").enqueue(new Callback<ThirdPartyList>() {
            @Override
            public void onResponse(Call<ThirdPartyList> call, Response<ThirdPartyList> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ArrayList<ThirdPartyObj> list = response.body().getValue();
                    ThirdPartyList vehicleConditionDisclosure;
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new ThirdPartyList();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setViewReport(false);
                            list.get(i).setActive(false);
                            list.get(i).setMandatory(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setThirdPartyList(vehicleConditionDisclosure);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Log.i("ThirdParty: ", gson.toJson(vehicleConditionDisclosure));
                    }
                    String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
                    for (int i = 0; i < list.size(); i++) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
                            for (int j = 0; j < recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size(); j++) {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyID().equalsIgnoreCase(list.get(i).getId())) {
                                    File file = ShowImageFromPath(list.get(i).getTitle().trim() + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId() + ".pdf", "localPdf");
                                    if (list.get(i).isActive()) {
                                        if (file == null || !file.exists()) {
                                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId().equalsIgnoreCase("0"))
                                                getAllThirdPartyReports(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().get(j).getThirdPartyReportRawId(), list.get(i).getId(), list.get(i).getTitle().trim(), progressDialog);
                                        } else {
                                            Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
                                            intent.putExtra("url", list.get(i).getTitle());
                                            intent.putExtra("id", list.get(i).getId());
                                            intent.putExtra("isCoBuyer", false);
                                            intent.putExtra("customerResponse", carfax);
                                            intent.putExtra("isDisclosure", true);
                                            requireActivity().startActivity(intent);
                                            progressDialog.dismiss();
                                            break;
                                        }
                                    }
                                    progressDialog.dismiss();
                                }
                            }
                        }
                    }
                    Log.e("Size of reports: ", response.body().getValue().size() + "");
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyList> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(getActivity())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    private void getAllThirdPartyReports(final String TPRRID, final String tpId, final String name, final ProgressDialog progressDialog) {

//        "40334", "2g1fd1e34f9207229", "1", lng
        RetrofitInitialization.getDs_services().getThirdPartyReportsBytes("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), TPRRID).enqueue(new Callback<ThirdPartyReportBytes>() {
            @Override
            public void onResponse(Call<ThirdPartyReportBytes> call, Response<ThirdPartyReportBytes> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getSucceeded().equalsIgnoreCase("true") && response.body().getMessage() != null && response.body().getMessage().length() > 0) {
                        File dwldsPath = getOutputMediaFilePDF("localPdf", name.trim() + TPRRID);
                        byte[] pdfAsBytes = Base64.decode(response.body().getMessage(), 0);
                        FileOutputStream os;
                        PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, false);
                        PreferenceManger.putString(AppConstant.ALERT, "");
                        try {
                            os = new FileOutputStream(dwldsPath, false);
                            os.write(pdfAsBytes);
                            os.flush();
                            os.close();
//                            Log.e("File is creating: ", dwldsPath.getName() + " " + dwldsPath.length());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {

//                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        ArrayList<ThirdPartyObj> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getId().equalsIgnoreCase(tpId)) {
                                list.get(i).setThirdPartyReportBytes(response.body());
                            }
                        }
                        if (tpId.equalsIgnoreCase("1") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            alerts.setAlertName("CarFax Report is Not Available");
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "CarFax: " + response.body().getErrorMessage());
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCarFaxAvailable(alerts);
                        } else if (tpId.equalsIgnoreCase("2") && response.body().getSucceeded().equalsIgnoreCase("false")) {
                            CustomerVehicleDealAlerts alerts = new CustomerVehicleDealAlerts();
                            PreferenceManger.putBoolean(AppConstant.ALERT_AVAILABLE, true);
                            PreferenceManger.putString(AppConstant.ALERT, "AutoCheck: " + response.body().getErrorMessage());
                            alerts.setAlertName("AutoCheck Report is Not Available");
                            alerts.setAlertDesc(response.body().getErrorMessage());
                            alerts.setDocId(tpId);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAutoCheckAvailable(alerts);
                        }
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().setValue(list);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
//                        initView();
//                        adapter.notifyDataSetChanged();
//                        if (response.body().getSucceeded().equalsIgnoreCase("true")){
                        new CustomToast(getActivity()).alert("Report Downloaded. Click again to view.");
//                        }else {
//                            new CustomToast(getActivity()).alert(response.body().getErrorMessage());
//                        }


//                        Intent intent = new Intent(getActivity(), CustomWebViewActivity.class);
//                        intent.putExtra("url", name);
//                        intent.putExtra("id", tpId);
//                        intent.putExtra("isCoBuyer", false);
//                        intent.putExtra("isDisclosure", true);
//                        Objects.requireNonNull(getActivity()).startActivity(intent);
                    }
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ThirdPartyReportBytes> call, Throwable t) {
                progressDialog.dismiss();
                Timber.e("While downloading the reports: " + t.getLocalizedMessage());
            }
        });
    }

    public File getOutputMediaFilePDF(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private boolean removeIndex6(ArrayList<HistoryListPojo> commonList) {
        ArrayList<HistoryListPojo> list = new ArrayList<>(commonList);
        for (int i = 0; i < list.size(); i++) {
            if (commonList.get(i).getId().equalsIgnoreCase("6")) {
                return true;
            }
        }
        return false;
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            File svgFile;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                svgFile = getOutputMediaFile("Signatures", "co_buyer_condition_disclosure");
            else {
                svgFile = getOutputMediaFile("Signatures", "condition_disclosure");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    @Override
    public void languageSelected(boolean allSelected, boolean isCancel, boolean isOK) {
        if (first) {
            shouldNextShow = false;
            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                if (AppConstant.commonList.get(i).isChecked()) {
                    shouldNextShow = true;
                    /*Added to remove delay 11:44 24/06/2020*/
                    break;
                }
            }
            if (shouldNextShow) {
                enableWithGreen(next);
                next.setVisibility(View.VISIBLE);
            } else {
                disableWithGray(next);
                next.setVisibility(View.INVISIBLE);
            }
            if (isCancel) {
                cancel();
            }
            if (isOK) {
                File file = getOutputFile("Screenshot", "alert_warning_disclosures");
                takeScreenshotForAlert(file);
            }
        } else {
            //made true for bullet point
            allTrue = true;
//            allTrue = allSelected;
            if (allTrue && isSigned && allAdditional) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }, 100);
            } else {
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
        }
    }

    private String getIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase("Prior Damage")) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase("Prior Accident")) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase("Prior Fleet Vehicle")) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase("Prior Commercial Vehicle")) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase("The vehicle has been Painted")) {
            id = "5";
        } else if (aList.trim().equalsIgnoreCase("Possible Frame/Unibody or Structural Damage – which means damage to any component designed to provide structural integrity of the vehicle. Even minor accidents can cause structural/frame damage to a vehicle. Having a structural inspection before purchase is recommended.")) {
            id = "6";
        } else if (aList.trim().equalsIgnoreCase("Lowered/Raised and/or Lifted with Aftermarket Oversized Wheels and/or Low Profile Tires and/or Altered Suspension. Structural alteration. Manufacturer’s warranty may be void.")) {
            id = "7";
        } else if (aList.trim().equalsIgnoreCase("Modified and/or altered from manufacturer’s specifications, and/or aftermarket equipment(s)/accessories. Manufacturer’s warranty may be void.")) {
            id = "8";
        } else if (aList.trim().equalsIgnoreCase("Correct Odometer or Mileage unknown")) {
            id = "9";
        } else if (aList.trim().equalsIgnoreCase("May be an out-of-state vehicle and it may have an out-of-state title")) {
            id = "10";
        } else if (aList.trim().equalsIgnoreCase("Airbag has been previously deployed")) {
            id = "11";
        } else if (aList.trim().equalsIgnoreCase("Known Frame/Unibody or Structural Damage – which means damage to any component designed to provide structural integrity of the vehicle. Even minor accidents can cause structural/frame damage to a vehicle. Having a structural inspection before purchase is recommended.")) {
            id = "12";
        } else if (aList.trim().equalsIgnoreCase("Open Recall")) {
            id = "13";
        } else if (aList.trim().equalsIgnoreCase("Other")) {
            id = "14";
        } else if (aList.trim().equalsIgnoreCase("BASED ON THE INFORMATION WE ARE AWARE OF, NONE OF THE ABOVE CONDITIONS OR HISTORY DISCLOSURES APPLY TO THE VEHICLE.")) {
            id = "0";
        }
        return id;
    }

    @Override
    public void languageSelected(boolean allSelected, String id) {
        allAdditional = allSelected;
        if (allSelected && allAdditional && isSigned) {
            enableWithGreen(next);
            next.setVisibility(View.VISIBLE);
        } else {
            disableWithGray(next);
//            if (first)
//                next.setVisibility(View.GONE);
        }
    }
    private void getConditionDisclosure(String vin) {
        getAdditionalDisclosures(vin, "CONDITION");
        getAdditionalDisclosures(vin, "HISTORY");
        ProgressDialog progressDialog1 = new ProgressDialog(getActivity());
        progressDialog1.setMessage("Verifying Disclosures...");
        progressDialog1.setCancelable(false);
        progressDialog1.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog1.setCancelable(true);
            }
        },14000);
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(true);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        String vehicleType = "1";
        ArrayList<HistoryListPojo> localLi = new ArrayList<>();
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
            if (record.getVehicleDetails().getUsedVehicleCondition() != null) {
                localLi = new ArrayList<>(record.getVehicleDetails().getUsedVehicleCondition());
            }

            if (record.getVehicleDetails().getTypeOfVehicle() != null) {
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                    vehicleType = "1";
                } else {
                    vehicleType = "2";
                }
            }
        }

//WBAFR9C59BC270614  recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber()
        RetrofitInitialization.getDs_services().getVehicleConditionDisclosure("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, vin, "0", vehicleType).enqueue(new Callback<VehicleConditionDisclosure>() {
            @Override
            public void onResponse(Call<VehicleConditionDisclosure> call, Response<VehicleConditionDisclosure> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    VehicleConditionDisclosure vehicleConditionDisclosure = response.body();
                    ArrayList<Value> list = response.body().getValue();
                    if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                        vehicleConditionDisclosure = new VehicleConditionDisclosure();
                        for (int i = 0; i < list.size(); i++) {
                            list.get(i).setIsVerified(false);
                        }
                        vehicleConditionDisclosure.setValue(list);
                    } else
                        vehicleConditionDisclosure = response.body();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setVehicleConditionDisclosure(vehicleConditionDisclosure);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setWarningAlerts(response.body().getWarningAlerts());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setOfflineData(false);
//                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setDataDownloading(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionVerified(true);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        initView();
                        Timber.i(gson.toJson(vehicleConditionDisclosure));

//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.CONDITION_DISCLOSURE, dealershipDataString);
//                        Timber.e("%s", response.body().getValue().size());
                    }
                    progressDialog1.dismiss();
                }
            }

            @Override
            public void onFailure(Call<VehicleConditionDisclosure> call, Throwable t) {
                progressDialog1.dismiss();
                Log.e("error: ", t.getLocalizedMessage());

            }
        });
    }

    private void getAdditionalDisclosures(String vin, final String type) {
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getAdditionalDisclosures("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), /*"1FM5K8D84EGB73657"*/vin, /*"50348"*/id, type).enqueue(new Callback<AdditionalDisclosure>() {
            @Override
            public void onResponse(Call<AdditionalDisclosure> call, Response<AdditionalDisclosure> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                        if (type.equalsIgnoreCase("CONDITION") /*&& (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue().size() == 0)*/) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue().size() > 0) {
                                ArrayList<Value> local = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue();
                                ArrayList<Value> local1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure().getValue();
                                if (response.body() != null && response.body().getValue() != null && response.body().getValue().size() > 0) {
                                    for (int i = 0; i < response.body().getValue().size(); i++) {
                                        for (int j = 0; j < local.size(); j++) {
                                            if (response.body().getValue().get(i).getId().equalsIgnoreCase(local.get(j).getId())) {
                                                local1.get(j).setManual(false);
                                            }
                                        }
                                    }
                                }
                                AdditionalDisclosure additionalDisclosure1 = new AdditionalDisclosure();
                                additionalDisclosure1.setValue(local1);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(additionalDisclosure1);
                            } else {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(response.body());
                            }
                        } else if (type.equalsIgnoreCase("HISTORY")) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalHistoryDisclosure(response.body());
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        Log.i("AdditionalDisclosure: ", gson.toJson(response.body()));
//                    String dealershipDataString = gson.toJson(response.body());
//                    PreferenceManger.putString(AppConstant.HISTORY_DISCLOSURE, dealershipDataString);
//                        Log.e("Size of history: ", response.body().getValue().size() + "");
                    }
                } else {
                    try {
                        Log.e("Error Addtnl ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AdditionalDisclosure> call, Throwable t) {

            }
        });
    }

}
