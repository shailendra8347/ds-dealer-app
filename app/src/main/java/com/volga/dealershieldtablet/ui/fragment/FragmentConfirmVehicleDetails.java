package com.volga.dealershieldtablet.ui.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.TreeMap;

import timber.log.Timber;

import static android.os.Looper.getMainLooper;


public class FragmentConfirmVehicleDetails extends BaseFragment {

    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isConfirmSelected = false;
    IGoToNextPage iGoToNextPage;
    private Button yes, no;
    private DSTextView header, vinNumber, typeOfVehicle, mileage, make, model, year;
    private boolean isCoBuyer;
    private boolean isRemote;
    private DSTextView time;
    boolean isTaken;
    private DSTextView sign;
    private ImageView delete;
    LinearLayout mileageLL;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && isConfirmSelected && isSigned) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }

                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.confirm_car_information, container, false);
        isSigned = false;
        isConfirmSelected = false;
        if (NewDealViewPager.currentPage == 71) {
            initPhoto();
        }
        initView();
        return mView;
    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isConfirmSelected && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isConfirmSelected && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                        enableWithGreen(next);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
//                        isTaken = true;
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("UserPic", "car_info_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_car_info_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            signaturePad.clear();
            time.setText(getCurrentTimeString());

//            enableNext(next);
            isSigned = false;
            updateData();
            initPhoto();
            disableWithGray(next);
            //Code for displaying already selected Confirmation
        } else {
            isTaken = false;
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        next = mView.findViewById(R.id.text_next);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        time = mView.findViewById(R.id.time);
        vinNumber = mView.findViewById(R.id.vinNumber);
        typeOfVehicle = mView.findViewById(R.id.vehicleType);
        mileage = mView.findViewById(R.id.mileageKm);
        make = mView.findViewById(R.id.make);
        model = mView.findViewById(R.id.model);
        year = mView.findViewById(R.id.year);
        header = mView.findViewById(R.id.heading);
        mileageLL = mView.findViewById(R.id.mileageLL);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
//        boolean isPhotoAvailable = record.getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
        boolean isPhotoAvailable = isPhotoAvailable();
        if (!isPhotoAvailable) {
            text_cancelBack.setText(String.format(getString(R.string.page1), "3", decidePageNumber()));
        } else {
            text_cancelBack.setText(String.format(getString(R.string.page1), "4", decidePageNumber()));
        }
//        text_cancelBack.setText(String.format(getString(R.string.page1), "4", decidePageNumber()));
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
        updateData();

        if (!isCoBuyer || isRemote) {
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    yes.setTextColor(getResources().getColor(R.color.white));
                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                    if (isSigned) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);
                            }
                        }, 100);
                    }
                    isConfirmSelected = true;
                }
            });
        }
        if (!isCoBuyer || isRemote) {
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*show confirmtion pop up*/
                    disableWithGray(next);
                    isConfirmSelected = false;
                    no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    no.setTextColor(getResources().getColor(R.color.white));
                    yes.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                    yes.setTextColor(getResources().getColor(R.color.button_color_blue));
                    new AlertDialog.Builder(getActivity())
                            .setMessage(getString(R.string.are_you_sure))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                                    intent.putExtra(AppConstant.FROM_FRAGMENT, "handover");
                                    getActivity().startActivity(intent);


                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                    no.setBackground(getActivity().getDrawable(R.drawable.ractangle_border_blue));
                                    no.setTextColor(getResources().getColor(R.color.button_color_blue));
                                }
                            }).show();
                }
            });
        }

        disableWithGray(next);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "car_info_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_car_info_screen");
                }
                scrollableScreenshot(mainContent, file);
                iGoToNextPage.whatNextClick();
            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (!isCoBuyer)
                        svgFile = new File(path_signature + "/IMG_confirm_car_details_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg");
                    }
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("Screenshot", "car_info_screen");
                    } else {
                        file = getOutputFile("Screenshot", "co_car_info_screen");
                    }
                    File file1;
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "car_info_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_car_info_screen_pic");
                    }
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (!isCoBuyer)
                    svgFile = new File(path_signature + "/IMG_confirm_car_details_selection.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg");
                }
                File file;
                if (!isCoBuyer) {
                    file = getOutputFile("Screenshot", "car_info_screen");
                } else {
                    file = getOutputFile("Screenshot", "co_car_info_screen");
                }
                File file1;
                if (!isCoBuyer) {
                    file1 = getOutputFile("UserPic", "car_info_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "co_car_info_screen_pic");
                }
                Timber.e(svgFile.getName() + " is deleted : " + svgFile.delete());
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                iGoToNextPage.goToBackIndex();
            }
        });
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                isSigned = true;
                if (isConfirmSelected) {
                    int timeDelay = 50;
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    } else if (isTaken) {
                        next.setVisibility(View.VISIBLE);
                        enableWithGreen(next);
                    }
                }
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;

                disableWithGray(next);
            }
        });
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        disableWithGray(next);
                        isSigned = false;
                        signaturePad.clear();

                        return true;
                    }
                }
                return true;
            }
        });

    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        SettingsAndPermissions setting = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();

        if (setting.isHideMileage() && mileageLL != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal() && (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getMileage() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getMileage().equalsIgnoreCase("null") || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getMileage().isEmpty())) {
            mileageLL.setVisibility(View.GONE);
        } else {
            assert mileageLL != null;
            mileageLL.setVisibility(View.GONE);
        }
        isCoBuyer = setting.isCoBuyer();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }
//        if (isCoBuyer){
//            isConfirmSelected = true;
//            yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
//            no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_gray));
//        }

        isRemote = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote();

        VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
        if (yes != null) {
//                header.setText(setting.get);
            sign = mView.findViewById(R.id.signhere);
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                sign.setText(Html.fromHtml(getString(R.string.signature)));
            } else {
                sign.setText(Html.fromHtml(getString(R.string.co_signature)));
            }
            make.setText(MessageFormat.format(": {0}", vehicleDetails.getCarMake()));
            model.setText(MessageFormat.format(": {0}", vehicleDetails.getCarModel()));
            year.setText(MessageFormat.format(": {0}", vehicleDetails.getCarYear()));
            mileage.setText(MessageFormat.format(": {0}", getCorrectText(vehicleDetails.getMileage())));
            typeOfVehicle.setText(MessageFormat.format(": {0}", vehicleDetails.getTypeOfVehicle()));
            vinNumber.setText(MessageFormat.format(": {0}", vehicleDetails.getVINNumber()));
            if (setting.isCarDetailConfirmed()) {
                isConfirmSelected = true;
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                yes.setTextColor(getResources().getColor(R.color.white));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.ractangle_border_blue));
            }
        }
    }

    private String getCorrectText(String string) {
        if (string == null) {
            return "See Photos";
        } else if (string.equalsIgnoreCase("null")) {
            return "See Photos";
        } else if (string.isEmpty()) {
            return "See Photos";
        } else {
            return string;
        }
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile;
            if (isCoBuyer) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_confirm_car_details_selection");
            } else {
                svgFile = getOutputMediaFile("Signatures", "confirm_car_details_selection");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCarDetailConfirmed(true);
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

    }

}
