package com.volga.dealershieldtablet.ui.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyReportDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.customUI.EULAWebView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.InputStream;
import java.util.ArrayList;

public class WebViewActivity extends BaseActivity2 {
    private DSTextView text_next, text_back;
    private int height;
    private String redirectURL = "http://stg.dealerxt.com/Resource/ThirdPartyReportViewed?docId=123440";
    private boolean isCoBuyer;

    @Override
    public void onBackPressed() {
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_web_thirdparty);
        final EULAWebView mWebview = findViewById(R.id.webview);
//        mWebview.setOnBottomReachedListener(this, 50);
        String path = getIntent().getStringExtra("url");
        isCoBuyer = getIntent().getBooleanExtra("isCoBuyer", false);
//        mWebview.loadData(path, "text/html", "UTF-8");
        text_next = findViewById(R.id.text_next);
        text_back = findViewById(R.id.txt_logout);
        text_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mWebview.canGoBack()) {
                    mWebview.goBack();
                } else {
                    new AlertDialog.Builder(WebViewActivity.this)
                            .setMessage(R.string.are_you_sure)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    finish();
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                }
                            }).show();
                }

            }
        });
        text_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        text_next.setVisibility(View.GONE);
        mWebview.loadUrl(path);
//        mWebview.loadData(getString(R.string.report_data),
//                "text/html", "UTF-8");
        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
//        mWebview.getSettings().sets
        mWebview.setVerticalScrollBarEnabled(true);
        final ProgressBar progressBar = findViewById(R.id.progressBar);

//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
//        Log.e("Device height: ", "H: " + height);
//        int webH = (int) Math.floor(mWebview.getContentHeight() * mWebview.getScale());
//        ;
//        Log.e("Web height: ", "W H: " + webH);
        progressBar.setVisibility(View.VISIBLE);
        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
//                Log.e("url", "String url: " + req.getUrl().toString());
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                Log.e("url", "String url: " + url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                injectCSS(view);
                /*Implement this according buyer and co-buyer*/

                if (url.contains("ThirdPartyReportViewed")) {
                    Gson gson = new Gson();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    VehicleDetails vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();

                    ArrayList<ThirdPartyReportDocs> reportDocs = vehicleDetails.getThirdPartyReportDocs();
                    if (reportDocs == null) {
                        reportDocs = new ArrayList<>();
                    }
                    ThirdPartyReportDocs partyReportDocs = new ThirdPartyReportDocs();
                    String substring = url.trim().substring(url.trim().indexOf("docId=") + 6);
                    partyReportDocs.setDocId(substring);
                    if (isCoBuyer) {
                        partyReportDocs.setDocTypeName("cobuyer");
                    } else {
                        partyReportDocs.setDocTypeName("buyer");
                    }
                    partyReportDocs.setThirdPartyId(getIntent().getStringExtra("id"));
                    reportDocs.add(partyReportDocs);
                    vehicleDetails.setThirdPartyReportDocs(reportDocs);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).setVehicleDetails(vehicleDetails);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                    AppConstant.report.
                    int index = 0;
                    for (int i = 0; i < AppConstant.report.size(); i++) {
                        if (AppConstant.report.get(i).getId().equalsIgnoreCase(getIntent().getStringExtra("id"))) {
                            index = i;
                            break;
                        }
                    }
                    AppConstant.report.get(index).setReportSeen(true);
                    AppConstant.report.get(index).setChecked(true);
                    Log.e("url", "String url: " + url + " ID: " + substring);
                    finish();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    // Inject CSS method: read style.css from assets folder
// Append stylesheet to document head
    private void injectCSS(WebView webView) {
        try {
            InputStream inputStream = getAssets().open("style.css");
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            String encoded = Base64.encodeToString(buffer, Base64.NO_WRAP);
            webView.loadUrl("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var style = document.createElement('style');" +
                    "style.type = 'text/css';" +
                    // Tell the browser to BASE64-decode the string into your script !!!
                    "style.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(style)" +
                    "})()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
