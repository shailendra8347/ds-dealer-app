package com.volga.dealershieldtablet.ui.fragment;

import static android.os.Looper.getMainLooper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.adapter.CheckListAdapter;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckList;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListUpload;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentFinalReport extends BaseFragment implements CheckListAdapter.AllChecked {


    private View mView;
    IGoToNextPage iGoToNextPage;
    private DSTextView next, yes, no;
    private String[] list;
    private SignaturePad signaturePad;
    private boolean isSigned;
    private CheckListAdapter adapter;
    //changed for bullet point
    boolean isAllChecked = true;
    private ArrayList<CheckList> checkLists = new ArrayList<>();
    private DSTextView progress;
    private DSTextView time;
    boolean isTaken;
    private DSTextView sign;
    private ImageView delete;

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isAllChecked && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                enableWithGreen(next);}
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                }else if (picturesTaken.size()==1){
                                    isTaken = true;
                                    if (isAllChecked && isSigned) {
                                        next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);}
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }
//    @Override
//    public void onResume() {
//        super.onResume();
//        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
//    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && isAllChecked && isSigned){
                        next.setVisibility(View.VISIBLE);enableWithGreen(next);}
                }
            }
        }
    };
    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.report, container, false);
        initView();
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            isTaken = false;
        } else {
            if (signaturePad != null) {
                time.setText(getCurrentTimeString());
//                next.setVisibility(View.GONE);
                disableWithGray(next);
                signaturePad.clear();
                isSigned = false;
//                enableNext(next);
                initPhoto();
            }
        }
    }

    private void initPhoto() {
        Handler mainHandler = new Handler(getActivity().getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
                    Gson gson = new GsonBuilder().create();
                    File file;
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file = getOutputFile("UserPic", "co_check_list_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "check_list_screen_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (DSAPP.getInstance().isNetworkAvailable()) {
//            getCheckListItems();
//        } else {
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
        checkLists.clear();
        Gson gson = new GsonBuilder().create();
        CheckListPojo checkListPojo = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CHECK_LIST), CheckListPojo.class);
        final String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        ArrayList<CheckList> listPojos = new ArrayList<>(checkListPojo.getCheckList());
        for (int i = 0; i < listPojos.size(); i++) {
            listPojos.get(i).setIsActive(false);
        }
        VehicleDetails vehicleDetails = recordData.where(rand_int1).getVehicleDetails();
        if (vehicleDetails != null && vehicleDetails.getTypeOfVehicle() != null && vehicleDetails.getTypeOfVehicle().equalsIgnoreCase("New")) {
            for (int i = 0; i < listPojos.size(); i++) {
                if (!listPojos.get(i).isOnlyForUsedCar()) {
                    checkLists.add(listPojos.get(i));
                }
            }
        } else {
            for (int i = 0; i < listPojos.size(); i++) {
                if (!listPojos.get(i).isOnlyForNewCar()) {
                    checkLists.add(listPojos.get(i));
                }
            }
        }

        if (recordData.where(rand_int1).getSettingsAndPermissions()!=null&&recordData.where(rand_int1).getSettingsAndPermissions().isCoBuyer()) {
            if (recordData.where(rand_int1).getCoBuyerCustomerDetails().getCheckLists() != null && recordData.where(rand_int1).getCoBuyerCustomerDetails().getCheckLists().size() > 0) {
                ArrayList<CheckListUpload> localList = recordData.where(rand_int1).getCoBuyerCustomerDetails().getCheckLists();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < checkLists.size(); j++) {
                        if (localList.get(i).getCheckListId().equalsIgnoreCase(checkLists.get(j).getId())) {
                            checkLists.get(j).setIsActive(true);
                        }
                    }

                }
            }
        } else {
            if (recordData.where(rand_int1).getSettingsAndPermissions()!=null&&recordData.where(rand_int1).getSettingsAndPermissions().getCheckLists() != null && recordData.where(rand_int1).getSettingsAndPermissions().getCheckLists().size() > 0) {
                ArrayList<CheckListUpload> localList = recordData.where(rand_int1).getSettingsAndPermissions().getCheckLists();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < checkLists.size(); j++) {
                        if (localList.get(i).getCheckListId().equalsIgnoreCase(checkLists.get(j).getId())) {
                            checkLists.get(j).setIsActive(true);
                        }
                    }

                }
            }
        }
        isAllChecked = true;
        //bullet point change
//        for (int i = 0; i < checkLists.size(); i++) {
//            if (!checkLists.get(i).getIsActive()) {
//                isAllChecked = false;
//                break;
//            }
//
//        }
        adapter.notifyDataSetChanged();
//        }
    }

    private void initView() {
        if (NewDealViewPager.currentPage==79){
            initPhoto();
        }
        next = mView.findViewById(R.id.text_next);

        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        if (decidePageNumber() == 10)
            text_cancelBack.setText(String.format(getString(R.string.page1), "8", decidePageNumber()));
        else if (decidePageNumber() == 11)
            text_cancelBack.setText(String.format(getString(R.string.page1), "9", decidePageNumber()));
        else if (decidePageNumber() == 12)
            text_cancelBack.setText(String.format(getString(R.string.page1), "10", decidePageNumber()));
        else
            text_cancelBack.setText(String.format(getString(R.string.page1), "11", decidePageNumber()));
        TextView back = mView.findViewById(R.id.text_back_up);
        progress = mView.findViewById(R.id.progress);

        final DSTextView signature = mView.findViewById(R.id.signature);
          time = mView.findViewById(R.id.time);
        time.setText(getCurrentTimeString());

//        next.setVisibility(View.GONE);
        disableWithGray(next);
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        mView.findViewById(R.id.recyclerView).setFocusable(false);
        mView.findViewById(R.id.temp).requestFocus();
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
          sign = mView.findViewById(R.id.signhere);
        final DSTextView title = mView.findViewById(R.id.title);
        final DSTextView title1 = mView.findViewById(R.id.title1);
        title1.setText(Html.fromHtml(getString(R.string.confirmDocs)));
        checkLists = new ArrayList<>();

        if (sign != null) {
            Gson gson = new GsonBuilder().create();
            File file;
            final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                sign.setText(Html.fromHtml(getString(R.string.signature)));
            } else {
                sign.setText(Html.fromHtml(getString(R.string.co_signature)));
            }
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
                DSTextView vin = mView.findViewById(R.id.vin);
                DSTextView vehicle = mView.findViewById(R.id.vehicle);
                String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
                String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
                String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
                String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
                vin.setText(String.format("VIN: %s", vinNumber));
                vehicle.setText(String.format("Vehicle: %s, %s, %s", make, model, year));
            }
        }

        adapter = new CheckListAdapter(getActivity(), checkLists, this);
        recyclerView.setAdapter(adapter);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isAllChecked) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                    return;
                }
//                disableNext(next);
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
//                if (checkMandatoryImage())
                analyseTheImages();
                iGoToNextPage.whatNextClick();
                Gson gson = new GsonBuilder().create();
                File file;
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file = getOutputFile("Screenshot", "co_check_list_screen");
                } else {
                    file = getOutputFile("Screenshot", "check_list_screen");
                }
                scrollableScreenshot(mainContent, file);
            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        svgFile = new File(path_signature + "/IMG_co_buyer_checklist.svg");
                    } else {
                        svgFile = new File(path_signature + "/IMG_buyer_checklist.svg");
                    }
                    File file;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file = getOutputFile("Screenshot", "co_check_list_screen");
                    } else {
                        file = getOutputFile("Screenshot", "check_list_screen");
                    }
                    File file1;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file1 = getOutputFile("UserPic", "co_check_list_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "check_list_screen_pic");
                    }

                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    svgFile = new File(path_signature + "/IMG_co_buyer_checklist.svg");
                } else {
                    svgFile = new File(path_signature + "/IMG_buyer_checklist.svg");
                }
                File file;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file = getOutputFile("Screenshot", "co_check_list_screen");
                } else {
                    file = getOutputFile("Screenshot", "check_list_screen");
                }
                File file1;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file1 = getOutputFile("UserPic", "co_check_list_screen_pic");
                } else {
                    file1 = getOutputFile("UserPic", "check_list_screen_pic");
                }

                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                iGoToNextPage.goToBackIndex();
            }
        });
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
//                next.setVisibility(View.GONE);
                disableWithGray(next);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                if (isAllChecked) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    enableWithGreen(next);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    } else if (isTaken) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }
                isSigned = true;

            }

            @Override
            public void onClear() {
sign.setVisibility(View.VISIBLE);disableDelete(delete);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
//                        next.setVisibility(View.GONE);
                        disableWithGray(next);
                        return true;
                    }
                }
                return true;
            }
        });

    }

    private void getCheckListItems() {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.show();
//        progressDialog.setMessage("Fetching checklist...");
        checkLists.clear();
        progress.setVisibility(View.VISIBLE);
        final String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        String id = recordData.where(rand_int1).getDealershipDetails().getDealershipId() + "";
        String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        checkLists.clear();
        RetrofitInitialization.getDs_services().getCheckList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<CheckListPojo>() {
            @Override
            public void onResponse(Call<CheckListPojo> call, Response<CheckListPojo> response) {
                progress.setVisibility(View.GONE);
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.CHECK_LIST, "");
                    PreferenceManger.putString(AppConstant.CHECK_LIST, list);
                    ArrayList<CheckList> listPojos = new ArrayList<>(response.body().getCheckList());
                    for (int i = 0; i < listPojos.size(); i++) {
                        listPojos.get(i).setIsActive(false);
                    }
                    VehicleDetails vehicleDetails = recordData.where(rand_int1).getVehicleDetails();
                    if (vehicleDetails.getTypeOfVehicle().equalsIgnoreCase("New")) {
                        for (int i = 0; i < listPojos.size(); i++) {
                            if (!listPojos.get(i).isOnlyForUsedCar()) {
                                checkLists.add(listPojos.get(i));
                            }
                        }
                    } else {
                        for (int i = 0; i < listPojos.size(); i++) {
                            if (!listPojos.get(i).isOnlyForNewCar()) {
                                checkLists.add(listPojos.get(i));
                            }
                        }
                    }

                    if (recordData.where(rand_int1).getSettingsAndPermissions().isCoBuyer()) {
                        if (recordData.where(rand_int1).getCoBuyerCustomerDetails().getCheckLists() != null && recordData.where(rand_int1).getCoBuyerCustomerDetails().getCheckLists().size() > 0) {
                            ArrayList<CheckListUpload> localList = recordData.where(rand_int1).getCoBuyerCustomerDetails().getCheckLists();
                            for (int i = 0; i < localList.size(); i++) {
                                for (int j = 0; j < checkLists.size(); j++) {
                                    if (localList.get(i).getCheckListId().equalsIgnoreCase(checkLists.get(j).getId())) {
                                        checkLists.get(j).setIsActive(true);
                                    }
                                }

                            }
                        }
                    } else {
                        if (recordData.where(rand_int1).getSettingsAndPermissions().getCheckLists() != null && recordData.where(rand_int1).getSettingsAndPermissions().getCheckLists().size() > 0) {
                            ArrayList<CheckListUpload> localList = recordData.where(rand_int1).getSettingsAndPermissions().getCheckLists();
                            for (int i = 0; i < localList.size(); i++) {
                                for (int j = 0; j < checkLists.size(); j++) {
                                    if (localList.get(i).getCheckListId().equalsIgnoreCase(checkLists.get(j).getId())) {
                                        checkLists.get(j).setIsActive(true);
                                    }
                                }

                            }
                        }
                    }
                    isAllChecked = true;
//                    changed for bullet point
//                    for (int i = 0; i < checkLists.size(); i++) {
//                        if (!checkLists.get(i).getIsActive()) {
//                            isAllChecked = false;
//                            break;
//                        }
//
//                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<CheckListPojo> call, Throwable t) {

            }
        });
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {


            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            File svgFile;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_checklist");
            } else {
                svgFile = getOutputMediaFile("Signatures", "buyer_checklist");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
//            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setPrintCopyRecieved(true);

            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                ArrayList<CheckListUpload> checkListUploads = new ArrayList<>();
                if (checkLists != null) {
                    for (int i = 0; i < checkLists.size(); i++) {
                        CheckListUpload verbalPromiseUpload = new CheckListUpload();
                        verbalPromiseUpload.setCheckListId(checkLists.get(i).getId());
                        verbalPromiseUpload.setValue(true);
                        checkListUploads.add(verbalPromiseUpload);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setCheckLists(checkListUploads);
            } else {
                ArrayList<CheckListUpload> checkListUploads = new ArrayList<>();
                if (checkLists != null) {
                    for (int i = 0; i < checkLists.size(); i++) {
                        CheckListUpload verbalPromiseUpload = new CheckListUpload();
                        verbalPromiseUpload.setCheckListId(checkLists.get(i).getId());
                        verbalPromiseUpload.setValue(true);
                        checkListUploads.add(verbalPromiseUpload);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCheckLists(checkListUploads);
            }
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }

    }


    @Override
    public void allChecked(boolean allChecked) {
        isAllChecked = allChecked;
        if (allChecked && isSigned) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    enableWithGreen(next);
                    next.setVisibility(View.VISIBLE);
                }
            }, 100);
        } else {
            disableWithGray(next);
//            next.setVisibility(View.GONE);
        }
    }
}
