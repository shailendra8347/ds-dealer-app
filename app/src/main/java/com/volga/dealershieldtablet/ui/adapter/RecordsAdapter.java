package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleConditionDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.VehicleHistoryDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SyncUp;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.IncompleteDealDetailResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitiateThirdParty;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehicleTradeAdditionalDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.yariksoffice.lingver.Lingver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.TypeRow> {

    private Activity activity;
    private Boolean hideDeleteButton;
    private ArrayList<Record> recordData;
    private CallbackOnDelete onDelete;
//    android.support.v4.app.FragmentTransaction beginTransaction;


    public interface CallbackOnDelete {
        void onDelete(int position, boolean isSentEmail, boolean refresh, boolean reportDev);
    }

    public RecordsAdapter(FragmentActivity activity, boolean hideDeleteButton, ArrayList<Record> recordData, CallbackOnDelete onDelete) {
        this.activity = activity;
        this.hideDeleteButton = hideDeleteButton;
        this.recordData = recordData;
        this.onDelete = onDelete;
//        this.beginTransaction = beginTransaction;

    }

    @Override
    public RecordsAdapter.TypeRow onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.layout_unsync_record_row, parent, false);
        return new RecordsAdapter.TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(final RecordsAdapter.TypeRow holder, @SuppressLint("RecyclerView") final int position) {
//        holder.name.setText(placesArrayList.get(position).getName());
        final Record rowData = recordData.get(position);
        if (rowData.getSettingsAndPermissions().isError()) {
            holder.error.setVisibility(View.VISIBLE);
        } else {
            holder.error.setVisibility(View.GONE);
        }

        holder.sentEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete.onDelete(position, true, false, false);
            }
        });

        holder.error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onDelete.onDelete(position, false, false, true);

//                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
//                emailIntent.setType("text/plain");
//                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"shailendra.m@volgainfotech.com", "dealerxt@gmail.com"});
//                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Sync failed report");
//                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, rowData.getSettingsAndPermissions().getErrorDesc());
//                emailIntent.setType("message/rfc822");
//
//                try {
//                    activity.startActivity(Intent.createChooser(emailIntent,
//                            "Send email using..."));
//                } catch (android.content.ActivityNotFoundException ex) {
//                    new CustomToast(activity).alert("No email clients installed.");
//                }

            }
        });
        if (rowData.getCustomerDetails() != null) {
            holder.name.setText(MessageFormat.format("{0} {1}", rowData.getCustomerDetails().getFirstName() == null ? "NA" : rowData.getCustomerDetails().getFirstName(), rowData.getCustomerDetails().getLastName() == null ? "NA" : rowData.getCustomerDetails().getLastName()));
        }
        if (rowData.getDealStatusText() != null && rowData.isWebDeal() && rowData.getDealStatusId() != 4) {
            holder.statusWeb.setText(rowData.getDealStatusText());
            holder.statusWeb.setVisibility(View.VISIBLE);
        } else {
            holder.statusWeb.setVisibility(View.GONE);
        }
        if (rowData.getDealershipDetails() != null) {
            holder.dealershipName.setText(rowData.getDealershipDetails().getDealershipName());
        }
        if (rowData.getVehicleDetails() != null) {
            String Vin = rowData.getVehicleDetails().getVINNumber() == null ? "NA" : rowData.getVehicleDetails().getVINNumber();
            String make = rowData.getVehicleDetails().getCarMake() == null ? "NA" : rowData.getVehicleDetails().getCarMake();
            String model = rowData.getVehicleDetails().getCarModel() == null ? "NA" : rowData.getVehicleDetails().getCarModel();
            String year = rowData.getVehicleDetails().getCarYear() == null ? "NA" : rowData.getVehicleDetails().getCarYear();
            holder.vehicalDetail.setText(MessageFormat.format("{0}  |  {1}  |  {2}  |  {3}", Vin, make, model, year));
            if (rowData.getCustomerDetails().getSalesPersons() != null) {
                String fname = rowData.getCustomerDetails().getSalesPersons().getFirstName() == null ? "NA" : rowData.getCustomerDetails().getSalesPersons().getFirstName();
                String lname = rowData.getCustomerDetails().getSalesPersons().getLastName() == null ? "NA" : rowData.getCustomerDetails().getSalesPersons().getLastName();
                String mail = rowData.getCustomerDetails().getSalesPersons().getEmail() == null ? "NA" : rowData.getCustomerDetails().getSalesPersons().getEmail();
                holder.salespeson.setText(MessageFormat.format("{0}  |  {1}  |  {2}", fname, lname, mail));
            }
        }
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        SimpleDateFormat newFormatter = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        try {
            holder.date.setText(newFormatter.format(formatter.parse(rowData.getSettingsAndPermissions().getLatestTimeStamp())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (hideDeleteButton) {
            holder.dealershipName.setVisibility(View.VISIBLE);
            holder.dsNameHolder.setVisibility(View.VISIBLE);
            holder.deleteButton.setVisibility(View.GONE);
            holder.frontContainer.setVisibility(View.GONE);
        } else {
            holder.dealershipName.setVisibility(View.GONE);
            holder.dsNameHolder.setVisibility(View.GONE);
            if (recordData.get(position).getSettingsAndPermissions().getDealStatus() != null) {
                if (recordData.get(position).getSettingsAndPermissions().getDealStatus().equalsIgnoreCase("Syncing")) {
                    holder.inProgress.setVisibility(View.VISIBLE);
                } else {
                    holder.inProgress.setVisibility(View.GONE);
                }
                if (recordData.get(position).getSettingsAndPermissions().getDealStatus().equalsIgnoreCase("Synced")) {
                    holder.synced.setVisibility(View.VISIBLE);
                    if (getPageIndex(rowData.getSettingsAndPermissions().getScreenName()) >= 31)
                        holder.sentEmail.setVisibility(View.VISIBLE);
                } else {
                    holder.synced.setVisibility(View.GONE);
                    holder.sentEmail.setVisibility(View.GONE);
                }
                if (recordData.get(position).getSettingsAndPermissions().getDealStatus().equalsIgnoreCase("Locked")) {
                    holder.lock.setVisibility(View.VISIBLE);
                } else {
                    holder.lock.setVisibility(View.GONE);
                }
                if (recordData.get(position).isWebDeal()) {
                    if (recordData.get(position).getDealStatusId() != 4) {
                        holder.wait.setVisibility(View.VISIBLE);
                    } else {
                        holder.lock.setVisibility(View.GONE);
                    }
                }
            }
            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.row.setEnabled(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.row.setEnabled(true);
                        }
                    }, 3000);
                    /*do api call first then go next if there is tab inter connectivity*/
                    if (rowData.getDealershipDetails().getDealershipId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                        if (rowData.getSettingsAndPermissions().getCustomerVehicleTradeId() == null || rowData.getSettingsAndPermissions().getCustomerVehicleTradeId().length() == 0) {
                            navigateToSpecificScreen(rowData, true);
                        } else {
                            if (DSAPP.getInstance().isNetworkAvailable()) {
//                                if (rowData.isWebDeal() && rowData.getDealStatusId() == 4) {
//                                    // pass to customer info pages
//                                } else
                                if (rowData.isWebDeal() && rowData.getDealStatusId() != 4) {
                                    new CustomToast(activity).alert(rowData.getDealStatusText());
                                    return;
                                }
                                getDetailedJsonForTabInterConnectivity(rowData);
                            } else {
                                new CustomToast(activity).alert("Please check your internet connection.");
                            }

                        }
                    } else {
                        new CustomToast(activity).alert("You have created this deal from \"" + rowData.getDealershipDetails().getDealershipName() + "\" and current Dealership is \"" + PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME) + "\". Please change dealership to continue.");
                    }
                }
            });
            String path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + rowData.getCustomerDetails().getDLNumber() + "/" + "CarImages" + "/" + "IMG_front.jpg";
            File imgFile = new File(path);
            if (imgFile.exists()) {
                holder.frontContainer.setVisibility(View.VISIBLE);
                loadImageToImageView(ShowImageFromPath("IMG_front.jpg", "CarImages", rowData.getCustomerDetails().getDLNumber()), holder.front);
//                holder.front.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        showDialog(ShowImageFromPath("IMG_front.jpg", "CarImages", rowData.getCustomerDetails().getDLNumber()).toString());
//                    }
//                });
            } else if (rowData.getSettingsAndPermissions().getFrontUrl() != null && rowData.getSettingsAndPermissions().getFrontUrl().length() > 0) {
                loadImageToImageView(rowData.getSettingsAndPermissions().getFrontUrl(), holder.front);
//                holder.front.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        showDialog(rowData.getSettingsAndPermissions().getFrontUrl(), "front");
//                    }
//                });
                holder.frontContainer.setVisibility(View.VISIBLE);
            } else {
                holder.frontContainer.setVisibility(View.GONE);
            }
            if ((rowData.getSettingsAndPermissions().getCustomerVehicleTradeId() == null || rowData.getSettingsAndPermissions().getCustomerVehicleTradeId().trim().length() == 0) && !rowData.getSettingsAndPermissions().isIncompleteDeal()) {
                holder.deleteButton.setVisibility(View.VISIBLE);
            } else {
                holder.deleteButton.setVisibility(View.GONE);
            }
//            if (rowData.getSettingsAndPermissions().isFromServer()) {
//                holder.deleteButton.setVisibility(View.GONE);
//            } else {
//                holder.deleteButton.setVisibility(View.VISIBLE);
//            }
            holder.deleteButton.setEnabled(true);
            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.deleteButton.setEnabled(false);
                    onDelete.onDelete(position, false, false, false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.deleteButton.setEnabled(true);
                        }
                    }, 3000);
                }
            });


        }

    }

//    private void showDialog(String uri) {
//        DialogFragment newFragment = new ImageFragmentDialog();
//        Bundle bundle = new Bundle();
//        bundle.putString("name", uri);
//        bundle.putString("url", uri);
//        newFragment.setArguments(bundle);
//
//        newFragment.show(beginTransaction, "dialog");
//    }

//    private void showDialog(String uri, String name) {
//        DialogFragment newFragment = new ImageFragmentDialog();
//        Bundle bundle = new Bundle();
//        bundle.putString("name", name);
//        bundle.putString("url", uri);
//        newFragment.setArguments(bundle);
//        newFragment.show(beginTransaction, "dialog");
//    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(activity)
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(activity)
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
    }

    public Uri ShowImageFromPath(String fileName, String mainFolder, String dmv) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + dmv + "/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

        if (imgFile.exists()) {
            return Uri.fromFile(imgFile);
//            return rotateImage(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 90);
        }
        return null;
    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
//                    Log.e("Refresh: ",gson.toJson(loginData));
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
                    Intent intent = new Intent();
                    intent.putExtra("value", "refreshIncompleteDeal");
                    intent.setAction("notifyData");
                    activity.sendBroadcast(intent);

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    new CustomToast(activity).alert(activity.getString(R.string.connection_check));
                }
            }
        });
    }

    private void getDetailedJsonForTabInterConnectivity(final Record rowData) {
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Fetching deal...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.e("Tab id: deal data ", PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "");
        RetrofitInitialization.getDs_services().getIncompleteDealData("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), rowData.getSettingsAndPermissions().getCustomerVehicleTradeId(), PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "").enqueue(new Callback<IncompleteDealDetailResponse>() {
            @Override
            public void onResponse(@NonNull Call<IncompleteDealDetailResponse> call, @NonNull Response<IncompleteDealDetailResponse> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    assert response.body() != null;
                    Log.e("IncompleteDealPojo: ", "Here is data: " + response.body().toString());
                    convertDataInToLocalJson(response.body(), rowData);
                    progressDialog.dismiss();
                } else {
                    progressDialog.dismiss();
                    if (response.code() == 401 || response.code() == 403) {
                        if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                            callRefreshTokenAPI();
                        }
//                        else {
//                            PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
//                            PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
//                            Locale myLocale = new Locale("en");
//                            Resources res = getResources();
//                            DisplayMetrics dm = res.getDisplayMetrics();
//                            Configuration conf = res.getConfiguration();
//                            conf.locale = myLocale;
//                            res.updateConfiguration(conf, dm);
//                            callHomeActivity();
//                        }
                    } else {
                        assert response.errorBody() != null;
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response.errorBody().string());
                            new CustomToast(activity).alert(json.getString("Message"));
                            Intent intent = new Intent();
                            intent.putExtra("value", "refreshIncompleteDeal");
                            intent.setAction("notifyData");
                            activity.sendBroadcast(intent);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                        Timber.tag("Response ").e("onResponse: " + json);
                    }
                }
            }

            @Override
            public void onFailure(Call<IncompleteDealDetailResponse> call, Throwable t) {

            }
        });
    }

    private void convertDataInToLocalJson(IncompleteDealDetailResponse body, Record rowData) {
        PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
        Log.e("Dl number rd adapter: ", rowData.getCustomerDetails().getDLNumber());
        RecordData recordData;
        Gson gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(rowData.getCustomerDetails().getDLNumber());
        record.setBuyerSigned(rowData.isBuyerSigned());
        record.setCoBuyerSigned(rowData.isCoBuyerSigned());
        record.setCoBuyer(rowData.isCoBuyer());

        CoBuyerCustomerDetails coBuyerCustomerDetails = record.getCoBuyerCustomerDetails() == null ? new CoBuyerCustomerDetails() : record.getCoBuyerCustomerDetails();
        CustomerDetails customerDetails = record.getCustomerDetails() == null ? new CustomerDetails() : record.getCustomerDetails();
        VehicleDetails vehicleDetails = record.getVehicleDetails() == null ? new VehicleDetails() : record.getVehicleDetails();
        SalesPersons salesPersons = customerDetails.getSalesPersons() == null ? new SalesPersons() : customerDetails.getSalesPersons();
        TradeInVehicle tradeInVehicle = record.getTradeInVehicle() == null ? new TradeInVehicle() : record.getTradeInVehicle();
        SyncUp syncUp = new SyncUp();
        DealershipDetails dealershipDetails = record.getDealershipDetails() == null ? new DealershipDetails() : record.getDealershipDetails();
        SettingsAndPermissions settingsAndPermissions = record.getSettingsAndPermissions() == null ? new SettingsAndPermissions() : record.getSettingsAndPermissions();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        SimpleDateFormat originalFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        SimpleDateFormat originalFormatter1 = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
        /*Put all parsed data into local record*/

        dealershipDetails.setDealershipId(Integer.parseInt(body.getCustomerDetail().getDealershipId()));
//        dealershipDetails.setDealershipName(body.getCustomerDetail().getde());
        /*putting all customer related data here*/
        customerDetails.setDLNumber(rowData.getCustomerDetails().getDLNumber());
        customerDetails.setFirstName(body.getCustomerDetail().getFirstName() == null ? "" : body.getCustomerDetail().getFirstName());
        customerDetails.setLastName(body.getCustomerDetail().getLastName() == null ? "" : body.getCustomerDetail().getLastName());
        customerDetails.setGender(body.getCustomerDetail().getGenderTitle());
        customerDetails.setId(body.getId());
        customerDetails.setIsRemote(body.getCustomerDetail().getIsRemote());
        customerDetails.setCustomerId(body.getCustomerId());
        customerDetails.setEmail(body.getCustomerDetail().getEmail());
        customerDetails.setMobileNumber(body.getCustomerDetail().getContactNumber());
        customerDetails.setTermConditionAccepted(body.getCustomerDetail().getIsTermConditionAccepted());
        vehicleDetails.setBuyerFistName(body.getCustomerDetail().getFirstName() == null ? "" : body.getCustomerDetail().getFirstName());
        vehicleDetails.setBuyerLastName(body.getCustomerDetail().getLastName() == null ? "" : body.getCustomerDetail().getLastName());
//        customerDetails.setAddressLineOne(body.getCustomerDetail().get());
        customerDetails.setDrivingLicenceNumber(body.getCustomerDetail().getDrivingLicenseNumber());
        customerDetails.setMiddleName(body.getCustomerDetail().getMiddleName());
        if (body.getCustomerDetail().getLocation() != null) {
            customerDetails.setCity(body.getCustomerDetail().getLocation().getCity());
            customerDetails.setZipCode(body.getCustomerDetail().getLocation().getZipcode());
            customerDetails.setState(body.getCustomerDetail().getLocation().getState());
            customerDetails.setAddressLineOne(body.getCustomerDetail().getLocation().getAddress1());
            customerDetails.setAddressLineTwo(body.getCustomerDetail().getLocation().getAddress2());
            customerDetails.setCountry(body.getCustomerDetail().getLocation().getCountry());
        }
        try {
            if (body.getCustomerDetail().getDateOfBirth() != null && body.getCustomerDetail().getDateOfBirth().length() > 0) {
                customerDetails.setDateOfBirth(originalFormatter1.format(sdf.parse(body.getCustomerDetail().getDateOfBirth())));
            }
            if (body.getCustomerDetail().getDrivingLicenseExpiryDate() != null && body.getCustomerDetail().getDrivingLicenseExpiryDate().length() > 0) {
                customerDetails.setDLExpiryDate(originalFormatter1.format(sdf.parse(body.getCustomerDetail().getDrivingLicenseExpiryDate())));

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /*put address detail here*/

        /*sales person data is here*/
        if (body.getCustomerDetail() != null && body.getCustomerDetail().getSalesPersons() != null && body.getCustomerDetail().getSalesPersons().length > 0) {
            salesPersons.setFirstName(body.getCustomerDetail().getSalesPersons()[0].getFirstName() == null ? "" : body.getCustomerDetail().getSalesPersons()[0].getFirstName());
            salesPersons.setEmail(body.getCustomerDetail().getSalesPersons()[0].getEmail() == null ? "" : body.getCustomerDetail().getSalesPersons()[0].getEmail());
            salesPersons.setLastName(body.getCustomerDetail().getSalesPersons()[0].getLastName() == null ? "" : body.getCustomerDetail().getSalesPersons()[0].getLastName());
            salesPersons.setUserId(body.getCustomerDetail().getSalesPersons()[0].getUserId() == null ? "" : body.getCustomerDetail().getSalesPersons()[0].getUserId());
            salesPersons.setMiddleName(body.getCustomerDetail().getSalesPersons()[0].getMiddleName() == null ? "" : body.getCustomerDetail().getSalesPersons()[0].getMiddleName());
            customerDetails.setSalesPersons(salesPersons);
           vehicleDetails.setSalesId(body.getCustomerDetail().getSalesPersons()[0].getUserId() == null ? "" : body.getCustomerDetail().getSalesPersons()[0].getUserId());
        }
        settingsAndPermissions.setTestDriveTaken(body.getIsTestDriveTaken());
        settingsAndPermissions.setDisclosureSkipped(body.isDisclosureSkipped());

        settingsAndPermissions.setLastPageIndex(getPageIndex(body.getCustomerDetail().getLastScreenName()));

        if (body.getCustomerDetail().getLanguageId() != null && body.getCustomerDetail().getLanguageId().length() > 0) {
            settingsAndPermissions.setSelectedLanguageId(Integer.parseInt(body.getCustomerDetail().getLanguageId()));
        }
        settingsAndPermissions.setSelectedLanguage(body.getCustomerDetail().getLanguageTitle());
        settingsAndPermissions.setCustomerVehicleTradeId(rowData.getSettingsAndPermissions().getCustomerVehicleTradeId() + "");
        settingsAndPermissions.setCustomerVehicleTradeDocs(body.getCustomerVehicleTradeDocs());
        settingsAndPermissions.setCustomerVehicleTradeDocsAll(body.getCustomerVehicleTradeDocsAll());

//
        settingsAndPermissions.setVehicleTradeConditionDisclosures(body.getVehicleTradeConditionDisclosures());
        settingsAndPermissions.setVehicleTradeHistoryDisclosures(body.getVehicleTradeHistoryDisclosures());
        settingsAndPermissions.setVehicleTradeThirdPartyHistoryReports(body.getVehicleTradeThirdPartyHistoryReports());
        if (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 82) {
            settingsAndPermissions.setDataSyncedWithServer(true);
        }

        Date date = null, reportDate = null;
        try {
            date = sdf.parse(body.getTradeDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        settingsAndPermissions.setLatestTimeStamp(date.toString());
        settingsAndPermissions.setIncompleteDeal(false);
        settingsAndPermissions.setFromServer(false);
        settingsAndPermissions.setEditing(true);
        settingsAndPermissions.setAcceptedUsedCarPolicy(body.getCustomerDetail().getIsAcceptedUsedCarPolicy());
        settingsAndPermissions.setDealStatus(rowData.getSettingsAndPermissions().getDealStatus());
        settingsAndPermissions.setTestDriveTimeId(Integer.parseInt(body.getTestDriveDurationId()));
        if (body.getCoBuyers() != null && body.getCoBuyers().size() > 0) {
            settingsAndPermissions.setCoBuyerSelectedLanguage(body.getCoBuyers().get(0).getLanguageTitle());
            if (body.getCoBuyers().get(0).getLanguageId() != null && body.getCoBuyers().get(0).getLanguageId().length() > 0)
                settingsAndPermissions.setCoBuyerSelectedLanguageId(Integer.parseInt(body.getCoBuyers().get(0).getLanguageId()));
            settingsAndPermissions.setCoBuyerTestDriveTaken(Boolean.parseBoolean(body.getCoBuyers().get(0).getIsTestDriveTaken()));
            settingsAndPermissions.setCoBuyerTestDriveTimeId(Integer.parseInt(body.getCoBuyers().get(0).getTestDriveDurationId()));
        }
        vehicleDetails.setVINNumber(body.getVIN() == null ? "" : body.getVIN());
        vehicleDetails.setCarMake(body.getMake() == null ? "" : body.getMake());
        vehicleDetails.setCarModel(body.getModel() == null ? "" : body.getModel());
        vehicleDetails.setCarYear(body.getModelYear() == null ? "" : body.getModelYear());
        vehicleDetails.setWarningAlerts(body.getConditionDisclosureWarningAlert());

        if (body.getThirdPartyReportRawDTOList() != null && body.getThirdPartyReportRawDTOList().size() > 0) {
            InitiateThirdParty initiateThirdParty = new InitiateThirdParty();
            initiateThirdParty.setValue(body.getThirdPartyReportRawDTOList());
            vehicleDetails.setInitiateThirdParty(initiateThirdParty);
            for (int i = 0; i < body.getThirdPartyReportRawDTOList().size(); i++) {
                if (body.getThirdPartyReportRawDTOList().get(i).getThirdPartyID().equalsIgnoreCase("1") && !body.getThirdPartyReportRawDTOList().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                    vehicleDetails.setCarFaxReportAvailable(true);
                }
                if (body.getThirdPartyReportRawDTOList().get(i).getThirdPartyID().equalsIgnoreCase("2") && !body.getThirdPartyReportRawDTOList().get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                    vehicleDetails.setAutoCheckReportAvailable(true);
                }
            }
        }

        vehicleDetails.setTypeOfPurchase(body.getTradeTypeId());
        vehicleDetails.setTypeOfVehicle(body.getCarTypeTitle());

        VehicleConditionDisclosure vehicleConditionDisclosure = new VehicleConditionDisclosure();
        if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
            ArrayList<Value> list = body.getConditionDisclosure();
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsVerified(false);
            }
            vehicleConditionDisclosure.setValue(list);
        } else {
            vehicleConditionDisclosure.setValue(body.getConditionDisclosure());
        }

        vehicleDetails.setVehicleConditionDisclosure(vehicleConditionDisclosure);
        /* Code for additional disclosure*/
        ArrayList<VehicleTradeAdditionalDisclosures> additional = body.getVehicleTradeAdditionalDisclosures();
        if (additional != null && additional.size() > 0) {
            AdditionalDisclosure condition = new AdditionalDisclosure();
            AdditionalDisclosure history = new AdditionalDisclosure();
            ArrayList<Value> addHistory = new ArrayList<>();
            ArrayList<Value> addCondition = new ArrayList<>();
            for (int i = 0; i < additional.size(); i++) {
                if (additional.get(i).getDisclosureType().equalsIgnoreCase("CONDITION")) {
                    Value value = new Value();
                    value.setId(additional.get(i).getAdditionalDisclosureId());
                    value.setTitle(additional.get(i).getAdditionalTitle());
                    value.setManual(true);
                    value.setTranslations(additional.get(i).getTranslations());
                    addCondition.add(value);
                } else if (additional.get(i).getDisclosureType().equalsIgnoreCase("HISTORY")) {
                    Value value = new Value();
                    value.setId(additional.get(i).getAdditionalDisclosureId());
                    value.setTitle(additional.get(i).getAdditionalTitle());
                    value.setManual(true);
                    value.setTranslations(additional.get(i).getTranslations());
                    addHistory.add(value);
                }

            }
            condition.setValue(addCondition);
            history.setValue(addHistory);
            vehicleDetails.setAdditionalDisclosure(condition);
            vehicleDetails.setAdditionalHistoryDisclosure(history);
        }

        VehicleHistoryDisclosure vehicleHistoryDisclosure = new VehicleHistoryDisclosure();
        if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
            ArrayList<Value> list = body.getHistoryDisclosure();
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIsVerified(false);
            }
            vehicleHistoryDisclosure.setValue(list);
        } else {
            vehicleHistoryDisclosure.setValue(body.getHistoryDisclosure());
        }
//        vehicleHistoryDisclosure.setValue(body.getHistoryDisclosure());
        vehicleDetails.setVehicleHistoryDisclosure(vehicleHistoryDisclosure);

        ThirdPartyList thirdPartyList = new ThirdPartyList();
        if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
            ArrayList<ThirdPartyObj> list = body.getThirdParty();
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setViewReport(false);
                list.get(i).setActive(false);
                list.get(i).setMandatory(false);
            }
            thirdPartyList.setValue(list);
        } else {
            thirdPartyList.setValue(body.getThirdParty());
        }
//        thirdPartyList.setValue(body.getThirdParty());
        vehicleDetails.setThirdPartyList(thirdPartyList);

        /*Dislosures........*/

        if (vehicleConditionDisclosure.getValue() != null && vehicleConditionDisclosure.getValue().size() > 0) {
            AppConstant.commonList.clear();

            for (int i = 0; i < vehicleConditionDisclosure.getValue().size(); i++) {
                HistoryListPojo historyListPojo = new HistoryListPojo();
                historyListPojo.setId(vehicleConditionDisclosure.getValue().get(i).getId());
                historyListPojo.setName(vehicleConditionDisclosure.getValue().get(i).getTitle());
                if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API))
                    historyListPojo.setVerified(false);
                else
                    historyListPojo.setVerified(vehicleConditionDisclosure.getValue().get(i).getIsVerified());
                AppConstant.commonList.add(historyListPojo);
            }
        }

//        String[] condition = activity.getResources().getStringArray(R.array.CommonHistoryList);
//        final ArrayList<HistoryListPojo> historyListPojos = new ArrayList<>();
//        for (String aList : condition) {
//            HistoryListPojo pojo = new HistoryListPojo();
//            pojo.setName(aList);
//            pojo.setId(getIds(aList));
//            pojo.setChecked(false);
//            historyListPojos.add(pojo);
//
//        }
//        AppConstant.commonList.clear();
//        AppConstant.commonList.addAll(historyListPojos);
//        if(body.getCarTypeTitle().equalsIgnoreCase("New")) {
//            AppConstant.commonList.remove(0);
//            AppConstant.commonList.remove(1);
//            AppConstant.commonList.remove(1);
//            AppConstant.commonList.remove(1);
//            AppConstant.commonList.remove(2);
//            AppConstant.commonList.remove(2);
//            AppConstant.commonList.remove(4);
//            AppConstant.commonList.remove(4);
//            AppConstant.commonList.remove(4);
//            ArrayList<String> local = new ArrayList<>(Arrays.asList(condition));
//            local.remove(0);
//            local.remove(1);
//            local.remove(1);
//            local.remove(1);
//            local.remove(2);
//            local.remove(2);
//            local.remove(4);
//            local.remove(4);
//            local.remove(4);
//            condition = new String[local.size()];
//            condition = local.toArray(condition);
////            AppConstant.commonList.addAll(historyListPojos);
//        }
        vehicleDetails.setUsedVehicleCondition(AppConstant.commonList);
        if (body.getVehicleTradeConditionDisclosures() != null && body.getVehicleTradeConditionDisclosures().size() > 0) {
            ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
            for (int i = 0; i < body.getVehicleTradeConditionDisclosures().size(); i++) {
                for (int j = 0; j < AppConstant.commonList.size(); j++) {
                    if (body.getVehicleTradeConditionDisclosures().get(i).getVehicleConditionDisclosureId().equalsIgnoreCase(AppConstant.commonList.get(j).getId())) {
                        HistoryListPojo historyListPojo = AppConstant.commonList.get(j);
                        historyListPojo.setChecked(true);
                        historyListPojo.setAdditionalData(body.getVehicleTradeConditionDisclosures().get(i).getValue());
                        historyListPojos1.add(historyListPojo);
                    }
                }
            }
            vehicleDetails.setConditionSelection(historyListPojos1);
            vehicleDetails.setCoConditionSelection(historyListPojos1);
        } else {
            if (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 25 && !body.isDisclosureSkipped()) {
//                if (!body.isDisclosureSkipped()) {
//                ArrayList<HistoryListPojo> list = vehicleDetails.getUsedVehicleCondition();
//                list.get(list.size() - 1).setChecked(true);
//                vehicleDetails.setUsedVehicleCondition(list);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                HistoryListPojo historyListPojo = AppConstant.commonList.get(AppConstant.commonList.size() - 1);
                historyListPojo.setChecked(true);
                historyListPojos1.add(historyListPojo);
                vehicleDetails.setConditionSelection(historyListPojos1);
                vehicleDetails.setCoConditionSelection(historyListPojos1);
//                }
            }
            if (rowData.isWebDeal()) {
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                HistoryListPojo historyListPojo = AppConstant.commonList.get(AppConstant.commonList.size() - 1);
                historyListPojo.setChecked(true);
                historyListPojos1.add(historyListPojo);
                vehicleDetails.setConditionSelection(historyListPojos1);
                vehicleDetails.setCoConditionSelection(historyListPojos1);
            }
        }


        /*Vehicle History disclosures*/
        if (vehicleHistoryDisclosure.getValue() != null && vehicleHistoryDisclosure.getValue().size() > 0) {
            AppConstant.historyLists.clear();

            for (int i = 0; i < vehicleHistoryDisclosure.getValue().size(); i++) {
                HistoryListPojo historyListPojo = new HistoryListPojo();
                historyListPojo.setId(vehicleHistoryDisclosure.getValue().get(i).getId());
                historyListPojo.setName(vehicleHistoryDisclosure.getValue().get(i).getTitle());
                if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API))
                    historyListPojo.setVerified(false);
                else
                    historyListPojo.setVerified(vehicleHistoryDisclosure.getValue().get(i).getIsVerified());
                AppConstant.historyLists.add(historyListPojo);
            }
        }
    /*    final ArrayList<HistoryListPojo> historyListPojos2 = new ArrayList<>();
        String[] history = activity.getResources().getStringArray(R.array.historyList);
        int id = 0;
        for (String aList : history) {
            HistoryListPojo pojo = new HistoryListPojo();
            pojo.setName(aList);
            id++;
            pojo.setId(id + "");
            pojo.setChecked(false);
            historyListPojos2.add(pojo);

        }
        AppConstant.historyLists.clear();
        AppConstant.historyLists.addAll(historyListPojos2);*/

        vehicleDetails.setUsedVehicleHistory(AppConstant.historyLists);

        if (body.getVehicleTradeHistoryDisclosures() != null && body.getVehicleTradeHistoryDisclosures().size() > 0) {
            ArrayList<HistoryListPojo> historyListPojos3 = new ArrayList<>();
            for (int i = 0; i < body.getVehicleTradeHistoryDisclosures().size(); i++) {
                for (int j = 0; j < AppConstant.historyLists.size(); j++) {
                    if (body.getVehicleTradeHistoryDisclosures().get(i).getVehicleHistoryDisclosureId().equalsIgnoreCase(AppConstant.historyLists.get(j).getId())) {
                        HistoryListPojo historyListPojo = AppConstant.historyLists.get(j);
                        historyListPojo.setChecked(true);
                        historyListPojos3.add(historyListPojo);
                    }
                }
            }
            vehicleDetails.setHistorySelection(historyListPojos3);
            vehicleDetails.setCoHistorySelection(historyListPojos3);
        } else {
            if (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 26 && !body.isDisclosureSkipped()) {
//                ArrayList<HistoryListPojo> list = vehicleDetails.getUsedVehicleHistory();
//                list.get(list.size() - 1).setChecked(true);
//                vehicleDetails.setUsedVehicleHistory(list);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                HistoryListPojo historyListPojo = AppConstant.historyLists.get(AppConstant.historyLists.size() - 1);
                historyListPojo.setChecked(true);
                historyListPojos1.add(historyListPojo);
                vehicleDetails.setHistorySelection(historyListPojos1);
                vehicleDetails.setCoHistorySelection(historyListPojos1);
            }
            if (rowData.isWebDeal()) {
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                HistoryListPojo historyListPojo = AppConstant.historyLists.get(AppConstant.historyLists.size() - 1);
                historyListPojo.setChecked(true);
                historyListPojos1.add(historyListPojo);
                vehicleDetails.setHistorySelection(historyListPojos1);
                vehicleDetails.setCoHistorySelection(historyListPojos1);
            }
        }
        /*Third party reports*/
        if (thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
            AppConstant.reportLists.clear();

            for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                HistoryListPojo historyListPojo = new HistoryListPojo();
                historyListPojo.setId(thirdPartyList.getValue().get(i).getId());
                historyListPojo.setName(thirdPartyList.getValue().get(i).getTitle());
                if (!PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                    historyListPojo.setVerified(false);
                    historyListPojo.setMandatory(false);
                } else {
                    historyListPojo.setViewReport(thirdPartyList.getValue().get(i).isViewReport());
                    historyListPojo.setVerified(thirdPartyList.getValue().get(i).isActive());
                    historyListPojo.setMandatory(thirdPartyList.getValue().get(i).isMandatory());
                }

                AppConstant.reportLists.add(historyListPojo);
            }
        }
       /* final ArrayList<HistoryListPojo> historyListPojos4 = new ArrayList<>();
        String[] report = activity.getResources().getStringArray(R.array.autoCheckList);
        for (String aList : report) {
            HistoryListPojo pojo = new HistoryListPojo();
//                    id++;
            pojo.setId(getReportIds(aList));
            pojo.setName(aList);
            pojo.setChecked(false);
            historyListPojos4.add(pojo);
        }
        AppConstant.reportLists.clear();
        AppConstant.reportLists.addAll(historyListPojos4);*/


        vehicleDetails.setUsedVehicleReport(AppConstant.reportLists);

        if (body.getVehicleTradeThirdPartyHistoryReports() != null && body.getVehicleTradeThirdPartyHistoryReports().size() > 0) {
            ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
            for (int i = 0; i < body.getVehicleTradeThirdPartyHistoryReports().size(); i++) {
                for (int j = 0; j < AppConstant.reportLists.size(); j++) {
                    if (body.getVehicleTradeThirdPartyHistoryReports().get(i).getThirdPartyHistoryReportId().equalsIgnoreCase(AppConstant.reportLists.get(j).getId())) {
                        HistoryListPojo historyListPojo = AppConstant.reportLists.get(j);
                        historyListPojo.setChecked(true);
                        historyListPojos1.add(historyListPojo);
                    }
                }
            }
            vehicleDetails.setReportSelection(historyListPojos1);
            vehicleDetails.setCoReportSelection(historyListPojos1);
        } else {
            if (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 27 && !body.isDisclosureSkipped()) {
//                ArrayList<HistoryListPojo> list = vehicleDetails.getUsedVehicleReport();
//                list.get(list.size() - 1).setChecked(true);
//                vehicleDetails.setUsedVehicleReport(list);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                HistoryListPojo historyListPojo=new HistoryListPojo() ;
                if (AppConstant.reportLists.size()>0) {
                    historyListPojo = AppConstant.reportLists.get(AppConstant.reportLists.size() - 1);
                }
                historyListPojo.setChecked(true);
                historyListPojos1.add(historyListPojo);
                vehicleDetails.setReportSelection(historyListPojos1);
                vehicleDetails.setCoReportSelection(historyListPojos1);
            }
            if (rowData.isWebDeal()) {
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                HistoryListPojo historyListPojo=new HistoryListPojo() ;
                if (AppConstant.reportLists.size()>0) {
                    historyListPojo = AppConstant.reportLists.get(AppConstant.reportLists.size() - 1);
                }
                historyListPojo.setChecked(true);
                historyListPojos1.add(historyListPojo);
                vehicleDetails.setReportSelection(historyListPojos1);
                vehicleDetails.setCoReportSelection(historyListPojos1);
            }
        }
        vehicleDetails.setStockNumber(body.getStockNumber());
        vehicleDetails.setMileage(body.getMileage());


        /*co-buyer details*/
        settingsAndPermissions.setHasCoBuyer(false);
        settingsAndPermissions.setCoBuyer(false);
        if (body.getCoBuyers() != null && body.getCoBuyers().size() > 0) {
            settingsAndPermissions.setHasCoBuyer(true);
            if (body.getCustomerDetail().getLastScreenName() != null && body.getCustomerDetail().getLastScreenName().contains("Co-Buyer")) {
                settingsAndPermissions.setCoBuyer(true);
            }
            coBuyerCustomerDetails.setDLNumber(rowData.getCustomerDetails().getDLNumber());
            coBuyerCustomerDetails.setFirstName(body.getCoBuyers().get(0).getFirstName() == null ? "" : body.getCoBuyers().get(0).getFirstName());
            coBuyerCustomerDetails.setLastName(body.getCoBuyers().get(0).getLastName() == null ? "" : body.getCoBuyers().get(0).getLastName());
            coBuyerCustomerDetails.setGender(body.getCoBuyers().get(0).getGenderTitle());
            coBuyerCustomerDetails.setId(body.getCoBuyers().get(0).getId());
            coBuyerCustomerDetails.setEmail(body.getCoBuyers().get(0).getEmail());
            coBuyerCustomerDetails.setMobileNumber(body.getCoBuyers().get(0).getContactNumber());
            coBuyerCustomerDetails.setTermConditionAccepted(body.getCoBuyers().get(0).getIsTermConditionAccepted());
            coBuyerCustomerDetails.setAcceptedUsedCarPolicy(body.getCoBuyers().get(0).getIsAcceptedUsedCarPolicy());
            coBuyerCustomerDetails.setDLNumber(body.getCoBuyers().get(0).getDrivingLicenseNumber());
            coBuyerCustomerDetails.setMiddleName(body.getCoBuyers().get(0).getMiddleName());
            coBuyerCustomerDetails.setLanguageId(body.getCoBuyers().get(0).getLanguageId());
            coBuyerCustomerDetails.setCoBuyerDocs(body.getCoBuyers().get(0).getCoBuyerDocs());
            coBuyerCustomerDetails.setIsRemote(body.getCoBuyers().get(0).isRemote());
            coBuyerCustomerDetails.setCustomerAgreement(body.getCoBuyers().get(0).getCustomerAgreement());
            vehicleDetails.setCoFirstName(body.getCoBuyers().get(0).getFirstName() == null ? "" : body.getCoBuyers().get(0).getFirstName());
            vehicleDetails.setCoLastName(body.getCoBuyers().get(0).getLastName() == null ? "" : body.getCoBuyers().get(0).getLastName());
            if (body.getCoBuyers().get(0).getLocation() != null) {
                coBuyerCustomerDetails.setCity(body.getCoBuyers().get(0).getLocation().getCity());
                coBuyerCustomerDetails.setZipCode(body.getCoBuyers().get(0).getLocation().getZipcode());
                coBuyerCustomerDetails.setState(body.getCoBuyers().get(0).getLocation().getState());
                coBuyerCustomerDetails.setAddressLineOne(body.getCoBuyers().get(0).getLocation().getAddress1());
                coBuyerCustomerDetails.setAddressLineTwo(body.getCoBuyers().get(0).getLocation().getAddress2());
                coBuyerCustomerDetails.setCountry(body.getCoBuyers().get(0).getLocation().getCountry());
            }
//            coBuyerCustomerDetails.setCheckLists(body.getCoBuyers().get(0).get());
//            coBuyerCustomerDetails.setCoBuyerDocs(body.getCoBuyers().get(0).getCoBuyerDocs());
            try {
                if (body.getCoBuyers().get(0).getDateOfBirth() != null && body.getCoBuyers().get(0).getDateOfBirth().length() > 0) {
                    coBuyerCustomerDetails.setDateOfBirth(originalFormatter1.format(sdf.parse(body.getCoBuyers().get(0).getDateOfBirth())));
                }
                if (body.getCoBuyers().get(0).getDrivingLicenseExpiryDate() != null && body.getCoBuyers().get(0).getDrivingLicenseExpiryDate().length() > 0)
                    coBuyerCustomerDetails.setDLExpiryDate(originalFormatter1.format(sdf.parse(body.getCoBuyers().get(0).getDrivingLicenseExpiryDate())));
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        if (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 72 || (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 67 && settingsAndPermissions.isCoBuyer() && !customerDetails.getIsRemote())) {
            settingsAndPermissions.setStickerRemovalPermission(body.getAllow2RemoveWindowSticker());
            if (body.getAllow2RemoveWindowSticker()) {
                settingsAndPermissions.setStickerRemovalString("1");
                settingsAndPermissions.setStickerRemovalPermission(true);
            } else {
                settingsAndPermissions.setStickerRemovalString("2");
            }
        }
//       vehicleDetails.set


        /*Trade-in */
        settingsAndPermissions.setHasTradeIn(false);
        settingsAndPermissions.setTradeIn(false);
        if (body.getTradeInVehicle().size() > 0) {
            settingsAndPermissions.setHasTradeIn(true);
            if (body.getCustomerDetail().getLastScreenName().contains("Trade-in")) {
                settingsAndPermissions.setTradeIn(true);
            }
            tradeInVehicle.setCarMake(body.getTradeInVehicle().get(0).getMake());
            tradeInVehicle.setCarModel(body.getTradeInVehicle().get(0).getModel());
            tradeInVehicle.setCarYear(body.getTradeInVehicle().get(0).getModelYear());
            tradeInVehicle.setVINNumber(body.getTradeInVehicle().get(0).getVIN());
            tradeInVehicle.setMileage(body.getTradeInVehicle().get(0).getMileage());
            tradeInVehicle.setExteriorColor(body.getTradeInVehicle().get(0).getExteriorColor());
            tradeInVehicle.setInteriorColor(body.getTradeInVehicle().get(0).getInteriorColor());
            tradeInVehicle.setId(body.getTradeInVehicle().get(0).getId());
            tradeInVehicle.setTradeInVehicleDocs(body.getTradeInVehicle().get(0).getTradeInVehicleDocs());
        }
        record.setCustomerDetails(customerDetails);
        record.setVehicleDetails(vehicleDetails);
        record.setCoBuyerCustomerDetails(coBuyerCustomerDetails);
        record.setDealershipDetails(dealershipDetails);
        record.setTradeInVehicle(tradeInVehicle);
        record.setSettingsAndPermissions(settingsAndPermissions);
        record.setSyncUp(syncUp);

        int index = recordData.getIndexNumber(customerDetails.getDLNumber());
        if (recordData.getRecords().size() > index) {
            recordData.getRecords().set(index, record);
        } else {
            recordData.getRecords().add(index, record);
        }
        //Add the recordData object to the SharedPreference after converting it to string
        writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), " Json in deal Data API: " + gson.toJson(record));
        if (!record.isWebDeal()) {
            if (getPageIndex(body.getCustomerDetail().getLastScreenName()) > 81) {
                settingsAndPermissions.setDataSyncedWithServer(true);
                String recordDataString1 = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                Record record1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                onDelete.onDelete(0, false, true, false);
//            new CustomToast(activity).alert("This deal has been completed by " + settingsAndPermissions.getTabName() + ". Please select another deal to continue.");
            } else {
                String recordDataString1 = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                Record record1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                navigateToSpecificScreen(record1, false);
            }
        } else if (record.isWebDeal() && record.getDealStatusId() == 4) {
            if (record.isCoBuyer()) {
                if (record.isBuyerSigned()) {
                    settingsAndPermissions.setLastPageIndex(54);
                    AppConstant.restrictedPage = 54;
                    record.setSettingsAndPermissions(settingsAndPermissions);
                    record.getSettingsAndPermissions().setCoBuyer(true);
                    int index1 = recordData.getIndexNumber(customerDetails.getDLNumber());
                    if (recordData.getRecords().size() > index1) {
                        recordData.getRecords().set(index1, record);
                    } else {
                        recordData.getRecords().add(index1, record);
                    }
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    Record record1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    navigateToSpecificScreen(record1, false);
                } else {
                    settingsAndPermissions.setLastPageIndex(54);
                    AppConstant.restrictedPage = 54;
                    record.setSettingsAndPermissions(settingsAndPermissions);
                    int index1 = recordData.getIndexNumber(customerDetails.getDLNumber());
                    if (recordData.getRecords().size() > index1) {
                        recordData.getRecords().set(index1, record);
                    } else {
                        recordData.getRecords().add(index1, record);
                    }
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    Record record1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    navigateToSpecificScreen(record1, false);
                }
            } else {
                settingsAndPermissions.setLastPageIndex(54);
                AppConstant.restrictedPage = 54;
                record.setSettingsAndPermissions(settingsAndPermissions);
                int index1 = recordData.getIndexNumber(customerDetails.getDLNumber());
                if (recordData.getRecords().size() > index1) {
                    recordData.getRecords().set(index1, record);
                } else {
                    recordData.getRecords().add(index1, record);
                }
                String recordDataString1 = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                Record record1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                navigateToSpecificScreen(record1, false);
            }

            // pass to customer info pages
        }
//        else if (record.isWebDeal() && record.getDealStatusId() != 4) {
//            new CustomToast(activity).alert(record.getDealStatusText());
//        }
    }

    private void writeCustomLogsAPI(String id, String logs) {
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setLogText(logs);
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private VehicleDetails setDisclosures(String carTypeTitle, IncompleteDealDetailResponse body, VehicleDetails vehicleDetails) {


        return vehicleDetails;
    }

    private String getReportIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase("CarFax Report")) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase("AutoCheck Report")) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase("Auction Announcement")) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase("Other Vehicle History Report")) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase("NMVTIS Report")) {
            id = "6";
        } else if (aList.trim().equalsIgnoreCase("None")) {
            id = "5";
        }
        return id;
    }

    private String getPrintIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase(activity.getString(R.string.print1))) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase(activity.getString(R.string.print2))) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase(activity.getString(R.string.print3))) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase(activity.getString(R.string.print4))) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase(activity.getString(R.string.print5))) {
            id = "5";
        } else if (aList.trim().equalsIgnoreCase(activity.getString(R.string.print6))) {
            id = "6";
        }
        return id;
    }

    private String getIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase("Prior Damage")) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase("Prior Accident")) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase("Prior Fleet Vehicle")) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase("Prior Commercial Vehicle")) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase("The vehicle has been Painted")) {
            id = "5";
        } else if (aList.trim().equalsIgnoreCase("Possible Frame/Unibody or Structural Damage – which means damage to any component designed to provide structural integrity of the vehicle. Even minor accidents can cause structural/frame damage to a vehicle. Having a structural inspection before purchase is recommended.")) {
            id = "6";
        } else if (aList.trim().equalsIgnoreCase("Lowered/Raised and/or Lifted with Aftermarket Oversized Wheels and/or Low Profile Tires and/or Altered Suspension. Structural alteration. Manufacturer’s warranty may be void.")) {
            id = "7";
        } else if (aList.trim().equalsIgnoreCase("Modified and/or altered from manufacturer’s specifications, and/or aftermarket equipment(s)/accessories. Manufacturer’s warranty may be void.")) {
            id = "8";
        } else if (aList.trim().equalsIgnoreCase("Correct Odometer or Mileage unknown")) {
            id = "9";
        } else if (aList.trim().equalsIgnoreCase("May be an out-of-state vehicle and it may have an out-of-state title")) {
            id = "10";
        } else if (aList.trim().equalsIgnoreCase("Airbag has been previously deployed")) {
            id = "11";
        } else if (aList.trim().equalsIgnoreCase("Known Frame/Unibody or Structural Damage – which means damage to any component designed to provide structural integrity of the vehicle. Even minor accidents can cause structural/frame damage to a vehicle. Having a structural inspection before purchase is recommended.")) {
            id = "12";
        } else if (aList.trim().equalsIgnoreCase("Open Recall")) {
            id = "13";
        } else if (aList.trim().equalsIgnoreCase("Other")) {
            id = "14";
        } else if (aList.trim().equalsIgnoreCase("BASED ON THE INFORMATION WE ARE AWARE OF, NONE OF THE ABOVE CONDITIONS OR HISTORY DISCLOSURES APPLY TO THE VEHICLE.")) {
            id = "0";
        }
        return id;
    }

    private void navigateToSpecificScreen(Record rowData, boolean canGoBack) {

        PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
        PreferenceManger.putString(AppConstant.CURRENT_LANG, rowData.getSettingsAndPermissions().getSelectedLanguage() == null ? "en" : rowData.getSettingsAndPermissions().getSelectedLanguage());
        if (!canGoBack) {
            AppConstant.restrictedPage = rowData.getSettingsAndPermissions().getLastPageIndex();
        } else {
            AppConstant.restrictedPage = -1;
        }
        if (rowData.getSettingsAndPermissions().getLastPageIndex() > 68 && rowData.getSettingsAndPermissions().getSelectedLanguage() != null) {
            String selectedLanguage;
            if (rowData.getSettingsAndPermissions().isCoBuyer())
                selectedLanguage = rowData.getSettingsAndPermissions().getCoBuyerSelectedLanguage();
            else
                selectedLanguage = rowData.getSettingsAndPermissions().getSelectedLanguage();
            if (selectedLanguage == null) {
                selectedLanguage = "English";
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(rowData.getCustomerDetails().getDLNumber());
                record.getSettingsAndPermissions().setSelectedLanguage(selectedLanguage);
                record.getSettingsAndPermissions().setCoBuyerSelectedLanguage(selectedLanguage);
                String recordDataString1 = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
            }
            if (selectedLanguage.equalsIgnoreCase("Chinese")) {
                setLocale("zh", rowData.getSettingsAndPermissions().getLastPageIndex(), canGoBack);
            } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
                setLocale("es", rowData.getSettingsAndPermissions().getLastPageIndex(), canGoBack);
            } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
                setLocale("vi", rowData.getSettingsAndPermissions().getLastPageIndex(), canGoBack);
            } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
                setLocale("ko", rowData.getSettingsAndPermissions().getLastPageIndex(), canGoBack);
            } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
                setLocale("tl", rowData.getSettingsAndPermissions().getLastPageIndex(), canGoBack);
            } else if (selectedLanguage.equalsIgnoreCase("English")) {
                Intent intent1 = new Intent(activity, NewDealViewPager.class);
                PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                intent1.putExtra("page", rowData.getSettingsAndPermissions().getLastPageIndex());
                intent1.putExtra("canGoBack", canGoBack);
                activity.startActivity(intent1);
            }
        } else {
            if (rowData.getSettingsAndPermissions().getLastPageIndex() > 2 && rowData.getSettingsAndPermissions().getLastPageIndex() < 13) {
                Intent intent1 = new Intent(activity, NewDealViewPager.class);
                PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                intent1.putExtra("page", 2);
                intent1.putExtra("canGoBack", canGoBack);
                activity.startActivity(intent1);
//                            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else if (rowData.getSettingsAndPermissions().getLastPageIndex() > 13 && rowData.getSettingsAndPermissions().getLastPageIndex() < 24) {
                Intent intent1 = new Intent(activity, NewDealViewPager.class);
                PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                intent1.putExtra("page", 13);
                intent1.putExtra("canGoBack", canGoBack);
                activity.startActivity(intent1);
//                            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else if (rowData.getSettingsAndPermissions().getLastPageIndex() > 43 && rowData.getSettingsAndPermissions().getLastPageIndex() < 54) {
                Intent intent1 = new Intent(activity, NewDealViewPager.class);
                PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                intent1.putExtra("page", 43);
                intent1.putExtra("canGoBack", canGoBack);
                activity.startActivity(intent1);
//                            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else if (rowData.getSettingsAndPermissions().getLastPageIndex() > 64 && rowData.getSettingsAndPermissions().getLastPageIndex() < 67) {
                Intent intent1 = new Intent(activity, NewDealViewPager.class);
                PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                intent1.putExtra("page", 64);
                intent1.putExtra("canGoBack", canGoBack);
                activity.startActivity(intent1);
//                            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else {
                Intent intent1 = new Intent(activity, NewDealViewPager.class);
                PreferenceManger.putString(AppConstant.DMV_NUMBER, rowData.getCustomerDetails().getDLNumber());
//                if (rowData.getSettingsAndPermissions().getLastPageIndex() == 0) {
//                    intent1.putExtra("page", 1);
//                } else {
                intent1.putExtra("page", rowData.getSettingsAndPermissions().getLastPageIndex());
//                }
                intent1.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
                intent1.putExtra("canGoBack", canGoBack);
                activity.startActivity(intent1);
            }
        }
    }

    private void setLocale(String localeName, int page, boolean canGoBack) {
//        Locale myLocale = new Locale(localeName);
//
//        Resources res = activity.getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.setLocale(myLocale);
//        res.updateConfiguration(conf, dm);
        Lingver.getInstance().setLocale(activity, localeName);
        Log.e("record adapter Next: ", " Current locale: " + localeName);
        Intent refresh = new Intent(activity, NewDealViewPager.class);
        refresh.putExtra("page", page);
        refresh.putExtra("canGoBack", canGoBack);
        PreferenceManger.putString(AppConstant.CURRENT_LANG, localeName);
        refresh.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
        activity.startActivity(refresh);
    }

    public void removeAt(int position) {
        recordData.remove(position);
        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, cardPojoArrayList.size());
//        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return recordData.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        DSTextView name, vehicalDetail, salespeson, date, error, sentEmail, dsNameHolder, dealershipName;
        DSTextView deleteButton, statusWeb;
        ImageView lock, synced, inProgress, front, wait;
        LinearLayout row, frontContainer;

        TypeRow(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.customerName);
            statusWeb = itemView.findViewById(R.id.statusWeb);
            dsNameHolder = itemView.findViewById(R.id.dsNameHolder);
            dealershipName = itemView.findViewById(R.id.dealershipName);
            error = itemView.findViewById(R.id.error);
            sentEmail = itemView.findViewById(R.id.sentEmail);
            vehicalDetail = itemView.findViewById(R.id.vehicalDetail);
            salespeson = itemView.findViewById(R.id.salespeson);
            date = itemView.findViewById(R.id.date);
            deleteButton = itemView.findViewById(R.id.delete_record);
            row = itemView.findViewById(R.id.row);
            synced = itemView.findViewById(R.id.synced);
            inProgress = itemView.findViewById(R.id.inprogress);
            lock = itemView.findViewById(R.id.lock);
            wait = itemView.findViewById(R.id.wait);
            frontContainer = itemView.findViewById(R.id.frontContainter);
            front = itemView.findViewById(R.id.front);

        }
    }

    public int getPageIndex(String uniqueDName) {
        if (uniqueDName == null || uniqueDName.length() == 0) {
            return 0;
        }
        if (uniqueDName.equalsIgnoreCase("Vehicle Type Used/New")) {
            return 0;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle VIN Scanner Capture")) {
            return 1;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle info after Scan")) {
            return 2;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Front of Vehicle")) {
            return 3;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Front of Vehicle")) {
            return 4;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Passenger Side")) {
            return 5;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Passenger Side")) {
            return 6;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Back Vehicle")) {
            return 7;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Back Vehicle")) {
            return 8;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo Driver Side")) {
            return 9;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Driver Side")) {
            return 10;
        } else if (uniqueDName.equalsIgnoreCase("Capture Photo  Buyers Guide")) {
            return 11;
        } else if (uniqueDName.equalsIgnoreCase("Preview Photo Buyers Guide")) {
            return 12;
        } else if (uniqueDName.equalsIgnoreCase("Add More Vehicle Images")) {
            return 13;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional Photo 1")) {
            return 14;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 1")) {
            return 15;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 2")) {
            return 16;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 2")) {
            return 17;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 3")) {
            return 18;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 3")) {
            return 19;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 4")) {
            return 20;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 4")) {
            return 21;
        } else if (uniqueDName.equalsIgnoreCase("Capture Additional image 5")) {
            return 22;
        } else if (uniqueDName.equalsIgnoreCase("Preview Additional image 5")) {
            return 23;
        } else if (uniqueDName.equalsIgnoreCase("Skip Disclosures")) {
            return 24;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle Condition Disclosure Capture")) {
            return 25;
        }  else if (uniqueDName.equalsIgnoreCase("New Vehicle History Disclosure Capture")) {
            return 25;
        }else if (uniqueDName.equalsIgnoreCase("Vehicle Use Disclosure Capture")) {
            return 26;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle Third Party Reports Capture")) {
            return 27;
        } else if (uniqueDName.equalsIgnoreCase("Save deal or continue deal")) {
            return 28;
        } else if (uniqueDName.equalsIgnoreCase("Salesperson Selection Dropdown")) {
            return 29;
        } else if (uniqueDName.equalsIgnoreCase("Odometer Photo Capture")) {
            return 30;
        } else if (uniqueDName.equalsIgnoreCase("Odometer Photo Preview")) {
            return 31;
        } else if (uniqueDName.equalsIgnoreCase("Manual Vehicle Odometer Capture")) {
            return 32;
        } else if (uniqueDName.equalsIgnoreCase("Is There Trade-in Capture")) {
            return 33;
        } else if (uniqueDName.equalsIgnoreCase("Scan Trade-in Capture")) {
            return 34;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Info After Scan")) {
            return 35;
        } else if (uniqueDName.equalsIgnoreCase("Capture Front Photo of Trade-in")) {
            return 36;
        } else if (uniqueDName.equalsIgnoreCase("Preview Front Photo of Trade-in")) {
            return 37;
        } else if (uniqueDName.equalsIgnoreCase("Capture License Plate Photo of Trade-in")) {
            return 38;
        } else if (uniqueDName.equalsIgnoreCase("Preview License Plate Photo of trade-in")) {
            return 39;
        } else if (uniqueDName.equalsIgnoreCase("Capture Odometer Photo of Trade-in")) {
            return 40;
        } else if (uniqueDName.equalsIgnoreCase("Preview Odometer Photo of Trade-in")) {
            return 41;
        } else if (uniqueDName.equalsIgnoreCase("Manual Trade-in Odometer Capture")) {
            return 42;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Add More Images")) {
            return 43;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 1")) {
            return 44;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 1")) {
            return 45;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 2")) {
            return 46;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 2")) {
            return 47;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 3")) {
            return 48;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 3")) {
            return 49;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 4")) {
            return 50;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 4")) {
            return 51;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image 5")) {
            return 52;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Additional image preview 5")) {
            return 53;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Front Capture")) {
            return 54;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Front Capture")) {
            return 54;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Front Preview")) {
            return 55;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Front Preview")) {
            return 55;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Scan Back Capture")) {
            return 56;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Scan Back Capture")) {
            return 56;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Scanned Back Result")) {
            return 57;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Scanned Back Result")) {
            return 57;
        } else if (uniqueDName.equalsIgnoreCase("Buyer ID Info Confirmation")) {
            return 58;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer ID Info Confirmation")) {
            return 58;
        } else if (uniqueDName.equalsIgnoreCase("Is There Co-buyer?")) {
            return 59;
        } else if (uniqueDName.equalsIgnoreCase("Continue F&I Different Tablet?")) {
            return 60;
        } else if (uniqueDName.equalsIgnoreCase("Vehicle Summary Review Dealer")) {
            return 61;
        } else if (uniqueDName.equalsIgnoreCase("Trade-in Summary Dealer")) {
            return 62;
        } else if (uniqueDName.equalsIgnoreCase("Finalize Disclosures?")) {
            return 63;
        } else if (uniqueDName.equalsIgnoreCase("Proof of Insurance Photo?")) {
            return 64;
        } else if (uniqueDName.equalsIgnoreCase("Proof of Insurance Photo Capture")) {
            return 65;
        } else if (uniqueDName.equalsIgnoreCase("Proof of Insurance Photo Preview")) {
            return 66;
        } else if (uniqueDName.equalsIgnoreCase("Handover Tablet to Buyer")) {
            return 67;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Language Selection")) {
            return 68;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Language Selection")) {
            return 68;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Disclosures")) {
            return 69;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Disclosures")) {
            return 69;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Confirm Vehicle Photos")) {
            return 70;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Confirm Vehicle Photos")) {
            return 70;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Confirm vehicle details")) {
            return 71;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Confirm vehicle details")) {
            return 71;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Test Drive Screen")) {
            return 72;
        } else if (uniqueDName.equalsIgnoreCase("Buyer New Vehicle Test Drive Confirmation")) {
            return 72;
        }else if (uniqueDName.equalsIgnoreCase("Co-Buyer Test Drive Screen")) {
            return 72;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer New Vehicle Test Drive Confirmation")) {
            return 72;
        }  else if (uniqueDName.equalsIgnoreCase("Buyer Buyers Guide")) {
            return 73;
        } else if (uniqueDName.equalsIgnoreCase("Buyer MSRP/Addendum Sticker Authorization")) {
            return 73;
        }else if (uniqueDName.equalsIgnoreCase("Co-Buyer Buyers Guide")) {
            return 73;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer MSRP/Addendum Sticker Authorization")) {
            return 73;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Vehicle History Disclosure")) {
            return 74;
        } else if (uniqueDName.equalsIgnoreCase("Buyer New Vehicle History Disclosure")) {
            return 74;
        }else if (uniqueDName.equalsIgnoreCase("Co-Buyer Vehicle History Disclosure")) {
            return 74;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer New Vehicle History Disclosure")) {
            return 74;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Vehicle Use Disclosure")) {
            return 75;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Vehicle Use Disclosure")) {
            return 75;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Third Party Report Confirmation")) {
            return 76;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Third Party Report Confirmation")) {
            return 76;
        } else if (uniqueDName.equalsIgnoreCase("Buyer No Verbal Promises")) {
            return 77;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer No Verbal Promises")) {
            return 77;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Almost Done")) {
            return 78;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Almost Done")) {
            return 78;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Document Checklist")) {
            return 79;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Document Checklist")) {
            return 79;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Final Approval")) {
            return 80;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Final Approval")) {
            return 80;
        } else if (uniqueDName.equalsIgnoreCase("Buyer Contact Info")) {
            return 81;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Contact Info")) {
            return 81;
        } else if (uniqueDName.equalsIgnoreCase("Co-Buyer Selection")) {
            return 82;
        } else if (uniqueDName.equalsIgnoreCase("Final Thank You Page Deal Completed")) {
            return 82;
        }
        /*Trade-In License Plate Image Preview*/
        return 0;
    }

}
