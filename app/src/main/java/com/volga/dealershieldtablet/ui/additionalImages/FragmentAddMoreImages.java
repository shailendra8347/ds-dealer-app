package com.volga.dealershieldtablet.ui.additionalImages;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

/**
 * Created by ${Shailendra} on 22-08-2018.
 */
public class FragmentAddMoreImages extends BaseFragment {
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    IGoToNextPage iGoToNextPage;
    private RecordData recordData;
    private Gson gson;
    private boolean addMore = false;
    private Button yes, no;
    private DSTextView title;
    private boolean isCalled = false;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            isCalled = true;
            showFragmentInPortrait();
            updateData();

            //Code for displaying already selected Confirmation
        }
    }

    private void updateData() {
//        showFragmentInPortrait();
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (title != null) {
            if (NewDealViewPager.currentPage == 64) {
                title.setText(R.string.add_insurence);
            } else {
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                    title.setText(Html.fromHtml(getString(R.string.add_more_title_tradein)));
                } else {
                    title.setText(R.string.add_more_title);
                }
            }


        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.addtional_layout, container, false);
        initView();
        return mView;
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        next.setVisibility(View.GONE);

        final TextView back = mView.findViewById(R.id.text_back_up);
        title = mView.findViewById(R.id.title);

        final DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_cancelBack.setClickable(false);
                text_cancelBack.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        text_cancelBack.setClickable(true);
                        text_cancelBack.setEnabled(true);
                    }
                }, 6000);
                cancel();
            }
        });
        DSTextView txt_logout = mView.findViewById(R.id.txt_logout);
        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
//        text_cancelBack.setVisibility(View.INVISIBLE);
        updateData();
//        if (NewDealViewPager.currentPage == 13) {
//            showFragmentInPortrait();
//        }
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.whatNextClick();
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
                isAnsSelected = true;
                addMore = true;
                next.setVisibility(View.VISIBLE);

            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*show confirmtion pop up*/
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                next.setVisibility(View.VISIBLE);
                addMore = false;
                goToSpecificPage();
            }
        });
        next.setVisibility(View.GONE);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back.setClickable(false);
                back.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        back.setClickable(true);
                        back.setEnabled(true);
                    }
                }, 3000);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    ((NewDealViewPager) getActivity()).setCurrentPage(41);
                else
                    iGoToNextPage.goToBackIndex();

            }
        });

    }

    private void goToSpecificPage() {

        if (NewDealViewPager.currentPage == 64) {
            ((NewDealViewPager) getActivity()).setCurrentPage(67);
        } else if (NewDealViewPager.currentPage == 43) {
            PreferenceManger.putInt(AppConstant.SKIPPED_PAGE, ((NewDealViewPager) getActivity()).getCurrentPage());
            ((NewDealViewPager) getActivity()).setCurrentPage(54);
        } else {
            PreferenceManger.putInt(AppConstant.SKIPPED_PAGE, ((NewDealViewPager) getActivity()).getCurrentPage());
            ((NewDealViewPager) getActivity()).setCurrentPage(24);
        }
//        PreferenceManger.putInt(AppConstant.SKIPPED_PAGE, ((NewDealViewPager) getActivity()).getCurrentPage());
//        if (NewDealViewPager.currentPage == 42) {
//            ((NewDealViewPager) getActivity()).setCurrentPage(45);
//        } else {
//            Gson gson = new GsonBuilder().create();
//            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
//            String recordDataString = gson.toJson(recordData);
//            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//            PreferenceManger.putInt(AppConstant.SKIPPED_PAGE, ((NewDealViewPager) getActivity()).getCurrentPage());
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
//                ((NewDealViewPager) getActivity()).setCurrentPage(36);
//            } else
//                ((NewDealViewPager) getActivity()).setCurrentPage(35);
//        }

//        iGoToNextPage.whatNextClick();
//
    }

    private void SetRecordData() {
        PreferenceManger.putBoolean(AppConstant.IS_CO_BUYER_SELECTED, addMore);
    }
}
