package com.volga.dealershieldtablet.ui.tradeIn;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.pojo.AddSalesperson;
import com.volga.dealershieldtablet.screenRevamping.pojo.LocalSalesPerson;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentSalesPerson extends BaseFragment {

    private View mView;
    private DSTextView next;
    DSEdittext email, firstName, lastName, et_mobile;

    IGoToNextPage iGoToNextPage;
    private RadioButton radioId;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && firstName != null) {
            next.setEnabled(true);
            next.setClickable(true);
//            updateData();
        }
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons() != null) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName().length() > 0) {
                firstName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName());
            }
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName().length() > 0) {
                lastName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getLastName());
            }
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName().length() > 0) {
                email.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getEmail());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.sales_person_layout, container, false);
        initView();
        return mView;
    }


    private void initView() {
        next = mView.findViewById(R.id.save);
        next.setVisibility(View.VISIBLE);
        TextView back = mView.findViewById(R.id.txt_logout);
        back.setVisibility(View.VISIBLE);
        back.setText("Back");
        back.setTextColor(getResources().getColor(R.color.back_red));
        DSTextView title = mView.findViewById(R.id.title);
        title.setText(Html.fromHtml(getString(R.string.salesperson)));
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setVisibility(View.INVISIBLE);
        email = mView.findViewById(R.id.et_email);
        firstName = mView.findViewById(R.id.firstName);
        lastName = mView.findViewById(R.id.lastName);
        et_mobile = mView.findViewById(R.id.et_mobile);
        RadioGroup radioGroup = mView.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioId = mView.findViewById(checkedId);
            }
        });
        final String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });
//        updateData();
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                String regx = "^[\\p{L} .'-]+$";
                if (firstName.getText().toString().trim().length() == 0) {
                    new CustomToast(getActivity()).alert(getString(R.string.first_name_empty));

                } else if (lastName.getText().toString().trim().length() == 0) {
                    new CustomToast(getActivity()).alert(getString(R.string.lastName_empty));
                } else if (firstName.getText().toString().trim().length() == 1) {
                    new CustomToast(getActivity()).alert("Please enter full first name.");
                } else if (lastName.getText().toString().trim().length() == 1) {
                    new CustomToast(getActivity()).alert("Please enter full last name.");
                } else if (firstName.getText().toString().trim().length() > 0 && !firstName.getText().toString().matches(regx)) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_first_name));
                } else if (lastName.getText().toString().trim().length() > 0 && !lastName.getText().toString().matches(regx)) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_last_name));
                } else if (email.getText().toString().trim().length() == 0) {
                    new CustomToast(getActivity()).alert(getString(R.string.empty_email));
                } else if (et_mobile.getText().toString().trim().length() == 0) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_mobile));
                } else if (email.getText().toString().trim().length() > 0 && !email.getText().toString().trim().matches(emailPattern)) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_email));
                } else if (et_mobile.getText().toString().trim().length() != 10) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_mobile));
                } else {

                    Gson gson = new GsonBuilder().create();

                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    AddSalesperson addSalesperson = new AddSalesperson();
                    addSalesperson.setEmail(email.getText().toString().trim());
                    addSalesperson.setFirstName(firstName.getText().toString().trim());
                    addSalesperson.setLastName(lastName.getText().toString().trim());
                    addSalesperson.setPhoneNumber(et_mobile.getText().toString().trim());
                    addSalesperson.setDealershipIds(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getDealershipDetails().getDealershipId() + "");
                    addSalesperson.setPhoneNumber(et_mobile.getText().toString().trim());
                    addSalesperson.setRole("Staff");
                    if (radioId != null) {
                        if (radioId.getText().toString().equalsIgnoreCase("Sales"))
                            addSalesperson.setDepartmentId(1 + "");
                        else if (radioId.getText().toString().equalsIgnoreCase("Both"))
                            addSalesperson.setDepartmentId(3 + "");
                        else
                            addSalesperson.setDepartmentId(2 + "");
                    } else {
                        addSalesperson.setDepartmentId(1 + "");
                    }
                    if (DSAPP.getInstance().isNetworkAvailable()) {
                        next.setEnabled(false);
                        next.setClickable(false);
                        saveSalesPerson(addSalesperson);
                    } else {
                        next.setEnabled(true);
                        next.setClickable(true);
                        new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                    }

                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
            }
        });
    }

    private void saveSalesPerson(final AddSalesperson addSalesperson) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setMessage("Adding Salesperson...");

        RetrofitInitialization.getDs_services().addSalesPerson("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), addSalesperson).enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                progressDialog.dismiss();
                if (response.code() == 200 && response.isSuccessful()) {
                    PreferenceManger.putBoolean("added", true);
                    PreferenceManger.putString("sEmail", addSalesperson.getEmail());
                    if (response.body().getMessage() != null) {
                        new CustomToast(getActivity()).alert(response.body().getMessage());
                    } else
                        new CustomToast(getActivity()).alert("User has been added successfully. An email and sms will go to the user with their credentials.");

                    /*Update sales person Here*/

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSalesPersonList();
                        }
                    }, 2800);
                    Log.e("Response ", "onResponse: " + response.message());
                } else {
                    if (response.code() == 401 || response.code() == 403) {
                        if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                            callRefreshTokenAPI();
                        } else {
                            PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                            PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                            Locale myLocale = new Locale("en");
                            Resources res = getResources();
                            DisplayMetrics dm = res.getDisplayMetrics();
                            Configuration conf = res.getConfiguration();
                            conf.locale = myLocale;
                            res.updateConfiguration(conf, dm);
                            callHomeActivity();
                        }
                    }
                    assert response.errorBody() != null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        new CustomToast(getActivity()).alert(json.getString("Message"));
                        next.setEnabled(true);
                        next.setClickable(true);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                    Log.e("Response ", "onResponse: " + json);
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                }
            }
        });
    }

    private void getSalesPersonList() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        progressDialog.setMessage("Updating Salesperson...");
//        progress.setVisibility(View.VISIBLE);
        String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        final Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String id = recordData.where(rand_int1).getDealershipDetails().getDealershipId() + "";
//        salesPersons.clear();
//        adapter.notifyDataSetChanged();
        RetrofitInitialization.getDs_services().getSalesPerson("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<ArrayList<SalesPersonList>>() {
            @Override
            public void onResponse(Call<ArrayList<SalesPersonList>> call, Response<ArrayList<SalesPersonList>> response) {
//                progress.setVisibility(View.GONE);
                progressDialog.dismiss();
                if (response.isSuccessful() && response.code() == 200) {
                    LocalSalesPerson localSalesPerson = new LocalSalesPerson();
                    localSalesPerson.setLocalSales(response.body());
                    String list = gson.toJson(localSalesPerson);
                    PreferenceManger.putString(AppConstant.SALES_PERSON, "");
                    PreferenceManger.putString(AppConstant.SALES_PERSON, list);
                    Log.i("Sales lis: ", "onResponse: " + list);
                    if (getActivity() != null && !getActivity().isFinishing())
                        getActivity().finish();
//                    SalesPersonList salesPersonList = new SalesPersonList();
//                    salesPersonList.setFirstName("Please select salesperson");
//                    salesPersons.addAll(response.body());
//                    salesPersons.add(0, salesPersonList);
//                    adapter.notifyDataSetChanged();
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons() != null) {
//                        com.volga.dealershieldtablet.ui.coBuyer.SalesPersons salesPersons1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons();
//                        if (!PreferenceManger.getBooleanValue("added")) {
//                            if (salesPersons1 != null && salesPersons1.getUserId() != null && salesPersons1.getUserId().length() > 0) {
//                                int pos = 0;
//                                for (int i = 0; i < salesPersons.size(); i++) {
//                                    if (salesPersons.get(i).getId() != null && salesPersons.get(i).getId().equalsIgnoreCase(salesPersons1.getUserId())) {
//                                        pos = i;
//                                    }
//                                }
//                                spinnerOfferType.setSelection(pos);
//
//                            }
//                        } else {
//                            int pos = 0;
//                            for (int i = 1; i < salesPersons.size(); i++) {
//                                if (salesPersons.get(i).getEmail().equalsIgnoreCase(PreferenceManger.getStringValue("sEmail"))) {
//                                    pos = i;
//                                }
//                            }
//                            spinnerOfferType.setSelection(pos);
//                            PreferenceManger.putBoolean("added", false);
//                            PreferenceManger.putString("sEmail", "");
//                        }
//                    }else if (PreferenceManger.getBooleanValue("added")){
//                        int pos = 0;
//                        for (int i = 1; i < salesPersons.size(); i++) {
//                            if (salesPersons.get(i).getEmail().equalsIgnoreCase(PreferenceManger.getStringValue("sEmail"))) {
//                                pos = i;
//                            }
//                        }
//                        spinnerOfferType.setSelection(pos);
//                        PreferenceManger.putBoolean("added", false);
//                        PreferenceManger.putString("sEmail", "");
//                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SalesPersonList>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


}
