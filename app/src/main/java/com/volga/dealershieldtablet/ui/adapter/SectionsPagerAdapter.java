package com.volga.dealershieldtablet.ui.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.ui.additionalImages.AdditionalImageFragment;
import com.volga.dealershieldtablet.ui.additionalImages.FragmentAddMoreImages;
import com.volga.dealershieldtablet.ui.coBuyer.FragmentCoBuyerHandover;
import com.volga.dealershieldtablet.ui.coBuyer.FragmentIsCoBuyer;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarMileageEdit;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarMileagePicture;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureFront;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureLeft;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureRear;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureRight;
import com.volga.dealershieldtablet.ui.fragment.FragmentConfirmCarImages;
import com.volga.dealershieldtablet.ui.fragment.FragmentConfirmVehicleDetails;
import com.volga.dealershieldtablet.ui.fragment.FragmentContactInfo;
import com.volga.dealershieldtablet.ui.fragment.FragmentCustomerHandover1;
import com.volga.dealershieldtablet.ui.fragment.FragmentDMVPreview;
import com.volga.dealershieldtablet.ui.fragment.FragmentFinalReport;
import com.volga.dealershieldtablet.ui.fragment.FragmentFinanceScreen;
import com.volga.dealershieldtablet.ui.fragment.FragmentImagePreview;
import com.volga.dealershieldtablet.ui.fragment.FragmentLicenseInfoConfirmation;
import com.volga.dealershieldtablet.ui.fragment.FragmentLicenseInformation;
import com.volga.dealershieldtablet.ui.fragment.FragmentReceivedPrint;
import com.volga.dealershieldtablet.ui.fragment.FragmentRemoveStickers;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanDMVBack;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanDMVFront;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanVINSticker;
import com.volga.dealershieldtablet.ui.fragment.FragmentSelectLanguage;
import com.volga.dealershieldtablet.ui.fragment.FragmentTestDrive;
import com.volga.dealershieldtablet.ui.fragment.FragmentThankYouUser1;
import com.volga.dealershieldtablet.ui.fragment.FragmentThankYouUser2;
import com.volga.dealershieldtablet.ui.fragment.FragmentTypeOfPurchase;
import com.volga.dealershieldtablet.ui.fragment.FragmentUsedVehicleDisclosure;
import com.volga.dealershieldtablet.ui.fragment.FragmentUsedVehicleHistory;
import com.volga.dealershieldtablet.ui.fragment.FragmentUsedVehicleHistoryPart1;
import com.volga.dealershieldtablet.ui.fragment.FragmentVehicleInfo;
import com.volga.dealershieldtablet.ui.fragment.FragmentVehicleType;
import com.volga.dealershieldtablet.ui.fragment.FragmentWindowSticker;
import com.volga.dealershieldtablet.ui.fragment.UsedVehicleHistoryAfterSticker;
import com.volga.dealershieldtablet.ui.tradeIn.FragmentIsTradeIn;
import com.volga.dealershieldtablet.ui.tradeIn.FragmentSalesPerson;
import com.volga.dealershieldtablet.utils.AppConstant;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
    private final SingleDirectionViewPager mViewPager;


    public SectionsPagerAdapter(FragmentManager supportFragmentManager, SingleDirectionViewPager mViewPager) {
        super(supportFragmentManager);
        this.mViewPager = mViewPager;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FragmentSalesPerson();
        } else if (position == 1) {
            return new FragmentContactInfo();
        } else if (position == 2) {
            return new FragmentScanDMVFront();
        } else if (position == 3) {
            FragmentDMVPreview nextFragment = new FragmentDMVPreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.DMV_FRONT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 4) {
            return new FragmentScanDMVBack();
        } else if (position == 5) {
            return new FragmentLicenseInformation();
        } else if (position == 6) {
            return new FragmentLicenseInfoConfirmation();
        } else if (position == 7) {
            return new FragmentIsCoBuyer();
        } else if (position == 8) {
            return new FragmentVehicleType();
        } else if (position == 9) {
            return new FragmentScanVINSticker();
        } else if (position == 10) {
            return new FragmentVehicleInfo();
        } else if (position == 11) {
            return new FragmentCarPictureFront();
        } else if (position == 12) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_FRONT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 13) {
            return new FragmentCarPictureRight();
        } else if (position == 14) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_RIGHT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 15) {
            return new FragmentCarPictureRear();
        } else if (position == 16) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_REAR);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 17) {
            return new FragmentCarPictureLeft();
        } else if (position == 18) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_LEFT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 19) {
            return new FragmentWindowSticker();
        } else if (position == 20) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_WINDOW_VIN);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 21) {
            return new FragmentCarMileagePicture();
        } else if (position == 22) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_MILEAGE);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 23) {
            return new FragmentCarMileageEdit();
        } else if (position == 24) {
            return new FragmentAddMoreImages();
        }
        /*Add more images logic is here*/
        else if (position == 25) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "1");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 26) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL1);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 27) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "2");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 28) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL2);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 29) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "3");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 30) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL3);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 31) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "4");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 32) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL4);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 33) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "5");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 34) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL5);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 35) {
            return new FragmentIsTradeIn();
        } else if (position == 36) {
            return new FragmentUsedVehicleDisclosure();
        } else if (position == 37) {
            return new FragmentFinanceScreen();
        } else if (position == 38) {
            return new FragmentFinanceScreen();
        } else if (position == 39) {
            FragmentUsedVehicleHistoryPart1 history = new FragmentUsedVehicleHistoryPart1();
            Bundle bundle = new Bundle();
            bundle.putBoolean("first", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 40) {
            FragmentUsedVehicleHistory history = new FragmentUsedVehicleHistory();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 41) {
            FragmentUsedVehicleHistory history = new FragmentUsedVehicleHistory();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 42) {
            return new FragmentAddMoreImages();
        } else if (position == 43) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "insurance");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 44) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.INSURANCE);
            bundle.putBoolean("isAdditional", false);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 45) {
            return new FragmentCustomerHandover1();
        } else if (position == 46) {
            return new FragmentSelectLanguage();
        } else if (position == 47) {
            return new FragmentTypeOfPurchase();
        } else if (position == 48) {
            return new FragmentConfirmCarImages();
        } else if (position == 49) {
            return new FragmentConfirmVehicleDetails();
        } else if (position == 50) {
            return new FragmentTestDrive();
        } else if (position == 51) {
            return new FragmentRemoveStickers();
        } else if (position == 52) {
            FragmentUsedVehicleHistoryPart1 history = new FragmentUsedVehicleHistoryPart1();
            Bundle bundle = new Bundle();
            bundle.putBoolean("first", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 53) {
            UsedVehicleHistoryAfterSticker history = new UsedVehicleHistoryAfterSticker();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 54) {
            UsedVehicleHistoryAfterSticker history = new UsedVehicleHistoryAfterSticker();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 55) {
            return new FragmentThankYouUser1();
        } else if (position == 56) {
            return new FragmentFinalReport();
        } else if (position == 57) {
            return new FragmentReceivedPrint();
        } else if (position == 58) {
            return new FragmentCoBuyerHandover();
        } else if (position == 59) {
            return new FragmentThankYouUser2();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 60;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
//    @NonNull
//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
//        // get the tags set by FragmentPagerAdapter
//        switch (position) {
//            case 0:
//                String firstTag = createdFragment.getTag();
//                break;
//            case 1:
//                String secondTag = createdFragment.getTag();
//                break;
//        }
//        // ... save the tags somewhere so you can reference them later
//        return createdFragment;
//    }
}