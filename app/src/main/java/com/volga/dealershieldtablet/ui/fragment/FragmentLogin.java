package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.BuildConfig;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentLogin extends BaseFragment {


    private View mView;
    private ProgressDialog progressDialog;
    private static final String FRAGMENT_DIALOG = "dialog";
    private static final int REQUEST_STORAGE = 1;
    private static final int REQUEST_PHONE_STATE = 2;
    private String deviceId;
    //PERMISSION request constant, assign any value
    private static final int STORAGE_PERMISSION_CODE = 100;

    private static final String TAG = "PERMISSION_TAG";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_login, container, false);
        initView();
        return mView;
    }

    @SuppressLint("HardwareIds")
    @Override
    public void onResume() {
        super.onResume();
        if (checkPermission()){
            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
        }else {
            requestPermission();
        }
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {
            initPushLink();
        }
        else
//            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            ConfirmationDialogFragment
//                    .newInstance(R.string.phone_state_permission,
//                            new String[]{Manifest.permission.READ_PHONE_STATE},
//                            REQUEST_PHONE_STATE,
//                            R.string.phone_state_not_granted)
//                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
//        } else
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PHONE_STATE);
        }
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                == PackageManager.PERMISSION_GRANTED) {
////            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//////            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
////            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
//        } else
//            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            ConfirmationDialogFragment
//                    .newInstance(R.string.storage_permission_confirmation,
//                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            REQUEST_STORAGE,
//                            R.string.storage_permission_not_granted)
//                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
//        }
//            else {
//            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    REQUEST_STORAGE);
//            requestPermission();
//        }
    }

    private void initView() {
        final EditText emailEditText = mView.findViewById(R.id.et_email);
        final EditText passwordEditText = mView.findViewById(R.id.et_password);
        final DSTextView forgotPassword = mView.findViewById(R.id.forgotPassword);
        LinearLayout linearLayout = mView.findViewById(R.id.linearLayoutContainer);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(R.id.container, new FragmentForgotPassword(), FragmentForgotPassword.class.getName());
            }
        });
        String versionName = BuildConfig.VERSION_NAME;
        DSTextView vName = mView.findViewById(R.id.versionNumber);
        vName.setText(MessageFormat.format("Current Version: {0}", versionName));
        KeyboardUtils.showSoftKeyboard(requireActivity());
        final AppCompatButton signIn = mView.findViewById(R.id.signin);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED){
                    signIn.setEnabled(false);
                    signIn.setClickable(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            signIn.setEnabled(true);
                            signIn.setClickable(true);
                        }
                    }, 3000);
                    final String email = emailEditText.getText().toString();
                    final String password = passwordEditText.getText().toString();
                    String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                    if (email.equals("")) {
                        showPopup("Please enter your email address.");
    //                    new CustomToast(getActivity()).alert("Please enter your email address.");
                    } else if (!email.trim().matches(emailPattern)) {
                        new CustomToast(getActivity()).alert(" Please enter a valid email address.");
                    } else {
                        KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                        if (DSAPP.getInstance().isNetworkAvailable()) {
                            callLogin(email, password);
                        } else {
                            new CustomToast(getActivity()).alert(getString(R.string.connection_check));
                        }
                    }
                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_PHONE_STATE)) {
                        FragmentScanDMVFront.ConfirmationDialogFragment
                                .newInstance(R.string.phone_state_permission,
                                        new String[]{Manifest.permission.READ_PHONE_STATE},
                                        REQUEST_PHONE_STATE,
                                        R.string.phone_state_not_granted)
                                .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE},
                                REQUEST_PHONE_STATE);
                    }
                }
            }
        });
    }

    private void callLogin(final String email, String password) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.logging_in));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<LoginData> logincall = RetrofitInitialization.getDs_services().login("password", email, password);
        logincall.enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.code() == 200) {
                    LoginData loginData = response.body();
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.USER_NAME, response.body().getUserFullName().trim());
                    PreferenceManger.putString(AppConstant.USER_ROLE, response.body().getUserRole());
                    PreferenceManger.putString(AppConstant.USER_MAIL, email.trim());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
                    PreferenceManger.putString(AppConstant.LOGIN_STAMP, Calendar.getInstance().getTime().toString());
                    getDealerships();
                } else {
                    try {
                        /*remove this line when login apis are working*/
//                                    getDealerships();
                        assert response.errorBody() != null;
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Login error : ", "onResponse: " + response.errorBody().string());
                        showPopup(json.getString("error_description"));
//                        Toast.makeText(getActivity(), json.getString("error_description"), Toast.LENGTH_LONG).show();
                        if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<LoginData> call, @NonNull Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host")||t.getCause().getMessage().contains("Unable to resolve host")){
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
//                showPopup(t.getLocalizedMessage());
//                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void getDealerships() {
        Call<DealershipData> get_dealerships = RetrofitInitialization.getDs_services().get_dealerships("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        get_dealerships.enqueue(new Callback<DealershipData>() {
            @Override
            public void onResponse(Call<DealershipData> call, Response<DealershipData> response) {
                if (response.code() == 200) {
                    DealershipData dealershipData = response.body();
                    Gson gson = new GsonBuilder().create();
                    String dealershipDataString = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, dealershipDataString);
                    if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (dealershipData != null && dealershipData.getDealerships() != null && dealershipData.getDealerships().size() > 1) {
                        replaceFragment(R.id.container, new Select_Dealership(), Select_Dealership.class.getSimpleName());
                    } else if (dealershipData != null && dealershipData.getDealerships() != null && dealershipData.getDealerships().size() == 1) {
                        PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(0).getId());
                        PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(0).getDealershipName());
                        PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(0).isAllowCheckID());
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        if (getActivity() != null)
                            getActivity().finish();
                    } else {
                        showPopup("No Dealership found.");
                    }
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
//                        Toast.makeText(getActivity(), json.getString("error_description"), Toast.LENGTH_LONG).show();
                        showPopup(json.getString("error_description"));
                        Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());
                        if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<DealershipData> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host")||t.getCause().getMessage().contains("Unable to resolve host")){
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                }
                if (progressDialog != null && getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void showPopup(String textMessage) {
        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(R.layout.popup_view);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(textMessage);
        dialog.show();
        dialog.setCancelable(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 3000);
    }

    public void generateNoteOnSD(Context context, String sBody) {
        try {
            File root = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM), "/DSXT/DeviceID");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "DeviceID.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
//            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static FragmentScanDMVFront.ConfirmationDialogFragment newInstance(@StringRes int message,
                                                                                  String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            FragmentScanDMVFront.ConfirmationDialogFragment fragment = new FragmentScanDMVFront.ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .create();
        }

    }

    @SuppressLint("HardwareIds")
    private void initPushLink() {
        deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("Secure Device Id : ", deviceId);
    }

    public void requestPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            //Android is 11(R) or above
            try {
                Log.d(TAG, "requestPermission: try");

                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                storageActivityResultLauncher.launch(intent);
            }
            catch (Exception e){
                Log.e(TAG, "requestPermission: catch", e);
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                storageActivityResultLauncher.launch(intent);
            }
        }
        else {
            //Android is below 11(R)
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_CODE
            );
        }
    }

    public ActivityResultLauncher<Intent> storageActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    Log.d(TAG, "onActivityResult: ");
                    //here we will handle the result of our intent
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
                        //Android is 11(R) or above
                        if (Environment.isExternalStorageManager()){
                            //Manage External Storage Permission is granted
                            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                            generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
//                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is granted");
//                            createFolder();
                        }
                        else{
                            requestPermission();
                            //Manage External Storage Permission is denied
//                            Log.d(TAG, "onActivityResult: Manage External Storage Permission is denied");
//                            Toast.makeText(MainActivity.this, "Manage External Storage Permission is denied", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        //Android is below 11(R)
                    }
                }
            }
    );
    public boolean checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R){
            //Android is 11(R) or above
            return Environment.isExternalStorageManager();
        }
        else{
            //Android is below 11(R)
            int write = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int read = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED;
        }
    }

    /*Handle permission request results*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_CODE){
            if (grantResults.length > 0){
                //check each permission if granted or not
                boolean write = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean read = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (write && read){
                    //External Storage permissions granted
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permissions granted");
                    deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//            @SuppressLint("HardwareIds") String deviceId = MessageFormat.format("Device Id: {0}", Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                    generateNoteOnSD(getActivity(), "Device Id: " + deviceId);
                }
                else{
                    //External Storage permission denied
                    Log.d(TAG, "onRequestPermissionsResult: External Storage permission denied");
//                    Toast.makeText(this, "External Storage permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
