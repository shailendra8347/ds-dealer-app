package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyList;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class HistoryAfterStickerAdapter extends RecyclerView.Adapter<HistoryAfterStickerAdapter.TypeRow> {

    private final Activity activity;
    private ArrayList<HistoryListPojo> list;
    private boolean isHistory;

    public interface IOnLanguageSelect {
        void languageSelected(boolean allSelected, boolean isLogoClicked, String id);
    }

    IOnLanguageSelect iOnLanguageSelect;

    public HistoryAfterStickerAdapter(Activity activity, IOnLanguageSelect iOnLanguageSelect, ArrayList<HistoryListPojo> list, boolean isHistory) {
        this.activity = activity;
        this.list = list;
        this.iOnLanguageSelect = iOnLanguageSelect;
        this.isHistory = isHistory;
    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.history_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        String myFormat = "MM-dd-yyyy"; //Change as you need
        SimpleDateFormat sdf = new SimpleDateFormat(
                myFormat, Locale.getDefault());
        Gson gson = new GsonBuilder().create();
        holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        holder.checkBox.setVisibility(View.GONE);
        if (!isHistory) {
            holder.bullet.setVisibility(View.VISIBLE);
        } else {
            holder.bullet.setVisibility(View.GONE);
        }
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (list.get(position).getId().equalsIgnoreCase("15") && isHistory && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate1() != null) {
//            holder.name.setText(Html.fromHtml(String.format(activity.getString(R.string.history15), sdf.format(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate1()))));
//        } else if (list.get(position).getId().equalsIgnoreCase("16") && isHistory && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate2() != null) {
//            holder.name.setText(Html.fromHtml(String.format(activity.getString(R.string.history16), sdf.format(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getHistoryDate2()))));
//        } else {
        String append1 = "";
        boolean showDetails = false;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {
            ArrayList<ThirdPartyObj> tpList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
            for (int i = 0; i < tpList.size(); i++) {
                if (tpList.get(i).isViewReport() &&(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable()||recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable())) {
                    showDetails = true;
                    break;
                }
            }
        }
        if (list.get(position).isVerified() && !isHistory && showDetails/*&& list.get(position).isViewReport()*/) {
//
            append1 = "<br/><font color=\"#484747\">" + activity.getString(R.string.see_thirdparty) + "</font>";
        }
        if (list.get(position).getName() != null) {
            holder.name.setText(Html.fromHtml(list.get(position).getName() + append1));
//            if (list.get(position).isVerified())
//            holder.name.setText(Html.fromHtml(list.get(position).getName()+"\n See third party reports for details"));
//            else{
//                holder.name.setText(Html.fromHtml(list.get(position).getName()));
//            }
        } else {
            holder.name.setText(" ");
        }
//        }
        holder.checkBox.setOnCheckedChangeListener(null);
//        holder.checkBox.setChecked(list.get(position).isChecked());
        if (list.get(position).isChecked()) {
            holder.checkBox.setChecked(true);
            holder.name.setTextColor(activity.getResources().getColor(R.color.button_color_blue));
        } else {
            holder.checkBox.setChecked(false);
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                checkChangeLocal(position, b, false, holder);

            }
        });
        if (isHistory) {
            if (list.get(position).isViewReport() && PreferenceManger.getBooleanValue(AppConstant.USE_THIRD_PARTY_API)) {
                holder.logo.setVisibility(View.VISIBLE);
                holder.logoView.setVisibility(View.VISIBLE);
                RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
                Glide.with(activity)
                        .load(getLogoById(list.get(position).getId()))
                        .apply(requestOptions)
                        .into(holder.logo);
                holder.name.setVisibility(View.GONE);
                holder.bullet.setVisibility(View.GONE);
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        if (holder.checkBox.isChecked()) {
//                            holder.checkBox.setChecked(false);
//                            checkChangeLocal(positioin, false,true);
//                        } else {
                        holder.view.setClickable(false);
                        holder.view.setEnabled(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                holder.view.setClickable(true);
                                holder.view.setEnabled(true);
                            }
                        }, 3000);
                        holder.checkBox.setChecked(!list.get(position).isMandatory());
                        checkChangeLocal(position, true, true, holder);
//                        }
//                        checkChangeLocal(position, false,true);
                    }
                });

                if (!list.get(position).isMandatory()) {
                    holder.checkBox.setEnabled(true);
                    holder.mandatory.setVisibility(View.GONE);
                } else {
//                    holder.checkBox.setEnabled(true);
                    /*Changed in 1.3.1*/
//                     holder.mandatory.setVisibility(View.VISIBLE);
                }
                holder.mandatory.setVisibility(View.GONE);

                if (list.get(position).isReportSeen()) {
                    holder.finger.setVisibility(View.GONE);
                    holder.view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new CustomToast(activity).alert(activity.getString(R.string.already_signed));
                        }
                    });
//                    holder.checkBox.setEnabled(false);
//                    holder.view.setEnabled(false);
                    holder.view.setText(R.string.button_signed);
                    holder.view.setBackground(activity.getDrawable(R.drawable.rounded_corner_gray));
                    holder.mandatory.setVisibility(View.GONE);
                    holder.checkBox.setChecked(true);
                } else {
                    holder.finger.setVisibility(View.VISIBLE);
                    holder.view.setEnabled(true);
                    if (list.get(position).isMandatory()) {
                        holder.checkBox.setChecked(true);
                        holder.view.setText(R.string.click_to_sign);
                        holder.view.setBackground(activity.getDrawable(R.drawable.rounded_corner_red));
                    } else {
                        holder.view.setBackground(activity.getDrawable(R.drawable.rounded_corner_gray));
                        holder.view.setText(R.string.view_button);
                    }

//                    holder.checkBox.setEnabled(true);
                    holder.checkBox.setChecked(list.get(position).isChecked());
                }
            } else {
                holder.name.setVisibility(View.VISIBLE);
                holder.bullet.setVisibility(View.VISIBLE);
                holder.logo.setVisibility(View.GONE);
                holder.logoView.setVisibility(View.GONE);
            }
        } else {
            holder.logo.setVisibility(View.GONE);
        }
//        holder.name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.checkBox.isChecked()) {
//                    holder.checkBox.setChecked(false);
//                    checkChangeLocal(position, false, false, holder);
//                } else {
//                    holder.checkBox.setChecked(true);
//                    checkChangeLocal(position, true, false, holder);
//                }
//            }
//        });
//        holder.checkBox.setChecked(true);
//        holder.checkBox.setEnabled(false);

    }

    private void checkChangeLocal(int position, boolean b, boolean islogo, TypeRow holder) {
        if (b)
            holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        else {
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        }
//        if (!list.get(position).isMandatory()) {
//            list.get(position).setChecked(b);
//        } else {
        list.get(position).setChecked(false);
//        }
        boolean isAllSelected = false;
        if (!isHistory) {
            AppConstant.history.get(position).setChecked(b);
            for (int i = 0; i < AppConstant.history.size(); i++) {
                if (!AppConstant.history.get(i).isChecked()) {
                    isAllSelected = AppConstant.history.get(i).isChecked();
                    break;
                }
                isAllSelected = AppConstant.history.get(i).isChecked();
            }
        } else {
//            if (!list.get(position).isMandatory())
            AppConstant.report.get(position).setChecked(b);
//            else {
//                AppConstant.report.get(position).setChecked(false);
//            }
            for (int i = 0; i < AppConstant.report.size(); i++) {

                if (AppConstant.report.get(i).isMandatory() && AppConstant.report.get(i).isViewReport()) {
                    if (!AppConstant.report.get(i).isChecked() || !AppConstant.report.get(i).isReportSeen()) {
                        isAllSelected = false;
                        break;
                    }
                } else {
                    if (!AppConstant.report.get(i).isChecked()) {
                        isAllSelected = AppConstant.report.get(i).isChecked();
                        break;
                    }
                }
                isAllSelected = AppConstant.report.get(i).isChecked();
            }
        }
        iOnLanguageSelect.languageSelected(isAllSelected, islogo, list.get(position).getId());
    }

    private int mYear, mMonth, mDay;


    @Override
    public int getItemCount() {
        return list.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name, view, mandatory, bullet;
        CheckBox checkBox;
        ImageView logo, finger;
        LinearLayout logoView;

        TypeRow(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            bullet = itemView.findViewById(R.id.bulletPoint);
            name = itemView.findViewById(R.id.name);
            view = itemView.findViewById(R.id.view);
            mandatory = itemView.findViewById(R.id.mandatory);
            logo = itemView.findViewById(R.id.logo);
            finger = itemView.findViewById(R.id.finger);
            logoView = itemView.findViewById(R.id.logoBox);
        }
    }

    private String getLogoById(String id) {
        Gson gson = new GsonBuilder().create();
        String url = "";
        ThirdPartyList thirdPartyList = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTY_REPORT), ThirdPartyList.class);
        if (thirdPartyList != null && thirdPartyList.getValue() != null && thirdPartyList.getValue().size() > 0) {
            for (int i = 0; i < thirdPartyList.getValue().size(); i++) {
                if (thirdPartyList.getValue().get(i).getId().equalsIgnoreCase(id)) {
                    if (thirdPartyList.getValue().get(i).getLogoUrl() == null) {
                        return url;
                    } else
                        return thirdPartyList.getValue().get(i).getLogoUrl();
                }

            }
        }
//        ThirdPartyListObject thirdPartyListObject = gson.fromJson(PreferenceManger.getStringValue(AppConstant.THIRD_PARTIES), ThirdPartyListObject.class);
//        if (thirdPartyListObject != null) {
//            ArrayList<DealershipThirdParties> dealershipThirdParties = new ArrayList<>(thirdPartyListObject.getDealershipThirdParties());
//            if (dealershipThirdParties.size() > 0) {
//                for (int i = 0; i < dealershipThirdParties.size(); i++) {
//                    if (dealershipThirdParties.get(i).getId().equalsIgnoreCase(id)) {
//                        return dealershipThirdParties.get(i).getThirdPartyHistoryReport().getLogoUrl();
//                    }
//
//                }
//            }
//        }
        return "";
    }
}
