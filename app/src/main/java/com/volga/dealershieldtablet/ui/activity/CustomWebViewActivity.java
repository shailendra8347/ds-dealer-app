package com.volga.dealershieldtablet.ui.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pdfview.PDFView;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.TreeMap;

public class CustomWebViewActivity extends BaseActivity2 {
    private DSTextView text_next, text_back, proceed;
    private String redirectURL = "http://stg.dealerxt.com/Resource/ThirdPartyReportViewed?docId=123440";
    private boolean isCoBuyer;
    private boolean isSigned;
    private File svgFile;
    private SignaturePad signaturePad;
    private DSTextView sign;
    private ImageView delete;
    private boolean isTaken;
    private File file;
    private boolean runningSign;

    @Override
    public void onBackPressed() {
    }

    public String getCurrentTimeString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.getDefault());
        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.getDefault());
        SimpleDateFormat outputFormatTime = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a zzz", Locale.getDefault());
//        Date dateValue = new Date(Calendar.getInstance().getTime().toString());
//        TimeZone pstTimeZone = TimeZone.getTimeZone("America/Los_Angeles");
//        outputFormatTime.setTimeZone(pstTimeZone);
        outputFormatTime.setTimeZone(TimeZone.getTimeZone("PST"));
//        outputFormatTime.setTimeZone(TimeZone.getDefault());
        Date parsedDate = null;
        //            parsedDate = dateFormat.parse(dateValue.toString());
        long longs = TimeZoneUtils.convertTime(Calendar.getInstance().getTimeInMillis(), TimeZone.getTimeZone("UTC"), TimeZone.getTimeZone("America/Los_Angeles"));
        String returnDate = outputFormatTime.format(longs);

        returnDate = returnDate.replace("a. m.", "AM");
        returnDate = returnDate.replace("a.m.", "AM");
        returnDate = returnDate.replace("p. m.", "PM");
        returnDate = returnDate.replace("p.m.", "PM");
        returnDate = returnDate.replace("GMT", "PST");
        returnDate = returnDate.replace("PDT", "PST");
        returnDate = returnDate.replace("+00:00", "");
        return returnDate;


//        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a zzz", Locale.getDefault());
//        /*yyyy-MM-dd HH:mm:ss*/
//        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
//        String time = "";
//        try {
//            Log.e("Current Time: ", tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString())));
//            time = tradeTimeFormatter.format(formatter.parse(Calendar.getInstance().getTime().toString()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return time;
//        return "";
    }

    //    @SuppressLint("StringFormatInvalid")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_webview);
        final PDFView pdfView = findViewById(R.id.activity_main_pdf_view);

        proceed = findViewById(R.id.text_next);
        disableWithGray(proceed);
        DSTextView signText = findViewById(R.id.signtext);
        sign = findViewById(R.id.signhere);
        delete = findViewById(R.id.deleteButton);
        disableDelete(delete);
        LinearLayout boxSign = findViewById(R.id.boxSign);
        final DSTextView dynamicString = findViewById(R.id.dynamicString);

        final String path = getIntent().getStringExtra("url");

        Gson gson = new GsonBuilder().create();
        String id = getIntent().getStringExtra("id");
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        isCoBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();
        int langId = 1;
        if (getIntent().getBooleanExtra("customerResponse", false)) {
            if (!isCoBuyer)
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
            else {
                langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
            }

        }
        if (langId == 2 || langId > 3 || langId == 0) {
            langId = 1;
        }
        if (getIntent().getBooleanExtra("showBox", false)) {
            proceed.setVisibility(View.VISIBLE);
            signText.setVisibility(View.VISIBLE);
            boxSign.setVisibility(View.VISIBLE);
        } else {
            proceed.setVisibility(View.GONE);
            signText.setVisibility(View.GONE);
            boxSign.setVisibility(View.GONE);
        }
        String appendingString = "1", reportFeatchTime = null;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
            ArrayList<InitThirdParty> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getThirdPartyID().equalsIgnoreCase(id) && list.get(i).getLanguageId().equalsIgnoreCase(langId + "") && !list.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                    appendingString = list.get(i).getThirdPartyReportRawId();
                    reportFeatchTime = list.get(i).getReportFetchDateTimeUTC();

                }
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        SimpleDateFormat outputFormatTime = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.getDefault());
        Date reportDate = null;
        String returnDate = null;
        try {
            reportDate = sdf.parse(reportFeatchTime);
            long longs = TimeZoneUtils.convertTime(reportDate.getTime(), TimeZone.getTimeZone("UTC"), TimeZone.getTimeZone("America/Los_Angeles"));
            returnDate = outputFormatTime.format(longs);
            returnDate = returnDate + " PST";
//            returnDate = returnDate.replace("a. m.", "AM");
//            returnDate = returnDate.replace("p.m.", "PM");
//            returnDate = returnDate.replace("a.m.", "AM");
//            returnDate = returnDate.replace("p. m.", "PM");
//            returnDate = returnDate.replace("GMT", "PST");
//            returnDate = returnDate.replace("PDT", "PST");
//            returnDate = returnDate.replace("+00:00", "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        File file;
        file = ShowImageFromPath(path + "" + appendingString + ".pdf", "localPdf");
        if (langId != 1) {
            if (file != null && !file.exists()) {
                file = ShowImageFromPath(path + appendingString + ".pdf", "localPdf");
            }
        }
        if (file != null && file.length() > 0)
            pdfView.fromFile(file).show();


//        text_next = findViewById(R.id.text_next);
        text_back = findViewById(R.id.txt_logout);

        text_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(CustomWebViewActivity.this)
                        .setMessage(R.string.are_you_sure)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();
            }

//            }
        });
        final String firstString, secondString, thirdString, fourthString;
        if (id.equalsIgnoreCase("1")) {
            firstString = "CARFAX";
        } else {
            firstString = "AutoCheck";
        }
        final VehicleDetails vehicleDetail = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails();
        secondString = vehicleDetail.getCarYear() + " " + vehicleDetail.getCarMake() + " " + vehicleDetail.getCarModel();

        dynamicString.setText(String.format(Locale.getDefault(), getResources().getString(R.string.dynamic_third_party_disclosure), firstString, secondString, vehicleDetail.getVINNumber(), firstString, returnDate));
        proceed.setVisibility(View.VISIBLE);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSigned) {
                    proceed.setVisibility(View.VISIBLE);
                    int index = 0;
                    for (int i = 0; i < AppConstant.report.size(); i++) {
                        if (AppConstant.report.get(i).getId().equalsIgnoreCase(getIntent().getStringExtra("id"))) {
                            index = i;
                            break;
                        }
                    }
                    AppConstant.report.get(index).setReportSeen(true);
                    AppConstant.report.get(index).setChecked(true);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
                    if (isCoBuyer) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoReportSelection(AppConstant.report);
                    } else {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setReportSelection(AppConstant.report);
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    addSvgSignatureToGallery(signaturePad.getSignatureSvg(), id);
                    finish();
                } else {
                    new CustomToast(CustomWebViewActivity.this).alert("Please sign document.");
                }
            }
        });
//        pdfView.setVerticalScrollBarEnabled(true);
//        final ProgressBar progressBar = findViewById(R.id.progressBar);
//        progressBar.setVisibility(View.GONE);
        final ProgressDialog progressBar = new ProgressDialog(CustomWebViewActivity.this);
        progressBar.setMessage("Loading...");
        progressBar.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.dismiss();
            }
        }, 2500);
        final DSTextView signature = findViewById(R.id.signature);
        findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                runningSign = false;
                disableWithGray(proceed);
                disableDelete(delete);
//                proceed.setVisibility(View.GONE);
            }
        });
        signaturePad = (SignaturePad) findViewById(R.id.signaturePad);
//        signaturePad.setOnClickListener(v -> {
//            isSigned = false;
//            runningSign = false;
//        });
        findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                runningSign = false;
//                proceed.setVisibility(View.GONE);
            }
        });

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                proceed.setVisibility(View.VISIBLE);
                sign.setVisibility(View.GONE);
//                enableWithGreen(proceed);
                enableDelete(delete);


                if (!runningSign) {
                    runningSign = true;
                    File file;
                    if (!isCoBuyer) {
                        file = getOutputFile("UserPic", "third_party_sign_" + firstString + "_Report_pic");
                    } else {
                        file = getOutputFile("UserPic", "co_buyer_third_party_sign_" + firstString + "_Report_pic");
                    }
                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    }else{
                        proceed.setVisibility(View.VISIBLE);
                        enableWithGreen(proceed);
                    }
                }


//                dynamicString.setText(String.format(Locale.getDefault(), getResources().getString(R.string.dynamic_third_party_disclosure), firstString, secondString, vehicleDetail.getVINNumber(), firstString, getCurrentTimeString()));
//                Handler mainHandler = new Handler(DSAPP.getInstance().getMainLooper());
//
//                Runnable myRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        File file;
//                        if (!isCoBuyer) {
//                            file = getOutputFile("UserPic", "no_test_drive_screen_pic");
//                        } else {
//                            file = getOutputFile("UserPic", "co_no_test_drive_screen_pic");
//                        }
//                        if (!isTaken) {
//                            isTaken=true;
//                            takeUserPicture(file);
//                        }
//                    } // This is your code
//                };
//                mainHandler.post(myRunnable);
            }

            @Override
            public void onSigned() {
                isSigned = true;

                if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isSigned) {
                                proceed.setVisibility(View.VISIBLE);
                                enableWithGreen(proceed);
                            }
                        }
                    }, AppConstant.NEXT_DELAY);
                } else  if (isTaken) {
                    proceed.setVisibility(View.VISIBLE);
                    enableWithGreen(proceed);
                }
            }

            @Override
            public void onClear() {
//                proceed.setVisibility(View.GONE);
                disableWithGray(proceed);
                disableDelete(delete);
                isSigned = false;
                runningSign=false;
                sign.setVisibility(View.VISIBLE);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        runningSign = false;
//                        proceed.setVisibility(View.GONE);
                        return true;
                    }
                }
                return true;
            }
        });
    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(DSAPP.getInstance())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    public File getOutputFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public boolean addSvgSignatureToGallery(String signatureSvg, String id) {
        boolean result = false;
        try {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                svgFile = getOutputMediaFile("Signatures", "co_buyer_third_party_sign_" + id.trim().replace(" ", "_"));//autocheck_report carfax
            else {
                svgFile = getOutputMediaFile("Signatures", "third_party_sign_" + id.trim().replace(" ", "_"));
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    @Override
    public void onResume() {
        super.onResume();
        isSigned = false;
        registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("photoCaptured", false)) {
                isTaken = true;
                if (isSigned) {
                    proceed.setVisibility(View.VISIBLE);
                    enableWithGreen(proceed);
                }
            }
        }
    };
    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
//            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(CustomWebViewActivity.this);
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (isSigned) {
                                    proceed.setVisibility(View.VISIBLE);
                                    enableWithGreen(proceed);
                                }
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (isSigned) {
                                        proceed.setVisibility(View.VISIBLE);
                                        enableWithGreen(proceed);
                                    }
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(CustomWebViewActivity.this, CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        File mediaStorageDir;
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void disableDelete(ImageView delete) {
        delete.setImageResource(R.drawable.ic_delete_black_24dp);
//        delete.setClickable(false);
//        delete.setEnabled(false);
//        delete.setOnClickListener(null);
    }

    public void enableDelete(ImageView delete) {
        delete.setImageResource(R.drawable.delete_red);
//        delete.setClickable(true);
//        delete.setEnabled(true);

    }

    public void disableWithGray(DSTextView next) {
        next.setBackground(getDrawable(R.drawable.rounded_corner_next_gray));
        next.setClickable(false);
        next.setPadding(0, 15, 0, 15);
        next.setEnabled(false);

    }

    public void enableWithGreen(DSTextView next) {
        next.setBackground(getDrawable(R.drawable.rounded_corner_green));
        next.setPadding(0, 15, 0, 15);
        next.setClickable(true);
        next.setEnabled(true);
    }
    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
        String path_licence, path_signature, path_car, path_user_pic, path_screenshots;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
        path_user_pic = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/UserPic";
        path_screenshots = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Screenshot";


        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        }

        /*Car Images*/
        if (new File(path_car + "/IMG_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();

            synced.setName("FRONTIMG");
            synced.setFile(new File(path_car + "/IMG_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        }
        if (new File(path_car + "/IMG_right.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("RIGHTIMG");
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_right.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        }
        if (new File(path_car + "/IMG_rear.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("REARIMG");
            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        }
        if (new File(path_car + "/IMG_left.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LEFTIMG");
            synced.setFile(new File(path_car + "/IMG_left.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        }
        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("WINDOWWSTICKERIMG");
            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);

//            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        }
        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("MILEAGEIMG");
            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TRADEINMILEAGE");
            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        }
        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LICENCEPLATENUMBER");
            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/

        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("Customer_Insurance");
            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
            synced.setName("language_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_1.svg"));
            synced.setName("third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg"));
            synced.setName("co_buyer_third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_2.svg"));
            synced.setName("third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg"));
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
            synced.setName("type_of_purchase_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
            synced.setName("confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
            synced.setName("test_drive_taken_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
            synced.setName("no_test_drive_confirm_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
            synced.setName("remove_stickers_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
            synced.setName("CONTACTINFOSIGNATUREIMG");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        }
        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
            synced.setName("prints_recieved_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure");
            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
            synced.setName("history_report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        }
        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure");
            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_language_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_type_of_purchase_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
            synced.setName("co_buyer_confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_test_drive_taken_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_no_test_drive_confirm_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_remove_stickers_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_contact_info");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_prints_recieved_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
            synced.setName("co_buyer_condition_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {

            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_history_report");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
            synced.setName("co_buyer_history_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_no_verble_promise.svg"));
            synced.setName("buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg"));
            synced.setName("co_buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_checklist.svg"));
            synced.setName("co_buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_checklist.svg"));
            synced.setName("buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_DLFRONT");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("DRIVERLICENSEIMG");
            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TradeInFront");
            synced.setFile(new File(path_car + "/IMG_trade_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }

        /*Buyers screenshots*/
        if (new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_co_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_safety_recall.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_safety_recall");
            synced.setFile(new File(path_screenshots + "/IMG_alert_safety_recall.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_warning_disclosures");
            synced.setFile(new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_vehicle_info_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_co_buyer_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        if (new File(path_screenshots + "/IMG_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers screenshots*/

        if (new File(path_screenshots + "/IMG_co_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_co_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        /*Buyers pictures*/

        if (new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }if (new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers picture*/

        if (new File(path_user_pic + "/IMG_co_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        return imagesSynceds;
    }
}
