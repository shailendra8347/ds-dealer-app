package com.volga.dealershieldtablet.ui.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.ui.fragment.FragmentConfirmTestDrive;
import com.volga.dealershieldtablet.ui.fragment.FragmentDealershipHandover;
import com.volga.dealershieldtablet.ui.fragment.FragmentESignContactInfo;
import com.volga.dealershieldtablet.ui.fragment.FragmentTestDriveMandetory;
import com.volga.dealershieldtablet.ui.tradeIn.FragmentSalesPerson;
import com.volga.dealershieldtablet.utils.AppConstant;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class PopUpContainerActivity extends BaseActivity2 {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        if (getIntent().getStringExtra(AppConstant.FROM_FRAGMENT).equalsIgnoreCase("handover")) {
            FragmentDealershipHandover fragmentDealershipHandover = new FragmentDealershipHandover();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isFromPrints", false);
            fragmentDealershipHandover.setArguments(bundle);
            callFragment(R.id.container, fragmentDealershipHandover, fragmentDealershipHandover.getClass().getSimpleName());
        } else if (getIntent().getStringExtra(AppConstant.FROM_FRAGMENT).equalsIgnoreCase("testDrive")) {
            FragmentTestDriveMandetory fragmentDealershipHandover = new FragmentTestDriveMandetory();
//            Bundle bundle = new Bundle();confirmTestDrive
//            bundle.putBoolean("isFromPrints", false);
//            fragmentDealershipHandover.setArguments(bundle);
            callFragment(R.id.container, fragmentDealershipHandover, fragmentDealershipHandover.getClass().getSimpleName());
        } else if (getIntent().getStringExtra(AppConstant.FROM_FRAGMENT).equalsIgnoreCase("confirmTestDrive")) {
            FragmentConfirmTestDrive fragmentDealershipHandover = new FragmentConfirmTestDrive();
//            Bundle bundle = new Bundle();confirmTestDrive
//            bundle.putBoolean("isFromPrints", false);
//            fragmentDealershipHandover.setArguments(bundle);
            callFragment(R.id.container, fragmentDealershipHandover, fragmentDealershipHandover.getClass().getSimpleName());
        } else if (getIntent().getStringExtra(AppConstant.FROM_FRAGMENT).equalsIgnoreCase("handover1")) {

            FragmentDealershipHandover fragmentDealershipHandover = new FragmentDealershipHandover();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isFromPrints", true);
            fragmentDealershipHandover.setArguments(bundle);
            callFragment(R.id.container, fragmentDealershipHandover, fragmentDealershipHandover.getClass().getSimpleName());
        } else if (getIntent().getStringExtra(AppConstant.FROM_FRAGMENT).equalsIgnoreCase("addsalesperson")) {

            FragmentSalesPerson fragmentSalesPerson = new FragmentSalesPerson();
            Bundle bundle = new Bundle();
            fragmentSalesPerson.setArguments(bundle);
            callFragment(R.id.container, fragmentSalesPerson, fragmentSalesPerson.getClass().getSimpleName());
        }
        else if (getIntent().getStringExtra(AppConstant.FROM_FRAGMENT).equalsIgnoreCase("esignContact")) {

            FragmentESignContactInfo fragmentSalesPerson = new FragmentESignContactInfo();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isCobuyer",getIntent().getBooleanExtra("isCobuyer",false));
            bundle.putBoolean("hasCobuyer",getIntent().getBooleanExtra("hasCobuyer",false));
            fragmentSalesPerson.setArguments(bundle);
            callFragment(R.id.container, fragmentSalesPerson, fragmentSalesPerson.getClass().getSimpleName());
        }

    }
}
