package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class DealerShipTypeAdapter extends RecyclerView.Adapter<DealerShipTypeAdapter.TypeRow> {

    private final Activity activity;
    private final DealershipData dealershipData;
    private DealershipSelected dealershipSelected;

    public interface DealershipSelected {
        void onSelected(int pos);
    }

    public DealerShipTypeAdapter(Activity activity, DealershipData dealershipData, DealershipSelected dealershipSelected) {

        this.activity = activity;
        this.dealershipData = dealershipData;
        this.dealershipSelected = dealershipSelected;
    }

    @Override
    public TypeRow onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.dealership_type_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
//        holder.name.setText(placesArrayList.get(position).getName());
        holder.name.setText(dealershipData.getDealerships().get(position).getDealershipName());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.name.setEnabled(false);
                holder.name.setClickable(false);
               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       holder.name.setEnabled(true);
                       holder.name.setClickable(true);
                   }
               },3000);
//                addFragment(R.id.container,new FragmentScanDMVFront(),FragmentScanDMVFront.class.getCanonicalName());
                dealershipSelected.onSelected(position);
                PreferenceManger.putInt(AppConstant.SELECTED_DEALERSHIP_ID, dealershipData.getDealerships().get(position).getId());
                PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(position).getDealershipName());
                PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(position).isAllowCheckID());
                PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(position).isCaptureCustomerPhotoOnSign());
                PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(position).isCaptureScreenOnSign());
                PreferenceManger.putBoolean(AppConstant.IS_AI_ENABLED, dealershipData.getDealerships().get(position).isAIFeatureEnabled());
                PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(position).isUsedCarOnly());
                PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(position).isProcessTradeIn());
                PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(position).isTakePhotoOfProofOfInsurance());
                PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(position).isTakePhotoOfVehicle());
                PreferenceManger.putBoolean(AppConstant.PROCESS_MULTI_SIGN, dealershipData.getDealerships().get(position).isThirdPartyMultiSign());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dealershipData.getDealerships().size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name;

        public TypeRow(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
        }
    }
}
