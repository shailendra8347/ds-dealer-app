package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.scanner.PartialViewContainerActivity;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class FragmentScanDMVBack extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_processing, container, false);
    }

    @Override
    public void setUserVisibleHint(final boolean isVisibleToUser) {
        if (isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(Objects.requireNonNull(getActivity()), PartialViewContainerActivity.class);
                    Bundle b = new Bundle();
                    b.putBoolean("isVinScanner", false);
                    b.putBoolean("isDMVScanner", true);
                    b.putBoolean("checkId", getArguments().getBoolean("checkId"));
                    intent.putExtras(b);
                    Objects.requireNonNull(getActivity()).startActivity(intent);
                }
            }, 200);
        }
        super.setUserVisibleHint(isVisibleToUser);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (PartialViewContainerActivity.scanningDone) {
            PartialViewContainerActivity.scanningDone = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    iGoToNextPage.whatNextClick();
                }
            }, 300);
        }
        if (PartialViewContainerActivity.skipped) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    iGoToNextPage.whatNextClick();
                }
            }, 300);
        }
        if (PartialViewContainerActivity.logout) {
            PartialViewContainerActivity.logout = false;
            logout();
        }
        if (PartialViewContainerActivity.back) {
            PartialViewContainerActivity.back = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn())
                    iGoToNextPage.goToBackIndex();
                }
            }, 300);
        }
//        if (PartialViewContainerActivity.tradeInBack) {
//            PartialViewContainerActivity.tradeInBack = false;
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    ((ViewPagerActivity) getActivity()).setCurrentPage(36);
//                    iGoToNextPage.goToBackIndex();
//                }
//            }, 300);
//        }

    }

    IGoToNextPage iGoToNextPage;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    public void goBack() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    public void cancel() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(NewDealViewPager.currentPage);
        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        String recordDataString = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        callHomeActivity();
//        getActivity().getSupportFragmentManager().popBackStack();
    }


    public void logout() {
        showAlertDialog();
    }

    private void logoutInBase() {
        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
        Locale myLocale = new Locale("en");
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        callHomeActivity();
    }

    private void callHomeActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }


    private void showAlertDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.custom_alert_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);

        DSTextView btnAdd1 = promptView.findViewById(R.id.yes);

        DSTextView btnAdd2 = promptView.findViewById(R.id.no);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                logoutInBase();
            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.dismiss();
            }
        });

        alertD.setView(promptView);

        alertD.show();
    }
}
