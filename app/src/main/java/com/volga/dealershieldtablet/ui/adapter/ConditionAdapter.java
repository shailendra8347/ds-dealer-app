package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosure;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.Value;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.WarningAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.function.Predicate;

import timber.log.Timber;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class ConditionAdapter extends RecyclerView.Adapter<ConditionAdapter.TypeRow> {

    private final Activity activity;
    private ArrayList<HistoryListPojo> list;
    private boolean first;
    String myFormat = "MM-dd-yyyy"; //Change as you need
    SimpleDateFormat sdf = new SimpleDateFormat(
            myFormat, Locale.getDefault());
    private int allSelected = 0;
    private boolean disableNone = false;

    public interface IOnLanguageSelect {
        void languageSelected(boolean allSelected, boolean isCancel, boolean isOK);
    }

    private IOnLanguageSelect iOnLanguageSelect;

    public ConditionAdapter(Activity activity, IOnLanguageSelect iOnLanguageSelect, ArrayList<HistoryListPojo> list, boolean isHistory) {
        this.activity = activity;
        this.list = list;
        this.iOnLanguageSelect = iOnLanguageSelect;
        this.first = isHistory;
    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.history_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        Gson gson = new GsonBuilder().create();

        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String append = "", append1 = "";
        boolean showDetails = false;
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue() != null) {
                ArrayList<ThirdPartyObj> tpList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
                for (int i = 0; i < tpList.size(); i++) {
                    if (tpList.get(i).isViewReport()&&(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isAutoCheckReportAvailable()||recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isCarFaxReportAvailable())) {
                        showDetails = true;
                        break;
                    }
                }
            }
        }
        if (first) {
            if (list.get(position).isVerified() && position != list.size() - 1) {
                append1 = "<br/><font color=\"#484747\">" + activity.getString(R.string.see_thirdparty) + "</font>";
            }
        } else {
            if (list.get(position).isVerified() && showDetails/*&& list.get(position).isViewReport()*/) {
                append1 = "<br/><font color=\"#484747\">" + activity.getString(R.string.see_thirdparty) + "</font>";
            }
        }
        if (list.get(position).getAdditionalData() != null && list.get(position).getAdditionalData().length() > 0) {
            append = ": <font color=\"#F54337\">" + list.get(position).getAdditionalData() + "</font>";
        }
        holder.name.setText(Html.fromHtml(list.get(position).getName() + append + append1));
        if (first) {
            if (position == list.size() - 1) {
                holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
            } else {
                holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            }
        }else{
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        }
        holder.checkBox.setOnCheckedChangeListener(null);
        if (first) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.bullet.setVisibility(View.GONE);
            if (list.get(position).isChecked() && list.get(position).getManualAdditionalDisclosures() != null && list.get(position).getManualAdditionalDisclosures().size() > 0)
                autoSelectDependentIds(position, list, true, false);
            if (list.get(position).isVerified()) {
                if (position == list.size() - 1) {
                    holder.checkBox.setChecked(false);
                    AppConstant.commonList.get(position).setChecked(false);
                } else {
                    holder.checkBox.setChecked(true);
                    AppConstant.commonList.get(position).setChecked(true);
                }
                holder.checkBox.setEnabled(false);
            } else {
                holder.checkBox.setEnabled(true);
                holder.checkBox.setChecked(list.get(position).isChecked());
            }
//            if (!list.get(position).isVerified()) {
            if ((position < list.size() - 1)) {
                holder.name.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onClick(View v) {
                        autoSelectDependentIds(position, list, true, true);
                        if (holder.checkBox.isChecked()) {
                            showEditDialog(position, false);
                        }
                        holder.checkBox.setChecked(true);
                        list.get(position).setChecked(true);

                        AppConstant.commonList.get(position).setChecked(true);
//                        showEditDialog(position);
                        KeyboardUtils.hideSoftKeyboard(holder.name, activity);

                        //                        uncheckAll();
                    }
                });
            } else {
                holder.name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!holder.checkBox.isChecked()) {
                            holder.checkBox.setChecked(true);
                            list.get(position).setChecked(true);
                            AppConstant.commonList.get(position).setChecked(true);
                        } else {
                            holder.checkBox.setChecked(false);
                            list.get(position).setChecked(false);
                            AppConstant.commonList.get(position).setChecked(false);
                        }
                        KeyboardUtils.hideSoftKeyboard(holder.name, activity);
                        notifyDataSetChanged();
                    }
                });
            }
//            } else {
//                holder.name.setOnClickListener(null);
//            }
        } else {
            holder.checkBox.setVisibility(View.GONE);
            holder.bullet.setVisibility(View.VISIBLE);
            if (list.get(position).isChecked()) {
                holder.checkBox.setChecked(true);
                holder.name.setTextColor(activity.getResources().getColor(R.color.button_color_blue));
            }else {
                holder.checkBox.setChecked(false);
                holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
            }
//            holder.checkBox.setChecked(list.get(position).isChecked());
            /*Change checkbox to bullet point*/

//            holder.name.setOnClickListener(new View.OnClickListener() {
//                @RequiresApi(api = Build.VERSION_CODES.N)
//                @Override
//                public void onClick(View v) {
//                    if (holder.checkBox.isChecked()) {
//                        checkChangedLocal(position, false,holder);
//                        holder.checkBox.setChecked(false);
//                    } else {
//                        checkChangedLocal(position, true,holder);
//                        holder.checkBox.setChecked(true);
//                    }
//                }
//            });
        }
//        if (first) {
//            holder.checkBox.setEnabled(true);
//            if (list.get(position).getAdditionalData() != null && list.get(position).getAdditionalData().length() > 0) {
//                holder.name.setSingleLine(true);
//            } else {
//                holder.name.setSingleLine(false);
//            }
//        }
//        else {
//            holder.checkBox.setChecked(true);
//            holder.checkBox.setEnabled(false);
//        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                checkChangedLocal(position, b,holder);

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void autoSelectDependentIds(int position, ArrayList<HistoryListPojo> list, boolean b, boolean notify) {
        if (b) {
            if (list.get(position).getDependentIds() != null && list.get(position).getDependentIds().size() > 0) {
                for (int i = 0; i < list.get(position).getDependentIds().size(); i++) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(position).getDependentIds().get(i).equalsIgnoreCase(list.get(j).getId())) {
                            list.get(j).setChecked(true);
                            AppConstant.commonList.get(j).setChecked(true);
                        }
                    }
                }
            }
        }
        //Add manual additional disclosure
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        AdditionalDisclosure additionalDisclosure =  new AdditionalDisclosure();
        try {
            additionalDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getAdditionalDisclosure();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (b) {
            if (additionalDisclosure != null && additionalDisclosure.getValue() != null && additionalDisclosure.getValue().size() > 0) {
                if (list.get(position).getManualAdditionalDisclosures() != null && list.get(position).getManualAdditionalDisclosures().size() > 0) {
//                boolean isDuplicate = checkDuplicateAdditionalItem(list.get(position).getManualAdditionalDisclosures(), additionalDisclosure);
                    for (int i = 0; i < list.get(position).getManualAdditionalDisclosures().size(); i++) {
                        if (additionalDisclosure.getValue() != null && additionalDisclosure.getValue().size() > 0)
                            for (int j = 0; j < additionalDisclosure.getValue().size(); j++) {
                                boolean isDuplicate = checkDuplicateAdditionalItem(list.get(position).getManualAdditionalDisclosures().get(i).getId(), additionalDisclosure.getValue());
                                if (!isDuplicate) {
                                    Value value = new Value();
                                    value.setId(list.get(position).getManualAdditionalDisclosures().get(i).getId());
                                    value.setManual(true);
                                    value.setTitle(list.get(position).getManualAdditionalDisclosures().get(i).getTitle());
                                    value.setTranslations(list.get(position).getManualAdditionalDisclosures().get(i).getTranslations());
                                    additionalDisclosure.getValue().add(value);
                                    Timber.e("List size after: " + additionalDisclosure.getValue().size());
                                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(additionalDisclosure);
                                    String recordDataString = gson.toJson(recordData);
                                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                                }
                            }
                    }
                }
            } else {
                AdditionalDisclosure additionalDisclosure1 = new AdditionalDisclosure();
                if (list.get(position).getManualAdditionalDisclosures() != null && list.get(position).getManualAdditionalDisclosures().size() > 0) {
                    ArrayList<Value> list1 = new ArrayList<>();
                    for (int i = 0; i < list.get(position).getManualAdditionalDisclosures().size(); i++) {

                        Value value = new Value();
                        value.setId(list.get(position).getManualAdditionalDisclosures().get(i).getId());
                        value.setManual(true);
                        value.setTitle(list.get(position).getManualAdditionalDisclosures().get(i).getTitle());
                        value.setTranslations(list.get(position).getManualAdditionalDisclosures().get(i).getTranslations());
                        list1.add(value);
                        Timber.e("List size after: %s", list1.size());
                        additionalDisclosure1.setValue(list1);
                    }

                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(additionalDisclosure1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            }
        } else {

            AdditionalDisclosure additionalDisclosure1 = new AdditionalDisclosure();
            if (additionalDisclosure != null && additionalDisclosure.getValue() != null && additionalDisclosure.getValue().size() > 0) {
                ArrayList<Value> list1 = additionalDisclosure.getValue();
                Timber.e("List size before: %s", list1.size());
                for (int i = 0; i < additionalDisclosure.getValue().size(); i++) {
                    final Value value = additionalDisclosure.getValue().get(i);
                    if (additionalDisclosure.getValue().get(i).isManual()) {
                        final int finalI = i;
                        AdditionalDisclosure finalAdditionalDisclosure = additionalDisclosure;
                        Predicate<Value> pr = new Predicate<Value>() {
                            @Override
                            public boolean test(Value value1) {
                                return (value.getId().equalsIgnoreCase(finalAdditionalDisclosure.getValue().get(finalI).getId()));
                            }
                        };
                        list1.removeIf(pr);
                    }
                }
                Timber.e("List size after: %s", list1.size());
                additionalDisclosure1.setValue(list1);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setAdditionalDisclosure(additionalDisclosure1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            }
        }
        if (notify)
            notifyDataSetChanged();
    }

    private boolean checkDuplicateAdditionalItem(String manualAdditionalDisclosures, ArrayList<Value> additionalDisclosure) {
        for (int i = 0; i < additionalDisclosure.size(); i++) {
            if (manualAdditionalDisclosures.equalsIgnoreCase(additionalDisclosure.get(i).getId())) {
                return true;
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void checkChangedLocal(int position, boolean b, TypeRow holder) {
        list.get(position).setChecked(b);

        boolean isAllSelected = false;
        if (first) {
            AppConstant.commonList.get(position).setChecked(b);
            if (list.size() - 1 != position) {
                autoSelectDependentIds(position, list, b, true);
                list.get(list.size() - 1).setChecked(false);
                AppConstant.commonList.get(AppConstant.commonList.size() - 1).setChecked(false);
                if (b) {
                    showEditDialog(position, true);
                }
            } else {
                for (int i = 0; i < list.size() - 1; i++) {
                    AppConstant.commonList.get(i).setChecked(false);
                    list.get(i).setChecked(false);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    }, 60);
                }
            }
            iOnLanguageSelect.languageSelected(false, false, false);
        } else {
            if (b)
            holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            else{
                holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
            }
            AppConstant.condition.get(position).setChecked(b);
            for (int i = 0; i < AppConstant.condition.size(); i++) {
                if (!AppConstant.condition.get(i).isChecked()) {
                    isAllSelected = AppConstant.condition.get(i).isChecked();
                    break;
                }
                isAllSelected = AppConstant.condition.get(i).isChecked();
            }
            iOnLanguageSelect.languageSelected(isAllSelected, false, false);
        }
    }


    private void showSafetyRecall(String title, String msg, final AlertDialog alertDialog) {

        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
        final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(activity).create();
        alertD.setCancelable(false);
        final DSTextView titleTV = promptView.findViewById(R.id.title);
        DSTextView messageTV = promptView.findViewById(R.id.message);
        titleTV.setText("WARNING: " + title);
        messageTV.setText(msg);
        final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);

        DSTextView ok = promptView.findViewById(R.id.ok);

        stopDeal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertDialog.dismiss();
                stopDeal.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopDeal.setEnabled(true);
                    }
                }, 4000);
                if (activity != null && !activity.isFinishing() && alertD.isShowing()) {
                    alertD.dismiss();
                }
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (activity instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) activity).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                        pojo.setId(AppConstant.commonList.get(i).getId());
                        pojo.setName(AppConstant.commonList.get(i).getName());
                        historyListPojos1.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                iOnLanguageSelect.languageSelected(false, true, false);
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                iOnLanguageSelect.languageSelected(false, false, true);

                if (activity != null && !activity.isFinishing() && alertD.isShowing()) {
                    alertD.dismiss();
                }

            }
        });
        alertD.setView(promptView);
        alertD.show();
    }

    private void showDialogForRecall(int position, AlertDialog alertD) {
        Gson gson = new GsonBuilder().create();
        ArrayList<WarningAlerts> msg = list.get(position).getWarningAlerts();
        ;
//        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
//            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getWarningAlerts() == null) {
//                VehicleConditionDisclosure vehicleConditionDisclosure = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVehicleConditionDisclosure();
//                if (vehicleConditionDisclosure != null && vehicleConditionDisclosure.getValue() != null&&vehicleConditionDisclosure.getValue().get(position).getWarningAlerts()!=null) {
//                    msg = vehicleConditionDisclosure.getValue().get(position).getWarningAlerts();
//                } else {
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//                        vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE_NEW), VehicleConditionDisclosure.class);
//                    else {
//                        vehicleConditionDisclosure = gson.fromJson(PreferenceManger.getStringValue(AppConstant.CONDITION_DISCLOSURE), VehicleConditionDisclosure.class);
//                    }
//                    if (vehicleConditionDisclosure != null&& vehicleConditionDisclosure.getValue() != null&&vehicleConditionDisclosure.getValue().get(position).getWarningAlerts()!=null) {
//                        msg = vehicleConditionDisclosure.getValue().get(position).getWarningAlerts();
//                    }
//                }
//            } else {
//                msg = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getWarningAlerts();
//            }
//
//        }

        if (msg != null && msg.size() > 0) {
            showSafetyRecall(msg.get(0).getTitle(), msg.get(0).getMessage(), alertD);
        }
    }

    private void showEditDialog(final int position, boolean show) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View promptView = layoutInflater.inflate(R.layout.edit_condition_dialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(activity).create();
        alertD.setCancelable(false);

        DSTextView btnAdd1 = promptView.findViewById(R.id.save);
        final DSTextView close = promptView.findViewById(R.id.close);
        DSTextView title = promptView.findViewById(R.id.title);
        title.setText(list.get(position).getName());
        final DSEdittext data = promptView.findViewById(R.id.addCondition);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyboardUtils.hideSoftKeyboard(data, activity);
                notifyDataSetChanged();
                alertD.dismiss();
            }
        });


        if (list.get(position).getAdditionalData() != null) {
            data.setText(list.get(position).getAdditionalData());
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    data.setSelection(list.get(position).getAdditionalData().length());
                }
            });
        }

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(data, activity);
                notifyDataSetChanged();
                AppConstant.commonList.get(position).setAdditionalData(data.getText().toString().trim());
                alertD.dismiss();

            }
        });


        alertD.setView(promptView);

        alertD.show();
        if (show)
            showDialogForRecall(position, alertD);
    }

    private void saveData() {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name,bullet;
        CheckBox checkBox;

        TypeRow(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            name = itemView.findViewById(R.id.name);
            bullet = itemView.findViewById(R.id.bulletPoint);
        }
    }
}
