package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentCustomerHandover extends BaseFragment {


    private View mView;
    IGoToNextPage iGoToNextPage;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_customer_handover1, container, false);
        initView();
        return mView;
    }

    private void initView() {
//        Button logout = (Button) mView.findViewById(R.id.logout);
//        Button back = (Button) mView.findViewById(R.id.back);
//        Button next = (Button) mView.findViewById(R.id.next);
//        Button cancel = (Button) mView.findViewById(R.id.cancel);
//        logout.setOnClickListener(mOnClickListener);
//        next.setOnClickListener(mOnClickListener);
//        back.setOnClickListener(mOnClickListener);
//        cancel.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.next:
                    iGoToNextPage.whatNextClick();
//                    next(new FragmentSelectLanguage());
                    break;
                case R.id.back:
                    iGoToNextPage.goToBackIndex();
//                    goBack();
                    break;

            }
        }
    };
}
