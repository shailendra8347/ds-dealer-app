package com.volga.dealershieldtablet.ui.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomerVehicleDealAlerts;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;
import com.yariksoffice.lingver.Lingver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentESignContactInfo extends BaseFragment {

    private View mView;
    private DSTextView next;
    DSEdittext email, confirmEmail, mobile;

    private boolean isCoBuyer;
    private DSTextView tvEmail, tvCnfEmail, tvMobile;
    private TextView title;
    private boolean called = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mobile != null) {
            next.setVisibility(View.GONE);
            next.setClickable(true);
            next.setEnabled(true);
            updateData();
        } else {
            isTaken = false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            isCoBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();
            if (getArguments() != null && getArguments().getBoolean("isCobuyer")) {
                PreferenceManger.getStringValue("SFName");
                mobile.setText("");
                email.setText("");
                confirmEmail.setText("");
                tvEmail.setText(Html.fromHtml(getResources().getString(R.string.co_compulsory_emails), Html.FROM_HTML_MODE_COMPACT));
                tvCnfEmail.setText(Html.fromHtml(getResources().getString(R.string.co_confirm_email), Html.FROM_HTML_MODE_COMPACT));
                tvMobile.setText(Html.fromHtml(getResources().getString(R.string.co_mobile) + "*", Html.FROM_HTML_MODE_COMPACT));


                email.setHint(getText(R.string.co_compulsory_emails_h));
                confirmEmail.setHint(getText(R.string.co_confirm_email_h));


                mobile.setHint(getText(R.string.co_mobile_) + "*");
                next.setText("Save");
//                checkBox.setChecked(true);
                title.setText(Html.fromHtml(getResources().getString(R.string.esign_co_buyer), Html.FROM_HTML_MODE_COMPACT));
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    title.setText(Html.fromHtml(getResources().getString(R.string.co_contact_info_input), Html.FROM_HTML_MODE_LEGACY));
//                } else {
//                    title.setText(Html.fromHtml(getResources().getString(R.string.co_contact_info_input)));
//                }
//                mobile.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getMobileNumber() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getMobileNumber());
//                email.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getEmail() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getEmail());
//                confirmEmail.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getEmail() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().getEmail());
            } else {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    title.setText(Html.fromHtml(getResources().getString(R.string.contact_info_input), Html.FROM_HTML_MODE_COMPACT));
//                } else {
//                    title.setText(Html.fromHtml(getResources().getString(R.string.contact_info_input)));
//                }
                title.setText(Html.fromHtml(getResources().getString(R.string.esign_buyer), Html.FROM_HTML_MODE_COMPACT));

                tvEmail.setText(Html.fromHtml(getResources().getString(R.string.compulsory_email), Html.FROM_HTML_MODE_COMPACT));
                tvCnfEmail.setText(Html.fromHtml(getResources().getString(R.string.confirm_email), Html.FROM_HTML_MODE_COMPACT));
                tvMobile.setText(Html.fromHtml(getResources().getString(R.string.mobile) + "*", Html.FROM_HTML_MODE_COMPACT));

                email.setHint(getText(R.string.compulsory_email_h));
                confirmEmail.setHint(getText(R.string.confirm_email_h));
                mobile.setHint(getText(R.string.mobile_) + "*");

//                checkBox.setChecked(true);
//                mobile.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getMobileNumber() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getMobileNumber());
//                email.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getEmail() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getEmail());
//                confirmEmail.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getEmail() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getEmail());

            }
        }
        if (getArguments().getBoolean("checkId")) {
            title.setText(Html.fromHtml(getResources().getString(R.string.contact_info_input)));
        }
        if (!getArguments().getBoolean("hasCobuyer")) {
            next.setText("Save");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.esign_contact_info, container, false);
        initView();
        return mView;
    }

    private void showAlert() {
        if (getActivity() != null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View promptView = layoutInflater.inflate(R.layout.warning_alert_dialog, null);
            final android.app.AlertDialog alertD = new android.app.AlertDialog.Builder(getActivity()).create();
            alertD.setCancelable(false);
            final DSTextView titleTV = promptView.findViewById(R.id.title);
            DSTextView messageTV = promptView.findViewById(R.id.message);
//        titleTV.setText("WARNING: " + title);
            titleTV.setVisibility(View.GONE);
            messageTV.setText(R.string.internet_alert);
            final DSTextView stopDeal = promptView.findViewById(R.id.stopDeal);
            stopDeal.setVisibility(View.GONE);
            DSTextView ok = promptView.findViewById(R.id.ok);
            alertD.setView(promptView);
            alertD.show();
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    File file;
                    if (getArguments() != null && getArguments().getBoolean("isCobuyer")) {
                        file = getOutputFile("Screenshot", "alert_remote_deal_co_buyer_page");
                    } else {
                        file = getOutputFile("Screenshot", "alert_remote_deal_buyer_page");
                    }
//                    file = getOutputFile("Screenshot", "alert_remote_deal_page");
                    takeScreenshotForAlert(file);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                        ArrayList<CustomerVehicleDealAlerts> alert = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCustomerVehicleDealAlerts();
                        if (alert == null || alert.size() == 0) {
                            ArrayList<CustomerVehicleDealAlerts> arrayList = new ArrayList<>();
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("No Internet access");
                            customerVehicleDealAlerts.setAlertDesc("While saving remote deal data Internet was not available at contact info screen");
                            arrayList.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(arrayList);
                        } else {
                            CustomerVehicleDealAlerts customerVehicleDealAlerts = new CustomerVehicleDealAlerts();
                            customerVehicleDealAlerts.setAlertName("No Internet access");
                            customerVehicleDealAlerts.setAlertDesc("While saving remote deal data Internet was not available at contact info screen");
                            alert.add(customerVehicleDealAlerts);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCustomerVehicleDealAlerts(alert);
                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    alertD.dismiss();
                }
            });


        }
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initView() {
        Lingver.getInstance().setLocale(getActivity(), "en");
        next = mView.findViewById(R.id.text_next);
        next.setVisibility(View.GONE);
        TextView back = mView.findViewById(R.id.text_back_up);
        final TextView terms = mView.findViewById(R.id.termsText);
        TextView notice = mView.findViewById(R.id.notice);
        tvEmail = mView.findViewById(R.id.tvemail);
        tvCnfEmail = mView.findViewById(R.id.tvcnfemail);
        tvMobile = mView.findViewById(R.id.tvmobile);
        title = mView.findViewById(R.id.title);
//        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
//        text_cancelBack.setVisibility(View.INVISIBLE);
//        if (getActivity() instanceof NewDealViewPager) {
//            if (decidePageNumber() == 10)
//                text_cancelBack.setText(String.format(getString(R.string.page1), "10", decidePageNumber()));
//            else if (decidePageNumber() == 11)
//                text_cancelBack.setText(String.format(getString(R.string.page1), "11", decidePageNumber()));
//            else if (decidePageNumber() == 12)
//                text_cancelBack.setText(String.format(getString(R.string.page1), "12", decidePageNumber()));
//            else
//                text_cancelBack.setText(String.format(getString(R.string.page1), "13", decidePageNumber()));
//        }

//        if (getArguments().getBoolean("checkId")) {
//            next.setText("Save");
//            text_cancelBack.setVisibility(View.INVISIBLE);
//        } else {
//            text_cancelBack.setVisibility(View.VISIBLE);
//        }

        email = mView.findViewById(R.id.et_email);
        email.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        disablePaste(email);
        confirmEmail = mView.findViewById(R.id.et_confirm_email);
        confirmEmail.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        disablePaste(confirmEmail);
        confirmEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (email.getText().toString().trim().length() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            next.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else {
                    next.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (confirmEmail.getText().toString().trim().length() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            next.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else {
                    next.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mobile = mView.findViewById(R.id.et_mobile);
        final LinearLayout emailLayout = mView.findViewById(R.id.email_layout);
        final String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });
        updateData();
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                KeyboardUtils.hideSoftKeyboard(mView, getActivity());


                final String emailS = email.getText().toString().trim();
                final String emailCompulsory = confirmEmail.getText().toString().trim();
                if (emailS.trim().equals("")) {
                    new CustomToast(getActivity()).alert(getString(R.string.empty_email));
                } else if (emailS.trim().contains("..")) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_email));
                } else if (!emailS.trim().matches(emailPattern)) {
                    new CustomToast(getActivity()).alert(getString(R.string.valid_email));
                } else if (!emailCompulsory.trim().equalsIgnoreCase(emailS)) {
                    new CustomToast(getActivity()).alert(getString(R.string.email_not_matched));
                } else {

                    if (mobile.getText().toString().trim().length() == 10) {
                        next.setClickable(false);
                        next.setEnabled(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                next.setClickable(true);
                                next.setEnabled(true);
                            }
                        }, 3000);
                        try {
//                            mobile.getText().toString();
//                            email.getText().toString().trim();
                            if (!DSAPP.getInstance().isNetworkAvailable() && !called) {
                                called = true;
                                showAlert();
//                                new CustomToast(getActivity()).alert(getString(R.string.connection_check));
//                                takeScreenshotForAlert();
                                return;
                            }
                            SetRecordData();
                            if (getArguments() != null && getArguments().getBoolean("hasCobuyer") && !getArguments().getBoolean("isCobuyer")) {
                                Intent intent = new Intent(getActivity(), NewDealViewPager.class);
                                intent.putExtra("page", 82);
                                getActivity().startActivity(intent);
                            } else {
                                Intent intent = new Intent(getActivity(), NewDealViewPager.class);
                                intent.putExtra("page", 83);
                                getActivity().startActivity(intent);
//                                iGoToNextPage.whatNextClick();
//                                save data for upload & finalize the all images
//                                also add there either buyer/co-buyer has selected for e-sign  IsRemote
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        new CustomToast(getActivity()).alert(getString(R.string.valid_mobile));
                    }
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setMobileNumber(mobile.getText().toString());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setEmail(email.getText().toString().trim());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setIsRemote(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    } else {
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setMobileNumber(mobile.getText().toString());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setEmail(email.getText().toString().trim());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setIsRemote(false);
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    }
                }


                Lingver.getInstance().setLocale(getActivity(), "en");
                getActivity().finish();
            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("NewApi")
    private void disablePaste(DSEdittext mEditText) {
        ActionMode.Callback callback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                if (menu != null) {
                    menu.removeItem(android.R.id.paste);
                }
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        };

        mEditText.setCustomInsertionActionModeCallback(callback);

        mEditText.setCustomSelectionActionModeCallback(callback);
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        String middle;
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (getArguments().getBoolean("checkId")) {
            middle = "CheckID";
        } else {
            middle = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        }
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + middle + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public File getOutputMediaFileImage(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        String middle;
        if (!getArguments().getBoolean("checkId")) {
            middle = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        } else {
            middle = "CheckID";
        }
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + middle + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            if (getArguments() != null && getArguments().getBoolean("isCobuyer")) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setMobileNumber(mobile.getText().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setEmail(email.getText().toString().trim());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setIsRemote(true);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            } else {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setMobileNumber(mobile.getText().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setEmail(email.getText().toString().trim());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setIsRemote(true);
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            }
        }
    }


    private void customTextView(TextView view) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                getString(R.string.aggre) + " ");
        spanTxt.append(getString(R.string.data_policy));
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new WebViewFragmentDialog();
                Bundle bundle = new Bundle();

                String selectedLanguage;
                String url;
                if (!getArguments().getBoolean("checkId")) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record rowData = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (rowData.getSettingsAndPermissions().isCoBuyer())
                        selectedLanguage = rowData.getSettingsAndPermissions().getCoBuyerSelectedLanguage();
                    else
                        selectedLanguage = rowData.getSettingsAndPermissions().getSelectedLanguage();
                    if (selectedLanguage == null) {
                        selectedLanguage = "English";
                    }
                    url = null;
                    if (selectedLanguage.equalsIgnoreCase("Chinese")) {
                        url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
                        url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "es";
                    } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
                        url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
                        url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
                        url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("English")) {
                        url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "en";
                    }
                } else {
                    url = AppConstant.BASE_URL1 + "/resource/DataPolicy?lang=" + "en";
                }

                bundle.putString("url", url);
                newFragment.setArguments(bundle);
                newFragment.show(ft, "dialog");
//                Toast.makeText(getActivity(), "Terms of services Clicked",
//                        Toast.LENGTH_SHORT).show();
            }
        }, spanTxt.length() - (getString(R.string.data_policy)).length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    private void customNoticeTextView(TextView view) {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                getString(R.string.e_sign) + " ");
        spanTxt.append(getString(R.string.notice));
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new WebViewFragmentDialog();
                Bundle bundle = new Bundle();
                String selectedLanguage;
                String url;
                if (!getArguments().getBoolean("checkId")) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record rowData = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    if (rowData.getSettingsAndPermissions().isCoBuyer())
                        selectedLanguage = rowData.getSettingsAndPermissions().getCoBuyerSelectedLanguage();
                    else
                        selectedLanguage = rowData.getSettingsAndPermissions().getSelectedLanguage();

                    if (selectedLanguage == null || selectedLanguage.length() == 0) {
                        selectedLanguage = "English";
                    }
                    url = null;
                    if (selectedLanguage.equalsIgnoreCase("Chinese")) {
                        url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
                        url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "es";
                    } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
                        url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
                        url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
                        url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "en";
                    } else if (selectedLanguage.equalsIgnoreCase("English")) {
                        url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "en";
                    }
                } else {
                    url = AppConstant.BASE_URL1 + "/resource/eSignaturePolicy?lang=" + "en";
                }

                bundle.putString("url", url);
                newFragment.setArguments(bundle);
                newFragment.show(ft, "dialog");
//                Toast.makeText(getActivity(), "Terms of services Clicked",
//                        Toast.LENGTH_SHORT).show();
            }
        }, spanTxt.length() - (getString(R.string.notice)).length(), spanTxt.length(), 0);
        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

//    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
//        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
////        HashMap<String,boolean> stringHashMap=new HashMap<>();
//        String path_licence, path_signature, path_car, path_user_pic, path_screenshots;
//        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
//        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
//        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
//        path_user_pic = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/UserPic";
//        path_screenshots = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Screenshot";
//
//
//        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle1() == null) {
//                record.getVehicleDetails().setTitle1("Additional Image 1");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
//        }
//        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle2() == null) {
//                record.getVehicleDetails().setTitle2("Additional Image 2");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
//            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
//        }
//        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle3() == null) {
//                record.getVehicleDetails().setTitle3("Additional Image 3");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
//            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
//        }
//        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle4() == null) {
//                record.getVehicleDetails().setTitle4("Additional Image 4");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
//        }
//        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle5() == null) {
//                record.getVehicleDetails().setTitle5("Additional Image 5");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
//            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle1() == null) {
//                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
//            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle2() == null) {
//                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
//            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle3() == null) {
//                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
//            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle4() == null) {
//                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle5() == null) {
//                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
//        }
//
//        /*Car Images*/
//        if (new File(path_car + "/IMG_front.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//
//            synced.setName("FRONTIMG");
//            synced.setFile(new File(path_car + "/IMG_front.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
//        }
//        if (new File(path_car + "/IMG_right.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("RIGHTIMG");
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_right.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
//        }
//        if (new File(path_car + "/IMG_rear.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("REARIMG");
//            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
//        }
//        if (new File(path_car + "/IMG_left.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("LEFTIMG");
//            synced.setFile(new File(path_car + "/IMG_left.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
//        }
//        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("WINDOWWSTICKERIMG");
//            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//
////            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
//        }
//        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("MILEAGEIMG");
//            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
//        }
//        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("TRADEINMILEAGE");
//            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
//        }
//        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("LICENCEPLATENUMBER");
//            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
//        }
//        /*Additional Images*/
//
//        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("Customer_Insurance");
//            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
//        }
//        //Add all signature image here
//        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
//            synced.setName("language_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        }
//
//
//        if (new File(path_signature + "/IMG_third_party_sign_CarFax_Report.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_third_party_sign_CarFax_Report.svg"));
//            synced.setName("third_party_sign_CarFax_Report");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        }
//
//        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_CarFax_Report.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_CarFax_Report.svg"));
//            synced.setName("co_buyer_third_party_sign_CarFax_Report");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        }
//        if (new File(path_signature + "/IMG_third_party_sign_AutoCheck_Report.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_third_party_sign_AutoCheck_Report.svg"));
//            synced.setName("third_party_sign_AutoCheck_Report");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        }
//
//        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_AutoCheck_Report.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_AutoCheck_Report.svg"));
//            synced.setName("co_buyer_third_party_sign_AutoCheck_Report");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        }
//
//        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
//            synced.setName("type_of_purchase_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
//        }
//        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
//            synced.setName("confirm_car_images_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
//        }
//        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("confirm_car_details_selection");
//            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
//        }
//        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
//            synced.setName("test_drive_taken_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
//        }
//        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
//            synced.setName("no_test_drive_confirm_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
//        }
//        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
//            synced.setName("remove_stickers_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
//        }
//        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
//            synced.setName("CONTACTINFOSIGNATUREIMG");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
//        }
//        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
//            synced.setName("prints_recieved_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
//        }
//        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("history_disclosure");
//            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
//            synced.setName("history_report");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
//        }
//        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("condition_disclosure");
//            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_language_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_type_of_purchase_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
//            synced.setName("co_buyer_confirm_car_images_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_confirm_car_details_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_test_drive_taken_selection");
//            synced.setSynced(false);
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_no_test_drive_confirm_selection");
//            synced.setSynced(false);
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_remove_stickers_selection");
//            synced.setSynced(false);
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
//        }
//        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_contact_info");
//            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_prints_recieved_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
//            synced.setName("co_buyer_condition_disclosure");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {
//
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_history_report");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
//            synced.setName("co_buyer_history_disclosure");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_buyer_no_verble_promise.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_buyer_no_verble_promise.svg"));
//            synced.setName("buyer_no_verble_promise");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg"));
//            synced.setName("co_buyer_no_verble_promise");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_checklist.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_checklist.svg"));
//            synced.setName("co_buyer_checklist");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_buyer_checklist.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_buyer_checklist.svg"));
//            synced.setName("buyer_checklist");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        }
//        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_DLFRONT");
//            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
//        }
//        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("DRIVERLICENSEIMG");
//            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
//        }
//        if (new File(path_car + "/IMG_trade_front.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("TradeInFront");
//            synced.setFile(new File(path_car + "/IMG_trade_front.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
//        }
//
//        /*Buyers screenshots*/
////alert_report_fetching_error
//
//        if (new File(path_screenshots + "/IMG_alert_report_fetching_error.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_report_fetching_error");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//
//        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_condition_response_report_fetching_error_co_buyer");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_condition_response_report_fetching_error_buyer");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_history_response_report_fetching_error_buyer");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_history_response_report_fetching_error_co_buyer");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_history_response_report_fetching_error");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_finance_page_ac");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_finance_page_cf");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_vehicle_info_page");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_finance_page.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_finance_page");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_third_party_report_page");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("alert_co_buyer_third_party_report_page");
//            synced.setFile(new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//        if (new File(path_screenshots + "/IMG_language_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("language_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_language_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_purchase_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("purchase_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_purchase_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//        if (new File(path_screenshots + "/IMG_car_images_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("car_images_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_car_images_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_car_info_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("car_info_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_car_info_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_test_drive_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("test_drive_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_test_drive_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_no_test_drive_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("no_test_drive_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_no_test_drive_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_remove_sticker_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("remove_sticker_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_remove_sticker_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("condition_disclosure_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_history_disclosure_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("history_disclosure_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_history_disclosure_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_third_party_report_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("third_party_report_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_third_party_report_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("no_verbal_promise_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_check_list_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("check_list_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_check_list_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("print_receive_selection_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_contact_info_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("contact_info_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_contact_info_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        /*Co-Buyers screenshots*/
//
//        if (new File(path_screenshots + "/IMG_co_language_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_language_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_language_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_purchase_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_purchase_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_purchase_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//        if (new File(path_screenshots + "/IMG_co_car_images_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_car_images_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_car_images_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_car_info_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_car_info_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_car_info_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_test_drive_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_test_drive_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_test_drive_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_no_test_drive_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_remove_sticker_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_condition_disclosure_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_history_disclosure_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_third_party_report_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_no_verbal_promise_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_check_list_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_check_list_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_check_list_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_print_receive_selection_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_screenshots + "/IMG_co_contact_info_screen.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_contact_info_screen");
//            synced.setFile(new File(path_screenshots + "/IMG_co_contact_info_screen.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//
//        /*Buyers pictures*/
//
//        if (new File(path_user_pic + "/IMG_language_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("language_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_language_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_purchase_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("purchase_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_purchase_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//        if (new File(path_user_pic + "/IMG_car_images_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("car_images_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_car_images_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_car_info_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("car_info_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_car_info_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("test_drive_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("no_test_drive_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("remove_sticker_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("condition_disclosure_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("history_disclosure_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("third_party_report_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("no_verbal_promise_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_check_list_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("check_list_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_check_list_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("print_receive_selection_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("contact_info_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        /*Co-Buyers picture*/
//
//        if (new File(path_user_pic + "/IMG_co_language_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_language_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_language_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_purchase_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//        if (new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_car_images_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_car_info_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_test_drive_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_no_test_drive_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_remove_sticker_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_condition_disclosure_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_history_disclosure_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_third_party_report_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_no_verbal_promise_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_check_list_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_print_receive_selection_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//        if (new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_contact_info_screen_pic");
//            synced.setFile(new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//        }
//
//        return imagesSynceds;
//    }

    private void startImageSyncForRecursion(final File file, final String id, final String msg, final ProgressDialog progressDialog) {
//        System.gc();
        boolean additional = false;

        String imageFor = "1";
        /*Write logic for ImageFor */
        //TODO

        double size = (file.length() / (1024 * 1024));
        String imageDateTime = null;
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

        File filenew = null;
        Log.e("File size prev : ", file.length() / (1024 * 1024) + " M.B");
        if (size >= 1) {
            try {
                filenew = new Compressor(DSAPP.getInstance()).compressToFile(file);
                Log.e("File size new : ", filenew.length() / (1024 * 1024) + " M.B");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        MultipartBody.Part body;
        RequestBody reqFile;
        if (filenew != null) {
            reqFile = RequestBody.create(MediaType.parse("image/*"), filenew);
            body = MultipartBody.Part.createFormData("upload", filenew.getName(), reqFile);
        } else {
            reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
        }
        String utcForImg = null;
        try {
            imageDateTime = tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString()));
            long timeForUTC = new Date(file.lastModified()).getTime();
            utcForImg = tradeTimeFormatter.format(new Date(TimeZoneUtils.toUTC(timeForUTC, TimeZone.getDefault())));
            Log.e("UTC time for img: ", utcForImg);
            Log.e("Normal time for img: ", imageDateTime);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().syncImagesToServer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, msg, imageDateTime, body, imageFor, additional, utcForImg);
        final String finalDoctype = msg;
        imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(@NonNull Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                assert response.body() != null;
                if (response.isSuccessful() && response.body().isSucceeded()) {
                    assert response.body() != null;
                    Log.e("Image upload type :", "onResponse: " + finalDoctype + " " + response.body().getValue());
                    if (file.exists()) {
                        Log.e("File deleted :", "onResponse: " + finalDoctype + " " + file.delete());
                    }
                    if (msg.contains("dl_front")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                callHomeActivity();
                                clearDMVPreference();
                            }
                        }, 3100);
                    }

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString() + " Image Name: " + finalDoctype);
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        /*Push logs here every upload*/
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ImageUploadResponse> call, @NonNull Throwable t) {
                if (getActivity() != null && !getActivity().isFinishing())
                    new CustomToast(getActivity()).alert(getString(R.string.connection_check));

            }
        });
    }


}
