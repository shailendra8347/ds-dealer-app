package com.volga.dealershieldtablet.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealDetail.VehicleTradeConditionDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.adapter.ConditionAdapter;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class FragmentUsedVehicleHistoryPart1 extends BaseFragment implements ConditionAdapter.IOnLanguageSelect {
    private IGoToNextPage iGoToNextPage;
    private View mView;
    private String[] list;
    private boolean first = false;
    private boolean isSigned = false;
    private boolean shouldNextShow = false;
    private DSTextView next;
    private boolean allTrue = false;
    private ArrayList<HistoryListPojo> listPojos, listOnUserPage;
    private boolean isCobuyer = false;
    private SignaturePad signaturePad;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null && !first) {
            signaturePad.clear();
            enableNext(next);
            isSigned = false;
            next.setVisibility(View.GONE);

        }
        if (isVisibleToUser && next != null) {
            enableNext(next);
        }
        if (isVisibleToUser) {
            showFragmentInPortrait();
        } else {
            isTaken = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.used_vehicle_history_after_sticker, container, false);
        if (AppConstant.shouldCall) {
            AppConstant.shouldCall = false;
            showFragmentInPortrait();
        }
        initView();
        return mView;
    }

    private void initView() {
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        DSTextView history = mView.findViewById(R.id.history1);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            isCobuyer = true;
        }
        DSTextView history1 = mView.findViewById(R.id.history);
        DSTextView customerAcknowledge = mView.findViewById(R.id.customerAcknowledge);
        LinearLayout frameLayout = mView.findViewById(R.id.boxSign);
        LinearLayout noticeBox = mView.findViewById(R.id.noticeBox);
        DSTextView notice = mView.findViewById(R.id.notice);
        final NestedScrollView nestedScrollView = mView.findViewById(R.id.nestedScroll);
        mView.findViewById(R.id.recyclerView).setFocusable(false);
        mView.findViewById(R.id.temp).requestFocus();
//        nestedScrollView.fullScroll(View.FOCUS_UP);
//        nestedScrollView.scrollTo(0,0);
        final DSTextView sign = mView.findViewById(R.id.signhere);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
        }
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        next = mView.findViewById(R.id.text_next);
        enableNext(next);
        final ArrayList<HistoryListPojo> historyListPojos = new ArrayList<>();
        listOnUserPage = new ArrayList<>();
        first = getArguments().getBoolean("first", false);
        final LinearLayout disclosureMain = mView.findViewById(R.id.maineContent);
        if (first) {
            noticeBox.setVisibility(View.GONE);
            noticeBox.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);
            history.setText(Html.fromHtml(getString(R.string.vehicle_condition)));
            sign.setVisibility(View.GONE);
//            disclosureMain.setBackgroundResource(R.drawable.screen_bg);
        } else {
//            disclosureMain.setBackgroundResource(R.drawable.screen_bg_whitegrey);

            noticeBox.setVisibility(View.GONE);
            notice.setText(Html.fromHtml(getString(R.string.disclosureNotice)));
            customerAcknowledge.setVisibility(View.VISIBLE);
            history.setText(Html.fromHtml(getString(R.string.condition_disclosure)));
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                customerAcknowledge.setText(Html.fromHtml(getString(R.string.condition_disclosure_customer_new)));
            } else {
                customerAcknowledge.setText(Html.fromHtml(getString(R.string.condition_disclosure_customer)));
            }
            frameLayout.setVisibility(View.VISIBLE);
            sign.setVisibility(View.VISIBLE);
        }
        String newOrUsed;
        boolean isNew = true;

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//            newOrUsed = "";
        {
            history1.setText(R.string.vehicle_history);
        } else {
            isNew = false;
            newOrUsed = "USED";
            history1.setText(R.string.used_vehicle_history);
        }
//            newOrUsed = getString(R.string.used);
//        history1.setText(R.string.used_vehicle_history);
        history.setVisibility(View.VISIBLE);


        AppConstant.commonList.clear();
        list = getResources().getStringArray(R.array.CommonHistoryList);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition() == null) {
            for (String aList : list) {
                HistoryListPojo pojo = new HistoryListPojo();
                pojo.setName(aList);
                pojo.setId(getIds(aList));
                pojo.setChecked(false);
                historyListPojos.add(pojo);

            }
            AppConstant.commonList.addAll(historyListPojos);
            if (isNew) {
                AppConstant.commonList.remove(0);
                AppConstant.commonList.remove(1);
                AppConstant.commonList.remove(1);
                AppConstant.commonList.remove(1);
                AppConstant.commonList.remove(2);
                AppConstant.commonList.remove(2);
                AppConstant.commonList.remove(4);
                AppConstant.commonList.remove(4);
                AppConstant.commonList.remove(4);
                ArrayList<String> local = new ArrayList<>(Arrays.asList(list));
                local.remove(0);
                local.remove(1);
                local.remove(1);
                local.remove(1);
                local.remove(2);
                local.remove(2);
                local.remove(4);
                local.remove(4);
                local.remove(4);
                list = new String[local.size()];
                list = local.toArray(list);


            } else {
//                AppConstant.commonList.remove(6);
                ArrayList<String> local = new ArrayList<>(Arrays.asList(list));
                local.remove(6);
                list = new String[local.size()];
                list = local.toArray(list);

            }
            boolean shouldDelete = removeIndex6(AppConstant.commonList);
            if (shouldDelete) {
                AppConstant.commonList.remove(6);
            }
        } else {
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                AppConstant.commonList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition());
            boolean shouldDelete = removeIndex6(AppConstant.commonList);
            if (shouldDelete) {
                AppConstant.commonList.remove(6);
            }
        }
        listPojos = new ArrayList<>();
        if (!first) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getConditionSelection() != null) {
                if (!isCobuyer) {
                    listPojos = new ArrayList<>(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getConditionSelection());
                } else {
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCoConditionSelection() == null)
                        listPojos = new ArrayList<>(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getConditionSelection());
                    else {
                        listPojos = new ArrayList<>(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCoConditionSelection());
                    }
                }
            }
            AppConstant.condition.clear();
            if (isNew) {
                ArrayList<String> local = new ArrayList<>(Arrays.asList(list));
                local.remove(0);
                local.remove(1);
                local.remove(1);
                local.remove(1);
                local.remove(2);
                local.remove(2);
                local.remove(4);
                local.remove(4);
                local.remove(4);
                list = new String[local.size()];
                list = local.toArray(list);
            } else {
//                AppConstant.commonList.remove(6);
                ArrayList<String> local = new ArrayList<>(Arrays.asList(list));
                local.remove(6);
                list = new String[local.size()];
                list = local.toArray(list);

            }
            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                for (int j = 0; j < listPojos.size(); j++) {
                    if (AppConstant.commonList.get(i).getId().equalsIgnoreCase(listPojos.get(j).getId())) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(listPojos.get(j).isChecked());
                        pojo.setAdditionalData(listPojos.get(j).getAdditionalData());
                        pojo.setId(listPojos.get(j).getId());
                        if (i < list.length) {
                            pojo.setName(list[i]);
                        } else {
                            pojo.setName("");
                        }
                        AppConstant.condition.add(pojo);
                    }
                }

            }

            boolean isAllSelected = false;
            for (int i = 0; i < AppConstant.condition.size(); i++) {

                if (!AppConstant.condition.get(i).isChecked()) {
                    isAllSelected = AppConstant.condition.get(i).isChecked();
                    break;
                }
                isAllSelected = AppConstant.condition.get(i).isChecked();
            }
            allTrue = isAllSelected;
            if (allTrue && isSigned) {
                next.setVisibility(View.VISIBLE);
            } else {
                next.setVisibility(View.GONE);
            }
            boolean shouldDelete = removeIndex6(AppConstant.condition);
            if (shouldDelete) {
                AppConstant.condition.remove(6);
            }
            recyclerView.setAdapter(new ConditionAdapter(getActivity(), this, AppConstant.condition, first));
        } else {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures().size() > 0) {
                ArrayList<VehicleTradeConditionDisclosures> localList = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < AppConstant.commonList.size(); j++) {
                        if (localList.get(i).getVehicleConditionDisclosureId().equalsIgnoreCase(AppConstant.commonList.get(j).getId())) {
                            AppConstant.commonList.get(j).setAdditionalData(localList.get(i).getValue());
                            AppConstant.commonList.get(j).setChecked(true);
                        }
                    }
                }
            }
            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                if (AppConstant.commonList.get(i).isChecked()) {
                    shouldNextShow = true;
                }
            }
            if (shouldNextShow) {
                next.setVisibility(View.VISIBLE);
            } else {
                next.setVisibility(View.GONE);
            }
            boolean shouldDelete = removeIndex6(AppConstant.commonList);
            if (shouldDelete) {
                AppConstant.commonList.remove(6);
            }
            if (AppConstant.commonList != null && AppConstant.commonList.size() > 0) {
                if (AppConstant.commonList.get(AppConstant.commonList.size() - 1).isChecked()) {
                    for (int i = 0; i < AppConstant.commonList.size() - 1; i++) {
                        AppConstant.commonList.get(i).setChecked(false);
                        AppConstant.commonList.get(i).setAdditionalData("");
                    }
                }
            }
            recyclerView.setAdapter(new ConditionAdapter(getActivity(), this, AppConstant.commonList, first));
        }
        recyclerView.setLayoutManager(manager);
        DSTextView back = mView.findViewById(R.id.text_back);
        if (getActivity() instanceof NewDealViewPager) {
            mView.setFocusableInTouchMode(true);
            mView.requestFocus();
            mView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        String path_signature;
                        Gson gson = new GsonBuilder().create();
                        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                        File svgFile;
                        if (!isCobuyer)
                            svgFile = new File(path_signature + "/IMG_condition_disclosure.svg");
                        else {
                            svgFile = new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg");
                        }
                        File file;
                        if (!isCobuyer) {
                            file = getOutputFile("UserPic", "condition_disclosure_screen_pic");
                        } else {
                            file = getOutputFile("UserPic", "co_condition_disclosure_screen_pic");
                        }
                        File file1;
                        if (!isCobuyer) {
                            file1 = getOutputFile("Screenshot", "condition_disclosure_screen");
                        } else {
                            file1 = getOutputFile("Screenshot", "co_condition_disclosure_screen");
                        }
                        Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                        Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                        Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                        if (NewDealViewPager.currentPage == 39) {
                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                                ((NewDealViewPager) Objects.requireNonNull(getActivity())).setCurrentPage(37);
                            } else {
                                iGoToNextPage.goToBackIndex();
                            }
//                    showFragmentInLandscape();
                        } else {
                            iGoToNextPage.goToBackIndex();
                        }
                        Log.e("Device back: ", " Device back pressed: " + keyCode);
                        return false;
                    }
                    return false;
                }
            });
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (!isCobuyer)
                    svgFile = new File(path_signature + "/IMG_condition_disclosure.svg");
                else {
                    svgFile = new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg");
                }
                File file;
                if (!isCobuyer) {
                    file = getOutputFile("UserPic", "condition_disclosure_screen_pic");
                } else {
                    file = getOutputFile("UserPic", "co_condition_disclosure_screen_pic");
                }
                File file1;
                if (!isCobuyer) {
                    file1 = getOutputFile("Screenshot", "condition_disclosure_screen");
                } else {
                    file1 = getOutputFile("Screenshot", "co_condition_disclosure_screen");
                }
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                if (NewDealViewPager.currentPage == 39) {
                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                        ((NewDealViewPager) Objects.requireNonNull(getActivity())).setCurrentPage(37);
                    } else {
                        iGoToNextPage.goToBackIndex();
                    }
//                    showFragmentInLandscape();
                } else {
                    iGoToNextPage.goToBackIndex();
                }

            }
        });
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
        final DSTextView cancel1 = mView.findViewById(R.id.header1).findViewById(R.id.text_cancelBack);
        if (!first) {
            mView.findViewById(R.id.header).setVisibility(View.GONE);
            mView.findViewById(R.id.header1).setVisibility(View.VISIBLE);
            logout.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.VISIBLE);
            cancel1.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));

        } else {
//            cancel.setTextSize(28);
            mView.findViewById(R.id.header).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.header1).setVisibility(View.GONE);
            logout.setVisibility(View.INVISIBLE);
            if (getActivity() instanceof NewDealViewPager)
                cancel.setVisibility(View.VISIBLE);
            else {
                cancel.setVisibility(View.INVISIBLE);
            }
            cancel.setText(getString(R.string.cancel));
        }
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (getActivity() instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                        pojo.setId(AppConstant.commonList.get(i).getId());
                        pojo.setName(AppConstant.commonList.get(i).getName());
                        historyListPojos1.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                logout();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        cancel.setEnabled(true);
                    }
                }, 4000);
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (getActivity() instanceof NewDealViewPager)
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                for (int i = 0; i < AppConstant.commonList.size(); i++) {
                    if (AppConstant.commonList.get(i).isChecked()) {
                        HistoryListPojo pojo = new HistoryListPojo();
                        pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                        pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                        pojo.setId(AppConstant.commonList.get(i).getId());
                        pojo.setName(AppConstant.commonList.get(i).getName());
                        historyListPojos1.add(pojo);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
                String recordDataString = gson.toJson(recordData);
                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                cancel();
            }
        });

        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                next.setVisibility(View.GONE);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                Handler mainHandler = new Handler(getActivity().getMainLooper());

                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (!isTaken) {
                            isTaken = true;
                            File file;
                            if (!isCobuyer) {
                                file = getOutputFile("UserPic", "condition_disclosure_screen_pic");
                            } else {
                                file = getOutputFile("UserPic", "co_condition_disclosure_screen_pic");
                            }

//                            takeUserPicture(file);
                        }
                    } // This is your code
                };
                mainHandler.post(myRunnable);
            }

            @Override
            public void onSigned() {
                if (allTrue) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isSigned) {
                                next.setVisibility(View.VISIBLE);
                            }
                        }
                    }, AppConstant.NEXT_DELAY);
                }
                isSigned = true;
            }

            @Override
            public void onClear() {

            }
        });
        final DSTextView signature = mView.findViewById(R.id.signature);
        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        next.setVisibility(View.GONE);
                        return true;
                    }
                }
                return true;
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean goNext = false;
                if (first) {
                    for (int i = 0; i < AppConstant.commonList.size(); i++) {
                        if (AppConstant.commonList.get(i).isChecked()) {
                            goNext = true;
                            break;
                        }
                    }
                } else {
                    goNext = true;
                }

                if (!goNext) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_one_option));
                } else {
                    disableNext(next);
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (getActivity() instanceof NewDealViewPager)
                        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(AppConstant.commonList);
                    ArrayList<HistoryListPojo> historyListPojos1 = new ArrayList<>();
                    for (int i = 0; i < AppConstant.commonList.size(); i++) {
                        if (AppConstant.commonList.get(i).isChecked()) {
                            HistoryListPojo pojo = new HistoryListPojo();
                            pojo.setChecked(!AppConstant.commonList.get(i).isChecked());
                            pojo.setAdditionalData(AppConstant.commonList.get(i).getAdditionalData());
                            pojo.setId(AppConstant.commonList.get(i).getId());
                            pojo.setName(AppConstant.commonList.get(i).getName());
                            historyListPojos1.add(pojo);
                        }
                    }
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(historyListPojos1);
//                    else
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoConditionSelection(historyListPojos1);

                    if (!first) {
                        if (!allTrue) {
                            new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                            enableNext(next);
                            return;
                        }
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setConditionSelection(AppConstant.condition);
                        else
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setCoConditionSelection(AppConstant.condition);
                        addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        if (history != null && history.get(history.size() - 1).isChecked()) {
                            page = 76;
                        }
                        if (history != null && history.get(history.size() - 1).isChecked() && report != null && report.get(report.size() - 1).isChecked()) {
                            page = 77;
                        }
                        if (page != 0) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(page);
                        } else {
                            iGoToNextPage.whatNextClick();
                        }
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

                        File file;
                        if (!isCobuyer) {
                            file = getOutputFile("Screenshot", "condition_disclosure_screen");
                        } else {
                            file = getOutputFile("Screenshot", "co_condition_disclosure_screen");
                        }
                        scrollableScreenshot(disclosureMain, file);
                    } else {
//                        enableNext(next);
                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        if (getActivity() instanceof NewDealViewPager) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                                ((NewDealViewPager) getActivity()).setCurrentPage(28);
                            } else {
                                iGoToNextPage.whatNextClick();
                            }
                        }else {
                            iGoToNextPage.whatNextClick();
                        }
                    }

                }
            }
        });
    }

    private boolean removeIndex6(ArrayList<HistoryListPojo> commonList) {
        ArrayList<HistoryListPojo> list = new ArrayList<>(commonList);
        for (int i = 0; i < list.size(); i++) {
            if (commonList.get(i).getId().equalsIgnoreCase("6")) {
                return true;
            }
        }
        return false;
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            File svgFile;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                svgFile = getOutputMediaFile("Signatures", "co_buyer_condition_disclosure");
            else {
                svgFile = getOutputMediaFile("Signatures", "condition_disclosure");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    @Override
    public void languageSelected(boolean allSelected,boolean isCancel,boolean isOK) {
        if (first) {
            shouldNextShow = false;
            for (int i = 0; i < AppConstant.commonList.size(); i++) {
                if (AppConstant.commonList.get(i).isChecked()) {
                    shouldNextShow = true;
                    /*Added to remove delay 11:44 24/06/2020*/
                    break;
                }
            }
            if (shouldNextShow) {
                next.setVisibility(View.VISIBLE);
            } else {
                next.setVisibility(View.GONE);
            }
        } else {
            allTrue = allSelected;
            if (allTrue && isSigned) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        next.setVisibility(View.VISIBLE);
                    }
                }, 100);
            } else {
                next.setVisibility(View.GONE);
            }
        }
    }

    private String getIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase("Prior Damage")) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase("Prior Accident")) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase("Prior Fleet Vehicle")) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase("Prior Commercial Vehicle")) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase("The vehicle has been Painted")) {
            id = "5";
        } else if (aList.trim().equalsIgnoreCase("Possible Frame/Unibody or Structural Damage – which means damage to any component designed to provide structural integrity of the vehicle. Even minor accidents can cause structural/frame damage to a vehicle. Having a structural inspection before purchase is recommended.")) {
            id = "6";
        } else if (aList.trim().equalsIgnoreCase("Lowered/Raised and/or Lifted with Aftermarket Oversized Wheels and/or Low Profile Tires and/or Altered Suspension. Structural alteration. Manufacturer’s warranty may be void.")) {
            id = "7";
        } else if (aList.trim().equalsIgnoreCase("Modified and/or altered from manufacturer’s specifications, and/or aftermarket equipment(s)/accessories. Manufacturer’s warranty may be void.")) {
            id = "8";
        } else if (aList.trim().equalsIgnoreCase("Correct Odometer or Mileage unknown")) {
            id = "9";
        } else if (aList.trim().equalsIgnoreCase("May be an out-of-state vehicle and it may have an out-of-state title")) {
            id = "10";
        } else if (aList.trim().equalsIgnoreCase("Airbag has been previously deployed")) {
            id = "11";
        } else if (aList.trim().equalsIgnoreCase("Known Frame/Unibody or Structural Damage – which means damage to any component designed to provide structural integrity of the vehicle. Even minor accidents can cause structural/frame damage to a vehicle. Having a structural inspection before purchase is recommended.")) {
            id = "12";
        } else if (aList.trim().equalsIgnoreCase("Open Recall")) {
            id = "13";
        } else if (aList.trim().equalsIgnoreCase("Other")) {
            id = "14";
        } else if (aList.trim().equalsIgnoreCase("BASED ON THE INFORMATION WE ARE AWARE OF, NONE OF THE ABOVE CONDITIONS OR HISTORY DISCLOSURES APPLY TO THE VEHICLE.")) {
            id = "0";
        }
        return id;
    }
}
