package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.cameraview.CameraView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by ${Shailendra} on 07-05-2018.
 */
public class FragmentCarPictureFront extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback {
    private static final int REQUEST_STORAGE = 1010;
    private View mView;
    private static final String TAG = "MainActivity";

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final String FRAGMENT_DIALOG = "dialog";
    private DSTextView desc;
    IGoToNextPage iGoToNextPage;
    private Button takePicture;
    private ProgressDialog progressDialog;
    private DSTextView cancel;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(final boolean isVisibleToUser) {
        Timber.e("%s", isVisibleToUser);
        if (mCameraView != null) {
            if (isVisibleToUser) {
                enableNext(takePicture);
                cancel.setEnabled(true);
                cancel.setClickable(true);
                cancel.setVisibility(View.VISIBLE);
                if (checkPermission()) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                        forMajorDevices();
                    } else {
                        forMinorDevices();
                    }
                }
                showFragmentInLandscape();
                mCameraView.setOrientation(90);
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
            } else {
                if (mCameraView.isCameraOpened())
                    mCameraView.stop();

            }

        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void forMajorDevices() {
        if (NewDealViewPager.currentPage == 3 || NewDealViewPager.currentPage == 36)
            mCameraView.start();
    }

    private void forMinorDevices() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (NewDealViewPager.currentPage == 3 || NewDealViewPager.currentPage == 36)
                    mCameraView.start();
            }
        }, 300);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mCameraView.isCameraOpened())
            mCameraView.stop();
        Log.e(TAG, "onStop: called");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.capture_dmv_front, container, false);
        initView();
        return mView;
    }

    private void initView() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.optimizing_image));
        progressDialog.setCancelable(false);
        mCameraView = (CameraView) mView.findViewById(R.id.camera);
        if (mCameraView != null) {
            mCameraView.addCallback(mCallback);
        }
        takePicture = (Button) mView.findViewById(R.id.take_picture);
        takePicture.setVisibility(View.GONE);
        takePicture.setText(R.string.take_pic);

//        mCameraView.setFlash(CameraView.FLASH_OFF);
        final ImageView flash = mView.findViewById(R.id.flashButton);
        if (hasFlash()) {
            mCameraView.setFlash(CameraView.FLASH_OFF);
            flash.setVisibility(View.VISIBLE);
            flash.setOnClickListener(view -> {
                if (mCameraView.getFlash() == CameraView.FLASH_OFF) {
                    flash.setImageResource(R.drawable.flashbuttonon);
                    mCameraView.setFlash(CameraView.FLASH_TORCH);
                } else {
                    flash.setImageResource(R.drawable.flashbuttonoff);
                    mCameraView.setFlash(CameraView.FLASH_OFF);
                }
            });
        }else {
            flash.setVisibility(View.GONE);
        }
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        desc = mView.findViewById(R.id.desc);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
            desc.setText(R.string.picture_front_tradein);
        } else {
            desc.setText(R.string.picture_front);
        }
//        desc.setText(R.string.picture_front);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        DSTextView back = mView.findViewById(R.id.text_cancelBack);
//        ImageView zoom = mView.findViewById(R.id.zoom);
        ImageView sampleImage = mView.findViewById(R.id.sampleImage);
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        Glide.with(this)
                .load(R.drawable.front)
                .apply(requestOptions)
                .into(sampleImage);
//        zoom.setOnClickListener(mOnClickListener);
//        next = mView.findViewById(R.id.text_next);
        cancel = mView.findViewById(R.id.text_back);
        logout.setOnClickListener(mOnClickListener);
//        next.setOnClickListener(mOnClickListener);
//        next.setVisibility(View.GONE);
        back.setOnClickListener(mOnClickListener);
        LinearLayout viewSample = mView.findViewById(R.id.sampleContainer);
        viewSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogCar("Front");
            }
        });
        cancel.setOnClickListener(mOnClickListener);
        if (takePicture != null) {
            takePicture.setOnClickListener(mOnClickListener);

        }
    }

    private CameraView mCameraView;


    private Handler mBackgroundHandler;

    private float zoomPower = 1;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.take_picture:
                    //ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //                            == PackageManager.PERMISSION_GRANTED
                    if (checkPermission()) {
                        progressDialog.show();
                        disableNext(takePicture);
                        if (mCameraView != null && mCameraView.isCameraOpened()) {
                            mCameraView.takePicture();
                        }
                    } else {
                        requestPermission();
//                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                            FragmentScanDMVFront.ConfirmationDialogFragment
//                                    .newInstance(R.string.storage_permission_confirmation,
//                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                            REQUEST_STORAGE,
//                                            R.string.storage_permission_not_granted)
//                                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
//                        } else {
//                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                    REQUEST_STORAGE);
//                        }
                    }
                    break;
                case R.id.text_cancelBack:
                    cancel();
                    break;
                case R.id.text_next:
                    savePageNumber();
                    iGoToNextPage.whatNextClick();

                    break;
                case R.id.text_back:
//                    cancel.setEnabled(false);
//                    cancel.setClickable(false);
//                    cancel.setVisibility(View.GONE);
                    iGoToNextPage.goToBackIndex();
                    break;
                case R.id.txt_logout:
                    logout();
                    break;
//                case R.id.zoom:
//                    zoomPower++;
//                    if (zoomPower <4) {
//                        mCameraView.setZoom(zoomPower);
//                    } else {
//                        zoomPower = 1;
//                        mCameraView.setZoom(zoomPower);
//                    }
//                    break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume : ", "Resume Called");
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            if (NewDealViewPager.currentPage == 3) {
                if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                    forMajorDevices();
                } else {
                    forMinorDevices();
                }
            }
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBackgroundHandler != null) {
            mBackgroundHandler.getLooper().quitSafely();
            mBackgroundHandler = null;
        }
        Log.e("Car_Front_Destroy", "blah blah");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
                    throw new RuntimeException("Error on requesting camera permission.");
                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), R.string.camera_permission_not_granted,
                            Toast.LENGTH_SHORT).show();
                }
                // No need to start camera here; it is handled by onResume
                break;
        }
    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    private CameraView.Callback mCallback
            = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
          new Handler().postDelayed(() -> takePicture.setVisibility(View.VISIBLE),1600);
            Log.d(TAG, "onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            Log.d(TAG, "onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] data) {
            Log.d(TAG, "onPictureTaken " + data.length);
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            File file;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                file = getOutputMediaFile("CarImages", "front");
            } else {
                file = getOutputMediaFile("CarImages", "trade_front");
            }
//            File file = getOutputMediaFile("CarImages", "front");
            SaveImageToMemory imageToMemory = new SaveImageToMemory(data, file);
            imageToMemory.execute();
        }

    };

    interface OnDone {
        void done();
    }

    OnDone onDone = new OnDone() {
        @Override
        public void done() {
            iGoToNextPage.whatNextClick();
        }
    };

    /**
     * Create a File for saving an image or video
     */
    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public Bitmap ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;


        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + mainFolder + "/" + fileName;

        File imgFile = new File(path);
        if (imgFile.exists()) {
            return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return null;
    }

    public static class ConfirmationDialogFragment extends DialogFragment {

        private static final String ARG_MESSAGE = "message";
        private static final String ARG_PERMISSIONS = "permissions";
        private static final String ARG_REQUEST_CODE = "request_code";
        private static final String ARG_NOT_GRANTED_MESSAGE = "not_granted_message";

        public static ConfirmationDialogFragment newInstance(@StringRes int message,
                                                             String[] permissions, int requestCode, @StringRes int notGrantedMessage) {
            ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_MESSAGE, message);
            args.putStringArray(ARG_PERMISSIONS, permissions);
            args.putInt(ARG_REQUEST_CODE, requestCode);
            args.putInt(ARG_NOT_GRANTED_MESSAGE, notGrantedMessage);
            fragment.setArguments(args);
            return fragment;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Bundle args = getArguments();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(args.getInt(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String[] permissions = args.getStringArray(ARG_PERMISSIONS);
                                    if (permissions == null) {
                                        throw new IllegalArgumentException();
                                    }
                                    ActivityCompat.requestPermissions(getActivity(),
                                            permissions, args.getInt(ARG_REQUEST_CODE));
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getActivity(),
                                            args.getInt(ARG_NOT_GRANTED_MESSAGE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            })
                    .create();
        }

    }

    public void compressImage(byte[] data, File file) {

        OutputStream os = null;
        try {

            os = new FileOutputStream(file);
            os.write(data);
            os.close();
            Log.e(TAG, "doInBackground: " + file.length() / 1024);

        } catch (IOException e) {
            saveLogsInToDB(" Vehicle Image Front: Error in saving Front Image " + e.getLocalizedMessage() + "\n");
            Log.w(TAG, "Cannot write to " + file, e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    saveLogsInToDB(" Vehicle Image Front: Error in saving Front Image " + e.getLocalizedMessage() + "\n");
                    Log.w(TAG, "Cannot write to " + file, e);
                }
            }

        }
//        optimizeImage(file, data);
    }

    @SuppressLint("StaticFieldLeak")
    private class SaveImageToMemory extends AsyncTask<String, Void, String> {
        byte[] data;
        File file;

        SaveImageToMemory(byte[] data, File file) {
            this.data = data;
            this.file = file;
        }


        @Override
        protected String doInBackground(String... params) {

            compressImage(data, file);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            iGoToNextPage.whatNextClick();
            savePageNumber();
            progressDialog.dismiss();
//            takePicture.setEnabled(true);
//            takePicture.setClickable(true);

        }


    }


}
