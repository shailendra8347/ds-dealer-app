package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class PrintsReceivedAdapter extends RecyclerView.Adapter<PrintsReceivedAdapter.TypeRow> {

    private Activity activity;
    private RecordData recordData;
    private Gson gson;
    private ArrayList<HistoryListPojo> list;
    private AllChecked allChecked;

    public interface AllChecked {
        void allChecked(boolean allChecked);
    }


    public PrintsReceivedAdapter(Activity activity, ArrayList<HistoryListPojo> list, AllChecked allChecked) {
        this.activity = activity;
        this.list = list;
        this.allChecked = allChecked;

    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.history_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        holder.name.setText(Html.fromHtml(list.get(position).getName()));
        holder.bulletPoint.setVisibility(View.VISIBLE);
        holder.checkBox.setVisibility(View.GONE);
//        holder.name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.checkBox.isChecked()){
//                    holder.checkBox.setChecked(false);
//                    checkChangedLocal(position,false);
//                }else {
//                    holder.checkBox.setChecked(true);
//                    checkChangedLocal(position,true);
//                }
//            }
//        });
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(list.get(position).isChecked());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
              checkChangedLocal(position,b);
            }
        });
    }

    private void checkChangedLocal(int position, boolean b) {
        list.get(position).setChecked(b);
        AppConstant.printReceivedList.get(position).setChecked(b);
        AppConstant.printReceivedList.get(position).setValue(b);
        boolean isAllSelected = false;
        for (int i = 0; i < AppConstant.printReceivedList.size(); i++) {
            if (!AppConstant.printReceivedList.get(i).isChecked()) {
                isAllSelected = AppConstant.printReceivedList.get(i).isChecked();
                break;
            }
            isAllSelected = AppConstant.printReceivedList.get(i).isChecked();
        }
        allChecked.allChecked(isAllSelected);
        notifyByHandler();
    }

    private void notifyByHandler() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        }, 100);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name,bulletPoint;
        CheckBox checkBox;

        TypeRow(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            name = itemView.findViewById(R.id.name);
            bulletPoint = itemView.findViewById(R.id.bulletPoint);
        }
    }
}
