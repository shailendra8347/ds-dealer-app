package com.volga.dealershieldtablet.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class  LanguageSelectionAdapter extends RecyclerView.Adapter<LanguageSelectionAdapter.TypeRow> {

    private final Activity activity;
    private final DealershipData dealershipData;
    private final DealershipDetails dealershipDetails;
    private final SettingsAndPermissions settingPermission;
    int lastSelected = 0;
    private int row_index = -1;

    public interface IOnLanguageSelect {
        void languageSelected(String selectedLanguage, int selectedLanguageId);
    }

    IOnLanguageSelect iOnLanguageSelect;

    public LanguageSelectionAdapter(Activity activity, IOnLanguageSelect iOnLanguageSelect, DealershipData dealershipData) {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        dealershipDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getDealershipDetails();
        settingPermission = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
        String selectedLanguage;
        if (settingPermission.isCoBuyer()) {
            selectedLanguage = settingPermission.getCoBuyerSelectedLanguage();
        } else {
            selectedLanguage = settingPermission.getSelectedLanguage();
        }
        if (selectedLanguage != null) {
            for (int i = 0; i < dealershipData.where(dealershipDetails.getDealershipId()).getLanguages().size(); i++) {

                if (selectedLanguage.equalsIgnoreCase(dealershipData.where(dealershipDetails.getDealershipId()).getLanguages().get(i).getLanguageName())) {
                    row_index = i;
                    break;
                }
            }
        }
        this.activity = activity;
        this.iOnLanguageSelect = iOnLanguageSelect;
        this.dealershipData = dealershipData;
    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.language_selection_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        holder.name.setText(dealershipData.where(dealershipDetails.getDealershipId()).getLanguages().get(position).getDisplayName());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnLanguageSelect.languageSelected(dealershipData.where(dealershipDetails.getDealershipId()).getLanguages().get(position).getLanguageName(), dealershipData.where(dealershipDetails.getDealershipId()).getLanguages().get(position).getLanguageId());
                row_index = position;
                notifyDataSetChanged();
            }
        });

        if (row_index == position) {
//            holder.name.setTextSize(16);
//            holder.name.setTypeface(face);
            holder.name.setTextColor(activity.getResources().getColor(R.color.white));
            holder.name.setBackground(activity.getDrawable(R.drawable.rounded_corner_appcolor));

        } else {
            holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            holder.name.setBackground(activity.getDrawable(R.drawable.rounded_corner_white));
            //            holder.name.setTextSize(14);
//            holder.name.setTypeface(face);
        }
        holder.name.setPadding(20, 20, 20, 20);
    }

    @Override
    public int getItemCount() {
        return dealershipData.where(dealershipDetails.getDealershipId()).getLanguages().size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name;

        public TypeRow(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
        }
    }
}
