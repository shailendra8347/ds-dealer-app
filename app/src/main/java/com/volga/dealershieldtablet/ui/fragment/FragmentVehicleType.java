package com.volga.dealershieldtablet.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.DealershipDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SyncUp;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.Calendar;
import java.util.Objects;
import java.util.Random;

import timber.log.Timber;

/**
 * Created by ${Shailendra} on 04-05-2018.
 */
public class FragmentVehicleType extends BaseFragment {


    private View mView;
    private DSTextView next;
    private Button buy, lease;
    private String local = "used";
    private static final int REQUEST_STORAGE = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_vehical_type, container, false);
        initView();
        return mView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            showFragmentInPortrait();
//            updateData();

        }
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (lease != null && !(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() == null || recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().length() == 0)) {
            visibleHintCalled = true;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase(getString(R.string.used))) {
                buy.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));

                lease.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
                next.setVisibility(View.VISIBLE);
            } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                buy.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
                lease.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                next.setVisibility(View.VISIBLE);
            }
        }
    }


    private void initView() {
//        DSTextView logout = (DSTextView) mView.findViewById(R.id.txt_logout);
        DSTextView back = (DSTextView) mView.findViewById(R.id.text_cancelBack);
        back.setVisibility(View.INVISIBLE);
        next = (DSTextView) mView.findViewById(R.id.text_next);
        DSTextView cancel = (DSTextView) mView.findViewById(R.id.text_back_up);
        buy = mView.findViewById(R.id.buy);
        lease = mView.findViewById(R.id.lease);
        lease.setOnClickListener(mOnClickListener);
        buy.setOnClickListener(mOnClickListener);
//        logout.setOnClickListener(mOnClickListener);
        next.setOnClickListener(mOnClickListener);
        next.setVisibility(View.GONE);
        back.setOnClickListener(mOnClickListener);
        cancel.setOnClickListener(mOnClickListener);

        mView.setFocusableInTouchMode(true);
        mView.requestFocus();

        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
//        if (!visibleHintCalled) {
//            updateData();
//        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_cancelBack:
                    break;
                case R.id.text_next:
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (local.length() != 0) {
                            setRecordData(local);
                            iGoToNextPage.whatNextClick();
                        } else {
                            new CustomToast(getActivity()).alert("Please select Vehicle type");
                        }
                    } else {
                        askCameraPermission();
                    }
                    break;
                case R.id.text_back_up:
//                    Gson gson1 = new GsonBuilder().create();
//                    RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//                    if (recordData1.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
//                        ((ViewPagerActivity) Objects.requireNonNull(getActivity())).setCurrentPage(6);
//                    } else {
//                        iGoToNextPage.goToBackIndex();
//                    }
//                    String recordDataString1 = gson1.toJson(recordData1);
//                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);

                    iGoToNextPage.goToBackIndex();

                    break;
                case R.id.txt_logout:
                    logout();
                    break;
                case R.id.buy:
//                    setRecordData("Used");
                    local = "Used";
                    next.setVisibility(View.VISIBLE);
                    buy.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    lease.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
                    buy.setTextColor(getActivity().getColor(R.color.white));
                    lease.setTextColor(getActivity().getColor(R.color.black));

                    break;
                case R.id.lease:
                    local = "New";
//                    setRecordData("New");
                    next.setVisibility(View.VISIBLE);
                    buy.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
                    lease.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
                    lease.setTextColor(getActivity().getColor(R.color.white));
                    buy.setTextColor(getActivity().getColor(R.color.black));
                    break;
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        askCameraPermission();
        askStoragePermission();
    }

    private void askStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            FragmentLogin.ConfirmationDialogFragment
                    .newInstance(R.string.storage_permission_confirmation,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_STORAGE,
                            R.string.storage_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
    }

    IGoToNextPage iGoToNextPage;
    private static final String FRAGMENT_DIALOG = "dialog";
    private static final int REQUEST_CAMERA_PERMISSION = 3;

    private void askCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.CAMERA)) {
            FragmentScanDMVFront.ConfirmationDialogFragment
                    .newInstance(R.string.camera_permission_confirmation,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA_PERMISSION,
                            R.string.camera_permission_not_granted)
                    .show(getActivity().getSupportFragmentManager(), FRAGMENT_DIALOG);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    private void setRecordData(String used) {

        Random rand = new Random();

        // Generate random integers in range 0 to 9999
        String rand_int1;
//        if (PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//            rand_int1 = String.format(Locale.getDefault(), "%09d", rand.nextInt(100000001));
        rand_int1 = System.currentTimeMillis() + "";/*String.format(Locale.getDefault(), "%09d", rand.nextInt(100000001));*/
//            System.currentTimeMillis();
        Log.e("Random number: ", rand_int1);
        PreferenceManger.putString(AppConstant.DMV_NUMBER, rand_int1);
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        if (PreferenceManger.getStringValue(AppConstant.RECORD_DATA).length() != 0) {
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        } else {
            recordData = new RecordData();
        }
        Record record = new Record();
        CustomerDetails customerDetails = new CustomerDetails();
        CoBuyerCustomerDetails coBuyerCustomerDetails = new CoBuyerCustomerDetails();
        VehicleDetails vehicleDetails = new VehicleDetails();
        TradeInVehicle tradeInVehicle = new TradeInVehicle();
        SettingsAndPermissions settingsAndPermissions = new SettingsAndPermissions();
        settingsAndPermissions.setLocalLogs("");
        SyncUp syncUp = new SyncUp();
        DealershipDetails dealershipDetails = new DealershipDetails();
        dealershipDetails.setDealershipId(PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID));
        dealershipDetails.setDealershipName(PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
        DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
        if (dealershipData != null) {
            for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                    dealershipDetails.setVehiclePhotoTypes(dealershipData.getDealerships().get(i).getDealershipVehiclePhotoTypes());
                    break;
                }
            }
        }
        settingsAndPermissions.setLastPageIndex(((NewDealViewPager) Objects.requireNonNull(getActivity())).getCurrentPage());
        settingsAndPermissions.setLatestTimeStamp(Calendar.getInstance().getTime().toString());
        Timber.e(Calendar.getInstance().getTime().toString());
        vehicleDetails.setTypeOfVehicle(used);
        customerDetails.setDLNumber(rand_int1);
        record.setCustomerDetails(customerDetails);
        record.setCoBuyerCustomerDetails(coBuyerCustomerDetails);
        record.setDealershipDetails(dealershipDetails);
        record.setTradeInVehicle(tradeInVehicle);
        record.setSettingsAndPermissions(settingsAndPermissions);
        record.setSyncUp(syncUp);
        record.setVehicleDetails(vehicleDetails);
        recordData.getRecords().add(record);
        //Add the recordData object to the SharedPreference after converting it to string
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//        } else {
//            rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
//            Gson gson = new GsonBuilder().create();
//            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//            recordData.where(rand_int1).getVehicleDetails().setTypeOfVehicle(used);
//            String recordDataString1 = gson.toJson(recordData);
//            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//        }
    }

}
