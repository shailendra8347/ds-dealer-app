package com.volga.dealershieldtablet.ui.customUI;

/**
 * Created by Abhishek Thanvi on 21/02/17.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.ui.adapter.TextWatcherAdapter;


/** Copyright 2014 Alex Yanchenko
 * To change clear icon, set
 * <p/>
 * <pre>
 * android:drawableRight="@drawable/custom_icon"
 * </pre>
 */
public class DSEdittext extends androidx.appcompat.widget.AppCompatEditText implements View.OnTouchListener,
        View.OnFocusChangeListener, TextWatcherAdapter.TextWatcherListener {



    public interface Listener {
        void didClearText();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private Drawable xD;
    private Listener listener;

    public DSEdittext(Context context) {
        super(context);
     }

    public DSEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public DSEdittext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context,attrs);
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        this.l = l;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f) {
        this.f = f;
    }

    private OnTouchListener l;
    private OnFocusChangeListener f;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - xD
                    .getIntrinsicWidth());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    setText("");
                    if (listener != null) {
                        listener.didClearText();
                    }
                }
                return true;
            }
        }
        if (l != null) {
            return l.onTouch(v, event);
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
//        if (hasFocus) {
//            setClearIconVisible(!TextUtils.isEmpty(getText()));
//        } else {
//            setClearIconVisible(false);
//        }
        if (f != null) {
            f.onFocusChange(v, hasFocus);
        }
    }

    @Override
    public void onTextChanged(EditText view, String text) {
//        if (isFocused()) {
//            setClearIconVisible(!TextUtils.isEmpty(text));
//        }
    }

    private void init(Context context , AttributeSet attrs) {
//        xD = getCompoundDrawables()[2];
//        if (xD == null) {
//            xD = getResources()
//                    .getDrawable(android.R.drawable.presence_offline);
//        }
//        xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
//        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(new TextWatcherAdapter(this, this));

        try {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView, 0, 0);

            String font_name = a.getString(R.styleable.CustomFontTextView_custom_ds_font);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), font_name);
            a.recycle();
            setTypeface(typeface);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setClearIconVisible(boolean visible) {
        Drawable x = visible ? xD : null;
        setCompoundDrawables(getCompoundDrawables()[0],
                getCompoundDrawables()[1], x, getCompoundDrawables()[3]);
    }
}