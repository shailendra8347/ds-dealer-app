package com.volga.dealershieldtablet.utils;

import androidx.annotation.IntDef;
import androidx.annotation.StringRes;
import android.widget.Toast;

import com.volga.dealershieldtablet.app.DSAPP;


/**
 * Created by
 */

public class ToastUtils {

    public static void shortToast(@StringRes int text) {
        shortToast(DSAPP.getInstance().getString(text));
    }

    public static void shortToast(String text) {
        show(text, Toast.LENGTH_SHORT);
    }

    public static void longToast(@StringRes int text) {
        longToast(DSAPP.getInstance().getString(text));
    }

    public static void longToast(String text) {
        show(text, Toast.LENGTH_LONG);
    }

    private static Toast makeToast(String text, @ToastLength int length) {
        return Toast.makeText(DSAPP.getInstance(), text, length);
    }

    private static void show(String text, @ToastLength int length) {
        makeToast(text, length).show();
    }

    @IntDef({Toast.LENGTH_LONG, Toast.LENGTH_SHORT})
    private @interface ToastLength {

    }
}