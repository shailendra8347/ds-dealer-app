package com.volga.dealershieldtablet.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.volga.dealershieldtablet.R;

import java.util.Objects;


/**
 * Created by SKM
 */

public class CustomToast {
    public Context context;


    public CustomToast(Context context) {
        this.context = context;
    }

    public void alert(String msg) {

        if (context != null) {
            final BottomSheetDialog dialog = new BottomSheetDialog(context);
            dialog.setContentView(R.layout.popup_view);
            TextView text = (TextView) dialog.findViewById(R.id.text);

            if (text != null) {
                text.setText(msg);
            }
            if (context != null && !dialog.isShowing() && !((Activity) context).isFinishing()) {
                dialog.show();
            }
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            (Objects.requireNonNull(dialog.getWindow())).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setBackgroundResource(android.R.color.transparent);
            dialog.setCancelable(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (context != null&&dialog!=null && dialog.isShowing() && !((Activity) context).isFinishing()) {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            Log.e("Error in dismissing:",e.toString());
                        }
                    }
                }
            }, 3000);
        }
//        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
//        View layout = inflater.inflate(R.layout.custom_toast, null);
//        TextView text = (TextView) layout.findViewById(R.id.text);
//        text.setText(msg);
//        Toast toast = new Toast(context);
////        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//        toast.show();
    }

    public void alert(String msg, boolean cancelable) {

        if (context != null) {
            final BottomSheetDialog dialog = new BottomSheetDialog(context);
            dialog.setContentView(R.layout.popup_view);
            TextView text = (TextView) dialog.findViewById(R.id.text);
            assert text != null;
            text.setText(msg);
            if (!dialog.isShowing() && context != null && !((Activity) context).isFinishing()) {
                dialog.show();
            }
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            (Objects.requireNonNull(dialog.getWindow())).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setBackgroundResource(android.R.color.transparent);
            dialog.setCancelable(cancelable);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (dialog.isShowing() && context != null && !((Activity) context).isFinishing()) {
                        dialog.dismiss();
                    }
                }
            }, 3000);
        }
//        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
//        View layout = inflater.inflate(R.layout.custom_toast, null);
//        TextView text = (TextView) layout.findViewById(R.id.text);
//        text.setText(msg);
//        Toast toast = new Toast(context);
////        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//        toast.show();
    }

    public void toast(String msg) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, null);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(msg);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}
