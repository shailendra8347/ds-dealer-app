package com.volga.dealershieldtablet.utils;

public enum SwipeDirection {
    all, left, right, none ;
}