package com.volga.dealershieldtablet.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Sajeev on 18-08-2017.
 */

public class DateUtils {
    public static String parseQuotationDate(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy HH:mm";
//        String outputPattern = "EEE dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseOrderDateWithOutDay(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy, hh:mm aa ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static boolean compareTwoDates(String oldDate, String newDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        try {
            Date newD = sdf.parse(newDate);
            Date old = sdf.parse(oldDate);
//            Logger.ErrorLog("Time", old.getTime() + " - " + newD.getTime());
            if (old.getTime() < newD.getTime()) {
                return true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static String getDateTime() {

        String datefor = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat dateFormat = new SimpleDateFormat(datefor,
                Locale.getDefault());
        Date date = new Date();

        return dateFormat.format(date);

    }

    public static String getTimeZone(){
        Log.e("Offset Of TimeZone ",TimeZone.getDefault().getRawOffset()+"");
        return TimeZone.getDefault().getID();

    }

}
