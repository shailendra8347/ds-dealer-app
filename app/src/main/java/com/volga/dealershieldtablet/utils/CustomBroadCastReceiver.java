package com.volga.dealershieldtablet.utils;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.services.MyNewService;

import static com.volga.dealershieldtablet.utils.AppConstant.isNetworkAvail;


public class CustomBroadCastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (isNetworkAvail(context)) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (!DSAPP.isAppIsInBackground(context)) {
                        context.startService(new Intent(context, MyNewService.class));
                    } else {
                        context.startForegroundService(new Intent(context, MyNewService.class));
                    }
                } else {
                    context.startService(new Intent(context, MyNewService.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}