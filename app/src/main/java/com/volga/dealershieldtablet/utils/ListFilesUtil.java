package com.volga.dealershieldtablet.utils;

import java.io.File;

public class ListFilesUtil {
    /**
     * List all the files and folders from a directory
     *
     * @param directoryName to be listed
     */
    public static File[] listFilesAndFolders(String directoryName) {
        File directory = new File(directoryName);
        return directory.listFiles();
    }

    /**
     * List all the files under a directory
     *
     * @param directoryName to be listed
     */
    public void listFiles(String directoryName) {
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                System.out.println(file.getName());
            }
        }
    }

    /**
     * List all the folder under a directory
     *
     * @param directoryName to be listed
     */
    public void listFolders(String directoryName) {
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isDirectory()) {
                System.out.println(file.getName());
            }
        }
    }

    /**
     * List all files from a directory and its subdirectories
     *
     * @param directoryName to be listed
     */
    public void listFilesAndFilesSubDirectories(String directoryName) {
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()) {
                listFilesAndFilesSubDirectories(file.getAbsolutePath());
            }
        }
    }

    public static void main(String[] args) {
        ListFilesUtil listFilesUtil = new ListFilesUtil();
        final String directoryLinuxMac = "/Users/loiane/test";
        //Windows directory example
        final String directoryWindows = "C://test";
        listFilesUtil.listFiles(directoryLinuxMac);
    }
}