package com.volga.dealershieldtablet.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.TabUniqueName;
import com.volga.dealershieldtablet.app.DSAPP;


/**
 * Created by SKM on 08/02/18.
 */

public class PreferenceManger {

    /**
     * preference keys
     */
    public static String DS_APP_PREFS = "ds_app_preferences";
    public static String MODE_PREF = "mode_preferences";
    public static String SUB_MODE_PREF = "sub_mode_preferences";

    public static String SUB_SUB_MODE_PREF = "sub_sub_mode_preferences";

    public static String ONE_TIME_CREATION = "created";


    private static SharedPreferences preferences;

    private PreferenceManger() {
    }

    private static synchronized SharedPreferences getInstance() {
        if (preferences == null) {
            preferences = DSAPP.getInstance().getSharedPreferences(DS_APP_PREFS, Context.MODE_PRIVATE);
        }
        return preferences;
    }

    private static SharedPreferences.Editor getEditor() {
        return getInstance().edit();
    }

    public static void remove(String key) {
        SharedPreferences.Editor editor = getEditor();
        editor.remove(key);
        editor.apply();
    }

    public static void putString(String key, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(key, value);
        editor.apply();
    }


    public static void putInt(String key, int value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static TabUniqueName getUniqueTabName() {
        Gson gson = new Gson();
        String json = getStringValue(AppConstant.UNIQU_TAB_OBJECT);
        return gson.fromJson(json, TabUniqueName.class);
    }

    public static String getStringValue(String key) {
        return getInstance().getString(key, "");
    }

    public static boolean getBooleanValue(String key) {
        return getInstance().getBoolean(key, false);
    }

    public static int getIntegerValue(String key) {
        return getInstance().getInt(key, 0);
    }


    public static void logoutUser() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear();
        editor.commit();
    }
}
