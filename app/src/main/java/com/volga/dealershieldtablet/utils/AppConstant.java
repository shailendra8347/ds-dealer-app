package com.volga.dealershieldtablet.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;

import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ${Shailendra} on 09-05-2018.
 */
public class AppConstant {

    public static final String CURRENT_LANG = "current_lang";
    public static final String IS_CO_BUYER = "is_co_buyer";
    public static final String IS_TRADE_IN = "is_trade_in";
    public static final String IS_CO_BUYER_SELECTED = "co_buyer_selected";
    public static final String IS_TRADE_IN_SELECTED = "trade_in_selected";
    public static final String SKIPPED = "skipped";
    public static final String LOGIN_STAMP = "login_stamp";
    public static final String CHECK_ID = "check_id";
    public static final String CHECK_ID_DATA = "check_id_data";
    public static final String TAKE_USER_PHOTO = "TAKE_USER_PHOTO";
    public static final String TAKE_SCREENSHOT = "TAKE_SCREENSHOT";
    public static final String THIRD_PARTIES = "third_parties";
    public static final int LONG_DELAY = 2000;
    public static final int MID_DELAY = 1000;
    public static final String CONDITION_DISCLOSURE = "condition_disclosure";
    public static final String HISTORY_DISCLOSURE = "history_disclosure";
    public static final String THIRD_PARTY_REPORT = "third_party_report";
    public static final String VIEW_MANDATORY = "view_mandatory";
    public static final String CONDITION_DISCLOSURE_NEW = "condition_disclosure_new";
    public static final String USE_THIRD_PARTY_API = "use_third_party_api";
    public static final String ALERT = "alert";
    public static final String ALERT_AVAILABLE = "alert_available";
    public static final String IS_AI_ENABLED = "is_ai_enabled";
    public static final String USER_MAIL = "email";
    public static final String TAB_NAME = "tab_name";
    public static final String IS_USEDCAR_ONLY = "is_usedcar_only";
    public static long NEXT_DELAY = 400;
    public static int restrictedPage = -1;
    /*http://staging-api.dealershield.net/*/
//    public static String BASE_URL = "http://devapi.dealerxt.com";

    public static String BASE_URL = "http://stgapi.dealerxt.com";
//    public static String BASE_URL = "http://staging-api.dealershield.net/";
//    public static String BASE_URL = "http://api1.dealerxt.com/";

//    public static String BASE_URL = "http://api.dealershield.net/";
    // new url live at 4.0 version

//    public static String BASE_URL = "http://ds.dealerxt.com/";

    //            public static String BASE_URL1 = "http://stg.dealerxt.com";
    //old
//    public static String BASE_URL1 = "https://www.dealerxt.com";
    //new
    public static String BASE_URL1 = "https://app.dealerxt.com";
//    public static String BASE_URL1 = "https://stg.safedealsai.com";
    //https://stg.safedealsai.com/
//    app.dealerxt.com

    public static String UNIQU_TAB_OBJECT = "unique_tab_object";
    public static String REFRESH_TOKEN = "refresh_token";
    public static String SALES_PERSON = "sales_person";
    public static String CHECK_LIST = "check_list";
    public static String VERBAL_PROMISE = "verbal_promise";
    public static String SELECTED_DEALERSHIP_NAME = "dealership_name";
    public static boolean shouldCall = false;
    public static boolean TAKE_PHOTO_OR_NOT = true;
    public static String USER_ROLE = "user_role";
    public static String PROCESS_TRADE_IN = "process_trade_in";
    public static String PROCESS_MULTI_SIGN = "process_multi_sign";
    public static String SKIP_INSURANCE = "skip_insurance";
    public static String SKIP_PHOTOS = "skip_photos";
    public static String TIME_STAMP="time_stamp";
    public short demmo = Short.parseShort(null);

    public static final String ACCESS_TOKEN = "access_token";
    /*Shailendra*/
//    public static final String PUSHLINK_KEY = "v8fgvttm7hpqrrl3";

    /*dealerXT*/
//    public static final String PUSHLINK_KEY = "dllj0ejo109ui6jn";

    /*dealerXT Staging*/
    public static final String PUSHLINK_KEY = "ofu36ikqecdf3b85";

    public static final String VIN_QUERY_BASE_URL = "http://ws.vinquery.com";
    public static final String VIN_QUERY_ACCESS_TOKEN = "065fe852-6109-48db-8bd0-c7fd970af087";

    public static final String DEALERSHIP_DATA = "dealership_data";
    public static final String RECORD_DATA = "record_data";
    public static final String SELECTED_DEALERSHIP_ID = "selected_dealership_id";

    public static final String FROM_FRAGMENT = "from_fragment";
    public static final String DMV_FRONT = "dmv_front";
    public static final String DMV_NUMBER = "dmv_number";

    public static final String USER_NAME = "username";

    public static final String IS_NO_EMAIL_SELECTED = "no_email";
    public static final String CAR_FRONT = "car_front";
    public static final String CAR_RIGHT = "car_right";
    public static final String CAR_LEFT = "car_left";
    public static final String CAR_REAR = "car_rear";
    public static final String CAR_WINDOW_VIN = "car_window_vin";
    public static final String CAR_MILEAGE = "car_mileage";
    /*additional images*/
    public static final String CAR_ADDITIONAL1 = "car_additional1";
    public static final String CAR_ADDITIONAL2 = "car_additional2";
    public static final String CAR_ADDITIONAL3 = "car_additional3";
    public static final String CAR_ADDITIONAL4 = "car_additional4";
    public static final String CAR_ADDITIONAL5 = "car_additional5";

    public static final String DEALERSHIPS = "dealerships";
    public static final String LOCALE_EN = "en";
    public static final String LOCALE_ES = "es";
    public static final String LOCALE_KO = "ko";
    public static final String LOCALE_ZH = "zh";
    public static final String LOCALE_TL = "tl";
    public static final String LOCALE_VI = "vi";
    public static ArrayList<HistoryListPojo> historyLists = new ArrayList<>();
    public static ArrayList<HistoryListPojo> printReceivedList = new ArrayList<>();
    public static ArrayList<HistoryListPojo> reportLists = new ArrayList<>();
    public static ArrayList<HistoryListPojo> commonList = new ArrayList<>();
    public static ArrayList<HistoryListPojo> condition = new ArrayList<>();
    public static ArrayList<HistoryListPojo> history = new ArrayList<>();
    public static ArrayList<HistoryListPojo> additional = new ArrayList<>();
    public static ArrayList<HistoryListPojo> additionalHistory = new ArrayList<>();
    public static ArrayList<HistoryListPojo> report = new ArrayList<>();


    public static final String MANATEEWORKS_KEY = "9WgcYxPqU+nAM0qbs09TesSPkOIEJFC2TqNQTgraqRk=";
    public static Date history1Time;
    public static Date history2Time;
    public static String INSURANCE = "insurance";
    public static String SKIPPED_PAGE = "skipped_page";

    public static boolean isNetworkAvail(Context mContext) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if (networkConnectivity(mContext)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL(
                        "http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(3000);
                urlc.setReadTimeout(4000);
                urlc.connect();
                // networkcode2 = urlc.getResponseCode();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return (false);
            }
        } else
            return false;
    }

    private static boolean networkConnectivity(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


}
