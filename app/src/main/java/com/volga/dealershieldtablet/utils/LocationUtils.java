package com.volga.dealershieldtablet.utils;

import android.content.Context;
import android.location.LocationManager;
import android.util.Log;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Sonu on 02/09/17.
 */

public class LocationUtils {

    private static final String TAG = LocationUtils.class.getSimpleName();

    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Log.e(TAG, "GPS Provider Enabled");
            return true;
        }
        return false;
    }
}
