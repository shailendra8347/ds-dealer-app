package com.volga.dealershieldtablet.services;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehicleDataUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyService extends Service {
    Calendar cur_cal = Calendar.getInstance();
    private Gson gson = new Gson();
    private Callbacks activity;
    private boolean emailNotCalling = false;


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
//        Intent intent = new Intent(this, MyService.class);
//        PendingIntent pintent = PendingIntent.getService(getApplicationContext(),
//                0, intent, 0);
//        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        cur_cal.setTimeInMillis(System.currentTimeMillis());
//        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(),
//                3000, pintent);
    }


//    @Override
//    public void onStart(Intent intent, int startId) {
//        handleStart(intent, startId);
//    }

    private void handleStart() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

        if (recordData != null) {
            ArrayList<Record> allRecords = new ArrayList<>(recordData.getRecords());
            for (int i = 0; i < allRecords.size(); i++) {
                Date date = new Date();
                try {
                    date = formatter.parse(allRecords.get(i).getSettingsAndPermissions().getLatestTimeStamp());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long lasttime = date.getTime();
                long currentTime = System.currentTimeMillis();
                long diff = currentTime - lasttime;
                int hours = (int) ((currentTime - lasttime) / (1000 * 60 * 60));
//                int hours = (int) ((currentTime - lasttime) / (1000 * 60));
                if (hours > 12 && !allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData1.where(allRecords.get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLead(true);
                    String recordDataString1 = gson.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory( Intent.CATEGORY_HOME );
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                }
            }
        }
        RecordData recordData2 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        ArrayList<Record> recordArrayList = new ArrayList<>();
        if (recordData2 != null) {
            for (int i = 0; i < recordData2.getRecords().size(); i++) {
                if (recordData2.getRecords().get(i).getSettingsAndPermissions() != null) {
                    recordArrayList.add(recordData2.getRecords().get(i));

                }
            }
        }
        //retrofit code for vehicle data sync
        startSyncing(recordArrayList);

    }

    private void startSyncing(ArrayList<Record> recordArrayList) {
        if (recordArrayList.size() > 0) {
            for (int i = 0; i < recordArrayList.size(); i++) {
                if (!recordArrayList.get(i).getSettingsAndPermissions().getIsDataSynced() && recordArrayList.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                    syncVehicleDate(recordArrayList.get(i));
                } else if (!recordArrayList.get(i).getSettingsAndPermissions().getIsRecordComplete() && recordArrayList.get(i).getSettingsAndPermissions().isBussinessLead() && !recordArrayList.get(i).getSettingsAndPermissions().isBussinessLeadSynced()) {
                    syncVehicleDate(recordArrayList.get(i));
                } else {
                    if (recordArrayList.get(i).getSettingsAndPermissions().getCustomerVehicleTradeId() != null) {
                        uploadFile(recordArrayList.get(i));
                    }
                }
//                try {
//                    Log.e("Loop Count: ", "i= " + i);
//                    Thread.sleep(60000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                //sent email
            }
        }

    }

    private VehicleDataUploadResponse getVehicleData(Record record) {

        SimpleDateFormat originalFormatter = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
        SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        Date date;
        VehicleDataUploadResponse response = new VehicleDataUploadResponse();

        /*disclosure*/
        ArrayList<HistoryListPojo> disclosure = record.getVehicleDetails().getUsedVehicleCondition();
        ArrayList<ConditionDisclosures> disclosureList = new ArrayList<>();
        if (disclosure != null) {
            for (int i = 0; i < disclosure.size(); i++) {
                if (disclosure.get(i).isChecked() && i != disclosure.size() - 1) {
                    ConditionDisclosures thirdPartyHistoryReport = new ConditionDisclosures();
                    thirdPartyHistoryReport.setVehicleConditionDisclosureId(disclosure.get(i).getId());
                    thirdPartyHistoryReport.setValue(disclosure.get(i).getAdditionalData());
                    disclosureList.add(thirdPartyHistoryReport);
                }
            }
        }
        response.setConditionDisclosures(disclosureList);


        /*history*/
        ArrayList<HistoryListPojo> listPojos = record.getSettingsAndPermissions().getPrintReceivedList();
        ArrayList<CustomerAgreement> customerAgreements = new ArrayList<>();
        if (listPojos != null) {
            for (int i = 0; i < listPojos.size(); i++) {
                CustomerAgreement customerAgreement = new CustomerAgreement();
                customerAgreement.setCustomerSignAgreementsId(listPojos.get(i).getId());
                customerAgreement.setValue(listPojos.get(i).isValue());
                customerAgreements.add(customerAgreement);
            }
        }
        response.setCustomerAgreement(customerAgreements);

        ArrayList<HistoryListPojo> list = record.getVehicleDetails().getUsedVehicleReport();
        ArrayList<ThirdPartyHistoryReport> historyReports = new ArrayList<>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isChecked() && i != list.size() - 1) {
                    ThirdPartyHistoryReport thirdPartyHistoryReport = new ThirdPartyHistoryReport();
                    thirdPartyHistoryReport.setThirdPartyHistoryReportId(list.get(i).getId());
                    historyReports.add(thirdPartyHistoryReport);
                }
            }
        }
        response.setThirdPartyHistoryReport(historyReports);
        ArrayList<HistoryListPojo> list1 = record.getVehicleDetails().getUsedVehicleHistory();
        ArrayList<HistoryDisclosures> history = new ArrayList<>();
        if (list1 != null) {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i).isChecked() && i != list1.size() - 1) {
                    HistoryDisclosures thirdPartyHistoryReport = new HistoryDisclosures();
                    thirdPartyHistoryReport.setVehicleHistoryDisclosureId(list1.get(i).getId());
                    try {
                        if (list1.get(i).getId().equalsIgnoreCase("15")) {
                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate1().toString())));
                        } else if (list1.get(i).getId().equalsIgnoreCase("16")) {
                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate2().toString())));
                        } else {
                            thirdPartyHistoryReport.setDate("");
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    history.add(thirdPartyHistoryReport);
                }
            }
        }
        response.setHistoryDisclosures(history);

        try {
            if (record.getCustomerDetails().getDateOfBirth() != null && record.getCustomerDetails().getDateOfBirth().length() > 0) {
                date = originalFormatter.parse(record.getCustomerDetails().getDateOfBirth());
                String dob = newFormatter.format(date);
                response.setDateOfBirth(record.getCustomerDetails().getDateOfBirth());
            }

            response.setDrivingLicenseExpiryDate(record.getCustomerDetails().getDLExpiryDate());
            response.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
            Log.e("TradeID date: ", tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<SalesPersons> salesPersons = new ArrayList<>();
        if (record.getCustomerDetails().getSalesPersons() != null && record.getCustomerDetails().getSalesPersons().getFirstName().length() > 0)
            salesPersons.add(record.getCustomerDetails().getSalesPersons());
        response.setSalesPersons(salesPersons);

        response.setAddress1(record.getCustomerDetails().getAddressLineOne() == null ? "" : record.getCustomerDetails().getAddressLineOne().trim());
        response.setTermConditionAccepted(record.getCustomerDetails().isTermConditionAccepted());
        response.setAddress2(record.getCustomerDetails().getAddressLineTwo() == null ? "" : record.getCustomerDetails().getAddressLineTwo().trim());
        response.setAllow2RemoveWindowSticker(record.getSettingsAndPermissions().getStickerRemovalPermission());
        if (record.getVehicleDetails().getTypeOfVehicle() != null) {
            if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase(getString(R.string.used))) {
                response.setCarTypeId("1");
            } else {
                response.setAcceptedUsedCarPolicy(record.getSettingsAndPermissions().isAcceptedUsedCarPolicy());
                response.setCarTypeId("2");
            }
        }
        response.setCity(record.getCustomerDetails().getCity());
        response.setContactNumber(record.getCustomerDetails().getMobileNumber());

        response.setLead(record.getSettingsAndPermissions().isBussinessLead());
        response.setLastScreenName(getScreenName(record.getSettingsAndPermissions().getLastPageIndex()));
        response.setDrivingLicenseNumber(record.getCustomerDetails().getDrivingLicenceNumber());
        response.setDealershipId(record.getDealershipDetails().getDealershipId() + "");
        response.setEmail(record.getCustomerDetails().getEmail() == null ? "".trim() : record.getCustomerDetails().getEmail().trim());
        response.setZipcode(record.getCustomerDetails().getZipCode());
        response.setVIN(record.getVehicleDetails().getVINNumber());
        response.setMake(record.getVehicleDetails().getCarMake());
        response.setModel(record.getVehicleDetails().getCarModel());
        response.setModelYear(record.getVehicleDetails().getCarYear());
        response.setMileage(record.getVehicleDetails().getMileage() + "");
        response.setStockNumber(record.getVehicleDetails().getStockNumber());
        if (record.getSettingsAndPermissions().isHasCoBuyer()) {
            CoBuyerCustomerDetails coBuyerCustomerDetails = record.getCoBuyerCustomerDetails();
            coBuyerCustomerDetails.setTestDriveTaken(record.getSettingsAndPermissions().isCoBuyerTestDriveTaken());
            coBuyerCustomerDetails.setTestDriveTimeId(record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId()==0?null:record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId()+"");
            coBuyerCustomerDetails.setNoTestDriveConfirm(record.getSettingsAndPermissions().isCoBuyerNoTestDriveConfirm());
            /*Cobuyer module*/
            if (record.getCoBuyerCustomerDetails().getGender() != null) {
                if (record.getCoBuyerCustomerDetails().getGender().equalsIgnoreCase("male"))
                    coBuyerCustomerDetails.setGender("1"/*record.getCustomerDetails().getGender()*/);
                else if (record.getCustomerDetails().getGender().equalsIgnoreCase("female"))
                    coBuyerCustomerDetails.setGender("2"/*record.getCustomerDetails().getGender()*/);
                else {
                    coBuyerCustomerDetails.setGender("3"/*record.getCustomerDetails().getGender()*/);
                }
            }
            coBuyerCustomerDetails.setLanguageId(record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "");
            response.setCoBuyerDetail(coBuyerCustomerDetails);
        }
        if (record.getTradeInVehicle().getVINNumber() != null && record.getTradeInVehicle().getVINNumber().length() > 0) {
            response.setTradeInVehicle(record.getTradeInVehicle());
        }
        response.setTradeTypeId(record.getVehicleDetails().getTypeOfPurchase());

        response.setTestDriveDurationId(record.getSettingsAndPermissions().getTestDriveTimeId()+"");
        response.setState(record.getCustomerDetails().getState());
        response.setCountry(record.getCustomerDetails().getCountry());
        response.setLastName(record.getCustomerDetails().getLastName());
        response.setFirstName(record.getCustomerDetails().getFirstName());
        response.setMiddleName(record.getCustomerDetails().getMiddleName());
        response.setLanguageId(record.getSettingsAndPermissions().getSelectedLanguageId());
        response.setLocationId(0);
        response.setIsTestDriveTaken(record.getSettingsAndPermissions().getTestDriveTaken());
        if (record.getCustomerDetails().getGender() != null && record.getCustomerDetails().getGender().equalsIgnoreCase("male"))
            response.setGenderTypeId(1/*record.getCustomerDetails().getGender()*/);
        else if (record.getCustomerDetails().getGender() != null && record.getCustomerDetails().getGender().equalsIgnoreCase("female"))
            response.setGenderTypeId(2/*record.getCustomerDetails().getGender()*/);
        else {
            response.setGenderTypeId(3/*record.getCustomerDetails().getGender()*/);
        }

        // transfer the data
        return response;
    }

    private String getScreenName(int lastPageIndex) {
        String pageName = "Default Page";
        switch (lastPageIndex) {

            case 0:
                pageName = "Contact info";
                break;
            case 1:
                pageName = "DMV Front";
                break;
            case 2:
                pageName = "DMV Front Preview";
                break;
            case 3:
                pageName = "Co-Buyer Selection";
                break;
            case 4:
                pageName = "Scan DMV Back";
                break;
            case 5:
                pageName = "License info";
                break;
            case 6:
                pageName = "License info confirmation";
                break;
            case 7:
                pageName = "Vehicle Type";
                break;
            case 8:
                pageName = "Trade IN";
                break;
            case 9:
                pageName = "Scan VIN";
                break;
            case 10:
                pageName = "Vehicle info after Scan";
                break;
            case 11:
                pageName = "Front Image";
                break;
            case 12:
                pageName = "Front Image Preview";
                break;
            case 13:
                pageName = "Passengers Side Image";
                break;
            case 14:
                pageName = "Passengers Side Image Preview";
                break;
            case 15:
                pageName = "Back Side Image";
                break;
            case 16:
                pageName = "Back Side Image Preview";
                break;
            case 17:
                pageName = "Drivers Side Image";
                break;
            case 18:
                pageName = "Drivers Side Image Preview";
                break;

            case 19:
                pageName = "Window Sticker Image";
                break;
            case 20:
                pageName = "Window Sticker Image Preview";
                break;
            case 21:
                pageName = "Odometer Image";
                break;
            case 22:
                pageName = "Odometer Image Preview";
                break;
            case 23:
                pageName = "Mileage entry";
                break;
            case 24:
                pageName = "Add More Image";
                break;
            case 25:
                pageName = "Add More Image 1";
                break;
            case 26:
                pageName = "Add More Image 1 Preview";
                break;
            case 27:
                pageName = "Add More Image 2";
                break;
            case 28:
                pageName = "Add More Image 2 Preview";
                break;
            case 29:
                pageName = "Add More Image 3";
                break;
            case 30:
                pageName = "Add More Image 3 Preview";
                break;
            case 31:
                pageName = "Add More Image 4";
                break;
            case 32:
                pageName = "Add More Image 4 Preview";
                break;
            case 33:
                pageName = "Add More Image 5";
                break;
            case 34:
                pageName = "Add More Image 5 Preview";
                break;
            case 35:
                pageName = "Finance Manager";
                break;
            case 36:
                pageName = "Vehicle Disclosure";
                break;
            case 37:
                pageName = "Vehicle Condition Selection";
                break;
            case 38:
                pageName = "Vehicle History Selection";
                break;
            case 39:
                pageName = "Vehicle History Report Selection";
                break;
            case 40:
                pageName = "Add Customer Insurance Image";
                break;
            case 41:
                pageName = "Insurance Image";
                break;
            case 42:
                pageName = "Insurance Image Preview";
                break;
            case 43:
                pageName = "Customer Handover";
                break;
            case 44:
                pageName = "Language Selection";
                break;
            case 45:
                pageName = "Type of Purchase";
                break;
            case 46:
                pageName = "Confirm Car Image";
                break;
            case 47:
                pageName = "Confirm car info";
                break;
            case 48:
                pageName = "Test Drive";
                break;
            case 49:
                pageName = "Window Sticker removal permission";
                break;
            case 50:
                pageName = "Vehicle Condition ";
                break;
            case 51:
                pageName = "Vehicle History ";
                break;
            case 52:
                pageName = "History Report";
                break;
            case 53:
                pageName = "Thank you user 1";
                break;
            case 54:
                pageName = "Final report";
                break;
            case 55:
                pageName = "Print Received";
                break;
            case 56:
                pageName = "Handover Co-Buyer";
                break;
            case 57:
                pageName = "Thank you user 2";
                break;
        }
        return pageName;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {
        start5MinFunction();

        return START_NOT_STICKY;
    }

    private void start5MinFunction() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            //your method here
                            Log.e("Every 5 min : ", " startSyncing ");
                            handleStart();
                        } catch (Exception e) {
                            Log.e("In onStartCommand", "run: handleStart & startSyncing " + e.toString());
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 300000);
    }

    private void syncVehicleDate(final Record record) {
        VehicleDataUploadResponse vehicleDataUploadResponse = null;
        try {
            vehicleDataUploadResponse = getVehicleData(record);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Json", gson.toJson(vehicleDataUploadResponse));
        retrofit2.Call<ImageUploadResponse> call = RetrofitInitialization.getDs_services().uploadVehicleDetail("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vehicleDataUploadResponse);
        call.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.isSuccessful() && response.body().isSucceeded()) {
                    Log.e("Vehicle Info uploaded :", "onResponse: " + record.getCustomerDetails().getDLNumber());
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (!record.getSettingsAndPermissions().isBussinessLead()) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIsDataSynced(response.body().isSucceeded());
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCustomerVehicleTradeId(response.body().getValue());
                    } else {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCustomerVehicleTradeId(response.body().getValue());
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLeadSynced(response.body().isSucceeded());
                    }
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    /*upload file*/
                    if (response.body().getValue() != null) {
                        Gson gson1 = new GsonBuilder().create();
                        RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        uploadFile(recordData1.where(record.getCustomerDetails().getDLNumber()));
                    }

//                    startSyncing();
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Response ", "onResponse: " + json.toString());
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                if (t.getLocalizedMessage().contains("timeout")) {
                    handleStart();
                }
                Log.e("MyService class error: ", t.getLocalizedMessage());

            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void uploadFile(final Record record) {

        String path_licence, path_signature, path_car;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        } else if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        /*Car Images*/
        else if (new File(path_car + "/IMG_front.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        } else if (new File(path_car + "/IMG_right.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        } else if (new File(path_car + "/IMG_rear.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        } else if (new File(path_car + "/IMG_left.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        } else if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        } else if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        } else if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        } else if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/
        else if (new File(path_car + "/IMG_Additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            startImageSync(new File(path_car + "/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        } else if (new File(path_car + "/IMG_Additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        } else if (new File(path_car + "/IMG_Additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        } else if (new File(path_car + "/IMG_Additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        } else if (new File(path_car + "/IMG_Additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        } else if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        } else if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        } else if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        } else if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        } else if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        } else if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        else if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        } else if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        } else if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        } else if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        } else if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        } else if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        } else if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        } else if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        } else if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        } else if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        } else if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        } else if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        } else if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        } else if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        } else if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        } else if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        } else if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        } else if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        } else {
            //sendEmail
            if (!record.getSettingsAndPermissions().isBussinessLead()) {

                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (!recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().isEmailSent()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                sendEmailToCustomer(record);
                            } catch (Exception e) {
                                emailNotCalling = false;
                                e.printStackTrace();
                            }
                        }
                    }, 10000);
                }
            } else {
                emailNotCalling = false;
                if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLeadSynced(true);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }
                clearFolder(record);
            }
        }

    }

    private void sendEmailToCustomer(final Record record1) {

        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        final Record record = recordData.where(record1.getCustomerDetails().getDLNumber());
        if (!record.getSettingsAndPermissions().isDataSyncedWithServer() && !record.getSettingsAndPermissions().isEmailSent() && !emailNotCalling) {
            emailNotCalling = true;
            Log.e("Data", "uploadFile: Data synced");
            Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().sentMail("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), record.getSettingsAndPermissions().getCustomerVehicleTradeId());

            imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                    assert response.body() != null;

                    if (response.isSuccessful() && response.body().isSucceeded()) {
                        emailNotCalling = false;
                        try {
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setDataSyncedWithServer(true);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setEmailSent(true);
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            Intent intent = new Intent();
                            assert response.body() != null;
                            intent.putExtra("value", response.body().getMessage());
                            intent.setAction("notifyData");
                            sendBroadcast(intent);
                            clearFolder(record);
                        } catch (JsonSyntaxException e) {
                            emailNotCalling = false;
                            e.printStackTrace();
                        }
//                            Toast.makeText(MyService.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        assert response.body() != null;
//                            showPopup(response.body().getMessage());

//                            if (activity != null) {
//                                activity.updateClient();
//                            }
                    } else {
                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());
                            Log.e("sent email error ", "json: " + json.toString());
                            emailNotCalling = false;
                        } catch (IOException | JSONException e) {
                            emailNotCalling = false;
                            e.printStackTrace();

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ImageUploadResponse> call, Throwable t) {
                    if (t.getLocalizedMessage().contains("timeout")) {
//                        emailNotCalling = false;
                        handleStart();
                    }
                }
            });
        }
    }

    private void clearFolder(Record record) {
        Log.e("All Done ", "  Cheers");
        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
            if (main.isDirectory()) {
                String[] children = main.list();
                for (String aChildren : children) {
                    Log.e("All Done", " Child Folders deleted : " + new File(main, aChildren).delete());
                }
                Log.e("All Done", " Parent Folders deleted : " + main.delete());
            }
        }
    }

    private void showPopup(String textMessage) {
        LayoutInflater layoutInflater = LayoutInflater.from(MyService.this);
        View promptView = layoutInflater.inflate(R.layout.custom_toast, null);

        final AlertDialog alertD = new AlertDialog.Builder(MyService.this).create();
        alertD.setCancelable(false);
        alertD.setView(promptView);
        TextView text = (TextView) promptView.findViewById(R.id.text);
        text.setText(textMessage);
        alertD.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alertD.dismiss();
            }
        }, 4000);
    }

    private void startImageSync(final File file, String id, String docType, final Record record) {
//        System.gc();
        boolean additional = false;
        String doctype = docType;
        if (file.getAbsolutePath().contains("Additional")) {
            additional = true;
            if (docType != null && docType.trim().length() <= 0) {
                if (record.getSettingsAndPermissions().isHasTradeIn()) {
                    doctype = getString(R.string.tradeInAdditionalImage);
                } else {
                    doctype = getString(R.string.additionalImage);
                }
            }
        }
        String imageFor = "1";
        /*Write logic for ImageFor */
        //TODO

        double size = (file.length() / (1024 * 1024));
        String imageDateTime = null;
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        try {
            if (doctype.equalsIgnoreCase("DRIVERLICENSEIMG")) {
                imageFor = "1";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp()));
            } else if (doctype.equalsIgnoreCase("Customer_Insurance")) {
                imageFor = "1";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getInsurenceTime()));
            } else if (doctype.equalsIgnoreCase("FRONTIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarFrontTime()));
            } else if (doctype.equalsIgnoreCase("RIGHTIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarRightTime()));
            } else if (doctype.equalsIgnoreCase("REARIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarRearTime()));
            } else if (doctype.equalsIgnoreCase("LEFTIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarLeftTime()));
            } else if (doctype.equalsIgnoreCase("MILEAGEIMG")) {
                imageFor = "2";
//                if (record.getVehicleDetails().getCarOdoMeterTime() != null) {
//                    imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarOdoMeterTime()));
//                } else {
//                    imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarLeftTime()));
//                }
            } else if (doctype.equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
            } else if (doctype.equalsIgnoreCase("TRADEINMILEAGE")) {
                imageFor = "4";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
            } else if (doctype.equalsIgnoreCase("LICENCEPLATENUMBER")) {
                imageFor = "4";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
            }
            /*Here go additional*/
            else if (file.getAbsoluteFile().toString().contains("Additional")) {
                imageFor = "2";
            } else if (file.getAbsoluteFile().toString().contains("TradeInAdditional")) {
                imageFor = "4";
            }
//            else if (doctype.equalsIgnoreCase("Additional1")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional1()));
//            } else if (doctype.equalsIgnoreCase("Additional2")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional2()));
//            } else if (doctype.equalsIgnoreCase("Additional3")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional3()));
//            } else if (doctype.equalsIgnoreCase("Additional4")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional4()));
//            } else if (doctype.equalsIgnoreCase("Additional5")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional5()));
//            }
//
            else {
                if (docType.contains("co_buyer")) {
                    imageFor = "3";
                }
                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp()));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        File filenew = null;
        Log.e("File size prev : ", file.length() / (1024 * 1024) + " M.B");
        if (size >= 1) {
            try {
                filenew = new Compressor(DSAPP.getInstance()).compressToFile(file);
                Log.e("File size new : ", filenew.length() / (1024 * 1024) + " M.B");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        MultipartBody.Part body;
        RequestBody reqFile;
        if (filenew != null) {
            reqFile = RequestBody.create(MediaType.parse("image/*"), filenew);
            body = MultipartBody.Part.createFormData("upload", filenew.getName(), reqFile);
        } else {
            reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
        }
        String utcForImg = null;
        try {
            imageDateTime = tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString()));
            long timeForUTC = new Date(file.lastModified()).getTime();
            utcForImg = tradeTimeFormatter.format(new Date(TimeZoneUtils.toUTC(timeForUTC, TimeZone.getDefault())));
            Log.e("UTC time for img: ", utcForImg);
            Log.e("Normal time for img: ", imageDateTime);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        retrofit2.Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().syncImagesToServer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, doctype, imageDateTime, body, imageFor, additional,utcForImg);
        final String finalDoctype = doctype;
        imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.isSuccessful() && response.body().isSucceeded()) {
                    Log.e("Image upload type :", "onResponse: " + finalDoctype + " " + response.body().getValue());
//                    Crashlytics.logException(new Exception("onResponse: " + finalDoctype + " Value Id: " + response.body().getValue() + " Message: " + response.body().getMessage()));
                    if (file.exists()) {
                        if (file.delete()) {
                            if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                                uploadFile(record);
                        }
                    }
                } else {
                    try {
//                        response.errorBody().toString()
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString() + " Image Name: " + finalDoctype);
//                        Crashlytics.logException(new Exception("onResponse on 400 status : " + finalDoctype + " error message : " + json.toString()));
                        if (json.toString().contains("Already uploaded")) {
                            if (file.exists()) {
                                if (file.delete()) {
                                    if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                                        uploadFile(record);
                                }
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                Log.e("My Service img :", "onFailure: " + t.getLocalizedMessage());
                if (t.getLocalizedMessage().contains("timeout")) {
                    handleStart();
                }
            }
        });
    }

    public void registerClient(Activity activity) {
        this.activity = (Callbacks) activity;
    }

    public interface Callbacks {
        void updateClient();
    }

    public class LocalBinder extends Binder {
        public MyService getServiceInstance() {
            return MyService.this;
        }
    }

}