package com.volga.dealershieldtablet.services;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.AdditionalDisclosure.AdditionalDisclosureToUpload;
import com.volga.dealershieldtablet.Retrofit.Pojo.ConditionDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.CustomLogs;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CoBuyerCustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.CoBuyerDetail;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.IncompleteDealPojo;
import com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.TradeInVehicle;
import com.volga.dealershieldtablet.Retrofit.Pojo.UploadedDocs;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehicleDataUploadResponse;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.CustomerAgreement;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryDisclosures;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.ThirdPartyHistoryReport;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.ui.coBuyer.SalesPersons;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MyNewService extends Service {
    private Gson gson = new Gson();
    private boolean emailNotCalling = false;
    private boolean mRunning;
    private Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();
        mRunning = false;
        startServiceOreoCondition();
    }

    private void startServiceOreoCondition() {
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_service";
            String CHANNEL_NAME = "My Background Service";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) Objects.requireNonNull(getSystemService(Context.NOTIFICATION_SERVICE))).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.mipmap.launcher_icon).setPriority(PRIORITY_MIN).build();
            startForeground(101, notification);
        }
    }

    private void handleStart() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());

        if (recordData != null) {
            ArrayList<Record> allRecords = new ArrayList<>(recordData.getRecords());
            Log.e("Total records: ", " allRecords = " + allRecords.size());
            for (int i = 0; i < allRecords.size(); i++) {
                Date date = new Date();
                try {
                    date = formatter.parse(allRecords.get(i).getSettingsAndPermissions().getLatestTimeStamp());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long lasttime = date.getTime();
                long currentTime = System.currentTimeMillis();
                long diff = currentTime - lasttime;
                int hours = (int) ((currentTime - lasttime) / (1000 * 60 * 60));
//                int hours = (int) ((currentTime - lasttime) / (1000 * 60 ));
                int min = (int) ((currentTime - allRecords.get(i).getSettingsAndPermissions().getCurrentMillis()) / (1000 * 60));

                if (allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData1.where(allRecords.get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIncompleteDeal(false);
                    String recordDataString1 = gson.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }

                if (allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete() && !allRecords.get(i).getSettingsAndPermissions().getIsDataSynced()) {
                    if (allRecords.get(i).getVehicleDetails().getCarMake() == null || allRecords.get(i).getVehicleDetails().getCarModel() == null || allRecords.get(i).getVehicleDetails().getCarYear() == null || allRecords.get(i).getVehicleDetails().getCarMake().trim().length() == 0 || allRecords.get(i).getVehicleDetails().getCarModel().trim().length() == 0 || allRecords.get(i).getVehicleDetails().getCarYear().trim().length() == 0) {
                        callVinAPI(allRecords.get(i).getVehicleDetails().getVINNumber(), allRecords.get(i), false);
                        continue;
                    }
                    if (allRecords.get(i).getSettingsAndPermissions().isHasTradeIn() && (allRecords.get(i).getTradeInVehicle().getCarMake() == null || allRecords.get(i).getTradeInVehicle().getCarModel() == null || allRecords.get(i).getTradeInVehicle().getCarYear() == null || allRecords.get(i).getTradeInVehicle().getCarMake().trim().length() == 0 || allRecords.get(i).getTradeInVehicle().getCarModel().trim().length() == 0 || allRecords.get(i).getTradeInVehicle().getCarYear().trim().length() == 0)) {
                        callVinAPI(allRecords.get(i).getTradeInVehicle().getVINNumber(), allRecords.get(i), true);
                        continue;
                    }
                }
                if (min > 8 && allRecords.get(i).getSettingsAndPermissions().isIncompleteDeal() && !allRecords.get(i).getSettingsAndPermissions().isIncompleteDealSynced()) {
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData1.where(allRecords.get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                    String recordDataString1 = gson.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }
                if (min > 8 && allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete() && !allRecords.get(i).getSettingsAndPermissions().getIsDataSynced()) {
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData1.where(allRecords.get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                    String recordDataString1 = gson.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }
                int mins = (int) ((currentTime - lasttime) / (1000 * 60 * 4));
                //&& !allRecords.get(i).getSettingsAndPermissions().isSentEmailForIncomplete()
                if (hours >= 12 && !allRecords.get(i).getSettingsAndPermissions().getIsRecordComplete() && !allRecords.get(i).getSettingsAndPermissions().isFromServer() && !allRecords.get(i).getSettingsAndPermissions().isSentEmailForIncomplete()) {
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData1.where(allRecords.get(i).getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLead(true);
                    String recordDataString1 = gson.toJson(recordData1);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
//                    homeIntent.addCategory( Intent.CATEGORY_HOME );
//                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(homeIntent);
                }
            }
            RecordData recordData2 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            ArrayList<Record> recordArrayList = new ArrayList<>();
            if (recordData2 != null) {
                for (int i = 0; i < recordData2.getRecords().size(); i++) {
                    if (recordData2.getRecords().get(i).getSettingsAndPermissions() != null) {
                        recordArrayList.add(recordData2.getRecords().get(i));
                    }
                }
            }
            startSyncing(recordArrayList);
        } else {
            Timber.e(" data is null");
        }
    }

    private void callVinAPI(String vin, final Record record, final boolean isTradeIn) {
        //Call the API here and fill the text views!
        Call<String> vinDataCall = RetrofitInitialization.getVin_query_services().get_vinData(AppConstant.VIN_QUERY_ACCESS_TOKEN, 3, vin);
        vinDataCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.code() == 200) {
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
//                    String recordDataString1 = gson.toJson(recordData);
//                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    try {
                        assert response.body() != null;
                        JSONObject jsonObject = XML.toJSONObject(response.body().toString());
                        JSONObject vinQuery = jsonObject.getJSONObject("VINquery");
                        JSONObject vin = vinQuery.getJSONObject("VIN");

                        if (!vin.getString("Status").equalsIgnoreCase("failed")) {
                            Object vehicle = vin.get("Vehicle");

                            if (vehicle instanceof JSONObject) {
                                JSONObject jsonArray = vin.getJSONObject("Vehicle");
                                Gson gson = new GsonBuilder().create();
                                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                if (!isTradeIn) {
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarMake(jsonArray.getString("Make"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarYear(jsonArray.getString("Model_Year"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarModel(jsonArray.getString("Model"));

                                } else {
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getTradeInVehicle().setCarMake(jsonArray.getString("Make"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getTradeInVehicle().setCarYear(jsonArray.getString("Model_Year"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getTradeInVehicle().setCarModel(jsonArray.getString("Model"));

                                }
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                                year.setText(jsonArray.getString("Model_Year"));
//                                make.setText(jsonArray.getString("Make"));
//                                model.setText(jsonArray.getString("Model"));
                            } else if (vehicle instanceof JSONArray) {
                                JSONArray jsonArray = vin.getJSONArray("Vehicle");
                                Gson gson = new GsonBuilder().create();
                                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                if (isTradeIn) {
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getTradeInVehicle().setCarMake(jsonArray.getJSONObject(0).getString("Make"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getTradeInVehicle().setCarYear(jsonArray.getJSONObject(0).getString("Model_Year"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getTradeInVehicle().setCarModel(jsonArray.getJSONObject(0).getString("Model"));

                                } else {
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarMake(jsonArray.getJSONObject(0).getString("Make"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarYear(jsonArray.getJSONObject(0).getString("Model_Year"));
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarModel(jsonArray.getJSONObject(0).getString("Model"));

                                }
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                                year.setText(jsonArray.getJSONObject(0).getString("Model_Year"));
//                                make.setText(jsonArray.getJSONObject(0).getString("Make"));
//                                model.setText(jsonArray.getJSONObject(0).getString("Model"));
                            }


                        } else {
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarMake("Make");
                            recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarYear("2010");
                            recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().setCarModel("Model");
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//                            make.setText("");
//                            model.setText("");
//                            year.setText("");
//                            stockNumber.setText("");
//                            new CustomToast(DSAPP.getInstance()).alert(vin.getJSONObject("Message").getString("Value"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
//                        new CustomToast(DSAPP.getInstance()).alert(json.getString("error_description"));
//                        progressDialog.dismiss();
                    } catch (IOException | JSONException e) {
//                        progressDialog.dismiss();
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void startSyncing(final ArrayList<Record> recordArrayList) {
        if (recordArrayList.size() > 0) {
            for (int i = 0; i < recordArrayList.size(); i++) {
                final int finalI = i;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!recordArrayList.get(finalI).getSettingsAndPermissions().getIsDataSynced() && recordArrayList.get(finalI).getSettingsAndPermissions().getIsRecordComplete()) {
                            if (!recordArrayList.get(finalI).getSettingsAndPermissions().isSyncingInProgress())
                                syncVehicleDate(recordArrayList.get(finalI));
                        } else if (!recordArrayList.get(finalI).getSettingsAndPermissions().getIsRecordComplete() && recordArrayList.get(finalI).getSettingsAndPermissions().isBussinessLead() && !recordArrayList.get(finalI).getSettingsAndPermissions().isBussinessLeadSynced()) {
                            //&& !recordArrayList.get(finalI).getSettingsAndPermissions().isIncompleteDeal() this flag incse of incomplete deal lead email was not getting send
                            if (!recordArrayList.get(finalI).getSettingsAndPermissions().isSyncingInProgress())
                                syncVehicleDate(recordArrayList.get(finalI));
                        } else if (!recordArrayList.get(finalI).getSettingsAndPermissions().getIsRecordComplete() && !recordArrayList.get(finalI).getSettingsAndPermissions().isBussinessLead() && recordArrayList.get(finalI).getSettingsAndPermissions().isIncompleteDeal() && !recordArrayList.get(finalI).getSettingsAndPermissions().isIncompleteDealSynced() && recordArrayList.get(finalI).getDealershipDetails().getDealershipId() != 0) {
                            if (!recordArrayList.get(finalI).getSettingsAndPermissions().isSyncingInProgress())
                                syncVehicleDate(recordArrayList.get(finalI));
                        } else {
                            if (recordArrayList.get(finalI).getSettingsAndPermissions().getCustomerVehicleTradeId() != null) {
                                /*recordArrayList.get(i).getSettingsAndPermissions().isIncompleteDeal() &&*/
                                if (recordArrayList.get(finalI).getSettingsAndPermissions().isIncompleteDealTextSynced() && !recordArrayList.get(finalI).getSettingsAndPermissions().isIncompleteDealSynced()) {
                                    uploadFile(recordArrayList.get(finalI));
                                } else if (recordArrayList.get(finalI).getSettingsAndPermissions().getIsRecordComplete()) {
                                    uploadFile(recordArrayList.get(finalI));
                                }
                            }
                        }
                    }
                }, 1000);

//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
        }

    }

    private VehicleDataUploadResponse getVehicleData(Record record) {

        SimpleDateFormat originalFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
        SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        Date date;
        VehicleDataUploadResponse response = new VehicleDataUploadResponse();

        /*disclosure*/
        ArrayList<HistoryListPojo> disclosure = record.getVehicleDetails().getUsedVehicleCondition();
        ArrayList<ConditionDisclosures> disclosureList = new ArrayList<>();
        if (disclosure != null) {
            for (int i = 0; i < disclosure.size(); i++) {
                if (disclosure.get(i).isChecked() && i != disclosure.size() - 1) {
                    if (!disclosure.get(i).getId().equalsIgnoreCase("0")) {
                        ConditionDisclosures thirdPartyHistoryReport = new ConditionDisclosures();
                        thirdPartyHistoryReport.setVehicleConditionDisclosureId(disclosure.get(i).getId());
                        thirdPartyHistoryReport.setValue(disclosure.get(i).getAdditionalData());
                        thirdPartyHistoryReport.setVerified(disclosure.get(i).isVerified());
                        disclosureList.add(thirdPartyHistoryReport);
                    }
                }
            }
        }
        response.setConditionDisclosures(disclosureList);
        response.setCustomerVehicleDealAlerts(record.getVehicleDetails().getCustomerVehicleDealAlerts());

        /*history*/
        ArrayList<HistoryListPojo> listPojos = record.getSettingsAndPermissions().getPrintReceivedList();
        ArrayList<CustomerAgreement> customerAgreements = new ArrayList<>();
        if (listPojos != null) {
            for (int i = 0; i < listPojos.size(); i++) {
                CustomerAgreement customerAgreement = new CustomerAgreement();
                customerAgreement.setCustomerSignAgreementsId(listPojos.get(i).getId());
                customerAgreement.setValue(listPojos.get(i).isValue());
                customerAgreements.add(customerAgreement);
            }
        }
        response.setVerbalPromises(record.getSettingsAndPermissions().getVerbalPromises());
        response.setCheckLists(record.getSettingsAndPermissions().getCheckLists());
        response.setCustomerAgreement(customerAgreements);
        /*Report docs data*/
        response.setThirdPartyReportDocs(record.getVehicleDetails().getThirdPartyReportDocs());

        ArrayList<HistoryListPojo> list = record.getVehicleDetails().getUsedVehicleReport();
        ArrayList<ThirdPartyHistoryReport> historyReports = new ArrayList<>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isChecked() && i != list.size() - 1) {
                    ThirdPartyHistoryReport thirdPartyHistoryReport = new ThirdPartyHistoryReport();
                    thirdPartyHistoryReport.setThirdPartyHistoryReportId(list.get(i).getId());
                    historyReports.add(thirdPartyHistoryReport);
                }
            }
        }
        response.setThirdPartyHistoryReport(historyReports);
        ArrayList<HistoryListPojo> list1 = record.getVehicleDetails().getUsedVehicleHistory();
        ArrayList<HistoryDisclosures> history = new ArrayList<>();
        if (list1 != null) {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i).isChecked() && i != list1.size() - 1) {
                    HistoryDisclosures thirdPartyHistoryReport = new HistoryDisclosures();
                    thirdPartyHistoryReport.setVehicleHistoryDisclosureId(list1.get(i).getId());
                    thirdPartyHistoryReport.setVerified(list1.get(i).isVerified());
//                    try {
//                        if (list1.get(i).getId().equalsIgnoreCase("15")) {
//                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate1().toString())));
//                        } else if (list1.get(i).getId().equalsIgnoreCase("16")) {
//                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate2().toString())));
//                        } else {
//                            thirdPartyHistoryReport.setDate("");
//                        }
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
                    history.add(thirdPartyHistoryReport);
                }
            }
        }
        response.setHistoryDisclosures(history);

        try {
            if (record.getCustomerDetails().getDateOfBirth() != null && record.getCustomerDetails().getDateOfBirth().length() > 0) {
                date = originalFormatter.parse(record.getCustomerDetails().getDateOfBirth());
                int month = 0, dates = 0, year = 0;
                String dob = record.getCustomerDetails().getDateOfBirth();
                String[] str = dob.split("-");
                month = Integer.parseInt(str[0]);
                dates = Integer.parseInt(str[1]);
                year = Integer.parseInt(str[2]);
                if (!((month > 0 && month <= 12) && (dates > 0 && dates <= 31) && (year > 1900 && year < Calendar.getInstance().get(Calendar.YEAR)))) {
                    response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse("01-01-1991")));
                } else {
                    response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
                }
            }
//            response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
            if (record.getCustomerDetails().getDLExpiryDate() != null && record.getCustomerDetails().getDLExpiryDate().length() > 0) {
//                response.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDLExpiryDate())));

                int month1 = 0, date1 = 0, year1 = 0;
                String dob = record.getCustomerDetails().getDLExpiryDate();
                String[] str = dob.split("-");
                month1 = Integer.parseInt(str[0]);
                date1 = Integer.parseInt(str[1]);
                year1 = Integer.parseInt(str[2]);
                if (!((month1 > 0 && month1 <= 12) && (date1 > 0 && date1 <= 31) && (year1 >= Calendar.getInstance().get(Calendar.YEAR)))) {
                    response.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse("09-09-2019")));
                } else {
                    response.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDLExpiryDate())));
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            response.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
            response.setTradeCompletionUTC(record.getSettingsAndPermissions().getTradeCompletionUTC());
//            Log.e(" UTC Time: ",record.getSettingsAndPermissions().getTradeCompletionUTC());
            Log.e("TradeID date: ", tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (response.getTradeDateTime() == null || response.getTradeDateTime().length() == 0) {
            try {
                String path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                if (new File(path_signature + "/IMG_contact_info.svg").exists()) {
                    File file = new File(path_signature + "/IMG_contact_info.svg");
                    response.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString())));
                    Log.e("TradeID date: ", response.getTradeDateTime());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        /* add third party rows*/

        if (record.getVehicleDetails().getInitiateThirdParty() != null) {
            response.setThirdPartyReportRaws(record.getVehicleDetails().getInitiateThirdParty().getValue());
        }

        ArrayList<SalesPersons> salesPersons = new ArrayList<>();
        if (record.getCustomerDetails().getSalesPersons() != null && record.getCustomerDetails().getSalesPersons().getUserId() != null) {
            salesPersons.add(record.getCustomerDetails().getSalesPersons());
        } else {
            SalesPersons salesPersons1 = new SalesPersons();
            salesPersons1.setUserId(record.getVehicleDetails().getSalesId());
            salesPersons.add(salesPersons1);
        }
        response.setSalesPersons(salesPersons);

        response.setAddress1(record.getCustomerDetails().getAddressLineOne() == null ? "" : record.getCustomerDetails().getAddressLineOne().trim());
        response.setTermConditionAccepted(record.getCustomerDetails().isTermConditionAccepted());
        response.setAddress2(record.getCustomerDetails().getAddressLineTwo() == null ? "" : record.getCustomerDetails().getAddressLineTwo().trim());
        response.setAllow2RemoveWindowSticker(record.getSettingsAndPermissions().getStickerRemovalPermission());
        if (record.getVehicleDetails().getTypeOfVehicle() != null) {
            if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                response.setCarTypeId("1");
            } else {
                response.setAcceptedUsedCarPolicy(record.getSettingsAndPermissions().isAcceptedUsedCarPolicy());
                response.setCarTypeId("2");
            }
        }
        response.setCity(record.getCustomerDetails().getCity());
        response.setContactNumber(record.getCustomerDetails().getMobileNumber());

        response.setLead(record.getSettingsAndPermissions().isBussinessLead());
        response.setIsRemote(record.getCustomerDetails().getIsRemote());
        if (record.getCustomerDetails().getIsRemote() && !record.getSettingsAndPermissions().isHasCoBuyer()) {
            response.setLastScreenName("Deal got completed as remote");
        } else if (record.getCustomerDetails().getIsRemote() && record.getSettingsAndPermissions().isHasCoBuyer() && !record.getCoBuyerCustomerDetails().getIsRemote()) {
            response.setLastScreenName("Final Thank you page. Co-buyer Signature completed.");
        } else if (!record.getCustomerDetails().getIsRemote() && record.getSettingsAndPermissions().isHasCoBuyer() && record.getCoBuyerCustomerDetails().getIsRemote()) {
            response.setLastScreenName("Final Thank you page. Buyer Signature completed.");
        } else {
            response.setLastScreenName(getScreenName(record.getSettingsAndPermissions().getLastPageIndex(), record));
        }

        if ((record.getCustomerDetails().getDrivingLicenceNumber() == null || record.getCustomerDetails().getDrivingLicenceNumber().trim().length() == 0) && record.getCustomerDetails().getFirstName() != null && record.getCustomerDetails().getFirstName().length() != 0) {
            response.setDrivingLicenseNumber(record.getCustomerDetails().getDLNumber().substring(0, record.getCustomerDetails().getDLNumber().length() - 4));
        } else {
            response.setDrivingLicenseNumber(record.getCustomerDetails().getDrivingLicenceNumber());
        }
//        response.setDrivingLicenseNumber(record.getCustomerDetails().getDrivingLicenceNumber()); record.getDealershipDetails().getDealershipId()
        response.setDealershipId(record.getDealershipDetails().getDealershipId() + "");
        response.setEmail(record.getCustomerDetails().getEmail() == null ? "".trim() : record.getCustomerDetails().getEmail().trim());
        response.setZipcode(record.getCustomerDetails().getZipCode());
        response.setVIN(record.getVehicleDetails().getVINNumber());
        response.setMake(record.getVehicleDetails().getCarMake());
        response.setModel(record.getVehicleDetails().getCarModel());
        response.setModelYear(record.getVehicleDetails().getCarYear());
        response.setMileage(record.getVehicleDetails().getMileage());
        response.setStockNumber(record.getVehicleDetails().getStockNumber());
        if (record.getSettingsAndPermissions().isHasCoBuyer()) {
            CoBuyerCustomerDetails coBuyerCustomerDetails = record.getCoBuyerCustomerDetails();

            if (coBuyerCustomerDetails.getFirstName() == null || coBuyerCustomerDetails.getFirstName().length() == 0) {
                coBuyerCustomerDetails.setFirstName(record.getVehicleDetails().getCoFirstName());
            }
            if (coBuyerCustomerDetails.getLastName() == null || coBuyerCustomerDetails.getLastName().length() == 0) {
                coBuyerCustomerDetails.setLastName(record.getVehicleDetails().getCoLastName());
            }


            try {
                if (record.getCoBuyerCustomerDetails().getDateOfBirth() != null && record.getCoBuyerCustomerDetails().getDateOfBirth().length() > 0) {
                    date = originalFormatter.parse(record.getCoBuyerCustomerDetails().getDateOfBirth());
//                response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
                    int month = 0, dates = 0, year = 0;
                    String dob = record.getCoBuyerCustomerDetails().getDateOfBirth();
                    String[] str = dob.split("-");
                    month = Integer.parseInt(str[0]);
                    dates = Integer.parseInt(str[1]);
                    year = Integer.parseInt(str[2]);
                    if (!((month > 0 && month <= 12) && (dates > 0 && dates <= 31) && (year > 1900 && year < Calendar.getInstance().get(Calendar.YEAR)))) {
                        coBuyerCustomerDetails.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse("01-01-1991")));
                    } else {
                        coBuyerCustomerDetails.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCoBuyerCustomerDetails().getDateOfBirth())));
                    }

                }
                if (record.getCoBuyerCustomerDetails().getDLExpiryDate() != null && record.getCoBuyerCustomerDetails().getDLExpiryDate().length() > 0) {
                    int month1 = 0, date1 = 0, year1 = 0;
                    String dob = record.getCoBuyerCustomerDetails().getDLExpiryDate();
                    String[] str = dob.split("-");
                    month1 = Integer.parseInt(str[0]);
                    date1 = Integer.parseInt(str[1]);
                    year1 = Integer.parseInt(str[2]);
                    if (!((month1 > 0 && month1 <= 12) && (date1 > 0 && date1 <= 31) && (year1 >= Calendar.getInstance().get(Calendar.YEAR)))) {
                        coBuyerCustomerDetails.setDLExpiryDate(tradeTimeFormatter.format(originalFormatter.parse("09-09-2019")));
                    } else {
                        coBuyerCustomerDetails.setDLExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(record.getCoBuyerCustomerDetails().getDLExpiryDate())));
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            coBuyerCustomerDetails.setCustomerAgreement(record.getCoBuyerCustomerDetails().getPrintReceivedList());
            coBuyerCustomerDetails.setVerbalPromises(record.getCoBuyerCustomerDetails().getVerbalPromises());
            coBuyerCustomerDetails.setCheckLists(record.getCoBuyerCustomerDetails().getCheckLists());
            response.setVerbalPromises(record.getSettingsAndPermissions().getVerbalPromises());
            response.setCheckLists(record.getSettingsAndPermissions().getCheckLists());
            coBuyerCustomerDetails.setTestDriveTaken(record.getSettingsAndPermissions().isCoBuyerTestDriveTaken());
            coBuyerCustomerDetails.setTestDriveTimeId(record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId() + "");
            coBuyerCustomerDetails.setNoTestDriveConfirm(record.getSettingsAndPermissions().isCoBuyerNoTestDriveConfirm());

            coBuyerCustomerDetails.setIsRemote(record.getCoBuyerCustomerDetails().getIsRemote());

            /*Cobuyer module*/
            if (record.getCoBuyerCustomerDetails().getGender() != null) {
                if (record.getCoBuyerCustomerDetails().getGender().equalsIgnoreCase("male"))
                    coBuyerCustomerDetails.setGender("1"/*record.getCustomerDetails().getGender()*/);
                else if (record.getCustomerDetails().getGender().equalsIgnoreCase("female"))
                    coBuyerCustomerDetails.setGender("2"/*record.getCustomerDetails().getGender()*/);
                else {
                    coBuyerCustomerDetails.setGender("3"/*record.getCustomerDetails().getGender()*/);
                }
            } else {
                coBuyerCustomerDetails.setGender("3"/*record.getCustomerDetails().getGender()*/);
            }
            coBuyerCustomerDetails.setLanguageId(record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() == 0 ? 1 + "" : record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "");
            response.setCoBuyerDetail(coBuyerCustomerDetails);
        }
        if (record.getSettingsAndPermissions().isHasTradeIn() && record.getTradeInVehicle().getVINNumber() != null && record.getTradeInVehicle().getVINNumber().length() > 0) {
            response.setTradeInVehicle(record.getTradeInVehicle());
        }
        response.setTradeTypeId(record.getVehicleDetails().getTypeOfPurchase());

//        response.setTestDriveDurationId(record.getSettingsAndPermissions().getTestDriveTimeId());
        if (record.getSettingsAndPermissions().getTestDriveTaken())
            response.setTestDriveDurationId(record.getSettingsAndPermissions().getTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getTestDriveTimeId() + "");
        else {
            response.setTestDriveDurationId(record.getSettingsAndPermissions().getTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getTestDriveTimeId() + "");
        }
        response.setState(record.getCustomerDetails().getState());
        response.setCountry(record.getCustomerDetails().getCountry());
        /*......*/

        if (record.getCustomerDetails().getFirstName() == null || record.getCustomerDetails().getFirstName().length() == 0) {
            response.setFirstName(record.getVehicleDetails().getBuyerFistName());
        } else {
            response.setFirstName(record.getCustomerDetails().getFirstName());
        }
        if (record.getCustomerDetails().getLastName() == null || record.getCustomerDetails().getLastName().length() == 0) {
            response.setLastName(record.getVehicleDetails().getBuyerLastName());
        } else {
            response.setLastName(record.getCustomerDetails().getLastName());
        }
        /*......*/
        response.setLastName(record.getCustomerDetails().getLastName());

        ArrayList<AdditionalDisclosureToUpload> additionalDisclosure = new ArrayList<>();
        if (record.getVehicleDetails().getAdditionalDisclosure() != null && record.getVehicleDetails().getAdditionalDisclosure().getValue() != null) {

            for (int i = 0; i < record.getVehicleDetails().getAdditionalDisclosure().getValue().size(); i++) {
                AdditionalDisclosureToUpload additionalDisclosureToUpload = new AdditionalDisclosureToUpload();
                additionalDisclosureToUpload.setAdditionalDisclosureId(record.getVehicleDetails().getAdditionalDisclosure().getValue().get(i).getId());
                additionalDisclosureToUpload.setDisclosureType("CONDITION");
                additionalDisclosure.add(additionalDisclosureToUpload);
            }
        }
        if (record.getVehicleDetails().getAdditionalHistoryDisclosure() != null && record.getVehicleDetails().getAdditionalHistoryDisclosure().getValue() != null) {
            for (int i = 0; i < record.getVehicleDetails().getAdditionalHistoryDisclosure().getValue().size(); i++) {
                AdditionalDisclosureToUpload additionalDisclosureToUpload = new AdditionalDisclosureToUpload();
                additionalDisclosureToUpload.setAdditionalDisclosureId(record.getVehicleDetails().getAdditionalHistoryDisclosure().getValue().get(i).getId());
                additionalDisclosureToUpload.setDisclosureType("HISTORY");
                additionalDisclosure.add(additionalDisclosureToUpload);
            }
        }
        response.setAdditinalDisclosures(additionalDisclosure);
        response.setMiddleName(record.getCustomerDetails().getMiddleName());
        response.setLanguageId(record.getSettingsAndPermissions().getSelectedLanguageId() == 0 ? 1 : record.getSettingsAndPermissions().getSelectedLanguageId());
        response.setLocationId(0);
        response.setIsTestDriveTaken(record.getSettingsAndPermissions().getTestDriveTaken());
        if (record.getCustomerDetails().getGender() != null && record.getCustomerDetails().getGender().equalsIgnoreCase("male"))
            response.setGenderTypeId(1/*record.getCustomerDetails().getGender()*/);
        else if (record.getCustomerDetails().getGender() != null && record.getCustomerDetails().getGender().equalsIgnoreCase("female"))
            response.setGenderTypeId(2/*record.getCustomerDetails().getGender()*/);
        else {
            response.setGenderTypeId(3/*record.getCustomerDetails().getGender()*/);
        }
        // transfer the data
        return response;
    }

    private String getScreenName(int lastPageIndex, Record record) {
        String pageName = "Default Page";
        switch (lastPageIndex) {
            case 0:
                pageName = "Vehicle Type Used/New";
                break;
            case 1:
                pageName = "Vehicle VIN Scanner Capture";
                break;
            case 2:
                pageName = "Vehicle info after Scan";
                break;
            case 3:
                pageName = "Capture Photo Front of Vehicle";
                break;
            case 4:
                pageName = "Preview Photo Front of Vehicle";
                break;
            case 5:
                pageName = "Capture Photo Passenger Side";
                break;
            case 6:
                pageName = "Preview Photo Passenger Side";
                break;
            case 7:
                pageName = "Capture Photo Back Vehicle";
                break;
            case 8:
                pageName = "Preview Photo Back Vehicle";
                break;
            case 9:
                pageName = "Capture Photo Driver Side";
                break;
            case 10:
                pageName = "Preview Photo Driver Side";
                break;
            case 11:
                pageName = "Capture Photo  Buyers Guide";
                break;
            case 12:
                pageName = "Preview Photo Buyers Guide";
                break;
            case 13:
                pageName = "Add More Vehicle Images";
                break;
            case 14:
                pageName = "Capture Additional Photo 1";
                break;
            case 15:
                pageName = "Preview Additional image 1";
                break;
            case 16:
                pageName = "Capture Additional image 2";
                break;
            case 17:
                pageName = "Preview Additional image 2";
                break;
            case 18:
                pageName = "Capture Additional image 3";
                break;
            case 19:
                pageName = "Preview Additional image 3";
                break;
            case 20:
                pageName = "Capture Additional image 4";
                break;
            case 21:
                pageName = "Preview Additional image 4";
                break;
            case 22:
                pageName = "Capture Additional image 5";
                break;
            case 23:
                pageName = "Preview Additional image 5";
                break;
            case 24:
                pageName = "Skip Disclosures";
                break;
            case 25:
                if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                    pageName = "Vehicle Condition Disclosure Capture";
                else {
                    pageName = "New Vehicle History Disclosure Capture";
                }
                break;
            case 26:
                pageName = "Vehicle Use Disclosure Capture";
                break;
            case 27:
                pageName = "Vehicle Third Party Reports Capture";
                break;
            case 28:
                pageName = "Save deal or continue deal";
                break;
            case 29:
                pageName = "Salesperson Selection Dropdown";
                break;
            case 30:
                pageName = "Odometer Photo Capture";
                break;
            case 31:
                pageName = "Odometer Photo Preview";
                break;
            case 32:
                pageName = "Manual Vehicle Odometer Capture";
                break;
            case 33:
                pageName = "Is There Trade-in Capture";
                break;
            case 34:
                pageName = "Scan Trade-in Capture";
                break;
            case 35:
                pageName = "Trade-in Info After Scan";
                break;
            case 36:
                pageName = "Capture Front Photo of Trade-in";
                break;
            case 37:
                pageName = "Preview Front Photo of Trade-in";
                break;
            case 38:
                pageName = "Capture License Plate Photo of Trade-in";
                break;
            case 39:
                pageName = "Preview License Plate Photo of trade-in";
                break;
            case 40:
                pageName = "Capture Odometer Photo of Trade-in";
                break;
            case 41:
                pageName = "Preview Odometer Photo of Trade-in";
                break;
            case 42:
                pageName = "Manual Trade-in Odometer Capture";
                break;
            case 43:
                pageName = "Trade-in Add More Images";
                break;
            case 44:
                pageName = "Trade-in Additional image 1";
                break;
            case 45:
                pageName = "Trade-in Additional image preview 1";
                break;
            case 46:
                pageName = "Trade-in Additional image 2";
                break;
            case 47:
                pageName = "Trade-in Additional image preview 2";
                break;
            case 48:
                pageName = "Trade-in Additional image 3";
                break;
            case 49:
                pageName = "Trade-in Additional image preview 3";
                break;
            case 50:
                pageName = "Trade-in Additional image 4";
                break;
            case 51:
                pageName = "Trade-in Additional image preview 4";
                break;
            case 52:
                pageName = "Trade-in Additional image 5";
                break;
            case 53:
                pageName = "Trade-in Additional image preview 5";
                break;
            case 54:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer ID Front Capture";
                else {
                    pageName = "Co-Buyer ID Front Capture";
                }
                break;
            case 55:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer ID Front Preview";
                else
                    pageName = "Co-Buyer ID Front Preview";
                break;
            case 56:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer ID Scan Back Capture";
                else {
                    pageName = "Co-Buyer ID Scan Back Capture";
                }
                break;
            case 57:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer ID Scanned Back Result";
                else
                    pageName = "Co-Buyer ID Scanned Back Result";
                break;
            case 58:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer ID Info Confirmation";
                else {
                    pageName = "Co-Buyer ID Info Confirmation";
                }
                break;
            case 59:
                pageName = "Is There Co-buyer?";
                break;
            case 60:
                pageName = "Continue F&I Different Tablet?";
                break;
            case 61:
                pageName = "Vehicle Summary Review Dealer";
                break;
            case 62:
                pageName = "Trade-in Summary Dealer";
                break;
            case 63:
                pageName = "Finalize Disclosures?";
                break;
            case 64:
                pageName = "Proof of Insurance Photo?";
                break;
            case 65:
                pageName = "Proof of Insurance Photo Capture";
                break;
            case 66:
                pageName = "Proof of Insurance Photo Preview";
                break;
            case 67:
                pageName = "Handover Tablet to Buyer";
                break;
            case 68:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Language Selection";
                else
                    pageName = "Co-Buyer Language Selection";
                break;
            case 69:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Disclosures";
                else
                    pageName = "Co-Buyer Disclosures";
                break;
            case 70:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Confirm Vehicle Photos";
                else
                    pageName = "Co-Buyer Confirm Vehicle Photos";
                break;
            case 71:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Confirm vehicle details";
                else
                    pageName = "Co-Buyer Confirm vehicle details";
                break;
            case 72:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                        pageName = "Buyer Test Drive Screen";
                    else {
                        pageName = "Buyer New Vehicle Test Drive Confirmation";
                    }
//                    pageName = "Buyer Test Drive Screen p.5";
                } else {
                    if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                        pageName = "Co-Buyer Test Drive Screen";
                    else {
                        pageName = "Co-Buyer New Vehicle Test Drive Confirmation";
                    }
                }
                break;
            case 73:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                        pageName = "Buyer Buyers Guide";
                    else {
                        pageName = "Buyer MSRP/Addendum Sticker Authorization";
                    }
                } else {
                    if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                        pageName = "Co-Buyer Buyers Guide";
                    else {
                        pageName = "Co-Buyer MSRP/Addendum Sticker Authorization";
                    }
                }
                break;
            case 74:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                        pageName = "Buyer Vehicle History Disclosure";
                    else {
                        pageName = "Buyer New Vehicle History Disclosure";
                    }
                } else {
                    if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used"))
                        pageName = "Co-Buyer Vehicle History Disclosure";
                    else {
                        pageName = "Co-Buyer New Vehicle History Disclosure";
                    }
                }
                break;
            case 75:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Vehicle Use Disclosure";
                else
                    pageName = "Co-Buyer Vehicle Use Disclosure";
                break;
            case 76:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Third Party Report Confirmation";
                else
                    pageName = "Co-Buyer Third Party Report Confirmation";
                break;
            case 77:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer No Verbal Promises";
                else
                    pageName = "Co-Buyer No Verbal Promises";
                break;
            case 78:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Almost Done";
                else
                    pageName = "Co-Buyer Almost Done";
                break;
            case 79:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Document Checklist";
                else
                    pageName = "Co-Buyer Document Checklist";
                break;
            case 80:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Final Approval";
                else
                    pageName = "Co-Buyer Final Approval";
                break;
            case 81:
                if (!record.getSettingsAndPermissions().isHasCoBuyer())
                    pageName = "Buyer Contact Info";
                else
                    pageName = "Co-Buyer Contact Info";
                break;
            case 82:
                pageName = "Co-Buyer Selection";
                break;
            case 83:
                pageName = "Final Thank You Page Deal Completed";
                break;
        }
        return pageName;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {
        start5MinFunction();
        if (!mRunning) {
            mRunning = true;
            Timber.e("newly created service Id: %s", startId);
        } else {
            Timber.e(" Service Id: %s", startId);
        }
        return START_NOT_STICKY;
    }

    private void start5MinFunction() {
        if (timer != null) {
            Timber.e("Previous timer canceled.");
            timer.cancel();
        }
        final Handler handler = new Handler();
        timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            //your method here
                            Log.e("Every 5 min : ", " startSyncing ");
                            handleStart();
                        } catch (Exception e) {
                            writeCustomLogsAPI("0", "Error before making data object: " + e.getMessage());
                            Log.e("In onStartCommand", "run: handleStart & startSyncing " + e.toString());
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 300000);
    }

    private void syncVehicleDate(final Record record) {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCurrentMillis(System.currentTimeMillis());
            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(true);
            if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0)
                writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Save deal incomplete data Json before making server object : " + gson.toJson(record));
        }
        if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0")) {
            record.getSettingsAndPermissions().setCustomerVehicleTradeId(null);
        }
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
        if (record.getSettingsAndPermissions().isIncompleteDeal() && !record.getSettingsAndPermissions().getIsRecordComplete()) {
            IncompleteDealPojo incompleteDealPojo = getIncompleteDealPojo(record);
            Timber.e(gson.toJson(incompleteDealPojo));
            Call<ImageUploadResponse> call = RetrofitInitialization.getDs_services().uploadInCompleteDeal("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), incompleteDealPojo);
            final IncompleteDealPojo finalVehicleDataUploadResponse = incompleteDealPojo;
            call.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(@NonNull Call<ImageUploadResponse> call, @NonNull Response<ImageUploadResponse> response) {
                    try {
                        Timber.e("Incomplete deal onResponse: " + response.toString() + " Message " + response.message() + " " + response.errorBody());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (response.isSuccessful() && response.body().isSucceeded()) {

                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                            writeCustomLogsAPI(response.body().getValue(), "Save Deal incomplete Data Json: " + gson.toJson(finalVehicleDataUploadResponse));
                        Timber.tag("Vehicle Info uploaded :").e("onResponse: Trade ID: " + response.body().getValue());
//                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + "\n" + "TextDataUpload onResponse: " + record.getCustomerDetails().getDLNumber());
                        try {
                            if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
//                                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIsDataSynced(response.body().isSucceeded());
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setError(false);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setErrorDesc("");

                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCustomerVehicleTradeId(response.body().getValue());
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIncompleteDealTextSynced(true);
                                if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs());
                                if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0)
                                    writeCustomLogsAPI(response.body().getValue(), "Save deal incomplete data Json before making server object : " + gson.toJson(record));

                            }
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            /*upload file*/
//                            assert response.body() != null;
                            if (response.body().getValue() != null) {
                                Gson gson1 = new GsonBuilder().create();
                                RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                uploadFile(recordData1.where(record.getCustomerDetails().getDLNumber()));
                            }
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                            if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + "\n" + e.getLocalizedMessage() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
                        }

//                    startSyncing();
                    } else {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);

                        JSONObject json = null;
                        try {
//                            //assert response.errorBody() != null;
                            json = new JSONObject(response.errorBody().string());

                            if (response.code() == 401 || response.code() == 403) {
                                if (json.getString("Message").contains("INACTIVE")) {
                                    String[] arr = json.getString("Message").split("\\|");
//                                    new CustomToast(getApplicationContext()).toast(arr[1]);
                                    Intent intent = new Intent();
//                                    //assert response.body() != null;
                                    intent.putExtra("value", "logOut");
                                    intent.setAction("notifyData");
                                    sendBroadcast(intent);
                                }

                            } else {
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setError(true);
//                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setErrorDesc("Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                                generateNoteOnSD(DSAPP.getContext(), "Response code: " + response.code() + " : " + json.toString() + "\n " + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                                if (!recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().isManualEmailSent()) {
                                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getDealershipDetails().getDealershipId() + "", "Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getVINNumber(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarMake(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarModel());
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setManualEmailSent(true);
                                } else
                                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
                            }
//                            if (json.getString("Message").contains("error") || json.getString("Message").contains("Error")) {
//
//                            }
//                            if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)

                            Log.e("Response ", "onResponse: " + json.toString());
                        } catch (IOException | JSONException e) {
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setError(true);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setErrorDesc(json + "Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + "\n " + e.getLocalizedMessage() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));


                            generateNoteOnSD(DSAPP.getContext(), json + "\n " + e.getLocalizedMessage() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                            if (!recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().isManualEmailSent()) {
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getDealershipDetails().getDealershipId() + "", "Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getVINNumber(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarMake(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarModel());
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setManualEmailSent(true);
                            } else
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
//                            writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + "\n" + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
                            e.printStackTrace();
                        }
                        //assert json != null;
                        if (response.code() == 401 || response.code() == 403) {
                            if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                callRefreshTokenAPI(record);
                            } else {
                                Intent intent = new Intent();
                                //assert response.body() != null;
                                intent.putExtra("value", "logOut");
                                intent.setAction("notifyData");
                                sendBroadcast(intent);
                            }

                        }
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    }

                }

                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    }
                    if (t.getLocalizedMessage().contains("timeout")) {
                        if (timer != null) {
                            timer.cancel();
                        }

                        start5MinFunction();
                    }
                    Log.e("MyService class error: ", t.getLocalizedMessage());

                }
            });


        } else {
            VehicleDataUploadResponse vehicleDataUploadResponse = null;
            try {
                vehicleDataUploadResponse = getVehicleData(record);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Timber.e(gson.toJson(vehicleDataUploadResponse));
            Call<ImageUploadResponse> call = RetrofitInitialization.getDs_services().uploadVehicleDetail("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), vehicleDataUploadResponse);
            final VehicleDataUploadResponse finalVehicleDataUploadResponse = vehicleDataUploadResponse;
            call.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(@NonNull Call<ImageUploadResponse> call, @NonNull Response<ImageUploadResponse> response) {
                    try {
                        Timber.e("Complete deal onResponse: " + response.toString() + " Message " + response.message() + " " + response.errorBody());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //assert response.body() != null;
                    if (response.isSuccessful() && response.body().isSucceeded()) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        writeCustomLogsAPI(response.body().getValue(), "Save Complete Deal Data Json: " + gson.toJson(finalVehicleDataUploadResponse));
                        if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0)
                            writeCustomLogsAPI(response.body().getValue(), "Save deal incomplete data Json before making server object : " + gson.toJson(record));

                        Log.e("Vehicle Info uploaded :", "onResponse: " + record.getCustomerDetails().getDLNumber());
//                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + "\n" + "TextDataUpload onResponse: " + record.getCustomerDetails().getDLNumber());
                        try {
                            if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setError(false);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setErrorDesc("");
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                                if (!record.getSettingsAndPermissions().isBussinessLead()) {
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIsDataSynced(response.body().isSucceeded());
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCustomerVehicleTradeId(response.body().getValue());
                                } else {
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCustomerVehicleTradeId(response.body().getValue());
                                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLeadSynced(response.body().isSucceeded());
                                }
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs());
                            }
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                            /*upload file*/
                            //assert response.body() != null;
                            if (response.body().getValue() != null) {
                                Gson gson1 = new GsonBuilder().create();
                                RecordData recordData1 = gson1.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                uploadFile(recordData1.where(record.getCustomerDetails().getDLNumber()));
                            }
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                            writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + "\n" + e.getLocalizedMessage() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
                        }

//                    startSyncing();
                    } else {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        JSONObject json = null;
                        try {

                            //assert response.errorBody() != null;
                            json = new JSONObject(response.errorBody().string());
                            if (response.code() == 401 || response.code() == 403) {
                                if (json.getString("Message").contains("INACTIVE")) {
                                    String[] arr = json.getString("Message").split("\\|");
//                                    new CustomToast(getApplicationContext()).toast(arr[1]);
                                    Intent intent = new Intent();
                                    //assert response.body() != null;
                                    intent.putExtra("value", "logOut");
                                    intent.setAction("notifyData");
                                    sendBroadcast(intent);
                                } else if (response.code() == 401) {
                                    Intent intent = new Intent();
                                    //assert response.body() != null;
                                    intent.putExtra("value", "logOut");
                                    intent.setAction("notifyData");
                                    sendBroadcast(intent);
                                }

                            }
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setError(true);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setErrorDesc("Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);

                            generateNoteOnSD(DSAPP.getContext(), "Response code: " + response.code() + " : " + json.toString() + "\n " + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                            if (!recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().isManualEmailSent()) {
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getDealershipDetails().getDealershipId() + "", "Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getVINNumber(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarMake(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarModel());
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setManualEmailSent(true);
                            } else
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
//                            writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
                            Log.e("Response ", "onResponse: " + json.toString());
                        } catch (IOException | JSONException e) {
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setError(true);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setErrorDesc(json + "Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + "\n " + e.getLocalizedMessage() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                            String recordDataString1 = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);

                            generateNoteOnSD(DSAPP.getContext(), json + "\n " + e.getLocalizedMessage() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse));
                            if (!recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().isManualEmailSent()) {
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getDealershipDetails().getDealershipId() + "", "Access token: " + "bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + json.toString() + "\n Failed json: " + gson.toJson(finalVehicleDataUploadResponse), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getVINNumber(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarMake(), recordData.where(record.getCustomerDetails().getDLNumber()).getVehicleDetails().getCarModel());
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setManualEmailSent(true);
                            } else
                                writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
//                            writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs() + "\n" + " Json: " + gson.toJson(finalVehicleDataUploadResponse));
                            e.printStackTrace();
                        }
                        if (response.code() == 401 || response.code() == 403) {
                            if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                callRefreshTokenAPI(record);
                            } else {
                                Intent intent = new Intent();
                                //assert response.body() != null;
                                intent.putExtra("value", "logOut");
                                intent.setAction("notifyData");
                                sendBroadcast(intent);
                            }
                        }

                    }

                }

                @Override
                public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    }
                    if (t.getLocalizedMessage().contains("timeout")) {
                        if (timer != null) {
                            timer.cancel();
                        }
                        start5MinFunction();
                    }
                    Log.e("MyService class error: ", t.getLocalizedMessage());

                }
            });
        }

    }

    private void writeCustomLogsAPI(String id, String dID, String logs, String vin, String make, String model) {
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setDealershipId(dID);
        customLogs.setEmailSubject(null);
        customLogs.setIsSendAlertEmail(true);
//        ArrayList<String> emails=new ArrayList<>();
//        emails.add("shailendra.m@volgainfotech.com");
//        emails.add(getArguments().getString("data"));
//        customLogs.setEmails(emails);
        customLogs.setLogText("<div><b>VIN:</b> " + vin + " <br><b>Make:</b> " + make + " <br><b>Model:</b> " + model + " <br><b>JSON DATA: </b><br>" + logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN) + "</div>");
//        customLogs.setLogText(logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    private void callRefreshTokenAPI(final Record record) {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    Log.e("Refresh: ", "Success");
                    LoginData loginData = response.body();
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
                    if (timer != null) {
                        timer.cancel();
                    }
                    start5MinFunction();
                    writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Token was refreshed of this user ");
                } else {
                    Log.e("Refresh: ", "Fail Logging out");
                    Intent intent = new Intent();
                    //assert response.body() != null;
                    intent.putExtra("value", "logOut");
                    intent.setAction("notifyData");
                    sendBroadcast(intent);
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Error in token refreshing: " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Error in token refreshing: " + t.getLocalizedMessage());
            }
        });
    }

    private IncompleteDealPojo getIncompleteDealPojo(Record record) {
        IncompleteDealPojo response = new IncompleteDealPojo();

        if (record.getCustomerDetails().getFirstName() == null || record.getCustomerDetails().getFirstName().length() == 0) {
            response.setFirstName(record.getVehicleDetails().getBuyerFistName());
        } else {
            response.setFirstName(record.getCustomerDetails().getFirstName());
        }
        if (record.getCustomerDetails().getLastName() == null || record.getCustomerDetails().getLastName().length() == 0) {
            response.setLastName(record.getVehicleDetails().getBuyerLastName());
        } else {
            response.setLastName(record.getCustomerDetails().getLastName());
        }
//        response.setFirstName(record.getCustomerDetails().getFirstName());
//        response.setLastName(record.getCustomerDetails().getLastName());
        response.setMiddleName(record.getCustomerDetails().getMiddleName());
        response.setIsRemote(record.getCustomerDetails().getIsRemote());
        response.setDisclosureSkipped(record.getSettingsAndPermissions().isDisclosureSkipped());
        SimpleDateFormat originalFormatter = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault());
        SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        Date date;

        /*disclosure*/
        ArrayList<HistoryListPojo> disclosure = null;
        if (record.getVehicleDetails() != null)
            disclosure = record.getVehicleDetails().getUsedVehicleCondition();
        ArrayList<ConditionDisclosures> disclosureList = new ArrayList<>();
        if (disclosure != null) {
            for (int i = 0; i < disclosure.size(); i++) {
                if (disclosure.get(i).isChecked() && i != disclosure.size() - 1) {
                    if (!disclosure.get(i).getId().equalsIgnoreCase("0")) {
                        ConditionDisclosures thirdPartyHistoryReport = new ConditionDisclosures();
                        thirdPartyHistoryReport.setVehicleConditionDisclosureId(disclosure.get(i).getId());
                        thirdPartyHistoryReport.setValue(disclosure.get(i).getAdditionalData());
                        thirdPartyHistoryReport.setVerified(disclosure.get(i).isVerified());
                        disclosureList.add(thirdPartyHistoryReport);
                    }
                }
            }
            response.setConditionDisclosures(disclosureList);
        } else {
            if (record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures() != null && record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().size() > 0) {
                ArrayList<ConditionDisclosures> fromServer = new ArrayList<>();
                for (int i = 0; i < record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().size(); i++) {

                    ConditionDisclosures thirdPartyHistoryReport = new ConditionDisclosures();
                    thirdPartyHistoryReport.setVehicleConditionDisclosureId(record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().get(i).getVehicleConditionDisclosureId());
                    thirdPartyHistoryReport.setValue(record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().get(i).getValue());
//                    thirdPartyHistoryReport.setVerified(record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().get(i).isVerified());
                    fromServer.add(thirdPartyHistoryReport);
                }
                response.setConditionDisclosures(fromServer);
            }
        }
        /*do all for history and third party reports*/
        if (Objects.requireNonNull(record).getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0 && !record.getSettingsAndPermissions().getCustomerVehicleTradeId().equalsIgnoreCase("0"))
            response.setCustomerVehicleTradeId(Integer.parseInt(record.getSettingsAndPermissions().getCustomerVehicleTradeId()));

        /*history*/
        ArrayList<HistoryListPojo> listPojos = record.getSettingsAndPermissions().getPrintReceivedList();
        ArrayList<CustomerAgreement> customerAgreements = new ArrayList<>();
        if (listPojos != null) {
            for (int i = 0; i < listPojos.size(); i++) {
                CustomerAgreement customerAgreement = new CustomerAgreement();
                customerAgreement.setCustomerSignAgreementsId(listPojos.get(i).getId());
                customerAgreement.setValue(listPojos.get(i).isValue());
                customerAgreements.add(customerAgreement);
            }
        }
        response.setCustomerAgreement(customerAgreements);

        response.setCustomerVehicleDealAlerts(record.getVehicleDetails().getCustomerVehicleDealAlerts());

        /*Report docs data*/
        response.setThirdPartyReportDocs(record.getVehicleDetails().getThirdPartyReportDocs());
        ArrayList<HistoryListPojo> list = null;
        if (record.getVehicleDetails() != null)
            list = record.getVehicleDetails().getUsedVehicleReport();
        ArrayList<ThirdPartyHistoryReport> historyReports = new ArrayList<>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isChecked() && i != list.size() - 1) {
                    ThirdPartyHistoryReport thirdPartyHistoryReport = new ThirdPartyHistoryReport();
                    thirdPartyHistoryReport.setThirdPartyHistoryReportId(list.get(i).getId());
                    thirdPartyHistoryReport.setVerified(list.get(i).isVerified());
                    historyReports.add(thirdPartyHistoryReport);
                }
            }
            response.setThirdPartyHistoryReport(historyReports);
        } else {
            if (record.getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports() != null && record.getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports().size() > 0) {
                ArrayList<ThirdPartyHistoryReport> fromServer = new ArrayList<>();
                for (int i = 0; i < record.getSettingsAndPermissions().getVehicleTradeThirdPartyHistoryReports().size(); i++) {

                    ThirdPartyHistoryReport thirdPartyHistoryReport = new ThirdPartyHistoryReport();
                    thirdPartyHistoryReport.setThirdPartyHistoryReportId(record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().get(i).getVehicleConditionDisclosureId());
//                    thirdPartyHistoryReport.setVerified(record.getSettingsAndPermissions().getVehicleTradeConditionDisclosures().get(i).isve());
                    fromServer.add(thirdPartyHistoryReport);
                }
                response.setThirdPartyHistoryReport(fromServer);
            }
        }

        ArrayList<HistoryListPojo> list1 = null;
        if (record.getVehicleDetails() != null)
            list1 = record.getVehicleDetails().getUsedVehicleHistory();
        ArrayList<HistoryDisclosures> history = new ArrayList<>();
        if (list1 != null) {
            for (int i = 0; i < list1.size(); i++) {
                if (list1.get(i).isChecked() && i != list1.size() - 1) {
                    HistoryDisclosures thirdPartyHistoryReport = new HistoryDisclosures();
                    thirdPartyHistoryReport.setVehicleHistoryDisclosureId(list1.get(i).getId());
                    thirdPartyHistoryReport.setVerified(list1.get(i).isVerified());
//                    try {
//                        if (list1.get(i).getId().equalsIgnoreCase("15")) {
//                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate1().toString())));
//                        } else if (list1.get(i).getId().equalsIgnoreCase("16")) {
//                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate2().toString())));
//                        } else {
//                            thirdPartyHistoryReport.setDate("");
//                        }
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
                    history.add(thirdPartyHistoryReport);
                }

            }
            response.setHistoryDisclosures(history);
        } else {
            if (record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures() != null && record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().size() > 0) {
                ArrayList<HistoryDisclosures> fromServer = new ArrayList<>();
                for (int i = 0; i < record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().size(); i++) {

                    HistoryDisclosures thirdPartyHistoryReport = new HistoryDisclosures();

                    thirdPartyHistoryReport.setVehicleHistoryDisclosureId(record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().get(i).getVehicleHistoryDisclosureId());
//                    thirdPartyHistoryReport.setVerified(record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().get(i).isv());
//                    try {
//                        if (record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().get(i).getVehicleHistoryDisclosureId().equalsIgnoreCase("15")) {
//                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate1().toString())));
//                        } else if (record.getSettingsAndPermissions().getVehicleTradeHistoryDisclosures().get(i).getVehicleHistoryDisclosureId().equalsIgnoreCase("16")) {
//                            thirdPartyHistoryReport.setDate(tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getHistoryDate2().toString())));
//                        } else {
//                            thirdPartyHistoryReport.setDate("");
//                        }
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
                    fromServer.add(thirdPartyHistoryReport);
                }
                response.setHistoryDisclosures(fromServer);
            }
        }
        /* add third party rows*/

        if (record.getVehicleDetails().getInitiateThirdParty() != null) {
            response.setThirdPartyReportRaws(record.getVehicleDetails().getInitiateThirdParty().getValue());
        }
        try {
            if (record.getCustomerDetails().getDateOfBirth() != null && record.getCustomerDetails().getDateOfBirth().length() > 0) {
                date = originalFormatter.parse(record.getCustomerDetails().getDateOfBirth());
//                response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
                int month = 0, dates = 0, year = 0;
                String dob = record.getCustomerDetails().getDateOfBirth();
                String[] str = dob.split("-");
                month = Integer.parseInt(str[0]);
                dates = Integer.parseInt(str[1]);
                year = Integer.parseInt(str[2]);
                if (!((month > 0 && month <= 12) && (dates > 0 && dates <= 31) && (year > 1900 && year < Calendar.getInstance().get(Calendar.YEAR)))) {
                    response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse("01-01-1991")));
                } else {
                    response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
                }

            }
//            response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
            if (record.getCustomerDetails().getDLExpiryDate() != null && record.getCustomerDetails().getDLExpiryDate().length() > 0) {
//                response.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDLExpiryDate())));
                int month1 = 0, date1 = 0, year1 = 0;
                String dob = record.getCustomerDetails().getDLExpiryDate();
                String[] str = dob.split("-");
                month1 = Integer.parseInt(str[0]);
                date1 = Integer.parseInt(str[1]);
                year1 = Integer.parseInt(str[2]);
                if (!((month1 > 0 && month1 <= 12) && (date1 > 0 && date1 <= 31) && (year1 >= Calendar.getInstance().get(Calendar.YEAR)))) {
                    response.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse("09-09-2019")));
                } else {
                    response.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDLExpiryDate())));
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
            /*write log to api*/

        }
        try {
            response.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
            response.setTradeCompletionUTC(record.getSettingsAndPermissions().getTradeCompletionUTC());
//Log.e(" UTC Time: ",record.getSettingsAndPermissions().getTradeCompletionUTC());
            Log.e("TradeID date: ", tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (response.getTradeDateTime() == null || response.getTradeDateTime().length() == 0) {
            try {
                String path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
                    File file = new File(path_signature + "/IMG_prints_recieved_selection.svg");
                    response.setTradeDateTime(tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString())));
                    Log.e("TradeID date: ", response.getTradeDateTime());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        ArrayList<com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.SalesPersons> salesPersons = new ArrayList<>();
        com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.SalesPersons salesPersonsObj = new com.volga.dealershieldtablet.Retrofit.Pojo.TabletInterConnectivity.SalesPersons();
        if (record.getCustomerDetails().getSalesPersons() != null && record.getCustomerDetails().getSalesPersons().getUserId() != null && record.getCustomerDetails().getSalesPersons().getUserId().length() > 0) {
            try {
                salesPersonsObj.setUserId(record.getCustomerDetails().getSalesPersons().getUserId());
                salesPersonsObj.setEmail(record.getCustomerDetails().getSalesPersons().getEmail().trim());
                salesPersonsObj.setFirstName(record.getCustomerDetails().getSalesPersons().getFirstName());
                salesPersonsObj.setLastName(record.getCustomerDetails().getSalesPersons().getLastName());

                if (record.getCustomerDetails().getSalesPersons().getMiddleName() != null && record.getCustomerDetails().getSalesPersons().getMiddleName().length() > 0) {
                    salesPersonsObj.setSalesPersonId(record.getCustomerDetails().getSalesPersons().getMiddleName());
                }
                salesPersons.add(salesPersonsObj);
            } catch (Exception e) {
                Timber.tag("Error : ").e(e.toString());
                salesPersons.add(salesPersonsObj);
            }
        } else {
            salesPersonsObj.setUserId(record.getVehicleDetails().getSalesId());
        }
        response.setSalesPersons(salesPersons);
        ArrayList<AdditionalDisclosureToUpload> additionalDisclosure = new ArrayList<>();
        if (record.getVehicleDetails().getAdditionalDisclosure() != null && record.getVehicleDetails().getAdditionalDisclosure().getValue() != null) {

            for (int i = 0; i < record.getVehicleDetails().getAdditionalDisclosure().getValue().size(); i++) {
                AdditionalDisclosureToUpload additionalDisclosureToUpload = new AdditionalDisclosureToUpload();
                additionalDisclosureToUpload.setAdditionalDisclosureId(record.getVehicleDetails().getAdditionalDisclosure().getValue().get(i).getId());
                additionalDisclosureToUpload.setDisclosureType("CONDITION");
                additionalDisclosure.add(additionalDisclosureToUpload);
            }
        }
        if (record.getVehicleDetails().getAdditionalHistoryDisclosure() != null && record.getVehicleDetails().getAdditionalHistoryDisclosure().getValue() != null) {
            for (int i = 0; i < record.getVehicleDetails().getAdditionalHistoryDisclosure().getValue().size(); i++) {
                AdditionalDisclosureToUpload additionalDisclosureToUpload = new AdditionalDisclosureToUpload();
                additionalDisclosureToUpload.setAdditionalDisclosureId(record.getVehicleDetails().getAdditionalHistoryDisclosure().getValue().get(i).getId());
                additionalDisclosureToUpload.setDisclosureType("HISTORY");
                additionalDisclosure.add(additionalDisclosureToUpload);
            }
        }
        response.setAdditinalDisclosures(additionalDisclosure);
        response.setAddress1(record.getCustomerDetails().getAddressLineOne() == null ? "" : record.getCustomerDetails().getAddressLineOne().trim());
        response.setIsTermConditionAccepted(record.getCustomerDetails().isTermConditionAccepted());
        response.setAddress2(record.getCustomerDetails().getAddressLineTwo() == null ? "" : record.getCustomerDetails().getAddressLineTwo().trim());
        response.setAllow2RemoveWindowSticker(record.getSettingsAndPermissions().getStickerRemovalPermission());
        if (record.getCustomerDetails().getCustomerId() != null && record.getCustomerDetails().getCustomerId().length() > 0) {
            response.setCustomerId(record.getCustomerDetails().getCustomerId());
        }
        if (record.getVehicleDetails().getTypeOfVehicle() != null) {
            if (record.getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("Used")) {
                response.setCarTypeId(1 + "");
            } else {
                response.setIsAcceptedUsedCarPolicy(record.getSettingsAndPermissions().isAcceptedUsedCarPolicy());
                response.setCarTypeId(2 + "");
            }
        }
        response.setCity(record.getCustomerDetails().getCity());
        response.setIsLead(record.getSettingsAndPermissions().isBussinessLead());
        response.setContactNumber(record.getCustomerDetails().getMobileNumber());
        if (record.getCustomerDetails().getIsRemote() && !record.getSettingsAndPermissions().isHasCoBuyer()) {
            response.setLastScreenName("Deal got completed as remote");
        } else if (record.getCustomerDetails().getIsRemote() && record.getSettingsAndPermissions().isHasCoBuyer() && !record.getCoBuyerCustomerDetails().getIsRemote()) {
            response.setLastScreenName("Final Thank you page. Co-buyer Signature completed.");
        } else if (!record.getCustomerDetails().getIsRemote() && record.getSettingsAndPermissions().isHasCoBuyer() && record.getCoBuyerCustomerDetails().getIsRemote()) {
            response.setLastScreenName("Final Thank you page. Buyer Signature completed.");
        } else {
            response.setLastScreenName(getScreenName(record.getSettingsAndPermissions().getLastPageIndex(), record));
        }
//        response.setLastScreenName(getScreenName(record.getSettingsAndPermissions().getLastPageIndex(), record));
        if ((record.getCustomerDetails().getDrivingLicenceNumber() == null || record.getCustomerDetails().getDrivingLicenceNumber().trim().length() == 0) && record.getCustomerDetails().getFirstName() != null && record.getCustomerDetails().getFirstName().length() != 0) {
            response.setDrivingLicenseNumber(record.getCustomerDetails().getDLNumber().substring(0, record.getCustomerDetails().getDLNumber().length() - 4));
        } else {
            response.setDrivingLicenseNumber(record.getCustomerDetails().getDrivingLicenceNumber());
        }
        response.setDealershipId(record.getDealershipDetails().getDealershipId());
        response.setEmail(record.getCustomerDetails().getEmail() == null ? "".trim() : record.getCustomerDetails().getEmail().trim());
        response.setZipcode(record.getCustomerDetails().getZipCode());
        response.setVIN(record.getVehicleDetails().getVINNumber());
        response.setMake(record.getVehicleDetails().getCarMake());
        response.setModel(record.getVehicleDetails().getCarModel());
        response.setModelYear(record.getVehicleDetails().getCarYear() == null || record.getVehicleDetails().getCarYear().equalsIgnoreCase("") ? 0 : Integer.parseInt(record.getVehicleDetails().getCarYear()));
        response.setMileage(record.getVehicleDetails().getMileage());
        response.setStockNumber(record.getVehicleDetails().getStockNumber());
        response.setVerbalPromises(record.getSettingsAndPermissions().getVerbalPromises());
        response.setCheckLists(record.getSettingsAndPermissions().getCheckLists());
        if (record.getSettingsAndPermissions().isHasCoBuyer()) {
            CoBuyerDetail coBuyerCustomerDetails = new CoBuyerDetail();
            if (coBuyerCustomerDetails.getFirstName() == null || coBuyerCustomerDetails.getFirstName().length() == 0) {
                coBuyerCustomerDetails.setFirstName(record.getVehicleDetails().getCoFirstName());
            } else {
                coBuyerCustomerDetails.setFirstName(record.getCoBuyerCustomerDetails().getFirstName());
            }
            if (coBuyerCustomerDetails.getLastName() == null || coBuyerCustomerDetails.getLastName().length() == 0) {
                coBuyerCustomerDetails.setLastName(record.getVehicleDetails().getCoLastName());
            } else {
                coBuyerCustomerDetails.setLastName(record.getCoBuyerCustomerDetails().getLastName());
            }
            coBuyerCustomerDetails.setMiddleName(record.getCoBuyerCustomerDetails().getMiddleName());

            try {
                if (record.getCoBuyerCustomerDetails().getDateOfBirth() != null && record.getCoBuyerCustomerDetails().getDateOfBirth().length() > 0) {
                    date = originalFormatter.parse(record.getCoBuyerCustomerDetails().getDateOfBirth());
//                response.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCustomerDetails().getDateOfBirth())));
                    int month = 0, dates = 0, year = 0;
                    String dob = record.getCoBuyerCustomerDetails().getDateOfBirth();
                    String[] str = dob.split("-");
                    month = Integer.parseInt(str[0]);
                    dates = Integer.parseInt(str[1]);
                    year = Integer.parseInt(str[2]);
                    if (!((month > 0 && month <= 12) && (dates > 0 && dates <= 31) && (year > 1900 && year < Calendar.getInstance().get(Calendar.YEAR)))) {
                        coBuyerCustomerDetails.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse("01-01-1991")));
                    } else {
                        coBuyerCustomerDetails.setDateOfBirth(tradeTimeFormatter.format(originalFormatter.parse(record.getCoBuyerCustomerDetails().getDateOfBirth())));
                    }

                }
                if (record.getCoBuyerCustomerDetails().getDLExpiryDate() != null && record.getCoBuyerCustomerDetails().getDLExpiryDate().length() > 0) {
                    int month1 = 0, date1 = 0, year1 = 0;
                    String dob = record.getCoBuyerCustomerDetails().getDLExpiryDate();
                    String[] str = dob.split("-");
                    month1 = Integer.parseInt(str[0]);
                    date1 = Integer.parseInt(str[1]);
                    year1 = Integer.parseInt(str[2]);
                    if (!((month1 > 0 && month1 <= 12) && (date1 > 0 && date1 <= 31) && (year1 >= Calendar.getInstance().get(Calendar.YEAR)))) {
                        coBuyerCustomerDetails.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse("09-09-2019")));
                    } else {
                        coBuyerCustomerDetails.setDrivingLicenseExpiryDate(tradeTimeFormatter.format(originalFormatter.parse(record.getCoBuyerCustomerDetails().getDLExpiryDate())));
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            coBuyerCustomerDetails.setAddress1(record.getCoBuyerCustomerDetails().getAddressLineOne());
            coBuyerCustomerDetails.setAddress2(record.getCoBuyerCustomerDetails().getAddressLineTwo());
            coBuyerCustomerDetails.setDrivingLicenseNumber(record.getCoBuyerCustomerDetails().getDrivingLicenseNumber());
            coBuyerCustomerDetails.setEmail(record.getCoBuyerCustomerDetails().getEmail() != null ? record.getCoBuyerCustomerDetails().getEmail().trim() : null);
            coBuyerCustomerDetails.setContactNumber(record.getCoBuyerCustomerDetails().getMobileNumber());
            coBuyerCustomerDetails.setCity(record.getCoBuyerCustomerDetails().getCity());
            coBuyerCustomerDetails.setState(record.getCoBuyerCustomerDetails().getState());
            coBuyerCustomerDetails.setCountry(record.getCoBuyerCustomerDetails().getCountry());
            coBuyerCustomerDetails.setZipcode(record.getCoBuyerCustomerDetails().getZipCode());
            coBuyerCustomerDetails.setIsTermConditionAccepted(record.getCoBuyerCustomerDetails().isTermConditionAccepted());
            coBuyerCustomerDetails.setIsAcceptedUsedCarPolicy(record.getCoBuyerCustomerDetails().isAcceptedUsedCarPolicy());
            coBuyerCustomerDetails.setIsRemote(record.getCoBuyerCustomerDetails().getIsRemote());
            if (record.getCoBuyerCustomerDetails().getId() != null && record.getCoBuyerCustomerDetails().getId().length() > 0) {
                coBuyerCustomerDetails.setId(record.getCoBuyerCustomerDetails().getId());
            }
            coBuyerCustomerDetails.setCheckLists(record.getCoBuyerCustomerDetails().getCheckLists());
            coBuyerCustomerDetails.setCustomerAgreement(record.getCoBuyerCustomerDetails().getPrintReceivedList());
            coBuyerCustomerDetails.setVerbalPromises(record.getCoBuyerCustomerDetails().getVerbalPromises());
            coBuyerCustomerDetails.setIsTestDriveTaken(record.getSettingsAndPermissions().isCoBuyerTestDriveTaken());
            if (record.getSettingsAndPermissions().isCoBuyerTestDriveTaken())
                coBuyerCustomerDetails.setTestDriveDurationId(record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId() + "");
            else {
                coBuyerCustomerDetails.setTestDriveDurationId(record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getCoBuyerTestDriveTimeId() + "");
            }
//            coBuyerCustomerDetails.setIsTestDriveTaken(record.getSettingsAndPermissions().isCoBuyerNoTestDriveConfirm());
            /*Co-buyer module*/
            if (record.getCoBuyerCustomerDetails().getGender() != null) {
                if (record.getCoBuyerCustomerDetails().getGender().equalsIgnoreCase("male"))
                    coBuyerCustomerDetails.setGenderTypeId(1/*record.getCustomerDetails().getGender()*/);
                else if (record.getCoBuyerCustomerDetails().getGender().equalsIgnoreCase("female"))
                    coBuyerCustomerDetails.setGenderTypeId(2/*record.getCustomerDetails().getGender()*/);
                else {
                    coBuyerCustomerDetails.setGenderTypeId(3/*record.getCustomerDetails().getGender()*/);
                }
            } else {
                coBuyerCustomerDetails.setGenderTypeId(3/*record.getCustomerDetails().getGender()*/);
            }
            coBuyerCustomerDetails.setCustomerAgreement(record.getCoBuyerCustomerDetails().getPrintReceivedList());
            coBuyerCustomerDetails.setLanguageId(record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() == 0 ? null : record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() + "");
            response.setCoBuyerDetail(coBuyerCustomerDetails);
        }
        TradeInVehicle tradeInVehicle = new TradeInVehicle();
        if (record.getSettingsAndPermissions().isHasTradeIn() && record.getTradeInVehicle().getVINNumber() != null && record.getTradeInVehicle().getVINNumber().length() > 0) {
            if (record.getTradeInVehicle().getId() != null && record.getTradeInVehicle().getId().length() > 0) {
                tradeInVehicle.setId(Integer.parseInt(record.getTradeInVehicle().getId()));
            }
            tradeInVehicle.setVIN(record.getTradeInVehicle().getVINNumber());
            tradeInVehicle.setModel(record.getTradeInVehicle().getCarModel());
            tradeInVehicle.setMake(record.getTradeInVehicle().getCarMake());
            tradeInVehicle.setModelYear(Integer.parseInt(record.getTradeInVehicle().getCarYear()));
            tradeInVehicle.setMileage(record.getTradeInVehicle().getMileage());
            tradeInVehicle.setExteriorColor(record.getTradeInVehicle().getExteriorColor());
            tradeInVehicle.setInteriorColor(record.getTradeInVehicle().getInteriorColor());

            response.setTradeInVehicle(tradeInVehicle);
        }
        if (record.getVehicleDetails().getTypeOfPurchase() != null) {
            if (record.getVehicleDetails().getTypeOfPurchase().trim().length() == 1)
                response.setTradeTypeId(record.getVehicleDetails().getTypeOfPurchase());
            else {
                if (record.getVehicleDetails().getTypeOfPurchase().equalsIgnoreCase("lease")) {
                    response.setTradeTypeId("2");
                } else {
                    response.setTradeTypeId("1");
                }

            }
        }
        if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getTabletUniquenessId() > 0) {
            response.setTabletUniquenessId(PreferenceManger.getUniqueTabName().getTabletUniquenessId());
        }
        if (record.getSettingsAndPermissions().getTestDriveTaken())
            response.setTestDriveDurationId(record.getSettingsAndPermissions().getTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getTestDriveTimeId() + "");
        else {
            response.setTestDriveDurationId(record.getSettingsAndPermissions().getTestDriveTimeId() == 0 ? null : record.getSettingsAndPermissions().getTestDriveTimeId() + "");
        }
        response.setState(record.getCustomerDetails().getState());
        response.setCountry(record.getCustomerDetails().getCountry());
        /*moved to very first line*/
        response.setLanguageId(record.getSettingsAndPermissions().getSelectedLanguageId() == 0 ? null : record.getSettingsAndPermissions().getSelectedLanguageId() + "");
        response.setIsTestDriveTaken(record.getSettingsAndPermissions().getTestDriveTaken());
        if (record.getCustomerDetails().getGender() != null && record.getCustomerDetails().getGender().equalsIgnoreCase("male"))
            response.setGenderTypeId(1/*record.getCustomerDetails().getGender()*/);
        else if (record.getCustomerDetails().getGender() != null && record.getCustomerDetails().getGender().equalsIgnoreCase("female"))
            response.setGenderTypeId(2/*record.getCustomerDetails().getGender()*/);
        else {
            response.setGenderTypeId(3/*record.getCustomerDetails().getGender()*/);
        }


        return response;
    }

    private void writeCustomLogsAPI(String id, String logs) {
        CustomLogs customLogs = new CustomLogs();
        customLogs.setCustomerVehicleTradeId(id);
        customLogs.setLogText(logs + "token: bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
        Call<ImageUploadResponse> logsCall = RetrofitInitialization.getDs_services().writeLogs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), customLogs);
        logsCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Log.e("logs data value: ", response.body().getValue());
                } else {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(response.errorBody().string());
                        Log.e("Error response ", "json : " + json.toString());
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                Log.e("Log Api failed: ", t.getLocalizedMessage());
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void uploadFile(final Record record) {

//        String path_licence, path_signature, path_car;
//        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
//        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
//        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
//        makeHashMapOfImages(record);onFailure
        ArrayList<ImagesSynced> arrayList = null;
        if (record.getSettingsAndPermissions() != null)
            arrayList = record.getSettingsAndPermissions().getImagesSyncedArrayList();
//        arrayList.clear();
//        ArrayList<ImagesSynced> arrayList=doubleCheckFileIsExistOrNot(arrayLists);
        if (arrayList != null) {
            uploadImage(arrayList.size(), arrayList, record);
        } else {
            if (record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null) {
//                ArrayList<ImagesSynced>  arrayList=makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)));
//                arrayList.addAll(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getMultiSignImages());
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(arrayList);
                makeHashMapOfImages(record);
            }
//            else if (record.getSettingsAndPermissions() != null && !record.getSettingsAndPermissions().isIncompleteDealSynced() && record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
//                try {
//                    if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
//                        callSyncCompletedAPI(record);
//                    } else if (record.getSettingsAndPermissions().isSentEmailForIncomplete()) {
//                        callSyncCompletedAPI(record);
//                    }
//                } catch (JsonSyntaxException e) {
//                    e.printStackTrace();
//                }
//            }
        }
//        recursiveCallForImageUpload(arrayList,record);

//        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
//            startImageSync(arrayList, new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
//        } else if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
//            startImageSync(arrayList, new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
//        }
//        /*Car Images*/
//        else if (new File(path_car + "/IMG_front.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
//        } else if (new File(path_car + "/IMG_right.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
//        } else if (new File(path_car + "/IMG_rear.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
//        } else if (new File(path_car + "/IMG_left.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
//        } else if (new File(path_car + "/IMG_sticker.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
//        } else if (new File(path_car + "/IMG_mileage.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
//        } else if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
//        } else if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
//        }
//        /*Additional Images*/
//        else if (new File(path_car + "/IMG_Additional1.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle1() == null) {
//                record.getVehicleDetails().setTitle1("Additional Image 1");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
//        } else if (new File(path_car + "/IMG_Additional2.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle2() == null) {
//                record.getVehicleDetails().setTitle2("Additional Image 2");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
//        } else if (new File(path_car + "/IMG_Additional3.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle3() == null) {
//                record.getVehicleDetails().setTitle3("Additional Image 3");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
//        } else if (new File(path_car + "/IMG_Additional4.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle4() == null) {
//                record.getVehicleDetails().setTitle4("Additional Image 4");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
//        } else if (new File(path_car + "/IMG_Additional5.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle5() == null) {
//                record.getVehicleDetails().setTitle5("Additional Image 5");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
//        } else if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle1() == null) {
//                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
//        } else if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle2() == null) {
//                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
//        } else if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle3() == null) {
//                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
//        } else if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle4() == null) {
//                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
//        } else if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle5() == null) {
//                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
//            }
//            startImageSync(arrayList, new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
//        } else if (new File(path_car + "/IMG_insurance.jpg").exists()) {
//            startImageSync(arrayList, new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
//        }
//        //Add all signature image here
//        else if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        } else if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
//        } else if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
//        } else if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
//        } else if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
//        } else if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
//        } else if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
//        } else if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
//            startImageSync(arrayList, new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
//        } else if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
//        } else if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
//        } else if (new File(path_signature + "/IMG_history_report.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
//        } else if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
//        } else if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
//            startImageSync(arrayList, new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
//        } else if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
//            startImageSync(arrayList, new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        } else {
//            //sendEmail
//            if (!record.getSettingsAndPermissions().isBussinessLead()) {
//
//                Gson gson = new GsonBuilder().create();
//                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                if (!recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().isEmailSent()) {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                sendEmailToCustomer(record);
//                            } catch (Exception e) {
//                                emailNotCalling = false;
//                                e.printStackTrace();
//                            }
//                        }
//                    }, 10000);
//                }
//            } else {
//                emailNotCalling = false;
//                if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLeadSynced(true);
//                    String recordDataString1 = gson.toJson(recordData);
//                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                }
//                clearFolder(record);
//            }
//        }

    }

    private ArrayList<ImagesSynced> doubleCheckFileIsExistOrNot(ArrayList<ImagesSynced> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (!arrayList.get(i).getFile().exists()) {
                arrayList.remove(arrayList.get(i));
            }
        }
        return arrayList;
    }

    private void uploadImage(int size, ArrayList<ImagesSynced> arrayList, Record record) {
        if (size > 0) {
            if (!arrayList.get(size - 1).isSynced()) {
                /*handled logic in case there is not co- buyer but still there are images*/
                if (!record.getSettingsAndPermissions().isHasCoBuyer() && arrayList.get(size - 1).getName().contains("co_")) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    ArrayList<ImagesSynced> list = recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList();
//                    ImagesSynced synced = list.remove(size - 1);
                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().remove(size - 1);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs());
                    uploadImage(size - 2, recordData1.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData1.where(record.getCustomerDetails().getDLNumber()));
                } else if (!record.getSettingsAndPermissions().isHasTradeIn() && ((arrayList.get(size - 1)).getName().toLowerCase().contains("tradein") /*|| arrayList.get(size - 1).getFile().getAbsolutePath().contains("tradein")*/ || arrayList.get(size - 1).getName().contains("LICENCEPLATENUMBER"))) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    ArrayList<ImagesSynced> list = recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList();
//                    ImagesSynced synced = list.remove(size - 1);
                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().remove(size - 1);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs());
                    uploadImage(size - 2, recordData1.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData1.where(record.getCustomerDetails().getDLNumber()));
                } else
                    startImageSyncForReccursion(size - 1, arrayList, arrayList.get(size - 1).getFile(), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), arrayList.get(size - 1).getName(), record);
            } else {
                uploadImage(size - 1, arrayList, record);
            }
        } else {
            // confirm total images
            getUploadedDocsList(record, arrayList);
        }
    }

    private void getUploadedDocsList(final Record record, final ArrayList<ImagesSynced> arrayList) {
        Call<ArrayList<UploadedDocs>> uploadedDocs = RetrofitInitialization.getDs_services().getUploadedDocs("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), record.getSettingsAndPermissions().getCustomerVehicleTradeId());
        uploadedDocs.enqueue(new Callback<ArrayList<UploadedDocs>>() {
            @Override
            public void onResponse(Call<ArrayList<UploadedDocs>> call, Response<ArrayList<UploadedDocs>> response) {
                try {
                    Log.e("Get Uploaded docs : ", "Response : " + response.message() + " error msg: " + response.errorBody());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful() && response.code() == 200) {
                    boolean allUploaded = true;
                    ArrayList<ImagesSynced> newArray = new ArrayList<>(arrayList);
                    for (int i = 0; i < newArray.size(); i++) {
                        if (!record.getSettingsAndPermissions().isHasCoBuyer() && arrayList.get(i).getName().contains("co_buyer")) {
                            newArray.get(i).setSynced(true);
                        } else
                            newArray.get(i).setSynced(false);
                    }
                    for (int i = 0; i < newArray.size(); i++) {
                        if (!record.getSettingsAndPermissions().isHasTradeIn() && (arrayList.get(i).getName().toLowerCase().contains("tradein") || arrayList.get(i).getFile().getAbsolutePath().contains("tradein") || arrayList.get(i).getName().toLowerCase().contains("LICENCEPLATENUMBER"))) {
                            newArray.get(i).setSynced(true);
                        } else
                            newArray.get(i).setSynced(false);
                    }
                    for (int i = 0; i < arrayList.size(); i++) {
                        //assert response.body() != null;
                        for (int j = 0; j < response.body().size(); j++) {
                            if (arrayList.get(i).getName().equalsIgnoreCase(response.body().get(j).getDocTypeName())) {
                                newArray.get(i).setSynced(true);
                            }
                        }

                    }
                    for (int i = 0; i < newArray.size(); i++) {
                        if (!newArray.get(i).isSynced()) {
                            allUploaded = false;
                        }

                    }
                    Timber.e("all uploaded : %s", allUploaded);
                    if (allUploaded) {
                        if (!record.getSettingsAndPermissions().isBussinessLead() && !record.getSettingsAndPermissions().isIncompleteDeal() && record.getSettingsAndPermissions().getIsRecordComplete()) {
                            if (!emailNotCalling)
                                sendEmailToCustomer(record);
                        } else {
                            if (!record.getSettingsAndPermissions().isIncompleteDealSynced() && !record.getSettingsAndPermissions().isBussinessLead() && !record.getSettingsAndPermissions().getIsRecordComplete() && record.getSettingsAndPermissions().isIncompleteDealTextSynced()) {
                                try {
                                    if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
                                        if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getTabletUniquenessId() > 0) {
                                            callSyncCompletedAPI(record);
                                        }
                                    } else if (record.getSettingsAndPermissions().isSentEmailForIncomplete()) {
                                        if (PreferenceManger.getUniqueTabName() != null && PreferenceManger.getUniqueTabName().getTabletUniquenessId() > 0) {
                                            callSyncCompletedAPI(record);
                                        }
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    //this code is added in 1.3.0 to remove leads from list
                                    //&& !record.getSettingsAndPermissions().isIncompleteDeal()
                                    if (record.getSettingsAndPermissions().isBussinessLead() && record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null && record.getSettingsAndPermissions().getCustomerVehicleTradeId().length() > 0) {
                                        Gson gson = new GsonBuilder().create();
                                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                        if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null)
                                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setBussinessLeadSynced(true);
                                        String recordDataString1 = gson.toJson(recordData);
                                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                        clearFolder(record);
                                    }
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
//                        sendEmailToCustomer(record);
                    } else {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        uploadImage(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().size(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData.where(record.getCustomerDetails().getDLNumber()));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<UploadedDocs>> call, @NonNull Throwable t) {

            }
        });
    }

    private void callSyncCompletedAPI(final Record record) {
        RetrofitInitialization.getDs_services().markIncompleteDealSync("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), PreferenceManger.getUniqueTabName().getTabletUniquenessId() + "").enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                try {
                    Log.e("Sync complete Api", response.message() + "  logs");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful() && response.code() == 200) {

                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setCustomerVehicleTradeId(record.getSettingsAndPermissions().getCustomerVehicleTradeId());
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                        if (record.getSettingsAndPermissions().isSentEmailForIncomplete() && !emailNotCalling) {
                            sendEmailToCustomer(record);
                        } else {
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIncompleteDealSynced(true);
                            recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIncompleteDeal(false);
//                            record.getSettingsAndPermissions().setFromServer(true);
                            Log.e("Incomplete Deal: ", "Incomplete deal was uploaded successfully");
                            Intent intent = new Intent();
                            intent.putExtra("value", "refreshIncompleteDeal");
                            intent.setAction("notifyData");
                            sendBroadcast(intent);
                            clearOnlyFolder(record);
                        }
                    }


                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);

                } else {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Mark Sync error ", "json: " + json.toString() + " Trade Id: " + record.getSettingsAndPermissions().getCustomerVehicleTradeId());
                        emailNotCalling = false;
                        writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Mark Sync error json: " + json.toString());
                    } catch (IOException | JSONException e) {
                        emailNotCalling = false;
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                emailNotCalling = false;
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }
                Log.e("Incomplete Deal Error: ", "Message: " + t.getLocalizedMessage());
            }
        });
    }


    private void sendEmailToCustomer(final Record record1) {

        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        final Record record = recordData.where(record1.getCustomerDetails().getDLNumber());
        if (record != null && record.getSettingsAndPermissions() != null && !record.getSettingsAndPermissions().isDataSyncedWithServer() && !record.getSettingsAndPermissions().isEmailSent()) {
            emailNotCalling = true;
            Log.e("Data", "uploadFile: Data synced");
            Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().sentMail("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), record.getSettingsAndPermissions().getCustomerVehicleTradeId());

            imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
                @Override
                public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                    //assert response.body() != null;
                    emailNotCalling = false;
                    Log.e("Trying to send email: ", response.message());
                    if (response.isSuccessful() && response.body().isSucceeded()) {
                        Intent intent = new Intent();
                        //assert response.body() != null;
                        intent.putExtra("value", response.body().getMessage());
                        Log.e("Message from Server: ", response.body().getMessage());
                        intent.setAction("notifyData");
                        sendBroadcast(intent);
                        Log.e("Email Sent for Id: ", record.getSettingsAndPermissions().getCustomerVehicleTradeId());
                        writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Email sent to vehicleTradeId : " + record.getSettingsAndPermissions().getCustomerVehicleTradeId());
                        try {
                            Gson gson = new GsonBuilder().create();
                            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                            if (recordData != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setDataSyncedWithServer(true);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setEmailSent(true);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIsRecordComplete(true);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIncompleteDeal(false);
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setIncompleteDealSynced(true);
//                                record.getSettingsAndPermissions().setFromServer(true);
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                clearFolder(record);

                            }
                        } catch (JsonSyntaxException e) {
                            emailNotCalling = false;
                            e.printStackTrace();
                        }
//                            Toast.makeText(MyService.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        //assert response.body() != null;
//                            showPopup(response.body().getMessage());

//                            if (activity != null) {
//                                activity.updateClient();
//                            }
                    } else {
                        emailNotCalling = false;
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                        if (response.code() == 401 || response.code() == 403) {
                            if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                callRefreshTokenAPI(record);
                            } else {
                                Intent intent = new Intent();
                                //assert response.body() != null;
                                intent.putExtra("value", "logOut");
                                intent.setAction("notifyData");
                                sendBroadcast(intent);
                            }
                        }
                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());
                            if (response.code() == 401 || response.code() == 403) {
                                if (json.getString("Message").contains("INACTIVE")) {
                                    String[] arr = json.getString("Message").split("\\|");
//                                    new CustomToast(getApplicationContext()).toast(arr[1]);
                                    Intent intent = new Intent();
                                    //assert response.body() != null;
                                    intent.putExtra("value", "logOut");
                                    intent.setAction("notifyData");
                                    sendBroadcast(intent);
                                }

                            }
                            Log.e("sent email error ", "json: " + json.toString());
                            emailNotCalling = false;
                            if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                                writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "sent email error json: " + json.toString());
                        } catch (IOException | JSONException e) {
                            emailNotCalling = false;
                            e.printStackTrace();

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ImageUploadResponse> call, Throwable t) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                    }

                    emailNotCalling = false;
                    if (t.getLocalizedMessage().contains("timeout")) {
//                        emailNotCalling = false;
                        if (timer != null) {
                            timer.cancel();
                        }
                        start5MinFunction();
                    }
                }
            });
        }
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        }
        Log.e("file deleted : ", fileOrDirectory.delete() + "");
    }

    private void clearFolder(Record record) {
        writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DMV images and all signature deleted after successful transaction.");
        Log.e("All Done ", "  Cheers");
        Log.e("Current User: ", PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record1 = recordData.where(record.getCustomerDetails().getDLNumber());
        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
            deleteRecursive(main);
        }
        recordData.getRecords().remove(record1);
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//        Intent intent = new Intent();
//        intent.putExtra("value", "refreshIncompleteDeal");
//        intent.setAction("notifyData");
//        sendBroadcast(intent);
//        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
//            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
//            deleteRecursive(main);
//        }
    }

    private void clearOnlyFolder(Record record) {
        writeCustomLogsAPI(record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Incomplete deal uploaded successfully");
        Log.e("All Done ", "  Only folders deleted");
        Log.e("Current User: ", PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record1 = recordData.where(record.getCustomerDetails().getDLNumber());
        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record1.getCustomerDetails().getDLNumber()).exists()) {
            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record1.getCustomerDetails().getDLNumber());
            deleteRecursive(main);
        }
//        recordData.getRecords().remove(record1);
//        String recordDataString1 = gson.toJson(recordData);
//        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//        Intent intent = new Intent();
//        intent.putExtra("value", "refreshIncompleteDeal");
//        intent.setAction("notifyData");
//        sendBroadcast(intent);
//        if (new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber()).exists()) {
//            File main = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber());
//            deleteRecursive(main);
//        }
    }

    private void showPopup(String textMessage) {
        LayoutInflater layoutInflater = LayoutInflater.from(MyNewService.this);
        View promptView = layoutInflater.inflate(R.layout.custom_toast, null);

        final AlertDialog alertD = new AlertDialog.Builder(MyNewService.this).create();
        alertD.setCancelable(false);
        alertD.setView(promptView);
        TextView text = (TextView) promptView.findViewById(R.id.text);
        text.setText(textMessage);
        alertD.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alertD.dismiss();
            }
        }, 4000);
    }

//    private void startImageSync(ArrayList<ImagesSynced> arrayList, final File file, final String id, final String docType, final Record record) {
////        System.gc();
//        boolean additional = false;
//        String doctype = docType;
//        if (file.getAbsolutePath().contains("Additional")) {
//            additional = true;
//            if (docType != null && docType.trim().length() <= 0) {
//                if (record.getSettingsAndPermissions().isHasTradeIn()) {
//                    doctype = getString(R.string.tradeInAdditionalImage);
//                } else {
//                    doctype = getString(R.string.additionalImage);
//                }
//            }
//        }
//        String imageFor = "1";
//        /*Write logic for ImageFor */
//        //TODO
//
//        double size = (file.length() / (1024 * 1024));
//        String imageDateTime = null;
//        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//        /*yyyy-MM-dd HH:mm:ss*/
//        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
//        try {
//            if (doctype.equalsIgnoreCase("DRIVERLICENSEIMG")) {
//                imageFor = "1";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp()));
//            } else if (doctype.equalsIgnoreCase("Customer_Insurance")) {
//                imageFor = "1";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getInsurenceTime()));
//            } else if (doctype.equalsIgnoreCase("FRONTIMG")) {
//                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarFrontTime()));
//            } else if (doctype.equalsIgnoreCase("RIGHTIMG")) {
//                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarRightTime()));
//            } else if (doctype.equalsIgnoreCase("REARIMG")) {
//                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarRearTime()));
//            } else if (doctype.equalsIgnoreCase("LEFTIMG")) {
//                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarLeftTime()));
//            } else if (doctype.equalsIgnoreCase("MILEAGEIMG")) {
//                imageFor = "2";
////                if (record.getVehicleDetails().getCarOdoMeterTime() != null) {
////                    imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarOdoMeterTime()));
////                } else {
////                    imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarLeftTime()));
////                }
//            } else if (doctype.equalsIgnoreCase("WINDOWWSTICKERIMG")) {
//                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
//            } else if (doctype.equalsIgnoreCase("TRADEINMILEAGE")) {
//                imageFor = "4";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
//            } else if (doctype.equalsIgnoreCase("LICENCEPLATENUMBER")) {
//                imageFor = "4";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
//            }
//            /*Here go additional*/
//            else if (file.getAbsoluteFile().toString().contains("Additional")) {
//                imageFor = "2";
//            } else if (file.getAbsoluteFile().toString().contains("TradeInAdditional")) {
//                imageFor = "4";
//            }
////            else if (doctype.equalsIgnoreCase("Additional1")) {
////                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional1()));
////            } else if (doctype.equalsIgnoreCase("Additional2")) {
////                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional2()));
////            } else if (doctype.equalsIgnoreCase("Additional3")) {
////                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional3()));
////            } else if (doctype.equalsIgnoreCase("Additional4")) {
////                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional4()));
////            } else if (doctype.equalsIgnoreCase("Additional5")) {
////                imageFor = "2";
////                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional5()));
////            }
////
//            else {
//                if (docType.contains("co_buyer")) {
//                    imageFor = "3";
//                }
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp()));
//            }
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        File filenew = null;
//        Log.e("File size prev : ", file.length() / (1024 * 1024) + " M.B");
//        if (size >= 1) {
//            try {
//                filenew = new Compressor(DSAPP.getInstance()).compressToFile(file);
//                Log.e("File size new : ", filenew.length() / (1024 * 1024) + " M.B");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//        MultipartBody.Part body;
//        RequestBody reqFile;
//        if (filenew != null) {
//            reqFile = RequestBody.create(MediaType.parse("image/*"), filenew);
//            body = MultipartBody.Part.createFormData("upload", filenew.getName(), reqFile);
//        } else {
//            reqFile = RequestBody.create(MediaType.parse("image/*"), file);
//            body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
//        }
//        try {
//            imageDateTime = tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().syncImagesToServer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, doctype, imageDateTime, body, imageFor, additional);
//        final String finalDoctype = doctype;
//        imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
//            @Override
//            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
//                if (response.isSuccessful() && response.body().isSucceeded()) {
//                    Log.e("Image upload type :", "onResponse: " + finalDoctype + " " + response.body().getValue());
//
////                    if (file.exists()) {
////                        if (file.delete()) {
////                            if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
//                    uploadFile(record);
////                        }
////                    }
//                } else {
//                    try {
////                        response.errorBody().toString()
//                        JSONObject json = new JSONObject(response.errorBody().string());
//                        Log.e("Error response ", "json : " + json.toString() + " Image Name: " + finalDoctype);
//                        if (json.toString().contains("Already uploaded")) {
//                            if (file.exists()) {
//                                if (file.delete()) {
//                                    if (record.getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
//                                        uploadFile(record);
//                                }
//                            }
//                        }
//                    } catch (IOException | JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
//                Log.e("My Service img :", "onFailure: " + t.getLocalizedMessage());
//                if (t.getLocalizedMessage().contains("timeout")) {
//                    handleStart();
//                }
//            }
//        });
//    }


    private static String copyFileAndGetPath(Context context, Uri realUri, String id) {
        final String selection = "_id=?";
        final String[] selectionArgs = new String[]{id};
        String path = null;
        Cursor cursor = null;
        try {
            final String[] projection = {"_display_name"};
            cursor = context.getContentResolver().query(realUri, projection, selection, selectionArgs,
                    null);
            cursor.moveToFirst();
            final String fileName = cursor.getString(cursor.getColumnIndexOrThrow("_display_name"));
            File file = new File(context.getCacheDir(), fileName);

//            FileUtils.saveAnswerFileFromUri(realUri, file, context);
            path = file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return path;
    }

    private void startImageSyncForReccursion(final int i, final ArrayList<ImagesSynced> arrayList, final File file, final String id, final String docType, final Record record) {
//        System.gc();
        boolean additional = false;
        String doctype = docType;
//        copyFileAndGetPath(DSAPP.getContext())
//       Uri uri= Uri.fromFile(file);

        if (file.getAbsolutePath().contains("Additional")|| file.getAbsolutePath().contains("additional")) {
            additional = true;
            if (docType.trim().length() <= 0) {
                if (record.getSettingsAndPermissions().isHasTradeIn()) {
                    doctype = getString(R.string.tradeInAdditionalImage);
                } else {
                    doctype = getString(R.string.additionalImage);
                }
            }
        }
        String imageFor = "1";
        /*Write logic for ImageFor */
        //TODO

        double size = file.length() / (1024);
        String imageDateTime = null;
        SimpleDateFormat tradeTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        /*yyyy-MM-dd HH:mm:ss*/
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
        try {
            if (doctype.equalsIgnoreCase("DRIVERLICENSEIMG")) {
                imageFor = "1";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp()));
            } else if (doctype.equalsIgnoreCase("Customer_Insurance")) {
                imageFor = "1";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getInsurenceTime()));
            } else if (doctype.equalsIgnoreCase("FRONTIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarFrontTime()));
            } else if (doctype.equalsIgnoreCase("RIGHTIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarRightTime()));
            } else if (doctype.equalsIgnoreCase("REARIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarRearTime()));
            } else if (doctype.equalsIgnoreCase("LEFTIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarLeftTime()));
            } else if (doctype.equalsIgnoreCase("MILEAGEIMG")) {
                imageFor = "2";
//                if (record.getVehicleDetails().getCarOdoMeterTime() != null) {
//                    imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarOdoMeterTime()));
//                } else {
//                    imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarLeftTime()));
//                }
            } else if (doctype.equalsIgnoreCase("WINDOWWSTICKERIMG")) {
                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formattonFailureer.parse(record.getVehicleDetails().getCarStickerTime()));
            } else if (doctype.equalsIgnoreCase("TRADEINMILEAGE")) {
                imageFor = "4";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
            } else if (doctype.equalsIgnoreCase("LICENCEPLATENUMBER")) {
                imageFor = "4";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
            } else if (doctype.equalsIgnoreCase("TradeInFront")) {
                imageFor = "4";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getCarStickerTime()));
            }
            /*Here go additional*/
            else if (file.getAbsolutePath().contains("additional") || doctype.contains("additional")) {
                imageFor = "2";
            } else if (file.getAbsolutePath().contains("TradeInAdditional") || doctype.contains("TradeInAdditional")) {
                imageFor = "4";
            }
//            else if (doctype.equalsIgnoreCase("Additional1")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional1()));
//            } else if (doctype.equalsIgnoreCase("Additional2")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional2()));
//            } else if (doctype.equalsIgnoreCase("Additional3")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional3()));
//            } else if (doctype.equalsIgnoreCase("Additional4")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional4()));
//            } else if (doctype.equalsIgnoreCase("Additional5")) {
//                imageFor = "2";
//                imageDateTime = tradeTimeFormatter.format(formatter.parse(record.getVehicleDetails().getAdditional5()));
//            }
//
            else {
                //assert docType != null;
                if (docType.contains("co_buyer") || docType.startsWith("co_") || docType.startsWith("co_buyer")) {
                    imageFor = "3";
                }
                imageDateTime = tradeTimeFormatter.format(Objects.requireNonNull(formatter.parse(record.getSettingsAndPermissions().getLatestTimeStamp())));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        File filenew = null;
        Timber.e(file.length() / (1024 * 1024) + " M.B");
        if (size >= 400) {
            try {
                filenew = new Compressor(DSAPP.getInstance()).compressToFile(file);
//                if (filenew.length() / (1024) > 300&&filenew.length() / (1024) !< 256) {
//                    File compFile = new Compressor(DSAPP.getInstance()).compressToFile(filenew);
//                    Log.e("Pic size new : ", compFile.length() / (1024) + " K.B");
//                    filenew = compFile;
//                }
//                Log.e("File size new : ", filenew.length() / (1024) + " K.B");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        MultipartBody.Part body;
        RequestBody reqFile;
        if (filenew != null) {
            reqFile = RequestBody.create(MediaType.parse("image/*"), filenew);
            body = MultipartBody.Part.createFormData("upload", filenew.getName(), reqFile);
        } else {
            reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("upload", file.getName(), reqFile);
        }
        String utcForImg = null;
        try {
            imageDateTime = tradeTimeFormatter.format(formatter.parse(new Date(file.lastModified()).toString()));
            long timeForUTC = new Date(file.lastModified()).getTime();
            utcForImg = tradeTimeFormatter.format(new Date(TimeZoneUtils.toUTC(timeForUTC, TimeZone.getDefault())));
            Log.e("UTC time for img: ", utcForImg);
            Log.e("Normal time for img: ", imageDateTime);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        Log.i("Image trying :", " to upload " + doctype);
        Call<ImageUploadResponse> imageUploadResponseCall = RetrofitInitialization.getDs_services().syncImagesToServer("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id, doctype, imageDateTime, body, imageFor, additional, utcForImg);
        final String finalDoctype = doctype;
        String finalImageFor = imageFor;
        imageUploadResponseCall.enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(@NonNull Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
//                assert response.body() != null;
                if (response.isSuccessful() && response.body().isSucceeded()) {
                    //assert response.body() != null;
                    Log.e("Image upload type :", "onResponse: " + finalDoctype + " " + response.body().getValue() + " Image for " + finalImageFor);

                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData != null && recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().get(i).setSynced(true);
                        String recordDataString1 = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                        RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getLocalLogs());
                        uploadImage(i - 1, recordData1.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData1.where(record.getCustomerDetails().getDLNumber()));
                        if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                            writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), " response " + response.body().getMessage() + " Image Name " + finalDoctype + "\n");
                    }
                } else {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (record != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                        recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                    }
                    String recordDataStringL = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataStringL);
                    try {
                        if (response.code() == 401 || response.code() == 403) {
                            if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                callRefreshTokenAPI(record);
                            } else {
                                Intent intent = new Intent();
                                //assert response.body() != null;
                                intent.putExtra("value", "logOut");
                                intent.setAction("notifyData");
                                sendBroadcast(intent);
                            }
                        }
                        JSONObject json = new JSONObject(response.errorBody().string());
                        if (response.code() == 401 || response.code() == 403) {
                            if (json.getString("Message").contains("INACTIVE")) {
                                String[] arr = json.getString("Message").split("\\|");
//                                    new CustomToast(getApplicationContext()).toast(arr[1]);
                                Intent intent = new Intent();
                                //assert response.body() != null;
                                intent.putExtra("value", "logOut");
                                intent.setAction("notifyData");
                                sendBroadcast(intent);
                            }

                        }
                        Timber.tag("Error response ").e("json : " + json.toString() + " Image Name: " + finalDoctype);
                        /*Push logs here every upload*/
                        if (json.toString().contains("Already uploaded")) {
                            assert record != null;
                            if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().get(i).setSynced(true);
                                RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                uploadImage(i - 1, recordData1.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData1.where(record.getCustomerDetails().getDLNumber()));
                                if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                                    writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), " Error response " + json.toString() + " Image Name " + finalDoctype + "\n");
                            }


                        }
                        if (json.toString().equalsIgnoreCase("No Co Buyer found for Customer Vehicle trade Id")) {
                            if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().get(i).setSynced(true);
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                uploadImage(i - 1, recordData1.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData1.where(record.getCustomerDetails().getDLNumber()));
                            }
                        }
                        if (json.toString().equalsIgnoreCase("No Trade in vehicle for Customer Vehicle trade Id")) {
                            if (recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                                recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList().get(i).setSynced(true);
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                                uploadImage(i - 1, recordData1.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getImagesSyncedArrayList(), recordData1.where(record.getCustomerDetails().getDLNumber()));
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        /*Push logs here every upload*/
//                        Gson gson = new GsonBuilder().create();
//                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId() != null)
                            writeCustomLogsAPI(recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().getCustomerVehicleTradeId(), "Exception Error response " + e.getLocalizedMessage() + " Image Name " + finalDoctype + "\n");
                    }
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ImageUploadResponse> call, @NonNull Throwable t) {
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData != null && record.getCustomerDetails() != null && record.getCustomerDetails().getDLNumber() != null && recordData.where(record.getCustomerDetails().getDLNumber()) != null && recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions() != null) {
                    recordData.where(record.getCustomerDetails().getDLNumber()).getSettingsAndPermissions().setSyncingInProgress(false);
                    String recordDataString1 = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                }
                Log.e("My Service img :", "onFailure: " + t.getLocalizedMessage());
                if (t.getLocalizedMessage().contains("timeout")) {
                    if (timer != null) {
                        timer.cancel();
                    }
                    start5MinFunction();
                }
            }
        });
    }

    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
        String path_licence, path_signature, path_car, path_user_pic, path_screenshots;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
        path_user_pic = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/UserPic";
        path_screenshots = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Screenshot";


        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        }

        /*Car Images*/
        if (new File(path_car + "/IMG_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();

            synced.setName("FRONTIMG");
            synced.setFile(new File(path_car + "/IMG_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        }
        if (new File(path_car + "/IMG_right.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("RIGHTIMG");
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_right.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        }
        if (new File(path_car + "/IMG_rear.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("REARIMG");
            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        }
        if (new File(path_car + "/IMG_left.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LEFTIMG");
            synced.setFile(new File(path_car + "/IMG_left.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        }
        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("WINDOWWSTICKERIMG");
            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);

//            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        }
        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("MILEAGEIMG");
            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TRADEINMILEAGE");
            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        }
        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LICENCEPLATENUMBER");
            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/

        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("Customer_Insurance");
            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
            synced.setName("language_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_CarFax_Report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_CarFax_Report.svg"));
            synced.setName("third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_CarFax_Report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_CarFax_Report.svg"));
            synced.setName("co_buyer_third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_AutoCheck_Reports.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_AutoCheck_Reports.svg"));
            synced.setName("third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_AutoCheck_Reports.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_AutoCheck_Reports.svg"));
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
            synced.setName("type_of_purchase_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
            synced.setName("confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
            synced.setName("test_drive_taken_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
            synced.setName("no_test_drive_confirm_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
            synced.setName("remove_stickers_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
            synced.setName("CONTACTINFOSIGNATUREIMG");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        }
        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
            synced.setName("prints_recieved_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure");
            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
            synced.setName("history_report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        }
        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure");
            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_language_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_type_of_purchase_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
            synced.setName("co_buyer_confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_test_drive_taken_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_no_test_drive_confirm_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_remove_stickers_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_contact_info");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_prints_recieved_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
            synced.setName("co_buyer_condition_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {

            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_history_report");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
            synced.setName("co_buyer_history_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_no_verble_promise.svg"));
            synced.setName("buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg"));
            synced.setName("co_buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_checklist.svg"));
            synced.setName("co_buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_checklist.svg"));
            synced.setName("buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_DLFRONT");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("DRIVERLICENSEIMG");
            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TradeInFront");
            synced.setFile(new File(path_car + "/IMG_trade_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }

        /*Buyers screenshots*/
        if (new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_co_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_safety_recall.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_safety_recall");
            synced.setFile(new File(path_screenshots + "/IMG_alert_safety_recall.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_warning_disclosures");
            synced.setFile(new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_vehicle_info_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_co_buyer_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        if (new File(path_screenshots + "/IMG_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers screenshots*/

        if (new File(path_screenshots + "/IMG_co_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_co_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        /*Buyers pictures*/
        if (new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers picture*/

        if (new File(path_user_pic + "/IMG_co_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        return imagesSynceds;
    }

    public void saveLogsInToDB(String logsData, String dmvNumber) {
        Gson gson = new GsonBuilder().create();
        final RecordData recordData1 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData1.where(dmvNumber).getSettingsAndPermissions() != null) {
            String log = recordData1.where(dmvNumber).getSettingsAndPermissions().getLocalLogs();
            recordData1.where(dmvNumber).getSettingsAndPermissions().setLocalLogs(log + logsData);
        }
        String recordDataString1 = gson.toJson(recordData1);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
    }

    public void generateNoteOnSD(Context context, String sBody) {
        try {
            File root = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM), "/DSXT/DeviceID");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "ErrorLog.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
//            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}