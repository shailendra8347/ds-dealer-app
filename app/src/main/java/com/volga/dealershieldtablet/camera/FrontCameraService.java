package com.volga.dealershieldtablet.camera;

import android.app.Service;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.IBinder;
import androidx.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FrontCameraService extends Service {

    File file;
    Intent intents;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        this.intents = intent;
        file = new File(intent.getStringExtra("fileName"));
        CapturePhoto();
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    private void CapturePhoto() {

        Log.d("kkkk", "Preparing to take photo");
        Camera camera = null;

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        int frontCamera = 1;
        //int backCamera=0;

        Camera.getCameraInfo(frontCamera, cameraInfo);

        try {
            camera = Camera.open(frontCamera);
        } catch (RuntimeException e) {
            Log.d("kkkk", "Camera not available: " + 1);
            camera = null;
            //e.printStackTrace();
        }
        try {
            if (null == camera) {
                Log.d("kkkk", "Could not get camera instance");
            } else {
                Log.d("kkkk", "Got the camera, creating the dummy surface texture");
                try {
                    camera.setPreviewTexture(new SurfaceTexture(0));
                    camera.startPreview();
                } catch (Exception e) {
                    Log.d("kkkk", "Could not set the surface preview texture");
                    e.printStackTrace();
                }
                camera.takePicture(null, null, new Camera.PictureCallback() {

                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        File pictureFileDir = new File("/sdcard/CaptureByService");

                        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
                            pictureFileDir.mkdirs();
                        }
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
                        String date = dateFormat.format(new Date());
                        String photoFile = "ServiceClickedPic_" + "_" + date + ".jpg";
                        String filename = pictureFileDir.getPath() + File.separator + photoFile;
                        File mainPicture = new File(filename);

                        try {
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(data);
                            fos.close();
                            Log.d("kkkk", "image saved");
                        } catch (Exception error) {
                            Log.d("kkkk", "Image could not be saved");
                        }
                        camera.release();
                        Intent intent = new Intent();
                        intent.putExtra("photoCaptured", true);
                        intent.setAction("PhotoTaken");
                        sendBroadcast(intent);

                    }
                });
            }
        } catch (Exception e) {
            camera.release();
            stopService(intents);
        }
    }
}