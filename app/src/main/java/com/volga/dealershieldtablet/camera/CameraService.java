package com.volga.dealershieldtablet.camera;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import timber.log.Timber;

import static com.volga.dealershieldtablet.app.DSAPP.getContext;

public class CameraService extends Service {
    static int x = 0;
    //Camera variables
    //a surface holder
    private SurfaceHolder sHolder;
    private File file;
    //a variable to control the camera
    private Camera mCamera;
    //the camera parameters
    private android.hardware.Camera.Parameters parameters;
    private boolean safeToCapture;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("onCreate: ", "From service:");
    }

    private Camera getAvailableFrontCamera() {
        Log.e("getAvailableCamera: ", "From service:");
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    Timber.tag("CAMERA").e("Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        return cam;
    }


    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
        try {
            file = new File(intent.getStringExtra("fileName"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("onStart: ", "From service:");
        mCamera = getAvailableFrontCamera();     // globally declared instance of camera
        if (mCamera == null) {
            mCamera = Camera.open();    //Take rear facing camera only if no front camera available
        }else {
            Intent intent1 = new Intent();
            intent1.putExtra("photoCaptured", true);
            intent1.setAction("PhotoTaken");
            sendBroadcast(intent1);
        }
        SurfaceView sv = new SurfaceView(getApplicationContext());
        SurfaceTexture surfaceTexture = new SurfaceTexture(10);

        try {
            mCamera.setPreviewTexture(surfaceTexture);
            //mCamera.setPreviewDisplay(sv.getHolder());
            parameters = mCamera.getParameters();
            //set camera parameters
            mCamera.setParameters(parameters);
            mCamera.enableShutterSound(false);
            //This boolean is used as app crashes while writing images to file if simultaneous calls are made to takePicture
//            if (safeToCapture) {
            mCamera.startPreview();
            mCamera.takePicture(null, null, mCall);
//            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //Get a surface
        sHolder = sv.getHolder();
        //tells Android that this surface will have its data constantly replaced
        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


    }

    Camera.PictureCallback mCall = new Camera.PictureCallback() {

        public void onPictureTaken(final byte[] data, Camera camera) {
            //Set a boolean to true again after saving file.
            SpyCamAsync spyCamAsync = new SpyCamAsync(data);
            spyCamAsync.execute();
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private class SpyCamAsync extends AsyncTask<String, String, String> {
        byte[] data;

        public SpyCamAsync(byte[] data) {
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("onPreExecute: ", "From service:");
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.e("onPreExecute: ", "From service:");
            safeToCapture = false;
            //decode the data obtained by the camera into a Bitmap
            final FileOutputStream[] outStream = {null};
            //                // create a File object for the parent directory
//                File myDirectory = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)+"/Test");
//                // have the object build the directory structure, if needed.
//                myDirectory.mkdirs();
//
//                //SDF for getting current time for unique image name
//                SimpleDateFormat curTimeFormat = new SimpleDateFormat("ddMMyyyyhhmmss");
//                String curTime = curTimeFormat.format(new java.util.Date());

            // create a File object for the output file

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
            try {
                outStream[0] = new FileOutputStream(file);
                outStream[0].write(data);
                outStream[0].close();
                if (mCamera != null) {
                    mCamera.stopPreview();
                    mCamera.setPreviewCallback(null);

                    mCamera.release();
                    mCamera = null;
                }
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), Uri.fromFile(file));
                Matrix matrix = new Matrix();
                matrix.postRotate(270);
                Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                //learn content provider for more info
                OutputStream os = getContext().getContentResolver().openOutputStream(Uri.fromFile(file));
                bmp.compress(Bitmap.CompressFormat.PNG, 100, os);

                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

//                }
//            }, 100);


            Timber.d("picture clicked - " + file + " length: " + file.length());

            safeToCapture = true;
            Timber.e("From service: ON BG " + file.length()+" file name: "+file.getName());
            return file.getAbsolutePath();
        }

        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null && bitmap.length() > 0) {
                File file = new File(bitmap);
                Timber.e("From service: ON POST " + file.length() + " file Name: " + file.getName());
                Intent intent = new Intent();
                intent.putExtra("photoCaptured", true);
                intent.setAction("PhotoTaken");
                sendBroadcast(intent);
            }
//            stopSelf();
        }
    }
}

