package com.volga.dealershieldtablet.app;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.provider.Settings;

import com.pushlink.android.AnnoyingPopUpStrategy;
import com.pushlink.android.PushLink;
import com.pushlink.android.StrategyEnum;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.TypefaceUtil;
import com.yariksoffice.lingver.Lingver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import timber.log.Timber;


/**
 * Created by Abhishek Thanvi on 11/03/18.
 * Copyright © 2018 Abhishek Thanvi. All rights reserved.
 */


public class DSAPP extends Application {

    public static DSAPP instance;


    public static DSAPP getInstance() {
        return instance;
    }

    private final String TAG = "DXT_Tab";

    // for the sake of simplicity. use DI in real apps instead
//    public static LocaleManager localeManager;

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert am != null;
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        isInBackground = false;
                    }
                }
            }
        }
        return isInBackground;
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        localeManager = new LocaleManager(base);
//        super.attachBaseContext(localeManager.setLocale(base));
//        Log.d(TAG, "attachBaseContext");
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        localeManager.setLocale(this);
//        Log.d(TAG, "onConfigurationChanged: " + newConfig.locale.getLanguage());
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        Lingver.init(this, "en");
//        if(android_get_device_api_level()>=30)
//            android_fdsan_set_error_level(ANDROID_FDSAN_ERROR_LEVEL_WARN_ONCE);
//        Fabric.with(this, new Crashlytics());
//        initPushLink();
        instance = this;
        Timber.plant(new Timber.DebugTree());
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/roboto_medium.ttf");
    }

    private void initPushLink() {
        @SuppressLint("HardwareIds") String yourDeviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        PushLink.start(this, R.drawable.safeai, AppConstant.PUSHLINK_KEY, yourDeviceID);
        PushLink.setCurrentStrategy(StrategyEnum.ANNOYING_POPUP);
        PushLink.enableExceptionNotification();
        ((AnnoyingPopUpStrategy) PushLink.getCurrentStrategy()).setPopUpMessage("Update must occur before you enter deal!");
        ((AnnoyingPopUpStrategy) PushLink.getCurrentStrategy()).setUpdateButton("Update Now!");
//        PushLink.sendAsyncException();
        Timber.e(yourDeviceID);
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

//    public boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager
//                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        assert connectivityManager != null;
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }

    public boolean isNetworkAvailable() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if (networkConnectivity()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL(
                        "http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(3000);
                urlc.setReadTimeout(4000);
                urlc.connect();
                // networkcode2 = urlc.getResponseCode();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                return (false);
            }
        } else
            return false;

    }

    private boolean networkConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}