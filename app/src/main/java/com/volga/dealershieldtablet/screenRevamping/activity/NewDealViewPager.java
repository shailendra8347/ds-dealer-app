package com.volga.dealershieldtablet.screenRevamping.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pushlink.android.PushLink;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Login.LoginData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.CustomerDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.NetworkChangeReceiver;
import com.volga.dealershieldtablet.screenRevamping.adapter.NewDealViewPagerAdapter;
import com.volga.dealershieldtablet.screenRevamping.pojo.CheckListPojo;
import com.volga.dealershieldtablet.screenRevamping.pojo.LocalSalesPerson;
import com.volga.dealershieldtablet.screenRevamping.pojo.NoVerbalPromise;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.ui.activity.MainActivity;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.SwipeDirection;
import com.yariksoffice.lingver.Lingver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class NewDealViewPager extends AppCompatActivity implements IGoToNextPage {

    public static int currentPage = 0;
    public static int restrictedPage = -1;
    boolean doubleTap = false;
    BroadcastReceiver broadCastNewMessage = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("Data update: ", "Data updated from service");
            if (intent.getStringExtra("value").trim().equalsIgnoreCase("logOut")) {
                PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                Locale myLocale = new Locale("en");
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);
                callHomeActivity();
            } else if (intent.getStringExtra("value").equalsIgnoreCase("refreshIncompleteDeal")) {
                Log.i("Notified in: ", "New deal view pager");
            } else {
                doubleTap = true;
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleTap = false;
                    }
                }, 5000);
                new CustomToast(NewDealViewPager.this).alert(intent.getStringExtra("value"));
            }

        }
    };
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private SingleDirectionViewPager mViewPager;
    private boolean canGoBack = true;
    private NetworkChangeReceiver receiver;
    private boolean called = false;
    private boolean isUsedCarOnly = false;
    private boolean goingBack = false;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        NewDealViewPager.currentPage = currentPage;
        if (currentPage > 0) {
//                    restrictedPage = getIntent().getIntExtra("page", 0);
            restrictedPage = AppConstant.restrictedPage;
            if (restrictedPage > 53 && restrictedPage < 59) {
                restrictedPage = 54;
            }
        }
        if (NewDealViewPager.currentPage < restrictedPage) {
            new CustomToast(this).alert("You can not edit prepped deal. Please continue.");
        } else {
            if (NewDealViewPager.currentPage > 1) {
                saveDataOnback();
            }
            mViewPager.setCurrentItem(NewDealViewPager.currentPage,false);
            setOrientation();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pager);
//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,
//                MainActivity.class));
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
        mViewPager = findViewById(R.id.viewPager);
        if (!called) {
            called = true;
            getDealershipsSilently();
            getSalesPersonList();
            final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
//           getConditionDisclosure();
//           getThirdParty(id);
//           getConditionDisclosureNew(id);
//           getHistoryDisclosure();
            getCheckListItems(id);
            getVerbalPromiseList(id);
        }
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
            AppConstant.NEXT_DELAY = 1600;
        } else {
            AppConstant.NEXT_DELAY = 50;
        }
        registerReceiver(this.broadCastNewMessage, new IntentFilter("notifyData"));
        mViewPager.setAllowedSwipeDirection(SwipeDirection.none);
        NewDealViewPagerAdapter mSectionsPagerAdapter = new NewDealViewPagerAdapter(getSupportFragmentManager(), mViewPager);
        final int[] lastPosition = new int[1];
        AppConstant.TAKE_PHOTO_OR_NOT = true;
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int oldPos = mViewPager.getCurrentItem();
                Log.i("onPageScrolled", "Page No: " + mViewPager.getCurrentItem());
            }

            @Override
            public void onPageSelected(int position) {
                Log.i("onPageSelected", "Page No: " + position);
                if (lastPosition[0] > position) {
                    System.out.println("Left");
                    AppConstant.TAKE_PHOTO_OR_NOT = false;
                } else if (lastPosition[0] < position) {
                    AppConstant.TAKE_PHOTO_OR_NOT = true;
                }
                lastPosition[0] = position;
                currentPage = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (ViewPager.SCROLL_STATE_IDLE == state) {

                }
                Log.i("onPageSclStateChanged", "Page No: " + mViewPager.getCurrentItem() + " State " + state);
            }
        });
        mViewPager.setAdapter(mSectionsPagerAdapter);
        if (getIntent() != null) {
            currentPage = getIntent().getIntExtra("page", 0);
            Timber.e(currentPage + "");
            isUsedCarOnly = getIntent().getBooleanExtra("usedCarOnly", false);
            canGoBack = getIntent().getBooleanExtra("canGoBack", true);
            restrictedPage = AppConstant.restrictedPage;
//            if (!canGoBack) {
//                if (currentPage > 0) {
//                    restrictedPage = getIntent().getIntExtra("page", 0);
////                    restrictedPage = AppConstant.restrictedPage;
//                    if (restrictedPage > 53 && restrictedPage < 59) {
//                        restrictedPage = 54;
//                    }
//                }
//            } else {
//                restrictedPage = -1;
//            }
            mViewPager.setCurrentItem(currentPage);
        }

    }

    public File getOutputFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void setOrientation() {
        if ((NewDealViewPager.currentPage > 2 && NewDealViewPager.currentPage < 13)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 13 && NewDealViewPager.currentPage < 24)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 29 && NewDealViewPager.currentPage < 33)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 43 && NewDealViewPager.currentPage < 54)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 35 && NewDealViewPager.currentPage < 43)) {
            showFragmentInLandscape();
        } else if ((NewDealViewPager.currentPage > 64 && NewDealViewPager.currentPage < 67)) {
            showFragmentInLandscape();
        } else {
            showFragmentInPortrait();
        }
    }

    private void checkIfDealMadeLead() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            if (NewDealViewPager.currentPage > 2 && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isBussinessLead()) {
                Log.e("Base Fragment: ", "onResume: Called and redirected to path selection.");
//                Locale myLocale = new Locale("en");
//
//                Resources res = getResources();
//                DisplayMetrics dm = res.getDisplayMetrics();
//                Configuration conf = res.getConfiguration();
//                conf.setLocale(myLocale);
//                res.updateConfiguration(conf, dm);
                new androidx.appcompat.app.AlertDialog.Builder(this)
                        .setMessage("As 12 hours completed this deal marked as Lead, the deal will be automatically uploaded and will appear in the \"Lead\" reports in your DealerXT account.")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Lingver.getInstance().setLocale(NewDealViewPager.this, "en");
                                callHomeActivity();
                            }
                        }).show().setCancelable(false);

            }
        } else if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() == null&&NewDealViewPager.currentPage > 2){
            Lingver.getInstance().setLocale(NewDealViewPager.this, "en");
            callHomeActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PushLink.setCurrentActivity(this);
        setCustomData();
        checkIfDealMadeLead();
    }

    private void setCustomData() {
        String tabName = "", pendingDeal, CompletedDeal;
        int pending = 0, complete = 0;
        try {
            tabName = PreferenceManger.getUniqueTabName().getDeviceName() + "";
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            List<Record> list = recordData.getRecords();
            for (int i = 0; i < list.size(); i++) {
                if (!list.get(i).getSettingsAndPermissions().getIsRecordComplete()) {
                    pending++;
                } else {
                    complete++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        PushLink.addMetadata("Brand", Build.BRAND);
        PushLink.addMetadata("Model", Build.MODEL);
        PushLink.addMetadata("OS Version", Build.VERSION.RELEASE);
        PushLink.addMetadata("Logged in user", PreferenceManger.getStringValue(AppConstant.USER_NAME));
        PushLink.addMetadata("Email", PreferenceManger.getStringValue(AppConstant.USER_MAIL));
        PushLink.addMetadata("DealershipName", PreferenceManger.getStringValue(AppConstant.SELECTED_DEALERSHIP_NAME));
        PushLink.addMetadata("Tab Name", tabName);
        PushLink.addMetadata("Pending Deals", pending + "");
        PushLink.addMetadata("Deals to be Uploaded", complete + "");

//        PushLink.addMetadata("Error", "");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadCastNewMessage);
        unregisterReceiver(receiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(DSAPP.getInstance())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }


    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    public void playSound(int selectedLanguage) {
        final MediaPlayer mp;
        final ArrayList<Integer> numbers = new ArrayList();
        if (selectedLanguage == 3) {
            for (int i = 0; i < 9; i++) {
                numbers.add(i + 1);
            }
            Collections.shuffle(numbers);

//        int random = new Random().nextInt(10) + 1;
            int random = numbers.get(0);
            String soundname = "es_" + random;
            int res_sound_id = getResources().getIdentifier(soundname, "raw", getPackageName());
            Uri u = Uri.parse("android.resource://" + getPackageName() + "/" + res_sound_id);
            Log.e("URI: + Random ", u.getPath() + " Random " + random);
            mp = MediaPlayer.create(this, u);
        } else {
            for (int i = 0; i < 7; i++) {
                numbers.add(i + 1);
            }
            Collections.shuffle(numbers);

//        int random = new Random().nextInt(10) + 1;
            int random = numbers.get(0);
            String soundname = "en_" + random;
            int res_sound_id = getResources().getIdentifier(soundname, "raw", getPackageName());
            Uri u = Uri.parse("android.resource://" + getPackageName() + "/" + res_sound_id);
            Log.e("URI: + Random ", u.getPath() + " Random " + random);
            mp = MediaPlayer.create(this, u);
        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.reset();
                mp.release();
                mp = null;
            }
        });
        mp.start();
    }

    @Override
    public void whatNextClick() {
        int millies;
        goingBack = false;
        millies = AppConstant.MID_DELAY;
//        reportsInHandler();.
//        if (currentPage < 67) {
//            millies = AppConstant.MID_DELAY;
//        } else {
//            millies = AppConstant.MID_DELAY;
//        }
        if (currentPage > 67) {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            int languageId = 1;
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
            else {
                languageId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
            }
            playSound(languageId);
        }
        {
            new Handler().postDelayed(() -> doubleTap = false, millies);
            if (!doubleTap) {
                doubleTap = true;
                if (currentPage > 56) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    CustomerDetails customerDetail = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails();
//                    if (customerDetail.getFirstName() == null || customerDetail.getFirstName().length() == 0) {
//                        new CustomToast(NewDealViewPager.this).alert("First name is missing. Current page: " + currentPage);
//                    }
                }
//                if (currentPage == 25) {
//                    Gson gson = new GsonBuilder().create();
//                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
//                        doubleTap = true;
//                        currentPage = 28;
//                        mViewPager.setCurrentItem(currentPage, false);
//                    } else {
//                        doubleTap = true;
//                        currentPage++;
//                        mViewPager.setCurrentItem(currentPage);
//                    }
//                } else
                if (currentPage == 74) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                        doubleTap = true;
                        currentPage = 77;
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {
                        doubleTap = true;
                        currentPage++;
                        mViewPager.setCurrentItem(currentPage);
                    }
                } else if (currentPage == 69) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    boolean isPhotoAvailable;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())
                        isPhotoAvailable = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
                    else
                        isPhotoAvailable = !PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS);
                    if (!isPhotoAvailable) {
                        currentPage = 71;
                    } else {
                        currentPage++;
                    }
//                    if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
//                        currentPage = 71;
//                    } else {
//                        currentPage++;
//                    }
                    mViewPager.setCurrentItem(currentPage, false);
                } else if (currentPage == 82) {
//                    doubleTap = true;
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
                        Lingver.getInstance().setLocale(NewDealViewPager.this, "en");
                        Log.e("OnLanguageSelect Next: ", " Current locale: " + "en");
                    } else if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() == null) {
                        Lingver.getInstance().setLocale(NewDealViewPager.this, "en");
                        Log.e("OnLanguageSelect Next: ", " Current locale: " + "en");
                    }
                    Gson gson2 = new GsonBuilder().create();
                    RecordData recordData2 = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    currentPage = 68;
                    mViewPager.setCurrentItem(currentPage, false);
                } else if (currentPage == 81) {
                    doubleTap = true;
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                        currentPage = 83;
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            currentPage = 83;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal() && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isCoBuyerSigned()) {
                                currentPage = 83;
                                mViewPager.setCurrentItem(currentPage, false);
                            } else {
                                doubleTap = true;
                                currentPage++;
                                mViewPager.setCurrentItem(currentPage);
                            }
                        }
                    }
                } else {
                    doubleTap = true;
                    currentPage++;
                    mViewPager.setCurrentItem(currentPage);
                }
//                saveDataOnback();
            }
        }

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);

    }

    public void showFragmentInLandscape() {
        Activity a = NewDealViewPager.this;
        a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public void showSensorOrientation() {
        Activity a = NewDealViewPager.this;
        a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    public void showFragmentInPortrait() {
        Activity a = NewDealViewPager.this;
        a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void goToBackIndex() {
        int millies;
        goingBack = true;
//        if (isUsedCarOnly && currentPage == 0) {
//            Intent intent1 = new Intent(this, MainActivity.class);
//            startActivity(intent1);
//            finish();
//        }
        if (currentPage > 67) {
            millies = AppConstant.LONG_DELAY;
        } else {
            millies = AppConstant.MID_DELAY;
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleTap = false;
            }
        }, millies);
        if (currentPage <= restrictedPage) {
            new CustomToast(this).alert("You can not edit prepped deal. Please continue.");
        } else {
            if (!doubleTap) {
                doubleTap = true;
                if (currentPage == 0) {
                    Intent intent1 = new Intent(this, MainActivity.class);
                    startActivity(intent1);
                    finish();
                }
                if (currentPage > 0) {
                    saveDataOnback();
                }
                if (currentPage == 24) {
                    doubleTap = true;
                    if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                        currentPage = 2;
                    } else {
                        currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                    }
//                    currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                    mViewPager.setCurrentItem(currentPage, false);
                } else if (currentPage == 54) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                        currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                        if (currentPage >= 45) {
                        } else {
                            currentPage = 43;
                        }
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {

                        currentPage = 33;
                        mViewPager.setCurrentItem(currentPage, false);
                    }

                } else if (currentPage > 4 && currentPage < 14) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    boolean isNormal = false;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                        isNormal = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isNormalFlow();
                    if (!isNormal)
                        decideLanding(currentPage);
                    else {
                        currentPage--;
                        mViewPager.setCurrentItem(currentPage);
                    }
                } else if (currentPage == 63) {
                    doubleTap = true;
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                        currentPage--;
                        mViewPager.setCurrentItem(currentPage);
                    } else {
                        currentPage = 61;
                        mViewPager.setCurrentItem(currentPage);
                    }
                } else if (currentPage == 71) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    boolean isPhotoAvailable;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())
                        isPhotoAvailable = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
                    else
                        isPhotoAvailable = !PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS);
                    if (!isPhotoAvailable) {
                        currentPage = 69;
                    } else {
                        currentPage--;
                    }

//                    if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
//                        currentPage = 69;
//                    } else {
//                        currentPage--;
//                    }
                    mViewPager.setCurrentItem(currentPage, false);
                } else if (currentPage == 67) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    String path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber() + "/CarImages";

                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        }
                        currentPage = 58;
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {
                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                            currentPage = 63;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            if (PreferenceManger.getBooleanValue(AppConstant.SKIP_INSURANCE)) {
                                currentPage = 63;
                                mViewPager.setCurrentItem(currentPage, false);
                            } else
                                currentPage--;
                        }
                        mViewPager.setCurrentItem(currentPage);
                    }
                    //                   if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)||PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)){
//                       currentPage = 63;
//                       mViewPager.setCurrentItem(currentPage, false);
//                   }else {
//                       if (!new File(path_car + "/IMG_insurance.jpg").exists()) {
//                           currentPage = 64;
//                           mViewPager.setCurrentItem(currentPage, false);
//                       } else {
//                           doubleTap = true;
//                           currentPage--;
//                           mViewPager.setCurrentItem(currentPage);
//                       }
//                   }
                } else if (currentPage == 77) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                        doubleTap = true;
                        currentPage = 74;
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {
                        doubleTap = true;
                        currentPage--;
                        mViewPager.setCurrentItem(currentPage);
                    }

                } else if (currentPage == 28) {
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
                        currentPage = 24;
                        mViewPager.setCurrentItem(currentPage, false);
                    } else {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                            doubleTap = true;
                            currentPage = 25;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
                    }


                } else if (currentPage == 68) {
                    doubleTap = true;
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase(null);


                    String path_signature;
//                    Gson gson = new GsonBuilder().create();
//                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    boolean isCoBuyer = recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;

                    File fileScr;
                    if (!isCoBuyer) {
                        fileScr = getOutputFile("Screenshot", "language_screen");
                    } else {
                        fileScr = getOutputFile("Screenshot", "co_language_screen");
                    }
                    Timber.e(fileScr.getName() + " is deleted : " + fileScr.delete());
                    if (!isCoBuyer)
                        svgFile = new File(path_signature + "/IMG_language_selection.svg");
                    else {
                        svgFile = new File(path_signature + "/IMG_co_buyer_language_selection.svg");
                    }
                    File file1;
                    if (!isCoBuyer) {
                        file1 = getOutputFile("UserPic", "language_screen_pic");
                    } else {
                        file1 = getOutputFile("UserPic", "co_language_screen_pic");
                    }
                    Timber.e(file1.getName() + " is deleted : " + file1.delete());
                    Timber.e(svgFile.getName() + " is deleted : " + svgFile.delete());

                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
//                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        }
                        setLocale("en", 67);
//                        } else {
//                            setLocale("en", 58);
//                        }

                    } else {
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() == null) {
                            Lingver.getInstance().setLocale(NewDealViewPager.this, "en");
                            Log.e("OnLanguageSelect Next: ", " Current locale: " + "en");
                        }
//                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
//                            setLocale("en", 58);
//                        } else {
                        currentPage--;
                        mViewPager.setCurrentItem(currentPage);
//                        }
                    }
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                } else if (currentPage == 83) {
                    setLocale("en");
                } else {
                    currentPage--;
                    mViewPager.setCurrentItem(currentPage);
                }

            }

        }
    }

    private void decideLanding(int currentPages) {
        Log.e("Time1 ", "time: " + System.currentTimeMillis());
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        boolean hasRight = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeRight();
        boolean hasLeft = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeLeft();
        boolean hasBack = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeBack();
        boolean hasWindow = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeWindow();
        boolean hasFront = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeFront();

        if (currentPages == 13) {
            if (hasWindow) {
                currentPage--;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasLeft) {
                currentPage = 10;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasBack) {
                currentPage = 8;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasRight) {
                currentPage = 6;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasFront) {
                currentPage = 4;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else {
                currentPage = 2;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            }
        } else if (currentPages == 11) {
            if (hasLeft) {
                currentPage--;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasBack) {
                currentPage = 8;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasRight) {
                currentPage = 6;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasFront) {
                currentPage = 4;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else {
                currentPage = 2;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            }
        } else if (currentPages == 9) {
            if (hasBack) {
                currentPage--;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasRight) {
                currentPage = 6;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasFront) {
                currentPage = 4;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else {
                currentPage = 2;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            }
        } else if (currentPages == 7) {
            if (hasRight) {
                currentPage--;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else if (hasFront) {
                currentPage = 4;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            } else {
                currentPage = 2;
                setCurrentPage(currentPage);
//                mViewPager.setCurrentItem(currentPage, false);
            }
        } else if (currentPages == 5) {
            if (hasFront) {
                currentPage--;
            } else {
                currentPage = 2;
            }
            setCurrentPage(currentPage);
//            mViewPager.setCurrentItem(currentPage, false);
        } else {
            currentPage--;
            setCurrentPage(currentPage);
//            mViewPager.setCurrentItem(currentPage, false);
        }
    }

    private void saveDataOnback() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String recordDataString;
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            String format = "yyyy-MM-dd HH:mm:ss";
            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            long dateInMillis = System.currentTimeMillis();
            sdf.setTimeZone(TimeZone.getTimeZone("gmt"));
            String dateString = sdf.format(new Date(dateInMillis));
            if (currentPage > 2)
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeCompletionUTC(dateString);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(currentPage);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
            recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }
    }

    public void saveLogsInToDB(String logsData) {
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            String log = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getLocalLogs();
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLocalLogs(log + " " + Calendar.getInstance().getTime().toString() + " : PageName: " + getScreenName(currentPage, recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))) + " " + logsData);
        }
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
    }


    private String getScreenName(int lastPageIndex, Record record) {
        String pageName = "Default Page";
        switch (lastPageIndex) {
            case 0:
                pageName = "Salesperson info";
                break;
            case 1:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer Contact info";
                } else {
                    pageName = "Co-Buyer Contact info";
                }
                break;
            case 2:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer DMV Front";
                } else {
                    pageName = "Co-Buyer DMV Front";
                }
//                pageName = "DMV Front";
                break;
            case 3:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer DMV Front Preview";
                } else {
                    pageName = "Co-Buyer DMV Front Preview";
                }
//                pageName = "DMV Front Preview";
                break;
            case 4:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer Scan DMV Back";
                } else {
                    pageName = "Co-Buyer Scan DMV Back";
                }
//                pageName = "Scan DMV Back";
                break;
            case 5:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer License info";
                } else {
                    pageName = "Co-Buyer License info";
                }
//                pageName = "License info";
                break;
            case 6:
                if (!record.getSettingsAndPermissions().isHasCoBuyer()) {
                    pageName = "Buyer License info confirmation";
                } else {
                    pageName = "Co-Buyer License info confirmation";
                }
//                pageName = "License info confirmation";
                break;
            case 7:
                pageName = "Co-Buyer Selection";
                break;
            case 8:
                pageName = "Vehicle Type Used/New";
                break;
            case 9:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle VIN Scanner Screen";
                } else {
                    pageName = "Trade-In Vehicle VIN Scanner Screen";
                }
//                pageName = "Vehicle VIN Scanner Screen";
                break;
            case 10:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle info after Scan";
                } else {
                    pageName = "Trade-In Vehicle info after Scan";
                }
//                pageName = "Vehicle info after Scan";
                break;
            case 11:
                pageName = "Vehicle Front Image";
                break;
            case 12:
                pageName = "Vehicle Front Image Preview";
                break;
            case 13:
                pageName = "Vehicle Passengers Side Image";
                break;
            case 14:
                pageName = "Vehicle Passengers Side Image Preview";
                break;
            case 15:
                pageName = "Vehicle Back Side Image";
                break;
            case 16:
                pageName = "Vehicle Back Side Image Preview";
                break;
            case 17:
                pageName = "Vehicle Drivers Side Image";
                break;
            case 18:
                pageName = "Vehicle Drivers Side Image Preview";
                break;

            case 19:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Window Sticker Image";
                } else {
                    pageName = "Trade-In License Plate Image";
                }
//                pageName = "Vehicle Window Sticker Image";
                break;
            case 20:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Window Sticker Image Preview";
                } else {
                    pageName = "Trade-In License Plate Image Preview";
                }
//                pageName = "Window Sticker Image Preview";
                break;
            case 21:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Odometer Image";
                } else {
                    pageName = "Trade-In Vehicle Odometer Image";
                }
//                pageName = "Odometer Image";
                break;
            case 22:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Odometer Image Preview";
                } else {
                    pageName = "Trade-In Vehicle Odometer Image Preview";
                }
//                pageName = "Odometer Image Preview";
                break;
            case 23:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Mileage entry";
                } else {
                    pageName = "Trade-In Vehicle Mileage entry";
                }
//                pageName = "Mileage entry";
                break;
            case 24:

                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image";
                } else {
                    pageName = "Trade-In Vehicle Add More Image";
                }
//                pageName = "Add More Image";
                break;
            case 25:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 1";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 1";
                }
//                pageName = "Add More Image 1";
                break;
            case 26:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 1 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 1 Preview";
                }
//                pageName = "Add More Image 1 Preview";
                break;
            case 27:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 2";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 2";
                }
//                pageName = "Add More Image 2";
                break;
            case 28:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 2 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 2 Preview";
                }
//                pageName = "Add More Image 2 Preview";
                break;
            case 29:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 3";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 3";
                }
//                pageName = "Add More Image 3";
                break;
            case 30:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 3 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 3 Preview";
                }
//                pageName = "Add More Image 3 Preview";
                break;
            case 31:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 4";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 4";
                }
//                pageName = "Add More Image 4";
                break;
            case 32:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 4 Preview";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 4 Preview";
                }
//                pageName = "Add More Image 4 Preview";
                break;
            case 33:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Vehicle Add More Image 5";
                } else {
                    pageName = "Trade-In Vehicle Add More Image 5";
                }
//                pageName = "Add More Image 5";
                break;
            case 34:
                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
                    pageName = "Add More Image 5 Preview";
                } else {
                    pageName = "Trade-In Add More Image 5 Preview";
                }
//                pageName = "Add More Image 5 Preview";
                break;
            case 35:
//                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
//                    pageName = "Vehicle VIN Scanner Screen";
//                } else {
//                    pageName = "";
//                }
                pageName = "Trade-In Selection";
                break;
            case 36:
//                if (!record.getSettingsAndPermissions().isHasTradeIn()) {
//                    pageName = "Vehicle VIN Scanner Screen";
//                } else {
//                    pageName = "";
//                }
                pageName = "Hand Tablet to Finance Vehicle Disclosure";
                break;
            case 37:
                pageName = "Vehicle Summary";
                break;
            case 38:
                pageName = "Trade-In Vehicle Summary";
                break;
            case 39:
                pageName = "Vehicle Condition Disclosure Selection";
                break;
            case 40:
                pageName = "Vehicle History Disclosure Selection";
                break;
            case 41:
                pageName = "Vehicle Third Party Reports Selection";
                break;
            case 42:
                pageName = "Add Insurance Image";
                break;
            case 43:
                pageName = "Capture Insurance Image";
                break;
            case 44:
                pageName = "Insurance Image Preview";
                break;
            case 45:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Handover Screen for Disclosure Signatures";
                } else {
                    pageName = "Co-Buyer Handover Screen for Disclosure Signatures";
                }
//                pageName = "Customer Handover Screen";
                break;
            case 46:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Language Selection Screen";
                } else {
                    pageName = "Co-Buyer Language Selection Screen";
                }
//                pageName = "Language Selection Screen";
                break;
            case 47:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Purchase/Lease Confirmation";
                } else {
                    pageName = "Co-Buyer Purchase/Lease Confirmation";
                }
//                pageName = "Type of Purchase of the vehicle";
                break;
            case 48:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle Image Confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle Image Confirmation";
                }
//                pageName = "Vehicle Image Confirmation Page";
                break;
            case 49:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Make, model, year & mileage confirmation";
                } else {
                    pageName = "Co-Buyer Make, model, year & mileage confirmation";
                }
//                pageName = "Make, model, year & mileage confirmation";
                break;
            case 50:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Test drive confirmation";
                } else {
                    pageName = "Co-Buyer Test drive confirmation";
                }
//                pageName = "Test drive Screen";
                break;
            case 51:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Buyers Guide / New Car/Addendum Sticker(s) Confirmation";
                } else {
                    pageName = "Co-Buyer Buyers Guide / New Car/Addendum Sticker(s) Confirmation";
                }
//                pageName = "Buyers Guide / New Car/Addendum Sticker(s) Screen";
                break;
            case 52:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle Condition Disclosure confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle Condition Disclosure confirmation";
                }
//                pageName = "Vehicle Condition Disclosure confirmation";
                break;
            case 53:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle History Disclosure confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle History Disclosure confirmation";
                }
//                pageName = "Vehicle History Disclosure confirmation";
                break;
            case 54:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Vehicle Third Party Report confirmation";
                } else {
                    pageName = "Co-Buyer Vehicle Third Party Report confirmation";
                }
//                pageName = "Vehicle Third Party Report confirmation";
                break;
            case 55:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Thank you page after Disclosure Confirmation";
                } else {
                    pageName = "Co-Buyer Thank you page after Disclosure Confirmation";
                }
//                pageName = "Thank you page after Vehicle Disclosure";
                break;
            case 56:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Final Report Screen";
                } else {
                    pageName = "Co-Buyer Final Report Screen";
                }
//                pageName = "Final Report Screen";
                break;
            case 57:
                if (!record.getSettingsAndPermissions().isCoBuyer()) {
                    pageName = "Buyer Print Received Checkbox Selection";
                } else {
                    pageName = "Co-Buyer Print Received Checkbox Selection";
                }
//                pageName = "Print Received Checkbox Selection";
                break;
            case 58:
                pageName = "Co-Buyer Handover Screen";
                break;
            case 59:
                pageName = "Final Thank You Page";
                break;
        }
        return pageName;
    }

    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
        String path_licence, path_signature, path_car, path_user_pic, path_screenshots;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
        path_user_pic = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/UserPic";
        path_screenshots = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Screenshot";


        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        }

        /*Car Images*/
        if (new File(path_car + "/IMG_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();

            synced.setName("FRONTIMG");
            synced.setFile(new File(path_car + "/IMG_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        }
        if (new File(path_car + "/IMG_right.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("RIGHTIMG");
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_right.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        }
        if (new File(path_car + "/IMG_rear.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("REARIMG");
            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        }
        if (new File(path_car + "/IMG_left.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LEFTIMG");
            synced.setFile(new File(path_car + "/IMG_left.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        }
        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("WINDOWWSTICKERIMG");
            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);

//            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        }
        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("MILEAGEIMG");
            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TRADEINMILEAGE");
            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        }
        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LICENCEPLATENUMBER");
            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/

        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("Customer_Insurance");
            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
            synced.setName("language_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_1.svg"));
            synced.setName("third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg"));
            synced.setName("co_buyer_third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_2.svg"));
            synced.setName("third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg"));
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
            synced.setName("type_of_purchase_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
            synced.setName("confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
            synced.setName("test_drive_taken_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
            synced.setName("no_test_drive_confirm_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
            synced.setName("remove_stickers_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
            synced.setName("CONTACTINFOSIGNATUREIMG");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        }
        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
            synced.setName("prints_recieved_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure");
            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
            synced.setName("history_report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        }
        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure");
            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_language_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_type_of_purchase_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
            synced.setName("co_buyer_confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_test_drive_taken_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_no_test_drive_confirm_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_remove_stickers_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_contact_info");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_prints_recieved_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
            synced.setName("co_buyer_condition_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {

            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_history_report");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
            synced.setName("co_buyer_history_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_no_verble_promise.svg"));
            synced.setName("buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg"));
            synced.setName("co_buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_checklist.svg"));
            synced.setName("co_buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_checklist.svg"));
            synced.setName("buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_DLFRONT");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("DRIVERLICENSEIMG");
            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TradeInFront");
            synced.setFile(new File(path_car + "/IMG_trade_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }

        /*Buyers screenshots*/
        if (new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_co_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_safety_recall.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_safety_recall");
            synced.setFile(new File(path_screenshots + "/IMG_alert_safety_recall.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_warning_disclosures");
            synced.setFile(new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_vehicle_info_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_co_buyer_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        if (new File(path_screenshots + "/IMG_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers screenshots*/

        if (new File(path_screenshots + "/IMG_co_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_co_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        /*Buyers pictures*/
        if (new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers picture*/

        if (new File(path_user_pic + "/IMG_co_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        return imagesSynceds;
    }


    @Override
    public void onBackPressed() {
        goingBack = true;
//        if (isUsedCarOnly && currentPage == 0) {
//            Intent intent1 = new Intent(this, MainActivity.class);
//            startActivity(intent1);
//            finish();
//        }
        if (mViewPager.getCurrentItem() == 0) {
            Intent intent1 = new Intent(this, MainActivity.class);
            startActivity(intent1);
            finish();
        } else {
            int millies;
            if (currentPage > 67) {
                millies = AppConstant.LONG_DELAY;
            } else {
                millies = AppConstant.MID_DELAY;
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleTap = false;
                }
            }, millies);
//            {
//                Gson gson = new GsonBuilder().create();
//                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isCoBuyer()) {
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()&&currentPage==54){
//                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
//                            new CustomToast(this).alert("You can not edit prepped deal. Please continue.");
//                        } else {
//                            mViewPager.setCurrentItem(58);
//                        }
//                        return;
//                    }
//                }
//            }
            if (currentPage <= restrictedPage) {
                new CustomToast(this).alert("You can not edit prepped deal. Please continue.");
            } else {
                if (!doubleTap) {
                    doubleTap = true;
                    if (currentPage > 1) {
                        saveDataOnback();
                    }
                    if (currentPage == 0) {
                        Intent intent1 = new Intent(this, MainActivity.class);
                        startActivity(intent1);
                        finish();
                    }
                    if (currentPage == 24) {
                        doubleTap = true;
                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                            currentPage = 2;
                        } else {
                            currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                        }
                        mViewPager.setCurrentItem(currentPage, false);
                    } else if (currentPage > 4 && currentPage < 14) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        boolean isNormal = false;
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                            isNormal = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isNormalFlow();
                        if (!isNormal)
                            decideLanding(currentPage);
                        else {
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
                    } else if (currentPage == 34) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData;
                        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isTradeIn()) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(false);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(false);
                                String recordDataString1 = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
                                currentPage = 33;
                                mViewPager.setCurrentItem(currentPage, false);
                            }
                        }
                    } else if (currentPage == 54) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                                currentPage = PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE);
                                if (currentPage < 45) {
                                    currentPage = 43;
                                }
                            } else {
                                if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN))
                                    currentPage = 33;
                                else {
                                    currentPage = 31;
                                }
                            }
                            setCurrentPage(currentPage);
                        } else {
                            assert recordData != null;
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setDLNumber("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setFirstName("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setMiddleName("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setLastName("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setAddressLineOne("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setAddressLineTwo("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setCity("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setState("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setCountry("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setDateOfBirth("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setDLExpiryDate("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setGender("");
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setZipCode("");

                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasCoBuyer(false);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            } else {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                            }

                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                                currentPage = 59;
                                mViewPager.setCurrentItem(currentPage, false);
                            } else {
                                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
                                    new CustomToast(DSAPP.getInstance()).alert("You can not edit a prepped deal.");
                                } else {
                                    currentPage = 58;
                                    mViewPager.setCurrentItem(currentPage, false);
                                }
                            }
                        }


                    } else if (currentPage == 63) {
                        doubleTap = true;
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                            currentPage--;
                        } else {
                            currentPage = 61;
                        }
                        mViewPager.setCurrentItem(currentPage);
                    } else if (currentPage == 43) {
                        setCurrentPage(41);
                    } else if (currentPage == 33) {
                        Gson gson2 = new GsonBuilder().create();
                        RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        boolean takeOdometer = recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isTakeOdometer();

                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS) || !takeOdometer) {
                            currentPage = 29;
                        } else {
                            currentPage = 31;
                        }
                        setCurrentPage(currentPage);
                    } else if (currentPage == 69) {

                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer())
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase(null);

                        String recordDataString = gson.toJson(recordData);
                        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                        setLocale("en", 68);
                    } else if (currentPage == 67) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        String path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber() + "/CarImages";

                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                                String recordDataString = gson.toJson(recordData);
                                PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            }
                            currentPage = 58;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            if (PreferenceManger.getBooleanValue(AppConstant.SKIP_INSURANCE) || PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                                currentPage = 63;
                                mViewPager.setCurrentItem(currentPage, false);
                            } else {
                                if (!new File(path_car + "/IMG_insurance.jpg").exists()) {
                                    currentPage = 64;
                                    mViewPager.setCurrentItem(currentPage, false);
                                } else {
                                    doubleTap = true;
                                    currentPage--;
                                    mViewPager.setCurrentItem(currentPage);
                                }
                            }
                        }


                    } else if (currentPage == 71) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        boolean isPhotoAvailable;
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal())
                            isPhotoAvailable = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCustomerVehicleTradeDocs().size() != 0;
                        else
                            isPhotoAvailable = !PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS);
                        if (!isPhotoAvailable) {
                            currentPage = 69;
                        } else {
                            currentPage--;
                        }
//                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
//                            currentPage = 69;
//                        } else {
//                            currentPage--;
//                        }
                        mViewPager.setCurrentItem(currentPage, false);
                    } else if (currentPage == 76) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        if (history.get(history.size() - 1).isChecked()) {
                            page = 74;
                        }
                        if (history.get(history.size() - 1).isChecked() && condition.get(condition.size() - 1).isChecked()) {
                            page = 73;
                        }
                        if (page != 0) {
                            doubleTap = true;
                            mViewPager.setCurrentItem(page, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
                    } else if (currentPage == 75) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        if (condition.get(condition.size() - 1).isChecked()) {
                            page = 73;
                        }
                        if (page != 0) {
                            doubleTap = true;
                            mViewPager.setCurrentItem(page, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
                    } else if (currentPage == 68) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        String path_signature;
//                    Gson gson = new GsonBuilder().create();
//                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        boolean isCoBuyer = recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();
                        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                        File svgFile;

                        File fileScr;
                        if (!isCoBuyer) {
                            fileScr = getOutputFile("Screenshot", "language_screen");
                        } else {
                            fileScr = getOutputFile("Screenshot", "co_language_screen");
                        }
                        Timber.e(fileScr.getName() + " is deleted : " + fileScr.delete());
                        if (!isCoBuyer)
                            svgFile = new File(path_signature + "/IMG_language_selection.svg");
                        else {
                            svgFile = new File(path_signature + "/IMG_co_buyer_language_selection.svg");
                        }
                        File file1;
                        if (!isCoBuyer) {
                            file1 = getOutputFile("UserPic", "language_screen_pic");
                        } else {
                            file1 = getOutputFile("UserPic", "co_language_screen_pic");
                        }
                        Timber.e(file1.getName() + " is deleted : " + file1.delete());
                        Timber.e(svgFile.getName() + " is deleted : " + svgFile.delete());
                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTypeOfPurchase(null);
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyerAdded(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            setLocale("en", 67);
                        } else {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null) {
                                openSpecificPage(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            } else {
                                openSpecificPage("English");
                            }
                        }

                    } else if (currentPage == 77) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                        int page = 0;
                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                            if (report != null && report.get(report.size() - 1).isChecked() && condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                                page = 73;
                            } else if (report != null && report.get(report.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                                page = 74;
                            } else if (report != null && report.get(report.size() - 1).isChecked()) {
                                page = 75;
                            }

                        } else {
                            if (condition != null) {
                                if (condition.get(condition.size() - 1).isChecked())
                                    page = 73;
                                else {
                                    page = 74;
                                }
                            }
                        }
                        if (page != 0) {
                            doubleTap = true;
                            mViewPager.setCurrentItem(page, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
//                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
//                        doubleTap = true;
//                        currentPage = 73;
//                        mViewPager.setCurrentItem(currentPage, false);
//                    } else {
//                        doubleTap = true;
//                        currentPage--;
//                        mViewPager.setCurrentItem(currentPage);
//                    }

                    } else if (currentPage == 28) {
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
                            currentPage = 24;
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                                doubleTap = true;
                                currentPage = 25;
                                mViewPager.setCurrentItem(currentPage, false);
                            } else {
                                doubleTap = true;
                                currentPage--;
                                mViewPager.setCurrentItem(currentPage);
                            }
                        }

                    } else if (mViewPager.getCurrentItem() == 60) {
                        doubleTap = true;
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                            doubleTap = true;
                            currentPage = 58;
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            mViewPager.setCurrentItem(currentPage, false);
                        } else {
                            doubleTap = true;
                            currentPage--;
                            mViewPager.setCurrentItem(currentPage);
                        }
                    } else if (currentPage == 68) {
                        doubleTap = true;
                        Gson gson = new GsonBuilder().create();
                        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
//                            setLocale("en", 66);
//                        } else {
//                            currentPage--;
//                            mViewPager.setCurrentItem(currentPage);
//                        }
                        if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null) {
                                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("english")) {
                                    setLocale("en", 67);
                                } else {
                                    currentPage--;
                                    mViewPager.setCurrentItem(currentPage);
                                }
                            } else {
                                Lingver.getInstance().setLocale(NewDealViewPager.this, "en");
                                currentPage--;
                                mViewPager.setCurrentItem(currentPage);
                            }
                        } else {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString = gson.toJson(recordData);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                            openSpecificPage(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
//                    ((NewDealViewPager) DSAPP.getInstance()).setCurrentPage(58);
                        }

                    } else if (currentPage == 83) {
                        setLocale("en");
                    } else if (currentPage == 82) {
                        Gson gson2 = new GsonBuilder().create();
                        RecordData recordData2 = gson2.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                        if (!recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isWebDeal()) {
                            recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(false);
                            String recordDataString2 = gson2.toJson(recordData2);
                            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString2);
                            if (recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage() != null)
                                localeSelection(recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguage());
                            if (recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && !recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getIsRemote()) {
                                currentPage--;
                                mViewPager.setCurrentItem(currentPage);
                            } else {
                                currentPage = 67;
                                mViewPager.setCurrentItem(currentPage, false);
                            }
                        } else {

                            if (recordData2.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).isBuyerSigned()) {
                                currentPage = 58;
                                mViewPager.setCurrentItem(currentPage, false);
                            } else {
                                currentPage--;
                                mViewPager.setCurrentItem(currentPage);
                            }
                        }
                    } else {
                        currentPage--;
                        mViewPager.setCurrentItem(currentPage);
                    }
                }

            }
        }

    }

    private void openSpecificPage(String selectedLanguage) {

        if (selectedLanguage != null) {
            if (selectedLanguage.equalsIgnoreCase("Chinese")) {
                setLocale("zh", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
                setLocale("es", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
                setLocale("vi", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
                setLocale("ko", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
                setLocale("tl", 82);
            } else if (selectedLanguage.equalsIgnoreCase("English")) {
                setLocale("en", 82);
            }
        } else {
            setLocale("en", 82);
        }
    }

    public void setLocale(String localeName, int page) {
//        Locale myLocale = new Locale(localeName);
//        Resources res = getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Configuration conf = res.getConfiguration();
//        conf.locale = myLocale;
//        res.updateConfiguration(conf, dm);
        Lingver.getInstance().setLocale(NewDealViewPager.this, localeName);
        Log.e("OnLanguageSelect Next: ", " Current locale: " + localeName);
        Intent refresh = new Intent(this, NewDealViewPager.class);
        refresh.putExtra("page", page);
        PreferenceManger.putString(AppConstant.CURRENT_LANG, localeName);
        refresh.putExtra("CurrentLang", PreferenceManger.getStringValue(AppConstant.CURRENT_LANG));
        startActivity(refresh);
        finish();
    }

    private void localeSelection(String selectedLanguage) {

        if (selectedLanguage != null) {
            if (selectedLanguage.equalsIgnoreCase("Chinese")) {
                setLocaleLocal("zh", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Spanish")) {
                setLocaleLocal("es", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Vietnamese")) {
                setLocaleLocal("vi", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Korean")) {
                setLocaleLocal("ko", 82);
            } else if (selectedLanguage.equalsIgnoreCase("Tagalog")) {
                setLocaleLocal("tl", 82);
            } else if (selectedLanguage.equalsIgnoreCase("English")) {
                setLocaleLocal("en", 82);
            }
        } else {
            setLocaleLocal("en", 82);
        }
    }

    public void setLocaleLocal(String localeName, int page) {
        Lingver.getInstance().setLocale(this, localeName);
        Log.e("OnLanguageSelect Next: ", " Current locale: " + localeName);

    }

    public void callHomeActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void setLocale(String localeName) {
        Lingver.getInstance().setLocale(NewDealViewPager.this, localeName);
        Log.e("OnLanguageSelect Next: ", " Current locale: " + localeName);
        callHomeActivity();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public void getDealershipsSilently() {
        String time = PreferenceManger.getStringValue(AppConstant.TIME_STAMP);
        int min = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.getDefault());
            Date date = formatter.parse(time);
            long lastTime = date.getTime();
            long currentTime = System.currentTimeMillis();
            long diff = currentTime - lastTime;
            min = (int) ((currentTime - lastTime) / (1000 * 60));
        } catch (ParseException e) {
            Timber.e(e.getLocalizedMessage());
        }
        if (min >= 1 || time.length() == 0) {
            Call<DealershipData> get_dealerships = RetrofitInitialization.getDs_services().get_dealerships("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN));
            get_dealerships.enqueue(new Callback<DealershipData>() {
                @Override
                public void onResponse(Call<DealershipData> call, Response<DealershipData> response) {
                    if (response.code() == 200 && response.isSuccessful()) {
                        DealershipData dealershipData = response.body();
                        Gson gson = new GsonBuilder().create();
                        Timber.tag("Refreshed dealer: ").i(gson.toJson(dealershipData));
                        String dealershipDataString = gson.toJson(response.body());
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, dealershipDataString);
                        if (dealershipData != null) {
                            for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
                                if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                                    PreferenceManger.putString(AppConstant.SELECTED_DEALERSHIP_NAME, dealershipData.getDealerships().get(i).getDealershipName());
                                    PreferenceManger.putBoolean(AppConstant.CHECK_ID, dealershipData.getDealerships().get(i).isAllowCheckID());
                                    PreferenceManger.putBoolean(AppConstant.TAKE_USER_PHOTO, dealershipData.getDealerships().get(i).isCaptureCustomerPhotoOnSign());
                                    PreferenceManger.putBoolean(AppConstant.TAKE_SCREENSHOT, dealershipData.getDealerships().get(i).isCaptureScreenOnSign());
                                    PreferenceManger.putBoolean(AppConstant.VIEW_MANDATORY, dealershipData.getDealerships().get(i).isViewThirdPartyReportMandatory());
                                    PreferenceManger.putBoolean(AppConstant.USE_THIRD_PARTY_API, dealershipData.getDealerships().get(i).isUseThirdPartyAPI());
                                    PreferenceManger.putBoolean(AppConstant.IS_USEDCAR_ONLY, dealershipData.getDealerships().get(i).isUsedCarOnly());
                                    PreferenceManger.putBoolean(AppConstant.PROCESS_TRADE_IN, dealershipData.getDealerships().get(i).isProcessTradeIn());
                                    PreferenceManger.putBoolean(AppConstant.SKIP_INSURANCE, !dealershipData.getDealerships().get(i).isTakePhotoOfProofOfInsurance());
                                    PreferenceManger.putBoolean(AppConstant.SKIP_PHOTOS, !dealershipData.getDealerships().get(i).isTakePhotoOfVehicle());
                                }
                            }
                        }

                    } else {

                        assert response.errorBody() != null;
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response.errorBody().string());
                            if (response.code() == 401 || response.code() == 403) {
                                if (json.getString("Message").contains("INACTIVE")) {
                                    String[] arr = json.getString("Message").split("\\|");
                                    new CustomToast(NewDealViewPager.this).toast(arr[1]);
                                    Log.e("Logout: ", "logout from NewDeal during inactive" + json.toString());
                                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                    PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                    Locale myLocale = new Locale("en");
                                    Resources res = getResources();
                                    DisplayMetrics dm = res.getDisplayMetrics();
                                    Configuration conf = res.getConfiguration();
                                    conf.locale = myLocale;
                                    res.updateConfiguration(conf, dm);
                                    callHomeActivity();
                                } else {
                                    if (PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN).length() > 0) {
                                        callRefreshTokenAPI();
                                    } else {
                                        Log.e("Logout: ", "logout from NewDeal during zero length" + json.toString());
                                        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                                        Locale myLocale = new Locale("en");
                                        Resources res = getResources();
                                        DisplayMetrics dm = res.getDisplayMetrics();
                                        Configuration conf = res.getConfiguration();
                                        conf.locale = myLocale;
                                        res.updateConfiguration(conf, dm);
                                        callHomeActivity();
                                    }
                                }

                            }
//                        showPopup(json.getString("error_description"));
                            Log.e("GetDealership error : ", "onResponse: " + response.errorBody().string());

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<DealershipData> call, Throwable t) {
//                new CustomToast(DSAPP.getInstance()).alert(t.getLocalizedMessage());

                }
            });
        }
    }

    private void callRefreshTokenAPI() {
        RetrofitInitialization.getDs_services().refreshToken(AppConstant.REFRESH_TOKEN, PreferenceManger.getStringValue(AppConstant.REFRESH_TOKEN)).enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    LoginData loginData = response.body();
                    Log.e("Refresh: ", loginData.getAccessToken());
                    PreferenceManger.putString(AppConstant.ACCESS_TOKEN, response.body().getAccessToken());
                    PreferenceManger.putString(AppConstant.REFRESH_TOKEN, response.body().getRefresh_token());
//                    final ProgressDialog progressDialog = new ProgressDialog(DSAPP.getInstance());
//                    progressDialog.setMessage("Refreshing pending deals...");
//                    progressDialog.show();
//                    progressDialog.setCancelable(false);

                } else {
                    try {
                        JSONObject json = new JSONObject(response.errorBody().string());
                        Log.e("Logout: ", "logout from NewDeal during during refresh" + json.toString());
                        PreferenceManger.putString(AppConstant.ACCESS_TOKEN, "");
                        PreferenceManger.putString(AppConstant.DEALERSHIP_DATA, "");
                        Locale myLocale = new Locale("en");
                        Resources res = getResources();
                        DisplayMetrics dm = res.getDisplayMetrics();
                        Configuration conf = res.getConfiguration();
                        conf.locale = myLocale;
                        res.updateConfiguration(conf, dm);
                        callHomeActivity();
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
//                if (t.getMessage().contains("Unable to resolve host") || t.getCause().getMessage().contains("Unable to resolve host")) {
//                if (DSAPP.getInstance() != null && !DSAPP.getInstance().isFinishing())
//                    new CustomToast(DSAPP.getInstance()).alert(getString(R.string.connection_check));
//                }
            }
        });
    }


    private void getSalesPersonList() {
//        final ProgressDialog progressDialog = new ProgressDialog(DSAPP.getInstance());
//        progressDialog.setMessage("Loading " + dealershipName.getDealershipName() + " configuration...");
//        progressDialog.show();
//        progressDialog.setCancelable(true);
        final String id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        RetrofitInitialization.getDs_services().getSalesPerson("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<ArrayList<SalesPersonList>>() {
            @Override
            public void onResponse(Call<ArrayList<SalesPersonList>> call, Response<ArrayList<SalesPersonList>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new GsonBuilder().create();
                    LocalSalesPerson localSalesPerson = new LocalSalesPerson();
                    localSalesPerson.setLocalSales(response.body());
                    String list = gson.toJson(localSalesPerson);
                    Log.i("Sales persons: ", list);
                    PreferenceManger.putString(AppConstant.SALES_PERSON, list);

                }
            }

            @Override
            public void onFailure(Call<ArrayList<SalesPersonList>> call, Throwable t) {

            }
        });
    }

    private void getCheckListItems(String id) {
        RetrofitInitialization.getDs_services().getCheckList("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<CheckListPojo>() {
            @Override
            public void onResponse(Call<CheckListPojo> call, Response<CheckListPojo> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    Log.i("Check list: ", list);
                    PreferenceManger.putString(AppConstant.CHECK_LIST, list);
                } else {

                }
            }

            @Override
            public void onFailure(Call<CheckListPojo> call, Throwable t) {

            }
        });
    }

    private void getVerbalPromiseList(final String id) {
        RetrofitInitialization.getDs_services().getVerbalPromise("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<NoVerbalPromise>() {
            @Override
            public void onResponse(Call<NoVerbalPromise> call, Response<NoVerbalPromise> response) {
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    Log.i("no verbal promise: ", list);
                    PreferenceManger.putString(AppConstant.VERBAL_PROMISE, list);

                }
            }

            @Override
            public void onFailure(Call<NoVerbalPromise> call, Throwable t) {

            }
        });
    }
}
