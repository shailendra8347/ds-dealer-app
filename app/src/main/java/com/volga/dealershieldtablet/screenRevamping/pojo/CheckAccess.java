package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.List;

public class CheckAccess{
	private boolean Succeeded;
	private List<String> errors;
	private boolean isDuplicate;
	private boolean duePayment;
	private boolean Value;
	private String ErrorsString;
	private boolean isUpdated;
	private ErrorDetails errorDetails;

	public void setSucceeded(boolean succeeded){
		this.Succeeded = succeeded;
	}

	public boolean isSucceeded(){
		return Succeeded;
	}

	public void setErrors(List<String> errors){
		this.errors = errors;
	}

	public List<String> getErrors(){
		return errors;
	}

	public void setIsDuplicate(boolean isDuplicate){
		this.isDuplicate = isDuplicate;
	}

	public boolean isIsDuplicate(){
		return isDuplicate;
	}

	public void setDuePayment(boolean duePayment){
		this.duePayment = duePayment;
	}

	public boolean isDuePayment(){
		return duePayment;
	}

	public void setValue(boolean value){
		this.Value = value;
	}

	public boolean isValue(){
		return Value;
	}

	public void setErrorsString(String errorsString){
		this.ErrorsString = errorsString;
	}

	public String getErrorsString(){
		return ErrorsString;
	}

	public void setIsUpdated(boolean isUpdated){
		this.isUpdated = isUpdated;
	}

	public boolean isIsUpdated(){
		return isUpdated;
	}

	public void setErrorDetails(ErrorDetails errorDetails){
		this.errorDetails = errorDetails;
	}

	public ErrorDetails getErrorDetails(){
		return errorDetails;
	}

	@Override
 	public String toString(){
		return 
			"CheckAccess{" + 
			"Succeeded = '" + Succeeded + '\'' +
			",errors = '" + errors + '\'' + 
			",isDuplicate = '" + isDuplicate + '\'' + 
			",duePayment = '" + duePayment + '\'' + 
			",Value = '" + Value + '\'' +
			",ErrorsString = '" + ErrorsString + '\'' +
			",isUpdated = '" + isUpdated + '\'' + 
			",errorDetails = '" + errorDetails + '\'' + 
			"}";
		}
}