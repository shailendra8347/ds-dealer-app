package com.volga.dealershieldtablet.screenRevamping.pojo;

import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.StateProvince;

public class Location
{
    private String StateProvinceId;

    private String State;

    private com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.StateProvince StateProvince;

    private String Long;

    private String Address2;

    private String Zipcode;

    private String Country;

    private String Id;

    private String Address1;

    private String City;

    private String Lat;

    private String LocationName;

    public String getStateProvinceId ()
    {
        return StateProvinceId;
    }

    public void setStateProvinceId (String StateProvinceId)
    {
        this.StateProvinceId = StateProvinceId;
    }

    public String getState ()
    {
        return State;
    }

    public void setState (String State)
    {
        this.State = State;
    }

    public StateProvince getStateProvince ()
    {
        return StateProvince;
    }

    public void setStateProvince (StateProvince StateProvince)
    {
        this.StateProvince = StateProvince;
    }

    public String getLong ()
    {
        return Long;
    }

    public void setLong (String Long)
    {
        this.Long = Long;
    }

    public String getAddress2 ()
    {
        return Address2;
    }

    public void setAddress2 (String Address2)
    {
        this.Address2 = Address2;
    }

    public String getZipcode ()
    {
        return Zipcode;
    }

    public void setZipcode (String Zipcode)
    {
        this.Zipcode = Zipcode;
    }

    public String getCountry ()
    {
        return Country;
    }

    public void setCountry (String Country)
    {
        this.Country = Country;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getAddress1 ()
    {
        return Address1;
    }

    public void setAddress1 (String Address1)
    {
        this.Address1 = Address1;
    }

    public String getCity ()
    {
        return City;
    }

    public void setCity (String City)
    {
        this.City = City;
    }

    public String getLat ()
    {
        return Lat;
    }

    public void setLat (String Lat)
    {
        this.Lat = Lat;
    }

    public String getLocationName ()
    {
        return LocationName;
    }

    public void setLocationName (String LocationName)
    {
        this.LocationName = LocationName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [StateProvinceId = "+StateProvinceId+", State = "+State+", StateProvince = "+StateProvince+", Long = "+Long+", Address2 = "+Address2+", Zipcode = "+Zipcode+", Country = "+Country+", Id = "+Id+", Address1 = "+Address1+", City = "+City+", Lat = "+Lat+", LocationName = "+LocationName+"]";
    }
}
