package com.volga.dealershieldtablet.screenRevamping.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.DealershipData;
import com.volga.dealershieldtablet.Retrofit.Pojo.DecisionMaker;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.VehiclePhotoType;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.adapter.CustomArrayAdapter;
import com.volga.dealershieldtablet.screenRevamping.pojo.LocalSalesPerson;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.ui.activity.PopUpContainerActivity;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class FragmentSalesPersonDropDown extends BaseFragment {

    private View mView;
    private DSTextView next;
    DSEdittext email, firstName, lastName;

    IGoToNextPage iGoToNextPage;
    ArrayList<SalesPersonList> salesPersons = new ArrayList<>();
    private CustomArrayAdapter adapter;
    private DSTextView progress;
    private AppCompatSpinner spinnerOfferType;
    Gson gson = new GsonBuilder().create();
    private boolean takeFront, takeBack, takeRight, takeWindow, takeOdometer, takeLeft, takeAddenmum;
    private DecisionMaker dMaker = new DecisionMaker();
    private ArrayList<VehiclePhotoType> list;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && next != null) {

            showFragmentInPortrait();

//            updateData();
        }
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons() != null) {
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName().length() > 0) {
                firstName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName());
            }
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName().length() > 0) {
                lastName.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getLastName());
            }
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getFirstName().length() > 0) {
                email.setText(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getEmail());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.salesperson_layout, container, false);
        initView();
        return mView;
    }


    private void initView() {
        salesPersons = new ArrayList<>();
        spinnerOfferType = mView.findViewById(R.id.spinnerOfferType);
        progress = mView.findViewById(R.id.progress);
        final DSTextView add = mView.findViewById(R.id.addSalesPerson);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PopUpContainerActivity.class);
                intent.putExtra(AppConstant.FROM_FRAGMENT, "addsalesperson");
                getActivity().startActivity(intent);
            }
        });
        adapter = new CustomArrayAdapter(getActivity(),
                R.layout.spiner_item_selected, salesPersons);
        spinnerOfferType.setAdapter(adapter);
        final DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        text_cancelBack.setVisibility(View.VISIBLE);
        spinnerOfferType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = ((TextView) view.findViewById(R.id.salespesonName)).getText().toString();
                if (pos > 0) {
//                    salesPersons.add(salesPersons.get(pos));
                    add.setVisibility(View.GONE);
                    text_cancelBack.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);
                    SetRecordData(salesPersons.get(pos));
                } else {
                    add.setVisibility(View.VISIBLE);
                    next.setVisibility(View.GONE);
                    text_cancelBack.setVisibility(View.INVISIBLE);
                }
//                salesPersons.setText(item);
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        Gson gson = new GsonBuilder().create();
        DealershipData dealershipData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.DEALERSHIP_DATA), DealershipData.class);
        for (int i = 0; i < dealershipData.getDealerships().size(); i++) {
            if (dealershipData.getDealerships().get(i).getId() == PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID)) {
                list = dealershipData.getDealerships().get(i).getDealershipVehiclePhotoTypes();
            }
        }
//        list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getDealershipDetails().getVehiclePhotoTypes();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getDocumentType().equalsIgnoreCase("FRONTIMG") && list.get(i).getIsActive()) {
                    takeFront = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("REARIMG") && list.get(i).getIsActive()) {
                    takeBack = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("RIGHTIMG") && list.get(i).getIsActive()) {
                    takeRight = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("LEFTIMG") && list.get(i).getIsActive()) {
                    takeLeft = true;
                }
                if (list.get(i).getDocumentType().equalsIgnoreCase("MILEAGEIMG") && list.get(i).getIsActive()) {
                    takeOdometer = true;
                }
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
//                    takeWindow = true;
//                }
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
//                    takeAddenmum = true;
//                }
                if (list.get(i).getVehiclePhotoTypeId()==6 && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
                    takeWindow = true;
                }
                if (list.get(i).getVehiclePhotoTypeId()==7&& list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
                    takeAddenmum = true;
                }
//                if (list.get(i).getDocTypeTitle().equalsIgnoreCase("Buyers Guide") && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
//                    takeWindow = true;
//                }//
//                if (list.get(i).getDocTypeTitle().equalsIgnoreCase("Addendums Sticker") && list.get(i).getDocumentType().equalsIgnoreCase("WINDOWWSTICKERIMG") && list.get(i).getIsActive()) {
//                    takeAddenmum = true;
//                }
            }
            boolean normalFlow = true;
            if (takeFront || takeBack || takeRight || takeLeft || takeOdometer || takeWindow) {
                normalFlow = false;
            }
            boolean onlyWindow = false;
            if (!takeOdometer && !takeBack && !takeRight && !takeLeft && !takeFront) {
                onlyWindow = true;
            }
            if (takeAddenmum && takeWindow) {
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new")) {
                    takeWindow = false;
                } else {
                    takeAddenmum = false;
                }
            }
            //case 5:
            if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && !normalFlow) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
                takeWindow = false;
            } else
                //case 6:
                if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
                    takeWindow = true;
                } else
                    //case 3&4
                    if (onlyWindow && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
                        normalFlow = true;
                    } else
                        //case: 7&8
                        if (takeWindow && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && !normalFlow) {
                            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
                            takeWindow = false;
//                normalFlow = true;
                        } else {
                            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new")) {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(takeAddenmum);
                            } else {
                                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(takeWindow);
                            }
                        }


            //case :
//            if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && normalFlow){
//                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
//            }
            //case 1&2:
            if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("used") && !normalFlow) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(false);
                takeWindow = false;
            } else if (takeAddenmum && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("new") && normalFlow) {
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(true);
                takeWindow = true;
                normalFlow = false;
            }
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeFront(takeFront);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeBack(takeBack);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeLeft(takeLeft);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeOdometer(takeOdometer);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeRight(takeRight);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setTakeWindow(takeWindow);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setNormalFlow(normalFlow);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);

//            if (!normalFlow) {
//                dMaker = MakeDecision();
//            }
        }
        next = mView.findViewById(R.id.text_next);
        next.setVisibility(View.GONE);
        next.setEnabled(true);
        next.setClickable(true);
        TextView back = mView.findViewById(R.id.text_back_up);
        DSTextView title = mView.findViewById(R.id.title);
        title.setText(Html.fromHtml(getString(R.string.salesperson)));

        text_cancelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_cancelBack.setEnabled(false);
                text_cancelBack.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        text_cancelBack.setEnabled(true);
                        text_cancelBack.setClickable(true);
                    }
                }, 6000);
                cancel();
            }
        });
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                KeyboardUtils.hideSoftKeyboard(mView, getActivity());
                return false;
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (firstName.getText().toString().trim().length() > 0 && !firstName.getText().toString().matches(regx)) {
//                    new CustomToast(getActivity()).alert(getString(R.string.valid_first_name));
//                } else if (lastName.getText().toString().trim().length() > 0 && !lastName.getText().toString().matches(regx)) {
//                    new CustomToast(getActivity()).alert(getString(R.string.valid_last_name));
//                } else if (email.getText().toString().trim().length() > 0 && !email.getText().toString().trim().matches(emailPattern)) {
//                    new CustomToast(getActivity()).alert(getString(R.string.valid_email));
//                } else if (email.getText().toString().trim().length() > 0 || firstName.getText().toString().trim().length() > 0 || lastName.getText().toString().trim().length() > 0) {
                next.setEnabled(false);
                next.setClickable(false);
//                if (salesPersons.size() != 0)
//                    SetRecordData(salesPersons.get(0));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        next.setEnabled(true);
                        next.setClickable(true);
                    }
                }, 3000);
                String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
                final Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                boolean takeOdometer = recordData.where(rand_int1).getVehicleDetails().isTakeOdometer();
                boolean isNormal = false;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null)
                    isNormal = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().isNormalFlow();
                if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
//                    ((NewDealViewPager) getActivity()).setCurrentPage(32);
                    if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
                        ((NewDealViewPager) getActivity()).setCurrentPage(33);
                    } else {
                        ((NewDealViewPager) getActivity()).setCurrentPage(54);
                    }
                } else if (!isNormal && !takeOdometer) {
                    if (PreferenceManger.getBooleanValue(AppConstant.PROCESS_TRADE_IN)) {
                        ((NewDealViewPager) getActivity()).setCurrentPage(33);
                    } else {
                        ((NewDealViewPager) getActivity()).setCurrentPage(54);
                    }
//                    ((NewDealViewPager) getActivity()).setCurrentPage(32);
                } else if (isNormal)
                    iGoToNextPage.whatNextClick();
                else
                    iGoToNextPage.whatNextClick();
//                } else {
//                    new CustomToast(getActivity()).alert("Please fill at least one field.");
//                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.goToBackIndex();
            }
        });

    }

    private void getSalesPersonList() {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.show();
//        progressDialog.setMessage("Fetching Salesperson...");
        progress.setVisibility(View.VISIBLE);
        String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        final Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String id = recordData.where(rand_int1).getDealershipDetails().getDealershipId() + "";
        salesPersons.clear();
        adapter.notifyDataSetChanged();
        RetrofitInitialization.getDs_services().getSalesPerson("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<ArrayList<SalesPersonList>>() {
            @Override
            public void onResponse(Call<ArrayList<SalesPersonList>> call, Response<ArrayList<SalesPersonList>> response) {
                progress.setVisibility(View.GONE);
                if (response.isSuccessful() && response.code() == 200) {
                    LocalSalesPerson localSalesPerson = new LocalSalesPerson();
                    localSalesPerson.setLocalSales(response.body());
                    String list = gson.toJson(localSalesPerson);
                    PreferenceManger.putString(AppConstant.SALES_PERSON, "");
                    PreferenceManger.putString(AppConstant.SALES_PERSON, list);
                    SalesPersonList salesPersonList = new SalesPersonList();
                    salesPersonList.setFirstName("Please select salesperson");
                    salesPersons.addAll(response.body());
                    salesPersons.add(0, salesPersonList);
                    adapter.notifyDataSetChanged();
                    Gson gson = new GsonBuilder().create();
                    RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

                    if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons() != null) {
                        com.volga.dealershieldtablet.ui.coBuyer.SalesPersons salesPersons1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons();
                        if (!PreferenceManger.getBooleanValue("added")) {
                            if (salesPersons1 != null && salesPersons1.getUserId() != null && salesPersons1.getUserId().length() > 0) {
                                int pos = 0;
                                for (int i = 0; i < salesPersons.size(); i++) {
                                    if (salesPersons.get(i).getId() != null && salesPersons.get(i).getId().equalsIgnoreCase(salesPersons1.getUserId())) {
                                        pos = i;
                                    }
                                }
                                spinnerOfferType.setSelection(pos);

                            }
                        } else {
                            int pos = 0;
                            for (int i = 1; i < salesPersons.size(); i++) {
                                if (salesPersons.get(i).getEmail().equalsIgnoreCase(PreferenceManger.getStringValue("sEmail"))) {
                                    pos = i;
                                }
                            }
                            spinnerOfferType.setSelection(pos);
                            PreferenceManger.putBoolean("added", false);
                            PreferenceManger.putString("sEmail", "");
                        }
                    } else if (PreferenceManger.getBooleanValue("added")) {
                        int pos = 0;
                        for (int i = 1; i < salesPersons.size(); i++) {
                            if (salesPersons.get(i).getEmail().equalsIgnoreCase(PreferenceManger.getStringValue("sEmail"))) {
                                pos = i;
                            }
                        }
                        spinnerOfferType.setSelection(pos);
                        PreferenceManger.putBoolean("added", false);
                        PreferenceManger.putString("sEmail", "");
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SalesPersonList>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (getUserVisibleHint()) {
//        if (DSAPP.getInstance().isNetworkAvailable()) {
//            String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
//            final Gson gson = new GsonBuilder().create();
//            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//
//            if (recordData != null && recordData.where(rand_int1) != null && recordData.where(rand_int1).getDealershipDetails() != null && recordData.where(rand_int1).getDealershipDetails().getDealershipId() > 0) {
//                String id = recordData.where(rand_int1).getDealershipDetails().getDealershipId() + "";
//                getSalesPersonList();
//            }
//        } else {
        getSalesFromLocal();
//        }
//        }else {
//            getSalesFromLocal();
//        }
    }

    private void getSalesFromLocal() {
        salesPersons.clear();
        Gson gson = new GsonBuilder().create();
        LocalSalesPerson salesPerson = gson.fromJson(PreferenceManger.getStringValue(AppConstant.SALES_PERSON), LocalSalesPerson.class);
        SalesPersonList salesPersonList = new SalesPersonList();
        salesPersonList.setFirstName("Please select salesperson");
        salesPersons.addAll(salesPerson.getLocalSales());
        salesPersons.add(0, salesPersonList);
        adapter.notifyDataSetChanged();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        com.volga.dealershieldtablet.ui.coBuyer.SalesPersons salesPersons1 = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons();
        if (!PreferenceManger.getBooleanValue("added")) {
            if (salesPersons1 != null && salesPersons1.getUserId() != null && salesPersons1.getUserId().length() > 0) {
                int pos = 0;
                for (int i = 1; i < salesPersons.size(); i++) {
                    if (salesPersons.get(i).getId() != null && salesPersons.get(i).getId().equalsIgnoreCase(salesPersons1.getUserId())) {
                        pos = i;
                    }
                }
                spinnerOfferType.setSelection(pos);
            }
        } else {
            int pos = 0;
            for (int i = 1; i < salesPersons.size(); i++) {
                if (salesPersons.get(i).getEmail().equalsIgnoreCase(PreferenceManger.getStringValue("sEmail"))) {
                    pos = i;
                }
            }
            spinnerOfferType.setSelection(pos);
            PreferenceManger.putBoolean("added", false);
            PreferenceManger.putString("sEmail", "");
        }
    }

    public void SetRecordData(SalesPersonList salesPersonList) {

//        String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        recordData.where(rand_int1).getCustomerDetails().getSalesPersons().getMiddleName()
        com.volga.dealershieldtablet.ui.coBuyer.SalesPersons salesPersons = new com.volga.dealershieldtablet.ui.coBuyer.SalesPersons();
        try {
            salesPersons.setUserId(salesPersonList.getId());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setSalesId(salesPersonList.getId());
            salesPersons.setEmail(salesPersonList.getEmail());
            salesPersons.setFirstName(salesPersonList.getFirstName());
            salesPersons.setLastName(salesPersonList.getLastName());

            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getMiddleName() != null) {
                salesPersons.setMiddleName(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getMiddleName() == null ? "" : recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getSalesPersons().getMiddleName());
            }
        } catch (Exception e) {
            Timber.tag("Error : ").e(e.toString());
        }

        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().setSalesPersons(salesPersons);
        String recordDataString1 = gson.toJson(recordData);
        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
    }
}
