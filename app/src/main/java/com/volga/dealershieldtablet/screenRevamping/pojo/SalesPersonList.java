package com.volga.dealershieldtablet.screenRevamping.pojo;

public class SalesPersonList {
    private String IsTempPassword;


    private String Email;

    private String IsActive;

    private String Commission;

    private String[] UserPrivileges;

    private String CompanyName;

    private String StatusChangeDateUtc;

    private String IsDeleted;

    private String IsCompanyActive;

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getRoles() {
        return Roles;
    }

    public void setRoles(String roles) {
        Roles = roles;
    }

    public String getProfilePicUrl() {
        return ProfilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        ProfilePicUrl = profilePicUrl;
    }

    private String UserName;

    private String CompanyId;

    private String FirstName;


    private String Roles;

    private String EmailConfirmed;

    private String DrawAmount;


    private String FullName;

    private String ProfilePicUrl;

    private String PhoneNumber;

    private String Id;

    private String LastName;

    private String[] DepartmentUserMappings;

    public String getIsTempPassword() {
        return IsTempPassword;
    }

    public void setIsTempPassword(String IsTempPassword) {
        this.IsTempPassword = IsTempPassword;
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String IsActive) {
        this.IsActive = IsActive;
    }

    public String getCommission() {
        return Commission;
    }

    public void setCommission(String Commission) {
        this.Commission = Commission;
    }

    public String[] getUserPrivileges() {
        return UserPrivileges;
    }

    public void setUserPrivileges(String[] UserPrivileges) {
        this.UserPrivileges = UserPrivileges;
    }


    public String getStatusChangeDateUtc() {
        return StatusChangeDateUtc;
    }

    public void setStatusChangeDateUtc(String StatusChangeDateUtc) {
        this.StatusChangeDateUtc = StatusChangeDateUtc;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String IsDeleted) {
        this.IsDeleted = IsDeleted;
    }

    public String getIsCompanyActive() {
        return IsCompanyActive;
    }

    public void setIsCompanyActive(String IsCompanyActive) {
        this.IsCompanyActive = IsCompanyActive;
    }


    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String CompanyId) {
        this.CompanyId = CompanyId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }


    public String getEmailConfirmed() {
        return EmailConfirmed;
    }

    public void setEmailConfirmed(String EmailConfirmed) {
        this.EmailConfirmed = EmailConfirmed;
    }

    public String getDrawAmount() {
        return DrawAmount;
    }

    public void setDrawAmount(String DrawAmount) {
        this.DrawAmount = DrawAmount;
    }


    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }


    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String[] getDepartmentUserMappings() {
        return DepartmentUserMappings;
    }

    public void setDepartmentUserMappings(String[] DepartmentUserMappings) {
        this.DepartmentUserMappings = DepartmentUserMappings;
    }

    @Override
    public String toString() {
        return "ClassPojo [IsTempPassword = " + IsTempPassword + ", SelectedTutorId = " + ", Email = " + Email + ", IsActive = " + IsActive + ", Commission = " + Commission + ", UserPrivileges = " + ", CompanyName = " + CompanyName + ", StatusChangeDateUtc = " + StatusChangeDateUtc + ", IsDeleted = " + IsDeleted + ", IsCompanyActive = " + IsCompanyActive + ", UserName = " + UserName + ", CompanyId = " + CompanyId + ", FirstName = " + FirstName + ", DealershipDetails = " + ", Roles = " + Roles + ", EmailConfirmed = " + EmailConfirmed + ", DrawAmount = " + DrawAmount + ", DocumentData = " + ", DealershipUserMappings = " + ", FullName = " + FullName + ", ProfilePicUrl = " + ProfilePicUrl + ", PhoneNumber = " + PhoneNumber + ", Id = " + Id + ", LastName = " + LastName + ", DepartmentUserMappings = " + "]";
    }
}
