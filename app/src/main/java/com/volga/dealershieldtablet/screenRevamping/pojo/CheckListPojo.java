package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.ArrayList;

public class CheckListPojo {
    private ArrayList<CheckList> CheckList;

    public ArrayList<CheckList> getCheckList() {
        return CheckList;
    }

    public void setCheckList(ArrayList<CheckList> CheckList) {
        this.CheckList = CheckList;
    }

    @Override
    public String toString() {
        return "ClassPojo [CheckList = " + CheckList + "]";
    }
}
	