package com.volga.dealershieldtablet.screenRevamping.pojo;

public class AddSalesperson
{
    private String Role;

    private String Email;

    private String FirstName;

    private String PhoneNumber;

    private String LastName;

    private String DepartmentId;

    private String DealershipIds;

    public String getRole ()
    {
        return Role;
    }

    public void setRole (String Role)
    {
        this.Role = Role;
    }

    public String getEmail ()
    {
        return Email;
    }

    public void setEmail (String Email)
    {
        this.Email = Email;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getPhoneNumber ()
    {
        return PhoneNumber;
    }

    public void setPhoneNumber (String PhoneNumber)
    {
        this.PhoneNumber = PhoneNumber;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    public String getDepartmentId ()
    {
        return DepartmentId;
    }

    public void setDepartmentId (String DepartmentId)
    {
        this.DepartmentId = DepartmentId;
    }

    public String getDealershipIds ()
    {
        return DealershipIds;
    }

    public void setDealershipIds (String DealershipIds)
    {
        this.DealershipIds = DealershipIds;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Role = "+Role+", Email = "+Email+", FirstName = "+FirstName+", PhoneNumber = "+PhoneNumber+", LastName = "+LastName+", DepartmentId = "+DepartmentId+", DealershipIds = "+DealershipIds+"]";
    }
}