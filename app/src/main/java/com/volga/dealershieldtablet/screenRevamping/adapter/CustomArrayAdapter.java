package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.content.Context;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.screenRevamping.pojo.SalesPersonList;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;

import java.text.MessageFormat;
import java.util.ArrayList;

public class CustomArrayAdapter extends ArrayAdapter<SalesPersonList> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<SalesPersonList> salesPersons;
    private final int mResource;

    public CustomArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                              @NonNull ArrayList<SalesPersonList> items) {
        super(context, resource, 0, items);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        this.salesPersons = items;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        DSTextView offTypeTv = view.findViewById(R.id.salespesonName);

        SalesPersonList salesPersonList = salesPersons.get(position);
        if (position == 0) {
            offTypeTv.setText(salesPersonList.getFirstName());
        } else {
            if (salesPersonList.getEmail() != null && salesPersonList.getEmail().length() != 0)
                offTypeTv.setText(MessageFormat.format("{0} {1} <{2}>", salesPersonList.getFirstName(), salesPersonList.getLastName(), salesPersonList.getEmail()));
            else{
                offTypeTv.setText(MessageFormat.format("{0} {1}", salesPersonList.getFirstName(), salesPersonList.getLastName()));
            }
        }
            return view;
    }
}
