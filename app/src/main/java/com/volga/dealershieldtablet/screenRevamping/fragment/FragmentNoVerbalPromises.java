package com.volga.dealershieldtablet.screenRevamping.fragment;

import static android.os.Looper.getMainLooper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.SettingsAndPermissions;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.VehicleDetails;
import com.volga.dealershieldtablet.Retrofit.Pojo.oldCar.HistoryListPojo;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.camera.CameraService;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.interfaceCallback.PictureCapturingListener;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.adapter.VerbalPromiseAdapter;
import com.volga.dealershieldtablet.screenRevamping.pojo.NoVerbalPromise;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromise;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromiseUpload;
import com.volga.dealershieldtablet.services.APictureCapturingService;
import com.volga.dealershieldtablet.services.PictureCapturingServiceImpl;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 21-08-2019.
 */
public class FragmentNoVerbalPromises extends BaseFragment implements VerbalPromiseAdapter.AllChecked {

    IGoToNextPage iGoToNextPage;
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    private Button yes, no;
    private String[] list;
    //changed for bullet point
    private boolean allChecked = true;
    private ArrayList<VerbalPromise> verbalPromises = new ArrayList<>();
    private VerbalPromiseAdapter adapter;
    private DSTextView progress;
    private DSTextView time;
    boolean isTaken;
    private DSTextView sign;
    private ImageView delete;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getActivity() != null) {
                if (intent.getBooleanExtra("photoCaptured", false)) {
                    isTaken = true;
                    if (next != null && allChecked && isSigned) {
                        next.setVisibility(View.VISIBLE);
                    enableWithGreen(next);}
                }
            }
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && next != null) {
//            next.setVisibility(View.GONE);
            disableWithGray(next);
            time.setText(getCurrentTimeString());
            signaturePad.clear();
            isSigned = false;
//            enableNext(next);
            Gson gson = new GsonBuilder().create();
            final RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            SettingsAndPermissions vehicleDetails = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions();
//            if (yes != null && vehicleDetails.getStickerRemovalPermission()) {
//                yes.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
//                no.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_gray));
//            }
            //Code for displaying already selected Confirmation

            initPhoto();

        } else {
            isTaken = false;
        }
    }

    private void initPhoto() {
        Gson gson = new GsonBuilder().create();
        final RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Handler mainHandler = new Handler(getActivity().getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isTaken = false;
                if (!isTaken) {
//                        isTaken = true;
                    File file;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file = getOutputFile("UserPic", "co_no_verbal_promise_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "no_verbal_promise_screen_pic");
                    }

                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
                        takeUserPicture(file);
                    } else {
                        isTaken = true;
                    }
                }
            } // This is your code
        };
        mainHandler.post(myRunnable);
    }

    public void takeUserPicture(final File file) {
        final int[] retryCount = {0};
        if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO) && AppConstant.TAKE_PHOTO_OR_NOT) {
            this.file = file;
            if (Build.MODEL.equalsIgnoreCase("SM-T380") || Build.MODEL.equalsIgnoreCase("SM-T385")) {
                Handler mainHandler = new Handler(getMainLooper());
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        retryCount[0]++;
                        APictureCapturingService pictureService = PictureCapturingServiceImpl.getInstance(getActivity());
                        PictureCapturingListener capturingListener = new PictureCapturingListener() {
                            @Override
                            public void onCaptureDone(String pictureUrl, byte[] pictureData) {
                                Log.e("Picture url: ", "Pic URL: " + pictureUrl);
                                isTaken = true;
                                if (allChecked && isSigned) {
                                    next.setVisibility(View.VISIBLE);
                                    enableWithGreen(next);}
                            }

                            @Override
                            public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
                                if (picturesTaken.size() == 0 && retryCount[0] < 3) {
                                    takeUserPicture(file);
                                } else if (picturesTaken.size() == 1) {
                                    isTaken = true;
                                    if (allChecked && isSigned){
                                        next.setVisibility(View.VISIBLE);enableWithGreen(next);}
                                }
                                Log.e("Picture saved: ", "Pic size: " + picturesTaken.size());
                            }
                        };
                        pictureService.startCapturing(capturingListener, file);
                    }
                }, 250);


            } else {
                Intent intent = new Intent(getActivity(), CameraService.class);
                intent.putExtra("fileName", file.getAbsolutePath());
                getActivity().startService(intent);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        next.setVisibility(View.VISIBLE);
//                    }
//                }, AppConstant.NEXT_DELAY);
            }
        }
//        PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.verbal_promises_layout, container, false);
        isSigned = false;
        isAnsSelected = false;
        if (NewDealViewPager.currentPage == 77) {
            initPhoto();
        }
        initView();
        return mView;
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        time = mView.findViewById(R.id.time);
        time.setText(getCurrentTimeString());
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView signature = mView.findViewById(R.id.signature);
        verbalPromises = new ArrayList<>();
        delete = mView.findViewById(R.id.deleteButton);
        disableDelete(delete);
       /* final LockableScrollView nestedScrollView = mView.findViewById(R.id.disclosureMain);
        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                //                    getPlaylistFromServer("more");
                nestedScrollView.setScrollingEnabled(diff != 0);
            }
        });*/

        progress = mView.findViewById(R.id.progress);


        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
//        next.setVisibility(View.GONE);
        disableWithGray(next);
        DSTextView hint = mView.findViewById(R.id.hint);
        hint.setText(getString(R.string.noverbal_hint));
        RecyclerView recyclerView = mView.findViewById(R.id.recyclerView);
        mView.findViewById(R.id.recyclerView).setFocusable(false);
        mView.findViewById(R.id.temp).requestFocus();
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

//        ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
//        ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
//        ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
//        int page = 0;
//            if (report != null && report.get(report.size() - 1).isChecked() && condition != null && condition.get(condition.size() - 1).isChecked()&&history != null && history.get(history.size() - 1).isChecked()) {
//                page = 72;
//            }else if (report != null && report.get(report.size() - 1).isChecked()&&history != null && history.get(history.size() - 1).isChecked()) {
//                page = 73;
//            }else  if (report != null && report.get(report.size() - 1).isChecked()) {
//                page = 74;
//            }else{
        if (decidePageNumber() == 10)
            text_cancelBack.setText(String.format(getString(R.string.page1), "7", decidePageNumber()));
        else if (decidePageNumber() == 11)
            text_cancelBack.setText(String.format(getString(R.string.page1), "8", decidePageNumber()));
        else if (decidePageNumber() == 12)
            text_cancelBack.setText(String.format(getString(R.string.page1), "9", decidePageNumber()));
        else
            text_cancelBack.setText(String.format(getString(R.string.page1), "10", decidePageNumber()));
//            }
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        sign = mView.findViewById(R.id.signhere);
        final DSTextView title = mView.findViewById(R.id.title);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && !recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
            sign.setText(Html.fromHtml(getString(R.string.signature)));
//            title.setText(Html.fromHtml(getString(R.string.printRecieveDisclosure)));
        } else {
            sign.setText(Html.fromHtml(getString(R.string.co_signature)));
//            title.setText(Html.fromHtml(getString(R.string.co_printRecieveDisclosure)));
        }
        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails() != null) {
            DSTextView vin = mView.findViewById(R.id.vin);
            DSTextView vehicle = mView.findViewById(R.id.vehicle);
            String vinNumber = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getVINNumber();
            String make = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarMake();
            String model = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarModel();
            String year = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getCarYear();
            vin.setText(String.format("VIN: %s", vinNumber));
            vehicle.setText(String.format("%s, %s, %s", make, model, year));
        }

        adapter = new VerbalPromiseAdapter(getActivity(), verbalPromises, this);
        recyclerView.setAdapter(adapter);
        final LinearLayout mainContent = mView.findViewById(R.id.maineContent);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!allChecked) {
                    new CustomToast(getActivity()).alert(getString(R.string.select_all_options));
                    return;
                }
//                disableNext(next);
                disableNext(next);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        enableNext(next);
                    }
                }, 5000);
                addSvgSignatureToGallery(signaturePad.getSignatureSvg());
                SetRecordData();
//                if (checkMandatoryImage())
                analyseTheImages();
                iGoToNextPage.whatNextClick();
                File file;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file = getOutputFile("Screenshot", "co_no_verbal_promise_screen");
                } else {
                    file = getOutputFile("Screenshot", "no_verbal_promise_screen");
                }
                scrollableScreenshot(mainContent, file);
            }
        });
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    String path_signature;
                    Gson gson = new GsonBuilder().create();
                    final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                    Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                    path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                    File svgFile;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        svgFile = new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg");
                    } else {
                        svgFile = new File(path_signature + "/IMG_buyer_no_verble_promise.svg");
                    }
                    File file;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file = getOutputFile("UserPic", "co_no_verbal_promise_screen_pic");
                    } else {
                        file = getOutputFile("UserPic", "no_verbal_promise_screen_pic");
                    }
                    File file1;
                    if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                        file1 = getOutputFile("Screenshot", "co_no_verbal_promise_screen");
                    } else {
                        file1 = getOutputFile("Screenshot", "no_verbal_promise_screen");
                    }
                    Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                    Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
//                SetRecordData();
//                    ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
//                    ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
//                    ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
//                    int page = 0;
//                    if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
//                        if (report != null && report.get(report.size() - 1).isChecked() && condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
//                            page = 73;
//                        } else if (report != null && report.get(report.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
//                            page = 74;
//                        } else if (report != null && report.get(report.size() - 1).isChecked()) {
//                            page = 75;
//                        }
//                    } else {
//
//                        if (condition != null) {
//                            if (condition.get(condition.size() - 1).isChecked())
//                                page = 73;
//                            else {
//                                page = 74;
//                            }
//                        }
//                    }
//                    if (page != 0) {
//                        ((NewDealViewPager) getActivity()).setCurrentPage(page);
//                    } else
//                        iGoToNextPage.goToBackIndex();
                    Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());
                    Log.e("Device back: ", " Device back pressed: " + keyCode);
                    return false;
                }
                return false;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String path_signature;
                Gson gson = new GsonBuilder().create();
                final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
                path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
                File svgFile;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    svgFile = new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg");
                } else {
                    svgFile = new File(path_signature + "/IMG_buyer_no_verble_promise.svg");
                }
                File file;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file = getOutputFile("UserPic", "co_no_verbal_promise_screen_pic");
                } else {
                    file = getOutputFile("UserPic", "no_verbal_promise_screen_pic");
                }
                File file1;
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                    file1 = getOutputFile("Screenshot", "co_no_verbal_promise_screen");
                } else {
                    file1 = getOutputFile("Screenshot", "no_verbal_promise_screen");
                }
                Log.e("FileDeleted ", file.getName() + " is deleted : " + file.delete());
                Log.e("FileDeleted ", file1.getName() + " is deleted : " + file1.delete());
//                SetRecordData();
                ArrayList<HistoryListPojo> condition = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleCondition();
                ArrayList<HistoryListPojo> history = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleHistory();
                ArrayList<HistoryListPojo> report = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getUsedVehicleReport();
                int page = 0;
                if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New")) {
                    if (report != null && report.get(report.size() - 1).isChecked() && condition != null && condition.get(condition.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                        page = 73;
                    } else if (report != null && report.get(report.size() - 1).isChecked() && history != null && history.get(history.size() - 1).isChecked()) {
                        page = 74;
                    } else if (report != null && report.get(report.size() - 1).isChecked()) {
                        page = 75;
                    }
                } else {

                    if (condition != null) {
                        if (condition.get(condition.size() - 1).isChecked())
                            page = 73;
                        else {
                            page = 74;
                        }
                    }
                }
                if (page != 0) {
                    ((NewDealViewPager) getActivity()).setCurrentPage(page);
                } else
                    iGoToNextPage.goToBackIndex();
                Log.e("FileDeleted ", svgFile.getName() + " is deleted : " + svgFile.delete());

            }
        });
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
//                next.setVisibility(View.GONE);
            }
        });
        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                time.setText(getCurrentTimeString());
                sign.setVisibility(View.GONE);
                enableDelete(delete);
            }

            @Override
            public void onSigned() {
                if (allChecked) {
                    if (Build.MODEL.equalsIgnoreCase("SM-T295")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isSigned) {
                                    enableWithGreen(next);
                                    next.setVisibility(View.VISIBLE);
                                }
                            }
                        }, AppConstant.NEXT_DELAY);
                    } else if (!AppConstant.TAKE_PHOTO_OR_NOT) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    } else if (isTaken) {
                        enableWithGreen(next);
                        next.setVisibility(View.VISIBLE);
                    }
                }
                isSigned = true;
            }

            @Override
            public void onClear() {
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });

        signature.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int[] textLocation = new int[2];

                    if (event.getRawX() >= textLocation[0] + signature.getWidth() - signature.getTotalPaddingRight()) {

                        signaturePad.clear();
                        isSigned = false;
                        disableWithGray(next);
//                        next.setVisibility(View.GONE);
                        return true;
                    }
                }
                return true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
//        if (DSAPP.getInstance().isNetworkAvailable())
//            getVerbalPromiseList();
//        else {
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("PhotoTaken"));
        verbalPromises.clear();
        Gson gson = new GsonBuilder().create();
        NoVerbalPromise verbalPromise = gson.fromJson(PreferenceManger.getStringValue(AppConstant.VERBAL_PROMISE), NoVerbalPromise.class);
        final String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);


        ArrayList<VerbalPromise> listPojos = new ArrayList<>(verbalPromise.getVerbalPromise());
        for (int i = 0; i < listPojos.size(); i++) {
            listPojos.get(i).setIsActive(false);
        }
        VehicleDetails vehicleDetails = recordData.where(rand_int1).getVehicleDetails();
        if (vehicleDetails != null && vehicleDetails.getTypeOfVehicle() != null && vehicleDetails.getTypeOfVehicle().equalsIgnoreCase("New")) {
            for (int i = 0; i < listPojos.size(); i++) {
                if (!listPojos.get(i).isOnlyForUsedCar()) {
                    verbalPromises.add(listPojos.get(i));
                }
            }
        } else {
            for (int i = 0; i < listPojos.size(); i++) {
                if (!listPojos.get(i).isOnlyForNewCar()) {
                    verbalPromises.add(listPojos.get(i));
                }
            }
        }

        if (recordData.where(rand_int1).getSettingsAndPermissions() != null && recordData.where(rand_int1).getSettingsAndPermissions().isCoBuyer()) {
            if (recordData.where(rand_int1).getCoBuyerCustomerDetails().getVerbalPromises() != null && recordData.where(rand_int1).getCoBuyerCustomerDetails().getVerbalPromises().size() > 0) {
                ArrayList<VerbalPromiseUpload> localList = recordData.where(rand_int1).getCoBuyerCustomerDetails().getVerbalPromises();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < verbalPromises.size(); j++) {
                        if (localList.get(i).getVerbalPromiseId().equalsIgnoreCase(verbalPromises.get(j).getId())) {
                            verbalPromises.get(j).setIsActive(true);
                        }
                    }

                }
            }
        } else {
            if (recordData.where(rand_int1).getSettingsAndPermissions() != null && recordData.where(rand_int1).getSettingsAndPermissions().getVerbalPromises() != null && recordData.where(rand_int1).getSettingsAndPermissions().getVerbalPromises().size() > 0) {
                ArrayList<VerbalPromiseUpload> localList = recordData.where(rand_int1).getSettingsAndPermissions().getVerbalPromises();
                for (int i = 0; i < localList.size(); i++) {
                    for (int j = 0; j < verbalPromises.size(); j++) {
                        if (localList.get(i).getVerbalPromiseId().equalsIgnoreCase(verbalPromises.get(j).getId())) {
                            verbalPromises.get(j).setIsActive(true);
                        }
                    }

                }
            }
        }
        allChecked = true;
        //changed for bullet point
//        for (int i = 0; i < verbalPromises.size(); i++) {
//            if (!verbalPromises.get(i).getIsActive()) {
//                allChecked = false;
//                break;
//            }
//
//        }
        adapter.notifyDataSetChanged();
//        }
    }

    private void getVerbalPromiseList() {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.show();
//        progressDialog.setMessage("Fetching no verbal promises...");
        progress.setVisibility(View.VISIBLE);
        verbalPromises.clear();
        final String rand_int1 = PreferenceManger.getStringValue(AppConstant.DMV_NUMBER);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        String id;
        if (recordData != null && recordData.where(rand_int1) != null && recordData.where(rand_int1).getDealershipDetails() != null && recordData.where(rand_int1).getDealershipDetails().getDealershipId() > 0)
            id = recordData.where(rand_int1).getDealershipDetails().getDealershipId() + "";
        else {
            id = PreferenceManger.getIntegerValue(AppConstant.SELECTED_DEALERSHIP_ID) + "";
        }
        verbalPromises.clear();
        RetrofitInitialization.getDs_services().getVerbalPromise("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), id).enqueue(new Callback<NoVerbalPromise>() {
            @Override
            public void onResponse(Call<NoVerbalPromise> call, Response<NoVerbalPromise> response) {
                progress.setVisibility(View.GONE);
                if (response.code() == 200 && response.isSuccessful()) {
                    Gson gson = new GsonBuilder().create();
                    String list = gson.toJson(response.body());
                    PreferenceManger.putString(AppConstant.VERBAL_PROMISE, "");
                    PreferenceManger.putString(AppConstant.VERBAL_PROMISE, list);
                    ArrayList<VerbalPromise> listPojos = new ArrayList<>(response.body().getVerbalPromise());
                    for (int i = 0; i < listPojos.size(); i++) {
                        listPojos.get(i).setIsActive(false);
                    }
                    VehicleDetails vehicleDetails = recordData.where(rand_int1).getVehicleDetails();
                    if (vehicleDetails != null && vehicleDetails.getTypeOfVehicle() != null && vehicleDetails.getTypeOfVehicle().equalsIgnoreCase("New")) {
                        for (int i = 0; i < listPojos.size(); i++) {
                            if (!listPojos.get(i).isOnlyForUsedCar()) {
                                verbalPromises.add(listPojos.get(i));
                            }
                        }
                    } else {
                        for (int i = 0; i < listPojos.size(); i++) {
                            if (!listPojos.get(i).isOnlyForNewCar()) {
                                verbalPromises.add(listPojos.get(i));
                            }
                        }
                    }

                    if (recordData.where(rand_int1).getSettingsAndPermissions().isCoBuyer()) {
                        if (recordData.where(rand_int1).getCoBuyerCustomerDetails().getVerbalPromises() != null && recordData.where(rand_int1).getCoBuyerCustomerDetails().getVerbalPromises().size() > 0) {
                            ArrayList<VerbalPromiseUpload> localList = recordData.where(rand_int1).getCoBuyerCustomerDetails().getVerbalPromises();
                            for (int i = 0; i < localList.size(); i++) {
                                for (int j = 0; j < verbalPromises.size(); j++) {
                                    if (localList.get(i).getVerbalPromiseId().equalsIgnoreCase(verbalPromises.get(j).getId())) {
                                        verbalPromises.get(j).setIsActive(true);
                                    }
                                }

                            }
                        }
                    } else {
                        if (recordData.where(rand_int1).getSettingsAndPermissions().getVerbalPromises() != null && recordData.where(rand_int1).getSettingsAndPermissions().getVerbalPromises().size() > 0) {
                            ArrayList<VerbalPromiseUpload> localList = recordData.where(rand_int1).getSettingsAndPermissions().getVerbalPromises();
                            for (int i = 0; i < localList.size(); i++) {
                                for (int j = 0; j < verbalPromises.size(); j++) {
                                    if (localList.get(i).getVerbalPromiseId().equalsIgnoreCase(verbalPromises.get(j).getId())) {
                                        verbalPromises.get(j).setIsActive(true);
                                    }
                                }

                            }
                        }
                    }

                    allChecked = true;
                    //changed for bullet point
//                    for (int i = 0; i < verbalPromises.size(); i++) {
//                        if (!verbalPromises.get(i).getIsActive()) {
//                            allChecked = false;
//                            break;
//                        }
//
//                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<NoVerbalPromise> call, Throwable t) {

            }
        });
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {


            Gson gson = new GsonBuilder().create();
            RecordData recordData;
            recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

            File svgFile;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                svgFile = getOutputMediaFile("Signatures", "co_buyer_no_verble_promise");
            } else {
                svgFile = getOutputMediaFile("Signatures", "buyer_no_verble_promise");
            }
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".svg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void SetRecordData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData;
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null) {
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLastPageIndex(((NewDealViewPager) getActivity()).getCurrentPage());
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setLatestTimeStamp(Calendar.getInstance().getTime().toString());
//            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setPrintCopyRecieved(true);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(makeHashMapOfImages(recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))));
            if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                ArrayList<VerbalPromiseUpload> verbalPromiseUploadArrayList = new ArrayList<>();
                if (verbalPromises != null) {
                    for (int i = 0; i < verbalPromises.size(); i++) {
                        VerbalPromiseUpload verbalPromiseUpload = new VerbalPromiseUpload();
                        verbalPromiseUpload.setVerbalPromiseId(verbalPromises.get(i).getId());
                        verbalPromiseUpload.setValue(true);
                        verbalPromiseUploadArrayList.add(verbalPromiseUpload);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCoBuyerCustomerDetails().setVerbalPromises(verbalPromiseUploadArrayList);
            } else {
                ArrayList<VerbalPromiseUpload> verbalPromiseUploadArrayList = new ArrayList<>();
                if (verbalPromises != null) {
                    for (int i = 0; i < verbalPromises.size(); i++) {
                        VerbalPromiseUpload verbalPromiseUpload = new VerbalPromiseUpload();
                        verbalPromiseUpload.setVerbalPromiseId(verbalPromises.get(i).getId());
                        verbalPromiseUpload.setValue(true);
                        verbalPromiseUploadArrayList.add(verbalPromiseUpload);
                    }
                }
                recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setVerbalPromises(verbalPromiseUploadArrayList);
            }
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
        }

    }

    @Override
    public void allChecked(boolean allChecked) {
        this.allChecked = allChecked;
        if (allChecked && isSigned) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    enableWithGreen(next);
                    next.setVisibility(View.VISIBLE);
                }
            }, 100);
        } else {
            disableWithGray(next);
//            next.setVisibility(View.GONE);
        }
    }

//    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
//        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
////        HashMap<String,boolean> stringHashMap=new HashMap<>();
//        String path_licence, path_signature, path_car;
//        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
//        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
//        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
//        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle1() == null) {
//                record.getVehicleDetails().setTitle1("Additional Image 1");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
//        }
//        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle2() == null) {
//                record.getVehicleDetails().setTitle2("Additional Image 2");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
//            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
//        }
//        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle3() == null) {
//                record.getVehicleDetails().setTitle3("Additional Image 3");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
//            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
//        }
//        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle4() == null) {
//                record.getVehicleDetails().setTitle4("Additional Image 4");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
//        }
//        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
//            if (record.getVehicleDetails().getTitle5() == null) {
//                record.getVehicleDetails().setTitle5("Additional Image 5");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
//            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle1() == null) {
//                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
//            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle2() == null) {
//                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
//            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle3() == null) {
//                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
//            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle4() == null) {
//                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
//        }
//        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
//            if (record.getTradeInVehicle().getTitle5() == null) {
//                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
//            }
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
//        }
//
//        /*Car Images*/
//        if (new File(path_car + "/IMG_front.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//
//            synced.setName("FRONTIMG");
//            synced.setFile(new File(path_car + "/IMG_front.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
//        }
//        if (new File(path_car + "/IMG_right.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("RIGHTIMG");
//            synced.setSynced(false);
//            synced.setFile(new File(path_car + "/IMG_right.jpg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
//        }
//        if (new File(path_car + "/IMG_rear.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("REARIMG");
//            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
//        }
//        if (new File(path_car + "/IMG_left.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("LEFTIMG");
//            synced.setFile(new File(path_car + "/IMG_left.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
//        }
//        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("WINDOWWSTICKERIMG");
//            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
//
////            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
//        }
//        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("MILEAGEIMG");
//            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
//        }
//        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("TRADEINMILEAGE");
//            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
//        }
//        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("LICENCEPLATENUMBER");
//            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
//        }
//        /*Additional Images*/
//
//        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("Customer_Insurance");
//            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
//        }
//        //Add all signature image here
//        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
//            synced.setName("language_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
//        }
//        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
//            synced.setName("type_of_purchase_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
//        }
//        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
//            synced.setName("confirm_car_images_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
//        }
//        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("confirm_car_details_selection");
//            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
//        }
//        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
//            synced.setName("test_drive_taken_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
//        }
//        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
//            synced.setName("no_test_drive_confirm_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
//        }
//        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
//            synced.setName("remove_stickers_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
//        }
//        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
//            synced.setName("CONTACTINFOSIGNATUREIMG");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
//        }
//        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
//            synced.setName("prints_recieved_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
//        }
//        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("history_disclosure");
//            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
//            synced.setName("history_report");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
//        }
//        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("condition_disclosure");
//            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_language_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_type_of_purchase_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
//            synced.setName("co_buyer_confirm_car_images_selection");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_confirm_car_details_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_test_drive_taken_selection");
//            synced.setSynced(false);
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_no_test_drive_confirm_selection");
//            synced.setSynced(false);
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_remove_stickers_selection");
//            synced.setSynced(false);
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
//        }
//        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_contact_info");
//            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_prints_recieved_selection");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
//            synced.setName("co_buyer_condition_disclosure");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {
//
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_history_report");
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
//        }
//        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
//            synced.setName("co_buyer_history_disclosure");
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
//        }
//        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("co_buyer_DLFRONT");
//            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
//        }
//        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
//            ImagesSynced synced = new ImagesSynced();
//            synced.setName("DRIVERLICENSEIMG");
//            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
//            synced.setSynced(false);
//            imagesSynceds.add(synced);
////            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
//        }
////        Gson gson = new GsonBuilder().create();
////        RecordData recordData;
////        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
////        String recordDataString = gson.toJson(recordData);
////        recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setImagesSyncedArrayList(imagesSynceds);
////        PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//        return imagesSynceds;
//    }

    private String getIds(String aList) {
        String id = "0";
        if (aList.trim().equalsIgnoreCase(getString(R.string.print1))) {
            id = "1";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print2))) {
            id = "2";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print3))) {
            id = "3";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print4))) {
            id = "4";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print5))) {
            id = "5";
        } else if (aList.trim().equalsIgnoreCase(getString(R.string.print6))) {
            id = "6";
        }
        return id;
    }

    private boolean checkMandatoryImage() {

        boolean allTrue = true;
        String path_licence, path_signature, path_car;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (!new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Buyer's DMV Front Image is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasCoBuyer() && !new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Co-Buyer's DMV Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_front.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_right.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Passengers side Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_rear.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Back Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_left.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Left Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_sticker.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Addendum Sticker/Buyers Guide Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Odometer Image side is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Odometer Image is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Licence Plate Image side is missing.");
            return false;
        } else {
            return true;
        }
    }


}
