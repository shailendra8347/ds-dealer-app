package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.List;

public class SendIncompleteEmail{
	private String CustomerVehicleTradeId;
	private List<String> ReportCodes;
	private List<String> Emails;

	public void setCustomerVehicleTradeId(String customerVehicleTradeId){
		this.CustomerVehicleTradeId = customerVehicleTradeId;
	}

	public String getCustomerVehicleTradeId(){
		return CustomerVehicleTradeId;
	}

	public void setReportCodes(List<String> reportCodes){
		this.ReportCodes = reportCodes;
	}

	public List<String> getReportCodes(){
		return ReportCodes;
	}

	public void setEmails(List<String> emails){
		this.Emails = emails;
	}

	public List<String> getEmails(){
		return Emails;
	}

	@Override
 	public String toString(){
		return 
			"SendIncompleteEmail{" + 
			"CustomerVehicleTradeId = '" + CustomerVehicleTradeId + '\'' +
			",ReportCodes = '" + ReportCodes + '\'' +
			",Emails = '" + Emails + '\'' +
			"}";
		}
}