package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.ArrayList;

public class CheckList
{
    private String DisplayOrder;

    private String IsForAll;

    private Language Language;

    private boolean IsActive;

    public boolean isOnlyForUsedCar() {
        return OnlyForUsedCar;
    }

    public void setOnlyForUsedCar(boolean onlyForUsedCar) {
        OnlyForUsedCar = onlyForUsedCar;
    }

    public boolean isOnlyForNewCar() {
        return OnlyForNewCar;
    }

    public void setOnlyForNewCar(boolean onlyForNewCar) {
        OnlyForNewCar = onlyForNewCar;
    }

    private boolean OnlyForUsedCar;
    private boolean OnlyForNewCar;

    private ArrayList<CheckListTranslation> CheckListTranslation;

    private String Published;

    private String Id;

    private String LanguageId;

    private String Name;

    public String getDisplayOrder ()
    {
        return DisplayOrder;
    }

    public void setDisplayOrder (String DisplayOrder)
    {
        this.DisplayOrder = DisplayOrder;
    }

    public String getIsForAll ()
    {
        return IsForAll;
    }

    public void setIsForAll (String IsForAll)
    {
        this.IsForAll = IsForAll;
    }

    public Language getLanguage ()
    {
        return Language;
    }

    public void setLanguage (Language Language)
    {
        this.Language = Language;
    }

    public boolean getIsActive ()
    {
        return IsActive;
    }

    public void setIsActive (boolean IsActive)
    {
        this.IsActive = IsActive;
    }

    public ArrayList<CheckListTranslation> getCheckListTranslation ()
    {
        return CheckListTranslation;
    }

    public void setCheckListTranslation (ArrayList<CheckListTranslation> CheckListTranslation)
    {
        this.CheckListTranslation = CheckListTranslation;
    }

    public String getPublished ()
    {
        return Published;
    }

    public void setPublished (String Published)
    {
        this.Published = Published;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getLanguageId ()
    {
        return LanguageId;
    }

    public void setLanguageId (String LanguageId)
    {
        this.LanguageId = LanguageId;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DisplayOrder = "+DisplayOrder+", IsForAll = "+IsForAll+", Language = "+Language+", IsActive = "+IsActive+", CheckListTranslation = "+CheckListTranslation+", Published = "+Published+", Id = "+Id+", LanguageId = "+LanguageId+", Name = "+Name+"]";
    }
}
		