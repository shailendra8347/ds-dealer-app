package com.volga.dealershieldtablet.screenRevamping.pojo;

public class VerbalPromiseTranslation
{
    private String DestinationLanguageId;

    private String VerbalPromiseId;

    private Language Language;

    private String Text;

    private String Id;

    public String getDestinationLanguageId ()
    {
        return DestinationLanguageId;
    }

    public void setDestinationLanguageId (String DestinationLanguageId)
    {
        this.DestinationLanguageId = DestinationLanguageId;
    }

    public String getVerbalPromiseId ()
    {
        return VerbalPromiseId;
    }

    public void setVerbalPromiseId (String VerbalPromiseId)
    {
        this.VerbalPromiseId = VerbalPromiseId;
    }

    public Language getLanguage ()
    {
        return Language;
    }

    public void setLanguage (Language Language)
    {
        this.Language = Language;
    }

    public String getText ()
    {
        return Text;
    }

    public void setText (String Text)
    {
        this.Text = Text;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DestinationLanguageId = "+DestinationLanguageId+", VerbalPromiseId = "+VerbalPromiseId+", Language = "+Language+", Text = "+Text+", Id = "+Id+"]";
    }
}