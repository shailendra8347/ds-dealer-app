package com.volga.dealershieldtablet.screenRevamping.pojo;

public class CheckListUpload{
	private String CheckListId;
	private boolean value;

	public void setCheckListId(String checkListId){
		this.CheckListId = checkListId;
	}

	public String getCheckListId(){
		return CheckListId;
	}

	public void setValue(boolean value){
		this.value = value;
	}

	public boolean isValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"CheckListUpload{" + 
			"CheckListId = '" + CheckListId + '\'' +
			",value = '" + value + '\'' + 
			"}";
		}
}
