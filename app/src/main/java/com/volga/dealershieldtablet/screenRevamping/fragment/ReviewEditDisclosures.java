package com.volga.dealershieldtablet.screenRevamping.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.screenRevamping.activity.ReviewEditDisclosureViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

/**
 * Created by ${Shailendra} on 22-08-2018.
 */
public class ReviewEditDisclosures extends BaseFragment {
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    IGoToNextPage iGoToNextPage;
    private boolean isTradeIn = false;
    private Button yes, no;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null) {
                updateData();
            }
            showFragmentInPortrait();
            //Code for displaying already selected Confirmation
        }
    }

    private void updateData() {
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
            new CustomToast(getActivity()).alert("Please wait\nLoading disclosures");
            Intent intent = new Intent(getActivity(), ReviewEditDisclosureViewPager.class);
            getActivity().startActivity(intent);
//                    yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
//                    no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
//                    next.setVisibility(View.VISIBLE);
            isAnsSelected = true;
            isTradeIn = true;

        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.review_edit_disclosure, container, false);
        initView();
        return mView;
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView txt_logout = mView.findViewById(R.id.txt_logout);
        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        text_cancelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_cancelBack.setEnabled(false);
                text_cancelBack.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        text_cancelBack.setEnabled(true);
                        text_cancelBack.setClickable(true);
                    }
                }, 6000);

                cancel();
            }
        });
        DSTextView title = mView.findViewById(R.id.title);
//        title.setText(Html.fromHtml(getString(R.string.tradeInVehicleUpDate)));
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
//            no.setEnabled(false);
//            no.setClickable(false);
//            next.setVisibility(View.GONE);
//        }else {
//            no.setEnabled(true);
//            no.setClickable(true);
//            next.setVisibility(View.VISIBLE);
//        }
        Gson gson = new GsonBuilder().create();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        try {
            if (!recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasTradeIn()) {
                updateData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Gson gson = new GsonBuilder().create();
//                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures() !=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getVehicleTradeConditionDisclosures().size() != 0) {
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleReport(null);
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleHistory(null);
//                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().setUsedVehicleCondition(null);
//                    String recordDataString1 = gson.toJson(recordData);
//                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString1);
//                }
                Intent intent = new Intent(getActivity(), ReviewEditDisclosureViewPager.class);
                getActivity().startActivity(intent);
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
                    next.setVisibility(View.GONE);
                } else {
                    next.setVisibility(View.VISIBLE);
                }

                isAnsSelected = true;
                isTradeIn = true;
                new CustomToast(getActivity()).alert("Please wait\nLoading disclosures");
//                SetRecordData(true);

//                iGoToNextPage.whatNextClick();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*show confirmation pop up*/
                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isDisclosureSkipped()) {
                    new CustomToast(getActivity()).alert("You have skipped the disclosures.\nPlease finalize first.");
                    return;
                }
                yes.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_white));
                no.setBackground(DSAPP.getContext().getDrawable(R.drawable.rounded_corner_appcolor));
                next.setVisibility(View.VISIBLE);
                isTradeIn = false;
                SetRecordData(false);
//                iGoToNextPage.whatNextClick();
            }
        });
        next.setVisibility(View.GONE);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetRecordData(isTradeIn);
                iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.goToBackIndex();
//                if (PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE) > 25)
//                    showFragmentInLandscape();
            }
        });

    }

    public void SetRecordData(boolean b) {
//        PreferenceManger.getBooleanValue(AppConstant.SKIP_INSURANCE)
        try {
            if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                ((NewDealViewPager) getActivity()).setCurrentPage(67);
            } else {
                if (PreferenceManger.getBooleanValue(AppConstant.SKIP_INSURANCE)) {
                    ((NewDealViewPager) getActivity()).setCurrentPage(67);
                } else
                    iGoToNextPage.whatNextClick();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        PreferenceManger.putBoolean(AppConstant.IS_TRADE_IN_SELECTED, isTradeIn);
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (recordData!=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))!=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions()!=null) {
//            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(b);
//            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(b);
//            String recordDataString = gson.toJson(recordData);
//            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//        }
//        if (!b) {
//            ((ViewPagerActivity) getActivity()).setCurrentPage(53);
//        } else {
//            iGoToNextPage.whatNextClick();
//        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (ReviewEditDisclosureViewPager.nowGotoNext) {
            new CustomToast(getActivity()).alert("Please wait\nSaving disclosures");
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ReviewEditDisclosureViewPager.nowGotoNext) {
                    ReviewEditDisclosureViewPager.nowGotoNext = false;
                    try {
                        if (PreferenceManger.getBooleanValue(AppConstant.SKIP_PHOTOS)) {
                            ((NewDealViewPager) getActivity()).setCurrentPage(67);
                        } else {
                            if (PreferenceManger.getBooleanValue(AppConstant.SKIP_INSURANCE)) {
                                ((NewDealViewPager) getActivity()).setCurrentPage(67);
                            } else
                                iGoToNextPage.whatNextClick();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 2000);
    }
}
