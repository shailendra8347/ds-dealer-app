package com.volga.dealershieldtablet.screenRevamping.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.ImageUploadResponse;
import com.volga.dealershieldtablet.Retrofit.RetrofitInitialization;
import com.volga.dealershieldtablet.screenRevamping.pojo.SendIncompleteEmail;
import com.volga.dealershieldtablet.ui.customUI.DSEdittext;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.KeyboardUtils;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class FragmentSendIncompleteEmailToBuyer extends BaseFragment {
    private View mView;
    private DSTextView send;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.incomplete_email_buyer, container, false);
        initViews();
        return mView;
    }

    private void showDialog(final SendIncompleteEmail sendIncompleteEmail) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View promptView = layoutInflater.inflate(R.layout.enter_multiple_email, null);

        final AlertDialog alertD = new AlertDialog.Builder(getActivity()).create();
        alertD.setCancelable(false);

        final DSTextView sendEmail = promptView.findViewById(R.id.sendEmail);
        final DSTextView close = promptView.findViewById(R.id.close);
        DSTextView title = promptView.findViewById(R.id.title);
        final DSEdittext emails = promptView.findViewById(R.id.emails);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyboardUtils.hideSoftKeyboard(emails, getActivity());
                send.setEnabled(true);
                send.setClickable(true);
                alertD.dismiss();
            }
        });


        sendEmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                sendEmail.setEnabled(false);
                sendEmail.setClickable(false);
                String[] array = emails.getText().toString().trim().split(",");
                List<String> emailsList = new ArrayList<>();
                for (String s : array) {
                    emailsList.add(s.trim());
                }
//                emailsList = Arrays.asList(array);

                if (emails.getText().toString().trim().length() > 0 && validateEmails(emailsList)) {
                    final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Sending email...");
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    sendIncompleteEmail.setEmails(emailsList);
                    KeyboardUtils.hideSoftKeyboard(emails, getActivity());
                    alertD.dismiss();
                    RetrofitInitialization.getDs_services().sendIncompleteDealEmail("bearer " + PreferenceManger.getStringValue(AppConstant.ACCESS_TOKEN), sendIncompleteEmail).enqueue(new Callback<ImageUploadResponse>() {
                        @Override
                        public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse> response) {
                            progressDialog.dismiss();
                            if (response.code() == 200 && response.isSuccessful()) {
                                if (response.body().getMessage() != null && response.body().getMessage().length() > 0)
                                    new CustomToast(getActivity()).alert(response.body().getMessage());
                                else {
                                    new CustomToast(getActivity()).alert("Email sent!");
                                }
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        goBack();
                                    }
                                }, 2900);
                            }else {
                                sendEmail.setEnabled(true);
                                sendEmail.setClickable(true);
                                send.setEnabled(true);
                                send.setClickable(true);
                            }
                        }

                        @Override
                        public void onFailure(Call<ImageUploadResponse> call, Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                } else {
                    new CustomToast(getActivity()).alert("Please enter valid emails.");
                }


            }
        });


        alertD.setView(promptView);

        alertD.show();
    }

    private boolean validateEmails(List<String> emailsList) {
        final String emailPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        for (int i = 0; i < emailsList.size(); i++) {
            if (!emailsList.get(i).trim().matches(emailPattern)) {
                return false;
            }
        }
        return true;
    }

    private void initViews() {

          send = mView.findViewById(R.id.text_next);
        send.setText("Proceed");

        DSTextView back = mView.findViewById(R.id.text_back_up);

        DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView title = mView.findViewById(R.id.history);
        text_cancelBack.setVisibility(View.INVISIBLE);
        DSTextView logout = mView.findViewById(R.id.txt_logout);
        Gson gson = new GsonBuilder().create();
        String id=getArguments().getString("dlnumber","");
//        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (recordData.where(id).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(id).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//        {
            title.setText("Vehicle Information");
//        } else {
//            title.setText("Vehicle History Disclosure");
//        }

        LinearLayout container1 = mView.findViewById(R.id.container1);
        LinearLayout container2 = mView.findViewById(R.id.container2);
        LinearLayout container3 = mView.findViewById(R.id.container3);
        LinearLayout container4 = mView.findViewById(R.id.container4);
        LinearLayout container5 = mView.findViewById(R.id.container5);

        container1.setVisibility(View.GONE);
        container2.setVisibility(View.GONE);
        container3.setVisibility(View.GONE);
        container4.setVisibility(View.GONE);

        final CheckBox checkBox = mView.findViewById(R.id.ckBox1);
        final CheckBox checkBox1 = mView.findViewById(R.id.ckBox2);
        final CheckBox checkBox2 = mView.findViewById(R.id.ckBox3);
        final CheckBox checkBox3 = mView.findViewById(R.id.ckBox4);
        final CheckBox checkBox4 = mView.findViewById(R.id.ckBox5);
        checkBox.setChecked(true);
        final boolean[] isChecked1 = new boolean[1];
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isChecked1[0] = isChecked;
            }
        });
        final String vtid = getArguments().getString("tradeId");
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send.setEnabled(false);
                send.setClickable(false);
//                if (isChecked1[0]) {
                ArrayList<String> arrayList = new ArrayList<>();
                arrayList.add("VIMG");
                SendIncompleteEmail sendIncompleteEmail = new SendIncompleteEmail();
                sendIncompleteEmail.setCustomerVehicleTradeId(vtid);
                sendIncompleteEmail.setReportCodes(arrayList);
                showDialog(sendIncompleteEmail);
//                } else {
//                    new CustomToast(getActivity()).alert("Please select the option.");
//                }

            }
        });

        container1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                } else {
                    checkBox.setChecked(true);
                }
            }
        });
        container2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox1.isChecked()) {
                    checkBox1.setChecked(false);
                } else {
                    checkBox1.setChecked(true);
                }
            }
        });
        container3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox2.isChecked()) {
                    checkBox2.setChecked(false);
                } else {
                    checkBox2.setChecked(true);
                }
            }
        });
        container4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox3.isChecked()) {
                    checkBox3.setChecked(false);
                } else {
                    checkBox3.setChecked(true);
                }
            }
        });
        container5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox4.isChecked()) {
                    checkBox4.setChecked(false);
                } else {
                    checkBox4.setChecked(true);
                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

    }
}
