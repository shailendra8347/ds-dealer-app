package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.screenRevamping.fragment.FragmentNoVerbalPromises;
import com.volga.dealershieldtablet.screenRevamping.fragment.FragmentSalesPersonDropDown;
import com.volga.dealershieldtablet.screenRevamping.fragment.ProofOfInsurence;
import com.volga.dealershieldtablet.screenRevamping.fragment.ReviewEditDisclosures;
import com.volga.dealershieldtablet.screenRevamping.fragment.SkipDisclosuresFragment;
import com.volga.dealershieldtablet.screenRevamping.fragment.TabletSelection;
import com.volga.dealershieldtablet.ui.additionalImages.AdditionalImageFragment;
import com.volga.dealershieldtablet.ui.additionalImages.FragmentAddMoreImages;
import com.volga.dealershieldtablet.ui.coBuyer.FragmentCoBuyerHandover;
import com.volga.dealershieldtablet.ui.coBuyer.FragmentIsCoBuyer;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarMileageEdit;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarMileagePicture;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureFront;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureLeft;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureRear;
import com.volga.dealershieldtablet.ui.fragment.FragmentCarPictureRight;
import com.volga.dealershieldtablet.ui.fragment.FragmentConditionDisclosure;
import com.volga.dealershieldtablet.ui.fragment.FragmentConfirmCarImages;
import com.volga.dealershieldtablet.ui.fragment.FragmentConfirmVehicleDetails;
import com.volga.dealershieldtablet.ui.fragment.FragmentContactInfo;
import com.volga.dealershieldtablet.ui.fragment.FragmentCustomerHandover1;
import com.volga.dealershieldtablet.ui.fragment.FragmentDMVPreview;
import com.volga.dealershieldtablet.ui.fragment.FragmentFinalReport;
import com.volga.dealershieldtablet.ui.fragment.FragmentFinanceScreen;
import com.volga.dealershieldtablet.ui.fragment.FragmentImagePreview;
import com.volga.dealershieldtablet.ui.fragment.FragmentLicenseInfoConfirmation;
import com.volga.dealershieldtablet.ui.fragment.FragmentLicenseInformation;
import com.volga.dealershieldtablet.ui.fragment.FragmentReceivedPrint;
import com.volga.dealershieldtablet.ui.fragment.FragmentRemoveStickers;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanDMVBack;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanDMVFront;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanVINSticker;
import com.volga.dealershieldtablet.ui.fragment.FragmentSelectLanguage;
import com.volga.dealershieldtablet.ui.fragment.FragmentTestDrive;
import com.volga.dealershieldtablet.ui.fragment.FragmentThankYouUser1;
import com.volga.dealershieldtablet.ui.fragment.FragmentThankYouUser2;
import com.volga.dealershieldtablet.ui.fragment.FragmentTypeOfPurchase;
import com.volga.dealershieldtablet.ui.fragment.FragmentUsedVehicleDisclosure;
import com.volga.dealershieldtablet.ui.fragment.FragmentUsedVehicleHistory;
import com.volga.dealershieldtablet.ui.fragment.FragmentVehicleInfo;
import com.volga.dealershieldtablet.ui.fragment.FragmentVehicleType;
import com.volga.dealershieldtablet.ui.fragment.FragmentWindowSticker;
import com.volga.dealershieldtablet.ui.fragment.UsedVehicleHistoryAfterSticker;
import com.volga.dealershieldtablet.ui.tradeIn.FragmentIsTradeIn;
import com.volga.dealershieldtablet.utils.AppConstant;

/**
 * Created by ${Shailendra} on 24-07-2019.
 */
public class WebDealViewPagerAdapter extends FragmentStatePagerAdapter {


    public WebDealViewPagerAdapter(FragmentManager supportFragmentManager, SingleDirectionViewPager mViewPager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            FragmentVehicleType nextFragment = new FragmentVehicleType();
            Bundle bundle = new Bundle();
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 1) {
            return new FragmentScanVINSticker();
        } else if (position == 2) {
            return new FragmentVehicleInfo();
        } else if (position == 3) {
            return new FragmentCarPictureFront();
        } else if (position == 4) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_FRONT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 5) {
            return new FragmentCarPictureRight();
        } else if (position == 6) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_RIGHT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 7) {
            return new FragmentCarPictureRear();
        } else if (position == 8) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_REAR);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 9) {
            return new FragmentCarPictureLeft();
        } else if (position == 10) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_LEFT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 11) {
            return new FragmentWindowSticker();
        } else if (position == 12) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_WINDOW_VIN);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 13) {
            return new FragmentAddMoreImages();
        }
        /*Add more images logic is here*/
        else if (position == 14) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "1");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 15) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL1);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 16) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "2");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 17) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL2);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 18) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "3");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 19) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL3);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 20) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "4");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 21) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL4);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 22) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "5");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 23) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL5);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 24) {
            SkipDisclosuresFragment nextFragment = new SkipDisclosuresFragment();
            Bundle bundle = new Bundle();
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 25) {
            FragmentConditionDisclosure history = new FragmentConditionDisclosure();
            Bundle bundle = new Bundle();
            bundle.putBoolean("first", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 26) {
            FragmentUsedVehicleHistory history = new FragmentUsedVehicleHistory();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 27) {
            FragmentUsedVehicleHistory history = new FragmentUsedVehicleHistory();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 28) {
            FragmentUsedVehicleDisclosure nextFragment = new FragmentUsedVehicleDisclosure();
            Bundle bundle = new Bundle();
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 29) {
            FragmentSalesPersonDropDown nextFragment = new FragmentSalesPersonDropDown();
            Bundle bundle = new Bundle();
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 30) {
            return new FragmentCarMileagePicture();
        } else if (position == 31) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_MILEAGE);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 32) {
            return new FragmentCarMileageEdit();
        } else if (position == 33) {
            return new FragmentIsTradeIn();
        } else if (position == 34) {
            return new FragmentScanVINSticker();
        } else if (position == 35) {
            return new FragmentVehicleInfo();
        } else if (position == 36) {
            return new FragmentCarPictureFront();
        } else if (position == 37) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_FRONT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 38) {
            return new FragmentWindowSticker();
        } else if (position == 39) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_WINDOW_VIN);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 40) {
            return new FragmentCarMileagePicture();
        } else if (position == 41) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_MILEAGE);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 42) {
            return new FragmentCarMileageEdit();
        } else if (position == 43) {
            return new FragmentAddMoreImages();
        }
        /*Add more images logic is here*/
        else if (position == 44) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "1");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 45) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL1);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 46) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "2");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 47) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL2);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 48) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "3");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 49) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL3);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 50) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "4");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 51) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL4);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 52) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "5");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 53) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.CAR_ADDITIONAL5);
            bundle.putBoolean("isAdditional", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 54) {
            FragmentScanDMVFront nextFragment = new FragmentScanDMVFront();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", false);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 55) {
            FragmentDMVPreview nextFragment = new FragmentDMVPreview();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", false);
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.DMV_FRONT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 56) {
            FragmentScanDMVBack nextFragment = new FragmentScanDMVBack();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", false);
            nextFragment.setArguments(bundle);
            return nextFragment;
//            return new FragmentScanDMVBack();
        } else if (position == 57) {
            FragmentLicenseInformation nextFragment = new FragmentLicenseInformation();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", false);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 58) {
            return new FragmentLicenseInfoConfirmation();
        } else if (position == 59) {
            return new FragmentIsCoBuyer();
        } else if (position == 60) {
            return new TabletSelection();
        } else if (position == 61) {
            return new FragmentFinanceScreen();
        } else if (position == 62) {
            return new FragmentFinanceScreen();
        } else if (position == 63) {
            return new ReviewEditDisclosures();
        } else if (position == 64) {
            return new ProofOfInsurence();
        } else if (position == 65) {
            AdditionalImageFragment additionalImageFragment = new AdditionalImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString("pageNumber", "insurance");
            additionalImageFragment.setArguments(bundle);
            return additionalImageFragment;
        } else if (position == 66) {
            FragmentImagePreview nextFragment = new FragmentImagePreview();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.INSURANCE);
            bundle.putBoolean("isAdditional", false);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 67) {
            return new FragmentCustomerHandover1();
        } else if (position == 68) {
            return new FragmentSelectLanguage();
        } else if (position == 69) {
            return new FragmentTypeOfPurchase();
        } else if (position == 70) {
            return new FragmentConfirmCarImages();
        } else if (position == 71) {
            return new FragmentConfirmVehicleDetails();
        } else if (position == 72) {
            return new FragmentTestDrive();
        } else if (position == 73) {
            return new FragmentRemoveStickers();
        } else if (position == 74) {
            FragmentConditionDisclosure history = new FragmentConditionDisclosure();
            Bundle bundle = new Bundle();
            bundle.putBoolean("first", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 75) {
            UsedVehicleHistoryAfterSticker history = new UsedVehicleHistoryAfterSticker();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 76) {
            UsedVehicleHistoryAfterSticker history = new UsedVehicleHistoryAfterSticker();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 77) {
            return new FragmentNoVerbalPromises();
        } else if (position == 78) {
            return new FragmentThankYouUser1();
        } else if (position == 79) {
            return new FragmentFinalReport();
        } else if (position == 80) {
            return new FragmentReceivedPrint();
        } else if (position == 81) {
            FragmentContactInfo nextFragment = new FragmentContactInfo();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", false);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 82) {
            return new FragmentCoBuyerHandover();
        } else if (position == 83) {
            return new FragmentThankYouUser2();
        }
        return null;

    }

    @Override
    public int getCount() {
        return 84;
    }

    @Override
    public void finishUpdate(@NonNull ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

}