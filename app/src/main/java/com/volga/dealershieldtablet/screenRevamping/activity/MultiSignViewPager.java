package com.volga.dealershieldtablet.screenRevamping.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.interfaceCallback.GoBackNextInterface;
import com.volga.dealershieldtablet.screenRevamping.adapter.MultiSignViewPagerAdapter;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.SwipeDirection;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;


public class MultiSignViewPager extends AppCompatActivity implements GoBackNextInterface {

    public static int currentPage = 0;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private SingleDirectionViewPager mViewPager;
    private boolean doubleTap=false;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        MultiSignViewPager.currentPage = currentPage;
        mViewPager.setCurrentItem(MultiSignViewPager.currentPage, true);
    }

    public SingleDirectionViewPager getViewPager() {
        return mViewPager;
    }

    String name, tpID;
    boolean isCoBuyer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_pager);
        mViewPager = findViewById(R.id.viewPager);
        mViewPager.setAllowedSwipeDirection(SwipeDirection.none);
        name = getIntent().getStringExtra("url");
        tpID = getIntent().getStringExtra("id");
        isCoBuyer = getIntent().getBooleanExtra("isCoBuyer", false);

        boolean showBox = false;
        Gson gson = new Gson();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        ArrayList<String> byteList = new ArrayList<>();
        ArrayList<ThirdPartyObj> listLangName = new ArrayList<>();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null)
            listLangName = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
        for (int i = 0; i < listLangName.size(); i++) {
            if (listLangName.get(i).getId().equalsIgnoreCase(tpID)) {
                byteList.addAll(listLangName.get(i).getThirdPartyList().getMessage());
                showBox=listLangName.get(i).isMandatory();
            }
        }
        byteList.add(byteList.size(), " ");

        MultiSignViewPagerAdapter mSectionsPagerAdapter = new MultiSignViewPagerAdapter(getSupportFragmentManager(), byteList, name, isCoBuyer, tpID,showBox);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int oldPos = mViewPager.getCurrentItem();
                Log.i("onPageScrolled", "Page No: " + mViewPager.getCurrentItem());
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.i("onPageSclStateChanged", "Page No: " + mViewPager.getCurrentItem() + " State " + state);
            }
        });
        mViewPager.setAdapter(mSectionsPagerAdapter);
        if (getIntent() != null) {
            currentPage = getIntent().getIntExtra("page", 0);
        }

    }

    public File getOutputFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    @Override
    protected void onResume() {
        super.onResume();
//        PushLink.setCurrentActivity(this);
//        setCustomData();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(broadCastNewMessage);
//        unregisterReceiver(receiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(DSAPP.getInstance())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }


    public File getOutputMediaFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;


        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        String mImageName = imageName + ".pdf";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    @Override
    public void whatNextClick() {
        new Handler().postDelayed(() -> doubleTap = false, 2000);
        if (!doubleTap) {
            doubleTap=true;
            currentPage++;
            if (currentPage == mViewPager.getAdapter().getCount()) {
                finish();
            } else {
                mViewPager.setCurrentItem(currentPage, true);
            }
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);

    }

    public void showFragmentInLandscape() {
        Activity a = MultiSignViewPager.this;
        a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public void showSensorOrientation() {
        Activity a = MultiSignViewPager.this;
        a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }

    public void showFragmentInPortrait() {
        Activity a = MultiSignViewPager.this;
        a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void goToBackIndex() {
        new Handler().postDelayed(() -> doubleTap = false, 2000);
        if (!doubleTap) {
            doubleTap = true;
            Timber.tag("page").e("+%s", currentPage);
            if (currentPage <= 0) {
                finish();
            } else {
                currentPage--;
                mViewPager.setCurrentItem(currentPage, true);
            }
        }
    }

    protected ArrayList<ImagesSynced> makeHashMapOfImages(Record record) {
        ArrayList<ImagesSynced> imagesSynceds = new ArrayList<>();
        String path_licence, path_signature, path_car, path_user_pic, path_screenshots;
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";
        path_user_pic = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/UserPic";
        path_screenshots = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Screenshot";


        if (new File(path_car + "/IMG_additional1.jpg").exists()) {
            if (record.getVehicleDetails().getTitle1() == null) {
                record.getVehicleDetails().setTitle1("Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional1.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "+/IMG_Additional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_additional2.jpg").exists()) {
            if (record.getVehicleDetails().getTitle2() == null) {
                record.getVehicleDetails().setTitle2("Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional2.jpg"));
            synced.setName(record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_additional3.jpg").exists()) {
            if (record.getVehicleDetails().getTitle3() == null) {
                record.getVehicleDetails().setTitle3("Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional3.jpg"));
            synced.setName(record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_additional4.jpg").exists()) {
            if (record.getVehicleDetails().getTitle4() == null) {
                record.getVehicleDetails().setTitle4("Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_additional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_additional5.jpg").exists()) {
            if (record.getVehicleDetails().getTitle5() == null) {
                record.getVehicleDetails().setTitle5("Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_additional5.jpg"));
            synced.setName(record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_Additional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getVehicleDetails().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.additionalImage) : record.getVehicleDetails().getTitle5(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional1.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle1() == null) {
                record.getTradeInVehicle().setTitle1("Trade In Additional Image 1");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional1.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional1.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle1().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle1(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional2.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle2() == null) {
                record.getTradeInVehicle().setTitle2("Trade In Additional Image 2");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional2.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional2.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle2().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle2(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional3.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle3() == null) {
                record.getTradeInVehicle().setTitle3("Trade In Additional Image 3");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional3.jpg"));
            synced.setName(record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3());
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional3.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle3().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle3(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional4.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle4() == null) {
                record.getTradeInVehicle().setTitle4("Trade In Additional Image 4");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional4.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional4.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle4().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle4(), record);
        }
        if (new File(path_car + "/IMG_TradeInAdditional5.jpg").exists()) {
            if (record.getTradeInVehicle().getTitle5() == null) {
                record.getTradeInVehicle().setTitle5("Trade In Additional Image 5");
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setName(record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5());
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_TradeInAdditional5.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_TradeInAdditional5.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), record.getTradeInVehicle().getTitle5().equalsIgnoreCase("Tap to edit title") ? getString(R.string.tradeInAdditionalImage) : record.getTradeInVehicle().getTitle5(), record);
        }

        /*Car Images*/
        if (new File(path_car + "/IMG_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();

            synced.setName("FRONTIMG");
            synced.setFile(new File(path_car + "/IMG_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_front.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "FRONTIMG", record);
        }
        if (new File(path_car + "/IMG_right.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("RIGHTIMG");
            synced.setSynced(false);
            synced.setFile(new File(path_car + "/IMG_right.jpg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_right.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "RIGHTIMG", record);
        }
        if (new File(path_car + "/IMG_rear.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("REARIMG");
            synced.setFile(new File(path_car + "/IMG_rear.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_rear.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "REARIMG", record);
        }
        if (new File(path_car + "/IMG_left.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LEFTIMG");
            synced.setFile(new File(path_car + "/IMG_left.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_left.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LEFTIMG", record);
        }
        if (new File(path_car + "/IMG_sticker.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("WINDOWWSTICKERIMG");
            synced.setFile(new File(path_car + "/IMG_sticker.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);

//            startImageSync(new File(path_car + "/IMG_sticker.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "WINDOWWSTICKERIMG", record);
        }
        if (new File(path_car + "/IMG_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("MILEAGEIMG");
            synced.setFile(new File(path_car + "/IMG_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "MILEAGEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TRADEINMILEAGE");
            synced.setFile(new File(path_car + "/IMG_trade_mileage.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_trade_mileage.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "TRADEINMILEAGE", record);
        }
        if (new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("LICENCEPLATENUMBER");
            synced.setFile(new File(path_car + "/IMG_licencePlate.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_licencePlate.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "LICENCEPLATENUMBER", record);
        }
        /*Additional Images*/

        if (new File(path_car + "/IMG_insurance.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("Customer_Insurance");
            synced.setFile(new File(path_car + "/IMG_insurance.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_car + "/IMG_insurance.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "Customer_Insurance", record);
        }
        //Add all signature image here
        if (new File(path_signature + "/IMG_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_language_selection.svg"));
            synced.setName("language_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_1.svg"));
            synced.setName("third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_1.svg"));
            synced.setName("co_buyer_third_party_sign_CarFax_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }
        if (new File(path_signature + "/IMG_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_third_party_sign_2.svg"));
            synced.setName("third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_third_party_sign_2.svg"));
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "language_selection", record);
        }

        if (new File(path_signature + "/IMG_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_type_of_purchase_selection.svg"));
            synced.setName("type_of_purchase_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_confirm_car_images_selection.svg"));
            synced.setName("confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_test_drive_taken_selection.svg"));
            synced.setName("test_drive_taken_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"));
            synced.setName("no_test_drive_confirm_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_remove_stickers_selection.svg"));
            synced.setName("remove_stickers_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_licence + "/IMG_contact_info.svg"));
            synced.setName("CONTACTINFOSIGNATUREIMG");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "CONTACTINFOSIGNATUREIMG", record);
        }
        if (new File(path_signature + "/IMG_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_prints_recieved_selection.svg"));
            synced.setName("prints_recieved_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure");
            synced.setFile(new File(path_signature + "/IMG_history_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_history_report.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_history_report.svg"));
            synced.setName("history_report");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "history_report", record);
        }
        if (new File(path_signature + "/IMG_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure");
            synced.setFile(new File(path_signature + "/IMG_condition_disclosure.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_language_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_language_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_language_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_language_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_language_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_type_of_purchase_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_type_of_purchase_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_type_of_purchase_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"));
            synced.setName("co_buyer_confirm_car_images_selection");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_images_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_images_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_confirm_car_details_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_confirm_car_details_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_confirm_car_details_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_test_drive_taken_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_test_drive_taken_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_test_drive_taken_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_no_test_drive_confirm_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_no_test_drive_confirm_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_no_test_drive_confirm_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_remove_stickers_selection");
            synced.setSynced(false);
            synced.setFile(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"));
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_remove_stickers_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_remove_stickers_selection", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_contact_info.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_contact_info");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_contact_info.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_contact_info.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_contact_info", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_prints_recieved_selection");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_prints_recieved_selection.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_prints_recieved_selection", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"));
            synced.setName("co_buyer_condition_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_condition_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_condition_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_report.svg").exists()) {

            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_history_report");
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_report.svg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_report.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_report", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_history_disclosure.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"));
            synced.setName("co_buyer_history_disclosure");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_no_verble_promise.svg"));
            synced.setName("buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_no_verble_promise.svg"));
            synced.setName("co_buyer_no_verble_promise");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_co_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_co_buyer_checklist.svg"));
            synced.setName("co_buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_signature + "/IMG_buyer_checklist.svg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(new File(path_signature + "/IMG_buyer_checklist.svg"));
            synced.setName("buyer_checklist");
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_signature + "/IMG_co_buyer_history_disclosure.svg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_history_disclosure", record);
        }
        if (new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_DLFRONT");
            synced.setFile(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "co_buyer_DLFRONT", record);
        }
        if (new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("DRIVERLICENSEIMG");
            synced.setFile(new File(path_licence + "/IMG_DLFRONT.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }
        if (new File(path_car + "/IMG_trade_front.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("TradeInFront");
            synced.setFile(new File(path_car + "/IMG_trade_front.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
//            startImageSync(new File(path_licence + "/IMG_DLFRONT.jpg"), record.getSettingsAndPermissions().getCustomerVehicleTradeId(), "DRIVERLICENSEIMG", record);
        }

        /*Buyers screenshots*/
        if (new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_co_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_co_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_remote_deal_buyer_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_remote_deal_buyer_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_safety_recall.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_safety_recall");
            synced.setFile(new File(path_screenshots + "/IMG_alert_safety_recall.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_warning_disclosures");
            synced.setFile(new File(path_screenshots + "/IMG_alert_warning_disclosures.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_condition_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_condition_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_report_fetching_error_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_report_fetching_error_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_finance_page_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_finance_page_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_co_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_co_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_cf");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_cf.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_history_response_report_fetching_error_buyer_ac");
            synced.setFile(new File(path_screenshots + "/IMG_alert_history_response_report_fetching_error_buyer_ac.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_vehicle_info_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_vehicle_info_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("alert_co_buyer_third_party_report_page");
            synced.setFile(new File(path_screenshots + "/IMG_alert_co_buyer_third_party_report_page.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        if (new File(path_screenshots + "/IMG_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers screenshots*/

        if (new File(path_screenshots + "/IMG_co_language_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_language_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_purchase_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_purchase_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_screenshots + "/IMG_co_car_images_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_images_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_car_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_car_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_test_drive_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_remove_sticker_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_condition_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_history_disclosure_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_third_party_report_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_no_verbal_promise_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_check_list_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_check_list_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_print_receive_selection_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_screenshots + "/IMG_co_contact_info_screen.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen");
            synced.setFile(new File(path_screenshots + "/IMG_co_contact_info_screen.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }


        /*Buyers pictures*/
        if (new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_CARFAX_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_CARFAX_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_buyer_third_party_sign_AutoCheck_Report_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_buyer_third_party_sign_AutoCheck_Report_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        /*Co-Buyers picture*/

        if (new File(path_user_pic + "/IMG_co_language_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_language_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_language_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_purchase_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_purchase_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        if (new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_images_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_images_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_car_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_car_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_test_drive_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_test_drive_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_remove_sticker_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_remove_sticker_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_condition_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_condition_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_history_disclosure_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_history_disclosure_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_third_party_report_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_third_party_report_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_no_verbal_promise_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_no_verbal_promise_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_check_list_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_check_list_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_print_receive_selection_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_print_receive_selection_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }
        if (new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg").exists()) {
            ImagesSynced synced = new ImagesSynced();
            synced.setName("co_contact_info_screen_pic");
            synced.setFile(new File(path_user_pic + "/IMG_co_contact_info_screen_pic.jpg"));
            synced.setSynced(false);
            imagesSynceds.add(synced);
        }

        return imagesSynceds;
    }


    @Override
    public void onBackPressed() {
        currentPage--;
        if (currentPage == 0) {
            finish();
        } else {
            mViewPager.setCurrentItem(currentPage--, true);
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

}
