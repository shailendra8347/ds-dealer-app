package com.volga.dealershieldtablet.screenRevamping.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.CustomToast;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.io.File;

import static com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager.restrictedPage;

/**
 * Created by ${Shailendra} on 14-08-2018.
 */
public class TabletSelection extends BaseFragment {
    private IGoToNextPage iGoToNextPage;
    private View mView;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showFragmentInPortrait();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.tablet_selection, container, false);
        initView();
        return mView;
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        View next = mView.findViewById(R.id.text_next);
        DSTextView back = mView.findViewById(R.id.text_back_up);
        final AppCompatButton continueDeal = mView.findViewById(R.id.continue_deal);
        final AppCompatButton preppedDeal = mView.findViewById(R.id.prepped_deal);
        continueDeal.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
//                continueDeal.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
//                preppedDeal.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
//                continueDeal.setTextColor(getActivity().getColor(R.color.white));
//                preppedDeal.setPadding(20, 20, 20, 20);
//                continueDeal.setPadding(20, 20, 20, 20);
                continueDeal.setEnabled(false);
                continueDeal.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        continueDeal.setEnabled(true);
                        continueDeal.setClickable(true);
                    }
                }, 6000);
                cancel();
            }
        });
        continueDeal.setText(Html.fromHtml(getString(R.string.differ_tab)));
        preppedDeal.setText(Html.fromHtml(getString(R.string.same_tab)));
        preppedDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iGoToNextPage.whatNextClick();
//                preppedDeal.setTextColor(getActivity().getColor(R.color.white));
//                preppedDeal.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_appcolor));
//                continueDeal.setBackground(getActivity().getDrawable(R.drawable.rounded_corner_white));
//                preppedDeal.setPadding(20, 20, 20, 20);
//                continueDeal.setPadding(20, 20, 20, 20);
//                preppedDeal.setEnabled(false);
//                preppedDeal.setClickable(false);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        preppedDeal.setEnabled(true);
//                        preppedDeal.setClickable(true);
//                    }
//                }, 6000);

            }
        });
        next.setVisibility(View.GONE);
//        DSTextView disclosure = mView.findViewById(R.id.disclosure);
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        String newOrUsed;
//        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getTypeOfVehicle().equalsIgnoreCase("New"))
//            newOrUsed = "";
//        else
//            newOrUsed = "USED";
//        disclosure.setText(String.format(getString(R.string.used_vehicle_disclosure), newOrUsed));
//        disclosure.setText("Hand to Finance Department");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (checkMandatoryImage())
                iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Gson gson = new GsonBuilder().create();
                RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isHasCoBuyer()) {
                    recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setCoBuyer(true);
                    String recordDataString = gson.toJson(recordData);
                    PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
                    if (60 == restrictedPage) {
                        new CustomToast(getActivity()).alert("You can not edit prepped deal. Please continue.");
                        return;
                    }
                    ((NewDealViewPager) getActivity()).setCurrentPage(58);
                } else {
                    iGoToNextPage.goToBackIndex();
                }

//                if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                    ((ViewPagerActivity) getActivity()).setCurrentPage(57);
//                } else
//                    iGoToNextPage.goToBackIndex();
//                showFragmentInLandscape();

            }
        });
//        DSTextView logout = mView.findViewById(R.id.txt_logout);
        final DSTextView cancel = mView.findViewById(R.id.text_cancelBack);
        cancel.setVisibility(View.INVISIBLE);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                logout();
//            }
//        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    private boolean checkMandatoryImage() {

        boolean allTrue = true;
        String path_licence, path_signature, path_car;
        Gson gson = new GsonBuilder().create();
        final RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        path_licence = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/LicenseFront";
        path_car = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/CarImages";
        path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + record.getCustomerDetails().getDLNumber() + "/Signatures";

        if (!new File(path_licence + "/IMG_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Buyer's DMV Front Image is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasCoBuyer() && !new File(path_licence + "/IMG_co_buyer_DLFRONT.jpg").exists()) {
            new CustomToast(getActivity()).alert("Co-Buyer's DMV Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_front.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Front Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_right.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Passengers side Image is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_rear.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Back Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_left.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Left Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_sticker.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Addendum Sticker/Buyers Guide Image side is missing.");
            return false;
        } else if (!new File(path_car + "/IMG_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Vehicle Odometer Image side is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_trade_mileage.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Odometer Image side is missing.");
            return false;
        } else if (record.getSettingsAndPermissions().isHasTradeIn() && !new File(path_car + "/IMG_licencePlate.jpg").exists()) {
            new CustomToast(getActivity()).alert("Trade-In Vehicle Licence Plate Image side is missing.");
            return false;
        } else {

            return true;
        }
    }
}
