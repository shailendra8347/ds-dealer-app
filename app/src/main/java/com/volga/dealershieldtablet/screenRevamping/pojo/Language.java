package com.volga.dealershieldtablet.screenRevamping.pojo;

public class Language
{
    private String DisplayOrder;

    private String IsForAll;

    private String DisplayName;

    private String Published;

    private String LanguageName;

    private String Code;

    private String LanguageId;

    public String getDisplayOrder ()
    {
        return DisplayOrder;
    }

    public void setDisplayOrder (String DisplayOrder)
    {
        this.DisplayOrder = DisplayOrder;
    }

    public String getIsForAll ()
    {
        return IsForAll;
    }

    public void setIsForAll (String IsForAll)
    {
        this.IsForAll = IsForAll;
    }

    public String getDisplayName ()
    {
        return DisplayName;
    }

    public void setDisplayName (String DisplayName)
    {
        this.DisplayName = DisplayName;
    }

    public String getPublished ()
    {
        return Published;
    }

    public void setPublished (String Published)
    {
        this.Published = Published;
    }

    public String getLanguageName ()
    {
        return LanguageName;
    }

    public void setLanguageName (String LanguageName)
    {
        this.LanguageName = LanguageName;
    }

    public String getCode ()
    {
        return Code;
    }

    public void setCode (String Code)
    {
        this.Code = Code;
    }

    public String getLanguageId ()
    {
        return LanguageId;
    }

    public void setLanguageId (String LanguageId)
    {
        this.LanguageId = LanguageId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DisplayOrder = "+DisplayOrder+", IsForAll = "+IsForAll+", DisplayName = "+DisplayName+", Published = "+Published+", LanguageName = "+LanguageName+", Code = "+Code+", LanguageId = "+LanguageId+"]";
    }
}