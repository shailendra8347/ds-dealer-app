package com.volga.dealershieldtablet.screenRevamping.pojo;

public class CheckListTranslation
{
    private String DestinationLanguageId;

    private Language Language;

    private String Text;

    private String Id;

    private String CheckListId;

    public String getDestinationLanguageId ()
    {
        return DestinationLanguageId;
    }

    public void setDestinationLanguageId (String DestinationLanguageId)
    {
        this.DestinationLanguageId = DestinationLanguageId;
    }

    public Language getLanguage ()
    {
        return Language;
    }

    public void setLanguage (Language Language)
    {
        this.Language = Language;
    }

    public String getText ()
    {
        return Text;
    }

    public void setText (String Text)
    {
        this.Text = Text;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getCheckListId ()
    {
        return CheckListId;
    }

    public void setCheckListId (String CheckListId)
    {
        this.CheckListId = CheckListId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DestinationLanguageId = "+DestinationLanguageId+", Language = "+Language+", Text = "+Text+", Id = "+Id+", CheckListId = "+CheckListId+"]";
    }
}