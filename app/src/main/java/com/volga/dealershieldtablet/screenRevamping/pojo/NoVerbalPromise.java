package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.ArrayList;

public class NoVerbalPromise
{
    private ArrayList<VerbalPromise> VerbalPromise;

    public ArrayList<VerbalPromise> getVerbalPromise ()
    {
        return VerbalPromise;
    }

    public void setVerbalPromise (ArrayList<VerbalPromise> VerbalPromise)
    {
        this.VerbalPromise = VerbalPromise;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [VerbalPromise = "+VerbalPromise+"]";
    }
}