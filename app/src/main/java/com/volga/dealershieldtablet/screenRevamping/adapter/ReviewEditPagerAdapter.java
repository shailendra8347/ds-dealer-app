package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.ui.fragment.FragmentConditionDisclosure;
import com.volga.dealershieldtablet.ui.fragment.FragmentUsedVehicleHistory;

/**
 * Created by ${Shailendra} on 24-07-2019.
 */
public class ReviewEditPagerAdapter extends FragmentStatePagerAdapter {
    private final SingleDirectionViewPager mViewPager;


    public ReviewEditPagerAdapter(FragmentManager supportFragmentManager, SingleDirectionViewPager mViewPager) {
        super(supportFragmentManager);
        this.mViewPager = mViewPager;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            FragmentConditionDisclosure history = new FragmentConditionDisclosure();
            Bundle bundle = new Bundle();
            bundle.putBoolean("first", true);
            history.setArguments(bundle);
            return history;
        } else if (position == 1) {
            FragmentUsedVehicleHistory history = new FragmentUsedVehicleHistory();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", false);
            history.setArguments(bundle);
            return history;
        } else if (position == 2) {
            FragmentUsedVehicleHistory history = new FragmentUsedVehicleHistory();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isAutoCheck", true);
            history.setArguments(bundle);
            return history;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

}