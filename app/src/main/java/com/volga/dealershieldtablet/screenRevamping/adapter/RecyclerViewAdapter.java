package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.screenRevamping.pojo.VehicleImagePojo;
import com.volga.dealershieldtablet.ui.fragment.ImageFragmentDialog;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private ArrayList<VehicleImagePojo> imagePojoArrayList;
    private FragmentActivity mcontext;
    boolean incompleteDeal;
    FragmentTransaction beginTransaction;

//    public RecyclerViewAdapter(ArrayList<VehicleImagePojo> recyclerDataArrayList, Context mcontext, boolean incompleteDeal) {
//        this.imagePojoArrayList = recyclerDataArrayList;
//        this.mcontext = mcontext;
//        this.incompleteDeal = incompleteDeal;
//    }

    public RecyclerViewAdapter(ArrayList<VehicleImagePojo> imagePojoArrayList, FragmentActivity activity, boolean isIncomplete, FragmentTransaction beginTransaction) {
        this.imagePojoArrayList = imagePojoArrayList;
        this.mcontext = activity;
        this.incompleteDeal = isIncomplete;
        this.beginTransaction = beginTransaction;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate Layout
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_row, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        // Set the data to textview and imageview.
        VehicleImagePojo recyclerData = imagePojoArrayList.get(position);
        holder.name.setText(recyclerData.getName());
//        holder.carImage.setImageResource(recyclerData.getImgid());
        if (incompleteDeal) {
            if (recyclerData.getUrl() != null && !recyclerData.getUrl().isEmpty()) {
                loadImageToImageView(recyclerData.getUrl(), holder.carImage);
                holder.carImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(recyclerData.getUrl(), recyclerData.getName());
                    }
                });
            } else {
                holder.carImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(recyclerData.getUri().toString());
                    }
                });

                loadImageToImageView(recyclerData.getUri(), holder.carImage);
            }
        } else {
            holder.carImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog(recyclerData.getUri().toString());
                }
            });

            loadImageToImageView(recyclerData.getUri(), holder.carImage);
        }

    }

    @Override
    public int getItemCount() {
        // this method returns the size of recyclerview
        return imagePojoArrayList.size();
    }

    void showDialog(String uri) {
        FragmentTransaction ft = mcontext.getSupportFragmentManager().beginTransaction();
        ;
        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", uri);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    void showDialog(String uri, String name) {
        FragmentTransaction ft = mcontext.getSupportFragmentManager().beginTransaction();
        ;

        DialogFragment newFragment = new ImageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("url", uri);
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
    }

    private void loadImageToImageView(Uri carImages, ImageView imageView) {
//        if (carImages != null) {
        imageView.setVisibility(View.VISIBLE);
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(mcontext)
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
//        } else {
//            imageView.setVisibility(View.GONE);
////            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.no_img_pic).error(R.drawable.car_placeholder);
////            Glide.with(getActivity())
////                    .load(carImages)
////                    .apply(requestOptions)
////                    .into(imageView);
//        }

    }

    private void loadImageToImageView(String carImages, ImageView imageView) {
//        if (new File(carImages).exists()) {
        imageView.setVisibility(View.VISIBLE);
        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.car_placeholder).error(R.drawable.car_placeholder);
        Glide.with(mcontext)
                .load(carImages)
                .apply(requestOptions)
                .into(imageView);
//        } else {
//            imageView.setVisibility(View.GONE);
////            RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).skipMemoryCache(false).placeholder(R.drawable.no_img_pic).error(R.drawable.car_placeholder);
////            Glide.with(getActivity())
////                    .load(carImages)
////                    .apply(requestOptions)
////                    .into(imageView);
//        }

    }

    // View Holder Class to handle Recycler View.
    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private ImageView carImage;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.title);
            carImage = itemView.findViewById(R.id.carImage);
        }
    }
}