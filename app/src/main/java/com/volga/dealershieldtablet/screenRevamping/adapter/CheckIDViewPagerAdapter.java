package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.volga.dealershieldtablet.customViews.SingleDirectionViewPager;
import com.volga.dealershieldtablet.ui.fragment.FragmentContactInfo;
import com.volga.dealershieldtablet.ui.fragment.FragmentDMVPreview;
import com.volga.dealershieldtablet.ui.fragment.FragmentLicenseInformation;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanDMVBack;
import com.volga.dealershieldtablet.ui.fragment.FragmentScanDMVFront;
import com.volga.dealershieldtablet.utils.AppConstant;

/**
 * Created by ${Shailendra} on 24-07-2019.
 */
public class CheckIDViewPagerAdapter extends FragmentStatePagerAdapter {
    private final SingleDirectionViewPager mViewPager;


    public CheckIDViewPagerAdapter(FragmentManager supportFragmentManager, SingleDirectionViewPager mViewPager) {
        super(supportFragmentManager);
        this.mViewPager = mViewPager;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            FragmentScanDMVFront nextFragment = new FragmentScanDMVFront();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 1) {
            FragmentDMVPreview nextFragment = new FragmentDMVPreview();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", true);
            bundle.putString(AppConstant.FROM_FRAGMENT, AppConstant.DMV_FRONT);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 2) {
            FragmentScanDMVBack nextFragment=new FragmentScanDMVBack();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
        } else if (position == 3) {
            FragmentLicenseInformation nextFragment = new FragmentLicenseInformation();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
//            return new FragmentLicenseInformation();
        }else if (position == 4) {
            FragmentContactInfo nextFragment = new FragmentContactInfo();
            Bundle bundle = new Bundle();
            bundle.putBoolean("checkId", true);
            nextFragment.setArguments(bundle);
            return nextFragment;
//            return new FragmentLicenseInformation();
        }
//      else if (position == 6) {
//            return new FragmentLicenseInfoConfirmation();
//        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

}