package com.volga.dealershieldtablet.screenRevamping.pojo;

import com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.Document;

/**
 * Created by ${Shailendra} on 20-09-2019.
 */
public class CoBuyerDocs {
    private String DocId;

    private String DocUrl;

    private String CoBuyerId;

    private String DateCreatedUtc;

    private String DateModifiedUtc;

    private String Id;

    private String DocTypeName;

    private com.volga.dealershieldtablet.Retrofit.Pojo.DealershipData.Document Document;

    public String getDocId ()
    {
        return DocId;
    }

    public void setDocId (String DocId)
    {
        this.DocId = DocId;
    }

    public String getDocUrl ()
    {
        return DocUrl;
    }

    public void setDocUrl (String DocUrl)
    {
        this.DocUrl = DocUrl;
    }

    public String getCoBuyerId ()
    {
        return CoBuyerId;
    }

    public void setCoBuyerId (String CoBuyerId)
    {
        this.CoBuyerId = CoBuyerId;
    }

    public String getDateCreatedUtc ()
    {
        return DateCreatedUtc;
    }

    public void setDateCreatedUtc (String DateCreatedUtc)
    {
        this.DateCreatedUtc = DateCreatedUtc;
    }

    public String getDateModifiedUtc ()
    {
        return DateModifiedUtc;
    }

    public void setDateModifiedUtc (String DateModifiedUtc)
    {
        this.DateModifiedUtc = DateModifiedUtc;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getDocTypeName ()
    {
        return DocTypeName;
    }

    public void setDocTypeName (String DocTypeName)
    {
        this.DocTypeName = DocTypeName;
    }

    public Document getDocument ()
    {
        return Document;
    }

    public void setDocument (Document Document)
    {
        this.Document = Document;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DocId = "+DocId+", DocUrl = "+DocUrl+", CoBuyerId = "+CoBuyerId+", DateCreatedUtc = "+DateCreatedUtc+", DateModifiedUtc = "+DateModifiedUtc+", Id = "+Id+", DocTypeName = "+DocTypeName+", Document = "+Document+"]";
    }
}