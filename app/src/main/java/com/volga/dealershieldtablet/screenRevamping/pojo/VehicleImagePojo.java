package com.volga.dealershieldtablet.screenRevamping.pojo;

import android.net.Uri;

public class VehicleImagePojo {
   public String name;
   public String url;

   public String getDocTypeName() {
      return docTypeName;
   }

   public void setDocTypeName(String docTypeName) {
      this.docTypeName = docTypeName;
   }

   public String docTypeName;

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }

   public Uri getUri() {
      return uri;
   }

   public void setUri(Uri uri) {
      this.uri = uri;
   }

   public Uri uri;
}
