package com.volga.dealershieldtablet.screenRevamping.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pdfview.PDFView;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.NewThirdParty.ThirdPartyObj;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.ImagesSynced;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.Retrofit.Pojo.ThirdParty.InitThirdParty;
import com.volga.dealershieldtablet.app.DSAPP;
import com.volga.dealershieldtablet.interfaceCallback.GoBackNextInterface;
import com.volga.dealershieldtablet.screenRevamping.activity.MultiSignViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;
import com.volga.dealershieldtablet.utils.TimeZoneUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ${Shailendra} on 22-08-2018.
 */
public class SinglePageSignFragment extends BaseFragment {
    private View mView;
    private DSTextView next;
    GoBackNextInterface iGoToNextPage;
    String thirdParty;
    private File svgFile;
    private boolean isSigned;
    private boolean runningSign;
    ImageView delete;
    private boolean isCoBuyer = false,showBox=false;
    private DSTextView sign, time;
    private SignaturePad signaturePad;
    private PDFView pdfView;
    String appendingString = "";
    private LinearLayout totalView;
    private FrameLayout frameLayout;
    private DSTextView timeLayout;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (GoBackNextInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && signaturePad != null) {
            signaturePad.clear();
//            enableNext(next);
            if (showBox) {
                disableWithGray(next);
            }
            isSigned = false;
            updateData();
        } else {
            runningSign = false;
        }
    }

    private void updateData() {
        File file;
        file = ShowImageFromPath(name + "" + MultiSignViewPager.currentPage + ".pdf", name + appendingString);
        if (file != null && file.exists()) {
            pdfView.fromFile(file).show();
        }
    }

    String name, tpID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.single_page_sign_layout, container, false);
        initView();
        return mView;
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        TextView back = mView.findViewById(R.id.text_back_up);
        back.setVisibility(View.VISIBLE);

        delete = mView.findViewById(R.id.deleteButton);
        sign = mView.findViewById(R.id.signhere);
        totalView=mView.findViewById(R.id.totalView);
        frameLayout=mView.findViewById(R.id.frameLayout);
//        timeLayout=mView.findViewById(R.id.timeLayout);
        time = mView.findViewById(R.id.time);
        disableDelete(delete);
          pdfView = mView.findViewById(R.id.activity_main_pdf_view);
        if (getArguments() != null) {
            isCoBuyer = getArguments().getBoolean("isCoBuyer");
            showBox = getArguments().getBoolean("showBox");
            name = getArguments().getString("name");
            tpID = getArguments().getString("tpID");
        }
        if (!showBox){
            totalView.setVisibility(View.GONE);
            frameLayout.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
            next.setVisibility(View.VISIBLE);
            enableWithGreen(next);
        }else {
            totalView.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.VISIBLE);
            time.setVisibility(View.VISIBLE);
            next.setVisibility(View.VISIBLE);
            disableWithGray(next);
        }
        Gson gson = new Gson();
        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);

        isCoBuyer = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer();

        if (tpID.equalsIgnoreCase("1")) {
            thirdParty = "CarFax_Report";
        } else {
            thirdParty = "AutoCheck_Report";
        }

        int langId = 1;
        if (!isCoBuyer)
            langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getSelectedLanguageId();
        else {
            langId = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getCoBuyerSelectedLanguageId();
        }
        if (langId == 2 || langId > 3 || langId == 0) {
            langId = 1;
        }
        String  reportFeatchTime = null;
        ArrayList<String> byteList = new ArrayList<>();
        ArrayList<ThirdPartyObj> listLangName = new ArrayList<>();
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList() != null)
            listLangName = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getThirdPartyList().getValue();
        for (int i = 0; i < listLangName.size(); i++) {
            if (listLangName.get(i).getId().equalsIgnoreCase(tpID)) {
                byteList.addAll(listLangName.get(i).getThirdPartyList().getMessage());
            }
        }
        byteList.add(byteList.size(), " ");
        if (recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue().size() > 0) {
            ArrayList<InitThirdParty> list = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getVehicleDetails().getInitiateThirdParty().getValue();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getThirdPartyID().equalsIgnoreCase(tpID) && list.get(i).getLanguageId().equalsIgnoreCase(langId + "") && !list.get(i).getThirdPartyReportRawId().equalsIgnoreCase("0")) {
                    appendingString = list.get(i).getThirdPartyReportRawId();
                    reportFeatchTime = list.get(i).getReportFetchDateTimeUTC();

                }
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        SimpleDateFormat outputFormatTime = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.getDefault());
        Date reportDate = null;
        String returnDate = null;
        try {
            reportDate = sdf.parse(reportFeatchTime);
            long longs = TimeZoneUtils.convertTime(reportDate.getTime(), TimeZone.getTimeZone("UTC"), TimeZone.getTimeZone("America/Los_Angeles"));
            returnDate = outputFormatTime.format(longs);
            returnDate = returnDate + " PST";
//            returnDate = returnDate.replace("a. m.", "AM");
//            returnDate = returnDate.replace("p.m.", "PM");
//            returnDate = returnDate.replace("a.m.", "AM");
//            returnDate = returnDate.replace("p. m.", "PM");
//            returnDate = returnDate.replace("GMT", "PST");
//            returnDate = returnDate.replace("PDT", "PST");
//            returnDate = returnDate.replace("+00:00", "");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        updateData();

        next.setVisibility(View.VISIBLE);
//        disableWithGray(next);
        signaturePad = (SignaturePad) mView.findViewById(R.id.signaturePad);
        signaturePad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        signaturePad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSigned = false;

            }
        });
        mView.findViewById(R.id.totalView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
                isSigned = false;
                disableWithGray(next);
                sign.setVisibility(View.VISIBLE);
            }
        });

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                sign.setVisibility(View.GONE);
                enableDelete(delete);
                time.setText(getCurrentTimeString());
                Log.e("signing start: ", " >>>>>>>>>>>>>>");
//                Handler mainHandler = new Handler(getActivity().getMainLooper());
//                Runnable myRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//                if (!runningSign) {
//                    runningSign = true;
//                    File file;
//                    if (!isCoBuyer) {
//                        file = getOutputFile("UserPic", "language_screen_pic");
//                    } else {
//                        file = getOutputFile("UserPic", "co_language_screen_pic");
//                    }
//                    if (PreferenceManger.getBooleanValue(AppConstant.TAKE_USER_PHOTO)) {
//                        takeUserPicture(file);
//                    } else {
//                        isTaken = true;
//                        if (selectedLanguage != null && selectedLanguage.length() > 0) {
//                            next.setVisibility(View.VISIBLE);
//                            enableWithGreen(next);
//                        }
//                    }
//                }
//                    } // This is your code
//                };
//                mainHandler.post(myRunnable);

            }

            @Override
            public void onSigned() {
                isSigned = true;
                enableWithGreen(next);
                next.setVisibility(View.VISIBLE);
            }

            @Override
            public void onClear() {
                Log.e("Clear: ", "Cleared");
                sign.setVisibility(View.VISIBLE);
                disableDelete(delete);
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showBox) {
                    addSvgSignatureToGallery(signaturePad.getTransparentSignatureBitmap(), name);
                }
                iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.goToBackIndex();
            }
        });

    }

    public File ShowImageFromPath(String fileName, String mainFolder) {
        String path = null;

//        if (mainFolder.equalsIgnoreCase("LicenseFront") && PreferenceManger.getStringValue(AppConstant.DMV_NUMBER).length() == 0) {
//           if (!PreferenceManger.getBooleanValue(AppConstant.IS_CO_BUYER)){
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/temp/LicenseFront/" + fileName;
//           }else {
//               path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
//                       + "/DSXT/"+PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)+"/LicenseFront/" + fileName;
//           }
//        } else {
//        if (!isCheckId)
        path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + mainFolder + "/" + fileName;
//        else
//            path = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/CheckID/" + mainFolder + "/" + fileName;
//        }
        File imgFile = new File(path);
//        fileToDelete = imgFile;
//        File filenew = null;
//        try {
//            filenew = new Compressor(DSAPP.getInstance())
//                    .compressToFile(imgFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Date lastModDate = new Date(imgFile.lastModified());
        Log.i("LoG..", "File last modified @ : " + lastModDate.toString());

//        if (filenew != null) {
//            if (filenew.exists()) {
//                return Uri.fromFile(filenew);
//            }
//        } else {
        if (imgFile.exists()) {
            return imgFile;
        }
//        }
        return null;
    }

    public File getOutputFile(String folderName, String imageName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir;

        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public boolean addSvgSignatureToGallery(Bitmap signatureSvg, String id) {
        boolean result = false;
        try {
            Gson gson = new GsonBuilder().create();
            RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
            int count = ((MultiSignViewPager) getActivity()).getViewPager().getAdapter().getCount();
            String imgName;
            //autocheck_report carfax
//            if (MultiSignViewPager.currentPage < count) {
            int page = MultiSignViewPager.currentPage;
            page++;
            if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
                imgName = "co_buyer_third_party_sign_" + thirdParty + "_page_" + page;
            } else {
                imgName = "third_party_sign_" + thirdParty + "_page_" + page;
            }
//            } else {
//                if (recordData != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)) != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions() != null && recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().isCoBuyer()) {
//                    imgName = "co_buyer_third_party_sign_" + id.trim().replace(" ", "_") + "_page_final";
//                } else {
//                    imgName = "third_party_sign_" + id.trim().replace(" ", "_") + "_page_final";
//                }
//            }
            svgFile = getOutputMediaFile("Signatures", imgName);//autocheck_report carfax
            OutputStream stream = new FileOutputStream(svgFile);
//            OutputStreamWriter writer = new OutputStreamWriter(stream);
            signatureSvg.compress(Bitmap.CompressFormat.PNG, 100, stream);

//            writer.write(signatureSvg);
//            writer.close();
            stream.flush();
            stream.close();
//            scanMediaFile(svgFile);
            String path_signature = DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM) + "/DSXT/" + recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getCustomerDetails().getDLNumber() + "/Signatures";
            ArrayList<ImagesSynced> arraylist = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().getMultiSignImages();
            if (arraylist == null) {
                arraylist = new ArrayList<>();
            }
            ImagesSynced synced = new ImagesSynced();
            synced.setFile(svgFile);
            synced.setName(imgName);
            synced.setSynced(false);
            arraylist.add(synced);
            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setMultiSignImages(arraylist);
            String recordDataString = gson.toJson(recordData);
            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File getOutputMediaFile(String folderName, String imageName) {
        File mediaStorageDir;
        mediaStorageDir = new File(DSAPP.getInstance().getExternalFilesDir(Environment.DIRECTORY_DCIM)
                + "/DSXT/" + PreferenceManger.getStringValue(AppConstant.DMV_NUMBER) + "/" + folderName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = "IMG_" + imageName + ".png";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }
}
