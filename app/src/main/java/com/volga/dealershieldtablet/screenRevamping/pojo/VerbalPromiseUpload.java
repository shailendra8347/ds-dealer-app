package com.volga.dealershieldtablet.screenRevamping.pojo;

public class VerbalPromiseUpload{
	private String VerbalPromiseId;
	private boolean value;

	public void setVerbalPromiseId(String verbalPromiseId){
		this.VerbalPromiseId = verbalPromiseId;
	}

	public String getVerbalPromiseId(){
		return VerbalPromiseId;
	}

	public void setValue(boolean value){
		this.value = value;
	}

	public boolean isValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"VerbalPromiseUpload{" + 
			"VerbalPromiseId = '" + VerbalPromiseId + '\'' +
			",value = '" + value + '\'' + 
			"}";
		}
}
