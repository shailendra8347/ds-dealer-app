package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.Record;
import com.volga.dealershieldtablet.Retrofit.Pojo.Records.RecordData;
import com.volga.dealershieldtablet.screenRevamping.pojo.VerbalPromise;
import com.volga.dealershieldtablet.utils.AppConstant;
import com.volga.dealershieldtablet.utils.PreferenceManger;

import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 12-10-2017.
 */

public class VerbalPromiseAdapter extends RecyclerView.Adapter<VerbalPromiseAdapter.TypeRow> {

    private Activity activity;
    private RecordData recordData;
    private Gson gson;
    private ArrayList<VerbalPromise> list;
    private AllChecked allChecked;
    int total;

    public interface AllChecked {
        void allChecked(boolean allChecked);
    }


    public VerbalPromiseAdapter(Activity activity, ArrayList<VerbalPromise> list, AllChecked allChecked) {
        this.activity = activity;
        this.list = list;
        this.allChecked = allChecked;

    }

    @NonNull
    @Override
    public TypeRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.history_row, parent, false);
        return new TypeRow(mainGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull final TypeRow holder, @SuppressLint("RecyclerView") final int position) {
        gson = new GsonBuilder().create();
        recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
        holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
       holder.bulletPoint.setVisibility(View.VISIBLE);
       holder.checkBox.setVisibility(View.GONE);
        Record record = recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER));
        if (!record.getSettingsAndPermissions().isCoBuyer()) {
            if (record.getSettingsAndPermissions().getSelectedLanguage() == null || record.getSettingsAndPermissions().getSelectedLanguage().equalsIgnoreCase("English"))
                holder.name.setText(Html.fromHtml(list.get(position).getName()));
            else {
                for (int i = 0; i < list.get(position).getVerbalPromiseTranslation().size(); i++) {
                    if (record.getSettingsAndPermissions().getSelectedLanguageId() == Integer.parseInt(list.get(position).getVerbalPromiseTranslation().get(i).getDestinationLanguageId().trim())) {
                        if (list.get(position).getVerbalPromiseTranslation().get(i).getText() != null) {
                            holder.name.setText(Html.fromHtml(list.get(position).getVerbalPromiseTranslation().get(i).getText()));
                        } else {
                            holder.name.setText(Html.fromHtml(list.get(position).getName()));
                        }
                    }
                }
            }
        } else {
            if (record.getSettingsAndPermissions().getCoBuyerSelectedLanguage() == null || record.getSettingsAndPermissions().getCoBuyerSelectedLanguage().equalsIgnoreCase("English"))
                holder.name.setText(Html.fromHtml(list.get(position).getName()));
            else {
                for (int i = 0; i < list.get(position).getVerbalPromiseTranslation().size(); i++) {
                    if (record.getSettingsAndPermissions().getCoBuyerSelectedLanguageId() == Integer.parseInt(list.get(position).getVerbalPromiseTranslation().get(i).getDestinationLanguageId().trim())) {
                        if (list.get(position).getVerbalPromiseTranslation().get(i).getText() != null)
                            holder.name.setText(Html.fromHtml(list.get(position).getVerbalPromiseTranslation().get(i).getText()));
                        else {
                            holder.name.setText(Html.fromHtml(list.get(position).getName()));
                        }
                    }
                }
            }
        }

        //changed for bullet point

//        holder.name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.checkBox.isChecked()) {
//                    holder.checkBox.setChecked(false);
//                    checkChangedLocal(position, false, holder);
//                } else {
//                    holder.checkBox.setChecked(true);
//                    checkChangedLocal(position, true, holder);
//                }
//            }
//        });
        holder.checkBox.setOnCheckedChangeListener(null);
        if (list.get(position).getIsActive()) {
            holder.checkBox.setChecked(true);
//            holder.name.setTextColor(activity.getResources().getColor(R.color.button_color_blue));
        }else {
            holder.checkBox.setChecked(false);
            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                checkChangedLocal(position, b, holder);
            }
        });
    }

    private void checkChangedLocal(int position, boolean b, TypeRow holder) {
        list.get(position).setIsActive(b);
//        if (b)
//            holder.name.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
//        else {
//            holder.name.setTextColor(activity.getResources().getColor(R.color.text_color_gray));
//        }
        allChecked.allChecked(checkAllItemsSeletcted());
        notifyByHandler();
    }

    private boolean checkAllItemsSeletcted() {
        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).getIsActive()) {
                return false;
            }
        }
        return true;
    }

    private void notifyByHandler() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        }, 50);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class TypeRow extends RecyclerView.ViewHolder {
        TextView name,bulletPoint;
        CheckBox checkBox;

        TypeRow(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkBox);
            name = itemView.findViewById(R.id.name);
            bulletPoint = itemView.findViewById(R.id.bulletPoint);
        }
    }
}
