package com.volga.dealershieldtablet.screenRevamping.pojo;

public class CheckIDRequest {
 private int DealershipId;
 private String TradeDateTime;
 private String FirstName;
 private String MiddleName;
 private String LastName;
 private int GenderTypeId;
 private String DateOfBirth;
 private String Address1;
 private String Address2;
 private String City;
 private String Zipcode;
 private String State;
 private String Country;
 private String DrivingLicenseNumber;
 private String DrivingLicenseExpiryDate;
 private String Email;
 private String ContactNumber,TradeCompletionUTC;

 public String getEmail() {
  return Email;
 }

 public void setEmail(String email) {
  Email = email;
 }

 public String getContactNumber() {
  return ContactNumber;
 }

 public void setContactNumber(String contactNumber) {
  ContactNumber = contactNumber;
 }

 public boolean isTermConditionAccepted() {
  return IsTermConditionAccepted;
 }

 public void setTermConditionAccepted(boolean termConditionAccepted) {
  IsTermConditionAccepted = termConditionAccepted;
 }

 private boolean IsTermConditionAccepted;


 // Getter Methods 

 public int getDealershipId() {
  return DealershipId;
 }

 public String getTradeDateTime() {
  return TradeDateTime;
 }

 public String getFirstName() {
  return FirstName;
 }

 public String getMiddleName() {
  return MiddleName;
 }

 public String getLastName() {
  return LastName;
 }

 public int getGenderTypeId() {
  return GenderTypeId;
 }

 public String getDateOfBirth() {
  return DateOfBirth;
 }

 public String getAddress1() {
  return Address1;
 }

 public String getAddress2() {
  return Address2;
 }

 public String getCity() {
  return City;
 }

 public String getZipcode() {
  return Zipcode;
 }

 public String getState() {
  return State;
 }

 public String getCountry() {
  return Country;
 }

 public String getDrivingLicenseNumber() {
  return DrivingLicenseNumber;
 }

 public String getDrivingLicenseExpiryDate() {
  return DrivingLicenseExpiryDate;
 }

 // Setter Methods 

 public void setDealershipId(int DealershipId) {
  this.DealershipId = DealershipId;
 }

 public void setTradeDateTime(String TradeDateTime) {
  this.TradeDateTime = TradeDateTime;
 }

 public void setFirstName(String FirstName) {
  this.FirstName = FirstName;
 }

 public void setMiddleName(String MiddleName) {
  this.MiddleName = MiddleName;
 }

 public void setLastName(String LastName) {
  this.LastName = LastName;
 }

 public void setGenderTypeId(int GenderTypeId) {
  this.GenderTypeId = GenderTypeId;
 }

 public void setDateOfBirth(String DateOfBirth) {
  this.DateOfBirth = DateOfBirth;
 }

 public void setAddress1(String Address1) {
  this.Address1 = Address1;
 }

 public void setAddress2(String Address2) {
  this.Address2 = Address2;
 }

 public void setCity(String City) {
  this.City = City;
 }

 public void setZipcode(String Zipcode) {
  this.Zipcode = Zipcode;
 }

 public void setState(String State) {
  this.State = State;
 }

 public void setCountry(String Country) {
  this.Country = Country;
 }

 public void setDrivingLicenseNumber(String DrivingLicenseNumber) {
  this.DrivingLicenseNumber = DrivingLicenseNumber;
 }

 public void setDrivingLicenseExpiryDate(String DrivingLicenseExpiryDate) {
  this.DrivingLicenseExpiryDate = DrivingLicenseExpiryDate;
 }

 public String getTradeCompletionUTC() {
  return TradeCompletionUTC;
 }

 public void setTradeCompletionUTC(String tradeCompletionUTC) {
  TradeCompletionUTC = tradeCompletionUTC;
 }
}