package com.volga.dealershieldtablet.screenRevamping.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.volga.dealershieldtablet.R;
import com.volga.dealershieldtablet.interfaceCallback.IGoToNextPage;
import com.volga.dealershieldtablet.screenRevamping.activity.NewDealViewPager;
import com.volga.dealershieldtablet.ui.customUI.DSTextView;
import com.volga.dealershieldtablet.ui.fragment.BaseFragment;

import java.util.Objects;

/**
 * Created by ${Shailendra} on 22-08-2018.
 */
public class ProofOfInsurence extends BaseFragment {
    private View mView;
    private SignaturePad signaturePad;
    private DSTextView next;
    private boolean isSigned = false, isAnsSelected = false;
    IGoToNextPage iGoToNextPage;
    private boolean isTradeIn = false;
    private Button yes, no;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            iGoToNextPage = (IGoToNextPage) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onViewSelected");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            updateData();
            showFragmentInPortrait();
            //Code for displaying already selected Confirmation
        }
    }

    private void updateData() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.review_edit_disclosure, container, false);
        initView();
        return mView;
    }

    private void initView() {
        next = mView.findViewById(R.id.text_next);
        TextView back = mView.findViewById(R.id.text_back_up);
        final DSTextView text_cancelBack = mView.findViewById(R.id.text_cancelBack);
        DSTextView txt_logout = mView.findViewById(R.id.txt_logout);
        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        text_cancelBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        DSTextView title = mView.findViewById(R.id.title);
        title.setText(Html.fromHtml(getString(R.string.insurence_image_new)));
        yes = mView.findViewById(R.id.yes);
        no = mView.findViewById(R.id.no);
        updateData();
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.whatNextClick();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*show confirmation pop up*/
                try {
                    ((NewDealViewPager) Objects.requireNonNull(getActivity())).setCurrentPage(67);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        next.setVisibility(View.GONE);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetRecordData(isTradeIn);
                iGoToNextPage.whatNextClick();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iGoToNextPage.goToBackIndex();
//                if (PreferenceManger.getIntegerValue(AppConstant.SKIPPED_PAGE) > 25)
//                    showFragmentInLandscape();
            }
        });

    }

    public void SetRecordData(boolean b) {
        iGoToNextPage.whatNextClick();
//        PreferenceManger.putBoolean(AppConstant.IS_TRADE_IN_SELECTED, isTradeIn);
//        Gson gson = new GsonBuilder().create();
//        RecordData recordData = gson.fromJson(PreferenceManger.getStringValue(AppConstant.RECORD_DATA), RecordData.class);
//        if (recordData!=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER))!=null&&recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions()!=null) {
//            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setHasTradeIn(b);
//            recordData.where(PreferenceManger.getStringValue(AppConstant.DMV_NUMBER)).getSettingsAndPermissions().setTradeIn(b);
//            String recordDataString = gson.toJson(recordData);
//            PreferenceManger.putString(AppConstant.RECORD_DATA, recordDataString);
//        }
//        if (!b) {
//            ((ViewPagerActivity) getActivity()).setCurrentPage(53);
//        } else {
//            iGoToNextPage.whatNextClick();
//        }

    }
}
