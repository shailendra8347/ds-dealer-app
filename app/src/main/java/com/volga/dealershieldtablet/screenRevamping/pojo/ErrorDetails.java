package com.volga.dealershieldtablet.screenRevamping.pojo;

public class ErrorDetails{
	private String ErrorDescription;
	private String Error;

	public void setErrorDescription(String errorDescription){
		this.ErrorDescription = errorDescription;
	}

	public String getErrorDescription(){
		return ErrorDescription;
	}

	public void setError(String error){
		this.Error = error;
	}

	public String getError(){
		return Error;
	}

	@Override
 	public String toString(){
		return 
			"ErrorDetails{" + 
			"ErrorDescription = '" + ErrorDescription + '\'' +
			",Error = '" + Error + '\'' +
			"}";
		}
}
