package com.volga.dealershieldtablet.screenRevamping.adapter;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.volga.dealershieldtablet.screenRevamping.fragment.MultisignFinalPage;
import com.volga.dealershieldtablet.screenRevamping.fragment.SinglePageSignFragment;

import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 24-07-2019.
 */
public class MultiSignViewPagerAdapter extends FragmentStatePagerAdapter {


    ArrayList<String> pageList = new ArrayList<>();
    String name,tpID;
    boolean isCoBuyer,showBox;

    public MultiSignViewPagerAdapter(FragmentManager supportFragmentManager, ArrayList<String> pageList,String name,boolean isCoBuyer,String tpID,boolean showBox) {
        super(supportFragmentManager);
        this.pageList = pageList;
        this.name=name;
        this.tpID=tpID;
        this.showBox=showBox;
        this.isCoBuyer=isCoBuyer;
    }

    @Override
    public Fragment getItem(int position) {
        if (position < pageList.size() - 1) {
            Bundle bundle = new Bundle();
            SinglePageSignFragment singlePageSignFragment = new SinglePageSignFragment();
            bundle.putString("name", name);
            bundle.putString("tpID", tpID);
            bundle.putBoolean("isCoBuyer", isCoBuyer);
            bundle.putBoolean("showBox", showBox);
            singlePageSignFragment.setArguments(bundle);
            return singlePageSignFragment;
        } else {
            Bundle bundle = new Bundle();
            MultisignFinalPage multisignFinalPage = new MultisignFinalPage();
            bundle.putString("name", name);
            bundle.putString("tpID", tpID);
            bundle.putBoolean("isCoBuyer", isCoBuyer);
            bundle.putBoolean("showBox", showBox);
            multisignFinalPage.setArguments(bundle);
            return multisignFinalPage;
        }
    }

    @Override
    public int getCount() {
        return pageList.size();
    }

    @Override
    public void finishUpdate(@NonNull ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

}