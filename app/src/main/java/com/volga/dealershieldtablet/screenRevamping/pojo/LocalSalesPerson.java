package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.ArrayList;

/**
 * Created by ${Shailendra} on 03-10-2019.
 */
public class LocalSalesPerson {
    public ArrayList<SalesPersonList> getLocalSales() {
        return localSales;
    }

    public void setLocalSales(ArrayList<SalesPersonList> localSales) {
        this.localSales = localSales;
    }

    ArrayList<SalesPersonList> localSales;
}
