package com.volga.dealershieldtablet.screenRevamping.pojo;

import java.util.ArrayList;

public class VerbalPromise
{
    private String DisplayOrder;

    private String IsForAll;

    private boolean IsActive;

    public boolean isOnlyForUsedCar() {
        return OnlyForUsedCar;
    }

    public void setOnlyForUsedCar(boolean onlyForUsedCar) {
        OnlyForUsedCar = onlyForUsedCar;
    }

    public boolean isOnlyForNewCar() {
        return OnlyForNewCar;
    }

    public void setOnlyForNewCar(boolean onlyForNewCar) {
        OnlyForNewCar = onlyForNewCar;
    }

    private boolean OnlyForUsedCar;
    private boolean OnlyForNewCar;

    private String Published;

    private ArrayList<VerbalPromiseTranslation> VerbalPromiseTranslation;

    private String Id;

    private String Name;

    public String getDisplayOrder ()
    {
        return DisplayOrder;
    }

    public void setDisplayOrder (String DisplayOrder)
    {
        this.DisplayOrder = DisplayOrder;
    }

    public String getIsForAll ()
    {
        return IsForAll;
    }

    public void setIsForAll (String IsForAll)
    {
        this.IsForAll = IsForAll;
    }

    public boolean getIsActive ()
    {
        return IsActive;
    }

    public void setIsActive (boolean IsActive)
    {
        this.IsActive = IsActive;
    }

    public String getPublished ()
    {
        return Published;
    }

    public void setPublished (String Published)
    {
        this.Published = Published;
    }

    public ArrayList<VerbalPromiseTranslation> getVerbalPromiseTranslation ()
    {
        return VerbalPromiseTranslation;
    }

    public void setVerbalPromiseTranslation (ArrayList<VerbalPromiseTranslation> VerbalPromiseTranslation)
    {
        this.VerbalPromiseTranslation = VerbalPromiseTranslation;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DisplayOrder = "+DisplayOrder+", IsForAll = "+IsForAll+", IsActive = "+IsActive+", Published = "+Published+", VerbalPromiseTranslation = "+VerbalPromiseTranslation+", Id = "+Id+", Name = "+Name+"]";
    }
}